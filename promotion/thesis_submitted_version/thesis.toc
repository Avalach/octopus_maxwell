\contentsline {chapter}{Abstract}{I}{Doc-Start}
\contentsline {chapter}{Deutsche Zusammenfassung}{III}{Doc-Start}
\contentsline {chapter}{Content}{V}{Doc-Start}
\contentsline {chapter}{Introduction}{1}{chapter*.2}
\contentsline {chapter}{\numberline {1}Maxwell's equations in Riemann-Silberstein formalism}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Riemann-Silberstein microscopic Maxwell's equations}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Definition of the microscopic Riemann-Silberstein vectors}{6}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Microscopic Maxwell's equations}{7}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Curl operation in representation of Spin-1 matrices }{8}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Riemann-Silberstein Maxwell Hamiltonian and eigensystem}{10}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Combined six-component helicity Riemann-Silberstein vector}{11}{subsection.1.1.5}
\contentsline {subsection}{\numberline {1.1.6}Photon\nobreakspace {}-\nobreakspace {}anti-photon relation of the six-component Riemann-Sil-berstein vector}{13}{subsection.1.1.6}
\contentsline {subsection}{\numberline {1.1.7}Scalar product of the six-component Riemann-Silberstein vector}{15}{subsection.1.1.7}
\contentsline {subsection}{\numberline {1.1.8}Eigenstate expansion of the Riemann-Silberstein six-vector}{16}{subsection.1.1.8}
\contentsline {section}{\numberline {1.2}Riemann-Silberstein approach for macroscopic Maxwell's equations}{17}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Riemann-Silberstein Maxwell's equations for linear media}{17}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Definition of the macroscopic Riemann-Silberstein vectors}{18}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Macroscopic Maxwell's equations}{19}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Combined helicity Riemann-Silberstein six-vector in linear medium}{21}{subsection.1.2.4}
\contentsline {chapter}{\numberline {2}Riemann-Silberstein time-evolution of Maxwell fields in Schr\"odinger-like form}{24}{chapter.2}
\contentsline {section}{\numberline {2.1}Time-evolution of homogeneous microscopic Maxwell's equations}{24}{section.2.1}
\contentsline {section}{\numberline {2.2}Time-evolution of inhomogeneous microscopic Riemann-Silberstein six-vector}{27}{section.2.2}
\contentsline {section}{\numberline {2.3}Time-evolution of macroscopic Maxwell's equations in linear medium}{29}{section.2.3}
\contentsline {section}{\numberline {2.4}Conservation of electric and magnetic Gau\ss {} laws in time}{30}{section.2.4}
\contentsline {chapter}{\numberline {3}Implementation of the Riemann-Silberstein Maxwell propagation in the real-time real-space code Octopus}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Discretized three-dimensional grid for the Maxwell field}{34}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Riemann-Silberstein Maxwell grid}{34}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Finite difference stencil}{34}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Parallelization strategy in the first principles code octopus}{39}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Discretized Riemann-Silberstein time-evolution operators}{40}{section.3.2}
\contentsline {section}{\numberline {3.3}Maxwell-propagation with Octopus}{41}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Comparison of the Octopus Maxwell-propagation with the electromagnetic propagation program MEEP}{42}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Longitudinal and transverse electromagnetic fields and currents}{46}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Maxwell boundaries}{52}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Absorbing boundaries by mask function}{52}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Absorbing boundaries by perfectly matched layer}{53}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Comparison of absorbing mask and perfectly matched layer \unhbox \voidb@x \hbox {boundaries}}{63}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Incident plane waves boundaries}{63}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Numerical dispersion for plane wave propagation}{68}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Incident waves boundaries plus absorbing boundaries}{70}{subsection.3.4.6}
\contentsline {section}{\numberline {3.5}Riemann-Silberstein Maxwell propagation for linear media}{71}{section.3.5}
\contentsline {chapter}{\numberline {4}Theoretical fundamentals of light-matter coupling}{77}{chapter.4}
\contentsline {section}{\numberline {4.1}Relativistic covariant notation}{77}{section.4.1}
\contentsline {section}{\numberline {4.2}Relativistic decomposition of spin particles and derivation of Maxwell's equations}{79}{section.4.2}
\contentsline {section}{\numberline {4.3}Multi-species Hamiltonian}{82}{section.4.3}
\contentsline {section}{\numberline {4.4}Quantum-electrodynamical density-functional theory for multi species}{84}{section.4.4}
\contentsline {section}{\numberline {4.5}Coupled Maxwell-Pauli-Kohn-Sham equations}{85}{section.4.5}
\contentsline {section}{\numberline {4.6}Classical limit for Nuclei}{87}{section.4.6}
\contentsline {chapter}{\numberline {5}Maxwell-Pauli-Kohn-Sham propagation on a three-dimensional grid}{90}{chapter.5}
\contentsline {section}{\numberline {5.1}Riemann-Silberstein Maxwell's equations with Kohn-Sham current density}{91}{section.5.1}
\contentsline {section}{\numberline {5.2}Riemann-Silberstein Maxwell propagation coupled to the Kohn-Sham current density}{94}{section.5.2}
\contentsline {section}{\numberline {5.3}Discretized time-evolution and Time-reversal symmetry of the Maxwell system}{96}{section.5.3}
\contentsline {section}{\numberline {5.4}Discretized time-evolution and Time-reversal symmetry of the matter system}{97}{section.5.4}
\contentsline {section}{\numberline {5.5}Kohn-Sham interaction Hamiltonian}{99}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Full minimal coupling}{99}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Multipole expansion}{100}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Transverse Riemann-Silberstein vector calculation}{101}{subsection.5.5.3}
\contentsline {section}{\numberline {5.6}Maxwell-Kohn-Sham multi-scale implementation}{102}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Multi-grid types}{102}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Multi-scales in time}{104}{subsection.5.6.2}
\contentsline {subsection}{\numberline {5.6.3}Finite difference operators and parallelization strategy}{105}{subsection.5.6.3}
\contentsline {subsection}{\numberline {5.6.4}Predictor-corrector method}{106}{subsection.5.6.4}
\contentsline {subsection}{\numberline {5.6.5}Forward coupling}{106}{subsection.5.6.5}
\contentsline {subsection}{\numberline {5.6.6}Forward and backward coupling}{107}{subsection.5.6.6}
\contentsline {section}{\numberline {5.7}Simulation of open quantum systems with the Maxwell-Kohn-Sham propagation}{108}{section.5.7}
\contentsline {section}{\numberline {5.8}Electromagnetic detectors}{108}{section.5.8}
\contentsline {section}{\numberline {5.9}Broken time reversal symmetry}{109}{section.5.9}
\contentsline {chapter}{\numberline {6}Applications}{110}{chapter.6}
\contentsline {section}{\numberline {6.1}Laser pulse simulation scheme for a plasmonic nanoparticle system and simulation parameters}{110}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Na$_{\mathrm {297}}$-dimer geometry and optical spectra}{112}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Simulation boxes and grid alignement}{113}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Measurement and detector regions}{113}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Laser pulse shape}{114}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Propagators}{115}{subsection.6.1.5}
\contentsline {section}{\numberline {6.2}Results from Ehrenfest-Maxwell-Pauli-Kohn-Sham simulations}{116}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Electric field enhancement}{116}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Next order in multipole coupling and energies}{124}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Electromagnetic detectors and harmonic generation}{126}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Ion motion}{128}{subsection.6.2.4}
\contentsline {subsection}{\numberline {6.2.5}Comparison of different density functionals}{130}{subsection.6.2.5}
\contentsline {chapter}{Summary, conclusion and outlook}{132}{chapter*.3}
\contentsline {chapter}{Appendices}{136}{section*.4}
\contentsline {chapter}{\numberline {A}Non-relativistic Pauli Hamiltonian in Coulomb gauge}{137}{Appendix.a.A}
\contentsline {section}{\numberline {A.1}Vector potential and Coulomb gauge}{137}{section.a.A.1}
\contentsline {section}{\numberline {A.2}Pauli-equation as non-relativistic limit of coupled light-matter systems}{138}{section.a.A.2}
\contentsline {section}{\numberline {A.3}Free matter Hamiltonians}{139}{section.a.A.3}
\contentsline {section}{\numberline {A.4}Free photon Hamiltonian}{140}{section.a.A.4}
\contentsline {section}{\numberline {A.5}Interaction Hamiltonians}{144}{section.a.A.5}
\contentsline {subsection}{\numberline {A.5.1}Longitudinal (Coulomb) interactions}{144}{subsection.a.A.5.1}
\contentsline {subsection}{\numberline {A.5.2}Transverse interactions}{145}{subsection.a.A.5.2}
\contentsline {chapter}{List of Figures}{148}{equation.a.A.5.20}
\contentsline {chapter}{List of Tables}{149}{chapter*.5}
\contentsline {chapter}{Bibliography}{150}{chapter*.6}
\contentsline {chapter}{Publication}{160}{chapter*.7}
\contentsline {chapter}{Acronyms}{161}{chapter*.7}
\contentsline {chapter}{Acknowledgements}{162}{chapter*.7}
\contentsline {chapter}{Colophon}{163}{chapter*.7}
