import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib import colors as mcolors

def fx(x,y,w):
    return - y * np.exp(-np.power(x,2)/(2*np.power(w,2))) * np.exp(-np.power(y,2)/(2*np.power(w,2))) 

def fy(x,y,w):
    return   x * np.exp(-np.power(x,2)/(2*np.power(w,2))) * np.exp(-np.power(y,2)/(2*np.power(w,2))) 

#fig = plt.figure(figsize=plt.figaspect(2)*1.0)
fig = plt.figure()
ax = fig.gca(projection='3d')

L=10.0
x = np.arange(-L, L+0.01, 0.1)
y = np.arange(-L, L+0.01, 0.1)

x, y = np.meshgrid(x, y)

#width = 4.0
#bound = L-width

#z = np.zeros((len(x),len(y)))

#for ii in range( 0, len(x)):
#    for jj in range( 0, len(y)):
#        if ( (np.absolute(x[ii,jj])-bound >= 0.0) and (np.absolute(y[ii,jj])-bound >= 0.0) ):
#           z[ii,jj] = (1.0 - np.power(np.sin((np.absolute(x[ii,jj])-bound)*np.pi/(2*width)),2)) * (1.0 - np.power(np.sin((np.absolute(y[ii,jj])-bound)*np.pi/(2*width)),2))
#        elif (np.absolute(x[ii,jj])-bound >= 0.0):
#           z[ii,jj] = 1.0 - np.power(np.sin((np.absolute(x[ii,jj])-bound)*np.pi/(2*width)),2)
#        elif (np.absolute(y[ii,jj])-bound >= 0.0):
#           z[ii,jj] = 1.0 - np.power(np.sin((np.absolute(y[ii,jj])-bound)*np.pi/(2*width)),2)
#        else:
#           z[ii,jj] = 1.0

norm = mcolors.Normalize(vmin=-1.,vmax=1.)
surf = ax.plot_surface(x, y, fy(x,y,2.0), rstride=1, cstride=1, cmap=cm.get_cmap('BrBG_r'), vmin=-1., vmax=1., linewidth=0, antialiased=False, norm=norm)

ax.text(  0.0, 15.0, -1.7, 'X', fontsize=20)
ax.text( 18.0,  0.0, -1.5, 'Y', fontsize=20)
ax.text( 21.0, -6.0,  0.7, r'j$_y$', fontsize=20, rotation=45)


ax.set_xticks([-10,-5,0,5,10])
ax.set_xticklabels([-10,-5,0,5,10])
ax.set_yticks([-10,-5,0,5,10])
ax.set_yticklabels([-10,-5,0,5,10])

#ax.set_xlabel('X', size=25, position=(  0.0, -2.0))
#ax.set_ylabel('Y', size=25, position=( -2.0,  0.0))
#ax.set_zlabel('j current')

ax.tick_params(axis='both', which='major', labelsize=15)

cbar = fig.colorbar(surf, shrink=0.5, aspect=10)
cbar.ax.tick_params(labelsize=15)
cbar.set_clim(-1.0,1.0)
cbar.remove()

ax.view_init(30, 50)

DPI = fig.get_dpi()
fig.set_size_inches(1040.0/float(DPI),1040.0/float(DPI))

fig.savefig('3D_gaussian_spatial_distribution_current_density_transverse_y.png')

plt.show()
