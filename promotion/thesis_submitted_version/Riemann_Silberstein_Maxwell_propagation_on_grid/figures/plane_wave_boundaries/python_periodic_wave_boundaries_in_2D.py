import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

# ax1.add_patch(patches.Rectangle((x,y),width,height)
#ax.add_patch(patches.Rectangle((-10,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
#ax.add_patch(patches.Rectangle((  8,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle(( -10,  8), 20,  2, ec='none', alpha=0.7, color=(0,0.692,0.692)))
ax.add_patch(patches.Rectangle(( -10,-10), 20,  2, ec='none', alpha=0.7, color=(0,0.692,0.692)))

r1 = [-10,-10]
r2 = [ 10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [ 10, 10]
r2 = [-10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [-10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [ 10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

# Box limit line x direction
ll=plt.arrow(-10.0,-11.0,20.0,0.0)
ll.set_clip_on(False)
ll=plt.arrow(-10.0,-11.4,0.0,0.8)
ll.set_clip_on(False)
ll=plt.arrow( 10.0,-11.4,0.0,0.8)
ll.set_clip_on(False)

#r1 = [-10, 10]
#r2 = [-10.2,-10.2]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [-10.0,-10.0]
#r2 = [-11.3,-10.7]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ 10.0, 10.0]
#r2 = [-11.3,-10.7]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)

# Box limit line y direction
ll=plt.arrow( 11.0,-10.0,0.0,20.0)
ll.set_clip_on(False)
ll=plt.arrow( 10.6,-10.0,0.8,0.0)
ll.set_clip_on(False)
ll=plt.arrow( 10.6, 10.0,0.8,0.0)
ll.set_clip_on(False)

#r1 = [ 11, 11]
#r2 = [-10, 10]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ 10.7, 11.3]
#r2 = [-10.0,-10.0]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ 10.7, 11.3]
#r2 = [ 10.0, 10.0]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)

# inner box limit
#r1 = [ -8.0,  8.0]
#r2 = [ -4.0, -4.0]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ -8.0, -8.0]
#r2 = [ -3.7, -4.3]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [  8.0,  8.0]
#r2 = [ -3.7, -4.3]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)

shift=0

plt.arrow(shift,0,7,0)

# inner box
x = np.linspace(-10.0, 10.0, 100)
y = np.linspace(-10.0, 10.0, 100)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) 
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=1.0)

#ax.text(0, -3.5, 'inner box limit',
#        horizontalalignment='center',
#        verticalalignment='center',
#        fontsize=11, color='black')
ax.text(0,-12, 'Simulation box limit in x-direction)',
        horizontalalignment='center',
        verticalalignment='center',
        fontsize=15, color='black')
ax.text(12, 0, 'Simulation box limit in y-direction',
        horizontalalignment='center',
        verticalalignment='center',
        fontsize=15, color='black',
        rotation='vertical')

#x = np.linspace(-10.0, 10.0, 200)
#y = np.linspace(-10.0, 10.0, 200)
#X, Y = np.meshgrid(x, y)
#Z = np.cos(2*X)*np.exp(-X**2/20)
#levels = np.linspace(-1,1,1000)
#cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100)

#x = np.linspace(-10.0, 10.0, 200)
#y = np.linspace(-15.0,-10.0,  50)
#X, Y = np.meshgrid(x, y)
#Z = np.cos(2*X)*np.exp(-X**2/20)
#levels = np.linspace(-1,1,1000)
#cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=0.5)

#x = np.linspace(-10.0, 10.0, 200)
#y = np.linspace( 10.0, 15.0,  50)
#X, Y = np.meshgrid(x, y)
#Z = np.cos(2*X)*np.exp(-X**2/20)
#levels = np.linspace(-1,1,1000)
#cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=0.5)

#cbar = fig.colorbar(cs)

incident_wave_region = mpatches.Patch(color=(0,0.692,0.692), alpha=0.7, label='Peridic boundaries region')

plt.legend(handles=[incident_wave_region], prop={'size': 15}, loc=(0.13,1.05))

for ii in range(-10, 10, 1):
   r1 = [  ii, ii]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)
   r1 = [ -10, 10]
   r2 = [  ii, ii]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)

ax.set_xlim(-10,10)
ax.set_ylim(-10,10)

ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

plt.axis('off')

DPI = fig.get_dpi()
fig.set_size_inches(6,6)

fig.savefig('periodic_boundaries_in_2D.png')

plt.show()
