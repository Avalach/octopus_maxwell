import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')


#ax.arrow(  0.0,-10.5,  0.0, 20.5, head_width=0.05, head_length=0.1, fc='k', ec='k')


# ax1.add_patch(patches.Rectangle((x,y),width,height)
ax.add_patch(patches.Rectangle(( -7, -7), 14, 14, ec='none', alpha=0.4, color=(1.000,1.000,0.500)))
ax.add_patch(patches.Rectangle((-10,-10),  3, 20, ec='none', alpha=0.7, color=(0.000,0.492,0.000)))
ax.add_patch(patches.Rectangle((  7,-10),  3, 20, ec='none', alpha=0.7, color=(0.000,0.492,0.000)))
ax.add_patch(patches.Rectangle(( -7,  7), 14,  3, ec='none', alpha=0.7, color=(0.000,0.492,0.000)))
ax.add_patch(patches.Rectangle(( -7,-10), 14,  3, ec='none', alpha=0.7, color=(0.000,0.492,0.000)))

r1 = [-10,-10]
r2 = [ 10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [ 10, 10]
r2 = [-10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [-10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [ 10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)


r1 = [ -7, -7]
r2 = [  7, -7]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)

r1 = [  7,  7]
r2 = [ -7,  7]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)

r1 = [ -7,  7]
r2 = [ -7, -7]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)

r1 = [ -7,  7]
r2 = [  7,  7]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)


# Box limit line x direction
#ll=plt.arrow(-10.0,-11.0,20.0,0.0)
#ll.set_clip_on(False)
#ll=plt.arrow(-10.0,-11.4,0.0,0.8)
#ll.set_clip_on(False)
#ll=plt.arrow( 10.0,-11.4,0.0,0.8)
#ll.set_clip_on(False)

#r1 = [-10, 10]
#r2 = [-11,-11]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [-10.0,-10.0]
#r2 = [-11.3,-10.7]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ 10.0, 10.0]
#r2 = [-11.3,-10.7]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)

# Box limit line y direction
#ll=plt.arrow( 11.0,-10.0,0.0,20.0)
#ll.set_clip_on(False)
#ll=plt.arrow( 10.6,-10.0,0.8,0.0)
#ll.set_clip_on(False)
#ll=plt.arrow( 10.6, 10.0,0.8,0.0)
#ll.set_clip_on(False)

#r1 = [ 11, 11]
#r2 = [-10, 10]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ 10.7, 11.3]
#r2 = [-10.0,-10.0]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ 10.7, 11.3]
#r2 = [ 10.0, 10.0]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)

# inner box limit
#r1 = [ -8.0,  8.0]
#r2 = [ -4.0, -4.0]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [ -8.0, -8.0]
#r2 = [ -3.7, -4.3]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)
#r1 = [  8.0,  8.0]
#r2 = [ -3.7, -4.3]
#line = lines.Line2D(r1, r2, color='black', lw=1.5)
#ax.add_line(line)

#ax.add_patch(patches.Rectangle((-5.2,3.8), 10.4, 2.4, ec='none', alpha=0.7, color=(1.0,1.0,1.0)))
#ax.text(0, 0.5, 'free Maxwell',
#        horizontalalignment='center',
#        verticalalignment='center',
#        fontsize=15, color='black')
#ax.text(0,-0.5, 'propagation region',
#        horizontalalignment='center',
#        verticalalignment='center',
#        fontsize=15, color='black')

#ax.text(0,-12, 'Simulation box limit in x-direction)',
#        horizontalalignment='center',
#        verticalalignment='center',
#        fontsize=15, color='black')
#ax.text(12, 0, 'Simulation box limit in y-direction',
#        horizontalalignment='center',
#        verticalalignment='center',
#        fontsize=15, color='black',
#        rotation='vertical')

#shift = 0

# inner box
#x = np.linspace(-10.0, 10.0, 100)
#y = np.linspace(-10.0, 10.0, 100)
#X, Y = np.meshgrid(x, y)
#Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) 
#levels = np.linspace(-1.4,1.4,1000)
#cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=1.0)

#x = np.linspace(-10.0, 10.0, 200)
#y = np.linspace(-10.0, 10.0, 200)
#X, Y = np.meshgrid(x, y)
#Z = np.cos(2*X)*np.exp(-X**2/20)
#levels = np.linspace(-1,1,1000)
#cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100)

#x = np.linspace(-10.0, 10.0, 200)
#y = np.linspace(-15.0,-10.0,  50)
#X, Y = np.meshgrid(x, y)
#Z = np.cos(2*X)*np.exp(-X**2/20)
#levels = np.linspace(-1,1,1000)
#cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=0.5)

#x = np.linspace(-10.0, 10.0, 200)
#y = np.linspace( 10.0, 15.0,  50)
#X, Y = np.meshgrid(x, y)
#Z = np.cos(2*X)*np.exp(-X**2/20)
#levels = np.linspace(-1,1,1000)
#cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=0.5)

#cbar = fig.colorbar(cs)

free_maxwell_region = mpatches.Patch(color=(1.000,1.000,0.500), alpha=0.7, label='free propagation region')
boundary_region = mpatches.Patch(color=(0.000,0.492,0.000), alpha=0.7, label='plane wave boundary region')


plt.legend(handles=[free_maxwell_region,boundary_region], prop={'size': 18}, loc=( 0.205,1.015))

# plot grid lines
#for ii in range(-10, 10, 1):
#   r1 = [  ii, ii]
#   r2 = [ -10, 10]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
#   ax.add_line(line)
#   r1 = [ -10, 10]
#   r2 = [  ii, ii]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
#   ax.add_line(line)

plt.xticks([-10.0, -7.0, 0.0, 7.0, 10.0], [r'$-L_x$', r'$-b_x$', r'$0.0$', r'$+b_x$', r'$+L_x$'])
plt.yticks([-10.0, -7.0, 0.0, 7.0, 10.0], [r'$-L_y$', r'$-b_y$', r'$0.0$', r'$+b_y$', r'$+L_y$'])

plt.tick_params(axis='both', which='major', labelsize=17)
plt.tick_params(axis='both', which='minor', labelsize=17)

r1 = [-10.0,-10.0]
r2 = [-11.5,-10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [ 10.0, 10.0]
r2 = [-11.5,-10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5,-10.0]
r2 = [ 10.0, 10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5,-10.0]
r2 = [-10.0,-10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)


r1 = [ -7.0, -7.0]
r2 = [-11.5, -7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [  7.0,  7.0]
r2 = [-11.5, -7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5, -7.0]
r2 = [  7.0,  7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5, -7.0]
r2 = [ -7.0, -7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)


shiftx = -2.5
shifty = -2.5
kabs = 2.0
kx = 1.78885
ky = 0.894427
width = 3.3

# inner box electromagnetic wave
x = np.linspace(-10.0, 10.0, 100)
y = np.linspace(-10.0, 10.0, 100)
X, Y = np.meshgrid(x, y)
Z = np.cos(kx*(X-shiftx)+ky*(Y-shifty)) * np.exp(- ( (kx*(X-shiftx) + ky*(Y-shifty))/kabs )**2/(2*width**2))
#Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) 
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=1.0)

ax.arrow(-2.5-0.5*0.894427,-2.5+0.5*1.78885, 3.0*1.78885, 3.0*0.894427, head_width=0.5, width=0.003, head_length=0.5, color=(0.300,0.300,0.300), alpha=1.0)
ax.text(2.8, 2.5, r'$\vec{k}$',
        horizontalalignment='center',
        verticalalignment='center',
        fontsize=20, color=(0.300,0.300,0.300))

ax.set_xlim(-11.5,11.5)
ax.set_ylim(-11.5,11.5)

#ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

#plt.axis('off')

DPI = fig.get_dpi()
fig.set_size_inches(1200.0/float(DPI),1000.0/float(DPI))

fig.savefig('plane_wave_boundaries_in_2D.png',bbox_inches='tight',pad_inches = 0)

plt.show()
