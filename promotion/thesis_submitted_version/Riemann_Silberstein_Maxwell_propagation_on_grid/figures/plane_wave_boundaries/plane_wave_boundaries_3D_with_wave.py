
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cbook
from matplotlib import cm
from matplotlib.colors import LightSource
import matplotlib.pyplot as plt

points = np.array([[-1, -1, -1],
                      [1, -1, -1 ],
                      [1, 1, -1],
                      [-1, 1, -1],
                      [-1, -1, 1],
                      [1, -1, 1 ],
                      [1, 1, 1],
                      [-1, 1, 1]])

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')


# wave function
r1 = np.arange(-10, 10, 0.05)
r2 = np.arange(-10, 10, 0.05)
R1, R2 = np.meshgrid(r1, r2)
R3 = 5*np.sin(R1)
ls = LightSource(270, 45)
rgb = ls.shade(R3, cmap=cm.seismic)
ax.plot_surface( R1, R2, R3, rstride=1, cstride=1, facecolors=rgb,
                 linewidth=0, antialiased=False, shade=True, alpha=0.5)
#ax.plot_wireframe(R1, R2, R3, rstride=5, cstride=5)


# outer surfaces:

r1 = [-10, 10]
r2 = [-10, 10]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface(-10,  R1,  R2, alpha=0.5)

r1 = [-10,  0]
r2 = [-10, 10]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( R1, -10,  R2, alpha=0.5)

r1 = [-10,  0]
r2 = [-10, 10]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( R1,  10,  R2, alpha=0.5)

r1 = [-10,  0]
r2 = [-10, 10]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( R1,  R2, -10, alpha=0.5)

r1 = [-10,  0]
r2 = [-10, 10]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( R1,  R2,  10, alpha=0.5)


# small front surfaces:

r1 = [-10, 10]
r2 = [-10, -8]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface(  0, R1, R2, alpha=0.5, edgecolor='none')

r1 = [-10, 10]
r2 = [  8, 10]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface(  0, R1, R2, alpha=0.5, edgecolor='none')

r1 = [-10, -8]
r2 = [ -8,  8]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface(  0, R1, R2, alpha=0.5, edgecolor='none')

r1 = [  8, 10]
r2 = [ -8,  8]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface(  0, R1, R2, alpha=0.5, edgecolor='none')


# inner surfaces

r1 = [ -8,  8]
r2 = [ -8,  8]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( -8, R1, R2, alpha=0.5)

r1 = [ -8,  0]
r2 = [ -8,  8]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( R1, R2, -8, alpha=0.5)

r1 = [ -8,  0]
r2 = [ -8,  8]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( R1, -8, R2, alpha=0.5)

r1 = [ -8,  0]
r2 = [ -8,  8]
R1, R2 = np.meshgrid(r1, r2)
ax.plot_surface( R1,  8, R2, alpha=0.5)



#r1 = [-10,  0]
#r2 = [-10, 10]
#R1, R2 = np.meshgrid(r1, r2)
#R3 = np.sin(10*R2)
#ax.plot_surface( R1, R2, R3, rstride=1, cstride=1, alpha=0.5)


#ax.scatter3D(points[:, 0], points[:, 1], points[:, 2])
ax.set_xlim(-10,10)
ax.set_ylim(-10,10)
ax.set_zlim(-10,10)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

ax.view_init(20,-35)

fig.savefig('plane_wave_boundaries_3D_with_wave.png')

plt.show()


#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.pyplot as plt
#import numpy as np


#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')

# Make data
#xx = 0 
#yy = np.linspace(-10, 10, 100)
#zz = np.linspace(-10, 10, 100)

#x = 0 # 10 * np.outer(np.cos(u), np.sin(v))
#y = yy # 10 * np.outer(np.sin(u), np.sin(v))
#z = zz # 10 * np.outer(np.ones(np.size(u)), np.cos(v))

# Plot the surface
#ax.plot_surface(x, y, z, color='b')
#
#plt.show()
