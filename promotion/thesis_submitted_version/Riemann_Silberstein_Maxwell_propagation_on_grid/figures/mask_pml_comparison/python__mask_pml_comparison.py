import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from scipy.fftpack import fft, ifft
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(figsize=(1,10))

gs = gridspec.GridSpec(4, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

ax1 = plt.subplot(gs[0:2,:])
ax2 = plt.subplot(gs[2:4,0])
#ax3 = plt.subplot(gs[4:8,0])
#ax4 = plt.subplot(gs[8:12,0])
#ax5 = plt.subplot(gs[12:16,0])


with open('./01_mask/energy_coefficient') as data1:
    readlines = data1.readlines()
    x_data1 = [line.split()[0] for line in readlines]
    y_data1 = [line.split()[1] for line in readlines]

with open('./01_mask/reflection_coefficient_08_00_00') as data2:
    readlines = data2.readlines()
    x_data2 = [line.split()[0] for line in readlines]
    y_data2 = [line.split()[1] for line in readlines]

with open('./01_mask/reflection_coefficient_08_08_00') as data3:
    readlines = data3.readlines()
    x_data3 = [line.split()[0] for line in readlines]
    y_data3 = [line.split()[1] for line in readlines]

with open('./02_pml/energy_coefficient') as data4:
    readlines = data4.readlines()
    x_data4 = [line.split()[0] for line in readlines]
    y_data4 = [line.split()[1] for line in readlines]

with open('./02_pml/reflection_coefficient_08_00_00') as data5:
    readlines = data5.readlines()
    x_data5 = [line.split()[0] for line in readlines]
    y_data5 = [line.split()[1] for line in readlines]

with open('./02_pml/reflection_coefficient_08_08_00') as data6:
    readlines = data6.readlines()
    x_data6 = [line.split()[0] for line in readlines]
    y_data6 = [line.split()[1] for line in readlines]




###################################################################################################
#  Plots:
###################################################################################################

fig.suptitle(r"Relative reflection error of the PML", size=18)

ax1_data1, = ax1.semilogy(x_data1, y_data1, 'o', linewidth=1.5, color=(0.000,0.000,0.700), label='mask absorbing')
ax1_data2, = ax1.semilogy(x_data4, y_data4, 'o', linewidth=1.5, color=(0.700,0.000,0.000), label='PML absorbing')

ax1_data1, = ax1.semilogy(x_data1, y_data1, '-', linewidth=1.5, color=(0.000,0.000,0.700), label='mask absorbing')
ax1_data2, = ax1.semilogy(x_data4, y_data4, '-', linewidth=1.5, color=(0.700,0.000,0.000), label='PML absorbing')


ax2_data1, = ax2.semilogy(x_data2,  y_data2, 'o', linewidth=1.5, color=(0.000,0.000,0.700), label='mask absorbing and $\vec{r}_{1}$')
ax2_data2, = ax2.semilogy(x_data5,  y_data5, 'o', linewidth=1.5, color=(0.700,0.000,0.000), label='PML absorbing and $\vec{r}_{1}$')

ax2_data1, = ax2.semilogy(x_data2,  y_data2, '-.', dashes=(3.0,4.0), linewidth=1.5, color=(0.000,0.000,0.700), label=r'mask absorbing and $\vec{r}_{1}$')
ax2_data2, = ax2.semilogy(x_data5,  y_data5, '-.', dashes=(3.0,4.0), linewidth=1.5, color=(0.700,0.000,0.000), label=r'PML absorbing and $\vec{r}_{1}$')


ax2_data3, = ax2.semilogy(x_data3, y_data3, 'o', linewidth=1.5, color=(0.000,0.000,0.700), label=r'mask absorbing and $\vec{r}_{2}$')
ax2_data4, = ax2.semilogy(x_data6, y_data6, 'o', linewidth=1.5, color=(0.700,0.000,0.000), label=r'PML absorbing and $\vec{r}_{2}$')

ax2_data3, = ax2.semilogy(x_data3, y_data3, ':', dashes=(1.0,1.0), linewidth=1.5, color=(0.000,0.000,0.700), label=r'mask absorbing and $\vec{r}_{2}$')
ax2_data4, = ax2.semilogy(x_data6, y_data6, ':', dashes=(1.0,1.0), linewidth=1.5, color=(0.700,0.000,0.000), label=r'PML absorbing and $\vec{r}_{2}$')



###################################################################################################
#  Panel 1: energy error coefficient
###################################################################################################
ax1.text(0.33,0.5,'a)',size=18)

# Axes label bottom
ax1.set_xlabel("Absorbing boundary width", size=15)
ax1.xaxis.set_label_position('bottom')

ax1.set_ylabel(r"$C_{E}$", size=15)
ax1.yaxis.set_label_position('left')

# labels
ax1.set_xticks([0.5,1.0,1.5,2.0,2.5])
ax1.set_xticklabels([0.5,1.0,1.5,2.0,2.5], size=12)
ax1.set_yticks([1e-7, 1e-5, 1e-3, 1e-1])
ax1.set_yticklabels([1e-7, 1e-5, 1e-3, 1e-1], size=12)
#ax1.set_
ax1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# Limits
ax1.set_xlim(0.25,2.75)
ax1.set_ylim(10**(-9), 10)



###################################################################################################
#  Panel 2: electric field reflection coefficient
###################################################################################################
ax2.text(0.33,2.0,'b)',size=18)

# Axes label bottom
ax2.set_xlabel(r"Absorbing boundary width $w_{\mathrm{ab}}$", size=15)
ax2.xaxis.set_label_position('bottom')

ax2.set_ylabel(r"$R_{1,2}$", size=15)
ax2.yaxis.set_label_position('left')

# labels
ax2.set_xticks([0.5,1.0,1.5,2.0,2.5])
ax2.set_xticklabels([0.5,1.0,1.5,2.0,2.5], size=12)
ax2.set_yticks([1e-3,1e-2,1e-1,1e0])
ax2.set_yticklabels([1e-3,1e-2,1e-1,1e0], size=12)
ax2.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='on', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# Limits
ax2.set_xlim(0.25,2.75)
ax2.set_ylim(10**(-4), 10)


ax1.legend(handles=[ax1_data1, ax1_data2, ax2_data1, ax2_data2, ax2_data3, ax2_data4], prop={'size': 11}, loc=( 1.02,0.19))


ax1.xaxis.grid(True, which='major')
ax1.yaxis.grid(True, which='major')

ax2.xaxis.grid(True,  which='major')
ax2.yaxis.grid(True, which='major')


DPI = fig.get_dpi()
fig.set_size_inches(720.0/float(DPI),600.0/float(DPI))

fig.savefig('mask_pml_comparison.png',bbox_inches='tight',pad_inches = 0)

plt.show()
