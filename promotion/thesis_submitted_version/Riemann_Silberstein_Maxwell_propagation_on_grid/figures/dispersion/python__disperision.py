import numpy as np
import matplotlib.pyplot as plt

with open('dispersion__dx_0.50__der_order_2') as f1:
    lines = f1.readlines()
    x1 = [line.split()[0] for line in lines]
    y1 = [line.split()[1] for line in lines]

with open('dispersion__dx_0.50__der_order_4') as f2:
    lines = f2.readlines()
    x2 = [line.split()[0] for line in lines]
    y2 = [line.split()[1] for line in lines]

with open('dispersion__dx_0.50__der_order_6') as f3:
    lines = f3.readlines()
    x3 = [line.split()[0] for line in lines]
    y3 = [line.split()[1] for line in lines]

with open('dispersion__dx_0.50__der_order_8') as f4:
    lines = f4.readlines()
    x4 = [line.split()[0] for line in lines]
    y4 = [line.split()[1] for line in lines]

with open('dispersion__dx_0.25__der_order_2') as f5:
    lines = f5.readlines()
    x5 = [line.split()[0] for line in lines]
    y5 = [line.split()[1] for line in lines]

with open('dispersion__dx_0.25__der_order_4') as f6:
    lines = f6.readlines()
    x6 = [line.split()[0] for line in lines]
    y6 = [line.split()[1] for line in lines]

with open('dispersion__dx_0.25__der_order_6') as f7:
    lines = f7.readlines()
    x7 = [line.split()[0] for line in lines]
    y7 = [line.split()[1] for line in lines]

with open('dispersion__dx_0.25__der_order_8') as f8:
    lines = f8.readlines()
    x8 = [line.split()[0] for line in lines]
    y8 = [line.split()[1] for line in lines]


fig = plt.figure()

plt.title("Variance between Maxwell propagation and analytical calculation", size=18)    
plt.xlabel(r'$\lambda$', size=18)
plt.ylabel('mean variance', size=18)

#data1 = mpatches.Patch(color=(0.118,0.118,0.588), alpha=1.0, label='dx = 0.50 and derivative order = 8')

#plt.legend(handles=[data1], prop={'size': 15}, loc=(0,0))

data1, = plt.plot(x1,y1, 'o', color=(0.118,0.118,0.588), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.50 and der. order = 2')
data2, = plt.plot(x2,y2, 'o', color=(0.588,0.118,0.118), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.50 and der. order = 4')
data3, = plt.plot(x3,y3, 'o', color=(0.118,0.588,0.118), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.50 and der. order = 6')
data4, = plt.plot(x4,y4, 'o', color=(1.000,0.705,0.000), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.50 and der. order = 8')

data5, = plt.plot(x5,y5, 's', color=(0.118,0.118,0.588), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.25 and der. order = 2')
data6, = plt.plot(x6,y6, 's', color=(0.588,0.118,0.118), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.25 and der. order = 4')
data7, = plt.plot(x7,y7, 's', color=(0.118,0.588,0.118), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.25 and der. order = 6')
data8, = plt.plot(x8,y8, 's', color=(1.000,0.705,0.000), label=r'$\Delta x$, $\Delta y$, $\Delta z$ = 0.25 and der. order = 8')


plt.loglog(x1, y1, basex=10, linestyle='-', color=(0.118,0.118,0.588))
plt.loglog(x2, y2, basex=10, linestyle='-', color=(0.588,0.118,0.118))
plt.loglog(x3, y3, basex=10, linestyle='-', color=(0.118,0.588,0.118))
plt.loglog(x4, y4, basex=10, linestyle='-', color=(1.000,0.705,0.000))

plt.loglog(x5, y5, basex=10, linestyle=':', color=(0.118,0.118,0.588))
plt.loglog(x6, y6, basex=10, linestyle=':', color=(0.588,0.118,0.118))
plt.loglog(x7, y7, basex=10, linestyle=':', color=(0.118,0.588,0.118))
plt.loglog(x8, y8, basex=10, linestyle=':', color=(1.000,0.705,0.000))

plt.tick_params(axis='both', which='major', labelsize=17, width=2.0, length=6.0)
plt.tick_params(axis='both', which='minor', labelsize=17, width=1.0, length=4.0)

plt.legend(handles=[data1,data2,data3,data4,data5,data6,data7,data8], prop={'size': 15}, loc=(0.006,0.75), ncol=2)

plt.xlim(1.0,2000)
plt.ylim(1e-14,1e+5)

DPI = fig.get_dpi()
fig.set_size_inches(1100.0/float(DPI),720.0/float(DPI))

fig.tight_layout()
fig.savefig('dispersion_log_log_2.png',bbox_inches='tight',pad_inches = 0)

plt.show()
