
  \chapter{Appendix}

    \section{Riemann-Silberstein Maxwell's equations in vacuum}   \label{appendix_chap_Mx_equation_in_RS}

      \noindent
      

      \subsection{Maxwell divergence condition in Riemann-Silberstein representation}   \label{appendix_subsec_Maxwell_Gauss_micro}

        \noindent
        The electric and magnetic Gau\ss{} laws are given by \cite{jackson1998classical}

        \begin{align}
             \nabla \cdot  \vec{E}(\vrt)
          &= \epsilon_{0}^{-1} \rho(\vrt)                                         \honemm ,          \label{eq_Appendix_Maxwell_equations_micro_Gauss_E}  \\[2.5mm]
             \nabla \cdot  \vec{B}(\vrt)
          &= 0                                                                    \honemm ,          \label{eq_Appendix_Maxwell_equations_micro_Gauss_B}
        \end{align}

        \noindent
        Multiplying Eq.~(\ref{eq_Appendix_Maxwell_equations_micro_Gauss_E}) by the imaginary unit and adding
        to Eq.~(\ref{eq_Appendix_Maxwell_equations_micro_Gauss_B}) yields
 
        \begin{align}
            \nabla \cdot \left( \vec{E}(\vrt) \pm \mi \vec{B}(\vrt) \right)
          = \frac{1}{\epsilon_{0}} \rho(\vrt)   \honemm .                          \label{eq_Appendix_RS_Gauss_vacuum_transform_1}
        \end{align}
 
        \noindent
        After another multiplication of the real part with $ \sqrt{\epsilon_{0}/2} $ and of the imaginary
        part with $ 1/\sqrt{2 \mu_{0}} $, we get
 
        \begin{align}
            \nabla \cdot
            \left(
              \sqrt{\frac{\epsilon_{0}}{2}} \vec{E}(\vrt) 
              \pm \mi \sqrt{\frac{1}{2 \mu_{0}}} \vec{B}(\vrt) 
            \right)
          = \sqrt{\frac{1}{2 \epsilon_{0}}} \rho(\vrt)   \honemm .                 \label{eq_Appendix_RS_Gauss_vacuum_transform_2}
        \end{align}
 
        \noindent
        Replacing the arising term in the brackets of the left-hand side with the definition of
        $ \vec{F}_{\pm}(\vrt) $ 

        \begin{align}
          \vec{F}_{\pm}(\vrt) = \sqrt{\frac{\epsilon_0}{2}} \vec{E} + \mi \sqrt{\frac{1}{2 \mu_0}} \vec{B}  \honemm ,    \label{eq_Appendix_RS_vector_pm}
        \end{align}

        \noindent
        leads directly to the combined electric and magnetic Gau\ss{} law in Riemann-Silberstein
        representation 
 
        \begin{align}
          \nabla \cdot \vec{F}_{\pm}(\vrt) &= \sqrt{\frac{1}{2 \epsilon_{0}}} \rho(\vrt)   \honemm .           \label{eq_Appendix_RS_Gauss_vacuum_pm}
        \end{align}


      \subsection{Maxwell Amp\`ere's and Faraday's law in Riemann-Silberstein representation}   \label{appendix_subsec_Ampere_Faraday_micro}

        \noindent
        Amp\`ere's and Faraday's law are given by

        \begin{align}
             \nabla \times \vec{B}(\vrt)
          &= \mu_{0} \left( \vec{j}(\vrt) 
             + \epsilon_{0} \frac{\partial}{\partial t} \vec{E}(\vrt) \right)     \honemm .          \label{eq_Appendix_Maxwell_equations_micro_Ampere}   \\[0.5mm]
             \nabla \times \vec{E}(\vrt)
          &= -\frac{\partial}{\partial t} \vec{B}(\vrt)                           \honemm ,          \label{eq_Appendix_Maxwell_equations_micro_Faraday}
        \end{align}

        \noindent
        We combine both equations by multiplying the imaginary unit to
        Amp\`ere's law Eq.~(\ref{eq_Appendix_Maxwell_equations_micro_Ampere}) and adding it to Faraday's law 
        Eq.~(\ref{eq_Appendix_Maxwell_equations_micro_Faraday})
 
        \begin{align}
            \mi \frac{\partial}{\partial t} \Big( \epsilon_{0} \vec{E}(\vrt) \pm \mi \vec{B}(\vrt) \Big)
          = \vec{\nabla} \times \vec{E}(\vrt) \pm \mi \frac{1}{\mu_{0}} \vec{\nabla} \times \vec{B}(\vrt)
            \mp \vec{j}(\vrt)   \honemm .                                                                      \label{eq_Appendix_RS_Faraday_Ampere_vacuum_transform_1}
        \end{align}
 
        \noindent
        The real part of Eq.~(\ref{eq_Appendix_RS_Faraday_Ampere_vacuum_transform_1}) multiplied by
        $ \sqrt{\epsilon_{0}/2} $ and the imaginary part multiplied by $ 1/\sqrt{2 \mu_{0}} $ transforms the equation
        into the form
 
        \begin{equation}
          \begin{alignedat}{2}
                \mi \frac{\partial}{\partial t} 
                \bigg( \sqrt{\frac{\epsilon_{0}}{2}} \vec{E}(\vrt) \pm \mi \sqrt{\frac{1}{2 \mu_{0}}} \vec{B}(\vrt) \bigg)
             &= \frac{1}{\sqrt{2 \mu_{0}}} \vec{\nabla} \times \vec{E}(\vrt)
                \pm \mi \frac{1}{\sqrt{2 \epsilon_{0}} \mu_{0}} \vec{\nabla} \times \vec{B}(\vrt)
                \mp \mi \frac{1}{\sqrt{2 \epsilon_{0}}} \vec{j}(\vrt)                              \label{eq_Appendix_RS_Faraday_Ampere_vacuum_transform_2}
          \end{alignedat}
        \end{equation}
 
        \noindent
        Using the electromagnetic definition of the speed of light $ c_{0} $
 
        \begin{align}
          c_{0} = \frac{1}{\sqrt{\epsilon_{0} \mu_{0}}}   \honemm ,                                \label{eq_Appendix_speed_of_light_vacuum}
        \end{align}
 
        \noindent
        and factorizing $ c_{0} $ on the right-hand side of Eq.~(\ref{eq_Appendix_RS_Faraday_Ampere_vacuum_transform_2})
        leads to the final form of the combined Faraday's and Amp\`ere's Riemann-Silberstein equations
 
        \begin{equation}
          \begin{alignedat}{2}
               \mi \frac{\partial}{\partial t}
               \bigg( \sqrt{\frac{\epsilon_{0}}{2}} \vec{E}(\vrt) \pm \mi \sqrt{\frac{1}{2 \mu_{0}}} \vec{B}(\vrt) \bigg)
            &= \pm c_{0} \vec{\nabla} \times  
               \bigg( \sqrt{\frac{\epsilon_{0}}{2}} \vec{E}(\vrt) \pm \mi \sqrt{\frac{1}{2 \mu_{0}}} \vec{B}(\vrt) \bigg)
               \mp \mi \frac{1}{\sqrt{2 \epsilon_{0}}} \vec{j}(\vrt)   \honemm .                   \label{eq_Appendix_RS_Faraday_Ampere_vacuum_transform_3}
          \end{alignedat}
        \end{equation}

        \noindent
        Substituting the Riemann-Silberstein vector $ \vec{F}_{\pm} $ from Eq.~(\ref{eq_Appendix_RS_vector_p})
        in Eq.~(\ref{eq_Appendix_RS_Faraday_Ampere_vacuum_transform_3}) leads directly in the Riemann-Silberstein representation of Amp\`ere's and 
        Faraday's law given by

        \begin{equation}
          \begin{alignedat}{2}
               \mi \frac{\partial}{\partial t}
               \vec{F}_{\pm}
            &= \pm c_{0} \vec{\nabla} \times  
               \vec{F}_{\pm}
               \mp \mi \frac{1}{\sqrt{2 \epsilon_{0}}} \vec{j}(\vrt)   \honemm .                   \label{eq_Appendix_RS_Faraday_Ampere_vacuum_transform_}
          \end{alignedat}
        \end{equation}



      %\subsection{Maxwell curl operation in Riemann-Silberstein representation}  \label{appendix_subsec_curl_operation_spin_matrices}

      %  \begin{align}
      %       \boldsymbol{S}_1
      %     = \left[
      %         \begin{array}{rrr}
      %           0 & \hspace{1.5mm}   0 &    0  \\
      %           0 & \hspace{1.5mm}   0 & -\mi  \\
      %           0 & \hspace{1.5mm} \mi &    0 
      %         \end{array}  
      %       \right]   \honemm ,   \hspace{5ex}
      %       \boldsymbol{S}_2
      %     = \left[
      %         \begin{array}{rrr}
      %               0 & \hspace{1.5mm} 0 & \hspace{1.5mm} \mi  \\
      %               0 & \hspace{1.5mm} 0 & \hspace{1.5mm}   0  \\
      %            -\mi & \hspace{1.5mm} 0 & \hspace{1.5mm}   0 
      %         \end{array} 
      %       \right]   \honemm ,   \hspace{5ex}
      %       \boldsymbol{S}_3
      %     = \left[ 
      %         \begin{array}{rrr} 
      %             0 & -\mi & \hspace{1.5mm} 0  \\
      %           \mi &    0 & \hspace{1.5mm} 0  \\
      %             0 &    0 & \hspace{1.5mm} 0
      %         \end{array}
      %       \right]   \honemm .
      %  \end{align}                                        \label{eq_Appendix_Spin-1_cartesian_matrices}

      %  \begin{equation}
      %    \begin{alignedat}{2}
      %         \vec{e}_1
      %      &= \begin{pmatrix}
      %           1 \\ 0 \\ 0
      %          \end{pmatrix} \honemm ,                    \hspace{5mm}
      %         \vec{e}_2
      %      &= \begin{pmatrix}
      %          0 \\ 1 \\ 0
      %         \end{pmatrix} \honemm ,                     \hspace{5mm}
      %         \vec{e}_3
      %      &= \begin{pmatrix}
      %           0 \\ 0 \\ 1 
      %         \end{pmatrix}                               \label{eq_Appendix_cartesian_Spin_1_basis}
      %    \end{alignedat}
      %  \end{equation}

      %  \begin{align}
      %      \vec{a}
      %    = a_1 \vec{e}_{1} + a_2 \vec{e}_{2} + a_3 \vec{e}_{3}
      %    = \begin{pmatrix} a_1 \\ a_2 \\ a_3 \end{pmatrix}
      %  \end{align}

      %  \begin{align}
      %      \tilde{S}_{3}
      %    = \begin{pmatrix}
      %        \begin{array}{rrr}
      %          1  & \hspace{2.5mm}  0  &  0   \\
      %          0  & \hspace{2.5mm}  0  &  0   \\
      %          0  & \hspace{2.5mm}  0  & -1
      %        \end{array}
      %      \end{pmatrix}   \honemm .                      \label{eq_Appendix_Spin-1_spherical_matrix_3}
      %  \end{align}

      %  \begin{equation}
      %    \begin{alignedat}{2}
      %      u &= - \frac{1}{\sqrt{2}} (x - \mi y) \honemm ,  \\
      %      v &=   \frac{1}{\sqrt{2}} (x + \mi y) \honemm ,  \\
      %      w &= z                                \honemm .  \\
      %    \end{alignedat}
      %  \end{equation}

      %  \begin{equation}
      %    \begin{alignedat}{2}
      %      x &=   \frac{1}{\sqrt{2}} ( v - u )     \honemm ,          \\
      %      y &= - \mi \frac{1}{\sqrt{2}} ( u + v ) \honemm ,          \\
      %      z &= w
      %    \end{alignedat}
      %  \end{equation}

      %  \begin{equation}
      %    \begin{alignedat}{2}
      %         \vec{\tilde{e}}_{1}
      %      &\propto \frac{\partial x}{\partial_u} \vec{e}_{1}
      %         + \frac{\partial y}{\partial_u} \vec{e}_{2}
      %         + \frac{\partial z}{\partial_u} \vec{e}_{3}
      %       = - \frac{1}{\sqrt{2}} \vec{e}_{1}
      %         - \mi \frac{1}{\sqrt{2}} \vec{e}_{2}                  \\
      %         \vec{\tilde{e}}_{2}
      %      &\propto \frac{\partial x}{\partial_v} \vec{e}_{1}
      %         + \frac{\partial y}{\partial_v} \vec{e}_{2}
      %         + \frac{\partial z}{\partial_v} \vec{e}_{3}
      %       =   \frac{1}{\sqrt{2}} \vec{e}_{1}
      %         - \mi \frac{1}{\sqrt{2}} \vec{e}_{2}                  \\
      %         \vec{\tilde{e}}_{3}
      %      &\propto \frac{\partial x}{\partial_w} \vec{e}_{1}
      %         + \frac{\partial y}{\partial_w} \vec{e}_{2}
      %         + \frac{\partial z}{\partial_w} \vec{e}_{3}
      %       = \vec{e}_{3}                                           \\
      %    \end{alignedat}
      %  \end{equation}

      %  \begin{equation}
      %    \begin{alignedat}{2}
      %         \vec{\tilde{e}}_1
      %      &= \begin{pmatrix}
      %           - \frac{1}{\sqrt{2}}     \\[2mm]
      %           - \mi \frac{1}{\sqrt{2}} \\[2mm]
      %           0
      %         \end{pmatrix}   \honemm ,                             \hspace{5mm}
      %         \vec{\tilde{e}}_2
      %      &= \begin{pmatrix}
      %           \frac{1}{\sqrt{2}}       \\[2mm]
      %           - \mi \frac{1}{\sqrt{2}} \\[2mm]
      %           0
      %         \end{pmatrix} \honemm ,                               \hspace{5mm}
      %         \vec{\tilde{e}}_3
      %      &= \begin{pmatrix}
      %           0 \\[2mm]
      %           0 \\[2mm]
      %           1 
      %         \end{pmatrix}                               \label{eq_Appendix_spherical_Spin_1_basis}
      %    \end{alignedat}
      %  \end{equation}

      %  \begin{align}
      %      \vec{\tilde{a}}
      %    = a_1 \vec{e}_{1} + a_2 \vec{e}_{2} + a_3 \vec{e}_{3}
      %    = \begin{pmatrix} a_1 \\ a_2 \\ a_3 \end{pmatrix}
      %  \end{align}



 
      %  \textcolor{red}{Divergence operation with Spin-1 matrices:}

      %  \begin{equation}
      %    \begin{alignedat}{2}
      %        \vec{\nabla} \left( \vec{\nabla} \cdot \vec{F}_{\pm}(\vrt) \right)
      %      = \begin{pmatrix}
      %           \frac{\partial^2}{\partial x^2}          \vec{F}_{\pm}(\vrt)
      %         + \frac{\partial^2}{\partial x \partial y} \vec{F}_{\pm}(\vrt)
      %         + \frac{\partial^2}{\partial x \partial z} \vec{F}_{\pm}(\vrt)           \\[2mm]
      %           \frac{\partial^2}{\partial y \partial x} \vec{F}_{\pm}(\vrt)
      %         + \frac{\partial^2}{\partial y^2}          \vec{F}_{\pm}(\vrt)
      %         + \frac{\partial^2}{\partial y \partial z} \vec{F}_{\pm}(\vrt)           \\[2mm]
      %           \frac{\partial^2}{\partial z \partial x} \vec{F}_{\pm}(\vrt)
      %         + \frac{\partial^2}{\partial z \partial y} \vec{F}_{\pm}(\vrt)
      %         + \frac{\partial^2}{\partial z^2}          \vec{F}_{\pm}(\vrt)           \\
      %        \end{pmatrix}
      %    \end{alignedat}
      %  \end{equation}

      %  Divergence condition operator:

      %  \begin{align}
      %      \hat{D}_{\mMx}
      %    = \begin{bmatrix}
      %        \frac{\partial}{\partial x} & \frac{\partial}{\partial y} & \frac{\partial}{\partial z}        \\[2mm]
      %        \frac{\partial}{\partial x} & \frac{\partial}{\partial y} & \frac{\partial}{\partial z}        \\[2mm]
      %        \frac{\partial}{\partial x} & \frac{\partial}{\partial y} & \frac{\partial}{\partial z}
      %      \end{bmatrix}
      %  \end{align}

      %  \begin{align}
      %      \hat{D}_{\mMx} \vec{F}_{\pm}(\vrt)
      %    = \sqrt{\frac{1}{2 \epsilon_0}} \rho(\vrt)
      %  \end{align}

      %  \begin{align}
      %      \hat{C}_{\mMx}
      %    = c_{0}^2
      %      \begin{bmatrix}
      %        \hat{p}_{x}^2           & \hat{p}_{x} \hat{p}_{y} & \hat{p}_{x} \hat{p}_{z}          \\[2mm]
      %        \hat{p}_{y} \hat{p}_{x} & \hat{p}_{y}^2           & \hat{p}_{y} \hat{p}_{z}          \\[2mm]
      %        \hat{p}_{z} \hat{p}_{x} & \hat{p}_{z} \hat{p}_{y} & \hat{p}_{z}^2
      %      \end{bmatrix}
      %    = - c_{0}^2 \hbar^2
      %      \begin{bmatrix}
      %        \frac{\partial^2}{\partial x^2}          & \frac{\partial^2}{\partial y \partial x} & \frac{\partial^2}{\partial x \partial z}         \\[2mm]
      %        \frac{\partial^2}{\partial y \partial x} & \frac{\partial^2}{\partial y^2}          & \frac{\partial^2}{\partial y \partial z}         \\[2mm]
      %        \frac{\partial^2}{\partial z \partial x} & \frac{\partial^2}{\partial z \partial y} & \frac{\partial^2}{\partial z^2}
      %      \end{bmatrix}
      %  \end{align}

      %  Commutator of D and H:

      %  \begin{align}
      %       \hat{H}_{\mMx}
      %     = - \mi c_{0} \hbar \left[ \vec{\nabla} \cdot \vec{\boldsymbol{S}} \right]
      %     = c_{0} \hbar
      %       \begin{bmatrix}
      %         \begin{array}{rrr}
      %                                       0 & - \frac{\partial}{\partial z} &   \frac{\partial}{\partial y} \hspace{1mm}  \\[2mm]  
      %             \frac{\partial}{\partial z} &                             0 & - \frac{\partial}{\partial_x} \hspace{1mm}  \\[2mm]
      %           - \frac{\partial}{\partial y} &   \frac{\partial}{\partial x} &                             0 \hspace{1mm}    
      %         \end{array}                                                                                           
      %       \end{bmatrix}   \honemm .       \label{eq_Appendix_Maxwell_hamiltonian_vacuum}                                 
      %  \end{align}   

      %  \begin{align}
      %    \left[ \hat{H}_{\mMx} \hat{}_{\mMx} - \hat{D}_{\mMx} \right]
      %  \end{align}

      %  \begin{align}
      %    \hat{H}_{\mMx} \hat{C}_{\mMx} \vec{F}_{\pm}(\vec{r},t_0) = 0
      %  \end{align}

      %  \noindent
      %  because of Divergence condition.

      %  \begin{align}
      %    \hat{C}_{\mMx} \hat{H}_{\mMx} \vec{F}_{\pm}(\vec{r},t_0) = 0
      %  \end{align}

        


    \section{Riemann-Silberstein Maxwell's equations in a linear medium}   \label{appendix_chapter_01.2}

      Starting with the two Gau\ss{} laws in a medium in (\ref{eq_Maxwells_equation_medium_Gauss_D}) and
      (\ref{eq_Maxwells_equation_medium_Gauss_B}) as a complex addition of both equations 

      \begin{align}
          \vec{\nabla} \cdot \vec{D}(\vrt) \pm \mi \vec{\nabla} \cdot \vec{B}(\vrt)
        = \rho_{\mathrm{free}}(\vrt)   \honemm ,                                                   \label{eq_Appendix_RS_Gauss_medium_transform_1}
      \end{align}

      \noindent
      and applying similar equivalence transformations like for the vacuum equations
      (\ref{eq_RS_Faraday_Ampere_vacuum_transform_1}) - (\ref{eq_RS_Faraday_Ampere_vacuum_transform_3})
      leads to (\textcolor{red}{Appendix})

      \begin{align}
        & \vec{\nabla} \cdot
          \left( \sqrt{\frac{\epsilon(\vrt)}{2}} \vec{E}(\vrt) \pm \mi \sqrt{\frac{1}{2 \mu(\vrt)}} \vec{B}(\vrt) \right)        \\
        & \quad + \frac{\big( \vec{\nabla} \epsilon(\vrt) \big)}{2 \epsilon(\vrt)}
          \cdot \sqrt{\frac{\epsilon(\vrt)}{2}} \vec{E}(\vrt)
          \pm \mi \frac{\big( \vec{\nabla} \mu(\vrt) \big)}{2 \mu(\vrt)}
          \cdot \sqrt{\frac{1}{2 \mu(\vrt)}} \vec{B}(\vrt)
          = \frac{1}{\sqrt{2 \epsilon(\vrt)}} \rho_{\mathrm{free}}(\vrt)   \honemm .               \label{eq_Appendix_RS_Gauss_medium_transform_2}
      \end{align}

      \noindent
      The expression in the right-handed brackets is equivalent to $ \vec{F}_{\pm,\mathrm{lm}}(\vrt) $ in
      (\ref{eq_RS_vector_medium_plus}) or (\ref{eq_RS_vector_medium_minus}). The remaining two terms, one
      with the electric field and the other one with the magnetic field expression can only be replaced by
      a linear combination of $ \vec{F}_{+,\mathrm{lm}}(\vrt) $ and $ \vec{F}_{-,\mathrm{lm}}(\vrt) $ in
      (\ref{eq_RS_E_back_transformation_medium}) and (\ref{eq_RS_B_back_transformation_medium}). Hence,
      the Riemann-Silberstein Maxwell Gau\ss{} law is given by

      \begin{align}
           \vec{\nabla} \cdot \vec{F}_{\pm,\mathrm{lm}}(\vrt)
        &= \frac{1}{\sqrt{2 \epsilon(\vrt)}} \rho_{\mathrm{free}}(\vrt)
            - 
           \left( 
             \frac{\big( \vec{\nabla} \epsilon(\vrt) \big)}{4 \epsilon(\vrt)}
             \pm \frac{\big( \vec{\nabla} \mu(\vrt) \big)}{4 \mu(\vrt)}
           \right)
           \cdot \vec{F}_{+,\mathrm{lm}}(\vrt)                                                     \\
        &  \quad -
           \left(
             \frac{\big( \vec{\nabla} \epsilon(\vrt) \big)}{4 \epsilon(\vrt)}
             \pm \frac{\big( \vec{\nabla} \mu(\vrt) \big)}{4 \mu(\vrt)}
           \right)
           \cdot \vec{F}_{-,\mathrm{lm}}(\vrt)   \honemm .                                         \label{Appendix_eq_RS_Gauss_medium_pm}
      \end{align}


      Now, we consider the remaining transformation of Faraday's and Amp\`ere's law in a linear medium into the
      corresponding Riemann-Silberstein equation. Faraday's and Amp\`ere's law mainly determine the
      time-propagation of the electromagnetic field. In more general form, the expressions in equations
      (\ref{eq_Maxwells_equation_medium_Faraday}) and (\ref{eq_Maxwells_equation_medium_Ampere}) contain
      an additional term to describe damping of the electromagnetic field, e.g. in a lossy medium region.
      In the most common case, the damping term is described proportional to the current electric field.
      The underlying constant of proportionality is the electric conductivity
      $ \sigma_{\me}(\vrt) $. For later useful updated Maxwell's equations for boundary regions, we introduce
      here a non physical magnetic conductivity $ \sigma_{\mm}(\vrt) $ like it is also described in A. Tavlofes book
      \cite{taflove2005computational}.
      Therefore, Faraday's and Amp\`ere's law with lossy electric and magnetic layer is given by

      \begin{align}
           \vec{\nabla} \times \vec{E}(\vrt)
        &= - \Big( \frac{\partial}{\partial t}
           + \sigma_{\mm}(\vrt) \Big) \vec{B}(\vrt)   \honemm ,                \label{eq_Appendix_Maxwells_equation_medium_Faraday_lossy_layer}     \\
           \vec{\nabla} \times \vec{H}(\vrt)
        &= \Big (\frac{\partial}{\partial t}
           + \sigma_{\me}(\vrt) \Big) \vec{D}(\vrt)
           + \vec{j}_{\mathrm{free}}(\vrt)   \honemm .                         \label{eq_Appendix_Maxwells_equation_medium_Ampere_lossy_layer}
      \end{align}

      \noindent
      Separation of just the temporal derivative terms on the right-hand side and adding equation
      (\ref{eq_Maxwells_equation_medium_Faraday_lossy_layer}) as imaginary part to equation
      (\ref{eq_Maxwells_equation_medium_Ampere_lossy_layer}) and multiplying the whole equation
      by the imaginary unit leads to

      \begin{equation}
        \begin{alignedat}{2}
          &    \mi \frac{\partial}{\partial t} \Big( \epsilon(\vrt) \vec{E}(\vrt)
               \pm \mi \vec{B}(\vrt) \Big) 
            && = \pm \vec{\nabla} \times \vec{E}(\vrt)
               + \mi \vec{\nabla} \times \frac{1}{\mu(\vrt)} \vec{B}(\vrt)                                   \\
          & && \quad \pm \sigma_{\mm}(\vrt) \vec{B}(\vrt)
               - \mi \sigma_{\me}(\vrt) \vec{D}(\vrt) - \mi \vec{j}_{\mathrm{free}}(\vrt)   \honemm .        \label{eq_Appendix_RS_Faraday_Ampere_medium_transform_1}
        \end{alignedat}
      \end{equation}

      \noindent
      Further equivalent transformations including the substitution
      $ c(\vrt) = 1/\sqrt{\epsilon(\vrt) \mu(\vrt)} $ and taking into account that the electric permittivity and magnetic
      permeabilty depend on time, results in the form (\textcolor{red}{Appendix})

      \begin{equation}
        \begin{alignedat}{2}
          &    \mi \frac{\partial}{\partial t} \left( \sqrt{\frac{\epsilon(\vrt)}{2}} \vec{E}(\vrt)
               \pm \mi \sqrt{\frac{1}{2 \mu(\vrt)}} \vec{B}(\vrt) \right) 
            && = \pm c(\vrt) \vec{\nabla} \times 
               \left(
                 \sqrt{\frac{\epsilon(\vrt)}{2}} \vec{E}(\vrt)
                 + \mi \sqrt{\frac{1}{2 \mu(\vrt)}} \vec{B}(\vrt)
               \right)                                                                                                           \\
          & && \quad \mp c(\vrt) \frac{(\vec{\nabla} \epsilon(\vrt))}{2 \epsilon(\vrt)}
               \times \sqrt{\frac{\epsilon(\vrt)}{2}} \vec{E}(\vrt)                                                              \\
          & && \quad - \mi c(\vrt) \frac{(\vec{\nabla} \mu(\vrt))}{2 \mu(\vrt)}
               \times \sqrt{\frac{1}{2 \mu(\vrt)}} \vec{B}(\vrt)                                                                 \\
          & && \quad \pm \sigma_{\mm}(\vrt) \sqrt{\frac{1}{2 \mu(\vrt)}} \vec{B}(\vrt)
               - \mi \sigma_{\me}(\vrt) \sqrt{\frac{\epsilon(\vrt)}{2}} \vec{E}(\vrt)                                            \\
          & && \quad - c(\vrt) \frac{\dot{\epsilon}(\vrt)}{2 \epsilon(\vrt)} \sqrt{\frac{\epsilon(\vrt)}{2}} \vec{E}(\vrt)       \\
          & && \quad \pm \mi c(\vrt) \frac{\dot{\mu}(\vrt)}{2 \mu(\vrt)} \sqrt{\frac{1}{2 \mu(\vrt)}} \vec{B}(\vrt)              \\
          & && \quad - \mi \frac{1}{\sqrt{2 \epsilon(\vrt)}} \vec{j}_{\mathrm{free}}(\vrt)   \honemm .                           
               \label{eq_Appendix_RS_Faraday_Ampere_medium_transform_2}
        \end{alignedat}
      \end{equation}

