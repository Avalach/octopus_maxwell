
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \chapter*{Introduction}   \label{chap_introduction}
    \addcontentsline{toc}{chapter}{Introduction}

    The complexity of light-matter interactions for realistic systems is very
    challenging for a full ab initio theoretical description. In this work,
    we present a first feasible methodology to simulate fully coupled systems.
    First, we summarize in the following the state of the art how complex
    light-matter systems are usually considered. 
    One common way to describe the
    interaction of electromagnetic fields with matter relies on the reduction
    of the considered degrees of freedom. The choices for what is considered
    relevant depends on the aspects of interest.  Historically as a consequence
    of this, the description of light-matter interactions has developed into
    different subfields. In case of quantum physics, one large area of research
    considers low-energy quantum physics which is again divided into different
    topics, i.e., quantum chemistry, quantum optics and solid-state physics.
    Each of these research areas is focussing on different aspects of low-energy
    physics and is based on the assumption that the effects that are in the
    focus of the respective other fields are rather small. The two main
    different aspects in this case can be summarized as follows. In quantum
    chemistry and solid state physics, the electromagnetic field is typically
    treated as as given environment that determines material science and a focus is
    on a detailed description of matter. On the other hand in quantum optics,
    certain matter properties are prescribed and a focus is placed on the
    electromagnetic fields.  In this context, it becomes clear, that the
    theoretical methodology of considering the two topics depends on the
    corresponding subfield. Hence, some literature describes the matter degrees
    of freedom more detailed
    \cite{szabo2012,martin2016,Schollwoeck2011,engel2011}, or in turn the
    electromagnetic fields \cite{loudon1988,grynberg2010,born2013}. \\

    While both research directions have reached significant progress in the past decades,
    mostly relying on the assumption of neglecting one main part, recent
    experiments go beyond this conventional picture into the regime, where
    both, light and matter, reveal a strong mutual correlation. Such strongly
    coupled systems, e.g., polaritons as light-matter hybrid states, can be
    observed when molecules are placed into  nanocavities
    \cite{chikkaraddy2016}, microcavities \cite{wang2017coherent} or other
    large nanostructures \cite{sukharev2017}. The large number of atoms inside
    nanostructures or molecules causes in most cases strong coupling effects
    \cite{ebbesen2016}, which has also an effect on a larger scale for chemical
    properties. As a consequence, strong electromagnetic fields can modify
    chemical reactions \cite{hutchison2012}, changes in commonly used selection
    rules can be observed \cite{yamamoto2014}, or energy transfer can be altered
    \cite{zhong2017}.  Similar to chemical reactions, the optical behavior
    changes significantly in strongly coupled systems. New features in
    spectroscopy have been found, e.g., the enhancement of Raman processes
    \cite{shalabney2015b}, creation of polariton condensates \cite{byrnes2014}
    or retardation effects like energy transfer induced by attosecond laser
    pulses \cite{sommer2016attosecond}. Indeed, research in strongly coupled
    systems has revealed neglected properties and new materials. We emphasize
    along these lines, e.g., detailed optical responses
    \cite{maki1991linear,taminiau2012quantifying}, or a new color (Vantablack)
    arising for long nanotubes, which absorb almost any visual light
    \cite{maki1991linear}. Additionally, photons carrying angular momentum
    \cite{schmiegelow2016transfer,birula2018,Yue2018} have been proposed for
    large volume and long distance information transport
    \cite{nature_angular_light}, and even some processes in living bacteria
    show strong coupling effects \cite{coles2014}. \\

    The common underlying basis of all previously listed examples is the triad
    of particle species of electrons, nuclei, and photons.  The ab-initio description of
    all three kinds of particle species together in one picture is in most
    cases considered in a reduced form to consider only the important ones
    \cite{ruggenthaler2018quantum}, since all their corresponding degrees of
    freedom lead to a non-feasible problem. One way to reduce the degrees of
    freedom is based on the limit of a one- or two-dimensional spatial
    description, while keeping the full particle interactions. Here, the full
    rotational symmetry is missing, and all its corresponding effects.  
    Model systems
    are another option to simplify the considered degrees of freedom, which
    still try to catch most relevant features of the real system, but they neglect
    also per construction some effects. Consequently, the choice of a model system is
    pre-decision which effects can be observed. In a third
    way, one particle species and the corresponding degrees of freedom are only
    considered as an external perturbation of the system. Hence, the
    back-reaction on this external particle species is neglected, which always breaks
    in the conservation of energy. The driven matter system gains too much energy
    when the get excited since they cannot screen the external electromagnetic field
    due to their induced current. However, in electronic
    structure theory, for example, photons and nuclei are typically only
    external variables, which leads to solvable systems \cite{szabo2012} with
    different kinds of methods
    \cite{booth2009,Orus2014,schollwoeck2005,kotliar2006,gull2011,Metzner2012,RevModPhys.86.779}.
    Due to the simplification of the full problem, many effects cannot be
    observed, which are caused by the neglected back-reaction. As a
    consequence, some techniques add more degrees of freedom, e.g., the
    electron-nucleus interaction via exact-factorization
    \cite{abedi2010exact,abedi2012correlated} or trajectory-based solutions
    \cite{kapral2015, tully2012, miller2012}, but taking all three interactions
    into account including the photon field, is up to now only considered in a
    very limited set of approaches. Only very recently some theoretical
    developments have emerged that attempt to treat matter and electromagnetic
    radiation on an equal ab-initio footing.  Examples include cases where
    light-matter coupling is treated classically
    \cite{lorin2007,lopata2009,PhysRevB.85.045134,Lucchini916,PhysRevE.94.023314,2018arXiv180702733Y,
    2018arXiv181006168Y, 2018arXiv181006500U, 2018arXiv181008344Y} or as a
    quantized field
    \cite{ruggenthaler2011time,tokatly2013,ruggenthaler2014quantum,ruggenthaler2015,galego2015cavity,flick2018strong,feist2017,ribeiro2018polariton}.
    Up to now only a few works describe all three particle types on the same
    level of description
    \cite{melo2015,flick2017cavity,flick2018cavity,schafer2018ab}. This leads
    to interesting light-matter behavior, e.g. due to modified Maxwell's
    equations in vacuum \cite{flick2018light}, as well as polariton states with
    new potential-energy surfaces \cite{flick2017atoms} or detailed chemical
    structures \cite{flick2018ab}. These researches followed the bottom-up
    direction to describe the fully coupled picture in terms of quantum electrodynamics.
    They provide applications to investigate the full particle interactions, but
    their applications are limited to few particle systems.
    This is where the present work is framed and
    follow the opposite top-down direction of handling with a large number of
    particles with mass. Since it is not possible to consider the full quantum
    nature of such large systems especially the degrees of freedom of the photons,
    we introduce a semi-classical method of coupled electromagnetic mean-field and
    quantum-mechanical matter. This first approximation should give us an impression
    how strong the electromagnetic back-reaction change the results compared to
    conventional simulations. Additionally such a mean-field simulation provides
    a basis which can be expand and modified by new developed QED approaches to catch more
    quantum effects. 
    %Another very 
    %Additionally, the impact of the matter on
    %the Maxwell fields can be detected directly and not only indirectly by
    %matter. \\

    The previously discussed methods to simplify the complete problem, like
    neglecting nuclear motion or using the dipole approximation of the
    Maxwell-matter coupling or are not applicable to larger realistic
    (three-dimensional, non-model) systems. The present work aims at describing
    electrons, nuclei and photons on an equal ab-initio level. The starting
    point for our approach is a generalized Pauli-Fierz Hamiltonian for
    non-relativistic quantum electrodynamics (QED) \cite{spohn2004,
    ruggenthaler2018quantum}.  Using density functional theory, this
    formulation in a multi-species and multi-scale ansatz leads to coupled
    Maxwell-Pauli-Kohn-Sham equations (MPKS) \cite{MPKS_paper}.  In a first numerical application
    for a nanoplasmonic dimer, we demonstrate the difference of considering
    only the forward coupling of electromagnetic fields to matter and compare
    to self-consistent forward and backward coupling between light and matter.
    Taking this feature and varying different options, e.g., the degrees of
    freedom of ion motion or the multipole expansion terms for Maxwell to
    matter coupling, and different density functionals, gives a first overview
    of new effects and perspectives for self-consistent light-matter coupling.
    We show that common stating that missing correlations among one type of
    particles (e.g. electrons) is the cause for a disagreement between theory
    and experiment can be misleading. Instead, the cause for a discrepancy might
    also be the conventionally omitted self-consistent light-matter coupling. 
    Our first implementation based on the
    newly introduced theoretical technique is a comprehensive tool to
    investigate experimental results, or design and control novel materials.
    Additionally we emphasize here, that simulating the physically electromagnetic
    field including the back-reaction of the matter superposed with the external
    field, has the advance to be directly detected. For instance, we can analyze
    the outgoing electromagnetic field at the simulation box boundaries.
    In contrast, conventional
    simulations obtain the corresponding spectroscopy indirectly by 
    investigating only the matter reaction. Therefore, our implementation can
    measure the error between such usual "indirect" spectroscopy and the 
    "real" emitted electromagnetic field. \\

    The present PhD focuses on the combination of propagating the quantum
    mechanical matter, the corresponding internal and external electromagnetic fields, and
    their mutual coupling. Hence, we provide a mathematical and physically
    consistent framework to deal with in interactions of classical light with matter at 
    arbitrary strength, length and time scales. 
    We have organized the thesis as follows.
%    Whereas in common works of
%    quantum mechanical systems and its propagation start by considering the
%    unperturbed quantum system and adding the electromagnetic field coupling
%    later as only a perturbing variable, we start to focus on the classical
%    electromagnetic fields but rewriting them into a quantum-mechanical-like
%    formalism. \\ 
    Using the Riemann-Silberstein vector of classical electrodynamics, we show
    in chapter~\ref{chap_Mx_equation_in_RS} how to rewrite the microscopic
    Maxwell's equations in Schr\"odinger like form.  Additionally in a similar
    manner, we obtain the macroscopic Maxwell's equations in linear media which
    requires a linear combination of the two electromagnetic helicity states of
    the electromagnetic field, i.e., Riemann-Silberstein vectors, which refer
    to different spin states.  Following the usual construction of
    quantum-mechanical time-evolution operators, we show in
    chapter~\ref{chap_RS_Mx_time_evolution}  how to construct the
    Riemann-Silberstein time-evolution operators for homogeneous and
    inhomogeneous propagations in vacuum or linear media. In this
    representation, the implemented code provides a method for simulating the
    propagation of electromagnetic fields which is an alternative to the
    commonly used finite difference time domain method (FDTD) \cite{Yee_1966,taflove2005computational}.
    Consequently,
    in chapter~\ref{chap_RS_Mx_propagation_on_grid} we describe the practical
    details of the Maxwell implementation on a three-dimensional grid including
    useful boundary conditions as absorbing boundaries, a perfectly matched
    layer formulation for the Riemann-Silberstein case, incident plane waves
    and a combination of both.  For each of these features, we demonstrate some
    applications to demonstrate the stand-alone electromagnetic field
    propagation simulation. \\ After introducing the novel Riemann-Silberstein
    electromagnetic field simulation, we summarize and introduce in
    chapter~\ref{chap_QED_for_many_body_systems} the fundamentals of quantum
    electrodynamics to obtain the generalized many-body Pauli-Fierz
    Hamiltonian, which forms the basis to couple quantized matter variables
    like current and charge densities to the classical electromagnetic fields.
    Furthermore, we employ this Pauli-Fierz Hamiltonian to develop a
    density-functional theory (DFT) \cite{Gross90,Marques2006} for non-relativistic
    QED for photons,
    electrons and effective nuclei on the level of a generalized
    quantum-electrodynamical density-functional theory (QEDFT) \cite{spohn2004,
    ruggenthaler2014quantum,ruggenthaler2018quantum}. 
    Taking the
    mean-field approximation of the electromagnetic field and nuclei, we arrive
    at coupled Ehrenfest-Maxwell-Pauli-Kohn-Sham (EMPKS) equations that build
    the basis for our coupled light-matter implementation, which is introduced
    in detail in chapter~\ref{chap_Maxwell_Pauli_Kohn_Sham_propagation}. This
    chapter also includes a discussion for using multi-scale grids and time
    steps to properly handle the different features of the systems, as well as
    full minimal-coupling and the multipole expansion, and a
    predictor-corrector scheme for self-consistent forward-backward coupling of
    light and matter.  In chapter~\ref{chap_applications}, the significance of
    the entire forward-backward coupling is taken into account and compared to
    the commonly used forward-coupling in a first Maxwell-Kohn-Sham application
    for a nanoplasmonic system excited by a laser. Applying different
    simulation options, we investigate the near-field effects of the
    electromagnetic fields, especially their field enhancements. Furthermore,
    we decompose the total field into a longitudinal and transverse fields, and
    investigate the interference of both transverse internal and external
    fields with corresponding frequency shift. The advantage of propagating the
    total electromagnetic fields on a numerical grid allows us to define
    corresponding electromagnetic detectors to analyze the outgoing
    electromagnetic radiation in the far-field. This provides a novel
    simulation tool to record commonly employed spectroscopies that are used in
    experiments. In the \hyperref[chap_summary]{Summary} and the
    \hyperref[chap_outlook]{Outlook}, we recapitulate the complex work that was 
    required to achieve our novel introduced methodology of considering electromagnetic
    field and matter fully coupled. 
    Further, we emphasize the main aspects and results of our first applications 
    which demonstrate that we provide a proper ab initio implementation which
    opens a new research area in material science and optics. Stepping forward in
    a field that was thought of being non-reachable means that a lot of new arising problems
    can be tackled in the near future. Hence, the insights of this thesis can be seen
    as a basic ingredient for future theoretical developments, for instance, finding 
    methods to include QEDFT effects \cite{flick2018strong,ribeiro2018polariton,flick2018cavity,schafer2018ab,ruggenthaler2018quantum,tokatly2013}.
    





  
