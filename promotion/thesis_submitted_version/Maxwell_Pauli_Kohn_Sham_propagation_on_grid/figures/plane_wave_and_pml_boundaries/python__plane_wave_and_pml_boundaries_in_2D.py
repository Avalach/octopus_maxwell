import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

# ax1.add_patch(patches.Rectangle((x,y),width,height)

# plane wave boundaries
ax.add_patch(patches.Rectangle((-10,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle((  8,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle(( -8,  8), 16,  2, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle(( -8,-10), 16,  2, ec='none', alpha=0.7, color=(0,0.392,0)))

# pml boundaries
ax.add_patch(patches.Rectangle(( -5, -8), 10,  3, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle(( -5,  5), 10,  3, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle(( -8, -5),  3, 10, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle((  5, -5),  3, 10, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle(( -8, -8),  3,  3, ec='none', alpha=0.5, color=(0.300,0.300,0)))
ax.add_patch(patches.Rectangle((  5, -8),  3,  3, ec='none', alpha=0.5, color=(0.300,0.300,0)))
ax.add_patch(patches.Rectangle(( -8,  5),  3,  3, ec='none', alpha=0.5, color=(0.300,0.300,0)))
ax.add_patch(patches.Rectangle((  5,  5),  3,  3, ec='none', alpha=0.5, color=(0.300,0.300,0)))


r1 = [-10,-10]
r2 = [ 10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [ 10, 10]
r2 = [-10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [-10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [ 10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [ -8,  8]
r2 = [  8,  8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)

r1 = [ -8,  8]
r2 = [  5,  5]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)

r1 = [ -8,  8]
r2 = [ -5, -5]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)

r1 = [ -8,  8]
r2 = [ -8, -8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)

r1 = [ -8, -8]
r2 = [ -8,  8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)

r1 = [ -5, -5]
r2 = [ -8,  8]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)

r1 = [  5,  5]
r2 = [ -8,  8]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)

r1 = [  8,  8]
r2 = [ -8,  8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)


# Box limit line x direction
r1 = [-10, 10]
r2 = [-11,-11]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)
r1 = [-10.0,-10.0]
r2 = [-11.3,-10.7]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)
r1 = [ 10.0, 10.0]
r2 = [-11.3,-10.7]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)

# Box limit line y direction
r1 = [ 11, 11]
r2 = [-10, 10]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)
r1 = [ 10.7, 11.3]
r2 = [-10.0,-10.0]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)
r1 = [ 10.7, 11.3]
r2 = [ 10.0, 10.0]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)

# inner box limit
r1 = [ -4.9,  5.0]
r2 = [ -4.0, -4.0]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)
r1 = [ -4.9, -4.9]
r2 = [ -3.7, -4.3]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)
r1 = [  5.0,  5.0]
r2 = [ -3.7, -4.3]
line = lines.Line2D(r1, r2, color='black', lw=1.5)
ax.add_line(line)

ax.text(0, -3.5, 'inner box limit',
        horizontalalignment='center',
        verticalalignment='center',
        fontsize=11, color='black')
ax.text(0,-12, 'Simulation box limit in x-direction)',
        horizontalalignment='center',
        verticalalignment='center',
        fontsize=11, color='black')
ax.text(12, 0, 'Simulation box limit in y-direction',
        horizontalalignment='center',
        verticalalignment='center',
        fontsize=11, color='black',
        rotation='vertical')

#point = plt.scatter(0, 0, marker='o', s=200, color="firebrick")
#point = fig.circle(0, 0, legend="matter system")

#point, = plt.plot(0, 0, marker='o', markersize=20, color=(0.588,0.294,0))

matter_circle = plt.Circle((0, 0), 1.5, color=(0.588,0.294,0))
ax.add_artist(matter_circle)

#print point

plane_wave_region = mpatches.Patch(color=(0,0.392,0), alpha=0.7, label='Incident waves region')
pml_single_region    = mpatches.Patch(color=(0.588,0.588,0), alpha=0.6, label='PML single region')
pml_overlap_region   = mpatches.Patch(color=(0.300,0.300,0), alpha=0.6, label='PML overlap region')
matter_system        = mpatches.Patch(color=(0.588,0.294,0), label='matter_system')


first_legend = plt.legend(handles=[matter_system], prop={'size': 10}, loc=(0.3,-0.1))
plt.gca().add_artist(first_legend)
#ax.legend()

#plt.legend.numpoints = 1
#plt.legend(numpoints=1)
#plt.legend([point],['matter system'])
plt.legend(handles=[plane_wave_region, pml_single_region, pml_overlap_region], prop={'size': 10}, loc=(0.235,0.95))

#for ii in range(-10, 10, 5):
   #r1 = [  ii, ii]
   #r2 = [ -10, 10]
   #line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   #ax.add_line(line)
   #r1 = [ -10, 10]
   #r2 = [  ii, ii]
   #line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   #ax.add_line(line)

for ii in range(-10, 10, 1):
   r1 = [  ii, ii]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)
   r1 = [ -10, 10]
   r2 = [  ii, ii]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)

#major_ticks = np.arange(-10, 10, 5)
#minor_ticks = np.arange(-10, 10, 1)

#ax.set_xticks(major_ticks)                                                       
#ax.set_xticks(minor_ticks, minor=True)                                           
#ax.set_yticks(major_ticks)                                                       
#ax.set_yticks(minor_ticks, minor=True)

#ax.grid(which='minor', alpha=0.5)                                                
#ax.grid(which='major', alpha=1.0)

ax.set_xlim(-10,10)
ax.set_ylim(-12,12)

ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

plt.axis('off')

DPI = fig.get_dpi()
fig.set_size_inches(6,6)

fig.savefig('plane_wave_and_pml_boundaries_in_2D.png')

plt.show()
