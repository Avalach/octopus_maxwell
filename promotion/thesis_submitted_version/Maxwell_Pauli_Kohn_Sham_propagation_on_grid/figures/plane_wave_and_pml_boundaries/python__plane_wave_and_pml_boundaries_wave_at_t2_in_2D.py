import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

# ax1.add_patch(patches.Rectangle((x,y),width,height)

# plane wave boundaries
ax.add_patch(patches.Rectangle((-10,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle((  8,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle(( -8,  8), 16,  2, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle(( -8,-10), 16,  2, ec='none', alpha=0.7, color=(0,0.392,0)))

# pml boundaries
ax.add_patch(patches.Rectangle(( -6, -8), 12,  2, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle(( -6,  6), 12,  2, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle(( -8, -8),  2, 16, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle((  6, -8),  2, 16, ec='none', alpha=0.5, color=(0.588,0.588,0)))

# detector boundaries
ax.add_patch(patches.Rectangle((  -6,  -6), 0.5, 12, ec='none', alpha=0.7, color=(1.000,1.000,0.000)))
ax.add_patch(patches.Rectangle(( 5.5,  -6), 0.5, 12, ec='none', alpha=0.7, color=(1.000,1.000,0.000)))
ax.add_patch(patches.Rectangle((-5.5, 5.5),  11,0.5, ec='none', alpha=0.7, color=(1.000,1.000,0.000)))
ax.add_patch(patches.Rectangle((-5.5,-6.0),  11,0.5, ec='none', alpha=0.7, color=(1.000,1.000,0.000)))


# Box lines
r1 = [-10,-10]
r2 = [ 10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)
r1 = [ 10, 10]
r2 = [-10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)
r1 = [-10, 10]
r2 = [-10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)
r1 = [-10, 10]
r2 = [ 10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

# PML lines
r1 = [ -8,  8]
r2 = [  8,  8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [ -6,  6]
r2 = [  6,  6]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [ -6,  6]
r2 = [ -6, -6]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [ -8,  8]
r2 = [ -8, -8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [ -8, -8]
r2 = [ -8,  8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [ -6, -6]
r2 = [ -6,  6]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [  6,  6]
r2 = [ -6,  6]
line = lines.Line2D(r1, r2, color='black', lw=0.5, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [  8,  8]
r2 = [ -8,  8]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)

# Detector
r1 = [ -5.5,  5.5]
r2 = [  5.5,  5.5]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [ -5.5,  5.5]
r2 = [ -5.5, -5.5]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [ -5.5, -5.5]
r2 = [ -5.5,  5.5]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)
r1 = [  5.5,  5.5]
r2 = [ -5.5,  5.5]
line = lines.Line2D(r1, r2, color='black', lw=1.0, linestyle='-', alpha=1.0)
ax.add_line(line)


shift=7

# inner box
x = np.linspace(-5.0, 5.0, 100)
y = np.linspace(-5.0, 5.0, 100)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, N=100, alpha=1.0)

# pml border left
x = np.linspace(-8.0,-5.0,  30)
y = np.linspace(-5.0, 5.0, 100)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# pml border right
x = np.linspace( 5.0, 8.0,  30)
y = np.linspace(-5.0, 5.0, 100)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# pml border top
x = np.linspace(-5.0, 5.0, 100)
y = np.linspace( 5.0, 8.0,  30)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(x-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# pml border bottom
x = np.linspace(-5.0, 5.0, 130)
y = np.linspace(-8.0,-5.0,  30)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(x-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# pml border left top
x = np.linspace(-8.0,-5.0,  30)
y = np.linspace( 5.0, 8.0,  30)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# pml border right top
x = np.linspace( 5.0, 8.0,  30)
y = np.linspace( 5.0, 8.0,  30)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# pml border left bottom
x = np.linspace(-8.0,-5.0,  30)
y = np.linspace(-8.0,-5.0,  30)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# pml border right bottom
x = np.linspace( 5.0, 8.0,  30)
y = np.linspace(-8.0,-5.0,  30)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# plane border left
x = np.linspace(-10.0,-8.0,  20)
y = np.linspace(-10.0,10.0, 200)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(x-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=0.6)

# plane border right
x = np.linspace(  8.0,10.0,  20)
y = np.linspace(-10.0,10.0, 200)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(x-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=0.6)

# plane top
x = np.linspace( -8.0, 8.0, 160)
y = np.linspace(  8.0,10.0,  20)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(x-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=0.6)

# plane bottom
x = np.linspace( -8.0, 8.0, 160)
y = np.linspace(-10.0,-8.0,  20)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(x-shift)**2/20) + 0.4*np.cos(np.sqrt(X**2+Y**2)*4)*np.exp(-np.sqrt(X**2+Y**2)**2/15)
levels = np.linspace(-1.4,1.4,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=0.6)

# outside box top
x = np.linspace(-30.0, 30.0, 600)
y = np.linspace( 10.0, 15.0,  50)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) 
levels = np.linspace(-5.0,5.0,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# outside box bottom
x = np.linspace(-30.0, 30.0, 600)
y = np.linspace(-15.0,-10.0,  50)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) 
levels = np.linspace(-5.0,5.0,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0) 

# outside box left
x = np.linspace(-30.0,-10.0, 200)
y = np.linspace(-10.0, 10.0, 200)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) 
levels = np.linspace(-5.0,5.0,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

# outside box right
x = np.linspace( 10.0, 30.0, 200)
y = np.linspace(-10.0, 10.0, 200)
X, Y = np.meshgrid(x, y)
Z = np.cos(2*(X-shift))*np.exp(-(X-shift)**2/20) 
levels = np.linspace(-5.0,5.0,1000)
cs = ax.contourf(X, Y, Z, levels, cmap=cm.seismic, alpha=1.0)

#cbar = fig.colorbar(cs)
#cbar.ax.set_yticklabels([''])

#point, = plt.plot(0, 0, marker='o', markersize=20, color=(0.588,0.294,0))

matter_circle = plt.Circle((0, 0), 1.5, color=(0.300,0.300,0.300))
ax.add_artist(matter_circle)

#plane_wave_region = mpatches.Patch(color=(0,0.392,0), alpha=0.7, label='Incident waves region')
#pml_single_region    = mpatches.Patch(color='yellowgreen', alpha=0.6, label='PML single region')
#pml_overlap_region   = mpatches.Patch(color='olive', alpha=0.6, label='PML overlap region')
#plt.legend(handles=[plane_wave_region, pml_single_region, pml_overlap_region], prop={'size': 10}, loc=1)

ax.text(-12.0, 12.0, 'c)', fontsize=25)

for ii in range(-10, 10, 1):
   r1 = [  ii, ii]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)
   r1 = [ -10, 10]
   r2 = [  ii, ii]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)

ax.set_xlim(-30,30)
ax.set_ylim(-15,15)

#ax.set_xlim(-15,15)
#ax.set_ylim(-15,15)

#major_ticks = np.arange(-15, 15, 5)
#minor_ticks = np.arange(-15, 15, 1)

#ax.set_xticks(major_ticks)                                                       
#ax.set_xticks(minor_ticks, minor=True)                                           
#ax.set_yticks(major_ticks)                                                       
#ax.set_yticks(minor_ticks, minor=True)

#ax.grid(which='minor', alpha=0.5)                                                
#ax.grid(which='major', alpha=1.0)

ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

plt.axis('off')

DPI = fig.get_dpi()
fig.set_size_inches(12,6)

fig.savefig('plane_wave_and_pml_boundaries_wave_at_t2_in_2D.png',bbox_inches='tight',pad_inches = 0)

plt.show()
