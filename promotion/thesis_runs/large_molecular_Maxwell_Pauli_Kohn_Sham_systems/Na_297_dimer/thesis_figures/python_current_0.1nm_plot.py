import numpy             as np
import matplotlib.pyplot as plt
from   matplotlib import cm


def read_and_edit_data(filename, dx, dy):
    unit_factor = 6.623618183*(10**(-3)) * (10**(3))  
    xx_lim = np.empty(2, dtype=int)
    yy_lim = np.empty(2, dtype=int)
    zz_lim = np.empty(2, dtype=int)
    xx = []
    yy = []
    zz = []
    with open(filename) as f:
        lines = f.read().splitlines()
        for line in lines:
            if (len(line)==69):
                split = line.split(" ")
                idx = 0
                for ii in range(0, len(split)):
                    if (len(split[ii]) != 0):
                        idx += 1
                        if (idx == 1):
                            xx.append(float(split[ii]))
                        elif (idx == 2):
                            yy.append(float(split[ii]))
                        elif (idx == 3):
                            zz.append(float(split[ii]))
    xx_lim[0] = min(xx)
    xx_lim[1] = max(xx)
    yy_lim[0] = min(yy)
    yy_lim[1] = max(yy)
    zz_lim[0] = min(zz) * unit_factor
    zz_lim[1] = max(zz) * unit_factor
    xx_idx_len = (len(range(int(min(xx)/dx), int(max(xx)/dx)))) + 1
    yy_idx_len = (len(range(int(min(yy)/dy), int(max(yy)/dy)))) + 1
    xx_idx_shift = -int(min(xx)/dx)
    yy_idx_shift = -int(min(yy)/dx)
    xxx = np.zeros(xx_idx_len*yy_idx_len, dtype=float)
    yyy = np.zeros(xx_idx_len*yy_idx_len, dtype=float)
    zzz = np.zeros(xx_idx_len*yy_idx_len, dtype=float)
    zzz = zzz
    idx_map = [[0 for ii in range(xx_idx_len)] for jj in range(yy_idx_len)]
    for ii in range(0, yy_idx_len):
        yval = yy_lim[0] + ii * dy
        for jj in range(0, xx_idx_len):
            xval = xx_lim[0] + jj * dx
            count = jj + ii * xx_idx_len
            xxx[count] = xval
            yyy[count] = yval
            idx_map[ii][jj] = count
    for ii in range(0, len(xx)):
        xx_idx = int(xx[ii]/dx)+xx_idx_shift
        yy_idx = int(yy[ii]/dy)+yy_idx_shift
        count = idx_map[yy_idx][xx_idx]
        zzz[count] = zz[ii] * unit_factor
    return xxx, yyy, zzz, xx_lim, yy_lim, zz_lim


def meshgrid_zz(xx_lim, yy_lim, dx, dy, zz):
    xx_mg, yy_mg = np.meshgrid(np.arange(xx_lim[0],xx_lim[1]+0.01,dx), np.arange(yy_lim[0],yy_lim[1]+0.01,dy))
    zz_mg = []
    for jj in range(0,len(xx_mg)):
        tmp = []
        for ii in range(0, len(xx_mg[0])):
            count = ii + jj*len(xx_mg[0])
            tmp.append(zz[count])
        zz_mg.append(tmp)
    return xx_mg, yy_mg, zz_mg


def geometry_coord(filename):
    xx = []
    yy = []
    zz = []
    with open(filename) as f:
        lines = f.read().splitlines()
        for line in lines:
            if (len(line) == 59):
                xx.append(float(line[25:35]))
                yy.append(float(line[37:47]))
                zz.append(float(line[49:59]))
    xxx = []
    zzz = []
    for ii in range(0, len(xx)):
        if (np.abs(yy[ii]) < 0.00001):
            xxx.append(xx[ii])
            zzz.append(zz[ii])
    return xxx, zzz


dx = float(1.0)
dy = float(1.0)

geo_x, geo_y = geometry_coord('../368_cobra/output_iter.new/td.0002700/geometry.xyz')

xx_1, yy_1, zz_1, xx_1_lim, yy_1_lim, zz_1_lim = read_and_edit_data('../368_cobra/output_iter.new/td.0002700/current-x.y=0', dx, dy)

xx_2, yy_2, zz_2, xx_2_lim, yy_2_lim, zz_2_lim = read_and_edit_data('../369_cobra/output_iter.new/td.0002700/current-x.y=0', dx, dy)

xx_3, yy_3, zz_3, xx_3_lim, yy_3_lim, zz_3_lim = read_and_edit_data('../368_cobra/output_iter.new/td.0002700/current-y.y=0', dx, dy)

xx_4, yy_4, zz_4, xx_4_lim, yy_4_lim, zz_4_lim = read_and_edit_data('../369_cobra/output_iter.new/td.0002700/current-y.y=0', dx, dy)

xx_5, yy_5, zz_5, xx_5_lim, yy_5_lim, zz_5_lim = read_and_edit_data('../368_cobra/output_iter.new/td.0002700/current-z.y=0', dx, dy)

xx_6, yy_6, zz_6, xx_6_lim, yy_6_lim, zz_6_lim = read_and_edit_data('../369_cobra/output_iter.new/td.0002700/current-z.y=0', dx, dy)


xx_1_mg, yy_1_mg, zz_1_mg = meshgrid_zz(xx_1_lim, yy_1_lim, dx, dy, zz_1)
levels_1 = np.linspace(-max(np.abs(zz_1)),max(np.abs(zz_1)),50)

xx_3_mg, yy_3_mg, zz_3_mg = meshgrid_zz(xx_3_lim, yy_3_lim, dx, dy, zz_3)
levels_3 = np.linspace(-max(np.abs(zz_3)),max(np.abs(zz_3)),50)

xx_5_mg, yy_5_mg, zz_5_mg = meshgrid_zz(xx_5_lim, yy_5_lim, dx, dy, zz_5)
levels_5 = np.linspace(-max(np.abs(zz_5)),max(np.abs(zz_5)),50)


delta = 1e-13
xx_7 = xx_1
yy_7 = yy_1
zz_7 = []
xx_7_lim = xx_1_lim
yy_7_lim = yy_1_lim
for ii in range(0, len(zz_1)):
    if ((zz_2[ii] != 0) and (np.abs(zz_1[ii]-zz_2[ii]) > delta)):
        zz_7.append(zz_1[ii]-zz_2[ii])
    else:
        zz_7.append(0)
xx_7_mg, yy_7_mg, zz_7_mg = meshgrid_zz(xx_7_lim, yy_7_lim, dx, dy, zz_7)
levels_7 = np.linspace(-max(np.abs(zz_7)),max(np.abs(zz_7)),50)


delta = 1e-13
xx_8 = xx_1
yy_8 = yy_1
zz_8 = []
xx_8_lim = xx_1_lim
yy_8_lim = yy_1_lim
for ii in range(0, len(zz_1)):
    if ((zz_4[ii] != 0) and (np.abs(zz_3[ii]-zz_4[ii]) > delta)):
        zz_8.append(zz_3[ii]-zz_4[ii])
    else:
        zz_8.append(0)
xx_8_mg, yy_8_mg, zz_8_mg = meshgrid_zz(xx_8_lim, yy_8_lim, dx, dy, zz_8)
levels_8 = np.linspace(-max(np.abs(zz_8)),max(np.abs(zz_8)),50)


delta = 1e-13
xx_9 = xx_1
yy_9 = yy_1
zz_9 = []
xx_9_lim = xx_1_lim
yy_9_lim = yy_1_lim
for ii in range(0, len(zz_1)):
    if ((zz_6[ii] != 0) and (np.abs(zz_5[ii]-zz_6[ii]) > delta)):
        zz_9.append(zz_5[ii]-zz_6[ii])
    else:
        zz_9.append(0)
xx_9_mg, yy_9_mg, zz_9_mg = meshgrid_zz(xx_9_lim, yy_9_lim, dx, dy, zz_9)
levels_9 = np.linspace(-max(np.abs(zz_9)),max(np.abs(zz_9)),50)


arrow_1_x = 34.0
arrow_1_y = 56.6917837387731

arrow_2_x = 28.3458918693866
arrow_2_y = 60.0


fig=plt.figure(figsize=(6,5), dpi= 100)
ax1 = fig.add_subplot(111)
ax1.axis(False)
cf1 = ax1.contourf(xx_1_mg, yy_1_mg, zz_1_mg, cmap=cm.PuOr_r, levels=levels_1)
ax1.scatter(geo_x, geo_y, s=10.0, c='k')
ax1.set_aspect('equal')
cbax1 = fig.add_axes([0.7, 0.2, 0.04, 0.6])
cb1 = fig.colorbar(cf1, cax=cbax1, format='%.0e')
cb1.ax.tick_params(labelsize=14)
ax1.text(50.0, 57.0, 'mA', fontsize=15)
cb1.set_ticks(np.linspace(-max(np.abs(zz_1)),max(np.abs(zz_1)),5))
ax1.plot([-arrow_1_x,-arrow_1_x], [-arrow_1_y, arrow_1_y], color='k') 
ax1.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [-arrow_1_y,-arrow_1_y], color='k')
ax1.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [ arrow_1_y, arrow_1_y], color='k')
ax1.text(-arrow_1_x+1.0, -9.0, '6.0nm', fontsize=15, rotation=90.)
ax1.plot([-arrow_2_x, arrow_2_x], [-arrow_2_y,-arrow_2_y], color='k')
ax1.plot([-arrow_2_x,-arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax1.plot([ arrow_2_x, arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax1.text(-11.0, -arrow_1_y-1.5, '3.0nm', fontsize=15)
plt.savefig('0.1nm_current-x.png', dpi=100)

fig=plt.figure(figsize=(6,5), dpi= 100)
ax2 = fig.add_subplot(111)
ax2.axis(False)
cf2 = ax2.contourf(xx_7_mg, yy_7_mg, zz_7_mg, cmap=cm.PuOr_r, levels=levels_7)
ax2.scatter(geo_x, geo_y, s=10.0, c='k')
ax2.set_aspect('equal')
cbax2 = fig.add_axes([0.7, 0.2, 0.04, 0.6])
cb2 = fig.colorbar(cf2, cax=cbax2, format='%.0e')
cb2.set_ticks(np.linspace(-max(np.abs(zz_7)),max(np.abs(zz_7)),5))
cb2.ax.tick_params(labelsize=14)
ax2.text(50.0, 57.0, 'mA', fontsize=15)
ax2.plot([-arrow_1_x,-arrow_1_x], [-arrow_1_y, arrow_1_y], color='k') 
ax2.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [-arrow_1_y,-arrow_1_y], color='k')
ax2.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [ arrow_1_y, arrow_1_y], color='k')
ax2.text(-arrow_1_x+1.0, -9.0, '6.0nm', fontsize=15, rotation=90.)
ax2.plot([-arrow_2_x, arrow_2_x], [-arrow_2_y,-arrow_2_y], color='k')
ax2.plot([-arrow_2_x,-arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax2.plot([ arrow_2_x, arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax2.text(-11.0, -arrow_1_y-1.5, '3.0nm', fontsize=15)
plt.savefig('0.1nm_current-x_diff.png', dpi=100)

fig=plt.figure(figsize=(6,5), dpi= 100)
ax3 = fig.add_subplot(111)
ax3.axis(False)
cf3 = ax3.contourf(xx_3_mg, yy_3_mg, zz_3_mg, cmap=cm.PuOr_r, levels=levels_3)
ax3.scatter(geo_x, geo_y, s=10.0, c='k')
ax3.set_aspect('equal')
cbax3 = fig.add_axes([0.7, 0.2, 0.04, 0.6])
cb3 = fig.colorbar(cf3, cax=cbax3, format='%.0e')
cb3.ax.tick_params(labelsize=14)
ax3.text(50.0, 57.0, 'mA', fontsize=15)
cb3.set_ticks(np.linspace(-max(np.abs(zz_3)),max(np.abs(zz_3)),5))
ax3.plot([-arrow_1_x,-arrow_1_x], [-arrow_1_y, arrow_1_y], color='k') 
ax3.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [-arrow_1_y,-arrow_1_y], color='k')
ax3.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [ arrow_1_y, arrow_1_y], color='k')
ax3.text(-arrow_1_x+1.0, -9.0, '6.0nm', fontsize=15, rotation=90.)
ax3.plot([-arrow_2_x, arrow_2_x], [-arrow_2_y,-arrow_2_y], color='k')
ax3.plot([-arrow_2_x,-arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax3.plot([ arrow_2_x, arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax3.text(-11.0, -arrow_1_y-1.5, '3.0nm', fontsize=15)
plt.savefig('0.1nm_current-y_0.png', dpi=100)

fig=plt.figure(figsize=(6,5), dpi= 100)
ax4 = fig.add_subplot(111)
ax4.axis(False)
cf4 = ax4.contourf(xx_8_mg, yy_8_mg, zz_8_mg, cmap=cm.PuOr_r, levels=levels_8)
ax4.scatter(geo_x, geo_y, s=10.0, c='k')
ax4.set_aspect('equal')
cbax4 = fig.add_axes([0.7, 0.2, 0.04, 0.6])
cb4 = fig.colorbar(cf4, cax=cbax4, format='%.0e')
cb4.ax.tick_params(labelsize=14)
ax4.text(50.0, 57.0, 'mA', fontsize=15)
cb4.set_ticks(np.linspace(-max(np.abs(zz_8)),max(np.abs(zz_8)),5))
ax4.plot([-arrow_1_x,-arrow_1_x], [-arrow_1_y, arrow_1_y], color='k') 
ax4.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [-arrow_1_y,-arrow_1_y], color='k')
ax4.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [ arrow_1_y, arrow_1_y], color='k')
ax4.text(-arrow_1_x+1.0, -9.0, '6.0nm', fontsize=15, rotation=90.)
ax4.plot([-arrow_2_x, arrow_2_x], [-arrow_2_y,-arrow_2_y], color='k')
ax4.plot([-arrow_2_x,-arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax4.plot([ arrow_2_x, arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax4.text(-11.0, -arrow_1_y-1.5, '3.0nm', fontsize=15)
plt.savefig('0.1nm_current-y_diff.png', dpi=100)

fig=plt.figure(figsize=(6,5), dpi= 100)
ax5 = fig.add_subplot(111)
ax5.axis(False)
cf5 = ax5.contourf(xx_5_mg, yy_5_mg, zz_5_mg, cmap=cm.PuOr_r, levels=levels_5)
ax5.scatter(geo_x, geo_y, s=10.0, c='k')
ax5.set_aspect('equal')
cbax5 = fig.add_axes([0.7, 0.2, 0.04, 0.6])
cb5 = fig.colorbar(cf5, cax=cbax5, format='%.0e')
cb5.ax.tick_params(labelsize=14)
ax5.text(50.0, 57.0, 'mA', fontsize=15)
cb5.set_ticks(np.linspace(-max(np.abs(zz_5)),max(np.abs(zz_5)),5))
ax5.plot([-arrow_1_x,-arrow_1_x], [-arrow_1_y, arrow_1_y], color='k') 
ax5.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [-arrow_1_y,-arrow_1_y], color='k')
ax5.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [ arrow_1_y, arrow_1_y], color='k')
ax5.text(-arrow_1_x+1.0, -9.0, '6.0nm', fontsize=15, rotation=90.)
ax5.plot([-arrow_2_x, arrow_2_x], [-arrow_2_y,-arrow_2_y], color='k')
ax5.plot([-arrow_2_x,-arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax5.plot([ arrow_2_x, arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax5.text(-11.0, -arrow_1_y-1.5, '3.0nm', fontsize=15)
plt.savefig('0.1nm_current-z_0.png', dpi=100)

fig=plt.figure(figsize=(6,5), dpi= 100)
ax6 = fig.add_subplot(111)
ax6.axis(False)
cf6 = ax6.contourf(xx_9_mg, yy_9_mg, zz_9_mg, cmap=cm.PuOr_r, levels=levels_9)
ax6.scatter(geo_x, geo_y, s=10.0, c='k')
ax6.set_aspect('equal')
cbax6 = fig.add_axes([0.7, 0.2, 0.04, 0.6])
cb6 = fig.colorbar(cf6, cax=cbax6, format='%.0e')
cb6.ax.tick_params(labelsize=14)
ax6.text(50.0, 57.0, 'mA', fontsize=15)
cb6.set_ticks(np.linspace(-max(np.abs(zz_9)),max(np.abs(zz_9)),5))
ax6.plot([-arrow_1_x,-arrow_1_x], [-arrow_1_y, arrow_1_y], color='k') 
ax6.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [-arrow_1_y,-arrow_1_y], color='k')
ax6.plot([-arrow_1_x-2.0,-arrow_1_x+2.0], [ arrow_1_y, arrow_1_y], color='k')
ax6.text(-arrow_1_x+1.0, -9.0, '6.0nm', fontsize=15, rotation=90.)
ax6.plot([-arrow_2_x, arrow_2_x], [-arrow_2_y,-arrow_2_y], color='k')
ax6.plot([-arrow_2_x,-arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax6.plot([ arrow_2_x, arrow_2_x], [-arrow_2_y-2.0,-arrow_2_y+2.0], color='k')
ax6.text(-11.0, -arrow_1_y-1.5, '3.0nm', fontsize=15)
plt.savefig('0.1nm_current-z_diff.png', dpi=100)


#plt.show()


