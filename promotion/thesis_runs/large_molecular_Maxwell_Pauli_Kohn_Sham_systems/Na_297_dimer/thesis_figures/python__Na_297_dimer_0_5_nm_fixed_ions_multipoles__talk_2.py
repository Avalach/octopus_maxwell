#!/bin/bash

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from scipy.fftpack import fft, ifft
from scipy import signal
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(figsize=(1,10))

gs = gridspec.GridSpec( 4, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

#ax1 = plt.subplot(gs[0:4,:])
#ax2 = plt.subplot(gs[4:8,0])
#ax3 = plt.subplot(gs[8:12,0])
#ax4 = plt.subplot(gs[0:4,0])
ax5 = plt.subplot(gs[0:4,0])
#ax6 = plt.subplot(gs[4:8,0])

#ax7 = plt.subplot(gs[23:25,0])
#ax8 = plt.subplot(gs[25:27,0])
#ax9 = plt.subplot(gs[27:29,0])
#ax10= plt.subplot(gs[29:31,0])


dt       = 0.2106550
relax    = 1065
step_0   = 107
t_offset = - relax * dt
f_offset = 0.0

cc        = 137.035999679
pi        = 3.14159265359
amplitude = 0.0001
omega     = 0.112000023380
period    = 2*pi/omega
width     = 38438.5000/cc
shift     = 38438.5000/cc 

period_number = 20

N_total_points = 6500
N_plot_points  = N_total_points-relax
N_fft          = int(N_plot_points/2+1)

N_total_points_2 = 650
N_plot_points_2  = N_total_points_2-step_0
N_fft_2          = int(N_plot_points_2/2+1)



###################################################################################################
#  Laser function:
###################################################################################################

def f_laser(x,a0,x0,w,omega):
    y = []
    for ii in range(0,len(x)):
       if ( np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2))) < w ):
          y.append( a0 * np.cos(pi/2*(x[ii]-2*w-x0)/w+pi) * np.cos(omega*(x[ii]-x0)) )
       else:
          y.append(0.0)
    return y

t_laser = np.arange(0.0, N_plot_points*dt, dt)


##### Laser fourier transform ###################

w_fftl = []
for ii in range(0, len(t_laser)):
    w_fftl.append(2 * np.pi * ii / len(t_laser))

N_fftl     = N_fft # int(len(t_laser)/2+1)
dt_fftl    = dt*10
w_max_fftl = 1/dt_fftl*2*np.pi
w_fftl     = np.linspace(0, w_max_fftl/2, N_fftl)
f_fftl     = 2*fft(f_laser(t_laser,amplitude,shift,width,omega))/N_fftl




###################################################################################################
#  Broadening function:
###################################################################################################

def f_broadening(x,w):
    y = []
    for ii in range(0,len(x)):
       y.append(np.exp(-x[ii]**2/(2*w**2)))
    return y

#def convolution(x,xw):
#    for ii in range(0,len(x)):
#       for jj in range(0,len(xw)):

x_broadening = np.arange(-period_number*omega/2,period_number*omega/2+w_fftl[2]-w_fftl[1],w_fftl[2]-w_fftl[1])

y_broadening = f_broadening(x_broadening,0.02)





###################################################################################################
#  Dataset 1:
###################################################################################################

t_m1 = []
f_m1 = []
with open('../370_cobra/td.general/multipoles') as m1:
    for line in m1:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m1.append(line.split()[1])
           f_m1.append(line.split()[3])
f0_m1 = float(f_m1[step_0])

tw_m1 = []
fw_m1 = []
for ii in range(0, N_total_points):
    if (float(t_m1[ii]) + float(t_offset) > 0.0):
       tw_m1.append(float(t_m1[ii]) + t_offset)
       fw_m1.append(5*float(f_m1[ii]))


##### Fourier transform #########################

w_fftm1 = []
for ii in range(0, len(tw_m1)):
   w_fftm1.append(2 * np.pi * ii / len(tw_m1))

N_fftm1     = N_fft # len(t_m1)/2+1
dt_fftm1    = tw_m1[2]-tw_m1[1]
w_max_fftm1 = 1/dt_fftm1*2*np.pi
w_fftm1     = np.linspace(0, w_max_fftm1/2, N_fftm1)
f_fftm1     = 2*fft(fw_m1)/N_fftm1
#f_fftm1     = signal.convolve(f_fftm1, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 2:
###################################################################################################

t_m2 = []
f_m2 = []
with open('../371_cobra/td.general/multipoles') as m2:
    for line in m2:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m2.append(line.split()[1])
           f_m2.append(line.split()[3])
f0_m2 = float(f_m2[step_0])

tw_m2 = []
fw_m2 = []
for ii in range(0, N_total_points):
    if (float(t_m2[ii]) + float(t_offset) > 0.0):
       tw_m2.append(float(t_m2[ii]) + t_offset)
       fw_m2.append(5*float(f_m2[ii])) 


##### Fourier transform #########################

w_fftm2 = []
for ii in range(0, len(tw_m2)):
   w_fftm2.append(2 * np.pi * ii / len(tw_m2))

N_fftm2     = N_fft # len(t_m2)/2+1
dt_fftm2    = tw_m2[2]-tw_m2[1]
w_max_fftm2 = 1/dt_fftm2*2*np.pi
w_fftm2     = np.linspace(0, w_max_fftm2/2, N_fftm2)
f_fftm2     = 2*fft(fw_m2)/N_fftm2
#f_fftm2     = signal.convolve(f_fftm2, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 3:
###################################################################################################

t_m3 = []
f_m3 = []
with open('../372_cobra/td.general/multipoles') as m3:
    for line in m3:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m3.append(line.split()[1])
           f_m3.append(line.split()[3])
f0_m3 = float(f_m3[step_0])

tw_m3 = []
fw_m3 = []
for ii in range(0, N_total_points):
    if (float(t_m3[ii]) + float(t_offset) > 0.0):
       tw_m3.append(float(t_m3[ii]) + t_offset)
       fw_m3.append(float(f_m3[ii])) 


##### Fourier transform #########################

w_fftm3 = []
for ii in range(0, len(tw_m3)):
   w_fftm3.append(2 * np.pi * ii / len(tw_m3))

N_fftm3     = N_fft # len(t_m3)/2+1
dt_fftm3    = tw_m3[2]-tw_m3[1]
w_max_fftm3 = 1/dt_fftm3*2*np.pi
w_fftm3     = np.linspace(0, w_max_fftm3/2, N_fftm3)
f_fftm3     = 2*fft(fw_m3)/N_fftm3
#f_fftm3     = signal.convolve(f_fftm3, y_broadening, mode='same') # / sum(y_broadening)

print('Number of common points are:', len(t_m3))
print('Number of plot points are  :', len(tw_m3))
print('Number of fft points are   :', N_fftm3)




###################################################################################################
#  Dataset 4:
###################################################################################################

t_m4 = []
f_m4 = []
with open('../373_cobra/td.general/multipoles') as m4:
    for line in m4:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m4.append(line.split()[1])
           f_m4.append(line.split()[3])
f0_m4 = float(f_m4[step_0])

tw_m4 = []
fw_m4 = []
for ii in range(0, N_total_points):
    if (float(t_m4[ii]) + float(t_offset) > 0.0):
       tw_m4.append(float(t_m4[ii]) + t_offset)
       fw_m4.append(float(f_m4[ii])) 



##### Fourier transform #########################

w_fftm4 = []
for ii in range(0, len(tw_m4)):
   w_fftm4.append(2 * np.pi * ii / len(tw_m4))

N_fftm4     = N_fft # len(t_m4)/2+1
dt_fftm4    = tw_m4[2]-tw_m4[1]
w_max_fftm4 = 1/dt_fftm4*2*np.pi
w_fftm4     = np.linspace(0, w_max_fftm4/2, N_fftm4)
f_fftm4     = 2*fft(fw_m4)/N_fftm4
#f_fftm4     = signal.convolve(f_fftm4, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 5:
###################################################################################################

t_m5 = []
f_m5 = []
with open('../370_cobra/td.general/multipoles') as m5:
    for line in m5:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m5.append(line.split()[1])
           f_m5.append(line.split()[4])
f0_m5 = float(f_m5[step_0])

tw_m5 = []
fw_m5 = []
for ii in range(0, N_total_points):
    if (float(t_m5[ii]) + float(t_offset) > 0.0):
       tw_m5.append(float(t_m5[ii]) + t_offset)
       fw_m5.append(float(f_m5[ii]))


##### Fourier transform #########################

w_fftm5 = []
for ii in range(0, len(tw_m5)):
   w_fftm5.append(2 * np.pi * ii / len(tw_m5))

N_fftm5     = N_fft # len(t_m5)/2+1
dt_fftm5    = tw_m5[2]-tw_m5[1]
w_max_fftm5 = 1/dt_fftm5*2*np.pi
w_fftm5     = np.linspace(0, w_max_fftm5/2, N_fftm5)
f_fftm5     = 2*fft(fw_m5)/N_fftm5
#f_fftm5     = signal.convolve(f_fftm5, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 6:
###################################################################################################

t_m6 = []
f_m6 = []
with open('../371_cobra/td.general/multipoles') as m6:
    for line in m6:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m6.append(line.split()[1])
           f_m6.append(line.split()[4])
f0_m6 = float(f_m6[step_0])

tw_m6 = []
fw_m6 = []
for ii in range(0, N_total_points):
    if (float(t_m6[ii]) + float(t_offset) > 0.0):
       tw_m6.append(float(t_m6[ii]) + t_offset)
       fw_m6.append(float(f_m6[ii]))


##### Fourier transform #########################

w_fftm6 = []
for ii in range(0, len(tw_m6)):
   w_fftm6.append(2 * np.pi * ii / len(tw_m6))

N_fftm6     = N_fft # len(t_m6)/2+1
dt_fftm6    = tw_m6[2]-tw_m6[1]
w_max_fftm6 = 1/dt_fftm6*2*np.pi
w_fftm6     = np.linspace(0, w_max_fftm6/2, N_fftm6)
f_fftm6     = 2*fft(fw_m6)/N_fftm6
#f_fftm6     = signal.convolve(f_fftm6, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 7:
###################################################################################################

t_m7 = []
f_m7 = []
with open('../372_cobra/td.general/multipoles') as m7:
    for line in m7:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m7.append(line.split()[1])
           f_m7.append(line.split()[4])
f0_m7 = float(f_m7[step_0])

tw_m7 = []
fw_m7 = []
for ii in range(0, N_total_points):
    if (float(t_m7[ii]) + float(t_offset) > 0.0):
       tw_m7.append(float(t_m7[ii]) + t_offset)
       fw_m7.append(float(f_m7[ii]))


##### Fourier transform #########################

w_fftm7 = []
for ii in range(0, len(tw_m7)):
   w_fftm7.append(2 * np.pi * ii / len(tw_m7))

N_fftm7     = N_fft # len(t_m7)/2+1
dt_fftm7    = tw_m7[2]-tw_m7[1]
w_max_fftm7 = 1/dt_fftm7*2*np.pi
w_fftm7     = np.linspace(0, w_max_fftm7/2, N_fftm7)
f_fftm7     = 2*fft(fw_m7)/N_fftm7
#f_fftm7     = signal.convolve(f_fftm7, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 8:
###################################################################################################

t_m8 = []
f_m8 = []
with open('../373_cobra/td.general/multipoles') as m8:
    for line in m8:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m8.append(line.split()[1])
           f_m8.append(line.split()[4])
f0_m8 = float(f_m8[step_0])

tw_m8 = []
fw_m8 = []
for ii in range(0, N_total_points):
    if (float(t_m8[ii]) + float(t_offset) > 0.0):
       tw_m8.append(float(t_m8[ii]) + t_offset)
       fw_m8.append(float(f_m8[ii]))


##### Fourier transform #########################

w_fftm8 = []
for ii in range(0, N_total_points):
   w_fftm8.append(2 * np.pi * ii / len(tw_m8))

N_fftm8     = N_fft # len(t_m8)/2+1
dt_fftm8    = tw_m8[2]-tw_m8[1]
w_max_fftm8 = 1/dt_fftm8*2*np.pi
w_fftm8     = np.linspace(0, w_max_fftm8/2, N_fftm8)
f_fftm8     = 2*fft(fw_m8)/N_fftm8
#f_fftm8     = signal.convolve(f_fftm8, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 9:
###################################################################################################

t_m9 = []
f_m9 = []
with open('../370_cobra/td.general/multipoles') as m9:
    for line in m9:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m9.append(line.split()[1])
           f_m9.append(line.split()[5])
f0_m9 = float(f_m5[step_0])

tw_m9 = []
fw_m9 = []
for ii in range(0, N_total_points):
    if (float(t_m9[ii]) + float(t_offset) > 0.0):
       tw_m9.append(float(t_m9[ii]) + t_offset)
       fw_m9.append(float(f_m9[ii]))


##### Fourier transform #########################

w_fftm9 = []
for ii in range(0, len(tw_m9)):
   w_fftm9.append(2 * np.pi * ii / len(tw_m9))

N_fftm9     = N_fft # len(t_m5)/2+1
dt_fftm9    = tw_m9[2]-tw_m9[1]
w_max_fftm9 = 1/dt_fftm9*2*np.pi
w_fftm9     = np.linspace(0, w_max_fftm5/2, N_fftm9)
f_fftm9     = 2*fft(fw_m9)/N_fftm9
#f_fftm9     = signal.convolve(f_fftm9, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 10:
###################################################################################################

t_m10 = []
f_m10 = []
with open('../371_cobra/td.general/multipoles') as m10:
    for line in m10:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m10.append(line.split()[1])
           f_m10.append(line.split()[5])
f0_m10 = float(f_m10[step_0])

tw_m10 = []
fw_m10 = []
for ii in range(0, N_total_points):
    if (float(t_m10[ii]) + float(t_offset) > 0.0):
       tw_m10.append(float(t_m10[ii]) + t_offset)
       fw_m10.append(float(f_m10[ii]))


##### Fourier transform #########################

w_fftm10 = []
for ii in range(0, len(tw_m10)):
   w_fftm10.append(2 * np.pi * ii / len(tw_m10))

N_fftm10     = N_fft # len(t_m10)/2+1
dt_fftm10    = tw_m10[2]-tw_m10[1]
w_max_fftm10 = 1/dt_fftm10*2*np.pi
w_fftm10     = np.linspace(0, w_max_fftm10/2, N_fftm10)
f_fftm10     = 2*fft(fw_m10)/N_fftm10
#f_fftm10     = signal.convolve(f_fftm10, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 11:
###################################################################################################

t_m11 = []
f_m11 = []
with open('../372_cobra/td.general/multipoles') as m11:
    for line in m11:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m11.append(line.split()[1])
           f_m11.append(line.split()[5])
f0_m11 = float(f_m11[step_0])

tw_m11 = []
fw_m11 = []
for ii in range(0, N_total_points):
    if (float(t_m11[ii]) + float(t_offset) > 0.0):
       tw_m11.append(float(t_m11[ii]) + t_offset)
       fw_m11.append(float(f_m11[ii]))


##### Fourier transform #########################

w_fftm11 = []
for ii in range(0, len(tw_m11)):
   w_fftm11.append(2 * np.pi * ii / len(tw_m11))

N_fftm11     = N_fft # len(t_m11)/2+1
dt_fftm11    = tw_m11[2]-tw_m11[1]
w_max_fftm11 = 1/dt_fftm11*2*np.pi
w_fftm11     = np.linspace(0, w_max_fftm11/2, N_fftm11)
f_fftm11     = 2*fft(fw_m11)/N_fftm11
#f_fftm11     = signal.convolve(f_fftm11, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 12:
###################################################################################################

t_m12 = []
f_m12 = []
with open('../373_cobra/td.general/multipoles') as m12:
    for line in m12:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_m12.append(line.split()[1])
           f_m12.append(line.split()[5])
f0_m12 = float(f_m12[step_0])

tw_m12 = []
fw_m12 = []
for ii in range(0, N_total_points):
    if (float(t_m12[ii]) + float(t_offset) > 0.0):
       tw_m12.append(float(t_m12[ii]) + t_offset)
       fw_m12.append(float(f_m12[ii]))


##### Fourier transform #########################

w_fftm12 = []
for ii in range(0, N_total_points):
   w_fftm12.append(2 * np.pi * ii / len(tw_m12))

N_fftm12     = N_fft # len(t_m12)/2+1
dt_fftm12    = tw_m12[2]-tw_m12[1]
w_max_fftm12 = 1/dt_fftm12*2*np.pi
w_fftm12     = np.linspace(0, w_max_fftm12/2, N_fftm12)
f_fftm12     = 2*fft(fw_m12)/N_fftm12
#f_fftm12     = signal.convolve(f_fftm12, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 13:
###################################################################################################
t_data1 = []
f_data1 = []
f_data1_max = 0.0
with open('../370_cobra/td.general/maxwell_e_field_x_at_00_p30_00') as data1:
    readlines = data1.readlines()
    t_data1 = [line.split()[0] for line in readlines]
    f_data1 = [line.split()[1] for line in readlines]
f0_data1 = float(f_data1[step_0])
tw_data1 = []
fw_data1 = []
for ii in range(0, N_total_points_2):
    if (float(t_data1[ii]) + float(t_offset) > 0.0):
       tw_data1.append(float(t_data1[ii]) + t_offset)
       fw_data1.append(float(f_data1[ii]))


##### Fourier transform #########################

w_fftdata1 = []
for ii in range(0, len(tw_data1)):
   w_fftdata1.append(2 * np.pi * ii / len(tw_data1))

N_fftdata1     = N_fft_2 # len(t_data1)/2+1
dt_fftdata1    = tw_data1[2]-tw_data1[1]
w_max_fftdata1 = 1/dt_fftdata1*2*np.pi
w_fftdata1     = np.linspace(0, w_max_fftdata1/2, N_fftdata1)
f_fftdata1     = 2*fft(fw_data1)/N_fftdata1
#f_fftdata1     = signal.convolve(f_fftdata1, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 14:
###################################################################################################
t_data2 = []
f_data2 = []
idx = 0
with open('../371_cobra/td.general/maxwell_e_field_x_at_00_p30_00') as data2:
    readlines = data2.readlines()
    t_data2 = [line.split()[0] for line in readlines]
    f_data2 = [line.split()[1] for line in readlines]
f0_data2 = float(f_data2[step_0])
tw_data2 = []
fw_data2 = []
for ii in range(0, N_total_points_2):
    if (float(t_data2[ii]) + float(t_offset) > 0.0):
       tw_data2.append(float(t_data2[ii]) + t_offset)
       fw_data2.append(float(f_data2[ii]))


##### Fourier transform #########################

w_fftdata2 = []
for ii in range(0, len(tw_data2)):
   w_fftdata2.append(2 * np.pi * ii / len(tw_data2))

N_fftdata2     = N_fft_2 # len(t_data2)/2+1
dt_fftdata2    = tw_data2[2]-tw_data2[1]
w_max_fftdata2 = 1/dt_fftdata2*2*np.pi
w_fftdata2     = np.linspace(0, w_max_fftdata2/2, N_fftdata2)
f_fftdata2     = 2*fft(fw_data2)/N_fftdata2
#f_fftdata2     = signal.convolve(f_fftdata2, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 15:
###################################################################################################
t_data3 = []
f_data3 = []
idx = 0
with open('../372_cobra/td.general/maxwell_e_field_x_at_00_p30_00') as data3:
    readlines = data3.readlines()
    t_data3 = [line.split()[0] for line in readlines]
    f_data3 = [line.split()[1] for line in readlines]
f0_data3 = float(f_data3[step_0])
tw_data3 = []
fw_data3 = []
for ii in range(0, N_total_points_2):
    if (float(t_data3[ii]) + float(t_offset) > 0.0):
       tw_data3.append(float(t_data3[ii]) + t_offset)
       fw_data3.append(float(f_data3[ii]))


##### Fourier transform #########################

w_fftdata3 = []
for ii in range(0, len(tw_data3)):
   w_fftdata3.append(2 * np.pi * ii / len(tw_data3))

N_fftdata3     = N_fft_2 # len(t_data3)/2+1
dt_fftdata3    = tw_data3[2]-tw_data3[1]
w_max_fftdata3 = 1/dt_fftdata3*2*np.pi
w_fftdata3     = np.linspace(0, w_max_fftdata3/2, N_fftdata3)
f_fftdata3     = 2*fft(fw_data3)/N_fftdata3
#f_fftdata3     = signal.convolve(f_fftdata3, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 16:
###################################################################################################
t_data4 = []
f_data4 = []
idx = 0
with open('../373_cobra/td.general/maxwell_e_field_x_at_00_p30_00') as data4:
    readlines = data4.readlines()
    t_data4 = [line.split()[0] for line in readlines]
    f_data4 = [line.split()[1] for line in readlines]
f0_data2 = float(f_data2[step_0])
tw_data4 = []
fw_data4 = []
for ii in range(0, N_total_points_2):
    if (float(t_data4[ii]) + float(t_offset) > 0.0):
       tw_data4.append(float(t_data4[ii]) + t_offset)
       fw_data4.append(float(f_data4[ii]))


##### Fourier transform #########################

w_fftdata4 = []
for ii in range(0, len(tw_data4)):
   w_fftdata4.append(2 * np.pi * ii / len(tw_data4))

N_fftdata4     = N_fft_2 # len(t_data4)/2+1
dt_fftdata4    = tw_data4[2]-tw_data4[1]
w_max_fftdata4 = 1/dt_fftdata4*2*np.pi
w_fftdata4     = np.linspace(0, w_max_fftdata4/2, N_fftdata4)
f_fftdata4     = 2*fft(fw_data4)/N_fftdata4
#f_fftdata4     = signal.convolve(f_fftdata4, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 16:
###################################################################################################
#t_data5 = []
fw_data5 = []
for ii in range(0, len(fw_data3)):
    tw_data5 = tw_data3
    fw_data5.append(float(fw_data3[ii])-float(fw_data1[ii]))


##### Fourier transform #########################

w_fftdata5 = []
for ii in range(0, len(tw_data5)):
   w_fftdata5.append(2 * np.pi * ii / len(tw_data5))

N_fftdata5     = N_fft_2 # len(t_data5)/2+1
dt_fftdata5    = tw_data5[2]-tw_data5[1]
w_max_fftdata5 = 1/dt_fftdata5*2*np.pi
w_fftdata5     = np.linspace(0, w_max_fftdata5/2, N_fftdata5)
f_fftdata5     = 2*fft(fw_data5)/N_fftdata5
#f_fftdata5     = signal.convolve(f_fftdata5, y_broadening, mode='same') # / sum(y_broadening)





###################################################################################################
#  Plots:
###################################################################################################

#fig.suptitle(r"Dipoles and electric field for the dimer with d$_2$=0.5nm",x=0.5,y=1.17, size=18)

##### Panel 1 plot ##############################
#ax1_datal, = ax1.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), '-', color=(0.100,0.100,0.100), linewidth=1.5, label='total field of the external laser')

##### Panel 2 plot ##############################
#ax2_data6, = ax2.plot(tw_m10, fw_m10, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='forward coupled in dipole approximation')
#ax2_data4, = ax2.plot(tw_m12, fw_m12, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='forward coupling beyond dipole approximation')
#ax2_data7, = ax2.plot(tw_m9, fw_m9, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='fully coupled in dipole approximation')
#ax2_data3, = ax2.plot(tw_m11, fw_m11, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='forward-backward coupling beyond dipole approximation')

##### Panel 3 plot ##############################
#ax3_data6, = ax3.plot(tw_m6, fw_m6, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='forward coupled in dipole approximation')
#ax3_data4, = ax3.plot(tw_m8, fw_m8, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='forward coupled beyond dipole approximation')
#ax3_data7, = ax3.plot(tw_m5, fw_m5, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='fully coupled in dipole approximation')
#ax3_data3, = ax3.plot(tw_m7, fw_m7, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='fully coupled beyond dipole approximation')

##### Panel 4 plot ##############################
#ax4_data2, = ax4.plot(tw_m2, fw_m2, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='forward coupled in dipole approximation')
#ax4_data1, = ax4.plot(tw_m1, fw_m1, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='fully coupled in dipole approximation')

##### Panel 5 plot ##############################
ax5_data2, = ax5.plot(tw_m2, fw_m2, '-', color=(0.000,0.000,1.000), linewidth=1.5, label='(5x) forward coupled in dipole approximation')
ax5_data1, = ax5.plot(tw_m1, fw_m1, '-', color=(1.000,0.000,0.000), linewidth=1.5, label='(5x) fully coupled in dipole approximation')
ax5_data4, = ax5.plot(tw_m4, fw_m4, '-', color=(0.200,0.800,0.200), linewidth=1.5, label='forward coupled beyond dipole approximation')
ax5_data3, = ax5.plot(tw_m3, fw_m3, '-', color=(1.000,0.600,0.000), linewidth=1.5, label='fully coupled beyond dipole approximation')

##### Panel 6 plot ##############################
#ax6_data5, = ax6.plot(tw_data5, fw_data5, '-', color=(0.500,0.200,0.700), linewidth=1.5, label='difference of fully coupled beyond dipole and dipole approximation')
#ax6_data1, = ax6.plot(tw_data1, fw_data1, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='fully coupled in dipole approximation')
#ax6_data3, = ax6.plot(tw_data3, fw_data3, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='fully coupled beyond dipole approximation')



###################################################################################################
#  Units transformation:
###################################################################################################

# time in au
t_au = 2.418884326505*10**(-17)

# time in femto seconds
t_fm = 10**(-15)

# energy from atomic untis to eV
au_in_eV       = 27.2114

# electric field from atomic units to V/m
au_in_V_per_m = 5.14220652*(10**11)

# lenght in au
l_au = 5.2917721092*10**(-11)

# lenght in nm
l_nm = 10**(-9)

# x-tick interval
femto_interval = 2.5

# Femtosecond to atomic units factor
femto_in_au = t_fm/t_au * femto_interval

# au in nm
au_in_nm = l_au / l_nm

# x-ticks
xticks_t_femto = np.arange(0, period_number*period, femto_in_au)
xticks_t_au    = np.arange(0, period_number*period/femto_in_au*femto_interval, femto_interval) 

# x-ticks
xticks_eV = np.arange(0, 0.31 * au_in_eV, 1.0)
xticks_au = xticks_eV/au_in_eV

# y-tick-labels
yticks_eV       = np.arange(-142.905, -142.88, 0.005)
yticklabels_eV  = np.arange(-3888.65, -3887.96, 0.05, dtype=float)




###################################################################################################
#  Panel 1: Laser
###################################################################################################

# Axes label top
#ax1.set_xlabel("time in atomic units", size=15)
#ax1.xaxis.set_label_coords(0.5,1.34)
#ax1.xaxis.set_label_position('top')

# au limits for Panel 3
#ax1_xlim_1 = 0.0
#ax1_xlim_2 = period_number*period
#ax1_ylim_1 = -1.10e-4
#ax1_ylim_2 =  1.10e-4

#factor = 10**7

#ax1.text(ax1_xlim_1+0.02*(np.abs(ax1_xlim_2-ax1_xlim_1)), ax1_ylim_1+0.8*(np.abs(ax1_ylim_2-ax1_ylim_1)),'a)',size=18)


##### Y left label axes #########################

#ax1_y1 = ax1.twinx()

# Axes label on the left
#ax1_y1.set_ylabel(r'E$_{z}$ [$\times 10^7$ V/m]', size=13, position=(0.0,0.55))
#ax1_y1.yaxis.set_label_position('left')
#ax1_y1.yaxis.set_label_coords(-0.080,0.55)

# ticks
#ax1_y1_ticks = [-4*10**7/factor,0.0/factor,4*10**7/factor]
#ax1_y1.set_yticks(ax1_y1_ticks)
#ax1_y1.set_yticklabels(ax1_y1_ticks, size=13)
#ax1_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
#ax1_y1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# limits
#ax1_y1_ylim_1 = ax1_ylim_1 * au_in_V_per_m / factor
#ax1_y1_ylim_2 = ax1_ylim_2 * au_in_V_per_m / factor
#ax1_y1.set_ylim(ax1_y1_ylim_1, ax1_y1_ylim_2)


##### Y right label axes ########################

#ax1_y2 = ax1.twinx()

# Axes label on the right
#ax1_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
#ax1_y2.yaxis.set_label_position('right')

# ticks
#ax1_y2_ticks = [-4*10**7/au_in_V_per_m, 0.0/au_in_V_per_m, 4*10**7/au_in_V_per_m]
#ax1_y2.set_yticks(ax1_y2_ticks)
#ax1_y2.set_yticklabels(ax1_y2_ticks, size=13)
#ax1_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
#ax1_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax1_y2_ylim_1 = ax1_ylim_1
#ax1_y2_ylim_2 = ax1_ylim_2
#ax1_y2.set_ylim(ax1_y2_ylim_1, ax1_y2_ylim_2)


##### plot axes #################################

# limits
#ax1.set_xlim(ax1_xlim_1, ax1_xlim_2)
#ax1.set_ylim(ax1_ylim_1, ax1_ylim_2)

# ticks
#ax1.set_xticks(xticks_t_femto)
#ax1.set_xticklabels(xticks_t_femto, size=13)
#ax1.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=False, bottom=True, top=True, right=False, left=False, direction='inout', length=8)
#ax1.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -10, 10]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax1.add_line(line)
#r1 = [ 0, period_number*period]
#r2 = [ 0,  0]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax1.add_line(line)




###################################################################################################
#  Panel 2: Multipole <z>
###################################################################################################

# au limits for the laser
#ax2_xlim_1 = 0.0
#ax2_xlim_2 = period_number*period
#ax2_ylim_1 = -3.7e+1
#ax2_ylim_2 =  3.7e+1

#factor = 1.0

#ax2.text(ax2_xlim_1+0.02*(np.abs(ax2_xlim_2-ax2_xlim_1)), ax2_ylim_1+0.8*(np.abs(ax2_ylim_2-ax2_ylim_1)),'b)',size=18)


##### Y left label axes #########################

#ax2_y1 = ax2.twinx()

# Axes label on the left
#ax2_y1.set_ylabel("<z> [nm]", size=13, position=(0.0,0.55))
#ax2_y1.yaxis.set_label_position('left')
#ax2_y1.yaxis.set_label_coords(-0.080,0.55)

# labels
#ax2_y1_ticks = np.arange(-3.0e0/factor, 3.1e0/factor, 1.0e0/factor)
#ax2_y1.set_yticks(ax2_y1_ticks)
#ax2_y1.set_yticklabels(ax2_y1_ticks, size=13)
#ax2_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
#ax2_y1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# limits
#ax2_y1_ylim_1 = ax2_ylim_1 * au_in_nm / factor
#ax2_y1_ylim_2 = ax2_ylim_2 * au_in_nm / factor
#ax2_y1.set_ylim(ax2_y1_ylim_1, ax2_y1_ylim_2)


##### Y right label axes ########################

#ax2_y2 = ax2.twinx()

# Axes label on the right
#ax2_y2.set_ylabel("<z> [a.u.]", size=15, position=(0.0,0.55))
#ax2_y2.yaxis.set_label_position('right')

# ticks
#ax2_y2_ticks = ax2_y1_ticks / au_in_nm
#ax2_y2.set_yticks(ax2_y2_ticks)
#ax2_y2.set_yticklabels(ax2_y2_ticks, size=13)
#ax2_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
#ax2_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax2_y2_ylim_1 = ax2_ylim_1
#ax2_y2_ylim_2 = ax2_ylim_2
#ax2_y2.set_ylim(ax2_y2_ylim_1, ax2_y2_ylim_2)


##### plot axes #################################

# ticks
#ax2.set_xticks(xticks_t_femto)
#ax2.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=False, bottom=True, top=True, right=False, left=False, direction='inout', length=6)
#ax2.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
#ax2.set_xlim(ax2_xlim_1, ax2_xlim_2)
#ax2.set_ylim(ax2_ylim_1, ax2_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -500, 500]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax2.add_line(line)
#r1 = [ 0, 30*period]
#r2 = [ f0_m1,  f0_m1]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax2.add_line(line)




###################################################################################################
#  Panel 3: Multipole <y>
###################################################################################################

# au limits for the laser
#ax3_xlim_1 = 0.0
#ax3_xlim_2 = period_number*period
#ax3_ylim_1 = -8.7e+0
#ax3_ylim_2 =  8.7e+0

#factor = 1.0

#ax3.text(ax3_xlim_1+0.02*(np.abs(ax3_xlim_2-ax3_xlim_1)), ax3_ylim_1+0.8*(np.abs(ax3_ylim_2-ax3_ylim_1)),'c)',size=18)


##### Y left label axes #########################

#ax3_y1 = ax3.twinx()

# Axes label on the left
#ax3_y1.set_ylabel("<y> [nm]", size=13, position=(0.0,0.55))
#ax3_y1.yaxis.set_label_position('left')
#ax3_y1.yaxis.set_label_coords(-0.080,0.55)

# labels
#ax3_y1_ticks = np.arange(-2.5e-1/factor, 2.6e-1/factor, 2.5e-1/factor)
#ax3_y1.set_yticks(ax3_y1_ticks)
#ax3_y1.set_yticklabels(ax3_y1_ticks, size=13)
#ax3_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
#ax3_y1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# limits
#ax3_y1_ylim_1 = ax3_ylim_1 * au_in_nm / factor
#ax3_y1_ylim_2 = ax3_ylim_2 * au_in_nm / factor
#ax3_y1.set_ylim(ax3_y1_ylim_1, ax3_y1_ylim_2)


##### Y right label axes ########################

#ax3_y2 = ax3.twinx()

# Axes label on the right
#ax3_y2.set_ylabel("<y> [a.u.]", size=15, position=(0.0,0.55))
#ax3_y2.yaxis.set_label_position('right')

# ticks
#ax3_y2_ticks = ax3_y1_ticks / au_in_nm
#ax3_y2.set_yticks(ax3_y2_ticks)
#ax3_y2.set_yticklabels(ax3_y2_ticks, size=13)
#ax3_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
#ax3_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax3_y2_ylim_1 = ax3_ylim_1
#ax3_y2_ylim_2 = ax3_ylim_2
#ax3_y2.set_ylim(ax3_y2_ylim_1, ax3_y2_ylim_2)


##### plot axes #################################

# ticks
#ax3.set_xticks(xticks_t_femto)
#ax3.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=False, bottom=True, top=True, right=False, left=False, direction='inout', length=6)
#ax3.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
#ax3.set_xlim(ax3_xlim_1, ax3_xlim_2)
#ax3.set_ylim(ax3_ylim_1, ax3_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -500, 500]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax3.add_line(line)
#r1 = [ 0, 30*period]
#r2 = [ f0_m1,  f0_m1]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax3.add_line(line)





###################################################################################################
#  Panel 4: Multipole <x>
###################################################################################################

# au limits for the laser
#ax4_xlim_1 = 0.0
#ax4_xlim_2 = period_number*period
#ax4_ylim_1 = -2.7e-5
#ax4_ylim_2 =  2.7e-5

#factor = 10**(-6)

#ax4.text(ax4_xlim_1+0.02*(np.abs(ax4_xlim_2-ax4_xlim_1)), ax4_ylim_1+0.8*(np.abs(ax4_ylim_2-ax4_ylim_1)),'d)',size=18)


##### Y left label axes #########################

#ax4_y1 = ax4.twinx()

# Axes label on the left
#ax4_y1.set_ylabel(r"<x> [$\times 10^{-6}$ nm]", size=13, position=(0.0,0.50))
#ax4_y1.yaxis.set_label_position('left')
#ax4_y1.yaxis.set_label_coords(-0.080,0.55)

# labels
#ax4_y1_ticks = np.arange(-1.0e-6/factor, 1.1e-6/factor, 1.0e-6/factor)
#ax4_y1.set_yticks(ax4_y1_ticks)
#ax4_y1.set_yticklabels(ax4_y1_ticks, size=13)
#ax4_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
#ax4_y1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# limits
#ax4_y1_ylim_1 = ax4_ylim_1 * au_in_nm / factor
#ax4_y1_ylim_2 = ax4_ylim_2 * au_in_nm / factor
#ax4_y1.set_ylim(ax4_y1_ylim_1, ax4_y1_ylim_2)


##### Y right label axes ########################

#ax4_y2 = ax4.twinx()

# Axes label on the right
#ax4_y2.set_ylabel("<x> [a.u.]", size=15, position=(0.0,0.50))
#ax4_y2.yaxis.set_label_position('right')

# ticks
#ax4_y2_ticks = ax4_y1_ticks / au_in_nm
#ax4_y2.set_yticks(ax4_y2_ticks)
#ax4_y2.set_yticklabels(ax4_y2_ticks, size=13)
#ax4_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
#ax4_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax4_y2_ylim_1 = ax4_ylim_1
#ax4_y2_ylim_2 = ax4_ylim_2
#ax4_y2.set_ylim(ax4_y2_ylim_1, ax4_y2_ylim_2)


##### plot axes #################################

# Axes label bottom
#ax4.set_xlabel("time in femto seconds", size=15)
#ax4.xaxis.set_label_position('bottom')

# ticks
#ax4.set_xticks(xticks_t_femto)
#ax4.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=False, bottom=True, top=True, right=False, left=False, direction='inout', length=6)
#ax4.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
#ax4.set_xlim(ax4_xlim_1, ax4_xlim_2)
#ax4.set_ylim(ax4_ylim_1, ax4_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -500, 500]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax4.add_line(line)
#r1 = [ 0, 30*period]
#r2 = [ f0_m1,  f0_m1]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax4.add_line(line)




###################################################################################################
#  Panel 5: Multipole <x> 
###################################################################################################

# au limits for the laser
ax5_xlim_1 = 0.0
ax5_xlim_2 = period_number*period
ax5_ylim_1 = -6.5e-4
ax5_ylim_2 =  6.5e-4

factor = 10**(-6)

#ax5.text(ax5_xlim_1+0.02*(np.abs(ax5_xlim_2-ax5_xlim_1)), ax5_ylim_1+0.8*(np.abs(ax5_ylim_2-ax5_ylim_1)),'e)',size=18)


##### Y left label axes #########################

ax5_y1 = ax5.twinx()

# Axes label on the left
ax5_y1.set_ylabel(r"<x> [$\times 10^{-6}$ nm]", size=15, position=(0.0,0.5))
#ax5_y1.yaxis.set_label_position('left')
ax5_y1.yaxis.set_label_coords(-0.095,0.55)

# ticks
ax5_y1_ticks = np.arange(-2.5e-5/factor, 2.6e-5/factor, 2.5e-5/factor)
ax5_y1.set_yticks(ax5_y1_ticks)
ax5_y1.set_yticklabels(ax5_y1_ticks, size=13)
ax5_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=6)
ax5_y1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# limits
ax5_y1_ylim_1 = ax5_ylim_1 * au_in_nm / factor
ax5_y1_ylim_2 = ax5_ylim_2 * au_in_nm / factor
ax5_y1.set_ylim(ax5_y1_ylim_1, ax5_y1_ylim_2)


##### Y right label axes ########################

#ax5_y2 = ax5.twinx()

# Axes label on the right
#ax5_y2.set_ylabel("<x> [a.u.]", size=15, position=(0.0,0.5))
#ax5_y2.yaxis.set_label_position('right')

# ticks
#ax5_y2_ticks = ax5_y1_ticks / au_in_nm
#ax5_y2.set_yticks(ax5_y2_ticks)
#ax5_y2.set_yticklabels(ax5_y2_ticks)
#ax5_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=6)
#ax5_y2.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
#ax5_y2_ylim_1 = ax5_ylim_1
#ax5_y2_ylim_2 = ax5_ylim_2
#ax5_y2.set_ylim(ax5_y2_ylim_1, ax5_y2_ylim_2)


##### plot axes #################################
# Axes label bottom
ax5.set_xlabel("time in femto seconds", size=15)
ax5.xaxis.set_label_position('bottom')

# ticks
ax5.set_xticks(xticks_t_femto)
ax5.set_xticklabels(xticks_t_au)
ax5.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=True, bottom=True, top=True, right=False, left=False, direction='inout', length=6)
ax5.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
ax5.set_xlim(ax5_xlim_1, ax5_xlim_2)
ax5.set_ylim(ax5_ylim_1, ax5_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -500, 500]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax5.add_line(line)
r1 = [ 0, 30*period]
r2 = [ f0_m1,  f0_m1]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax5.add_line(line)



###################################################################################################
#  Panel 6: Multipole <x> 
###################################################################################################

# au limits for the laser
#ax6_xlim_1 = 0.0
#ax6_xlim_2 = period_number*period
#ax6_ylim_1 = -3.5e-8
#ax6_ylim_2 =  3.5e-8

#factor = 10**4

#ax6.text(ax6_xlim_1+0.02*(np.abs(ax6_xlim_2-ax6_xlim_1)), ax6_ylim_1+0.8*(np.abs(ax6_ylim_2-ax6_ylim_1)),'f)',size=18)


##### Y left label axes #########################

#ax6_y1 = ax6.twinx()

# Axes label on the left
#ax6_y1.set_ylabel(r"E$_{x}$ [$\times 10^{4}$ V/m]", size=13, position=(0.0,0.5))
#ax6_y1.yaxis.set_label_position('left')
#ax6_y1.yaxis.set_label_coords(-0.080,0.55)

# ticks
#ax6_y1_ticks = np.arange(-1.0e+4/factor, 1.1e+4/factor, 1.0e+4/factor)
#ax6_y1.set_yticks(ax6_y1_ticks)
#ax6_y1.set_yticklabels(ax6_y1_ticks, size=13)
#ax6_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=6)
#ax6_y1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))

# limits
#ax6_y1_ylim_1 = ax6_ylim_1 * au_in_V_per_m / factor
#ax6_y1_ylim_2 = ax6_ylim_2 * au_in_V_per_m / factor
#ax6_y1.set_ylim(ax6_y1_ylim_1, ax6_y1_ylim_2)


##### Y right label axes ########################

#ax6_y2 = ax6.twinx()

# Axes label on the right
#ax6_y2.set_ylabel(r"E$_{x}$ [a.u.]", size=15, position=(0.0,0.5))
#ax6_y2.yaxis.set_label_position('right')

# ticks
#ax6_y2_ticks = ax6_y1_ticks / au_in_V_per_m
#ax6_y2.set_yticks(ax6_y2_ticks)
#ax6_y2.set_yticklabels(ax6_y2_ticks)
#ax6_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=6)
#ax6_y2.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
#ax6_y2_ylim_1 = ax6_ylim_1
#ax6_y2_ylim_2 = ax6_ylim_2
#ax6_y2.set_ylim(ax6_y2_ylim_1, ax6_y2_ylim_2)


##### plot axes #################################
# Axes label bottom
#ax6.set_xlabel("time in femto seconds", size=15)
#ax6.xaxis.set_label_position('bottom')

# ticks
#ax6.set_xticks(xticks_t_femto)
#ax6.set_xticklabels(xticks_t_au)
#ax6.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=True, bottom=True, top=True, right=False, left=False, direction='inout', length=6)
#ax6.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
#ax6.set_xlim(ax6_xlim_1, ax6_xlim_2)
#ax6.set_ylim(ax6_ylim_1, ax6_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -500, 500]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax6.add_line(line)
#r1 = [ 0, 30*period]
#r2 = [ f0_m1,  f0_m1]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax6.add_line(line)



###################################################################################################
#  Legend:
###################################################################################################

ax5.legend(handles=[ ax5_data2, ax5_data1, ax5_data4, ax5_data3], prop={'size': 13}, loc=(-0.085,1.10), frameon=False, ncol=2)




###################################################################################################
#  Save figure:
###################################################################################################

DPI = fig.get_dpi()
fig.set_size_inches(1200.0/float(DPI),300.0/float(DPI))

fig.savefig('Na_297_dimer_0_5_nm_fixed_ions_multipoles__talk_2.png',bbox_inches='tight',pad_inches = 0)

#plt.show()
