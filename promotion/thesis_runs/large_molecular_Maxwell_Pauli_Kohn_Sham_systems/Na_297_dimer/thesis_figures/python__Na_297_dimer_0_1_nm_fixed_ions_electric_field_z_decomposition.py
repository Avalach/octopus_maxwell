#!/bin/bash

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from scipy.fftpack import fft, ifft
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(figsize=(1,10))

gs = gridspec.GridSpec(8, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

ax1 = plt.subplot(gs[0:2,0])
ax2 = plt.subplot(gs[2:4,0])
ax3 = plt.subplot(gs[4:6,0])
ax4 = plt.subplot(gs[6:8,0])


dt       = 0.2106550
relax    = 1065
step_0   = 107
t_offset = - relax * dt
f_offset = 0.0

cc        = 137.035999679
pi        = 3.14159265359
amplitude = 0.0001
omega     = 0.112000023380
period    = 2*pi/omega
width     = 38438.5000/cc
shift     = 38438.5000/cc 

period_number = 15




###################################################################################################
#  Laser function:
###################################################################################################

def f_laser(x,a0,x0,w,omega):
    y = []
    for ii in range(0,len(x)):
       if ( np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2))) < w ):
          y.append( a0 * np.cos(pi/2*(x[ii]-2*w-x0)/w+pi) * np.cos(omega*(x[ii]-x0)) )
       else:
          y.append(0.0)
    return y

t_laser = np.arange(0.0, period_number * period, dt)

##### Laser fourier transform ###################

w_fftl = []
for ii in range(0, len(t_laser)):
    w_fftl.append(2 * np.pi * ii / len(t_laser))

N_fftl     = int(len(t_laser)/2+1)
dt_fftl    = dt
w_max_fftl = 1/dt_fftl*2*np.pi
w_fftl     = np.linspace(0, w_max_fftl/2, N_fftl)
f_fftl     = 2*fft(f_laser(t_laser,amplitude,shift,width,omega))/N_fftl




###################################################################################################
#  Dataset 1:
###################################################################################################

with open('../366_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez1:
    readlines = ez1.readlines()
    t_ez1 = [line.split()[0] for line in readlines]
    f_ez1 = [line.split()[1] for line in readlines]
f0_ez1 = f_ez1[step_0]
for ii in range(0, len(t_ez1)):
    t_ez1[ii] = float(t_ez1[ii]) + t_offset
    f_ez1[ii] = float(f_ez1[ii]) 


##### Fourier transform #########################

w_fftez1 = []
for ii in range(0, len(t_ez1)):
   w_fftez1.append(2 * np.pi * ii / len(t_ez1))

N_fftez1     = int(len(t_ez1)/2+1)
dt_fftez1    = t_ez1[2]-t_ez1[1]
w_max_fftez1 = 1/dt_fftez1*2*np.pi
w_fftez1     = np.linspace(0, w_max_fftez1/2, N_fftez1)
f_fftez1     = 2*fft(f_ez1)/N_fftez1




###################################################################################################
#  Dataset 2:
###################################################################################################

with open('../367_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez2:
    readlines = ez2.readlines()
    t_ez2 = [line.split()[0] for line in readlines]
    f_ez2 = [line.split()[1] for line in readlines]
f0_ez2 = f_ez2[step_0]
for ii in range(0, len(t_ez2)):
    t_ez2[ii] = float(t_ez2[ii]) + t_offset
    f_ez2[ii] = float(f_ez2[ii]) 


##### Fourier transform #########################

w_fftez2 = []
for ii in range(0, len(t_ez2)):
   w_fftez2.append(2 * np.pi * ii / len(t_ez2))

N_fftez2     = int(len(t_ez2)/2+1)
dt_fftez2    = t_ez2[2]-t_ez2[1]
w_max_fftez2 = 1/dt_fftez2*2*np.pi
w_fftez2     = np.linspace(0, w_max_fftez2/2, N_fftez2)
f_fftez2     = 2*fft(f_ez2)/N_fftez2




###################################################################################################
#  Dataset 3:
###################################################################################################

with open('../368_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez3:
    readlines = ez3.readlines()
    t_ez3 = [line.split()[0] for line in readlines]
    f_ez3 = [line.split()[1] for line in readlines]
f0_ez3 = f_ez3[step_0]
for ii in range(0, len(t_ez3)):
    t_ez3[ii] = float(t_ez3[ii]) + t_offset
    f_ez3[ii] = float(f_ez3[ii]) - float(f0_ez3)


##### Fourier transform #########################

w_fftez3 = []
for ii in range(0, len(t_ez3)):
   w_fftez3.append(2 * np.pi * ii / len(t_ez3))

N_fftez3     = int(len(t_ez3)/2+1)
dt_fftez3    = t_ez3[2]-t_ez3[1]
w_max_fftez3 = 1/dt_fftez3*2*np.pi
w_fftez3     = np.linspace(0, w_max_fftez3/2, N_fftez3)
f_fftez3     = 2*fft(f_ez3)/N_fftez3




###################################################################################################
#  Dataset 4:
###################################################################################################

with open('../369_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez4:
    readlines = ez4.readlines()
    t_ez4 = [line.split()[0] for line in readlines]
    f_ez4 = [line.split()[1] for line in readlines]
f0_ez4 = f_ez4[step_0]
for ii in range(0, len(t_ez4)):
    t_ez4[ii] = float(t_ez4[ii]) + t_offset
    f_ez4[ii] = float(f_ez4[ii]) 


##### Fourier transform #########################

w_fftez4 = []
for ii in range(0, len(t_ez4)):
   w_fftez4.append(2 * np.pi * ii / len(t_ez4))

N_fftez4     = int(len(t_ez4)/2+1)
dt_fftez4    = t_ez4[2]-t_ez4[1]
w_max_fftez4 = 1/dt_fftez4*2*np.pi
w_fftez4     = np.linspace(0, w_max_fftez4/2, N_fftez4)
f_fftez4     = 2*fft(f_ez4)/N_fftez4




###################################################################################################
#  Dataset 5:
###################################################################################################

with open('../366_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez5:
    readlines = ez5.readlines()
    t_ez5 = [line.split()[0] for line in readlines]
    f_ez5 = [line.split()[1] for line in readlines]
f0_ez5 = f_ez5[step_0]
for ii in range(0, len(t_ez5)):
    t_ez5[ii] = float(t_ez5[ii]) + t_offset
    f_ez5[ii] = float(f_ez5[ii]) 


##### Fourier transform #########################

w_fftez5 = []
for ii in range(0, len(t_ez5)):
   w_fftez5.append(2 * np.pi * ii / len(t_ez5))

N_fftez5     = int(len(t_ez5)/2+1)
dt_fftez5    = t_ez5[2]-t_ez5[1]
w_max_fftez5 = 1/dt_fftez5*2*np.pi
w_fftez5     = np.linspace(0, w_max_fftez5/2, N_fftez5)
f_fftez5     = 2*fft(f_ez5)/N_fftez5




###################################################################################################
#  Dataset 6:
###################################################################################################

with open('../367_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez6:
    readlines = ez6.readlines()
    t_ez6 = [line.split()[0] for line in readlines]
    f_ez6 = [line.split()[1] for line in readlines]
f0_ez6 = f_ez6[step_0]
for ii in range(0, len(t_ez6)):
    t_ez6[ii] = float(t_ez6[ii]) + t_offset
    f_ez6[ii] = float(f_ez6[ii]) 


##### Fourier transform #########################

w_fftez6 = []
for ii in range(0, len(t_ez6)):
   w_fftez6.append(2 * np.pi * ii / len(t_ez6))

N_fftez6     = int(len(t_ez6)/2+1)
dt_fftez6    = t_ez6[2]-t_ez6[1]
w_max_fftez6 = 1/dt_fftez6*2*np.pi
w_fftez6     = np.linspace(0, w_max_fftez6/2, N_fftez6)
f_fftez6     = 2*fft(f_ez6)/N_fftez6




###################################################################################################
#  Dataset 7:
###################################################################################################

with open('../368_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez7:
    readlines = ez7.readlines()
    t_ez7 = [line.split()[0] for line in readlines]
    f_ez7 = [line.split()[1] for line in readlines]
f0_ez7 = f_ez7[step_0]
for ii in range(0, len(t_ez7)):
    t_ez7[ii] = float(t_ez7[ii]) + t_offset
    f_ez7[ii] = float(f_ez7[ii]) 


##### Fourier transform #########################

w_fftez7 = []
for ii in range(0, len(t_ez7)):
   w_fftez7.append(2 * np.pi * ii / len(t_ez7))

N_fftez7     = int(len(t_ez7)/2+1)
dt_fftez7    = t_ez7[2]-t_ez7[1]
w_max_fftez7 = 1/dt_fftez7*2*np.pi
w_fftez7     = np.linspace(0, w_max_fftez7/2, N_fftez7)
f_fftez7     = 2*fft(f_ez7)/N_fftez7




###################################################################################################
#  Dataset 8:
###################################################################################################

with open('../369_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez8:
    readlines = ez8.readlines()
    t_ez8 = [line.split()[0] for line in readlines]
    f_ez8 = [line.split()[1] for line in readlines]
f0_ez8 = f_ez8[step_0]
for ii in range(0, len(t_ez8)):
    t_ez8[ii] = float(t_ez8[ii]) + t_offset
    f_ez8[ii] = float(f_ez8[ii]) 


##### Fourier transform #########################

w_fftez8 = []
for ii in range(0, len(t_ez8)):
   w_fftez8.append(2 * np.pi * ii / len(t_ez8))

N_fftez8     = int(len(t_ez8)/2+1)
dt_fftez8    = t_ez8[2]-t_ez8[1]
w_max_fftez8 = 1/dt_fftez8*2*np.pi
w_fftez8     = np.linspace(0, w_max_fftez8/2, N_fftez8)
f_fftez8     = 2*fft(f_ez8)/N_fftez8




###################################################################################################
#  Dataset 9:
###################################################################################################

with open('../366_cobra/td.general/maxwell_e_field_trans_z_at_00_00_00') as ez9:
    readlines = ez9.readlines()
    t_ez9 = [line.split()[0] for line in readlines]
    f_ez9 = [line.split()[1] for line in readlines]
f0_ez9 = f_ez9[step_0]
for ii in range(0, len(t_ez9)):
    t_ez9[ii] = float(t_ez9[ii]) + t_offset
    f_ez9[ii] = float(f_ez9[ii]) 


##### Fourier transform #########################

w_fftez9 = []
for ii in range(0, len(t_ez9)):
   w_fftez9.append(2 * np.pi * ii / len(t_ez9))

N_fftez9     = int(len(t_ez9)/2+1)
dt_fftez9    = t_ez9[2]-t_ez9[1]
w_max_fftez9 = 1/dt_fftez9*2*np.pi
w_fftez9     = np.linspace(0, w_max_fftez9/2, N_fftez9)
f_fftez9     = 2*fft(f_ez9)/N_fftez9




###################################################################################################
#  Dataset 10:
###################################################################################################

with open('../367_cobra/td.general/maxwell_e_field_trans_z_at_00_00_00') as ez10:
    readlines = ez10.readlines()
    t_ez10 = [line.split()[0] for line in readlines]
    f_ez10 = [line.split()[1] for line in readlines]
f0_ez10 = f_ez10[step_0]
for ii in range(0, len(t_ez10)):
    t_ez10[ii] = float(t_ez10[ii]) + t_offset
    f_ez10[ii] = float(f_ez10[ii]) 


##### Fourier transform #########################

w_fftez10 = []
for ii in range(0, len(t_ez10)):
   w_fftez10.append(2 * np.pi * ii / len(t_ez10))

N_fftez10     = int(len(t_ez10)/2+1)
dt_fftez10    = t_ez10[2]-t_ez10[1]
w_max_fftez10 = 1/dt_fftez10*2*np.pi
w_fftez10     = np.linspace(0, w_max_fftez10/2, N_fftez10)
f_fftez10     = 2*fft(f_ez10)/N_fftez10




###################################################################################################
#  Dataset 11:
###################################################################################################

with open('../368_cobra/td.general/maxwell_e_field_trans_z_at_00_00_00') as ez11:
    readlines = ez11.readlines()
    t_ez11 = [line.split()[0] for line in readlines]
    f_ez11 = [line.split()[1] for line in readlines]
f0_ez11 = f_ez11[step_0]
for ii in range(0, len(t_ez11)):
    t_ez11[ii] = float(t_ez11[ii]) + t_offset
    f_ez11[ii] = float(f_ez11[ii]) 


##### Fourier transform #########################

w_fftez11 = []
for ii in range(0, len(t_ez11)):
   w_fftez11.append(2 * np.pi * ii / len(t_ez11))

N_fftez11     = int(len(t_ez11)/2+1)
dt_fftez11    = t_ez11[2]-t_ez11[1]
w_max_fftez11 = 1/dt_fftez11*2*np.pi
w_fftez11     = np.linspace(0, w_max_fftez11/2, N_fftez11)
f_fftez11     = 2*fft(f_ez11)/N_fftez11




###################################################################################################
#  Dataset 12:
###################################################################################################

with open('../369_cobra/td.general/maxwell_e_field_trans_z_at_00_00_00') as ez12:
    readlines = ez12.readlines()
    t_ez12 = [line.split()[0] for line in readlines]
    f_ez12 = [line.split()[1] for line in readlines]
f0_ez12 = f_ez12[step_0]
for ii in range(0, len(t_ez12)):
    t_ez12[ii] = float(t_ez12[ii]) + t_offset
    f_ez12[ii] = float(f_ez12[ii]) 


##### Fourier transform #########################

w_fftez12 = []
for ii in range(0, len(t_ez12)):
   w_fftez12.append(2 * np.pi * ii / len(t_ez12))

N_fftez12     = int(len(t_ez12)/2+1)
dt_fftez12    = t_ez12[2]-t_ez12[1]
w_max_fftez12 = 1/dt_fftez12*2*np.pi
w_fftez12     = np.linspace(0, w_max_fftez12/2, N_fftez12)
f_fftez12     = 2*fft(f_ez12)/N_fftez12




###################################################################################################
#  Dataset 13:
###################################################################################################

with open('../366_cobra/td.general/maxwell_e_field_trans_z_at_p37_00_00') as ez13:
    readlines = ez13.readlines()
    t_ez13 = [line.split()[0] for line in readlines]
    f_ez13 = [line.split()[1] for line in readlines]
f0_ez13 = f_ez13[step_0]
for ii in range(0, len(t_ez13)):
    t_ez13[ii] = float(t_ez13[ii]) + t_offset
    f_ez13[ii] = float(f_ez13[ii]) 


##### Fourier transform #########################

w_fftez13 = []
for ii in range(0, len(t_ez13)):
   w_fftez13.append(2 * np.pi * ii / len(t_ez13))

N_fftez13     = int(len(t_ez13)/2+1)
dt_fftez13    = t_ez13[2]-t_ez13[1]
w_max_fftez13 = 1/dt_fftez13*2*np.pi
w_fftez13     = np.linspace(0, w_max_fftez13/2, N_fftez13)
f_fftez13     = 2*fft(f_ez13)/N_fftez13



###################################################################################################
#  Dataset 14:
###################################################################################################

with open('../367_cobra/td.general/maxwell_e_field_trans_z_at_p37_00_00') as ez14:
    readlines = ez14.readlines()
    t_ez14 = [line.split()[0] for line in readlines]
    f_ez14 = [line.split()[1] for line in readlines]
f0_ez14 = f_ez14[step_0]
for ii in range(0, len(t_ez14)):
    t_ez14[ii] = float(t_ez14[ii]) + t_offset
    f_ez14[ii] = float(f_ez14[ii]) 


##### Fourier transform #########################

w_fftez14 = []
for ii in range(0, len(t_ez14)):
   w_fftez14.append(2 * np.pi * ii / len(t_ez14))

N_fftez14     = int(len(t_ez14)/2+1)
dt_fftez14    = t_ez14[2]-t_ez14[1]
w_max_fftez14 = 1/dt_fftez14*2*np.pi
w_fftez14     = np.linspace(0, w_max_fftez14/2, N_fftez14)
f_fftez14     = 2*fft(f_ez14)/N_fftez14




###################################################################################################
#  Dataset 15:
###################################################################################################

with open('../368_cobra/td.general/maxwell_e_field_trans_z_at_p37_00_00') as ez15:
    readlines = ez15.readlines()
    t_ez15 = [line.split()[0] for line in readlines]
    f_ez15 = [line.split()[1] for line in readlines]
f0_ez15 = f_ez15[step_0]
for ii in range(0, len(t_ez15)):
    t_ez15[ii] = float(t_ez15[ii]) + t_offset
    f_ez15[ii] = float(f_ez15[ii]) 


##### Fourier transform #########################

w_fftez15 = []
for ii in range(0, len(t_ez15)):
   w_fftez15.append(2 * np.pi * ii / len(t_ez15))

N_fftez15     = int(len(t_ez15)/2+1)
dt_fftez15    = t_ez15[2]-t_ez15[1]
w_max_fftez15 = 1/dt_fftez15*2*np.pi
w_fftez15     = np.linspace(0, w_max_fftez15/2, N_fftez15)
f_fftez15     = 2*fft(f_ez15)/N_fftez15



###################################################################################################
#  Dataset 16:
###################################################################################################

with open('../369_cobra/td.general/maxwell_e_field_trans_z_at_p37_00_00') as ez16:
    readlines = ez16.readlines()
    t_ez16 = [line.split()[0] for line in readlines]
    f_ez16 = [line.split()[1] for line in readlines]
f0_ez16 = f_ez16[step_0]
for ii in range(0, len(t_ez16)):
    t_ez16[ii] = float(t_ez16[ii]) + t_offset
    f_ez16[ii] = float(f_ez16[ii]) 


##### Fourier transform #########################

w_fftez16 = []
for ii in range(0, len(t_ez16)):
   w_fftez16.append(2 * np.pi * ii / len(t_ez16))

N_fftez16     = int(len(t_ez16)/2+1)
dt_fftez16    = t_ez16[2]-t_ez16[1]
w_max_fftez16 = 1/dt_fftez16*2*np.pi
w_fftez16     = np.linspace(0, w_max_fftez16/2, N_fftez16)
f_fftez16     = 2*fft(f_ez16)/N_fftez16




###################################################################################################
#  Dataset 17:
###################################################################################################

with open('../366_cobra/td.general/maxwell_e_field_long_z_at_00_00_00') as ez17:
    readlines = ez17.readlines()
    t_ez17 = [line.split()[0] for line in readlines]
    f_ez17 = [line.split()[1] for line in readlines]
f0_ez17 = f_ez17[step_0]
for ii in range(0, len(t_ez17)):
    t_ez17[ii] = float(t_ez17[ii]) + t_offset
    f_ez17[ii] = float(f_ez17[ii]) 


##### Fourier transform #########################

w_fftez17 = []
for ii in range(0, len(t_ez17)):
   w_fftez17.append(2 * np.pi * ii / len(t_ez17))

N_fftez17     = int(len(t_ez17)/2+1)
dt_fftez17    = t_ez17[2]-t_ez17[1]
w_max_fftez17 = 1/dt_fftez17*2*np.pi
w_fftez17     = np.linspace(0, w_max_fftez17/2, N_fftez17)
f_fftez17     = 2*fft(f_ez17)/N_fftez17




###################################################################################################
#  Dataset 18:
###################################################################################################

with open('../367_cobra/td.general/maxwell_e_field_long_z_at_00_00_00') as ez18:
    readlines = ez18.readlines()
    t_ez18 = [line.split()[0] for line in readlines]
    f_ez18 = [line.split()[1] for line in readlines]
f0_ez18 = f_ez18[step_0]
for ii in range(0, len(t_ez18)):
    t_ez18[ii] = float(t_ez18[ii]) + t_offset
    f_ez18[ii] = float(f_ez18[ii]) 


##### Fourier transform #########################

w_fftez18 = []
for ii in range(0, len(t_ez18)):
   w_fftez18.append(2 * np.pi * ii / len(t_ez18))

N_fftez18     = int(len(t_ez18)/2+1)
dt_fftez18    = t_ez18[2]-t_ez18[1]
w_max_fftez18 = 1/dt_fftez18*2*np.pi
w_fftez18     = np.linspace(0, w_max_fftez18/2, N_fftez18)
f_fftez18     = 2*fft(f_ez18)/N_fftez18




###################################################################################################
#  Dataset 19:
###################################################################################################

with open('../368_cobra/td.general/maxwell_e_field_long_z_at_00_00_00') as ez19:
    readlines = ez19.readlines()
    t_ez19 = [line.split()[0] for line in readlines]
    f_ez19 = [line.split()[1] for line in readlines]
f0_ez19 = f_ez19[step_0]
for ii in range(0, len(t_ez19)):
    t_ez19[ii] = float(t_ez19[ii]) + t_offset
    f_ez19[ii] = float(f_ez19[ii]) 


##### Fourier transform #########################

w_fftez19 = []
for ii in range(0, len(t_ez19)):
   w_fftez19.append(2 * np.pi * ii / len(t_ez19))

N_fftez19     = int(len(t_ez19)/2+1)
dt_fftez19    = t_ez19[2]-t_ez19[1]
w_max_fftez19 = 1/dt_fftez19*2*np.pi
w_fftez19     = np.linspace(0, w_max_fftez19/2, N_fftez19)
f_fftez19     = 2*fft(f_ez19)/N_fftez19




###################################################################################################
#  Dataset 20:
###################################################################################################

with open('../369_cobra/td.general/maxwell_e_field_long_z_at_00_00_00') as ez20:
    readlines = ez20.readlines()
    t_ez20 = [line.split()[0] for line in readlines]
    f_ez20 = [line.split()[1] for line in readlines]
f0_ez20 = f_ez20[step_0]
for ii in range(0, len(t_ez20)):
    t_ez20[ii] = float(t_ez20[ii]) + t_offset
    f_ez20[ii] = float(f_ez20[ii]) 


##### Fourier transform #########################

w_fftez20 = []
for ii in range(0, len(t_ez20)):
   w_fftez20.append(2 * np.pi * ii / len(t_ez20))

N_fftez20     = int(len(t_ez20)/2+1)
dt_fftez20    = t_ez20[2]-t_ez20[1]
w_max_fftez20 = 1/dt_fftez20*2*np.pi
w_fftez20     = np.linspace(0, w_max_fftez20/2, N_fftez20)
f_fftez20     = 2*fft(f_ez20)/N_fftez20




###################################################################################################
#  Dataset 21:
###################################################################################################

with open('../366_cobra/td.general/maxwell_e_field_long_z_at_p37_00_00') as ez21:
    readlines = ez21.readlines()
    t_ez21 = [line.split()[0] for line in readlines]
    f_ez21 = [line.split()[1] for line in readlines]
f0_ez21 = f_ez21[step_0]
for ii in range(0, len(t_ez21)):
    t_ez21[ii] = float(t_ez21[ii]) + t_offset
    f_ez21[ii] = float(f_ez21[ii]) 


##### Fourier transform #########################

w_fftez21 = []
for ii in range(0, len(t_ez21)):
   w_fftez21.append(2 * np.pi * ii / len(t_ez21))

N_fftez21     = int(len(t_ez21)/2+1)
dt_fftez21    = t_ez21[2]-t_ez21[1]
w_max_fftez21 = 1/dt_fftez21*2*np.pi
w_fftez21     = np.linspace(0, w_max_fftez21/2, N_fftez21)
f_fftez21     = 2*fft(f_ez21)/N_fftez21




###################################################################################################
#  Dataset 22:
###################################################################################################

with open('../367_cobra/td.general/maxwell_e_field_long_z_at_p37_00_00') as ez22:
    readlines = ez22.readlines()
    t_ez22 = [line.split()[0] for line in readlines]
    f_ez22 = [line.split()[1] for line in readlines]
f0_ez22 = f_ez22[step_0]
for ii in range(0, len(t_ez22)):
    t_ez22[ii] = float(t_ez22[ii]) + t_offset
    f_ez22[ii] = float(f_ez22[ii]) 


##### Fourier transform #########################

w_fftez22 = []
for ii in range(0, len(t_ez22)):
   w_fftez22.append(2 * np.pi * ii / len(t_ez22))

N_fftez22     = int(len(t_ez22)/2+1)
dt_fftez22    = t_ez22[2]-t_ez22[1]
w_max_fftez22 = 1/dt_fftez22*2*np.pi
w_fftez22     = np.linspace(0, w_max_fftez22/2, N_fftez22)
f_fftez22     = 2*fft(f_ez22)/N_fftez22




###################################################################################################
#  Dataset 23:
###################################################################################################

with open('../368_cobra/td.general/maxwell_e_field_long_z_at_p37_00_00') as ez23:
    readlines = ez23.readlines()
    t_ez23 = [line.split()[0] for line in readlines]
    f_ez23 = [line.split()[1] for line in readlines]
f0_ez23 = f_ez23[step_0]
for ii in range(0, len(t_ez23)):
    t_ez23[ii] = float(t_ez23[ii]) + t_offset
    f_ez23[ii] = float(f_ez23[ii]) 


##### Fourier transform #########################

w_fftez23 = []
for ii in range(0, len(t_ez23)):
   w_fftez23.append(2 * np.pi * ii / len(t_ez23))

N_fftez23     = int(len(t_ez23)/2+1)
dt_fftez23    = t_ez23[2]-t_ez23[1]
w_max_fftez23 = 1/dt_fftez23*2*np.pi
w_fftez23     = np.linspace(0, w_max_fftez23/2, N_fftez23)
f_fftez23     = 2*fft(f_ez23)/N_fftez23




###################################################################################################
#  Dataset 24:
###################################################################################################

with open('../369_cobra/td.general/maxwell_e_field_long_z_at_p37_00_00') as ez24:
    readlines = ez24.readlines()
    t_ez24 = [line.split()[0] for line in readlines]
    f_ez24 = [line.split()[1] for line in readlines]
f0_ez24 = f_ez24[step_0]
for ii in range(0, len(t_ez24)):
    t_ez24[ii] = float(t_ez24[ii]) + t_offset
    f_ez24[ii] = float(f_ez24[ii]) 


##### Fourier transform #########################

w_fftez24 = []
for ii in range(0, len(t_ez24)):
   w_fftez24.append(2 * np.pi * ii / len(t_ez20))

N_fftez20     = int(len(t_ez20)/2+1)
dt_fftez20    = t_ez20[2]-t_ez20[1]
w_max_fftez20 = 1/dt_fftez20*2*np.pi
w_fftez20     = np.linspace(0, w_max_fftez20/2, N_fftez20)
f_fftez20     = 2*fft(f_ez20)/N_fftez20




###################################################################################################
#  Plots:
###################################################################################################

fig.suptitle(r"Decomposition of the electric field in z-direction for d$_1$=0.1nm",x=0.5,y=1.06, size=20)

##### Panel 1 plot ##############################
ax1_datal,  = ax1.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), '-', color=(0.100,0.100,0.100), linewidth=1.5, label='total external laser')
ax1_datall,  = ax1.plot(t_laser,f_laser(t_laser,amplitude,shift,width,omega), color=(0.100,0.100,0.100), linewidth=2.5, dashes=(2.5,2.0), label='longitudinal external laser')
ax1_datalt,  = ax1.plot(t_laser,f_laser(t_laser,amplitude,shift,width,omega), color=(0.100,0.100,0.100), linewidth=2.5, dashes=(1.5,1.0), label='transverse external laser')
ax1_data2,  = ax1.plot(t_ez2,  f_ez2,  '-', color=(0.000,0.000,0.700), linewidth=1.5, label='total F@ED')
ax1_data1,  = ax1.plot(t_ez1,  f_ez1,  '-', color=(0.000,0.700,0.000), linewidth=1.5, label='total FB@ED')
#ax1_data18l, = ax1.plot(t_ez18, f_ez18, color=(0.000,0.000,0.700), linewidth=2.5, dashes=(2.5,2.0), label='longitudinal F@ED')
ax1_data18, = ax1.plot(t_ez18, f_ez18, color=(0.000,0.000,0.700), linewidth=2.5, dashes=(2.5,2.0), label='longitudinal F@ED')
#ax1_data17l, = ax1.plot(t_ez17, f_ez17, color=(0.000,0.700,0.000), linewidth=2.5, dashes=(2.5,2.0), label='longitudinal FB@ED')
ax1_data17, = ax1.plot(t_ez17, f_ez17, color=(0.000,0.700,0.000), linewidth=2.5, dashes=(2.5,2.0), label='longitudinal FB@ED')
 

##### Panel 2 plot ##############################
#ax2_datalt,  = ax2.plot(t_laser,f_laser(t_laser,amplitude,shift,width,omega), color=(0.100,0.100,0.100), linewidth=1.5, dashes=(1.5,1.0), label='transverse external laser')
ax2_data10, = ax2.plot(t_ez10,  f_ez10, color=(0.000,0.000,0.700), linewidth=2.5, dashes=(1.5,1.0), label='transverse F@ED')
ax2_data9,  = ax2.plot(t_ez9,   f_ez9,  color=(0.000,0.700,0.000), linewidth=2.5, dashes=(1.5,1.0), label='transverse FB@ED')


##### Panel 3 plot ##############################
ax3_datal,  = ax3.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), '-', color=(0.100,0.100,0.100), linewidth=1.5, label='external laser')
ax3_data6,  = ax3.plot(t_ez6,  f_ez6, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='total F@ED')
ax3_data5,  = ax3.plot(t_ez5,  f_ez5, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='total FB@ED')
ax3_data22, = ax3.plot(t_ez22, f_ez22, '-.', color=(0.000,0.000,0.700), linewidth=2.5, dashes=(2.5,2.0), label='longitudinal F@ED')
ax3_data21, = ax3.plot(t_ez21, f_ez21, '-.', color=(0.000,0.700,0.000), linewidth=2.5, dashes=(2.5,2.0), label='longitudinal FB@ED')


##### Panel 2 plot ##############################
#ax4_datalt,  = ax4.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), color=(0.100,0.100,0.100), linewidth=1.5, dashes=(1.5,1.0), label='external laser')
ax4_data10, = ax4.plot(t_ez14,  f_ez14, color=(0.000,0.000,0.700), linewidth=2.5, dashes=(1.5,1.0), label='transverse F@ED')
ax4_data9,  = ax4.plot(t_ez13,  f_ez13, color=(0.000,0.700,0.000), linewidth=2.5, dashes=(1.5,1.0), label='transverse FB@ED')




###################################################################################################
#  Units transformation:
###################################################################################################

# Femtosecond to atomic units factor
femto_in_au    = 103.353

# energy from atomic untis to eV
au_in_eV       = 27.2114

# electric field from atomic units to V/m
au_in_V_per_m = 5.14220652*(10**11)

# current from atomic unts to A
au_in_A = 6.623618183*(10**(-3))

# x-tick interval
femto_interval = 2.5

# x-ticks for all plots
xticks_femto      = np.arange(0, period_number*period, femto_in_au)
xticklabels_femto = np.arange(0, period_number*period/femto_in_au*femto_interval, femto_interval)  

# x-tick-labels for all plots
yticks_eV       = np.arange(-142.905, -142.88, 0.005)
yticklabels_eV  = np.arange(-3888.65, -3887.96, 0.05, dtype=float)




###################################################################################################
#  Panel 1: Total field and longitudinal near field
###################################################################################################

# Axes label top
ax1.set_xlabel("time in atomic units", size=18)
ax1.xaxis.set_label_coords(0.5,1.20)
ax1.xaxis.set_label_position('top')

# au limits for the laser
ax1_xlim_1 = 0.0
ax1_xlim_2 = period_number*period
ax1_ylim_1 = -4.40e-4
ax1_ylim_2 =  4.40e-4

ax1.text(ax1_xlim_1+0.02*(np.abs(ax1_xlim_2-ax1_xlim_1)), ax1_ylim_1+0.8*(np.abs(ax1_ylim_2-ax1_ylim_1)),'a)',size=18)


##### Y left label axes #########################

ax1_y1 = ax1.twinx()

# Axes label on the left
ax1_y1.set_ylabel(r'E$_{z}$ [V/m]', size=18, position=(0.0,0.55))
ax1_y1.yaxis.set_label_position('left')

# ticks
ax1_y1_ticks = np.arange(-2.0*10**8, 2.1*10**8, 1*10**8)
ax1_y1.set_yticks(ax1_y1_ticks)
ax1_y1.set_yticklabels(ax1_y1_ticks, size=13)
ax1_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
ax1_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax1_y1_ylim_1 = ax1_ylim_1 * au_in_V_per_m
ax1_y1_ylim_2 = ax1_ylim_2 * au_in_V_per_m
ax1_y1.set_ylim(ax1_y1_ylim_1, ax1_y1_ylim_2)


##### Y right label axes ########################

ax1_y2 = ax1.twinx()

# Axes label on the right
ax1_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
ax1_y2.yaxis.set_label_position('right')

# ticks
ax1_y2_ticks = ax1_y1_ticks/au_in_V_per_m
ax1_y2.set_yticks(ax1_y2_ticks)
ax1_y2.set_yticklabels(ax1_y2_ticks, size=13)
ax1_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
ax1_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax1_y2_ylim_1 = ax1_ylim_1
ax1_y2_ylim_2 = ax1_ylim_2
ax1_y2.set_ylim(ax1_y2_ylim_1, ax1_y2_ylim_2)


##### plot axes #################################

# labels
ax1.set_xticks(xticks_femto)
ax1.set_xticklabels(xticks_femto, size=13)
ax1.set_xlim(ax1_xlim_1, ax1_xlim_2)
ax1.set_ylim(ax1_ylim_1, ax1_ylim_2)
ax1.tick_params(labelleft=False, labelright=False, labeltop=True, labelbottom=False, bottom=True, top=True, right=False, left=False, direction='inout', length=8)
ax1.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax1.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax1.add_line(line)




###################################################################################################
#  Panel 2: Transverse field
###################################################################################################

# au limits for the laser
ax2_xlim_1 = 0.0
ax2_xlim_2 = period_number*period
ax2_ylim_1 = -1.10e-4
ax2_ylim_2 =  1.10e-4

ax2.text(ax2_xlim_1+0.02*(np.abs(ax2_xlim_2-ax2_xlim_1)), ax2_ylim_1+0.8*(np.abs(ax2_ylim_2-ax2_ylim_1)),'b)',size=18)


##### Y left label axes #########################

ax2_y1 = ax2.twinx()

# Axes label on the left
ax2_y1.set_ylabel(r"E$_z$ [V/m]", size=15, position=(0.0,0.55))
ax2_y1.yaxis.set_label_position('left')

# labels
ax2_y1_ticks = np.arange(-4.0*10**7, 4.1*10**7, 2.0*10**7)
ax2_y1.set_yticks(ax2_y1_ticks)
ax2_y1.set_yticklabels(ax2_y1_ticks, size=13)
ax2_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
ax2_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax2_y1_ylim_1 = ax2_ylim_1 * au_in_V_per_m
ax2_y1_ylim_2 = ax2_ylim_2 * au_in_V_per_m
ax2_y1.set_ylim(ax2_y1_ylim_1, ax2_y1_ylim_2)


##### Y right label axes ########################

ax2_y2 = ax2.twinx()

# Axes label on the right
ax2_y2.set_ylabel(r"E$_z$ [a.u.]", size=15, position=(0.0,0.55))
ax2_y2.yaxis.set_label_position('right')

# ticks
ax2_y2_ticks = ax2_y1_ticks/au_in_V_per_m
ax2_y2.set_yticks(ax2_y2_ticks)
ax2_y2.set_yticklabels(ax2_y2_ticks, size=13)
ax2_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
ax2_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax2_y2_ylim_1 = ax2_ylim_1
ax2_y2_ylim_2 = ax2_ylim_2
ax2_y2.set_ylim(ax2_y2_ylim_1, ax2_y2_ylim_2)


##### plot axes #################################

# labels
ax2.set_xticks(xticks_femto)
ax2.set_xlim(ax2_xlim_1, ax2_xlim_2)
ax2.set_ylim(ax2_ylim_1, ax2_ylim_2)
ax2.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=False, bottom=True, top=True, right=False, left=False, direction='inout', length=8)
ax2.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax2.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax2.add_line(line)




###################################################################################################
#  Panel 3: Total field and longitudinal far field
###################################################################################################

# Axes label top
#ax3.set_xlabel("time in atomic units", size=15)
#ax3.xaxis.set_label_coords(0.5,1.20)
#ax3.xaxis.set_label_position('top')

# au limits for the laser
ax3_xlim_1 = 0.0
ax3_xlim_2 = period_number*period
ax3_ylim_1 = -2.40e-4
ax3_ylim_2 =  2.40e-4

ax3.text(ax3_xlim_1+0.02*(np.abs(ax3_xlim_2-ax3_xlim_1)), ax3_ylim_1+0.8*(np.abs(ax3_ylim_2-ax3_ylim_1)),'c)',size=18)

##### Y left label axes #########################

ax3_y1 = ax3.twinx()

# Axes label on the left
ax3_y1.set_ylabel(r'E$_{z}$ [V/m]', size=15, position=(0.0,0.55))
ax3_y1.yaxis.set_label_position('left')

# ticks
ax3_y1_ticks = np.arange(-2.0*10**8, 2.1*10**8, 0.5*10**8)
ax3_y1.set_yticks(ax3_y1_ticks)
ax3_y1.set_yticklabels(ax3_y1_ticks, size=13)
ax3_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
ax3_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax3_y1_ylim_1 = ax3_ylim_1 * au_in_V_per_m
ax3_y1_ylim_2 = ax3_ylim_2 * au_in_V_per_m
ax3_y1.set_ylim(ax3_y1_ylim_1, ax3_y1_ylim_2)


##### Y right label axes ########################

ax3_y2 = ax3.twinx()

# Axes label on the right
ax3_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
ax3_y2.yaxis.set_label_position('right')

# ticks
ax3_y2_ticks = ax3_y1_ticks/au_in_V_per_m
ax3_y2.set_yticks(ax3_y2_ticks)
ax3_y2.set_yticklabels(ax3_y2_ticks, size=13)
ax3_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
ax3_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax3_y2_ylim_1 = ax3_ylim_1
ax3_y2_ylim_2 = ax3_ylim_2
ax3_y2.set_ylim(ax3_y2_ylim_1, ax3_y2_ylim_2)


##### plot axes #################################

# labels
ax3.set_xticks(xticks_femto)
ax3.set_xticklabels(xticks_femto, size=13)
ax3.set_xlim(ax3_xlim_1, ax3_xlim_2)
ax3.set_ylim(ax3_ylim_1, ax3_ylim_2)
ax3.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=False, bottom=True, top=True, right=False, left=False, direction='inout', length=8)
ax3.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax3.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax3.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax3.add_line(line)




###################################################################################################
#  Panel 4: Transverse far field
###################################################################################################

# au limits for the laser
ax4_xlim_1 = 0.0
ax4_xlim_2 = period_number*period
ax4_ylim_1 = -1.10e-4
ax4_ylim_2 =  1.10e-4

ax4.text(ax4_xlim_1+0.02*(np.abs(ax4_xlim_2-ax4_xlim_1)), ax4_ylim_1+0.8*(np.abs(ax4_ylim_2-ax4_ylim_1)),'d)',size=18)


##### Y left label axes #########################

ax4_y1 = ax4.twinx()

# Axes label on the left
ax4_y1.set_ylabel(r"E$_z$ [V/m]", size=15, position=(0.0,0.55))
ax4_y1.yaxis.set_label_position('left')

# labels
ax4_y1_ticks = np.arange(-4.0*10**7, 4.1*10**7, 2.0*10**7)
ax4_y1.set_yticks(ax4_y1_ticks)
ax4_y1.set_yticklabels(ax4_y1_ticks, size=13)
ax4_y1.tick_params(labelleft=True, labelright=False, labeltop=False, labelbottom=False, bottom=False, top=False, right=True, left=True, direction='inout', length=8)
ax4_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax4_y1_ylim_1 = ax4_ylim_1 * au_in_V_per_m
ax4_y1_ylim_2 = ax4_ylim_2 * au_in_V_per_m
ax4_y1.set_ylim(ax4_y1_ylim_1, ax4_y1_ylim_2)


##### Y right label axes ########################

ax4_y2 = ax4.twinx()

# Axes label on the right
ax4_y2.set_ylabel(r"E$_z$ [a.u.]", size=15, position=(0.0,0.55))
ax4_y2.yaxis.set_label_position('right')

# ticks
ax4_y2_ticks = ax4_y1_ticks/au_in_V_per_m
ax4_y2.set_yticks(ax4_y2_ticks)
ax4_y2.set_yticklabels(ax4_y2_ticks, size=13)
ax4_y2.tick_params(labelleft=False, labelright=True, labeltop=False, labelbottom=False, bottom=False, top=False, right=False, left=False, direction='inout', length=8)
ax4_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax4_y2_ylim_1 = ax4_ylim_1
ax4_y2_ylim_2 = ax4_ylim_2
ax4_y2.set_ylim(ax4_y2_ylim_1, ax4_y2_ylim_2)


##### plot axes #################################

# Axes label bottom
ax4.set_xlabel("time in femto seconds", size=15)
ax4.xaxis.set_label_position('bottom')

# labels
ax4.set_xticks(xticks_femto)
ax4.set_xticklabels(xticklabels_femto, size=13)
ax4.tick_params(labelleft=False, labelright=False, labeltop=False, labelbottom=True, bottom=True, top=True, right=False, left=False, direction='inout', length=8)
ax4.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# Limits
ax4.set_xlim(ax4_xlim_1, ax4_xlim_2)
ax4.set_ylim(ax4_ylim_1, ax4_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax4.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax4.add_line(line)




###################################################################################################
#  Legend:
###################################################################################################

#ax1.legend(handles=[ax1_datal, ax2_data2, ax2_data4, ax2_data1, ax2_data3], prop={'size': 12}, loc=( 0.1,1.70), ncol=6)
ax1.legend(handles=[ax1_datal,ax1_datall,ax1_datalt,ax1_data2,ax1_data18,ax2_data10,ax1_data1,ax1_data17,ax2_data9], prop={'size': 12}, loc=( 0.10,1.32), ncol=3)




###################################################################################################
#  Save figure:
###################################################################################################

DPI = fig.get_dpi()
fig.set_size_inches(1200.0/float(DPI),1000.0/float(DPI))

fig.savefig('Na_297_dimer_0_1_nm_fixed_ions_electric_field_z_decomposition.png',bbox_inches='tight',pad_inches = 0)

#plt.show()

