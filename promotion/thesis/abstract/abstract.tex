
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract in English

\phantomsection
\addcontentsline{toc}{chapter}{Abstract}

\thispagestyle{plain}

  \Huge
  \hspace{-0.8cm} \textbf{Abstract}
  \normalsize

  \vspace{1cm}

  \noindent
  Light-matter interactions have always been an essential aspect of research.
  They cover the main properties of light and matter in atomic and molecular systems, in condensed phase, in
  chemical reactions, and in optics. This thesis presents a feasible
  implementation to simulate three-dimensional, real-time, real-space,
  self-consistently coupled light-matter systems based on the theoretical
  background of a generalized Pauli-Fierz field theory. Due to the one-to-one
  correspondence between external fields and internal variables, we use a
  Kohn-Sham construction to approach the many-body problem in a
  non-relativistic low energy regime. The formalism leads in mean-field and
  effective nuclei approximation to coupled Ehrenfest-Maxwell-Pauli-Kohn-Sham
  equations.  \\

  \noindent
  In the first part of the thesis, we use a complex bilinear representation of
  the classical microscopic and macroscopic Maxwell's equations based on the
  Riemann-Silberstein vector. Maxwell's equations in Riemann-Silberstein
  representation have the form of an inhomogeneous Schr\"odinger equation,
  which allows to introduce time-evolution operators similar to quantum mechanics and
  to use existing time-evolution algorithms. In this manner, the
  Riemann-Silberstein propagation scheme can solve the microscopic Maxwell's
  equation in vacuum and the macroscopic ones in linear media.

  Such a
  Riemann-Silberstein implementation for propagating electromagnetic fields
  requires proper boundary conditions. Therefore, we introduce incident plane
  wave boundaries to simulate incoming plane waves, as well as perfectly
  matched layer boundaries for efficient absorption.  

  We demonstrate our novel
  Riemann-Silberstein Maxwell propagation implementation for different typical
  electromagnetic applications, for instance, external current densities, plane
  wave propagation and field scattering in a linear medium. Our approach
  provides an alternative method of simulating electromagnetic fields compared
  to the standard finite-difference time-domain approach.  \\

  \noindent
  In the second part of the thesis, we couple the Kohn-Sham current density
  from our generalized Pauli-Fierz Hamiltonian self-consistently to the
  Riemann-Silberstein propagator, and in turn the electromagnetic field to the
  Kohn-Sham Hamiltonian. Including the back-reaction of the matter on the
  electromagnetic field goes beyond what is typically considered in literature.
  Starting with full minimal coupling, we derive for the Kohn-Sham Hamiltonian
  a multipole expansion based on the Power-Zienau-Woolley transformation.

  We introduce a predictor-corrector scheme that provides a practical method to
  simulate self-consistent light-matter systems. Propagating both, the matter wave functions as
  well as the electromagnetic fields alongside, allows to improve the \linebreak 
  efficiency by exploiting the different length- and time-scales of light and
  matter. As a consequence of taking the
  back-reaction of the electromagnetic field into account, we are able to define electromagnetic
  detectors next to the absorbing boundaries, which allows to analyze directly 
  spectroscopic signals in the outgoing radiation in the far-field of the simulation
  box.  \\

  \noindent
  We present a first application of our novel approach by inducing plasmons in a nanoplasmonic system
  by an external laser and investigate the corresponding nano-optical effects,
  in particular the electromagnetic field enhancements in the vicinity of the
  nanoparticles. It reveals that the self-consistent fully coupled
  forward-backward simulations lead to significant changes in observables
  compared to a conventional forward-only coupling. The differences are larger than
  the ones found between using local density and gradient corrected
  approximations for the exchange-correlation functionals. Additionally, the
  directly measured outgoing electromagnetic fields show also harmonic
  generation only beyond dipole approximation.  \\

  \noindent
  Overall, the presented implementation is a comprehensive tool to handle fully
  coupled light-matter systems, especially for nano-optics, nano-plasmonics,
  (photo) electrocatalysis, light propagation with orbital angular momentum or
  light-tailored chemical reactions in optical cavities.
 
  \newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract in German

\phantomsection
\addcontentsline{toc}{chapter}{Deutsche Zusammenfassung}

\thispagestyle{plain}

  \Huge
  \hspace{-0.8cm} \textbf{Deutsche Zusammenfassung}
  \normalsize

  \vspace{1cm}

  \noindent
  Die Wechselwirkung zwischen elektromagnetischen Feldern und Materie bildet
  die Basis f\"ur den Zusammenhalt von Atomen, Molek\"ulen und Festk\"orpern.
  W\"ahrend eine fundamentale Theorie der Wechselwirkung
  geladener Teilchen und Photonen durch die Mitte des 20. Jahrhunderts
  entwickelte Quantenelektrodynamik sehr exakt beschrieben wird,
  wird der Einfluss und die Berechnung mit steigender
  Teilchenzahl immer schwieriger. Aus diesem Grund werden oft vereinfachte
  Modelle oder \linebreak N\"aherungen angewandt, bei denen nicht die volle
  Wechselwirkung ber\"ucksichtigt wird. So wird meist die R\"uckkopplung der
  Materie auf das elektromagnetische Feld vernachl\"assigt oder vereinfacht.

  In dieser Arbeit
  wird auf Basis eines generalisierten Pauli-Fierz Hamiltonians die
  vollst\"andige Licht-Materie Kopplung betrachtet und mithilfe der
  quantenelektro-dynamischen Dichtefunktionaltheorie eine Methode und
  Implementierung vorgestellt, die realistische, dreidimensionale Licht-Materie
  Vielteilchensysteme simulieren kann.  \\

  \noindent
  Zu Beginn der Arbeit wird eine alternative Beschreibung der
  inhomogenen \linebreak Maxwell'schen Gleichungen mit Hilfe des komplexen,
  bilinearen Riemann-Silberstein Vektors pr\"asentiert. In dieser Darstellung wird das
  mikroskopische elektromagnet- \linebreak ische Feld durch zwei linear unabh\"angige
  Riemann-Silberstein Vektoren beschrieben, die einmal selbst und deren
  Riemann-Silberstein Maxwell Gleichungen durch komplexe Konjugation
  ineinander \"ubergehen.

  Es kann gezeigt werden, dass mit den zwei
  verschiedenen Vektordarstellungen die Spin-Natur, hier in Form der
  Helizit\"at, des Photonfeldes dargestellt wird. In der
  Riemann-Silberstein Darstellung haben die kombinierten Amp\`ere'schen und
  Faraday'schen Gleichungen eine zur Schr\"odinger Gleichung \"aquivalente Form.

  Damit l\"asst sich die zeitliche Entwicklung des elektromagnetischen Feldes
  durch eine quantenmechanische Propagation darstellen. Basierend auf dieser
  Riemann-Silberstein Formulierung wird eine Implementierung beschrieben, die die Zeitentwick-lung
  elektromagnetischer Felder berechnet. Dazu geh\"oren verschiedene
  Randbeding-ungen, wie einfallende ebene Wellen und absorbierende Box-R\"ander,
  die ausgehende Signale m\"oglichst ohne Reflexionen simuliert. Anhand
  mehrerer typischer Beispielanwendungen wird demonstriert, dass die
  Implementierung eine Alternative zur \linebreak g\"angigen Finite-Differenzen-Methode oder Finite-Elemente-Methode
  f\"ur elektromagnetische Felder bietet.  \\

  \noindent
  Im weiteren Verlauf der Arbeit wird die klassische Stromdichte der Maxwell \linebreak
  Gleichungen durch die quantenmechanische Betrachtung der Materie bestimmt.
  Dazu wird ausgehend von einem verallgemeinerten Pauli-Fierz
  Hamiltonian ein Kohn-Sham Hamiltonian verwendet, dessen Stromdichte direkt an das
  elektromagnetische Feld gekoppelt ist. In umgekehrter Richtung beeinflusst
  das elektromagnetische Feld die Materie durch minimale Kopplung.
  Ausgehend vom Prinzip der minimalen Kopp-lung geht der Hamiltonian mithilfe der
  Power-Zienau-Woolley Transformation in einen Hamiltonian \"uber, dessen
  Wechselwirkung zwischen elektromagnetischem Feld und Materie durch
  Multipolterme des elektromagnetischen Feldes dargestellt wird. 

  Damit die
  beiden Systeme, Materie und elektromagnetisches Feld, selbstkonsistent
  propagagiern, wird ein Pr\"adiktor-Korrektor-Verfahren eingef\"uhrt.
  Zus\"atzlich werden die unterschiedlichen L\"angen- und Zeitskalen der
  Systeme ausgenutzt, um eine bessere Effizienz der Implementierung vor allem bei
  gro{\ss}en Systemen zu erhalten.  \\

  \noindent
  Der letzte Teil der Arbeit zeigt den Einfluss der vollst\"andigen
  Vorw\"arts-R\"uckw\"arts-Kopplung am Beispiel eines nanoplasmonischen Dimers.
  Es wird verglichen zwischen  
  konventioneller, rein vorw\"arts gekoppelter Licht-Materie Simulation mit der
  hier neu entwickelten, vollst\"andigen, selbstkonsistenten Licht-Materie Kopplung.

  Die zum Teil stark
  abweichenden Ergebnisse werden anschaulich dargestellt und verdeutlichen die
  Notwendigkeit der Betrachtung einer vollst\"andigen Licht-Materie Kopplung.
  Diese Einsch\"atzung wird auch durch Berechnungen mit
  unterschiedlichen Dichtefunktionalen verdeutlicht, bei der die Unterschiede
  der Ergebnisse zwischen den Funktionalen der lokalen und gradientkorrigierten
  Dichten\"aherung kleiner sind als die Unterschiede zwischen vorw\"arts und
  vollst\"andiger Kopplung.  \\

  \noindent
  Insgesamt bietet die Implementierung damit eine praktikable M\"oglichkeit
  vollst\"andig gekoppelte Systeme zu simulieren, z.B. f\"ur die Nanooptik,
  Nanoplasmonik oder \linebreak Elektrokatalyse, um nur einige zu nennen.
  

  \newpage

