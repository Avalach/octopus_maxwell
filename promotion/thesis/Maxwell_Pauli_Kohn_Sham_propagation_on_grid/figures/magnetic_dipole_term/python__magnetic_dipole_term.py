import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def f0(x,x0,w):
    return np.exp(-np.power(x-x0,2)/2*(np.power(w,2)))

fig = plt.figure()
ax = fig.gca(projection='3d')


xin = []
yin = []
zin = []

idx=0
with open('../../../../thesis_runs/Maxwell_Pauli_Kohn_Sham_propagation_on_grid/magnetic_dipole_term/octopus_calculation/magnetic_dipole_term.z=0') as f1:
    for line in f1:
        if (len(line) > 1 ):
            idx = idx+1
            if (idx > 1 ):
               xin.append(line.split()[0])
               yin.append(line.split()[1])
               zin.append(line.split()[2])

L=10.0
x = np.arange(-L, L+0.01, 0.25)
y = np.arange(-L, L+0.01, 0.25)
z = np.zeros((len(x),len(y)))

idx=0
for ii in range(0, len(x)):
    for jj in range(0, len(y)):
        z[ii,jj] = zin[idx]
        idx = idx+1

zz = np.zeros((len(y),len(x)))

for jj in range(0, len(y)):
    for ii in range(0, len(x)):
        zz[jj,ii] = z[ii,jj]

xx, yy = np.meshgrid(x, y)

surf = ax.plot_surface(xx, yy, zz, cmap=cm.coolwarm, linewidth=0, antialiased=False)

plt.title("Magnetic dipole term",size=16,x=0.5,y=1.155)

ax.set_xlabel('x', size=16)
ax.set_ylabel('y', size=16)
ax.set_zlabel('z', size=16)

ax.set_xlim(-10.0,10.0)
ax.set_ylim(-10.0,10.0)
ax.set_zlim(-100.0,100.0)

xticks = np.arange(-10,10.1,2)
yticks = np.arange(-10,10.1,2)
zticks = np.arange(-100.1,100.1,25)
cticks = np.arange(-100.1,100.1,25)


ax.set_xticks(xticks)
ax.set_yticks(yticks)
ax.set_zticks(zticks)

cbar = fig.colorbar(surf, shrink=0.5, aspect=7, ticks=zticks)
cbar.set_clim(-100,100)

azim = ax.azim
elev = ax.elev

elev = 30
azim = -45

ax.view_init(elev, azim)

DPI = fig.get_dpi()
fig.set_size_inches(1000.0/float(DPI),700.0/float(DPI))

fig.savefig('magnetic_dipole_term.png')

plt.show()
