#!/bin/bash

delta=0.125

atom1=H
atom2=C

rm 'maxwell_multigrid_geometry_overlay_different.xyz'
echo '169' >> maxwell_multigrid_geometry_overlay_different.xyz
echo 'generated a' >> maxwell_multigrid_geometry_overlay_different.xyz
number=0
for ii in $(seq -6 6)
do
  for jj in $( seq -6 6 )
  do
    number=$( echo " $number + 1 " | bc )
    xx=$( printf "%12.6f" $ii )
    yy=$( printf "%12.6f" $jj )
    zz=$( printf "%12.6f" 0.0 )
    echo "$atom1      $xx    $yy     $zz " >> maxwell_multigrid_geometry_overlay_different.xyz
  done
done
echo $number
rm 'matter_multigrid_geometry_overlay_different.xyz'
echo '2500' >> matter_multigrid_geometry_overlay_different.xyz
echo 'generated a' >> matter_multigrid_geometry_overlay_different.xyz
number=0
for ii in $(seq -25 24)
do
  for jj in $( seq -25 24 )
  do
    number=$( echo " $number + 1 " | bc )
    xx=$( printf "%12.6f" $( echo " $ii * 0.25 + $delta " | bc -l ) )
    yy=$( printf "%12.6f" $( echo " $jj * 0.25 + $delta " | bc -l ) )
    zz=$( printf "%12.6f" 0.0 )
    echo "$atom2      $xx    $yy    $zz  " >> matter_multigrid_geometry_overlay_different.xyz
  done
done
echo $number


rm 'maxwell_multigrid_geometry_overlay_same.xyz'
echo '169' >> maxwell_multigrid_geometry_overlay_same.xyz
echo 'generated a' >> maxwell_multigrid_geometry_overlay_same.xyz
number=0
for ii in $(seq -6 6)
do
  for jj in $( seq -6 6 )
  do
    number=$( echo " $number + 1 " | bc )
    xx=$( printf "%12.6f" $ii )
    yy=$( printf "%12.6f" $jj )
    zz=$( printf "%12.6f" 0.0 )
    echo "$atom1      $xx    $yy     $zz " >> maxwell_multigrid_geometry_overlay_same.xyz
  done
done
echo $number
rm 'matter_multigrid_geometry_overlay_same.xyz'
echo '81' >> matter_multigrid_geometry_overlay_same.xyz
echo 'generated a' >> matter_multigrid_geometry_overlay_same.xyz
number=0
for ii in $(seq -4 4)
do
  for jj in $( seq -4 4 )
  do
    number=$( echo " $number + 1 " | bc )
    xx=$( printf "%12.6f" $( echo " $ii * 0.25 " | bc -l ) )
    yy=$( printf "%12.6f" $( echo " $jj * 0.25 " | bc -l ) )
    zz=$( printf "%12.6f" 0.0 )
    echo "$atom2      $xx    $yy    $zz  " >> matter_multigrid_geometry_overlay_same.xyz
  done
done
echo $number


rm 'maxwell_multigrid_geometry_overlay_large.xyz'
echo '169' >> maxwell_multigrid_geometry_overlay_large.xyz
echo 'generated a' >> maxwell_multigrid_geometry_overlay_large.xyz
number=0
for ii in $(seq -6 6)
do
  for jj in $( seq -6 6 )
  do
    number=$( echo " $number + 1 " | bc )
    xx=$( printf "%12.6f" $ii )
    yy=$( printf "%12.6f" $jj )
    zz=$( printf "%12.6f" 0.0 )
    echo "$atom1      $xx    $yy     $zz " >> maxwell_multigrid_geometry_overlay_large.xyz
  done
done
echo $number
rm 'matter_multigrid_geometry_overlay_large.xyz'
echo '169' >> matter_multigrid_geometry_overlay_large.xyz
echo 'generated a' >> matter_multigrid_geometry_overlay_large.xyz
number=0
for ii in $(seq -6 6)
do
  for jj in $( seq -6 6 )
  do
    number=$( echo " $number + 1 " | bc )
    xx=$( printf "%12.6f" $( echo " $ii * 0.05 " | bc -l ) )
    yy=$( printf "%12.6f" $( echo " $jj * 0.05 " | bc -l ) )
    zz=$( printf "%12.6f" 0.0 )
    echo "$atom2      $xx    $yy    $zz  " >> matter_multigrid_geometry_overlay_large.xyz
  done
done
echo $number

