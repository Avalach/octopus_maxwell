import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

plane_wave_region    = mpatches.Patch(color=(0,0.392,0), alpha=0.7, label='Incident waves region')
pml_single_region    = mpatches.Patch(color=(0.588,0.588,0), alpha=0.6, label='PML region')
pml_overlap_region   = mpatches.Patch(color=(1.000,1.000,0.000), alpha=0.7, label='Detector region')
matter_region        = mpatches.Patch(color=(0.300,0.300,0.300), alpha=1.0, label='molecule')
plt.legend(handles=[plane_wave_region, pml_single_region, pml_overlap_region, matter_region], prop={'size':18}, loc=(0.0,0.0))

ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

plt.axis('off')

DPI = fig.get_dpi()
fig.set_size_inches(1,1)

fig.savefig('plane_wave_and_pml_boundaries_legend.png',bbox_inches='tight',pad_inches = 0)

plt.show()
