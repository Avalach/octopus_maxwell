import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

# ax1.add_patch(patches.Rectangle((x,y),width,height)

# Domain 1
r1 = [ -2.0,  2.0]
r2 = [  4.0,  4.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [ -2.0, -2.0]
r2 = [  1.0,  4.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [ -2.0,  2.0]
r2 = [  1.0,  1.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [  2.0,  2.0]
r2 = [  1.0,  4.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
ax.text(-1.4, 2.85, 'Ma Partition 1', fontsize=23, color=(0.000,0.192,0.000))
ax.add_patch(patches.Rectangle((-2.0, 1.0), 4.0, 3.0, ec='none', alpha=0.7, color=(0.692,1.000,0.692)))

# Domain 2
r1 = [  2.0,  4.0]
r2 = [  2.0,  2.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [  4.0,  4.0]
r2 = [ -2.0,  2.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [  2.0,  4.0]
r2 = [ -2.0, -2.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
ax.text( 2.85, 1.1, 'Ma Partition 2', fontsize=23, rotation='vertical', color=(0.000,0.192,0.000))
ax.add_patch(patches.Rectangle((-4.0,-2.0), 2.0, 4.0, ec='none', alpha=0.7, color=(0.092,1.000,0.092)))
ax.add_patch(patches.Rectangle((-2.0,-1.0), 2.0, 2.0, ec='none', alpha=0.7, color=(0.092,1.000,0.092)))

# Domain 2/4
r1 = [  0.0,  0.0]
r2 = [ -1.0,  1.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)

# Domain 3
r1 = [ -2.0,  2.0]
r2 = [ -4.0, -4.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [ -2.0, -2.0]
r2 = [ -1.0, -4.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [ -2.0,  2.0]
r2 = [ -1.0, -1.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [  2.0,  2.0]
r2 = [ -1.0, -4.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
ax.text(-1.4,-2.15, 'Ma Partition 3', fontsize=23, color=(0.000,0.192,0.000))
ax.add_patch(patches.Rectangle((-2.0,-4.0), 4.0, 3.0, ec='none', alpha=0.7, color=(0.292,1.000,0.292)))

# Domain 4
r1 = [ -4.0, -2.0]
r2 = [  2.0,  2.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [ -4.0, -4.0]
r2 = [ -2.0,  2.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
r1 = [ -4.0, -2.0]
r2 = [ -2.0, -2.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.5, linestyle='-')
ax.add_line(line)
ax.text(-3.15, 1.1, 'Ma Partition 4', fontsize=23, rotation='vertical', color=(0.000,0.192,0.000))
ax.add_patch(patches.Rectangle(( 0.0,-1.0), 2.0, 2.0, ec='none', alpha=0.7, color=(0.492,1.000,0.492)))
ax.add_patch(patches.Rectangle(( 2.0,-2.0), 2.0, 4.0, ec='none', alpha=0.7, color=(0.492,1.000,0.492)))


for ii in range(-10, 10, 1):
   for jj in range(-10, 10, 1):
      x = ii + 0.5
      y = jj + 0.5
      if ( x**2+y**2 < 2**2 ):
         point, = plt.plot(x, y, marker='o', markersize=24, color=(0.000,0.688,0.000))
      if ( x**2+(y-2.5)**2 < 2**2 ):
         point, = plt.plot(x, y, marker='o', markersize=24, color=(0.000,0.688,0.000))
      if ( x**2+(y+2.5)**2 < 2**2 ):
         point, = plt.plot(x, y, marker='o', markersize=24, color=(0.000,0.688,0.000))
      if ( (x-2.5)**2+y**2 < 2**2 ):
         point, = plt.plot(x, y, marker='o', markersize=24, color=(0.000,0.688,0.000))
      if ( (x+2.5)**2+y**2 < 2**2 ):
         point, = plt.plot(x, y, marker='o', markersize=24, color=(0.000,0.688,0.000))

# Maxwell Domain 1
r1 = [   0,   0]
r2 = [   0, 6.0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax.add_line(line)
ax.text( 1.6, 4.85, 'Mx Partition 1', fontsize=23, color=(0.292,0.000,0.292))
#ax.add_patch(patches.Rectangle(( 0.0, 0.0), 6.5, 6.5, ec='none', alpha=0.7, color=(0.400,0.400,0.400)))

# Maxwell Domain 2
r1 = [   0,   0]
r2 = [   0,-6.0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax.add_line(line)
ax.text( 1.6,-5.15, 'Mx Partition 2', fontsize=23, color=(0.292,0.000,0.292))
#ax.add_patch(patches.Rectangle(( 0.0,-6.5), 6.5, 6.5, ec='none', alpha=0.7, color=(0.400,0.400,0.400)))

# Maxwell Domain 3
r1 = [   0, 6.0]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax.add_line(line)
ax.text(-4.4,-5.15, 'Mx Partition 3', fontsize=23, color=(0.292,0.000,0.292))
#ax.add_patch(patches.Rectangle((-6.5,-6.5), 6.5, 6.5, ec='none', alpha=0.7, color=(0.400,0.400,0.400)))

# Maxwell Domain 4
r1 = [   0,-6.0]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax.add_line(line)
ax.text(-4.4, 4.85, 'Mx Partition 4', fontsize=23, color=(0.292,0.000,0.292))
#ax.add_patch(patches.Rectangle((-6.5, 0.0), 6.5, 6.5, ec='none', alpha=0.7, color=(0.400,0.400,0.400)))



for ii in range(-6, 0, 1):
   for jj in range(-6,0, 1):
      x = ii + 0.5
      y = jj + 0.5
      point, = plt.plot(x, y, marker='o', markersize=12, color=(0.684,0.000,0.684))

for ii in range(-6, 0, 1):
   for jj in range(1, 7, 1):
      x = ii + 0.5
      y = jj - 0.5
      point, = plt.plot(x, y, marker='o', markersize=12, color=(0.684,0.000,0.684))

for ii in range(1, 7, 1):
   for jj in range(-6, 0, 1):
      x = ii - 0.5
      y = jj + 0.5
      point, = plt.plot(x, y, marker='o', markersize=12, color=(0.684,0.000,0.684))

for ii in range(1, 7, 1):
   for jj in range(1, 7, 1):
      x = ii - 0.5
      y = jj - 0.5
      point, = plt.plot(x, y, marker='o', markersize=12, color=(0.684,0.000,0.684))



#major_ticks = np.arange(-10, 10, 5)
#minor_ticks = np.arange(-10, 10, 1)

#ax.set_xticks(major_ticks)                                                       
#ax.set_xticks(minor_ticks, minor=True)                                           
#ax.set_yticks(major_ticks)                                                       
#ax.set_yticks(minor_ticks, minor=True)

#ax.grid(which='minor', alpha=0.5)                                                
#ax.grid(which='major', alpha=1.0)

ax.set_xlim(-6.0,6.0)
ax.set_ylim(-6.0,6.0)

ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

DPI = fig.get_dpi()
fig.set_size_inches(12,12)

fig.savefig('multigrid_prallel_matter_in_2D.png',bbox_inches='tight',pad_inches = 0)

plt.show()
