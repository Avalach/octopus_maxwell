#!/bin/bash

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots()

gs = gridspec.GridSpec(10, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

ax1 = plt.subplot(gs[0:2,:])
ax2 = plt.subplot(gs[2:6,0])
ax3 = plt.subplot(gs[6:10,0])

dt       = 0.2106550
relax    = 1147
step_0   = 115
t_offset = - relax * dt
f_offset = 0.0

cc        = 137.035999679
pi        = 3.14159265359
ep        = 1/(4*pi)
amplitude = 0.0001
omega     = 0.104000543511
period    = 2*pi/omega
width     = 41395.1000/cc
shift     = 41395.1000/cc # + t_offset

period_number = 25




###################################################################################################
#  Laser function:
###################################################################################################

def f_laser(x,a0,x0,w,omega):
    y = []
    for ii in range(0,len(x)):
       if ( np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2))) < w ):
          y.append( a0 * np.cos(pi/2*(x[ii]-2*w-x0)/w+pi) * np.cos(omega*(x[ii]-x0)) )
       else:
          y.append(0.0)
    return y

t_laser = np.arange(0.0, 2000.0, 1.0)




###################################################################################################
#  Laser energy:
###################################################################################################

def f_laser_energy(x,a0,x0,w,omega):
    y = []
    for ii in range(0,len(x)):
       #print(x[ii], w,np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2)))) 
       if ( np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2))) < w ):
          y.append( ep * np.power( a0 * np.cos(pi/2*(x[ii]-2*w-x0)/w+pi) * np.cos(omega*(x[ii]-x0)), 2 ) )
       else:
          y.append(0.0)
    return y




###################################################################################################
#  Dataset 1:
###################################################################################################

t_e1 = []
f_e1 = []
with open('./370_cobra/td.general/energy') as e1:
    for line in e1:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_e1.append(line.split()[1])
           f_e1.append(line.split()[2]) 
f0_e1 = f_e1[step_0]
for ii in range(0, len(t_e1)):
    t_e1[ii] = float(t_e1[ii]) + t_offset
    f_e1[ii] = float(f_e1[ii]) 




###################################################################################################
#  Dataset 2:
###################################################################################################

t_e2 = []
f_e2 = []
with open('./371_cobra/td.general/energy') as e2:
    for line in e2:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_e2.append(line.split()[1])
           f_e2.append(line.split()[2]) 
f0_e2 = f_e2[step_0]
for ii in range(0, len(t_e2)):
    t_e2[ii] = float(t_e2[ii]) + t_offset
    f_e2[ii] = float(f_e2[ii]) 




###################################################################################################
#  Dataset 3:
###################################################################################################

t_e3 = []
f_e3 = []
with open('./372_cobra/td.general/energy') as e3:
    for line in e3:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_e3.append(line.split()[1])
           f_e3.append(line.split()[2]) 
f0_e3 = f_e3[step_0]
for ii in range(0, len(t_e3)):
    t_e3[ii] = float(t_e3[ii]) + t_offset
    f_e3[ii] = float(f_e3[ii]) 




###################################################################################################
#  Dataset 4:
###################################################################################################

t_e4 = []
f_e4 = []
with open('./373_cobra/td.general/energy') as e4:
    for line in e4:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_e4.append(line.split()[1])
           f_e4.append(line.split()[2]) 
f0_e4 = f_e4[step_0]
for ii in range(0, len(t_e4)):
    t_e4[ii] = float(t_e4[ii]) + t_offset
    f_e4[ii] = float(f_e4[ii]) 




###################################################################################################
#  Dataset 5:
###################################################################################################

t_e5 = []
f_e5 = []
idx = 0
with open('./370_cobra/td.general/maxwell_energy') as e5:
    for line in e5:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_e5.append(line.split()[1])
           f_e5.append(line.split()[3]) 
f0_e5 = f_e5[step_0]
for ii in range(0, len(t_e5)):
    t_e5[ii] = float(t_e5[ii]) + t_offset 
    f_e5[ii] = float(f_e5[ii]) 




###################################################################################################
#  Dataset 6:
###################################################################################################

t_e6 = []
f_e6 = []
idx = 0
with open('./371_cobra/td.general/maxwell_energy') as e6:
    for line in e6:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_e6.append(line.split()[1])
           f_e6.append(line.split()[3]) 
f0_e6 = f_e6[step_0]
for ii in range(0, len(t_e6)):
    t_e6[ii] = float(t_e6[ii]) + t_offset
    f_e6[ii] = float(f_e6[ii]) 




###################################################################################################
#  Dataset 7:
###################################################################################################

t_e7 = []
f_e7 = []
idx = 0
with open('./372_cobra/td.general/maxwell_energy') as e7:
    for line in e7:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_e7.append(line.split()[1])
           f_e7.append(line.split()[3]) 
f0_e7 = f_e7[step_0]
for ii in range(0, len(t_e7)):
    t_e7[ii] = float(t_e7[ii]) + t_offset
    f_e7[ii] = float(f_e7[ii]) 



###################################################################################################
#  Dataset 8:
###################################################################################################

t_e8 = []
f_e8 = []
idx = 0
with open('./373_cobra/td.general/maxwell_energy') as e8:
    for line in e8:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_e8.append(line.split()[1])
           f_e8.append(line.split()[3]) 
f0_e8 = f_e8[step_0]
for ii in range(0, len(t_e8)):
    t_e8[ii] = float(t_e8[ii]) + t_offset
    f_e8[ii] = float(f_e8[ii]) 




###################################################################################################
#  Dataset 8:
###################################################################################################

laser_energy = f_laser_energy(t_laser,amplitude,shift,width,omega)
t_e9 = []
f_e9 = []
for ii in range(0, len(laser_energy)):
    t_e9.append(t_laser[ii])
    f_e9.append((2*41.5)*(2*41.5)*(2*76.5)*float(laser_energy[ii])+float(f0_e5))




###################################################################################################
#  Plots:
###################################################################################################

fig.suptitle(r"Matter and Maxwell energy of the dimer for d$_2$=0.5nm",x=0.5,y=1.08, size=18)

##### Panel 1 plot ##############################
ax1_data1, = ax1.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), '-', color=(0.100,0.100,0.100), linewidth=1.5, label='external laser')

##### Panel 2 plot ##############################
ax2_data4, = ax2.plot(t_e4, f_e4, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='F@(ED+MD+EQ)')
ax2_data2, = ax2.plot(t_e2, f_e2, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='F@ED')
ax2_data1, = ax2.plot(t_e1, f_e1, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='FB@ED')
ax2_data3, = ax2.plot(t_e3, f_e3, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='FB@(ED+MD+EQ)')

##### Panel 3 plot ##############################
ax3_datal, = ax3.plot(t_e9,f_e9,'-', color=(0.100,0.100,0.100), linewidth=1.5, label='external laser')
ax3_data4, = ax3.plot(t_e8,f_e8,'-', color=(0.700,0.700,0.000), linewidth=1.5, label='F@(ED+MD+EQ)')
ax3_data2, = ax3.plot(t_e6,f_e6,'-', color=(0.000,0.000,0.700), linewidth=1.5, label='F@ED')
ax3_data1, = ax3.plot(t_e5,f_e5,'-', color=(0.000,0.700,0.000), linewidth=1.5, label='FB@ED')
ax3_data3, = ax3.plot(t_e7,f_e7,'-', color=(0.700,0.000,0.000), linewidth=1.5, label='FB@(ED+MD+EQ)')




###################################################################################################
#  Units transformation:
###################################################################################################

# Femtosecond to atomic units factor
femto_in_au    = 103.353

# energy from atomic untis to eV
au_in_eV       = 27.2114

# electric field from atomic units to V/m
au_in_V_per_m = 5.14220652*(10**11)

# current from atomic unts to A
au_in_A = 6.623618183*(10**(-3))

# x-tick interval
femto_interval = 2.5

# x-ticks for all plots
xticks_femto      = np.arange(0, period_number*period, femto_in_au)
xticklabels_femto = np.arange(0, period_number*period/femto_in_au*femto_interval, femto_interval)  

# x-tick-labels for all plots
yticks_eV       = np.arange(-142.905, -142.88, 0.005)
yticklabels_eV  = np.arange(-3888.65, -3887.96, 0.05, dtype=float)




###################################################################################################
#  Panel 1: Laser
###################################################################################################

# Axes label top
ax1.set_xlabel("time in atomic units", size=15)
ax1.xaxis.set_label_coords(0.5,1.30)
ax1.xaxis.set_label_position('top')

# au limits for Panel 3
ax1_xlim_1 = 0.0
ax1_xlim_2 = period_number*period
ax1_ylim_1 = -1.10e-4
ax1_ylim_2 =  1.10e-4

ax1.text(ax1_xlim_1+0.02*(np.abs(ax1_xlim_2-ax1_xlim_1)), ax1_ylim_1+0.8*(np.abs(ax1_ylim_2-ax1_ylim_1)),'a)',size=18)


##### Y left label axes #########################

ax1_y1 = ax1.twinx()

# Axes label on the left
ax1_y1.set_ylabel(r'E$_{z}$ [V/m]', size=15, position=(0.0,0.55))
ax1_y1.yaxis.set_label_position('left')

# ticks
ax1_y1_ticks = [-4*10**7,0.0,4*10**7]
ax1_y1.set_yticks(ax1_y1_ticks)
ax1_y1.set_yticklabels(ax1_y1_ticks, size=13)
ax1_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
ax1_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax1_y1_ylim_1 = ax1_ylim_1 * au_in_V_per_m
ax1_y1_ylim_2 = ax1_ylim_2 * au_in_V_per_m
ax1_y1.set_ylim(ax1_y1_ylim_1, ax1_y1_ylim_2)


##### Y right label axes ########################

ax1_y2 = ax1.twinx()

# Axes label on the right
ax1_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
ax1_y2.yaxis.set_label_position('right')

# ticks
ax1_y2_ticks = [-4*10**7/au_in_V_per_m, 0.0/au_in_V_per_m, 4*10**7/au_in_V_per_m]
ax1_y2.set_yticks(ax1_y2_ticks)
ax1_y2.set_yticklabels(ax1_y2_ticks, size=13)
ax1_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
ax1_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax1_y2_ylim_1 = ax1_ylim_1
ax1_y2_ylim_2 = ax1_ylim_2
ax1_y2.set_ylim(ax1_y2_ylim_1, ax1_y2_ylim_2)


##### plot axes #################################

# ticks
ax1.set_xticks(xticks_femto)
ax1.set_xticklabels(xticks_femto, size=13)
ax1.tick_params(labelleft='off', labelright='off', labeltop='on', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax1.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# limits
ax1.set_xlim(ax1_xlim_1, ax1_xlim_2)
ax1.set_ylim(ax1_ylim_1, ax1_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax1.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax1.add_line(line)



###################################################################################################
#  Panel 2: Matter energy
###################################################################################################

# au limits for Panel 2
ax2_xlim_1 = 0.0
ax2_xlim_2 = period_number*period
ax2_ylim_1 = -1.42841e2
ax2_ylim_2 = -1.42795e2

ax2.text(ax2_xlim_1+0.02*(np.abs(ax2_xlim_2-ax1_xlim_1)), ax2_ylim_1+0.8*(np.abs(ax2_ylim_2-ax2_ylim_1)),'b)',size=18)


##### Y left label axes #########################

ax2_y1 = ax2.twinx()

# Axes label on the left
ax2_y1.set_ylabel("energy [eV]", size=15, position=(0.0,0.5))
ax2_y1.yaxis.set_label_position('left')

# ticks
ax2_y1_ticks = np.arange(-3.88680e3, -3.88570e3, 0.00020e3)
ax2_y1.set_yticks(ax2_y1_ticks)
ax2_y1.set_yticklabels(ax2_y1_ticks, size=13)
ax2_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
ax2_y1.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax2_y1_ylim_1 = ax2_ylim_1 * au_in_eV
ax2_y1_ylim_2 = ax2_ylim_2 * au_in_eV
ax2_y1.set_ylim(ax2_y1_ylim_1, ax2_y1_ylim_2)


##### Y right label axes ########################

ax2_y2 = ax2.twinx()

# Axes label on the right
ax2_y2.set_ylabel("energy [a.u.]", size=15, position=(0.0,0.5))
ax2_y2.yaxis.set_label_position('right')

# ticks
ax2_y2_ticks = ax2_y1_ticks / au_in_eV
ax2_y2.set_yticks(ax2_y2_ticks)
ax2_y2.set_yticklabels(ax2_y2_ticks, size=13)
ax2_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
ax2_y2.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax2_y2_ylim_1 = ax2_ylim_1
ax2_y2_ylim_2 = ax2_ylim_2
ax2_y2.set_ylim(ax2_y2_ylim_1, ax2_y2_ylim_2)


##### plot axes #################################

# ticks
ax2.set_xticks(xticks_femto)
ax2.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax2.set_xlim(ax1_xlim_1, ax1_xlim_2)
ax2.set_ylim(ax2_ylim_1, ax2_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -500, 500]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax2.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ f0_e1,  f0_e1]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax2.add_line(line)




###################################################################################################
#  Panel 3: Maxwell energy
###################################################################################################

# au limits for Panel 3
ax3_xlim_1 = 0.0
ax3_xlim_2 = period_number*period
ax3_ylim_1 = 3.85375e2
ax3_ylim_2 = 3.85440e2

ax3.text(ax3_xlim_1+0.02*(np.abs(ax3_xlim_2-ax3_xlim_1)), ax3_ylim_1+0.8*(np.abs(ax3_ylim_2-ax3_ylim_1)),'c)',size=18)


##### Y left label axes #########################

ax3_y1 = ax3.twinx()

# Axes label on the left
ax3_y1.set_ylabel("energy [eV]", size=15, position=(0.0,0.5))
ax3_y1.yaxis.set_label_position('left')

# ticks
ax3_y1_ticks = np.arange(1.04864e4, 1.04885e4, 0.00004e4)
ax3_y1.set_yticks(ax3_y1_ticks)
ax3_y1.set_yticklabels(ax3_y1_ticks, size=13)
ax3_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
ax3_y1.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax3_y1_ylim_1 = ax3_ylim_1 * au_in_eV
ax3_y1_ylim_2 = ax3_ylim_2 * au_in_eV
ax3_y1.set_ylim(ax3_y1_ylim_1, ax3_y1_ylim_2)


##### Y right label axes ########################

ax3_y2 = ax3.twinx()

# Axes label on the right
ax3_y2.set_ylabel("energy [a.u.]", size=15, position=(0.0,0.5))

# ticks
ax3_y2_ticks = ax3_y1_ticks / au_in_eV
ax3_y2.set_yticks(ax3_y2_ticks)
ax3_y2.set_yticklabels(ax3_y2_ticks, size=13)
ax3_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
ax3_y2.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax3_y2_ylim_1 = ax3_ylim_1
ax3_y2_ylim_2 = ax3_ylim_2
ax3_y2.set_ylim(ax3_y2_ylim_1, ax3_y2_ylim_2)


##### plot axes #################################

# Axes label bottom
ax3.set_xlabel("time in femto seconds", size=15)
ax3.xaxis.set_label_position('bottom')

# ticks
ax3.set_xticks(xticks_femto)
ax3.set_xticklabels(xticklabels_femto, size=13)
ax3.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='on', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax3.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax3.set_xlim(ax1_xlim_1, ax1_xlim_2)
ax3.set_ylim(ax3_y2_ylim_1, ax3_y2_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -500, 500]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax3.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ f0_e5,  f0_e5]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax3.add_line(line)



###################################################################################################
#  Legend:
###################################################################################################

ax1.legend(handles=[ax1_data1, ax2_data2, ax2_data4, ax2_data1, ax2_data3], prop={'size': 12}, loc=( 0.1,1.55), ncol=6)




###################################################################################################
#  Save figure:
###################################################################################################

DPI = fig.get_dpi()
fig.set_size_inches(1500.0/float(DPI),720.0/float(DPI))

fig.savefig('Na_297_dimer_0_5_nm_fixed_ions_energy.png',bbox_inches='tight',pad_inches = 0)

plt.show()
