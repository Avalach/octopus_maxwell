#!/bin/bash

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from scipy.fftpack import fft, ifft
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(figsize=(1,10))

gs = gridspec.GridSpec(6, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

ax1 = plt.subplot(gs[0:2,0])
ax2 = plt.subplot(gs[2:4,0])
ax3 = plt.subplot(gs[4:6,0])


dt       = 0.2106550
relax    = 1065
step_0   = 107
t_offset = - relax * dt
f_offset = 0.0

cc        = 137.035999679
pi        = 3.14159265359
amplitude = 0.0001
omega     = 0.112000023380
period    = 2*pi/omega
width     = 38438.5000/cc
shift     = 38438.5000/cc 

period_number = 20

N_total_points = 2000
N_plot_points  = N_total_points-relax
N_fft          = N_plot_points/2 + 1

N_total_points_2 = 200
N_plot_points_2  = N_total_points_2-step_0
N_fft_2          = N_plot_points_2/2 + 1




###################################################################################################
#  Laser function:
###################################################################################################

def f_laser(x,a0,x0,w,omega):
    y = []
    for ii in range(0,len(x)):
       if ( np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2))) < w ):
          y.append( a0 * np.cos(pi/2*(x[ii]-2*w-x0)/w+pi) * np.cos(omega*(x[ii]-x0)) )
       else:
          y.append(0.0)
    return y

t_laser = np.arange(0.0, period_number * period, dt)

##### Laser fourier transform ###################

w_fftl = []
for ii in range(0, len(t_laser)):
    w_fftl.append(2 * np.pi * ii / len(t_laser))

N_fftl     = len(t_laser)/2+1
dt_fftl    = dt
w_max_fftl = 1/dt_fftl*2*np.pi
w_fftl     = np.linspace(0, w_max_fftl/2, N_fftl)
f_fftl     = 2*fft(f_laser(t_laser,amplitude,shift,width,omega))/N_fftl




###################################################################################################
#  Dataset 1:
###################################################################################################

with open('./366_cobra/td.general/maxwell_e_field_z_at_00_00_00') as data1:
    readlines = data1.readlines()
    t_data1 = [line.split()[0] for line in readlines]
    f_data1 = [line.split()[1] for line in readlines]
f0_data1 = f_data1[step_0]
for ii in range(0, len(t_data1)):
    t_data1[ii] = float(t_data1[ii]) + t_offset
    f_data1[ii] = float(f_data1[ii]) 




###################################################################################################
#  Dataset 2:
###################################################################################################

with open('./367_cobra/td.general/maxwell_e_field_z_at_00_00_00') as data2:
    readlines = data2.readlines()
    t_data2 = [line.split()[0] for line in readlines]
    f_data2 = [line.split()[1] for line in readlines]
f0_data2 = f_data2[step_0]
for ii in range(0, len(t_data2)):
    t_data2[ii] = float(t_data2[ii]) + t_offset
    f_data2[ii] = float(f_data2[ii]) 




###################################################################################################
#  Dataset 3:
###################################################################################################

with open('./374_cobra/td.general/maxwell_e_field_z_at_00_00_00') as data3:
    readlines = data3.readlines()
    t_data3 = [line.split()[0] for line in readlines]
    f_data3 = [line.split()[1] for line in readlines]
f0_data3 = f_data3[step_0]
for ii in range(0, len(t_data3)):
    t_data3[ii] = float(t_data3[ii]) + t_offset
    f_data3[ii] = float(f_data3[ii]) - float(f0_data3)




###################################################################################################
#  Dataset 4:
###################################################################################################

with open('./375_cobra/td.general/maxwell_e_field_z_at_00_00_00') as data4:
    readlines = data4.readlines()
    t_data4 = [line.split()[0] for line in readlines]
    f_data4 = [line.split()[1] for line in readlines]
f0_data4 = f_data4[step_0]
for ii in range(0, len(t_data4)):
    t_data4[ii] = float(t_data4[ii]) + t_offset
    f_data4[ii] = float(f_data4[ii]) 




###################################################################################################
#  Dataset 5:
###################################################################################################

t_data5 = []
f_data5 = []
with open('./366_cobra/td.general/multipoles') as data5:
    for line in data5:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data5.append(line.split()[1])
           f_data5.append(line.split()[5])
f0_data5 = f_data5[step_0]

tw_data5 = []
fw_data5 = []
for ii in range(0, len(t_data5)):
    if (float(t_data5[ii]) + float(t_offset) > 0.0):
       tw_data5.append(float(t_data5[ii]) + t_offset)
       fw_data5.append(float(f_data5[ii]))




###################################################################################################
#  Dataset 6:
###################################################################################################

t_data6 = []
f_data6 = []
with open('./367_cobra/td.general/multipoles') as data6:
    for line in data6:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data6.append(line.split()[1])
           f_data6.append(line.split()[5])
f0_data6 = f_data6[step_0]

tw_data6 = []
fw_data6 = []
for ii in range(0, len(t_data6)):
    if (float(t_data6[ii]) + float(t_offset) > 0.0):
       tw_data6.append(float(t_data6[ii]) + t_offset)
       fw_data6.append(float(f_data6[ii]))





###################################################################################################
#  Dataset 7:
###################################################################################################

t_data7 = []
f_data7 = []
with open('./374_cobra/td.general/multipoles') as data7:
    for line in data7:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data7.append(line.split()[1])
           f_data7.append(line.split()[5])
f0_data7 = f_data7[step_0]

tw_data7 = []
fw_data7 = []
for ii in range(0, len(t_data7)):
    if (float(t_data7[ii]) + float(t_offset) > 0.0):
       tw_data7.append(float(t_data7[ii]) + t_offset)
       fw_data7.append(float(f_data7[ii]))




###################################################################################################
#  Dataset 8:
###################################################################################################

t_data8 = []
f_data8 = []
with open('./375_cobra/td.general/multipoles') as data8:
    for line in data8:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data8.append(line.split()[1])
           f_data8.append(line.split()[5])
f0_data8 = f_data8[step_0]

tw_data8 = []
fw_data8 = []
for ii in range(0, len(t_data8)):
    if (float(t_data8[ii]) + float(t_offset) > 0.0):
       tw_data8.append(float(t_data8[ii]) + t_offset)
       fw_data8.append(float(f_data8[ii]))




###################################################################################################
#  Dataset 9:
###################################################################################################

t_data9 = []
f_data9 = []
idx = 0
with open('./366_cobra/td.general/maxwell_energy') as data9:
    for line in data9:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_data9.append(line.split()[1])
           f_data9.append(line.split()[3])
f0_data9 = f_data9[step_0]

tw_data9 = []
fw_data9 = []
for ii in range(0, len(t_data9)):
    if (float(t_data9[ii]) + float(t_offset) > 0.0):
       tw_data9.append(float(t_data9[ii]) + t_offset)
       fw_data9.append(float(f_data9[ii]))




###################################################################################################
#  Dataset 10:
###################################################################################################

t_data10 = []
f_data10 = []
idx = 0
with open('./367_cobra/td.general/maxwell_energy') as data10:
    for line in data10:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_data10.append(line.split()[1])
           f_data10.append(line.split()[3])
f0_data10 = f_data10[step_0]

tw_data10 = []
fw_data10 = []
for ii in range(0, len(t_data10)):
    if (float(t_data10[ii]) + float(t_offset) > 0.0):
       tw_data10.append(float(t_data10[ii]) + t_offset)
       fw_data10.append(float(f_data10[ii]))




###################################################################################################
#  Dataset 11:
###################################################################################################

t_data11 = []
f_data11 = []
idx = 0
with open('./374_cobra/td.general/maxwell_energy') as data11:
    for line in data11:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_data11.append(line.split()[1])
           f_data11.append(line.split()[3])
f0_data11 = f_data11[step_0]

tw_data11 = []
fw_data11 = []
for ii in range(0, len(t_data11)):
    if (float(t_data11[ii]) + float(t_offset) > 0.0):
       tw_data11.append(float(t_data11[ii]) + t_offset)
       fw_data11.append(float(f_data11[ii]) - float(f0_data11) + float(f0_data9))




###################################################################################################
#  Dataset 12:
###################################################################################################

t_data12 = []
f_data12 = []
idx = 0
with open('./375_cobra/td.general/maxwell_energy') as data12:
    for line in data12:
        idx = idx+1
        split_len = len(line.split())
        if ( (split_len == 11 ) & ( idx > 5 ) ):
           t_data12.append(line.split()[1])
           f_data12.append(line.split()[3])
f0_data12 = f_data12[step_0]

tw_data12 = []
fw_data12 = []
for ii in range(0, len(t_data12)):
    if (float(t_data12[ii]) + float(t_offset) > 0.0):
       tw_data12.append(float(t_data12[ii]) + t_offset)
       fw_data12.append(float(f_data12[ii]) - float(f0_data12) + float(f0_data10))




###################################################################################################
#  Plots:
###################################################################################################

fig.suptitle(r"Comparison of density functionals for the dimer with distance d$_1$=0.1nm",x=0.5,y=1.04, size=18)

##### Panel 1 plot ##############################
ax1_data1, = ax1.plot(t_data1, f_data1, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='F@ED (TDLDA)')
ax1_data2, = ax1.plot(t_data2, f_data2, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='FB@ED (TDLDA)')
ax1_data3, = ax1.plot(t_data3, f_data3, color=(0.000,0.000,0.400), linewidth=3.0, dashes=(2.0,1.5), label='F@ED (TDPBE)')
ax1_data4, = ax1.plot(t_data4, f_data4, color=(0.000,0.400,0.000), linewidth=3.0, dashes=(2.0,1.5), label='FB@ED (TDPBE)')


##### Panel 2 plot ##############################
ax2_data9,  = ax2.plot(tw_data9,  fw_data9,  '-', color=(0.000,0.000,0.700), linewidth=1.5, label='F@ED (TDLDA)')
ax2_data10, = ax2.plot(tw_data10, fw_data10, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='FB@ED (TDLDA)')
ax2_data11, = ax2.plot(tw_data11, fw_data11, color=(0.000,0.000,0.400), linewidth=3.0, dashes=(2.0,1.5), label='F@ED (TDPBE)')
ax2_data12, = ax2.plot(tw_data12, fw_data12, color=(0.000,0.400,0.000), linewidth=3.0, dashes=(2.0,1.5), label='FB@ED (TDPBE)')


##### Panel 3 plot ##############################
ax3_data5, = ax3.plot(tw_data5, fw_data5, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='F@ED (TDLDA)')
ax3_data6, = ax3.plot(tw_data6, fw_data6, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='FB@ED (TDLDA)')
ax3_data7, = ax3.plot(tw_data7, fw_data7, color=(0.000,0.000,0.400), linewidth=3.0, dashes=(2.0,1.5), label='F@ED (TDPBE)')
ax3_data8, = ax3.plot(tw_data8, fw_data8, color=(0.000,0.400,0.000), linewidth=3.0, dashes=(2.0,1.5), label='FB@ED (TDPBE)')




###################################################################################################
#  Units transformation:
###################################################################################################

# Femtosecond to atomic units factor
femto_in_au    = 103.353

# energy from atomic untis to eV
au_in_eV       = 27.2114

# electric field from atomic units to V/m
au_in_V_per_m = 5.14220652*(10**11)

# current from atomic unts to A
au_in_A = 6.623618183*(10**(-3))

# x-tick interval
femto_interval = 2.5

# x-ticks for all plots
xticks_femto      = np.arange(0, period_number*period, femto_in_au)
xticklabels_femto = np.arange(0, period_number*period/femto_in_au*femto_interval, femto_interval)  

# x-tick-labels for all plots
yticks_eV       = np.arange(-142.905, -142.88, 0.005)
yticklabels_eV  = np.arange(-3888.65, -3887.96, 0.05, dtype=float)

# lenght in au
l_au = 5.2917721092*10**(-11)

# lenght in nm
l_nm = 10**(-9)

# au in nm
au_in_nm = l_au / l_nm




###################################################################################################
#  Panel 1: Total field and longitudinal near field
###################################################################################################

# Axes label top
ax1.set_xlabel("time in atomic units", size=15)
ax1.xaxis.set_label_coords(0.5,1.20)
ax1.xaxis.set_label_position('top')

# au limits for the laser
ax1_xlim_1 = 0.0
ax1_xlim_2 = period_number*period
ax1_ylim_1 = -4.40e-4
ax1_ylim_2 =  4.40e-4

ax1.text(ax1_xlim_1+0.02*(np.abs(ax1_xlim_2-ax1_xlim_1)), ax1_ylim_1+0.8*(np.abs(ax1_ylim_2-ax1_ylim_1)),'a)',size=18)


##### Y left label axes #########################

ax1_y1 = ax1.twinx()

# Axes label on the left
ax1_y1.set_ylabel(r'E$_{z}$ [V/m]', size=15, position=(0.0,0.55))
ax1_y1.yaxis.set_label_position('left')

# ticks
ax1_y1_ticks = np.arange(-2.0*10**8, 2.1*10**8, 1*10**8)
ax1_y1.set_yticks(ax1_y1_ticks)
ax1_y1.set_yticklabels(ax1_y1_ticks, size=13)
ax1_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
ax1_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax1_y1_ylim_1 = ax1_ylim_1 * au_in_V_per_m
ax1_y1_ylim_2 = ax1_ylim_2 * au_in_V_per_m
ax1_y1.set_ylim(ax1_y1_ylim_1, ax1_y1_ylim_2)


##### Y right label axes ########################

ax1_y2 = ax1.twinx()

# Axes label on the right
ax1_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
ax1_y2.yaxis.set_label_position('right')

# ticks
ax1_y2_ticks = ax1_y1_ticks/au_in_V_per_m
ax1_y2.set_yticks(ax1_y2_ticks)
ax1_y2.set_yticklabels(ax1_y2_ticks, size=13)
ax1_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
ax1_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax1_y2_ylim_1 = ax1_ylim_1
ax1_y2_ylim_2 = ax1_ylim_2
ax1_y2.set_ylim(ax1_y2_ylim_1, ax1_y2_ylim_2)


##### plot axes #################################

# labels
ax1.set_xticks(xticks_femto)
ax1.set_xticklabels(xticks_femto, size=13)
ax1.tick_params(labelleft='off', labelright='off', labeltop='on', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax1.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax1.set_xlim(ax1_xlim_1, ax1_xlim_2)
ax1.set_ylim(ax1_ylim_1, ax1_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax1.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax1.add_line(line)




###################################################################################################
#  Panel 2: Maxwell energy
###################################################################################################

# au limits for Panel 3
ax2_xlim_1 = 0.0
ax2_xlim_2 = period_number*period
ax2_ylim_1 = 3.85380e2
ax2_ylim_2 = 3.85420e2

ax2.text(ax2_xlim_1+0.02*(np.abs(ax2_xlim_2-ax2_xlim_1)), ax2_ylim_1+0.8*(np.abs(ax2_ylim_2-ax2_ylim_1)),'b)',size=18)


##### Y left label axes #########################

ax2_y1 = ax2.twinx()

# Axes label on the left
ax2_y1.set_ylabel("energy [eV]", size=15, position=(0.0,0.5))
ax2_y1.yaxis.set_label_position('left')

# ticks
ax2_y1_ticks = np.arange(1.04840e4,1.04879e4,0.00005e4)
ax2_y1.set_yticks(ax2_y1_ticks)
ax2_y1.set_yticklabels(ax2_y1_ticks, size=13)
ax2_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
ax2_y1.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax2_y1_ylim_1 = ax2_ylim_1 * au_in_eV
ax2_y1_ylim_2 = ax2_ylim_2 * au_in_eV
ax2_y1.set_ylim(ax2_y1_ylim_1, ax2_y1_ylim_2)


##### Y right label axes ########################

ax2_y2 = ax2.twinx()

# Axes label on the right
ax2_y2.set_ylabel("energy [a.u.]", size=15, position=(0.0,0.5))

# ticks
ax2_y2_ticks = ax2_y1_ticks / au_in_eV
ax2_y2.set_yticks(ax2_y2_ticks)
ax2_y2.set_yticklabels(ax2_y2_ticks, size=13)
ax2_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
ax2_y2.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax2_y2_ylim_1 = ax2_ylim_1
ax2_y2_ylim_2 = ax2_ylim_2
ax2_y2.set_ylim(ax2_y2_ylim_1, ax2_y2_ylim_2)


##### plot axes #################################

# Axes label bottom
ax2.set_xlabel("time in femto seconds", size=15)
ax2.xaxis.set_label_position('bottom')

# ticks
ax2.set_xticks(xticks_femto)
ax2.set_xticklabels(xticklabels_femto, size=13)
ax2.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# limits
ax2.set_xlim(ax1_xlim_1, ax1_xlim_2)
ax2.set_ylim(ax2_y2_ylim_1, ax2_y2_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -500, 500]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax2.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ f0_data9,  f0_data9]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax2.add_line(line)



###################################################################################################
#  Panel 3: Total field and longitudinal far field
###################################################################################################

# Axes label top
ax3.set_xlabel("time in atomic units", size=15)
#ax3.xaxis.set_label_coords(0.5,1.20)
ax3.xaxis.set_label_position('bottom')

# au limits for the laser
ax3_xlim_1 = 0.0
ax3_xlim_2 = period_number*period
ax3_ylim_1 = -2.40e+1
ax3_ylim_2 =  2.40e+1

ax3.text(ax3_xlim_1+0.02*(np.abs(ax3_xlim_2-ax3_xlim_1)), ax3_ylim_1+0.8*(np.abs(ax3_ylim_2-ax3_ylim_1)),'c)',size=18)


##### Y left label axes #########################

ax3_y1 = ax3.twinx()

# Axes label on the left
ax3_y1.set_ylabel(r'$<z>$ [Nm]', size=15, position=(0.0,0.55))
ax3_y1.yaxis.set_label_position('left')

# ticks
ax3_y1_ticks = np.arange(-2.0*10**0, 2.1*10**0, 0.5*10**0)
ax3_y1.set_yticks(ax3_y1_ticks)
ax3_y1.set_yticklabels(ax3_y1_ticks, size=13)
ax3_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
ax3_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax3_y1_ylim_1 = ax3_ylim_1 * au_in_nm
ax3_y1_ylim_2 = ax3_ylim_2 * au_in_nm
ax3_y1.set_ylim(ax3_y1_ylim_1, ax3_y1_ylim_2)


##### Y right label axes ########################

ax3_y2 = ax3.twinx()

# Axes label on the right
ax3_y2.set_ylabel(r'$<z>$ [a.u.]', size=15, position=(0.0,0.55))
ax3_y2.yaxis.set_label_position('right')

# ticks
ax3_y2_ticks = ax3_y1_ticks/au_in_nm
ax3_y2.set_yticks(ax3_y2_ticks)
ax3_y2.set_yticklabels(ax3_y2_ticks, size=13)
ax3_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
ax3_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
ax3_y2_ylim_1 = ax3_ylim_1
ax3_y2_ylim_2 = ax3_ylim_2
ax3_y2.set_ylim(ax3_y2_ylim_1, ax3_y2_ylim_2)


##### plot axes #################################

# limits
ax3.set_xlim(ax3_xlim_1, ax3_xlim_2)
ax3.set_ylim(ax3_ylim_1, ax3_ylim_2)

# labels
ax3.set_xticks(xticks_femto)
ax3.set_xticklabels(xticklabels_femto, size=13)
ax3.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='on', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax3.yaxis.set_major_formatter(FormatStrFormatter('%.5e'))

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -100, 100]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax3.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax3.add_line(line)




###################################################################################################
#  Legend:
###################################################################################################

ax1.legend(handles=[ax1_data1, ax1_data3, ax1_data2, ax1_data4], prop={'size': 12}, loc=(0.13,1.30), ncol=6)
#ax1.legend(handles=[ax1_datal,ax1_datall,ax2_datal,ax1_data2,ax1_data18,ax2_data10,ax1_data1,ax1_data17,ax2_data9], prop={'size': 12}, loc=( 0.16,1.32), ncol=3)




###################################################################################################
#  Save figure:
###################################################################################################

DPI = fig.get_dpi()
fig.set_size_inches(1500.0/float(DPI),800.0/float(DPI))

fig.savefig('Na_297_dimer_0_1_nm_fixed_ions_lda_gga_pbe_comparison.png',bbox_inches='tight',pad_inches = 0)

plt.show()

