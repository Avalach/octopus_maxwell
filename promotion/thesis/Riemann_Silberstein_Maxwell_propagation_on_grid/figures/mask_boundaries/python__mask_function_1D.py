import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.patches as patches
import matplotlib.patches as mpatches

def f1(x,w):
    return np.power(np.sin(x * np.pi / (2 * w)),2)

def f2(x,w):
    return np.power(np.sin(x * np.pi / (2 * w)),2)

fig = plt.figure()

ax = plt.axes()

x1 = np.arange(-10., -5., 0.01)
#x2 = np.arange( -5.,  5., 0.2)
x3 = np.arange( 5., 10., 0.01)

plt.plot(x1, f1(x1,5), 'r-', linewidth=2)
plt.plot(x3, f2(x3,5), 'r-', linewidth=2)
plt.hlines(y=1.01, xmin=-5.0, xmax=5.0, linewidth=2, color='r')

r1 = [-5.0,-5.0]
r2 = [ 0.0, 1.1]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [ 5.0, 5.0]
r2 = [ 0.0, 1.1]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

#ax  = fig.add_subplot(111, aspect=4)
ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

ax.text( -6.0,-0.2,r'$-b_u$',fontsize=15)
ax.text(  4.0,-0.2,r'$+b_u$',fontsize=15)
ax.text(-10.5,-0.2,r'$-L_u$',fontsize=15)
ax.text(  9.4,-0.2,r'$+L_u$',fontsize=15)
ax.text(-11.5, 0.95,r'$1.0$' ,fontsize=15)
ax.text(-11.5, 0.45,r'$0.5$' ,fontsize=15)
ax.text(-11.5,-0.05,r'$0.0$' ,fontsize=15)

ax.text( -3.8, 0.5, 'inner simulation box', fontsize=15)
#ax.text( -8.0, 1.0, 'absorbing',  fontsize=15)
#ax.text( -8.0, 0.5, 'boundaries', fontsize=15)
#ax.text(  8.0, 1.0, 'absorbing',  fontsize=15)
#ax.text(  8.0, 0.5, 'boundaries', fontsize=15)

ax.add_patch(patches.Rectangle(( -10,  0),  5.0,  1.1, ec='none', alpha=0.5, color=(0.588,0.588,0)))
ax.add_patch(patches.Rectangle((   5,  0),  5.0,  1.1, ec='none', alpha=0.5, color=(0.588,0.588,0)))

mask_region = mpatches.Patch(color=(0.588,0.588,0), alpha=0.6, label='Absorbing boundaries')
plt.legend(handles=[mask_region], prop={'size': 15}, loc=(0.23,1.1))

ax.set_xlim(-10,10)
ax.set_ylim(0.0,1.1)

ax.set_aspect(4)

DPI = fig.get_dpi()
fig.set_size_inches(720.0/float(DPI),220.0/float(DPI))

fig.savefig('mask_function_1D.png')

plt.show()
