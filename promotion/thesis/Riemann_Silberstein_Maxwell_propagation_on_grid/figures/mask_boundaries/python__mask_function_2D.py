import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def f1(x,w):
    return np.power(np.sin(x * np.pi / (2 * w)),2)

def f2(x,w):
    return np.power(np.sin(x * np.pi / (2 * w)),2)

#fig = plt.figure(figsize=plt.figaspect(2)*1.0)
fig = plt.figure()
ax = fig.gca(projection='3d')

L=10.0
x = np.arange(-L, L+0.01, 0.1)
y = np.arange(-L, L+0.01, 0.1)

x, y = np.meshgrid(x, y)

width = 4.0
bound = L-width

z = np.zeros((len(x),len(y)))

for ii in range( 0, len(x)):
    for jj in range( 0, len(y)):
        if ( (np.absolute(x[ii,jj])-bound >= 0.0) and (np.absolute(y[ii,jj])-bound >= 0.0) ):
           z[ii,jj] = (1.0 - np.power(np.sin((np.absolute(x[ii,jj])-bound)*np.pi/(2*width)),2)) * (1.0 - np.power(np.sin((np.absolute(y[ii,jj])-bound)*np.pi/(2*width)),2))
        elif (np.absolute(x[ii,jj])-bound >= 0.0):
           z[ii,jj] = 1.0 - np.power(np.sin((np.absolute(x[ii,jj])-bound)*np.pi/(2*width)),2)
        elif (np.absolute(y[ii,jj])-bound >= 0.0):
           z[ii,jj] = 1.0 - np.power(np.sin((np.absolute(y[ii,jj])-bound)*np.pi/(2*width)),2)
        else:
           z[ii,jj] = 1.0

surf = ax.plot_surface(x, y, z, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)

cbar = fig.colorbar(surf, shrink=0.5, aspect=7)
cbar.set_clim(0.0,1.0)

#plt.plot(x1, f1(x1,5), 'r-', linewidth=2)
#plt.plot(x3, f2(x3,5), 'r-', linewidth=2)
#plt.hlines(y=1.01, xmin=-5.0, xmax=5.0, linewidth=2, color='r')

#r1 = [-5.0,-5.0]
#r2 = [ 0.0, 1.1]
#line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
#ax.add_line(line)

#r1 = [ 5.0, 5.0]
#r2 = [ 0.0, 1.1]
#line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
#ax.add_line(line)

#ax  = fig.add_subplot(111, aspect=4)
#ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

#ax.add_patch(patches.Rectangle(( -10,  0),  5.0,  1.1, ec='none', alpha=0.5, color=(0.588,0.588,0)))
#ax.add_patch(patches.Rectangle((   5,  0),  5.0,  1.1, ec='none', alpha=0.5, color=(0.588,0.588,0)))

#mask_region = mpatches.Patch(color=(0.588,0.588,0), alpha=0.6, label='Absorbing boundaries')
#plt.legend(handles=[mask_region], prop={'size': 15}, loc=(0.23,1.1))

#ax.set_xlim(-10,10)
#ax.set_ylim(0.0,1.1)

#ax.set_aspect(4)

ax.view_init(60, 50)

DPI = fig.get_dpi()
fig.set_size_inches(720.0/float(DPI),420.0/float(DPI))

fig.savefig('mask_function_2D.png')

plt.show()
