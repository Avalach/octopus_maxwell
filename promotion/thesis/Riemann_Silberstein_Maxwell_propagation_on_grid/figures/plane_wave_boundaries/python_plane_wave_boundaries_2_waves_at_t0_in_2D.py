import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

# ax1.add_patch(patches.Rectangle((x,y),width,height)

# incident wave boundaries
ax.add_patch(patches.Rectangle((-10,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle((  8,-10),  2, 20, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle(( -8,  8), 16,  2, ec='none', alpha=0.7, color=(0,0.392,0)))
ax.add_patch(patches.Rectangle(( -8,-10), 16,  2, ec='none', alpha=0.7, color=(0,0.392,0)))

r1 = [-10,-10]
r2 = [ 10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [ 10, 10]
r2 = [-10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [-10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [ 10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

shift=-7

#plt.arrow(shift,0,7,0)

shift1x=-17.6777
shift1y=17.6777

k1x= 0.707107
k1y=-0.707107
k1abs= 1.0
width1=4.0

shift2x=22.3607
shift2y=11.1804

k2x=-0.447214
k2y=-0.223607
k2abs= 0.5
width2=6.0

# inner box
x = np.linspace(-5.0, 5.0, 100)
y = np.linspace(-5.0, 5.0, 100)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, N=100, alpha=1.0)

# pml border left
x = np.linspace(-8.0,-5.0,  30)
y = np.linspace(-5.0, 5.0, 100)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# pml border right
x = np.linspace( 5.0, 8.0,  30)
y = np.linspace(-5.0, 5.0, 100)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# pml border top
x = np.linspace(-5.0, 5.0, 100)
y = np.linspace( 5.0, 8.0,  30)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# pml border bottom
x = np.linspace(-5.0, 5.0, 130)
y = np.linspace(-8.0,-5.0,  30)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))

levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# pml border left top
x = np.linspace(-8.0,-5.0,  30)
y = np.linspace( 5.0, 8.0,  30)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2)) 
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# pml border right top
x = np.linspace( 5.0, 8.0,  30)
y = np.linspace( 5.0, 8.0,  30)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# pml border left bottom
x = np.linspace(-8.0,-5.0,  30)
y = np.linspace(-8.0,-5.0,  30)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# pml border right bottom
x = np.linspace( 5.0, 8.0,  30)
y = np.linspace(-8.0,-5.0,  30)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# incident border left
x = np.linspace(-10.0,-8.0,  20)
y = np.linspace(-10.0,10.0, 200)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=0.6)

# incident border right
x = np.linspace(  8.0,10.0,  20)
y = np.linspace(-10.0,10.0, 200)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=0.6)

# incident top
x = np.linspace( -8.0, 8.0, 160)
y = np.linspace(  8.0,10.0,  20)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=0.6)

# incident bottom
x = np.linspace( -8.0, 8.0, 160)
y = np.linspace(-10.0,-8.0,  20)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-2.4,2.4,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=0.6)

# outside box top
x = np.linspace(-30.0, 30.0, 600)
y = np.linspace( 10.0, 15.0,  50)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))

levels = np.linspace(-6.0,6.0,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)
# outside box bottom
x = np.linspace(-30.0, 30.0, 600)
y = np.linspace(-15.0,-10.0,  50)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-6.0,6.0,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0) 

# outside box left
x = np.linspace(-30.0,-10.0, 200)
y = np.linspace(-10.0, 10.0, 200)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-6.0,6.0,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

# outside box right
x = np.linspace( 10.0, 30.0, 200)
y = np.linspace(-10.0, 10.0, 200)
X, Y = np.meshgrid(x, y)
Z1 = np.cos(k1x*(X-shift1x)+k1y*(Y-shift1y)) * np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2)) 
Z2 = np.cos(k2x*(X-shift2x)+k2y*(Y-shift2y)) * np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
#Z1 = np.exp(- ( (k1x*(X-shift1x) + k1y*(Y-shift1y))/k1abs )**2/(2*width1**2))
#Z2 = np.exp(- ( (k2x*(X-shift2x) + k2y*(Y-shift2y))/k2abs )**2/(2*width2**2))
levels = np.linspace(-6.0,6.0,1000)
cs = ax.contourf(X, Y, Z1+Z2, levels, cmap=cm.seismic, alpha=1.0)

#cbar = fig.colorbar(cs)
#cbar.ax.set_yticklabels([''])

#point, = plt.plot(0, 0, marker='o', markersize=20, color=(0.588,0.294,0))

#plane_wave_region = mpatches.Patch(color=(0,0.392,0), alpha=0.7, label='Incident waves region')
#pml_single_region    = mpatches.Patch(color='yellowgreen', alpha=0.6, label='PML single region')
#pml_overlap_region   = mpatches.Patch(color='olive', alpha=0.6, label='PML overlap region')
#plt.legend(handles=[plane_wave_region, pml_single_region, pml_overlap_region], prop={'size': 10}, loc=9)

plane_wave_region = mpatches.Patch(color=(0,0.392,0), alpha=0.7, label='Incident plane waves region')
plt.legend(handles=[plane_wave_region], prop={'size':20}, loc=9)

for ii in range(-10, 10, 1):
   r1 = [  ii, ii]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)
   r1 = [ -10, 10]
   r2 = [  ii, ii]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle=':')
   ax.add_line(line)

ax.set_xlim(-30,30)
ax.set_ylim(-15,15)

#ax.set_xlim(-15,15)
#ax.set_ylim(-15,15)

#major_ticks = np.arange(-15, 15, 5)
#minor_ticks = np.arange(-15, 15, 1)

#ax.set_xticks(major_ticks)                                                       
#ax.set_xticks(minor_ticks, minor=True)                                           
#ax.set_yticks(major_ticks)                                                       
#ax.set_yticks(minor_ticks, minor=True)

#ax.grid(which='minor', alpha=0.5)                                                
#ax.grid(which='major', alpha=1.0)

ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

plt.axis('off')

DPI = fig.get_dpi()
fig.set_size_inches(10,6)

fig.tight_layout()
fig.savefig('plane_wave_boundaries_2_waves_at_t0_in_2D.png',bbox_inches='tight',pad_inches = 0)

plt.show()
