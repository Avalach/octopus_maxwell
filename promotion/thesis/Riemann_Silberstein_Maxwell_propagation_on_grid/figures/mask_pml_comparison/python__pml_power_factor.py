import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from scipy.fftpack import fft, ifft
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(figsize=(1,10))

gs = gridspec.GridSpec(29, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

ax1 = plt.subplot(gs[0:14,:])
ax2 = plt.subplot(gs[15:29,0])
#ax3 = plt.subplot(gs[4:8,0])
#ax4 = plt.subplot(gs[8:12,0])
#ax5 = plt.subplot(gs[12:16,0])


with open('./02_pml/energy_coefficient_cpml_width_0.5') as data1:
    readlines = data1.readlines()
    x_data1 = [float(line.split()[0]) for line in readlines]
    y_data1 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/energy_coefficient_cpml_width_1.0') as data2:
    readlines = data2.readlines()
    x_data2 = [float(line.split()[0]) for line in readlines]
    y_data2 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/energy_coefficient_cpml_width_1.5') as data3:
    readlines = data3.readlines()
    x_data3 = [float(line.split()[0]) for line in readlines]
    y_data3 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/energy_coefficient_cpml_width_2.0') as data4:
    readlines = data4.readlines()
    x_data4 = [float(line.split()[0]) for line in readlines]
    y_data4 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/energy_coefficient_cpml_width_2.5') as data5:
    readlines = data5.readlines()
    x_data5 = [float(line.split()[0]) for line in readlines]
    y_data5 = [float(line.split()[1]) for line in readlines]


with open('./02_pml/reflection_coefficient_08_00_00_cpml_width_0.5') as data6:
    readlines = data6.readlines()
    x_data6 = [float(line.split()[0]) for line in readlines]
    y_data6 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_00_00_cpml_width_1.0') as data7:
    readlines = data7.readlines()
    x_data7 = [float(line.split()[0]) for line in readlines]
    y_data7 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_00_00_cpml_width_1.5') as data8:
    readlines = data8.readlines()
    x_data8 = [float(line.split()[0]) for line in readlines]
    y_data8 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_00_00_cpml_width_2.0') as data9:
    readlines = data9.readlines()
    x_data9 = [float(line.split()[0]) for line in readlines]
    y_data9 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_00_00_cpml_width_2.5') as data10:
    readlines = data10.readlines()
    x_data10 = [float(line.split()[0]) for line in readlines]
    y_data10 = [float(line.split()[1]) for line in readlines]


with open('./02_pml/reflection_coefficient_08_08_00_cpml_width_0.5') as data11:
    readlines = data11.readlines()
    x_data11 = [float(line.split()[0]) for line in readlines]
    y_data11 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_08_00_cpml_width_1.0') as data12:
    readlines = data12.readlines()
    x_data12 = [float(line.split()[0]) for line in readlines]
    y_data12 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_08_00_cpml_width_1.5') as data13:
    readlines = data13.readlines()
    x_data13 = [float(line.split()[0]) for line in readlines]
    y_data13 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_08_00_cpml_width_2.0') as data14:
    readlines = data14.readlines()
    x_data14 = [float(line.split()[0]) for line in readlines]
    y_data14 = [float(line.split()[1]) for line in readlines]

with open('./02_pml/reflection_coefficient_08_08_00_cpml_width_2.5') as data15:
    readlines = data15.readlines()
    x_data15 = [float(line.split()[0]) for line in readlines]
    y_data15 = [float(line.split()[1]) for line in readlines]




###################################################################################################
#  Plots:
###################################################################################################

fig.suptitle(r"Relative reflection error of the perfectly matched layer", size=18)

ax1_data1, = ax1.semilogy(x_data1, y_data1, 'o', linewidth=1.5, color=(0.118,0.118,0.588), label=r'$w_{\mathrm{ab}}=0.5$')
ax1_data2, = ax1.semilogy(x_data2, y_data2, 'o', linewidth=1.5, color=(0.588,0.118,0.118), label=r'$w_{\mathrm{ab}}=1.0$')
ax1_data3, = ax1.semilogy(x_data3, y_data3, 'o', linewidth=1.5, color=(0.118,0.588,0.118), label=r'$w_{\mathrm{ab}}=1.5$')
ax1_data4, = ax1.semilogy(x_data4, y_data4, 'o', linewidth=1.5, color=(1.000,0.705,0.000), label=r'$w_{\mathrm{ab}}=2.0$')
ax1_data5, = ax1.semilogy(x_data5, y_data5, 'o', linewidth=1.5, color=(0.600,0.000,0.600), label=r'$w_{\mathrm{ab}}=2.5$')

ax1_data1, = ax1.semilogy(x_data1, y_data1, '-', linewidth=1.5, color=(0.118,0.118,0.588), label=r'$w_{\mathrm{ab}}=0.5$')
ax1_data2, = ax1.semilogy(x_data2, y_data2, '-', linewidth=1.5, color=(0.588,0.118,0.118), label=r'$w_{\mathrm{ab}}=1.0$')
ax1_data3, = ax1.semilogy(x_data3, y_data3, '-', linewidth=1.5, color=(0.118,0.588,0.118), label=r'$w_{\mathrm{ab}}=1.5$')
ax1_data4, = ax1.semilogy(x_data4, y_data4, '-', linewidth=1.5, color=(1.000,0.705,0.000), label=r'$w_{\mathrm{ab}}=2.0$')
ax1_data5, = ax1.semilogy(x_data5, y_data5, '-', linewidth=1.5, color=(0.700,0.000,0.700), label=r'$w_{\mathrm{ab}}=2.5$')


ax2_data1, = ax2.semilogy(x_data6,  y_data6, 'o', linewidth=1.5, color=(0.118,0.118,0.588), label=r'$w_{\mathrm{ab}}=0.5$ and $\vec{r}_{(1)}$')
ax2_data2, = ax2.semilogy(x_data7,  y_data7, 'o', linewidth=1.5, color=(0.588,0.118,0.118), label=r'$w_{\mathrm{ab}}=1.0$ and $\vec{r}_{(1)}$')
ax2_data3, = ax2.semilogy(x_data8,  y_data8, 'o', linewidth=1.5, color=(0.118,0.588,0.118), label=r'$w_{\mathrm{ab}}=1.5$ and $\vec{r}_{(1)}$')
ax2_data4, = ax2.semilogy(x_data9,  y_data9, 'o', linewidth=1.5, color=(1.000,0.705,0.000), label=r'$w_{\mathrm{ab}}=2.0$ and $\vec{r}_{(1)}$')
ax2_data5, = ax2.semilogy(x_data10, y_data10,'o', linewidth=1.5, color=(0.600,0.000,0.600), label=r'$w_{\mathrm{ab}}=2.5$ and $\vec{r}_{(1)}$')

ax2_data1, = ax2.semilogy(x_data6,  y_data6, '-.', dashes=(3.0,4.0), linewidth=1.5, color=(0.118,0.118,0.588), label=r'$w_{\mathrm{ab}}=0.5$ and $\vec{r}_{(1)}$')
ax2_data2, = ax2.semilogy(x_data7,  y_data7, '-.', dashes=(3.0,4.0), linewidth=1.5, color=(0.588,0.118,0.118), label=r'$w_{\mathrm{ab}}=1.0$ and $\vec{r}_{(1)}$')
ax2_data3, = ax2.semilogy(x_data8,  y_data8, '-.', dashes=(3.0,4.0), linewidth=1.5, color=(0.118,0.588,0.118), label=r'$w_{\mathrm{ab}}=1.5$ and $\vec{r}_{(1)}$')
ax2_data4, = ax2.semilogy(x_data9,  y_data9, '-.', dashes=(3.0,4.0), linewidth=1.5, color=(1.000,0.705,0.000), label=r'$w_{\mathrm{ab}}=2.0$ and $\vec{r}_{(1)}$')
ax2_data5, = ax2.semilogy(x_data10, y_data10,'-.', dashes=(3.0,4.0), linewidth=1.5, color=(0.700,0.000,0.700), label=r'$w_{\mathrm{ab}}=2.5$ and $\vec{r}_{(1)}$')


ax2_data6, = ax2.semilogy(x_data11, y_data11, 'o', linewidth=1.5, color=(0.118,0.118,0.588), label=r'$w_{\mathrm{ab}}=0.5$ and $\vec{r}_{(2)}$')
ax2_data7, = ax2.semilogy(x_data12, y_data12, 'o', linewidth=1.5, color=(0.588,0.118,0.118), label=r'$w_{\mathrm{ab}}=1.0$ and $\vec{r}_{(2)}$')
ax2_data8, = ax2.semilogy(x_data13, y_data13, 'o', linewidth=1.5, color=(0.118,0.588,0.118), label=r'$w_{\mathrm{ab}}=1.5$ and $\vec{r}_{(2)}$')
ax2_data9, = ax2.semilogy(x_data14, y_data14, 'o', linewidth=1.5, color=(1.000,0.705,0.000), label=r'$w_{\mathrm{ab}}=2.0$ and $\vec{r}_{(2)}$')
ax2_data10, = ax2.semilogy(x_data15, y_data15,'o', linewidth=1.5, color=(0.600,0.000,0.600), label=r'$w_{\mathrm{ab}}=2.5$ and $\vec{r}_{(2)}$')

ax2_data6, = ax2.semilogy(x_data11, y_data11, ':', dashes=(1.0,1.0), linewidth=1.5, color=(0.118,0.118,0.588), label=r'$w_{\mathrm{ab}}=0.5$ and $\vec{r}_{(2)}$')
ax2_data7, = ax2.semilogy(x_data12, y_data12, ':', dashes=(1.0,1.0), linewidth=1.5, color=(0.588,0.118,0.118), label=r'$w_{\mathrm{ab}}=1.0$ and $\vec{r}_{(2)}$')
ax2_data8, = ax2.semilogy(x_data13, y_data13, ':', dashes=(1.0,1.0), linewidth=1.5, color=(0.118,0.588,0.118), label=r'$w_{\mathrm{ab}}=1.5$ and $\vec{r}_{(2)}$')
ax2_data9, = ax2.semilogy(x_data14, y_data14, ':', dashes=(1.0,1.0), linewidth=1.5, color=(1.000,0.705,0.000), label=r'$w_{\mathrm{ab}}=2.0$ and $\vec{r}_{(2)}$')
ax2_data10, = ax2.semilogy(x_data15, y_data15,':', dashes=(1.0,1.0), linewidth=1.5, color=(0.700,0.000,0.700), label=r'$w_{\mathrm{ab}}=2.5$ and $\vec{r}_{(2)}$')



###################################################################################################
#  Panel 1: energy error coefficient
###################################################################################################
ax1.text(0.63,0.5,'a)',size=18)

# Axes label bottom
#ax1.set_xlabel(r"PML exponent $q$", size=15)
#ax1.xaxis.set_label_position('bottom')

ax1.set_ylabel(r"C$_{E}$", size=15)
ax1.yaxis.set_label_position('left')

# labels
ax1.set_xticks([1.0,2.0,3.0,4.0,5.0])
ax1.set_xticklabels([1.0,2.0,3.0,4.0,5.0], size=12)
ax1.set_yticks([1e-7, 1e-5, 1e-3, 1e-1])
ax1.set_yticklabels([1e-7, 1e-5, 1e-3, 1e-1], size=12)
#ax1.set_
ax1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# Limits
ax1.set_xlim(0.5,5.5)
ax1.set_ylim(10**(-9), 10)



###################################################################################################
#  Panel 2: electric field reflection coefficient
###################################################################################################
ax2.text(0.63,2.0,'b)',size=18)

# Axes label bottom
ax2.set_xlabel(r"PML exponent $q$", size=15)
ax2.xaxis.set_label_position('bottom')

ax2.set_ylabel(r"R", size=15)
ax2.yaxis.set_label_position('left')

# labels
ax2.set_xticks([1.0,2.0,3.0,4.0,5.0])
ax2.set_xticklabels([1.0,2.0,3.0,4.0,5.0], size=12)
ax2.set_yticks([1e-3,1e-2,1e-1,1e0])
ax2.set_yticklabels([1e-3,1e-2,1e-1,1e0], size=12)
ax2.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='on', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# Limits
ax2.set_xlim(0.5,5.5)
ax2.set_ylim(10**(-4), 10)


ax1.xaxis.grid(True, which='major')
ax1.yaxis.grid(True, which='major')

ax2.xaxis.grid(True,  which='major')
ax2.yaxis.grid(True, which='major')


#ax1.legend(handles=[ax1_data1, ax2_data1, ax2_data6, ax1_data2, ax2_data2, ax2_data7, ax1_data3, ax2_data3, ax2_data8, ax1_data4, ax2_data4, ax2_data8, ax1_data5, ax2_data5, ax2_data10], prop={'size': 12}, loc=( 1.02,-0.73))

ax1.legend(handles=[ax1_data1,ax1_data2,ax1_data3,ax1_data4,ax1_data5, ax2_data1,ax2_data2,ax2_data3,ax2_data4,ax2_data5,ax2_data6,ax2_data7,ax2_data8,ax2_data9,ax2_data10], prop={'size': 11}, loc=( 1.02,-0.925))

DPI = fig.get_dpi()
fig.set_size_inches(720.0/float(DPI),600.0/float(DPI))

fig.savefig('pml_power_factor.png',bbox_inches='tight',pad_inches = 0)

plt.show()
