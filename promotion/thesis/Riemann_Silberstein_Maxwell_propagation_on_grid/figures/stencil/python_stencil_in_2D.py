import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

# ax1.add_patch(patches.Rectangle((x,y),width,height)

#ax.add_patch(patches.Rectangle((-10,-10),  3, 20, ec='none', alpha=0.5, color='black'))
#ax.add_patch(patches.Rectangle((  7,-10),  3, 20, ec='none', alpha=0.5, color='black'))
#ax.add_patch(patches.Rectangle(( -7,  7), 14,  3, ec='none', alpha=0.5, color='black'))
#ax.add_patch(patches.Rectangle(( -7,-10), 14,  3, ec='none', alpha=0.5, color='black'))

#ax.add_patch(patches.Rectangle((-7,-7), 14, 14, ec='none', alpha=0.2, color='black'))

#r1 = [-10,-10]
#r2 = [ 10,-10]
#line = lines.Line2D(r1, r2, color='black', lw=2.5)
#ax.add_line(line)

#r1 = [ 10, 10]
#r2 = [-10, 10]
#line = lines.Line2D(r1, r2, color='black', lw=2.5)
#ax.add_line(line)

#r1 = [-10, 10]
#r2 = [-10,-10]
#line = lines.Line2D(r1, r2, color='black', lw=2.5)
#ax.add_line(line)

#r1 = [-10, 10]
#r2 = [ 10, 10]
#line = lines.Line2D(r1, r2, color='black', lw=2.5)
#ax.add_line(line)

x = 0
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(0.684,0.000,0.684), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = -2
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 2
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = -4
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 4
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = -6
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 6
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = -8
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 8
y = 0
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = -2
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = 2
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = -4
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = 4
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = -6
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = 6
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = -8
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

x = 0
y = 8
point, = plt.plot(x, y, marker='o', markersize=20, color=(1.000,0.000,1.000), alpha=1.0, markeredgewidth=2, markeredgecolor='black')

for ii in range(-8, 0, 2):
   for jj in range(-8, 0, 2):
      x = ii 
      y = jj
      point, = plt.plot(x, y, marker='o', markersize=20, color=(0.745,0.745,0.745), alpha=1.0, markeredgewidth=1, markeredgecolor='black')

for ii in range(-8, 0, 2):
   for jj in range(2, 10, 2):
      x = ii 
      y = jj
      point, = plt.plot(x, y, marker='o', markersize=20, color=(0.745,0.745,0.745), alpha=1.0, markeredgewidth=1, markeredgecolor='black')

for ii in range(2, 10, 2):
   for jj in range(-8, 0, 2):
      x = ii 
      y = jj
      point, = plt.plot(x, y, marker='o', markersize=20, color=(0.745,0.745,0.745), alpha=1.0, markeredgewidth=1, markeredgecolor='black')

for ii in range(2, 10, 2):
   for jj in range(2, 10, 2):
      x = ii 
      y = jj
      point, = plt.plot(x, y, marker='o', markersize=20, color=(0.745,0.745,0.745), alpha=1.0, markeredgewidth=1, markeredgecolor='black')


r1 = [-0.3,-1.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [ 0.3, 1.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [-2.3,-3.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [ 2.3, 3.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [-4.3,-5.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [ 4.3, 5.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [-6.3,-7.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [ 6.3, 7.7]
r2 = [   0,   0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)


r1 = [   0,   0]
r2 = [-0.3,-1.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [   0,   0]
r2 = [ 0.3, 1.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [   0,   0]
r2 = [-2.3,-3.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [   0,   0]
r2 = [ 2.3, 3.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [   0,   0]
r2 = [-4.3,-5.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [   0,   0]
r2 = [ 4.3, 5.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [   0,   0]
r2 = [-6.3,-7.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

r1 = [   0,   0]
r2 = [ 6.3, 7.7]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=2.0, linestyle='--')
ax.add_line(line)

size=15

ax.text(0.3, -0.9, 'i,j', fontsize=size)

ax.text(1.5, -0.9, 'i+1,j', fontsize=size)

ax.text(3.5, -0.9, 'i+2,j', fontsize=size)

ax.text(5.5, -0.9, 'i+3,j', fontsize=size)

ax.text(7.5, -0.9, 'i+4,j', fontsize=size)

ax.text(-2.5, -0.9, 'i-1,j', fontsize=size)

ax.text(-4.5, -0.9, 'i-2,j', fontsize=size)

ax.text(-6.5, -0.9, 'i-3,j', fontsize=size)

ax.text(-8.5, -0.9, 'i-4,j', fontsize=size)

ax.text(0.3, 1.1, 'i,j+1', fontsize=size)

ax.text(0.3, 3.1, 'i,j+2', fontsize=size)

ax.text(0.3, 5.1, 'i,j+3', fontsize=size)

ax.text(0.3, 7.1, 'i,j+4', fontsize=size)

ax.text(0.3, -2.9, 'i,j-1', fontsize=size)

ax.text(0.3, -4.9, 'i,j-2', fontsize=size)

ax.text(0.3, -6.9, 'i,j-3', fontsize=size)

ax.text(0.3, -8.9, 'i,j-4', fontsize=size)


ax.set_xlim(-10,10)
ax.set_ylim(-10,11)

RS_points     = mpatches.Patch(color=(0.684,0.000,0.684), alpha=1.0, label='RS grid point')
RS_points_der = mpatches.Patch(color=(1.000,0.000,1.000), alpha=1.0, label='RS grid points for derivative calculation')

plt.legend(handles=[RS_points, RS_points_der], prop={'size': 18}, loc=9)

#major_ticks = np.arange(-10, 10, 5)
#minor_ticks = np.arange(-10, 10, 1)

#ax.set_xticks(major_ticks)                                                       
#ax.set_xticks(minor_ticks, minor=True)                                           
#ax.set_yticks(major_ticks)                                                       
#ax.set_yticks(minor_ticks, minor=True)

#ax.grid(which='minor', alpha=0.5)                                                
#ax.grid(which='major', alpha=1.0)

DPI = fig.get_dpi()
fig.set_size_inches(12,12)

ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

plt.axis('off')

fig.savefig('stencil_in_2D.png',bbox_inches='tight',pad_inches = 0)
fig.savefig('stencil_in_2D.svg',bbox_inches='tight',pad_inches = 0)

#plt.show()
