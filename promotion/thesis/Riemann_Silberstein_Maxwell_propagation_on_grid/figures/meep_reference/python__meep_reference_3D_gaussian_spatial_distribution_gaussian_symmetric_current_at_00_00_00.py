import numpy as np
import matplotlib.pyplot as plt

def f0(x,x1,x2,w):
    return np.exp(-np.power(x-x1,2)/2*(np.power(w,2))) - np.exp(-np.power(x-x2,2)/2*(np.power(w,2)))

fig, ax1 = plt.subplots()

x0 = np.arange(0.0, 30.0, 0.01)

with open('../../../../thesis_runs/Riemann_Silberstein_Maxwell_propagation_on_grid/meep_reference/octopus_data/02__box_size_20_20_20__dx_0.25_dy_0.25_dz_0.25__gaussian_symmetric__current_time_width_1.0__current_time_shift_10.0__current_amplitude_1.0/maxwell_e_field-z.z=0_at_00_00_00') as f1:
    lines = f1.readlines()
    x1 = [line.split()[0] for line in lines]
    y1 = [line.split()[1] for line in lines]

with open('../../../../thesis_runs/Riemann_Silberstein_Maxwell_propagation_on_grid/meep_reference/meep_data/02__box_size_20_20_20__dx_0.20_dy_0.20_dz_0.20__gaussian_symmetric__current_time_width_1.0__current_time_shift_10.0__current_amplitude_1.0/Field-z_00_00_00/Ez.dat') as f2:
    lines = f2.readlines()
    x2 = [line.split()[0] for line in lines]
    y2 = [line.split()[1] for line in lines]

for ii in range(0,len(y2)):
    y2[ii] = y2[ii].split("+")[0] 

ax2 = ax1.twinx()

plt.title("Reference calculation at grid point (0,0,0)")    

data0, = ax2.plot(x0,f0(x0,10.0,12.0,1.0), '-', color=(1.000,0.700,0.000), label='external current density')
data1, = ax1.plot(x1,y1, '--', color=(1.000,0.000,0.000), label='octopus calculation')
data2, = ax1.plot(x2,y2, '-.', color=(0.000,0.000,1.000), label='meep calculation')

ax1.set_xlim(  0.0,30.0)
ax1.set_ylim(- 1.1, 1.1)

ax2.set_xlim( 0.0,30.0)
ax2.set_ylim(-1.1, 1.1)

ax1.set_xlabel('time', size=12)
ax1.set_ylabel('electric field in MEEP units', size=12)

ax2.set_ylabel('external current density', color=(1.000,0.700,0.000), size=12)

plt.legend(handles=[data0,data1,data2], prop={'size': 12}, loc=(0.5,0.75))

DPI = fig.get_dpi()
fig.set_size_inches(740.0/float(DPI),520.0/float(DPI))

fig.savefig('meep_reference_3D_gaussian_spatial_distribution_gaussian_symmetric_current_at_00_00_00.png')

plt.show()
