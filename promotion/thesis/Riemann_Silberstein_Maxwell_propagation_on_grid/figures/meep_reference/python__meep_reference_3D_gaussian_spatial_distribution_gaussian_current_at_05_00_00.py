import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import FormatStrFormatter

def f1(x,x0,w):
    return np.exp(-np.power(x-x0,2)/2*(np.power(w,2))) * np.exp(-np.power(5.0,2)/2.0)

fig, ax = plt.subplots()

x1 = np.arange(0.0, 30.0, 0.01)

with open('../../../../thesis_runs/Riemann_Silberstein_Maxwell_propagation_on_grid/meep_reference/octopus_data/01__box_size_20_20_20__dx_0.25_dy_0.25_dz_0.25__gaussian__current_time_width_1.0__current_time_shift_10.0__current_amplitude_1.0/maxwell_e_field-z.z=0_at_05_00_00') as f2:
    lines = f2.readlines()
    x2 = [line.split()[0] for line in lines]
    y2 = [line.split()[1] for line in lines]

with open('meep_data/01__box_size_20_20_20__dx_0.20_dy_0.20_dz_0.20__gaussian__current_time_width_1.0__current_time_shift_10.0__current_amplitude_1.0/Field-z_05_00_00/Ez.dat') as f3:
    lines = f3.readlines()
    x3 = [line.split()[0] for line in lines]
    y3 = [line.split()[1] for line in lines]

for ii in range(0,len(y3)):
    y3[ii] = y3[ii].split("+")[0] 

x4 = x3
y4 = []
for ii in range(1,len(y2)):
    y4.append( (float(y2[ii])-float(y3[ii-1])) )


with open('../../../../thesis_runs/Riemann_Silberstein_Maxwell_propagation_on_grid/meep_reference/octopus_data/01__box_size_20_20_20__dx_0.25_dy_0.25_dz_0.25__gaussian__current_time_width_1.0__current_time_shift_10.0__current_amplitude_1.0/maxwell_b_field-y.z=0_at_05_00_00') as f5:
    lines = f5.readlines()
    x5 = [line.split()[0] for line in lines]
    y5 = [line.split()[1] for line in lines]

with open('../../../../thesis_runs/Riemann_Silberstein_Maxwell_propagation_on_grid/meep_reference/meep_data/01__box_size_20_20_20__dx_0.20_dy_0.20_dz_0.20__gaussian__current_time_width_1.0__current_time_shift_10.0__current_amplitude_1.0/Field-z_05_00_00/Hy.dat') as f6:
    lines = f6.readlines()
    x6 = [line.split()[0] for line in lines]
    y6 = [line.split()[1] for line in lines]

for ii in range(0,len(y6)):
    y6[ii] = y6[ii].split("+")[0] 

x7 = x6
y7 = []
for ii in range(1,len(y5)):
    y7.append( (float(y5[ii])-float(y6[ii-1])) )

x8 = x1
y8 = []
for ii in range(0, len(x8)):
    y8.append(0.0)

gs = gridspec.GridSpec(10, 2)
gs.update(hspace=0.5)
gs.update(wspace=0.45)

ax1 = plt.subplot(gs[1:4,0], sharex=ax)

ax2 = plt.subplot(gs[4:7,0], sharex=ax)

ax3 = plt.subplot(gs[7:10,0], sharex=ax)

ax4 = plt.subplot(gs[1:4,1], sharex=ax)

ax5 = plt.subplot(gs[4:7,1], sharex=ax)

ax6 = plt.subplot(gs[7:10,1], sharex=ax)


fig.suptitle("Comparative calculation at grid point (5,0,0)")

ax1.set_title('Electric field in z-direction')
ax1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off')
ax1.text(-0.3, 3.5e-6, 'current density', size=12, rotation=90)

ax2.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off')
ax2.text(-0.3, 5e-2, 'electric field', size=12, rotation=90)

ax3.set_xlabel('Time in Meep units', size=12)
ax3.text(-0.3, 2.0e-3, 'electric field diff.', size=12, rotation=90)

ax4.set_title('Magnetic field in z-direction')
ax4.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off')
ax4.text(-0.3, 3.5e-6, 'current density', size=12, rotation=90)

ax5.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off')
ax5.text(-0.3, 8.0e-2, 'magnetic field', size=12, rotation=90)

ax6.set_xlabel('Time in Meep units', size=12)
ax6.text(-0.3,  5.0e-4, 'magnetic field diff.', size=12, rotation=90)


data1, = ax1.plot(x1,f1(x1,10.0,1.0), '-', color=(1.000,0.700,0.000), linewidth=2.0, label='external current density')
#data2, = ax1.plot(x8,y8, '-', color=(0.000,0.000,0.000), linewidth=0.5)

data3, = ax2.plot(x2,y2, '--', color=(1.000,0.000,0.000), linewidth=2.0, label='octopus calculation')
data4, = ax2.plot(x3,y3, '-.', color=(0.000,0.000,1.000), label='meep calculation')
data5, = ax2.plot(x8,y8, '-' , color=(0.000,0.000,0.000), linewidth=0.5)

data6, = ax3.plot(x4,y4, '--', color=(0.900,0.000,0.900), linewidth=2.0, label='difference')
data7, = ax3.plot(x8,y8, '-',  color=(0.000,0.000,0.000), linewidth=0.5)

data8, = ax4.plot(x1,f1(x1,10.0,1.0), '-', color=(1.000,0.700,0.000), linewidth=2.0, label='external current density')
#data9, = ax1.plot(x8,y8, '-', color=(0.000,0.000,0.000), linewidth=0.5)

data10, = ax5.plot(x5,y5, '--', color=(1.000,0.000,0.000), linewidth=2.0, label='octopus calculation')
data11, = ax5.plot(x6,y6, '-.', color=(0.000,0.000,1.000), label='meep calculation')
data12, = ax5.plot(x8,y8, '-',  color=(0.000,0.000,0.000), linewidth=0.5)

data13, = ax6.plot(x7,y7, '--', color=(0.900,0.000,0.900), linewidth=2.0, label='difference')
data14, = ax6.plot(x8,y8, '-',  color=(0.000,0.000,0.000), linewidth=0.5)

ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
ax3.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
ax4.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
ax5.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))
ax6.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

ax1.set_xlim(   5.00, 25.00)
ax1.set_ylim(  -5e-8,  4e-6)

ax2.set_xlim(   5.00, 25.00)
ax2.set_ylim(  -0.11,  0.11)

ax3.set_xlim(  5.000, 25.000)
ax3.set_ylim( -0.003,  0.003)

ax4.set_xlim(   5.00, 25.00)
ax4.set_ylim(  -5e-8,  4e-6)

ax5.set_xlim(   5.00, 25.00)

ax6.set_xlim(   5.00, 25,00)


ax1.legend(handles=[data1, data3, data4, data6], prop={'size': 12}, loc=(-0.05,1.3), ncol=4)
#ax2.legend(handles=[data3,data4], prop={'size': 12}, loc=(0.57,0.57), ncol=2)
#ax3.legend(handles=[data6], prop={'size': 12}, loc=(0.71,0.75))

#ax.set_aspect(4)

DPI = fig.get_dpi()
fig.set_size_inches(1100.0/float(DPI),720.0/float(DPI))

fig.savefig('meep_reference_3D_gaussian_spatial_distribution_gaussian_current_at_05_00_00.png')

plt.show()
