#!/usr/bin/gnuplot

set terminal png
set output "meep_reference_plot.png"

plot [0:30] [-0.8:0.6] "octopus_data/maxwell_e_field-z_at_0_0_0.z=0" u 2:5 w lines t "E-z at (0,0,0) with octopus", \
                    "meep_data/Field-z_0_0_0/Ez.dat" u 1:2 w lines t "E-z at (0,0,0) with meep", \
                    "octopus_data/maxwell_e_field-z_at_1_0_0.z=0" u 2:5 w lines t "E-z at (1,0,0) with octopus", \
                    "meep_data/Field-z_1_0_0/Ez.dat" u 1:2 w lines t "E-z at (1,0,0) with meep", \
                    "octopus_data/maxwell_e_field-z_at_5_0_0.z=0" u 2:5 w lines t "E-z at (5,0,0) with octopus", \
                    "meep_data/Field-z_5_0_0/Ez.dat" u 1:2 w lines t "E-z at (5,0,0) with meep"
