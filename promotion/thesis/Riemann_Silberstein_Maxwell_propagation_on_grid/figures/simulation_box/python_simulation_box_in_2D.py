import matplotlib.pyplot as plt
import matplotlib.lines as lines
import numpy as np
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib import colors, ticker, cm


#fig, ax = plt.subplots()

fig = plt.figure()
ax  = fig.add_subplot(111, aspect='equal')

# ax1.add_patch(patches.Rectangle((x,y),width,height)



###############################################################################
# Rectangles for the boundaries

ax.add_patch(patches.Rectangle((-10,-10),  3, 20, ec='none', alpha=0.5, color='black'))
ax.add_patch(patches.Rectangle((  7,-10),  3, 20, ec='none', alpha=0.5, color='black'))
ax.add_patch(patches.Rectangle(( -7,  7), 14,  3, ec='none', alpha=0.5, color='black'))
ax.add_patch(patches.Rectangle(( -7,-10), 14,  3, ec='none', alpha=0.5, color='black'))

ax.add_patch(patches.Rectangle((-7,-7), 14, 14, ec='none', alpha=0.2, color='black'))



###############################################################################
# Lines for the boundaries

r1 = [-10,-10]
r2 = [ 10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [ 10, 10]
r2 = [-10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [-10,-10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)

r1 = [-10, 10]
r2 = [ 10, 10]
line = lines.Line2D(r1, r2, color='black', lw=2.5)
ax.add_line(line)



###############################################################################
# Grid points

for ii in range(-10, 0, 1):
   for jj in range(-10,0, 1):
      x = ii + 0.5
      y = jj + 0.5
      point, = plt.plot(x, y, marker='o', markersize=8, markeredgewidth=0.5, markeredgecolor='k', color=(0.784,0.000,0.784))

for ii in range(-10, 0, 1):
   for jj in range(1, 11, 1):
      x = ii + 0.5
      y = jj - 0.5
      point, = plt.plot(x, y, marker='o', markersize=8, markeredgewidth=0.5, markeredgecolor='k', color=(0.784,0.000,0.784))

for ii in range(1, 11, 1):
   for jj in range(-10, 0, 1):
      x = ii - 0.5
      y = jj + 0.5
      point, = plt.plot(x, y, marker='o', markersize=8, markeredgewidth=0.5, markeredgecolor='k', color=(0.784,0.000,0.784))

for ii in range(1, 11, 1):
   for jj in range(1, 11, 1):
      x = ii - 0.5
      y = jj - 0.5
      point, = plt.plot(x, y, marker='o', markersize=8, markeredgewidth=0.5, markeredgecolor='k', color=(0.784,0.000,0.784))


#r1 = [ -7.0, -7.0]
#r2 = [-11.5, -7.0]
#line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
#ax.add_line(line)

#r1 = [  7.0,  7.0]
#r2 = [-11.5, -7.0]
#line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
#ax.add_line(line)

#r1 = [-11.5, -7.0]
#r2 = [  7.0,  7.0]
#line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
#ax.add_line(line)

#r1 = [-11.5, -7.0]
#r2 = [ -7.0, -7.0]
#line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
#ax.add_line(line)


###############################################################################
# Grid lines and ticks



#xticks = np.arange(-9.5,10.0,1.0)
#yticks = np.arange(-9.5,10.0,1.0)

#ax.set_xticks(xticks)
#ax.set_yticks(yticks)

#ax.xaxis.grid(True,  which='major')
#ax.yaxis.grid(True, which='major')

#ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off')

#plt.xticks([-10.0, -7.0, 0.0, 7.0, 10.0], [r'$-L_x$', r'$-b_x$', r'$0.0$', r'$+b_x$', r'$+L_x$'])
#plt.yticks([-10.0, -7.0, 0.0, 7.0, 10.0], [r'$-L_y$', r'$-b_y$', r'$0.0$', r'$+b_y$', r'$+L_y$'])

RS_points         = mpatches.Patch(color=(0.784,0.000,0.784), alpha=1.0, label='RS grid points')
simulation_region = mpatches.Patch(color='black', alpha=0.2, label='free simulation region')
boundary_region   = mpatches.Patch(color='black', alpha=0.5, label='boundary region')

plt.legend(handles=[RS_points, simulation_region, boundary_region], prop={'size': 15}, loc=( 0.30,1.015))

#plt.axis('off')

#major_ticks = np.arange(-10, 10, 5)
#minor_ticks = np.arange(-10, 10, 1)

#ax.set_xticks(major_ticks)                                                       
#ax.set_xticks(minor_ticks, minor=True)                                           
#ax.set_yticks(major_ticks)                                                       
#ax.set_yticks(minor_ticks, minor=True)

#ax.grid(which='minor', alpha=0.5)                                                
#ax.grid(which='major', alpha=1.0)

#DPI = fig.get_dpi()
#fig.set_size_inches(12,12)

r1 = [-10.0,-10.0]
r2 = [-11.5,-10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [ 10.0, 10.0]
r2 = [-11.5,-10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5,-10.0]
r2 = [ 10.0, 10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5,-10.0]
r2 = [-10.0,-10.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)


r1 = [ -7.0, -7.0]
r2 = [-11.5, -7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [  7.0,  7.0]
r2 = [-11.5, -7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5, -7.0]
r2 = [  7.0,  7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

r1 = [-11.5, -7.0]
r2 = [ -7.0, -7.0]
line = lines.Line2D(r1, r2, color='black', alpha=1.0, lw=1.0, linestyle=':')
ax.add_line(line)

ax.set_xlim(-11.5,11.5)
ax.set_ylim(-11.5,11.5)

plt.xticks([-10.0, -7.0, 0.0, 7.0, 10.0], [r'$-L_x$', r'$-b_x$', r'$0.0$', r'$+b_x$', r'$+L_x$'])
plt.yticks([-10.0, -7.0, 0.0, 7.0, 10.0], [r'$-L_y$', r'$-b_y$', r'$0.0$', r'$+b_y$', r'$+L_y$'])

plt.tick_params(axis='both', which='major', labelsize=17)
plt.tick_params(axis='both', which='minor', labelsize=17)

DPI = fig.get_dpi()
fig.set_size_inches(1200.0/float(DPI),1000.0/float(DPI))

#ax.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off')

fig.savefig('simulation_box_in_2D.png',bbox_inches='tight',pad_inches = 0)
fig.savefig('simulation_box_in_2D.svg',bbox_inches='tight',pad_inches = 0)

#plt.show()
