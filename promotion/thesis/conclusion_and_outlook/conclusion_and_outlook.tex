

  \chapter*{Summary, conclusion and outlook}   \label{chap_summary}
    \markboth{Summary, conclusion and outlook}{Summary, conclusion and outlook}
    \addcontentsline{toc}{chapter}{Summary, conclusion and outlook}
 
    In this thesis, we have faced the challenge of finding a feasible methodology and a first implementation of three-dimensional fully forward and backward coupled
    light-matter interactions. As a first key step, we have transformed the common Maxwell's equation into a Riemann-Silberstein representation,
    which provides the underlying equation of motion for the electromagnetic field in a inhomogeneous Schr\"odinger-like equation. This crucial step, 
    gives us the opportunity to use quantum-mechanical time-evolution operators to propagate the electromagnetic field similar to matter wave functions.
    We use this advantage to implement a fundamental real-time real-space classical electromagnetic equation propagation into the
    existing quantum-mechanical simulation code Octopus. First, we implemented a stand-alone electromagnetic field simulation, which handles external current
    densities and linear media. We validate our novel propagation scheme and implementation with the established electromagnetic simulation program MEEP,
    which uses a standard finite-difference time-domain method for the Maxwell field propagation. 
    The considered test simulations agree very well with the results of MEEP, also with regard to stability and efficiency of the runs. 

    To extend the possibilities for applications, we have considered adequate electromagnetic
    boundary conditions. As a first important boundary condition, we considered absorbing boundaries to simulate open Maxwell systems. We employ two different
    methods, first a mask absorption function which is easy to implement, and secondly a perfectly matched layer propagation. Our comparison of both different
    techniques has shown, that the PML provides a significantly better absorbing boundary and qualitatively better wave propagations in the simulation box. 
    To simulate incoming signals which enter the simulation box, we have introduced incident plane wave boundaries, which we also combined with the perfectly matched
    layer boundaries to simulate incoming waves and scattered outgoing fields. 
%    Our nanoparticle application uses this plane-wave perfectly matched
%    layer boundary condition, for one single incoming laser pulse, but as we shown in the only plane-wave application without matter coupling, we can
%    simulate two arbitrary incoming plane waves. 
    This feature is useful to simulate pump-probe experiments or a particle excitation from several laser pulses
    and different incident angles.
    One possible way to get scattered fields from plane waves, for instance, 
    is based on the interaction of the plane wave in vacuum and a linear medium, as we have shown in a first propagation example coupled to a linear medium. 
    Our implemented propagator considers besides space- and time-dependent electric permittivity and magnetic permeability electric and magnetic
    conductivities. With this feature at hand, one can design a large set of purpose tailored simulation boxes, e.g., perfect and semi-transparent mirrors, wave guides, lossy
    layers, just to list a view options. Overall, the purely Maxwell propagation based on quantum-mechanical time-evolution operations is an equivalently 
    alternative scheme to common electromagnetic time propagation via finite difference time domain, and it builds the first pillar for simulating fully 
    coupled three-dimensional light-matter systems. 

    The second pillar is based on the many-body considerations for large matter systems. Starting on the theory level of quantum electrodynamics, we have deduced a 
    multi-species auxiliary system, the Kohn-Sham system on a Pauli level to describe a non-relativistic low energy regime and
    based on density-functional theory (DFT). Using DFT instead of the full many-body particle problem
    leads to a feasible but still very computational expensive problem. Hence, the underlying Maxwell-Pauli-Kohn-Sham equations determine the corresponding
    matter current density, which
    is used as the classical current inhomogeneity term of our Maxwell propagation equation. In that way, we coupled the matter reaction to the surrounded
    electromagnetic field. 

    Furthermore, we have considered the influence of the electromagnetic field on the Kohn-Sham Hamiltonian. Starting with the non-relativistic Pauli equation, we 
    end up at the full minimal coupled Hamiltonian for the quantum mechanical system. We have solved the issue that the full minimal coupling term depends on 
    the gauge dependent vector potential by transforming the Kohn-Sham Hamiltonian with a Power-Zienau-Woolley transformation to get multipole coupling terms,
    which depend now only on the electromagnetic fields. Since in this picture, only the transverse components of the fields couple to the matter, we use a Poisson 
    solver and solve the Poisson equation which is part of a Helmholtz-decomposition to obtain the correct transverse fields. In a last step, we have handled the
    problem of a self-consistent time-step of the fully coupled light-matter system by introducing a predictor-corrector scheme. Taking into account, that the
    dynamics of our considered triad, nuclei, electron, and photon, differ fundamentally, we exploit these properties to get an efficient and accurate
    approach of the full coupled multi-scale problem. We employ our corresponding implementation to demonstrate fully coupled light-matter effects compared to
    only conventional forward coupling simulations. 

    Since nanoplasmonic systems reveal often a remarkable optical activity, we select a sodium dimer cluster as a first test system for our novel approach. Our first
    simulation uses only a simple subset of the presented full Ehrenfest-Pauli-Kohn-Sham scheme. Using only the paramagnetic current density,
    no spin matter states, static pseudopotentials and only mean-field vector potentials as a first approximation for the Kohn-Sham vector potential, the 
    corresponding results show clearly the importance of simulating the light-matter system in a fully self-consistent description. All measurable variables,
    where we only selected the most relevant ones, i.e., energy, dipole, and electric field, reveal a significant different behavior compared to the
    conventional forward coupling runs. In addition, the direct performed measurements of the electromagnetic fields at our defined electromagnetic
    detectors to analyze the far field attributes has shown, that the usual approximation to deduce optical properties only indirectly by
    using matter variables in general does not hold. 
    Furthermore, we observed that the difference of computed observables between local density approximation and gradient-corrected functional was minor 
    compared to the difference of calculating only forward coupling and the self-consistent forward-backward coupling. Hence, our results contradict the
    often argued position in common literature, that the discrepancies between theory and experiment are based on missing exchange-correlation effects. 

    To sum up, based on our results, we come to the conclusion that self-consistent forward-backward couplings should always be included. Their effects
    can easily reach the magnitude of longitudinal exchange-correlation contributions or even exceed them. \\


    The Ehrenfest-Maxwell-Pauli-Kohn-Sham scheme that we have introduced in this thesis as a mean-field limit of the exact density-functional formulation
    of the Pauli-Fierz field theory is only a first step. Future investigations have to focus on better approximations closer to the full field theory to go beyond
    the mean-field limit and to simulate and reveal more quantum effects. However, even in the EMPKS limit ensuing steps are the implementation of remaining
    mean-field features that we already discussed in this work. For instance, providing a propagation with total system current which includes the diamagnetic
    current and magnetization current. Taking the magnetization current into account, the electromagnetic field back-reaction becomes spin-dependent,
    and we can investigate magnetic effects from a fully resolved magnetization of the system. To complete the total current density and get the full back-coupling,
    we have to add the ionic current of the nuclei. 

    We already found a relatively strong dispersion with shifted phase and frequency of the transverse
    electromagnetic field while it passes the dimer. The diamagnetic current should increase this effect since the corresponding current term couples directly
    the two Riemann-Silberstein helicity states similar to the coupling terms in the Riemann-Silberstein description of a linear medium. In general,
    since our implemented Maxwell propagation couples to quantum mechanical matter as well as linear media, the interesting question arises, to find a relation
    between these two different description of matter. In other words, a quantum mechanical simulation leads to classical space- and 
    time-dependent electric permittivity and magnetic permeability, which approximately reproduce the electromagnetic field propagation replacing the quantum
    system by an artificially designed linear medium. In this case, we can exploit that our propagation can combine both, microscopic and 
    macroscopic systems, in one simulation. 
 
    According to our observations for the sodium dimer, the fully coupled system
    induces effects on every system variable. Therefore, conventional used variables have to be modified to reach accurate approximated
    simulations. One important task is the construction of adapted exchange-correlation scalar and vector potentials to go beyond the classical mean-field
    approximation for photons to consider their quantum nature, for instance, vacuum fluctuations. In turn, the quantum nature of the matter can be shown
    by correcting the physical-mass approximation.
    Besides the scalar and vector exchange-correlation potentials, 
    the pseudo-potentials require a dependency on the electromagnetic fields, starting from an all-electron consideration, since they have to take the full coupled
    electromagnetic field into account. Further, our
    nanoplasmonic simulations including ion motion and their remarkable raise in kinetic energy show
    that the electromagnetic fields very close to the ions is not correct. It seems that the calculated Lorentz-forces acting on the ions
    are too strong, which could be a consequence of the incorrect pseudo-potential behavior at that level. 

    Additionally, referring to the vector potential, which we already calculate in a first mean-field approxmation using the Poisson solver, we can employ
    in principle the full minimal coupling interaction instead of the multipole expansion terms. However, having both interaction levels gives us the opportunity 
    to find the order of the multipole expansion that shows the main responsibility of an observed effect. In our presented nanoplasmonic example we already 
    show, that only the beyond dipole coupling terms cause the detected radiation of the second harmonic generation. 

    In this work, we found the nanoparticle groundstate by a conventional diagonalization of the Hamiltonian, since we focused on the electromagnetic time-propagation.
    However, fully coupled light-matter interactions should also already be considered to find the correct groundstate. This requires to solve stationary
    Maxwell equations self-consistently coupled to the stationary Kohn-Sham equations. The groundstate can also be modified by starting from a thermal initial state
    for the ions to catch temperature effects. 

    Expanding our so far used simulation box for finite quantum systems to infinite periodic systems in one, two or three dimensions also opens a whole new class
    of possibilities. For instance, the investigation of the influence of the self-consistent forward-backward reaction for large crystal structures, e.g.,
    to improve the efficiency of solar cells.
    
    As a final conclusion, we note that our fully coupled Ehrenfest-Maxwell-Pauli-Kohn-Sham implementation provides a rather practicable, flexible, and comprehensive simulation approach
    to push research forward in optical material science (nano-optics, nano-plasmonics, (photo) electrocatalysis), in condensed matter
    or even in understanding chemical reactions, since they are mainly determined by the electromagnetic interactions between ions and electrons.


  %\chapter*{Outlook}   \label{chap_outlook}
  %  \addcontentsline{toc}{chapter}{Outlook}
