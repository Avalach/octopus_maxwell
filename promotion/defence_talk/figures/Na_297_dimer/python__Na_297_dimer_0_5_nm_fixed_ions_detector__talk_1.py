#!/bin/bash

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from scipy.fftpack import fft, ifft
from scipy import signal
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(figsize=(1,10))

gs = gridspec.GridSpec(3, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

#ax1 = plt.subplot(gs[0:3,:])
#ax2 = plt.subplot(gs[3:6,0])
ax3 = plt.subplot(gs[0:3,0])
#ax4 = plt.subplot(gs[9:12,0])
#ax5 = plt.subplot(gs[8:10,0])


#dt       = 0.2106550
#relax    = 1065
#step_0   = 107
#t_offset = - relax * dt
#f_offset = 0.0

#cc        = 137.035999679
#pi        = 3.14159265359
#amplitude = 0.0001
#omega     = 0.112000023380
#period    = 2*pi/omega
#width     = 38438.5000/cc
#shift     = 38438.5000/cc 

period_number = 2.5

dt       = 0.2106550
relax    = 1147
step_0   = 115
t_offset = - relax * dt
f_offset = 0.0

cc        = 137.035999679
pi        = 3.14159265359
amplitude = 0.0001
omega     = 0.104000543511
period    = 2*pi/omega
width     = 41395.1000/cc
shift     = 41395.1000/cc # + t_offset

N_total_points = 850
N_plot_points  = N_total_points - step_0
N_fft          = N_plot_points/2 + 1

N_total_points_2 = 700
N_plot_points_2  = N_total_points_2 - step_0
N_fft_2          = N_plot_points_2/2 + 1



###################################################################################################
#  Laser function:
###################################################################################################

def f_laser(x,a0,x0,w,omega):
    y = []
    for ii in range(0,len(x)):
       if ( np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2))) < w ):
          y.append( a0 * np.cos(pi/2*(x[ii]-2*w-x0)/w+pi) * np.cos(omega*(x[ii]-x0)) )
       else:
          y.append(0.0)
    return y

t_laser = np.arange(0.0, N_plot_points*10*dt, dt*10)


##### Laser fourier transform ###################

w_fftl = []
for ii in range(0, len(t_laser)):
    w_fftl.append(2 * np.pi * ii / len(t_laser))

N_fftl     = N_fft # len(t_laser)/2+1
dt_fftl    = dt*10
w_max_fftl = 1/dt_fftl*2*np.pi
w_fftl     = np.linspace(0, w_max_fftl/2, N_fftl)
f_fftl     = 2*fft(f_laser(t_laser,amplitude,shift,width,omega))/N_fftl




###################################################################################################
#  Broadening function:
###################################################################################################

def f_broadening(x,w):
    y = []
    for ii in range(0,len(x)):
       #y.append(np.exp(-x[ii]**2/(2*w**2)))
       y.append(np.exp(0))
    return y

#def convolution(x,xw):
#    for ii in range(0,len(x)):
#       for jj in range(0,len(xw)):

x_broadening = np.arange(-period_number*omega/2,period_number*omega/2+w_fftl[2]-w_fftl[1],w_fftl[2]-w_fftl[1])

y_broadening = f_broadening(x_broadening,0.02)




###################################################################################################
#  Dataset 1:
###################################################################################################
t_data1 = []
f_data1 = []
f_data1_max = 0.0
with open('./370_cobra/td.general/maxwell_e_field_trans_z_at_p37_00_00') as data1:
    readlines = data1.readlines()
    t_data1 = [line.split()[0] for line in readlines]
    f_data1 = [line.split()[1] for line in readlines]
f0_data1 = f_data1[step_0]
tw_data1 = []
fw_data1 = []
for ii in range(0, N_total_points):
    if (float(t_data1[ii]) + float(t_offset) > 0.0):
       tw_data1.append(float(t_data1[ii]) + t_offset)
       fw_data1.append(float(f_data1[ii]))


##### Fourier transform #########################

w_fftdata1 = []
for ii in range(0, len(tw_data1)):
   w_fftdata1.append(2 * np.pi * ii / len(tw_data1))

N_fftdata1     = N_fft # len(t_data1)/2+1
dt_fftdata1    = tw_data1[2]-tw_data1[1]
w_max_fftdata1 = 1/dt_fftdata1*2*np.pi
w_fftdata1     = np.linspace(0, w_max_fftdata1/2, N_fftdata1)
f_fftdata1     = 2*fft(fw_data1)/N_fftdata1
#f_fftdata1     = signal.convolve(f_fftdata1, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 2:
###################################################################################################
t_data2 = []
f_data2 = []
idx = 0
with open('./371_cobra/td.general/maxwell_e_field_trans_z_at_p37_00_00') as data2:
    readlines = data2.readlines()
    t_data2 = [line.split()[0] for line in readlines]
    f_data2 = [line.split()[1] for line in readlines]
f0_data2 = f_data2[step_0]
tw_data2 = []
fw_data2 = []
for ii in range(0, N_total_points):
    if (float(t_data2[ii]) + float(t_offset) > 0.0):
       tw_data2.append(float(t_data2[ii]) + t_offset)
       fw_data2.append(float(f_data2[ii]))


##### Fourier transform #########################

w_fftdata2 = []
for ii in range(0, len(tw_data2)):
   w_fftdata2.append(2 * np.pi * ii / len(tw_data2))

N_fftdata2     = N_fft # len(t_data2)/2+1
dt_fftdata2    = tw_data2[2]-tw_data2[1]
w_max_fftdata2 = 1/dt_fftdata2*2*np.pi
w_fftdata2     = np.linspace(0, w_max_fftdata2/2, N_fftdata2)
f_fftdata2     = 2*fft(fw_data2)/N_fftdata2
#f_fftdata2     = signal.convolve(f_fftdata2, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 3:
###################################################################################################
t_data3 = []
f_data3 = []
idx = 0
with open('./372_cobra/td.general/maxwell_e_field_x_at_00_p30_00') as data3:
    readlines = data3.readlines()
    t_data3 = [line.split()[0] for line in readlines]
    f_data3 = [line.split()[1] for line in readlines]
f0_data3 = f_data3[step_0]
tw_data3 = []
fw_data3 = []
for ii in range(0, N_total_points_2):
    if (float(t_data3[ii]) + float(t_offset) > 0.0):
       tw_data3.append(float(t_data3[ii]) + t_offset)
       fw_data3.append(float(f_data3[ii]))


##### Fourier transform #########################

w_fftdata3 = []
for ii in range(0, len(tw_data3)):
   w_fftdata3.append(2 * np.pi * ii / len(tw_data3))

N_fftdata3     = N_fft_2 # len(t_data3)/2+1
dt_fftdata3    = tw_data3[2]-tw_data3[1]
w_max_fftdata3 = 1/dt_fftdata3*2*np.pi
w_fftdata3     = np.linspace(0, w_max_fftdata3/2, N_fftdata3)
f_fftdata3     = 2*fft(fw_data3)/N_fftdata3
#f_fftdata3     = signal.convolve(f_fftdata3, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 4:
###################################################################################################
t_data4 = []
f_data4 = []
idx = 0
with open('./373_cobra/td.general/maxwell_e_field_x_at_00_p30_00') as data4:
    readlines = data4.readlines()
    t_data4 = [line.split()[0] for line in readlines]
    f_data4 = [line.split()[1] for line in readlines]
f0_data2 = f_data2[step_0]
tw_data4 = []
fw_data4 = []
for ii in range(0, N_total_points_2):
    if (float(t_data4[ii]) + float(t_offset) > 0.0):
       tw_data4.append(float(t_data4[ii]) + t_offset)
       fw_data4.append(float(f_data4[ii]))


##### Fourier transform #########################

w_fftdata4 = []
for ii in range(0, len(tw_data4)):
   w_fftdata4.append(2 * np.pi * ii / len(tw_data4))

N_fftdata4     = N_fft_2 # len(t_data4)/2+1
dt_fftdata4    = tw_data4[2]-tw_data4[1]
w_max_fftdata4 = 1/dt_fftdata4*2*np.pi
w_fftdata4     = np.linspace(0, w_max_fftdata4/2, N_fftdata4)
f_fftdata4     = 2*fft(fw_data4)/N_fftdata4
#f_fftdata4     = signal.convolve(f_fftdata4, y_broadening, mode='same') # / sum(y_broadening)



###################################################################################################
#  Dataset 4:
###################################################################################################
t_data4 = []
f_data4 = []
idx = 0
with open('./373_cobra/td.general/maxwell_e_field_x_at_00_p30_00') as data4:
    readlines = data4.readlines()
    t_data4 = [line.split()[0] for line in readlines]
    f_data4 = [line.split()[1] for line in readlines]
f0_data2 = f_data2[step_0]
tw_data4 = []
fw_data4 = []
for ii in range(0, N_total_points):
    if (float(t_data4[ii]) + float(t_offset) > 0.0):
       tw_data4.append(float(t_data4[ii]) + t_offset)
       fw_data4.append(float(f_data4[ii]))


##### Fourier transform #########################

w_fftdata4 = []
for ii in range(0, len(tw_data4)):
   w_fftdata4.append(2 * np.pi * ii / len(tw_data4))

N_fftdata4     = N_fft # len(t_data4)/2+1
dt_fftdata4    = tw_data4[2]-tw_data4[1]
w_max_fftdata4 = 1/dt_fftdata4*2*np.pi
w_fftdata4     = np.linspace(0, w_max_fftdata4/2, N_fftdata4)
f_fftdata4     = 2*fft(fw_data4)/N_fftdata4
#f_fftdata4     = signal.convolve(f_fftdata4, y_broadening, mode='same') # / sum(y_broadening)



###################################################################################################
#  Dataset 5:
###################################################################################################

t_data5 = []
f_data5 = []
with open('./370_cobra/td.general/multipoles') as data5:
    for line in data5:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data5.append(line.split()[1])
           f_data5.append(line.split()[3])
f0_data5 = f_data5[step_0]

tw_data5 = []
fw_data5 = []
for ii in range(0, N_total_points*10):
    if (float(t_data5[ii]) + float(t_offset) > 0.0):
       tw_data5.append(float(t_data5[ii]) + t_offset)
       fw_data5.append(float(f_data5[ii]))


##### Fourier transform #########################

w_fftdata5 = []
for ii in range(0, len(tw_data5)):
   w_fftdata5.append(2 * np.pi * ii / len(tw_data5))

N_fftdata5     = N_fft*10 # len(t_data5)/2+1
dt_fftdata5    = tw_data5[2]-tw_data5[1]
w_max_fftdata5 = 1/dt_fftdata5*2*np.pi
w_fftdata5     = np.linspace(0, w_max_fftdata5/2, N_fftdata5)
f_fftdata5     = 2*fft(fw_data5)/N_fftdata5
#f_fftdata5     = signal.convolve(f_fftdata5, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 6:
###################################################################################################

t_data6 = []
f_data6 = []
with open('./371_cobra/td.general/multipoles') as data6:
    for line in data6:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data6.append(line.split()[1])
           f_data6.append(line.split()[3])
f0_data6 = f_data6[step_0]

tw_data6 = []
fw_data6 = []
for ii in range(0, N_total_points*10):
    if (float(t_data6[ii]) + float(t_offset) > 0.0):
       tw_data6.append(float(t_data6[ii]) + t_offset)
       fw_data6.append(float(f_data6[ii])) 


##### Fourier transform #########################

w_fftdata6 = []
for ii in range(0, len(tw_data6)):
   w_fftdata6.append(2 * np.pi * ii / len(tw_data6))

N_fftdata6     = N_fft*10 # len(t_data6)/2+1
dt_fftdata6    = tw_data6[2]-tw_data6[1]
w_max_fftdata6 = 1/dt_fftdata6*2*np.pi
w_fftdata6     = np.linspace(0, w_max_fftdata6/2, N_fftdata6)
f_fftdata6     = 2*fft(fw_data6)/N_fftdata6
#f_fftdata6     = signal.convolve(f_fftdata6, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 7:
###################################################################################################

t_data7 = []
f_data7 = []
with open('./372_cobra/td.general/multipoles') as data7:
    for line in data7:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data7.append(line.split()[1])
           f_data7.append(line.split()[3])
f0_data7 = f_data7[step_0]

tw_data7 = []
fw_data7 = []
for ii in range(0, N_total_points_2*10):
    if (float(t_data7[ii]) + float(t_offset) > 0.0):
       tw_data7.append(float(t_data7[ii]) + t_offset)
       fw_data7.append(float(f_data7[ii])) 


##### Fourier transform #########################

w_fftdata7 = []
for ii in range(0, len(tw_data7)):
   w_fftdata7.append(2 * np.pi * ii / len(tw_data7))

N_fftdata7     = N_fft_2*10 # len(t_data7)/2+1
dt_fftdata7    = tw_data7[2]-tw_data7[1]
w_max_fftdata7 = 1/dt_fftdata7*2*np.pi
w_fftdata7     = np.linspace(0, w_max_fftdata7/2, N_fftdata7)
f_fftdata7     = 2*fft(fw_data7)/N_fftdata7
#f_fftdata7     = signal.convolve(f_fftdata7, y_broadening, mode='same') # / sum(y_broadening)




###################################################################################################
#  Dataset 8:
###################################################################################################

t_data8 = []
f_data8 = []
with open('./373_cobra/td.general/multipoles') as data8:
    for line in data8:
        split_len = len(line.split())
        if (split_len == 11 ):
           t_data8.append(line.split()[1])
           f_data8.append(line.split()[3])
f0_data8 = f_data8[step_0]

tw_data8 = []
fw_data8 = []
for ii in range(0, N_total_points_2*10):
    if (float(t_data8[ii]) + float(t_offset) > 0.0):
       tw_data8.append(float(t_data8[ii]) + t_offset)
       fw_data8.append(float(f_data8[ii])) 


##### Fourier transform #########################

w_fftdata8 = []
for ii in range(0, len(tw_data8)):
   w_fftdata8.append(2 * np.pi * ii / len(tw_data8))

N_fftdata8     = N_fft_2*10 # len(t_data8)/2+1
dt_fftdata8    = tw_data8[2]-tw_data8[1]
w_max_fftdata8 = 1/dt_fftdata8*2*np.pi
w_fftdata8     = np.linspace(0, w_max_fftdata8/2, N_fftdata8)
f_fftdata8     = 2*fft(fw_data8)/N_fftdata8
#f_fftdata8     = signal.convolve(f_fftdata8, y_broadening, mode='same') # / sum(y_broadening)


for ii in range(0, len(tw_data4)):
     f_fftdata4[ii] = 25e3*f_fftdata4[ii]



###################################################################################################
#  Plots:
###################################################################################################

#fig.suptitle(r"Fourier transform for the dimer with d$_1$=0.1nm",x=0.5,y=1.17, size=18)
#fig.text(0.000,1.25,r"Fourier transform of the electric far field and electric dipole",size=17)
#fig.text(0.16,1.11,r"dipole oscillation for the dimer with d$_2$=0.5nm", size=15)


##### Panel 1 plot ##############################
#data8, = ax1.plot(w_fftdata8, np.abs(f_fftdata8[:N_fftdata8])**2, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='forward coupled beyond dipole approximation')
#data7, = ax1.plot(w_fftdata7, np.abs(f_fftdata7[:N_fftdata7])**2, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='fully coupled beyond dipole approximation')

##### Panel 2 plot ##############################
#data4, = ax2.plot(w_fftdata4, np.abs(f_fftdata4[:N_fftdata4])**2, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='forward coupled beyond dipole approximation')
#data3, = ax2.plot(w_fftdata3, np.abs(f_fftdata3[:N_fftdata3])**2, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='fully coupled beyond dipole approximation')

##### Panel 3 plot ##############################
data6, = ax3.plot(w_fftdata6, np.abs(f_fftdata6[:N_fftdata6])**2, '-', color=(0.000,0.000,1.000), linewidth=1.5, label='forward coupled in dipole approximation')
data5, = ax3.plot(w_fftdata5, np.abs(f_fftdata5[:N_fftdata5])**2, '-', color=(1.000,0.000,0.000), linewidth=1.5, label='fully coupled in dipole approximation')

##### Panel 4 plot ##############################
#data2, = ax4.plot(w_fftdata2, np.abs(f_fftdata2[:N_fftdata2])**2, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='forward coupled in dipole approximation')
#data1, = ax4.plot(w_fftdata1, np.abs(f_fftdata1[:N_fftdata1])**2, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='fully coupled in dipole approximation')


###################################################################################################
#  Units transformation:
###################################################################################################

# time in au
t_au = 2.418884326505*10**(-17)

# time in femto seconds
t_fm = 10**(-15)

# energy from atomic untis to eV
au_in_eV       = 27.2114

# lenght in au
l_au = 5.2917721092*10**(-11)

# lenght in nm
l_nm = 10**(-9)

# x-tick interval
femto_interval = 2.5

# Femtosecond to atomic units factor
femto_in_au = t_fm/t_au * femto_interval

# au in nm
au_in_nm = l_au / l_nm

# electric field from atomic units to V/m
au_in_V_per_m = 5.14220652*(10**11)

# x-ticks
xticks_t_femto = np.arange(0, 30*period, femto_in_au)
xticks_t_au    = np.arange(0, 30*period/femto_in_au*femto_interval, femto_interval)  

# x-ticks
xticks_eV = np.arange(0, 0.31 * au_in_eV, 0.5)
xticks_au = xticks_eV/au_in_eV

# y-tick-labels
yticks_eV       = np.arange(-142.905, -142.88, 0.005)
yticklabels_eV  = np.arange(-3888.65, -3887.96, 0.05, dtype=float)




###################################################################################################
#  Panel 1: 
###################################################################################################

# au limits for the laser
#ax1_xlim_1 = 0.05
#ax1_xlim_2 = 0.245
#ax1_ylim_1 = -3.0e-9
#ax1_ylim_2 =  4.6e-8

#ax1.text(ax1_xlim_1+0.02*(np.abs(ax1_xlim_2-ax1_xlim_1)), ax1_ylim_1+0.8*(np.abs(ax1_ylim_2-ax1_ylim_1)),'a)',size=18)


##### Y left label axes #########################

#ax1_y1 = ax1.twinx()

# Axes label on the left
#ax1_y1.set_ylabel(r"<x($\omega$)>", size=15, position=(0.0,0.5))
#ax1_y1.yaxis.set_label_position('left')

# labels
#ax1_y1_ticks = [-1.0*(10**(-7)), 0.0, 1.0*(10**(-7))]
#ax1_y1.set_yticks(ax1_y1_ticks)
#ax1_y1.set_yticklabels(ax1_y1_ticks, size=13)
#ax1_y1.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax1_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax1_y1_ylim_1 = ax1_ylim_1 * au_in_A
#ax1_y1_ylim_2 = ax1_ylim_2 * au_in_A
#ax1_y1.set_ylim(ax1_y1_ylim_1, ax1_y1_ylim_2)


##### Y right label axes ########################

#ax1_y2 = ax1.twinx()

# Axes label on the right
#ax1_y2.set_ylabel(r"$<x(\omega)>$", size=15, position=(0.0,0.0))
#ax1_y2.yaxis.set_label_position('left')

# ticks
#ax1_y2_ticks = [-1.0*(10**(-7))/au_in_A, 0.0, 1.0*(10**(-7))/au_in_A]
#ax1_y2.set_yticks(ax1_y2_ticks)
#ax1_y2.set_yticklabels(ax1_y2_ticks, size=13)
#ax1_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax1_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax1_y2_ylim_1 = ax1_ylim_1
#ax1_y2_ylim_2 = ax1_ylim_2
#ax1_y2.set_ylim(ax1_y2_ylim_1, ax1_y2_ylim_2)


##### plot axes #################################

#ax1.set_xlabel(r"$\omega$ [a.u.]", size=15)
#ax1.xaxis.set_label_position('top')

# ticks
#ax1.set_xticks(xticks_au)
#ax1.set_xticklabels(xticks_au)
#ax1.tick_params(labelleft='off', labelright='off', labeltop='on', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=6)
#ax1.xaxis.set_major_formatter(FormatStrFormatter('%.3f'))
#ax1.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
#ax1.set_xlim(ax1_xlim_1, ax1_xlim_2)
#ax1.set_ylim(ax1_ylim_1, ax1_ylim_2)

# grid lines
#for ii in range(0, int(period_number)+1, 1):
#   r1 = [ 0, 2* ii*omega]
#   r2 = [ -500, 500]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax1.add_line(line)



###################################################################################################
#  Panel 2: 
###################################################################################################

# Axes label top
#ax2.set_xlabel("time in atomic units", size=15)
#ax2.xaxis.set_label_coords(0.5,1.40)
#ax2.xaxis.set_label_position('top')

# au limits for the laser
#ax2_xlim_1 = 0.05
#ax2_xlim_2 = 0.245
#ax2_ylim_1 = -1.10e-17
#ax2_ylim_2 =  4.10e-16

#ax2.text(ax2_xlim_1+0.02*(np.abs(ax2_xlim_2-ax2_xlim_1)), ax2_ylim_1+0.8*(np.abs(ax2_ylim_2-ax2_ylim_1)),'b)',size=18)


##### Y left label axes #########################

#ax2_y1 = ax2.twinx()

# Axes label on the left
#ax2_y1.set_ylabel(r"E$_x$($\omega$)", size=15, position=(0.0,0.50))
#ax2_y1.yaxis.set_label_position('left')

# ticks
#ax2_y1_ticks = [-4*10**7,0.0,4*10**7]
#ax2_y1.set_yticks(ax2_y1_ticks)
#ax2_y1.set_yticklabels(ax2_y1_ticks, size=13)
#ax2_y1.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax2_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax2_y1_ylim_1 = ax2_ylim_1 * au_in_V_per_m
#ax2_y1_ylim_2 = ax2_ylim_2 * au_in_V_per_m
#ax2_y1.set_ylim(ax2_y1_ylim_1, ax2_y1_ylim_2)


##### Y right label axes ########################

#ax2_y2 = ax2.twinx()

# Axes label on the right
#ax2_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
#ax2_y2.yaxis.set_label_position('right')

# ticks
#ax2_y2_ticks = [-4*10**7/au_in_V_per_m, 0.0/au_in_V_per_m, 4*10**7/au_in_V_per_m]
#ax2_y2.set_yticks(ax2_y2_ticks)
#ax2_y2.set_yticklabels(ax2_y2_ticks, size=13)
#ax2_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax2_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax2_y2_ylim_1 = ax2_ylim_1
#ax2_y2_ylim_2 = ax2_ylim_2
#ax2_y2.set_ylim(ax2_y2_ylim_1, ax2_y2_ylim_2)


##### plot axes #################################

#ax2.set_xlabel(r"$\omega$ [a.u.]", size=15)
#ax2.xaxis.set_label_position('top')
#ax2.set_ylabel("strength", size=15, position=(0.0,-1.0))
#ax2_y2.yaxis.set_label_position('right')
#ax2.set_xlabel(r"$\omega$ [eV]", size=15)
#ax2.xaxis.set_label_position('bottom')

# limits
#ax2.set_xlim(ax2_xlim_1, ax2_xlim_2)
#ax2.set_ylim(ax2_ylim_1, ax2_ylim_2)

# ticks
#ax2.set_xticks(xticks_au)
#ax2.set_xticklabels(xticks_eV)
#ax2.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=6)
#ax2.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

#ax2.set_xlim(ax2_xlim_1, ax2_xlim_2)

# grid lines
#for ii in range(0, int(period_number)+1, 1):
#   r1 = [ 0, 2* ii*omega]
#   r2 = [ -500, 500]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax2.add_line(line)




###################################################################################################
#  Panel 3: 
###################################################################################################

# au limits for the laser
ax3_xlim_1 = 0.05
ax3_xlim_2 = 0.245
ax3_ylim_1 = -0.1e-10
ax3_ylim_2 =  3.5e-10

#ax3.text(ax3_xlim_1+0.02*(np.abs(ax3_xlim_2-ax3_xlim_1)), ax3_ylim_1+0.8*(np.abs(ax3_ylim_2-ax3_ylim_1)),'c)',size=18)


##### Y left label axes #########################

ax3_y1 = ax3.twinx()

# Axes label on the left
ax3_y1.set_ylabel(r"<z($\omega$)>", size=15, position=(0.0,0.50))
ax3_y1.yaxis.set_label_position('left')

# labels
#ax3_y1_ticks = [-1.0*(10**(-7)), 0.0, 1.0*(10**(-7))]
#ax3_y1.set_yticks(ax3_y1_ticks)
#ax3_y1.set_yticklabels(ax3_y1_ticks, size=13)
ax3_y1.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax3_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax3_y1_ylim_1 = ax3_ylim_1 * au_in_A
#ax3_y1_ylim_2 = ax3_ylim_2 * au_in_A
#ax3_y1.set_ylim(ax3_y1_ylim_1, ax3_y1_ylim_2)


##### Y right label axes ########################

#ax3_y2 = ax3.twinx()

# Axes label on the right
#ax3_y2.set_ylabel(r"j$_z$ [a.u.]", size=15, position=(0.0,0.55))
#ax3_y2.yaxis.set_label_position('right')

# ticks
#ax3_y2_ticks = [-1.0*(10**(-7))/au_in_A, 0.0, 1.0*(10**(-7))/au_in_A]
#ax3_y2.set_yticks(ax3_y2_ticks)
#ax3_y2.set_yticklabels(ax3_y2_ticks, size=13)
#ax3_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax3_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax3_y2_ylim_1 = ax3_ylim_1
#ax3_y2_ylim_2 = ax3_ylim_2
#ax3_y2.set_ylim(ax3_y2_ylim_1, ax3_y2_ylim_2)


##### plot axes #################################

#ax3.set_xlabel(r"$\omega$ [a.u.]", size=15)
#ax3.xaxis.set_label_position('top')

# ticks
ax3.set_xticks(xticks_au)
ax3.set_xticklabels(xticks_eV)
ax3.set_xlabel(r"$\omega$ [eV]", size=15)
ax3.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='on', bottom='off', top='on', right='off', left='off', direction='inout', length=6)
#ax3.xaxis.set_major_formatter(FormatStrFormatter('%.3f'))
ax3.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

# limits
ax3.set_xlim(ax3_xlim_1, ax3_xlim_2)
ax3.set_ylim(ax3_ylim_1, ax3_ylim_2)

# grid lines
for ii in range(0, int(period_number)+1, 1):
   r1 = [ 0, 2* ii*omega]
   r2 = [ -500, 500]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax3.add_line(line)



###################################################################################################
#  Panel 4: 
###################################################################################################

# Axes label top
#ax4.set_ylabel("E$_z$($\omega$)", size=15, position=(0.0,0.50))
#ax4.yaxis.set_label_position('left')

# au limits for the laser
#ax4_xlim_1 = 0.05
#ax4_xlim_2 = 0.245
#ax4_ylim_1 = -0.10e-9
#ax4_ylim_2 =  2.70e-9

#ax4.text(ax4_xlim_1+0.02*(np.abs(ax4_xlim_2-ax4_xlim_1)), ax4_ylim_1+0.8*(np.abs(ax4_ylim_2-ax4_ylim_1)),'d)',size=18)


##### Y left label axes #########################

#ax4_y1 = ax4.twinx()

# Axes label on the left
#ax4_y1.set_ylabel(r'E$_{z}$ [V/m]', size=15, position=(0.0,0.55))
#ax4_y1.set_ylabel(r"relative strength", size=15, position=(0.0,1.00))
#ax4_y1.yaxis.set_label_position('left')

# ticks
#ax4_y1_ticks = [-4*10**7,0.0,4*10**7]
#ax4_y1.set_yticks(ax4_y1_ticks)
#ax4_y1.set_yticklabels(ax4_y1_ticks, size=13)
#ax4_y1.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax4_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax4_y1_ylim_1 = ax4_ylim_1 * au_in_V_per_m
#ax4_y1_ylim_2 = ax4_ylim_2 * au_in_V_per_m
#ax4_y1.set_ylim(ax4_y1_ylim_1, ax4_y1_ylim_2)


##### Y right label axes ########################

#ax4_y2 = ax4.twinx()

# Axes label on the right
#ax4_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
#ax4_y2.yaxis.set_label_position('right')

# ticks
#ax4_y2_ticks = [-4*10**7/au_in_V_per_m, 0.0/au_in_V_per_m, 4*10**7/au_in_V_per_m]
#ax4_y2.set_yticks(ax4_y2_ticks)
#ax4_y2.set_yticklabels(ax4_y2_ticks, size=13)
#ax4_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax4_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax4_y2_ylim_1 = ax4_ylim_1
#ax4_y2_ylim_2 = ax4_ylim_2
#ax4_y2.set_ylim(ax4_y2_ylim_1, ax4_y2_ylim_2)


##### plot axes #################################

#ax4.set_xlabel(r"$\omega$ [a.u.]", size=15)
#ax4.xaxis.set_label_position('top')
#ax4.set_ylabel("strength", size=15, position=(0.0,-1.0))
#ax4_y2.yaxis.set_label_position('right')
#ax4.set_xlabel(r"$\omega$ [eV]", size=15)
#ax4.xaxis.set_label_position('bottom')

# limits
#ax4.set_xlim(ax4_xlim_1, ax4_xlim_2)
#ax4.set_ylim(ax4_ylim_1, ax4_ylim_2)

# ticks
#ax4.set_xticks(xticks_au)
#ax4.set_xticklabels(xticks_eV)
#ax4.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='on', bottom='on', top='on', right='off', left='off', direction='inout', length=6)
#ax4.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

#ax4.set_xlim(ax4_xlim_1, ax4_xlim_2)

# grid lines
#for ii in range(0, int(period_number)+1, 1):
#   r1 = [ 0, 2* ii*omega]
#   r2 = [ -500, 500]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax4.add_line(line)






###################################################################################################
#  Legend:
###################################################################################################

#ax1.legend(handles=[data2, data1, data4, data3], prop={'size': 12}, loc=(0.050,1.35), frameon=False, ncol=1)




###################################################################################################
#  Save figure:
###################################################################################################

DPI = fig.get_dpi()
fig.set_size_inches(700.0/float(DPI),200.0/float(DPI))

fig.savefig('Na_297_dimer_0_5_nm_fixed_ions_detector__talk_1.png',bbox_inches='tight',pad_inches = 0)

#plt.show()
