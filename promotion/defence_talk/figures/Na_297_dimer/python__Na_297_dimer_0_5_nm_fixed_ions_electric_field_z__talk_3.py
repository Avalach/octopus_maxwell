#!/bin/bash

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.lines as lines
from scipy.fftpack import fft, ifft
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(figsize=(1,10))

gs = gridspec.GridSpec(4, 1)
gs.update(hspace=0.00)
gs.update(wspace=0.00)

#ax1 = plt.subplot(gs[0:4,:])
#ax2 = plt.subplot(gs[4:8,0])
ax3 = plt.subplot(gs[0:4,0])
#ax4 = plt.subplot(gs[12:16,0])
#ax5 = plt.subplot(gs[16:20,0])


dt       = 0.2106550
relax    = 1147
step_0   = 115
t_offset = - relax * dt
f_offset = 0.0

cc        = 137.035999679
pi        = 3.14159265359
amplitude = 0.0001
omega     = 0.104000543511
period    = 2*pi/omega
width     = 41395.1000/cc
shift     = 41395.1000/cc # + t_offset

period_number = 25




###################################################################################################
#  Laser function:
###################################################################################################

def f_laser(x,a0,x0,w,omega):
    y = []
    for ii in range(0,len(x)):
       if ( np.abs((2*pi/omega*(x[ii]-x0))/np.sqrt(np.power(2*pi/omega,2))) < w ):
          y.append( a0 * np.cos(pi/2*(x[ii]-2*w-x0)/w+pi) * np.cos(omega*(x[ii]-x0)) )
       else:
          y.append(0.0)
    return y

t_laser = np.arange(0.0, period_number * period, dt)

##### Laser fourier transform ###################

w_fftl = []
for ii in range(0, len(t_laser)):
    w_fftl.append(2 * np.pi * ii / len(t_laser))

N_fftl     = len(t_laser)/2+1
dt_fftl    = dt
w_max_fftl = 1/dt_fftl*2*np.pi
w_fftl     = np.linspace(0, w_max_fftl/2, N_fftl)
f_fftl     = 2*fft(f_laser(t_laser,amplitude,shift,width,omega))/N_fftl




###################################################################################################
#  Dataset 1:
###################################################################################################

with open('./370_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez1:
    readlines = ez1.readlines()
    t_ez1 = [line.split()[0] for line in readlines]
    f_ez1 = [line.split()[1] for line in readlines]
f0_ez1 = f_ez1[step_0]
for ii in range(0, len(t_ez1)):
    t_ez1[ii] = float(t_ez1[ii]) + t_offset
    f_ez1[ii] = float(f_ez1[ii]) #- float(f0_ez1)


##### Fourier transform #########################

w_fftez1 = []
for ii in range(0, len(t_ez1)):
   w_fftez1.append(2 * np.pi * ii / len(t_ez1))

N_fftez1     = len(t_ez1)/2+1
dt_fftez1    = t_ez1[2]-t_ez1[1]
w_max_fftez1 = 1/dt_fftez1*2*np.pi
w_fftez1     = np.linspace(0, w_max_fftez1/2, N_fftez1)
f_fftez1     = 2*fft(f_ez1)/N_fftez1




###################################################################################################
#  Dataset 2:
###################################################################################################

with open('./371_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez2:
    readlines = ez2.readlines()
    t_ez2 = [line.split()[0] for line in readlines]
    f_ez2 = [line.split()[1] for line in readlines]
f0_ez2 = f_ez2[step_0]
for ii in range(0, len(t_ez2)):
    t_ez2[ii] = float(t_ez2[ii]) + t_offset
    f_ez2[ii] = float(f_ez2[ii]) #- float(f0_ez2)


##### Fourier transform #########################

w_fftez2 = []
for ii in range(0, len(t_ez2)):
   w_fftez2.append(2 * np.pi * ii / len(t_ez2))

N_fftez2     = len(t_ez2)/2+1
dt_fftez2    = t_ez2[2]-t_ez2[1]
w_max_fftez2 = 1/dt_fftez2*2*np.pi
w_fftez2     = np.linspace(0, w_max_fftez2/2, N_fftez2)
f_fftez2     = 2*fft(f_ez2)/N_fftez2




###################################################################################################
#  Dataset 3:
###################################################################################################

with open('./372_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez3:
    readlines = ez3.readlines()
    t_ez3 = [line.split()[0] for line in readlines]
    f_ez3 = [line.split()[1] for line in readlines]
f0_ez3 = f_ez3[step_0]
for ii in range(0, len(t_ez3)):
    t_ez3[ii] = float(t_ez3[ii]) + t_offset
    f_ez3[ii] = float(f_ez3[ii]) - float(f0_ez3)


##### Fourier transform #########################

w_fftez3 = []
for ii in range(0, len(t_ez3)):
   w_fftez3.append(2 * np.pi * ii / len(t_ez3))

N_fftez3     = len(t_ez3)/2+1
dt_fftez3    = t_ez3[2]-t_ez3[1]
w_max_fftez3 = 1/dt_fftez3*2*np.pi
w_fftez3     = np.linspace(0, w_max_fftez3/2, N_fftez3)
f_fftez3     = 2*fft(f_ez3)/N_fftez3




###################################################################################################
#  Dataset 4:
###################################################################################################

with open('./373_cobra/td.general/maxwell_e_field_z_at_00_00_00') as ez4:
    readlines = ez4.readlines()
    t_ez4 = [line.split()[0] for line in readlines]
    f_ez4 = [line.split()[1] for line in readlines]
f0_ez4 = f_ez4[step_0]
for ii in range(0, len(t_ez4)):
    t_ez4[ii] = float(t_ez4[ii]) + t_offset
    f_ez4[ii] = float(f_ez4[ii]) #- float(f0_ez4)


##### Fourier transform #########################

w_fftez4 = []
for ii in range(0, len(t_ez4)):
   w_fftez4.append(2 * np.pi * ii / len(t_ez4))

N_fftez4     = len(t_ez4)/2+1
dt_fftez4    = t_ez4[2]-t_ez4[1]
w_max_fftez4 = 1/dt_fftez4*2*np.pi
w_fftez4     = np.linspace(0, w_max_fftez4/2, N_fftez4)
f_fftez4     = 2*fft(f_ez4)/N_fftez4




###################################################################################################
#  Dataset 5:
###################################################################################################

with open('./370_cobra/td.general/current_z_at_00_00_00') as c1:
    readlines = c1.readlines()
    t_c1 = [line.split()[0] for line in readlines]
    f_c1 = [line.split()[1] for line in readlines]
f0_c1 = f_c1[step_0]
for ii in range(0, len(t_c1)):
    t_c1[ii] = float(t_c1[ii]) + t_offset
    f_c1[ii] = float(f_c1[ii]) #- float(f0_c1)


##### Fourier transform #########################

w_fftc1 = []
for ii in range(0, len(t_c1)):
   w_fftc1.append(2 * np.pi * ii / len(t_c1))

N_fftc1     = len(t_c1)/2+1
dt_fftc1    = t_c1[2]-t_c1[1]
w_max_fftc1 = 1/dt_fftc1*2*np.pi
w_fftc1     = np.linspace(0, w_max_fftc1/2, N_fftc1)
f_fftc1     = 2*fft(f_c1)/N_fftc1




###################################################################################################
#  Dataset 6:
###################################################################################################

with open('./371_cobra/td.general/current_z_at_00_00_00') as c2:
    readlines = c2.readlines()
    t_c2 = [line.split()[0] for line in readlines]
    f_c2 = [line.split()[1] for line in readlines]
f0_c2 = f_c2[step_0]
for ii in range(0, len(t_c2)):
    t_c2[ii] = float(t_c2[ii]) + t_offset
    f_c2[ii] = float(f_c2[ii]) #- float(f0_c2)


##### Fourier transform #########################

w_fftc2 = []
for ii in range(0, len(t_c2)):
   w_fftc2.append(2 * np.pi * ii / len(t_c2))

N_fftc2     = len(t_c2)/2+1
dt_fftc2    = t_c2[2]-t_c2[1]
w_max_fftc2 = 1/dt_fftc2*2*np.pi
w_fftc2     = np.linspace(0, w_max_fftc2/2, N_fftc2)
f_fftc2     = 2*fft(f_c2)/N_fftc2




###################################################################################################
#  Dataset 7:
###################################################################################################

with open('./372_cobra/td.general/current_z_at_00_00_00') as c3:
    readlines = c3.readlines()
    t_c3 = [line.split()[0] for line in readlines]
    f_c3 = [line.split()[1] for line in readlines]
f0_c3 = f_c3[step_0]
for ii in range(0, len(t_c3)):
    t_c3[ii] = float(t_c3[ii]) + t_offset
    f_c3[ii] = float(f_c3[ii]) #- float(f0_c3)


##### Fourier transform #########################

w_fftc3 = []
for ii in range(0, len(t_c3)):
   w_fftc3.append(2 * np.pi * ii / len(t_c3))

N_fftc3     = len(t_c3)/2+1
dt_fftc3    = t_c3[2]-t_c3[1]
w_max_fftc3 = 1/dt_fftc3*2*np.pi
w_fftc3     = np.linspace(0, w_max_fftc3/2, N_fftc3)
f_fftc3     = 2*fft(f_c3)/N_fftc3




###################################################################################################
#  Dataset 8:
###################################################################################################

with open('./373_cobra/td.general/current_z_at_00_00_00') as c4:
    readlines = c4.readlines()
    t_c4 = [line.split()[0] for line in readlines]
    f_c4 = [line.split()[1] for line in readlines]
f0_c4 = f_c4[step_0]
for ii in range(0, len(t_c4)):
    t_c4[ii] = float(t_c4[ii]) + t_offset
    f_c4[ii] = float(f_c4[ii]) #- float(f0_c4)


##### Fourier transform #########################

w_fftc4 = []
for ii in range(0, len(t_c4)):
   w_fftc4.append(2 * np.pi * ii / len(t_c4))

N_fftc4     = len(t_c4)/2+1
dt_fftc4    = t_c4[2]-t_c4[1]
w_max_fftc4 = 1/dt_fftc4*2*np.pi
w_fftc4     = np.linspace(0, w_max_fftc4/2, N_fftc4)
f_fftc4     = 2*fft(f_c4)/N_fftc4




###################################################################################################
#  Dataset 9:
###################################################################################################

with open('./370_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez5:
    readlines = ez5.readlines()
    t_ez5 = [line.split()[0] for line in readlines]
    f_ez5 = [line.split()[1] for line in readlines]
f0_ez5 = f_ez5[step_0]
for ii in range(0, len(t_ez5)):
    t_ez5[ii] = float(t_ez5[ii]) + t_offset
    f_ez5[ii] = float(f_ez5[ii]) #- float(f0_ez5)


##### Fourier transform #########################

w_fftez5 = []
for ii in range(0, len(t_ez5)):
   w_fftez5.append(2 * np.pi * ii / len(t_ez5))

N_fftez5     = len(t_ez5)/2+1
dt_fftez5    = t_ez5[2]-t_ez5[1]
w_max_fftez5 = 1/dt_fftez5*2*np.pi
w_fftez5     = np.linspace(0, w_max_fftez5/2, N_fftez5)
f_fftez5     = 2*fft(f_ez5)/N_fftez5




###################################################################################################
#  Dataset 10:
###################################################################################################

with open('./371_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez6:
    readlines = ez6.readlines()
    t_ez6 = [line.split()[0] for line in readlines]
    f_ez6 = [line.split()[1] for line in readlines]
f0_ez6 = f_ez6[step_0]
for ii in range(0, len(t_ez6)):
    t_ez6[ii] = float(t_ez6[ii]) + t_offset
    f_ez6[ii] = float(f_ez6[ii]) #- float(f0_ez6)


##### Fourier transform #########################

w_fftez6 = []
for ii in range(0, len(t_ez6)):
   w_fftez6.append(2 * np.pi * ii / len(t_ez6))

N_fftez6     = len(t_ez6)/2+1
dt_fftez6    = t_ez6[2]-t_ez6[1]
w_max_fftez6 = 1/dt_fftez6*2*np.pi
w_fftez6     = np.linspace(0, w_max_fftez6/2, N_fftez6)
f_fftez6     = 2*fft(f_ez6)/N_fftez6




###################################################################################################
#  Dataset 11:
###################################################################################################

with open('./372_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez7:
    readlines = ez7.readlines()
    t_ez7 = [line.split()[0] for line in readlines]
    f_ez7 = [line.split()[1] for line in readlines]
f0_ez7 = f_ez7[step_0]
for ii in range(0, len(t_ez7)):
    t_ez7[ii] = float(t_ez7[ii]) + t_offset
    f_ez7[ii] = float(f_ez7[ii]) - float(f0_ez7)


##### Fourier transform #########################

w_fftez7 = []
for ii in range(0, len(t_ez7)):
   w_fftez7.append(2 * np.pi * ii / len(t_ez7))

N_fftez7     = len(t_ez7)/2+1
dt_fftez7    = t_ez7[2]-t_ez7[1]
w_max_fftez7 = 1/dt_fftez7*2*np.pi
w_fftez7     = np.linspace(0, w_max_fftez7/2, N_fftez7)
f_fftez7     = 2*fft(f_ez7)/N_fftez7




###################################################################################################
#  Dataset 12:
###################################################################################################

with open('./373_cobra/td.general/maxwell_e_field_z_at_p37_00_00') as ez8:
    readlines = ez8.readlines()
    t_ez8 = [line.split()[0] for line in readlines]
    f_ez8 = [line.split()[1] for line in readlines]
f0_ez8 = f_ez8[step_0]
for ii in range(0, len(t_ez8)):
    t_ez8[ii] = float(t_ez8[ii]) + t_offset
    f_ez8[ii] = float(f_ez8[ii]) #- float(f0_ez8)


##### Fourier transform #########################

w_fftez8 = []
for ii in range(0, len(t_ez8)):
   w_fftez8.append(2 * np.pi * ii / len(t_ez8))

N_fftez8     = len(t_ez8)/2+1
dt_fftez8    = t_ez8[2]-t_ez8[1]
w_max_fftez8 = 1/dt_fftez8*2*np.pi
w_fftez8     = np.linspace(0, w_max_fftez8/2, N_fftez8)
f_fftez8     = 2*fft(f_ez8)/N_fftez8




###################################################################################################
#  Dataset 13:
###################################################################################################
t_eszl = []
f_eszl = []
f_eszl_max = 0.0
with open('./370_cobra/td.general/maxwell_electric_field_surface-z') as eszl:
    for line in eszl:
        split_len = len(line.split())
        if (split_len == 14 ):
           t_eszl.append(line.split()[1])
           f_eszl.append(line.split()[13])
for ii in range(0, len(t_eszl)):
    t_eszl[ii] = float(t_eszl[ii]) + t_offset
    f_eszl[ii] = float(f_eszl[ii]) 
    if (np.abs(f_eszl[ii]) > f_eszl_max):
       f_eszl_max = np.abs(f_eszl[ii])
for ii in range(0, len(t_eszl)):
    t_eszl[ii] = float(t_eszl[ii])
    f_eszl[ii] = float(f_eszl[ii])/f_eszl_max*amplitude


##### Fourier transform #########################

w_ffteszl = []
for ii in range(0, len(t_eszl)):
   w_ffteszl.append(2 * np.pi * ii / len(t_eszl))

N_ffteszl     = len(t_eszl)/2+1
dt_ffteszl    = t_eszl[2]-t_eszl[1]
w_max_ffteszl = 1/dt_ffteszl*2*np.pi
w_ffteszl     = np.linspace(0, w_max_ffteszl/2, N_ffteszl)
f_ffteszl     = 2*fft(f_eszl)/N_ffteszl




###################################################################################################
#  Dataset 14:
###################################################################################################
t_esz1 = []
f_esz1 = []
with open('./370_cobra/td.general/maxwell_electric_field_surface-z') as esz1:
    for line in esz1:
        split_len = len(line.split())
        if (split_len == 14 ):
           t_esz1.append(line.split()[1])
           f_esz1.append(line.split()[7])
for ii in range(0, len(t_esz1)):
    t_esz1[ii] = float(t_esz1[ii]) + t_offset
    f_esz1[ii] = float(f_esz1[ii])/f_eszl_max*amplitude


##### Fourier transform #########################

w_fftesz1 = []
for ii in range(0, len(t_esz1)):
   w_fftesz1.append(2 * np.pi * ii / len(t_esz1))

N_fftesz1     = len(t_esz1)/2+1
dt_fftesz1    = t_esz1[2]-t_esz1[1]
w_max_fftesz1 = 1/dt_fftesz1*2*np.pi
w_fftesz1     = np.linspace(0, w_max_fftesz1/2, N_fftesz1)
f_fftesz1     = 2*fft(f_esz1)/N_fftesz1




###################################################################################################
#  Dataset 15:
###################################################################################################
t_esz2 = []
f_esz2 = []
with open('./371_cobra/td.general/maxwell_electric_field_surface-z') as esz2:
    for line in esz2:
        split_len = len(line.split())
        if (split_len == 14 ):
           t_esz2.append(line.split()[1])
           f_esz2.append(line.split()[7])
for ii in range(0, len(t_esz2)):
    t_esz2[ii] = float(t_esz2[ii]) + t_offset
    f_esz2[ii] = float(f_esz2[ii])/f_eszl_max*amplitude


##### Fourier transform #########################

w_fftesz2 = []
for ii in range(0, len(t_esz2)):
   w_fftesz2.append(2 * np.pi * ii / len(t_esz2))

N_fftesz2     = len(t_esz2)/2+1
dt_fftesz2    = t_esz2[2]-t_esz2[1]
w_max_fftesz2 = 1/dt_fftesz2*2*np.pi
w_fftesz2     = np.linspace(0, w_max_fftesz2/2, N_fftesz2)
f_fftesz2     = 2*fft(f_esz2)/N_fftesz2




###################################################################################################
#  Dataset 16:
###################################################################################################
t_esz3 = []
f_esz3 = []
with open('./372_cobra/td.general/maxwell_electric_field_surface-z') as esz3:
    for line in esz3:
        split_len = len(line.split())
        if (split_len == 14 ):
           t_esz3.append(line.split()[1])
           f_esz3.append(line.split()[7])
for ii in range(0, len(t_esz3)):
    t_esz3[ii] = float(t_esz3[ii]) + t_offset
    f_esz3[ii] = float(f_esz3[ii])/f_eszl_max*amplitude


##### Fourier transform #########################

w_fftesz3 = []
for ii in range(0, len(t_esz3)):
   w_fftesz3.append(2 * np.pi * ii / len(t_esz3))

N_fftesz3     = len(t_esz3)/2+1
dt_fftesz3    = t_esz3[2]-t_esz3[1]
w_max_fftesz3 = 1/dt_fftesz3*2*np.pi
w_fftesz3     = np.linspace(0, w_max_fftesz3/2, N_fftesz3)
f_fftesz3     = 2*fft(f_esz3)/N_fftesz3




###################################################################################################
#  Dataset 17:
###################################################################################################
t_esz4 = []
f_esz4 = []
with open('./373_cobra/td.general/maxwell_electric_field_surface-z') as esz4:
    for line in esz4:
        split_len = len(line.split())
        if (split_len == 14 ):
           t_esz4.append(line.split()[1])
           f_esz4.append(line.split()[7])
for ii in range(0, len(t_esz4)):
    t_esz4[ii] = float(t_esz4[ii]) + t_offset
    f_esz4[ii] = float(f_esz4[ii])/f_eszl_max*amplitude


##### Fourier transform #########################

w_fftesz4 = []
for ii in range(0, len(t_esz4)):
   w_fftesz4.append(2 * np.pi * ii / len(t_esz4))

N_fftesz4     = len(t_esz4)/2+1
dt_fftesz4    = t_esz4[2]-t_esz4[1]
w_max_fftesz4 = 1/dt_fftesz4*2*np.pi
w_fftesz4     = np.linspace(0, w_max_fftesz4/2, N_fftesz4)
f_fftesz4     = 2*fft(f_esz4)/N_fftesz4





###################################################################################################
#  Plots:
###################################################################################################

#fig.suptitle(r"Induced current and electric field enhancement in z-direction between the dimer for d$_2$=0.5nm",x=0.48,y=0.98, size=19)

##### Panel 1 plot ##############################
#ax1_datal, = ax1.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), '-', color=(0.100,0.100,0.100), linewidth=1.5, label='total field of the external laser')

##### Panel 2 plot ##############################
#ax2_data4, = ax2.plot(t_c4, f_c4, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='total field with forward coupling beyond dipole approximation')
#ax2_data2, = ax2.plot(t_c2, f_c2, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='total field forward coupled')
#ax2_data1, = ax2.plot(t_c1, f_c1, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='total field fully coupled')
#ax2_data3, = ax2.plot(t_c3, f_c3, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='total field with forward-backward coupling beyond dipole approximation')

##### Panel 3 plot ##############################
ax3_datal, = ax3.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), '-', color=(0.100,0.100,0.100), linewidth=1.5, label='total field of the external laser', alpha=0.5)
#ax3_data4, = ax3.plot(t_ez4, f_ez4, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='total field with forward coupling beyond dipole approximation')
ax3_data2, = ax3.plot(t_ez2, f_ez2, '-', color=(0.000,0.000,1.000), linewidth=1.5, label='total field forward coupled')
#ax3_data1, = ax3.plot(t_ez1, f_ez1, '-', color=(1.000,0.000,0.000), linewidth=1.5, label='total field fully coupled')
#ax3_data3, = ax3.plot(t_ez3, f_ez3, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='total field with forward-backward coupling beyond dipole approximation')

##### Panel 4 plot ##############################
#ax4_datal, = ax4.plot(t_laser, f_laser(t_laser,amplitude,shift,width,omega), '-', color=(0.100,0.100,0.100), linewidth=1.5, label='total field of the external laser', alpha=0.5)
#ax4_data4, = ax4.plot(t_ez8, f_ez8, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='total field with forward coupling beyond dipole approximation')
#ax4_data2, = ax4.plot(t_ez6, f_ez6, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='total field forward coupled')
#ax4_data1, = ax4.plot(t_ez5, f_ez5, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='total field fully coupled')
#ax4_data3, = ax4.plot(t_ez7, f_ez7, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='total field with forward-backward coupling beyond dipole approximation')

##### Panel 5 plot ##############################
#ax5_datal, = ax5.plot(t_eszl, f_eszl, '-', color=(0.100,0.100,0.100), linewidth=1.5, label='external laser', alpha=0.5)
#ax5_data4, = ax5.plot(t_esz4, f_esz4, '-', color=(0.700,0.700,0.000), linewidth=1.5, label='forward coupling beyond dipole approximation')
#ax5_data2, = ax5.plot(t_esz2, f_esz2, '-', color=(0.000,0.000,0.700), linewidth=1.5, label='total field forward coupled')
#ax5_data1, = ax5.plot(t_esz1, f_esz1, '-', color=(0.000,0.700,0.000), linewidth=1.5, label='total field fully coupled')
#ax5_data3, = ax5.plot(t_esz3, f_esz3, '-', color=(0.700,0.000,0.000), linewidth=1.5, label='forward-backward coupling beyond dipole approximation')



###################################################################################################
#  Units transformation:
###################################################################################################

# Femtosecond to atomic units factor
femto_in_au    = 103.353

# energy from atomic untis to eV
au_in_eV       = 27.2114

# electric field from atomic units to V/m
au_in_V_per_m = 5.14220652*(10**11)

# current from atomic unts to A
au_in_A = 6.623618183*(10**(-3))

# x-tick interval
femto_interval = 2.5

# x-ticks for all plots
xticks_femto      = np.arange(0, period_number*period, femto_in_au)
xticklabels_femto = np.arange(0, period_number*period/femto_in_au*femto_interval, femto_interval)  

# x-tick-labels for all plots
yticks_eV       = np.arange(-142.905, -142.88, 0.005)
yticklabels_eV  = np.arange(-3888.65, -3887.96, 0.05, dtype=float)




###################################################################################################
#  Panel 1: Laser
###################################################################################################

# Axes label top
#ax1.set_xlabel("time in atomic units", size=15)
#ax1.xaxis.set_label_coords(0.5,1.40)
#ax1.xaxis.set_label_position('top')

# au limits for the laser
#ax1_xlim_1 = 0.0
#ax1_xlim_2 = period_number*period
#ax1_ylim_1 = -1.10e-4
#ax1_ylim_2 =  1.10e-4

#factor = 10**7

#ax1.text(ax1_xlim_1+0.02*(np.abs(ax1_xlim_2-ax1_xlim_1)), ax1_ylim_1+0.8*(np.abs(ax1_ylim_2-ax1_ylim_1)),'a)',size=18)


##### Y left label axes #########################

#ax1_y1 = ax1.twinx()

# Axes label on the left
#ax1_y1.set_ylabel(r'E$_{z}$ [$\times 10^7$ V/m]', size=13, position=(0.0,0.55))
#ax1_y1.yaxis.set_label_position('left')
#ax1_y1.yaxis.set_label_coords(-.080, 0.55)

# ticks
#ax1_y1_ticks = [-4*10**7/factor,0.0/factor,4*10**7/factor]
#ax1_y1.set_yticks(ax1_y1_ticks)
#ax1_y1.set_yticklabels(ax1_y1_ticks, size=13)
#ax1_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
#ax1_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# limits
#ax1_y1_ylim_1 = ax1_ylim_1 * au_in_V_per_m / factor
#ax1_y1_ylim_2 = ax1_ylim_2 * au_in_V_per_m / factor
#ax1_y1.set_ylim(ax1_y1_ylim_1, ax1_y1_ylim_2)


##### Y right label axes ########################

#ax1_y2 = ax1.twinx()

# Axes label on the right
#ax1_y2.set_ylabel(r'E$_{z}$ [a.u.]', size=15, position=(0.0,0.55))
#ax1_y2.yaxis.set_label_position('right')

# ticks
#ax1_y2_ticks = [-4*10**7/au_in_V_per_m, 0.0/au_in_V_per_m, 4*10**7/au_in_V_per_m]
#ax1_y2.set_yticks(ax1_y2_ticks)
#ax1_y2.set_yticklabels(ax1_y2_ticks, size=13)
#ax1_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax1_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax1_y2_ylim_1 = ax1_ylim_1
#ax1_y2_ylim_2 = ax1_ylim_2
#ax1_y2.set_ylim(ax1_y2_ylim_1, ax1_y2_ylim_2)


##### plot axes #################################

# labels
#ax1.set_xticks(xticks_femto)
#ax1.set_xticklabels(xticks_femto, size=13)
#ax1.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
#ax1.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
#ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax1.set_xlim(ax1_xlim_1, ax1_xlim_2)
#ax1.set_ylim(ax1_ylim_1, ax1_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -10, 10]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax1.add_line(line)
#r1 = [ 0, period_number*period]
#r2 = [ 0,  0]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax1.add_line(line)




###################################################################################################
#  Panel 2: current density
###################################################################################################

# au limits for the laser
#ax2_xlim_1 = 0.0
#ax2_xlim_2 = period_number*period
#ax2_ylim_1 = -4.7e-8
#ax2_ylim_2 =  4.7e-8

#factor = 10**(-10)

#ax2.text(ax2_xlim_1+0.02*(np.abs(ax2_xlim_2-ax2_xlim_1)), ax2_ylim_1+0.8*(np.abs(ax2_ylim_2-ax2_ylim_1)),'b)',size=18)


##### Y left label axes #########################

#ax2_y1 = ax2.twinx()

# Axes label on the left
#ax2_y1.set_ylabel(r"j$_z$ [$\times 10^{-10}$ A]", size=13, position=(0.0,0.55))
#ax2_y1.yaxis.set_label_position('left')
#ax2_y1.yaxis.set_label_coords(-.080, 0.55)

# labels
#ax2_y1_ticks = [-2.0*(10**(-10))/factor, 0.0/factor, 2.0*(10**(-10))/factor]
#ax2_y1.set_yticks(ax2_y1_ticks)
#ax2_y1.set_yticklabels(ax2_y1_ticks, size=13)
#ax2_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
#ax2_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# limits
#ax2_y1_ylim_1 = ax2_ylim_1 * au_in_A / factor
#ax2_y1_ylim_2 = ax2_ylim_2 * au_in_A / factor
#ax2_y1.set_ylim(ax2_y1_ylim_1, ax2_y1_ylim_2)


##### Y right label axes ########################

#ax2_y2 = ax2.twinx()

# Axes label on the right
#ax2_y2.set_ylabel(r"j$_z$ [a.u.]", size=15, position=(0.0,0.55))
#ax2_y2.yaxis.set_label_position('right')

# ticks
#ax2_y2_ticks = [-2.0*(10**(-10))/au_in_A, 0.0, 2.0*(10**(-10))/au_in_A]
#ax2_y2.set_yticks(ax2_y2_ticks)
#ax2_y2.set_yticklabels(ax2_y2_ticks, size=13)
#ax2_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax2_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax2_y2_ylim_1 = ax2_ylim_1
#ax2_y2_ylim_2 = ax2_ylim_2
#ax2_y2.set_ylim(ax2_y2_ylim_1, ax2_y2_ylim_2)


##### plot axes #################################

# labels
#ax2.set_xticks(xticks_femto)
#ax2.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
#ax2.xaxis.set_major_formatter(FormatStrFormatter('%.1f'))
#ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax2.set_xlim(ax2_xlim_1, ax2_xlim_2)
#ax2.set_ylim(ax2_ylim_1, ax2_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -10, 10]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax2.add_line(line)
#r1 = [ 0, period_number*period]
#r2 = [ 0,  0]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax2.add_line(line)




###################################################################################################
#  Panel 3: electric near field
###################################################################################################

# au limits for the laser
ax3_xlim_1 = 0.0
ax3_xlim_2 = period_number*period
ax3_ylim_1 = -5.10e-3
ax3_ylim_2 =  5.10e-3

factor = 10**7

#ax3.text(ax3_xlim_1+0.02*(np.abs(ax3_xlim_2-ax3_xlim_1)), ax3_ylim_1+0.8*(np.abs(ax3_ylim_2-ax3_ylim_1)),'c)',size=18)


##### Y left label axes #########################

ax3_y1 = ax3.twinx()

# Axes label on the left
ax3_y1.set_ylabel(r"E$_{z}$ [$\times 10^{7}$ V/m]", size=15, position=(-0.,0.50))
#ax3_y1.yaxis.set_label_position('left')
ax3_y1.yaxis.set_label_coords(-.100, 0.55)

# labels
ax3_y1_ticks = np.arange(-2.0*10**9/factor, 2.1*10**9/factor, 1.0*10**9/factor)
ax3_y1.set_yticks(ax3_y1_ticks)
ax3_y1.set_yticklabels(ax3_y1_ticks, size=13)
ax3_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
ax3_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# limits
ax3_y1_ylim_1 = ax3_ylim_1 * au_in_V_per_m / factor
ax3_y1_ylim_2 = ax3_ylim_2 * au_in_V_per_m / factor
ax3_y1.set_ylim(ax3_y1_ylim_1, ax3_y1_ylim_2)


##### Y right label axes ########################

#ax3_y2 = ax3.twinx()

# Axes label on the right
#ax3_y2.set_ylabel(r"E$_{z}$ [a.u.]", size=15, position=(0.0,0.50))
#ax3_y2.yaxis.set_label_position('right')

# ticks
#ax3_y2_ticks = ax3_y1_ticks / au_in_V_per_m
#ax3_y2.set_yticks(ax3_y2_ticks)
#ax3_y2.set_yticklabels(ax3_y2_ticks, size=13)
#ax3_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax3_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax3_y2_ylim_1 = ax3_ylim_1
#ax3_y2_ylim_2 = ax3_ylim_2
#ax3_y2.set_ylim(ax3_y2_ylim_1, ax3_y2_ylim_2)


##### plot axes #################################

# Axes label bottom
ax3.set_xlabel("time in femto seconds", size=15)
ax3.xaxis.set_label_position('bottom')

# labels
ax3.set_xticks(xticks_femto)
ax3.set_xticklabels(xticklabels_femto, size=13)
ax3.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='on', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
ax3.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# Limits
ax3.set_xlim(ax3_xlim_1, ax3_xlim_2)
ax3.set_ylim(ax3_ylim_1, ax3_ylim_2)

# grid lines
for ii in range(0, 50, 1):
   r1 = [  ii*period, ii*period]
   r2 = [ -10, 10]
   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
   ax3.add_line(line)
r1 = [ 0, period_number*period]
r2 = [ 0,  0]
line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
ax3.add_line(line)




###################################################################################################
#  Panel 4: electric far field
###################################################################################################

# au limits for the laser
#ax4_xlim_1 = 0.0
#ax4_xlim_2 = period_number*period
#ax4_ylim_1 = -1.80e-4
#ax4_ylim_2 =  1.80e-4

#factor = 10**7

#ax4.text(ax4_xlim_1+0.02*(np.abs(ax4_xlim_2-ax4_xlim_1)), ax4_ylim_1+0.8*(np.abs(ax4_ylim_2-ax4_ylim_1)),'d)',size=18)


##### Y left label axes #########################

#ax4_y1 = ax4.twinx()

# Axes label on the left
#ax4_y1.set_ylabel(r"E$_{z}$ [$\times 10^{7}$ V/m]", size=13, position=(0.0,0.50))
#ax4_y1.yaxis.set_label_position('left')
#ax4_y1.yaxis.set_label_coords(-.080, 0.55)

# labels
#ax4_y1_ticks = np.arange(-2.0*10**8/factor, 2.1*10**8/factor, 5.0*10**7/factor)
#ax4_y1.set_yticks(ax4_y1_ticks)
#ax4_y1.set_yticklabels(ax4_y1_ticks, size=13)
#ax4_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
#ax4_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# limits
#ax4_y1_ylim_1 = ax4_ylim_1 * au_in_V_per_m / factor
#ax4_y1_ylim_2 = ax4_ylim_2 * au_in_V_per_m / factor
#ax4_y1.set_ylim(ax4_y1_ylim_1, ax4_y1_ylim_2)


##### Y right label axes ########################

#ax4_y2 = ax4.twinx()

# Axes label on the right
#ax4_y2.set_ylabel(r"E$_{z}$ [a.u.]", size=15, position=(0.0,0.50))
#ax4_y2.yaxis.set_label_position('right')

# ticks
#ax4_y2_ticks = ax4_y1_ticks / au_in_V_per_m
#ax4_y2.set_yticks(ax4_y2_ticks)
#ax4_y2.set_yticklabels(ax4_y2_ticks, size=13)
#ax4_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax4_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax4_y2_ylim_1 = ax4_ylim_1
#ax4_y2_ylim_2 = ax4_ylim_2
#ax4_y2.set_ylim(ax4_y2_ylim_1, ax4_y2_ylim_2)


##### plot axes #################################

# Axes label bottom
#ax4.set_xlabel("time in femto seconds", size=15)
#ax4.xaxis.set_label_position('bottom')

# labels
#ax4.set_xticks(xticks_femto)
#ax4.set_xticklabels(xticklabels_femto, size=13)
#ax4.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='off', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
#ax4.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# Limits
#ax4.set_xlim(ax4_xlim_1, ax4_xlim_2)
#ax4.set_ylim(ax4_ylim_1, ax4_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -10, 10]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax4.add_line(line)
#r1 = [ 0, period_number*period]
#r2 = [ 0,  0]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax4.add_line(line)




###################################################################################################
#  Panel 5: electric field at surface
###################################################################################################

# au limits for the laser
#ax5_xlim_1 = 0.0
#ax5_xlim_2 = period_number*period
#ax5_ylim_1 = -1.80e-4
#ax5_ylim_2 =  1.80e-4

#factor = 10**7

#ax5.text(ax5_xlim_1+0.02*(np.abs(ax5_xlim_2-ax5_xlim_1)), ax5_ylim_1+0.8*(np.abs(ax5_ylim_2-ax5_ylim_1)),'e)',size=18)


##### Y left label axes #########################

#ax5_y1 = ax5.twinx()

# Axes label on the left
#ax5_y1.set_ylabel(r"E$_{z}$ [$\times 10^{7}$ V/m]", size=13, position=(0.0,0.50))
#ax5_y1.yaxis.set_label_position('left')
#ax5_y1.yaxis.set_label_coords(-.080, 0.55)

# labels
#ax5_y1_ticks = np.arange(-2.0*10**8/factor, 2.1*10**8/factor, 5.0*10**7/factor)
#ax5_y1.set_yticks(ax5_y1_ticks)
#ax5_y1.set_yticklabels(ax5_y1_ticks, size=13)
#ax5_y1.tick_params(labelleft='on', labelright='off', labeltop='off', labelbottom='off', bottom='off', top='off', right='on', left='on', direction='inout', length=8)
#ax5_y1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# limits
#ax5_y1_ylim_1 = ax5_ylim_1 * au_in_V_per_m / factor
#ax5_y1_ylim_2 = ax5_ylim_2 * au_in_V_per_m / factor
#ax5_y1.set_ylim(ax5_y1_ylim_1, ax5_y1_ylim_2)


##### Y right label axes ########################

#ax5_y2 = ax5.twinx()

# Axes label on the right
#ax5_y2.set_ylabel(r"E$_{z}$ [a.u.]", size=15, position=(0.0,0.50))
#ax5_y2.yaxis.set_label_position('right')

# ticks
#ax5_y2_ticks = ax5_y1_ticks / au_in_V_per_m
#ax5_y2.set_yticks(ax5_y2_ticks)
#ax5_y2.set_yticklabels(ax5_y2_ticks, size=13)
#ax5_y2.tick_params(labelleft='off', labelright='on', labeltop='off', labelbottom='off', bottom='off', top='off', right='off', left='off', direction='inout', length=8)
#ax5_y2.yaxis.set_major_formatter(FormatStrFormatter('%.1e'))

# limits
#ax5_y2_ylim_1 = ax5_ylim_1
#ax5_y2_ylim_2 = ax5_ylim_2
#ax5_y2.set_ylim(ax5_y2_ylim_1, ax5_y2_ylim_2)


##### plot axes #################################

# Axes label bottom
#ax5.set_xlabel("time in femto seconds", size=15)
#ax5.xaxis.set_label_position('bottom')

# labels
#ax5.set_xticks(xticks_femto)
#ax5.set_xticklabels(xticklabels_femto, size=13)
#ax5.tick_params(labelleft='off', labelright='off', labeltop='off', labelbottom='on', bottom='on', top='on', right='off', left='off', direction='inout', length=8)
#ax5.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))

# Limits
#ax5.set_xlim(ax5_xlim_1, ax5_xlim_2)
#ax5.set_ylim(ax5_ylim_1, ax5_ylim_2)

# grid lines
#for ii in range(0, 50, 1):
#   r1 = [  ii*period, ii*period]
#   r2 = [ -10, 10]
#   line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#   ax5.add_line(line)
#r1 = [ 0, period_number*period]
#r2 = [ 0,  0]
#line = lines.Line2D(r1, r2, color='black', alpha=0.3, lw=1.0, linestyle='-')
#ax5.add_line(line)




###################################################################################################
#  Legend:
###################################################################################################

#ax1.legend(handles=[ax1_datal, ax2_data2, ax2_data4, ax2_data1, ax2_data3], prop={'size': 12}, loc=( 0.1,1.70), ncol=6)
ax3.legend(handles=[ax3_datal, ax3_data2], prop={'size': 14}, loc=(-0.11,1.05), frameon=False, ncol=3)



###################################################################################################
#  Save figure:
###################################################################################################

DPI = fig.get_dpi()
fig.set_size_inches(1200.0/float(DPI),600.0/float(DPI))

fig.savefig('Na_297_dimer_0_5_nm_fixed_ions_electric_field_z__talk_3.png',bbox_inches='tight',pad_inches = 0)

#plt.show()
