(define sim_size_x 12)
(define sim_size_y 12)
(define sim_size_z 12)
(define freq 0.5)
(define freq_width 1)
(define start_time 5)
(define extra_simulation_time 10)

;(define (myoutput)
; (print "myoutput:, " (meep-time) ", "
; (get-field-point Ex (vector3 0 0 0)) "\n"))

(define (my_func_ex) (print "Ex = " (get-field-point Ex (vector3 1 1 0)) "\n"))
(define (my_func_ey) (print "Ey = " (get-field-point Ey (vector3 1 1 0)) "\n"))
(define (my_func_ez) (print "Ez = " (get-field-point Ez (vector3 1 1 0)) "\n"))
(define (my_func_hx) (print "Hx = " (get-field-point Hx (vector3 1 1 0)) "\n"))
(define (my_func_hy) (print "Hy = " (get-field-point Hy (vector3 1 1 0)) "\n"))
(define (my_func_hz) (print "Hz = " (get-field-point Hz (vector3 1 1 0)) "\n"))
(define (my_func_free_line) (print "\n" ))
(define (my_func_time) (print meep-time  "\n"))

(set! geometry-lattice (make lattice (size sim_size_x sim_size_y sim_size_z)))

(set! resolution 5)

(set! sources (list (make source (src
 (make gaussian-src
  (fwidth freq_width)
  (frequency freq)))
  (component Ez)
  (amplitude 1)
  (center 0 0 0)
  (size 0 0 sim_size_z))))

;(get-field-point (Ez vector3( 5 5 0)))

;(size 0 sim_size_yz sim_size_yz)

(set! pml-layers (list (make pml (thickness 2.0))))

;(run-sources+ extra_simulation_time
;           (to-appended "ez" (at-every 0.1 output-efield-z)))

;(run-sources+ extra_simulation_time
;           (at-every 0.1 (output-png Ez "-vZc dkbluered -M 1 -0z0")))

(run-sources+ extra_simulation_time
           (at-every 0.1 my_func_time my_func_ex my_func_ey my_func_ez my_func_hx my_func_hy my_func_hz (output-png Ez "-vZc dkbluered -M 1 -0z0") my_func_free_line))
