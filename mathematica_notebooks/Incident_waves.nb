(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     38030,        820]
NotebookOptionsPosition[     36295,        752]
NotebookOutlinePosition[     36632,        767]
CellTagsIndexPosition[     36589,        764]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"k1x", "=", "0.707107"}]], "Input",
 CellChangeTimes->{{3.760257933442512*^9, 3.760257943072556*^9}}],

Cell[BoxData["0.707107`"], "Output",
 CellChangeTimes->{{3.7602580116011*^9, 3.760258032583778*^9}, 
   3.760258190931799*^9, {3.760279166800396*^9, 3.7602791950207357`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"k1y", "=", 
  RowBox[{"-", "0.707107"}]}]], "Input",
 CellChangeTimes->{{3.760257944645103*^9, 3.760257958843287*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.707107`"}]], "Output",
 CellChangeTimes->{{3.760258011783915*^9, 3.760258032940879*^9}, 
   3.760258191091989*^9, {3.7602791670173492`*^9, 3.760279195117675*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"k1abs", "=", 
  RowBox[{"Sqrt", "[", 
   RowBox[{
    RowBox[{"k1x", "^", "2"}], "+", 
    RowBox[{"k1y", "^", "2"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.760257998342639*^9, 3.7602580098763733`*^9}}],

Cell[BoxData["1.0000003094489522`"], "Output",
 CellChangeTimes->{{3.760258011914859*^9, 3.7602580330178013`*^9}, 
   3.7602581911637583`*^9, {3.760279167148513*^9, 3.760279195132777*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"k2x", "=", 
  RowBox[{"-", "0.447214"}]}]], "Input",
 CellChangeTimes->{{3.760257964058053*^9, 3.760257973275179*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.447214`"}]], "Output",
 CellChangeTimes->{{3.7602580120449038`*^9, 3.760258033189937*^9}, 
   3.760258191335926*^9, {3.760279167269209*^9, 3.760279195146781*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"k2y", "=", 
  RowBox[{"-", "0.223607"}]}]], "Input",
 CellChangeTimes->{{3.760257974544826*^9, 3.76025799524223*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.223607`"}]], "Output",
 CellChangeTimes->{{3.7602580121919117`*^9, 3.760258033359695*^9}, 
   3.760258191416843*^9, {3.7602791673844013`*^9, 3.760279195160844*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"k2abs", "=", 
  RowBox[{"Sqrt", "[", 
   RowBox[{
    RowBox[{"k2x", "^", "2"}], "+", 
    RowBox[{"k2y", "^", "2"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.760258014000757*^9, 3.7602580309707603`*^9}}],

Cell[BoxData["0.5000004522447955`"], "Output",
 CellChangeTimes->{
  3.760258033468054*^9, 3.760258191554008*^9, {3.7602791675234756`*^9, 
   3.760279195174715*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"shift1x", "=", 
  RowBox[{"-", "17.6777"}]}]], "Input",
 CellChangeTimes->{{3.760258110624991*^9, 3.760258127949264*^9}}],

Cell[BoxData[
 RowBox[{"-", "17.6777`"}]], "Output",
 CellChangeTimes->{
  3.760258191624555*^9, {3.76027916759548*^9, 3.760279195188758*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"shift1y", "=", "17.6777"}]], "Input",
 CellChangeTimes->{{3.760258129370254*^9, 3.760258141180457*^9}}],

Cell[BoxData["17.6777`"], "Output",
 CellChangeTimes->{
  3.760258191773875*^9, {3.7602791676983433`*^9, 3.7602791952024603`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"shift1", "=", 
  RowBox[{"Sqrt", "[", 
   RowBox[{
    RowBox[{"shift1x", "^", "2"}], "+", 
    RowBox[{"shift1y", "^", "2"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7602581587066917`*^9, 3.760258171807455*^9}}],

Cell[BoxData["25.000043091562866`"], "Output",
 CellChangeTimes->{
  3.760258191927423*^9, {3.760279167824695*^9, 3.760279195215631*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"shift2x", "=", "22.3607"}]], "Input",
 CellChangeTimes->{{3.760258142365942*^9, 3.760258149637327*^9}}],

Cell[BoxData["22.3607`"], "Output",
 CellChangeTimes->{
  3.760258192200465*^9, {3.7602791680005493`*^9, 3.7602791952296267`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"shift2y", "=", "11.1804"}]], "Input",
 CellChangeTimes->{{3.7602581506646338`*^9, 3.7602581559623213`*^9}}],

Cell[BoxData["11.1804`"], "Output",
 CellChangeTimes->{
  3.760258192341671*^9, {3.760279168143409*^9, 3.760279195245488*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"shift2", "=", 
  RowBox[{"Sqrt", "[", 
   RowBox[{
    RowBox[{"shift2x", "^", "2"}], "+", 
    RowBox[{"shift2y", "^", "2"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7602581741143427`*^9, 3.7602581896326427`*^9}}],

Cell[BoxData["25.00004497295955`"], "Output",
 CellChangeTimes->{
  3.760258192430484*^9, {3.7602791682781343`*^9, 3.7602791952602663`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{"-", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"x", "^", "2"}], "/", 
         RowBox[{"(", 
          RowBox[{"2", "*", 
           RowBox[{"4.0", "^", "2"}]}], ")"}]}], ")"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"-", "15"}], ",", "15"}], "}"}]}], "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"Cos", "[", 
      RowBox[{
       RowBox[{"\[Pi]", "/", "2"}], "*", 
       RowBox[{"x", "/", "10.0"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"-", "15"}], ",", "15"}], "}"}]}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.760279111647251*^9, 3.760279164736578*^9}, {
  3.7602792519272013`*^9, 3.7602792558030033`*^9}, {3.7602816854406548`*^9, 
  3.760281694094427*^9}, {3.7602817255506067`*^9, 3.760281818465207*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJw1mXk0VW/0/0slIpUmZLpcF9e9lLnJfjRJyFCGRBlLoSRjhoyJkCmZCpUp
GUPxifOUOfOQypSQyHBlzFC+9/db3+9Z55/X2uuss4f33s9e6yFZ3NSzZlnH
fFj+/7vucI/T+NoaHXucjUvduaAJubMvGOordFzNXX5WXVELRDg/zUYv0LHw
9X3CG1y1YMtRhVXxSTo+cXZm2+ZlLeh+Os+p003HXEb8qbas2uBm4SKdUkTH
37XlbKqSdGDiTrrsrzw63nPys+KR9zpgFt2lKJ9Nx0/YXyn9+6EDalWKUJdK
x7tZ78xb7NeFPZRF7elwOk7XehZfUacLRWOut9A1Oh46u0HmMNs5kGDJdA6x
omPrfj3TuAPnIInvi3vnZTq2v5j03+mL58BfQ9nPxoCOxUcyrqblnQO9nD9R
Ecfp2CU8WvuyyXn47eBeNCBAxzHWPjyXa/RB+s+dRZ92GtaZ+hq7/bsRtN5q
C7FuomHzmfJD9/gugOO4uOCZOhpWEPFBLucvQEl/54mdFTRMEZAXqq+/AEer
paPSsmhY1dZY5XyZMWhGDdI++tCwokVx/afXJjDFcRDnetDwS1VTPst5E4gI
fHgu2oWG076+Vb6jZAqdbkfumNjR8AmPyt/q5aZw8XJs7ZQhDce2NyuHtlyC
67QzFjtlaPh5k7t5VZAZcKanzC9K0rDh7wJ9nhwzyBVavN9LpuGUIM/S0nYz
mOF+kZfGR8OFGle2/xQ0B/elv6tKrDRclyU+s1JmDvdrCh6b9EnhdT9NlCNZ
LCHDjLcp7YEU1t5KLczvsAa9RI+N3+9JYdF1Cyhj3RX496nvML+fFOa4/zVs
hn4FDDSevYxyk8JPQpQTg4OvwEZFqft3raXwc2YK6k9cBTOOo8cvgBR+v7n5
9LZGG+AtMSvlnKXi5BufE3+Q7KBq+sO02hQVbzNq+htrbAcOUmIS/mNUHP9j
0TU/2g7qUsZil75RMT/98gXBzfbg8sDRaaSRiqmvcnz/zNhDu1mADE6n4l4L
5zGVbzfhAUdm2m1jKg7lXzsfsNkRnh1xaqnRp2JSXPE/Q6ojvLVHS7y6VFz9
9lqujZYjjLR80cRqVFxAOvn4WIwjoBj2OU4FKr6IliYGybdhgd/2WPo2Kn73
MI5E1XMCC2npb1+qJPGuudu3Hn5zAffLy2w0LIkdxR+lfNjiChERNbJ3/5PE
isjWHRRdoXzm0j2xQkl850Nk395wV+B585DumCyJj4jV+GqrukET/PbguCOJ
t3SOuB0pcQdl3SIeFRlJfDYi1v9Qnyd4DaW3SVEl8fyMbgDbXi/44Bwfwism
ia+kk3xUdL1AI8F7dY5PEusBbWq4xgsuDZ0eyGaVxElX+G7T33hDoHNfBm+/
BN7BqrNDyMAH2uNZleZDJbCPE8+rIBc/2EtbYgwGSeCDX1OXIhP8wKRiPLPV
TwILtWfx9FT4wY/BVr5XbhL48PcnSIrNH/5IJf6zsJLAsRxsgU1P/EGwQqam
9bAE1j+28Nz+UwDYDhrpv/oljoNYXCxb3YKgTMGwcPqHODb4crvD6VkQsAXr
b1P4Lo57Mne0GjcGwQsZvfryz+KY99HjjxPC96HfU+NoS5U43scmbSjbdB/0
9qqIzTwVxw/vvXGNlA2BQ5qi80rnxLEA+VaKoFwY3E8m6XmeFccvqqpkn1uH
wecZoTysLo5nqM9bnOLCwCme/5o6EsfTHep569bCIHdkd58xXRxnfT9ZXd0S
DiRftmqvzeJYOGxhvzSKALaSyZjK/yiYZvVn6dj2KAi4zu37rISCfzq73T51
IArWCynZ+xZQ8LWSrf9560XBcpDPScig4CabD3Xhj6Jg4sLOhbIoCmYtC3OX
F4yGtr/KhoXXKDhdeb6l42AMJJwM4E3dS8Ejb24+vpgUC3uWMzf5cFNwjaDL
Hv+qWIjObfp9aSsF303d7zY6EQuhe/d+5N9AwVFeQ7tvqDwG77GsO3FTYlj1
x4qY/fBjsAhr6XlYLYbvdXKVeB+JB6lPfE+9b4vh8qkt2vxCScDtk+XpfEMM
65nI5BefTYJlqvJFu2tiOFH2vFGMdxJ89NHnMb4shum5B8pEviWBDS0iSkFD
DL9y1XQNff4E0v03BU2IiOH7wy92dh9MBpLs9E3jdjJ+Zv91QFM7Fdj6vM/q
NpHxO1LF3Xe2qTAdtJV+uo6M0fEtQ+fupwLuo/5SqCDjKfbdqcvvU+FSsLXV
jpdkXHTRLoOm9AySBrqN6nzJ2Ma3O+i05HPY+7BaVfEAGZ+/r38gkp4GbCuX
q/xpZKxi+CRks1YaLF9ZPtUmTsbci0UqaXZp0Ksio2knSMbRljHqhq/S4NlU
nMEzDjLmSXw7EU9PB9pZO1uuEVH8dFNwRqlSBhzbuvPRSIIoTiBHXbhqmwXy
7jl75WNFsdn7j43bI7JA7Idagm+kKHa4kXJ8pCgL2Mo9k/nvi2JT2krH7rUs
aLIbydJzFsUroyqZDx69BKPG0ooKbVFcVWnjM/sxG248MBuN3SiK144Y3sZG
uRBuRFr2XhPB8kJk/4Z7uZAjNshxdVkE76YKrG4rzoUJwlJGaVoEL+RTGSe5
8+D63BWXzz0i+GJ5paptSx5Ym9pv5HktgmMy5BwtX+aDicwd4XhzEaxvpWEV
fqoQPFcPyfqaiOCRiPc3jc0LIbF+5fg1QxH8ZCZB7ZRnIXRbel09qCWC2yTk
CK/CQjCK88n9qiyC7w66+twQfg3n1wcd4dsugpUdWjvsWIpAoyPaKLGChOPv
d2y7/60YnMzNuLpLSThEo8HXfrUYnjJoVTzFJBzGU6B7i7cEpjlqZGKzSfj8
J/0LnedKIObEH9bIOBIeujIrseVjCfQVmxTfcyRhLh/j0pPv3sDNODFuRwoJ
j7/d4eT3thSiTd80nA4XxnK5NgrCuypAYnDydnKwMPbYYXLQRbkC3l0h8y8E
CGMnbpOOSZMKGL4ZaffcQxhzpJ9iO5ZeAQp+tlv/2Qjjw86EpM8hAj6nC2m/
Pi6MrS9KXsyXx6A4sC3t8IwQ5ntVfzXM4T2obSoo/FEriM23CmeWClRBY+t1
Ul25IH5lJDxzWLEKdJLIES9fC+Ku89HLHWerwFguzv5msiC+E4LNT/pUgZ35
XYklF0FcYtqkKj1cBVHlWk85KYJYMTyvVSC/GvqdfwXLBQhg0yo95ySjWnAa
ETX3Rfy45qhP3e7Jj8ApP793hxI/1vHoapFib4AXvjXNKXR+zDodna8v1gCd
/NeOYD5+7KWlz/LDtAHkz+XuXZvbhyM5d639bmmAOUK52TNrH86rbeitftMI
zvFah9249+EDUe/Hz8c2g4um6+6bw7w443FWm6VaGwSor5i80N+L63fdr0/m
6QKy552v7e27sMVQ4UZbhx5IkOu+eM6DG6tP/No/ZTsAShs/DSoObsOyJbiz
8s4QDL8s4bUlc2CD+VTh5fMj8PLukov0elbMufA4TPD4GLy02nN2/eQm/FnQ
omvb2THIUpcT6/yyCb+LG65bMRqDzJ32He75m7BtC+/dyhtjkJExIFN9eRMu
bO5e3Z4wBi9aakcvVmzEdvSRzATGGCQLxRoHe2zAPRv2JPHH/oIYQlZleH4d
tjAvSQhoHoeyTO+0HUPrsDSqvEr7PA4DkR85oXUdlj+WrNr0bRzoVhY98S/X
4cSDp1aWpsehhi3KTfvyOhzTNPL8H/cEzDwftHaOXCM6lSxrc/Qn4F3dZW0R
pb/EnNJlPbtPE8CWuCjgteMvsTbibELpmwB9+4cTn8dXiS5hS/cvwxMwtYMI
DktZJW7f3dBMnpsA0kXBqj/sq4ShZfhOU+5JuDfZq9zSu0ywvUnNPak1CTrc
xiKefn+Igkkd7+p3k8A1Rb6efeEPMRv/LmFD1SQ01DMKuvf/Id7ZpZkoNUzC
Kd9A1YPfFonerX55d79OwuHpfLOFw4uEgWddVtT8JFCaNyc7zM8TXhttP5dJ
TcFwVvtIcuM8EZzo4qotOwWpgU+kW57PE959J458VZ4C/qNyFXS9eULiQ+Du
xpNTsOPVpb5fuXOE5bVcWenLU7AaXLzP2maW2HDyaHLfwykos/axjIFZQpBe
zfn+0RS4qmpkV+6ZJRxqRiTjE6dg5s/AIZHqGaKE18hQPGMKfl7lNP5GmiGO
br2Iy8qnoP2kZdyFnmliZ+XM2pvRKdDeIEJNSpoglP+aPBhQYIBVw+1lK50J
4lRgcwnLIQbcia7+SN84QSSTS435VBjwQvSaLWE7TuzZPbQseYoBy8fzXw0e
+kW8ND4m/lGfAekBzO3o60+Ca+e5rC23GfBOM2Ltd+hPQvDjwi0/Fwa07Rps
KUM/iZtvntxmuDNg9UWgg2bmCKGVEbszy4cButWNBTddfxCF17daJYQx7Zsu
ypfsHiJIrSHGjmkM4G5+tfFu/SDx4PqNfUcyGSAe+69TzWuQePFiamD5JQP0
KM+cvw5/J9SSlG4Y5jMg49RYyerrAWJVMOJ4aBnTH67DQdVXBwhskdP99x0D
2rtCDcP3DRCtiq5mVgQDlprMpoH1G3Hzv1n69ipmvFwMx3sveonXSTqhg40M
aHkatZOHs5cQ6VU9v9LMjF9asSjrdg9BsepcZGtjgLa213zjsW7ijbNCPcsn
pv0hh9vO758Jx9Xvxg69DBATzuNJO/2ZiFh/r1qmnwE6+XqlivldBEUhZ2Xo
GwPSWuOXL9z9RBQKGFRSh5j2HRJeKQIdhOzPyxEHxhjw7cYW2/332wknFsmj
Eb8YcKNxwgjPtBFeGpY8w+MMCAsqkB+obSWouxrO204xgP9HtIiDbCtxcNby
9VMGA4wCJ4TXHWohTleeOFw7zYBHK85fYLiR8Dgj/WFxhgFFJYeKLpc2ECZZ
yxVrs8z83Fp7eDf8IxGsGTuwOseA7aP31QjlOuKI8ZHwrgUGSD/XEv22tZYo
+m3B83qRAVqXuNf+DVYTkdTIyoA/DAjpTCxRCask9jeXuG9eZkDmQ7OoSxYf
iAhBUf+3TK49I3bDW+k9caDf5+WlFQaMbPql/pQTE/YnKIwlJrP7mWp4VpYT
4ulOOiGrDDj1Z4Tlnf1/RIfLl6ZtfxkQcMOhbGVvKeGmXn8lhMnvh5duHf5Q
QkS11fMtM/mfsb+kh10RwRi6OnbpHwMOt3F+L9tTSFx3PdL+lsm/w2kH5n3z
iKdp452b1xhg5wEpTl0vicBs8m8NJo9c1ds2R00n/o6lUgKZbHbe2vv23VQC
jUg4v2Zyie9094+MeEL9TEhvF5OzjfeUDutEEOQfqSYMJvs0T5uE//QgaNkG
86tMNkpknJ8YtQC5koT0NSZPyE8bHbUJguSTtg6LTPZV1WsW4ngE1ypa9YaZ
HGpDmxa2eAo7svu1apjcqLaBPbL3Ofj899r8CZN1L07H77ifCU1V7uHXmNx1
o48aJZcDf7F1J5XJ0qo/TIMZ+ZDa/Ebu+/+L9+PFdPWTr+HW/v9ehTK56Hz7
FHtiMVRp1gCdyS79p5U+Tr+BjTmHJj8w83fQhrgbcqoMWP4GFGsxefW3Qt2Z
pHcAWwTim5j1qPB4tZ1jpgJIlYFxx5j8rLaxtbr5PTyO4JzkYNYvgJNtPm3p
A8gL2hw3Y9bbWvc47z1yFTiobXubtcQAiZ5S81N3auBBnMLaPqZe2IXnAylp
tWC+V6PjBFNPv6z2v2RtrYPtfV/qLJh6y5lKn6lhnsNy4xv4vZl6DJcb3JOh
0whaRyM8PJh6dXATOBzk0QRoxHHdDaaeZVli/NXaWmCSPdpblqn33hC0xUql
HRQ+b7M/yOyXkM1RSn4D7cBjtnbmA7O/lAOGrFL8OkDDw/qGyigDor3uEb01
nWDzajx0+w8GnHFodNLX/gyOex9e5mf2758JgWe3f3+GjEeuycDs7/RrN1si
o7/AwvRWBYNuBqy35KY2f/4KKQ/uXjftYsBbfaNvp8x6gZAyP9bZxIAr7Vmc
1ix9UBd+8HZUAwN2aa8c9H/RB7uiy7eeqGfAzdNPY4jRftBRHg7xZc6jaIEZ
ctLbAdiVkr9gzZxnZ7Yrd+9SHQLenbu0Pj9nMOs8MC0aNQSUdc8DmlKY/oUG
b5YbGoJ9/7FcePOE6R97j7xu4DCspKyNWD9m+rfB+2FY/Q9o68tgkQlh+jdf
eYJVbxT+O/W63MmeAduK7C7ufj4KT1hnE9OvMf113O1InhsFuc2vjRqtGbCT
cSX5WOwY8L/d7jNrypxHo+zL3t2/4EdsD3uBFnPe9WjnLVpMQljxz3OTVAYM
3j8SmJQ7CQrXD6TkUBjwVFHyouryJBj8Eku1FGHAniiWzQ8ip+AdQbTn8zKA
Vb34kiBmwI9P2LF1MwOcLm4Nesb1G75WyryRGJwCg3pHxXrbWZiWvPftMvN8
lBe+lpoYOQvDKtu5BoKngNv1MueNN7MgsT8o0iBgCprJmoPcG+ZA9kNFDsV9
CtR8xMJNEuagqGa4+5zFFCgrffnBqJuHxZcdpJ/M85o37eijPZQ/sE1R6s++
Jub+MLMuaJf2H8j8xzc/WD0Jc1Dlxu32B84sGXonV0xCc/cZE66PfyCOtcFw
KW8SArYbibLeWILfgdZ7D0RNwpSHY8F88TJ4XT3q+Pb8JFTpZTR1nvwLMdGH
J6zbJ0DZh82/XnY9et1UWnG+cByOaR0rHD21Ht2Kzn45lzkOmnye3zdfXI+q
ds53hSaPg1kRcyb6r0c/r3TzpIeOQ/Bo1+qHjvWoaVXJ1OvKOHTrpLm8c2JB
TrzanbU84+BJOnY9r2QD0rAs/L7L7RcQHzx0Hx1iRWu9+/gGRMcgaYW+q3x8
C1pgV+PzchmB/f7whbp+OzIj8bidvjoE5f5BwmLm3EhCmaS6FjIAb892ylGK
d6HTWXK/agp7IMNv+vW3w3sRf8CKXLpmF7w/QJNWe8yLLATzVr1b2kDkUl3O
3VRepLL8l65X3gZ+IVb0t9m86JpMQvH+7DY4NpREk8S8qHfYvHNbUBt8iNkq
xTHGi65obUozgDaoWpwSbznEhwJ2fFrPXtgKdeUFJMN+PhSXY+1GQi3Qoq60
+6oYP/IyDt79+2gD8M/9pzkjzY8ei4i6WpMawOYpCvBS5kcnbOfJUxsbgGVW
fTZagx996e02O9L4ERSTTNrwLX70aL2hYuPFj/BkyjeMr4IfeRs8XHnjXQ+2
0Y2bmg0EkNBQCHtBRy1s7jNfkL8viOJ+nSx3Lq0C7wRu9YhIQaQmnRV06VkV
zBlWJo4nCCIGNWST8YMqGGwnqz7LEUQUEf7NwaZVzP3254NtHYKouWQxrIaF
OReL7Em/BIRQA44bKtOphO4H7prJr4WQg8KLn5eX34OOOjV5+Z0Q6ss5JBPz
4z3Usvb81q8RQh35++27W99Dke+Rx5xfhVDSbHhBUsZ7eOi69t39nxB6d5Ir
eVH/PTwr+HD4JVkYZc8JbVhfgsHwyem2gJvCiL+i7shlVgKGE+6MP3YSRsPz
EX0b+yvAMe7Vpmx3YZTifj65vbgCQqO3H2rzF0YCpYTCiHUF4OAvzwTihFEO
14rbeG05UF1snEqwMIpqkLxuEv0O/p29v3d0BwktCQk6n1ArgweaZQdW9pCQ
KGNiolq0DHjOTGhw8ZOQOlfkN7t1ZSB3UsdHnkJC3otbeqllpWBzmGfM5xAJ
TVy5WW4lUwpt4pllvJYklGOaovtO+C28WFdnqlFEQr1FdzIuCJaAxNGpjs2l
JPRMl1PkxvoSyHbfdaaqnIR2exjhlOFiKJwxUzxaS0KXrvbymWUXAx5e4pLp
JqEhKzPJk4eKoa9OCnOvE0EBYrTMx6ZFsCfyoUi3hghiEefd+qm0EBKaiuNi
dURQnc6q9P2UQhDY0st1Tl8E5eRIrjcMKgSyv8RKwyURlPmH/aqafiHIOr/v
KL8lgpyr1m+9NVMAZy/MBqQ+FkEku0DTrgMFEEQy/GkzJIIyame8/rufB72r
Bs0hoyJoiNLOu/tCHsh+MSh+NcnkZ5mDWZJMe7iB//SiCBpd7N8f0JALsqv6
Qm6coujWfI/Swo5c6O86bxikIIr8ywKrnme9AsVQvdq0IFHUdcztVxXLSwi9
qpdbGyqKatw0YkS6s+D7Mb1HY5GiKFXrnFhxQRaELula0pNE0Vr1Z0U38ywY
vKK7VpQvioK3XMlzqMyEMFUdpaqvouho1cjAWkQGDC9oZQxKkdEUi2COqm4a
NPcMJU7sJ6Po4RYTL5k0eIvdIxYUyMieo016ZGsahIWkuW9BZLRLsSdQpeEF
KAr+1ZA9T0YOxzDPptMvIPjkq2lfTzJiky/v/KD+HKRjthwiNZHRf1lnqhvt
UoHHPUVaqp2MbEylWA11UoHlkqKowmcyopbeZ7DJp8JncQtO9e9kpLqdlXto
JYW5J5X2O8yTUYkn36JtWAp0DNr4YwExJFASLWV4MxncDtQ2Xr4hhn6UnT7l
r50ERvq6Ajtvi6FV/ezOHaJJcNC9x77GVQyF697w6V1IhGXM4KL7iiExScp+
seRE8NLh0V2OFkNPfAyWPGcSwO/mtU8xpWJIUFNzhetFPITmcPTXb6Sgs5ee
0jgOPAa7tkfSXuwUFKG40fgFx2PQmhe6u5+LgvrPCP++NRILXEflhR/vpaDk
bZkOWUmxENloYmFFpaCtuqoa/hyxEDueM/JPm4I+kLyy8mZiIFVShyGXSEEK
H/65mv6IgvMTr/M1UyhoQ4Hoi9uVUcCat9fROo2CDvUuCxCpUWAn/20uNo+C
Xt1iH5u7FAWKcGN5qZKCnEb97Tt7IuHj+dBNeIKC6HlFrhLfImDmbt0+TRVx
lMTiUV68KxxeHKP1WR0XR0Ud0awKfWFguCniqddpcWRy+n0eV3oYvAsxJOXp
iaMLoRkHW5TD4N6jEQr3VXEUbqox6moRCvuyN8p+eSiOTgjG7FqrDoETXei0
1XdxxJrb9+DNxyCY/BxnTRkRR93ZEosJKUEQ+3Xab/SXOOrq25eMXYJgtDel
3G5OHA3dZjvVJhoED4bWyTlvlkCjF4Su//O5B+3TWOAeXQK1ONXNHVELBDMO
1dkMdwlk+1K/fMN6f2DfGr/9urcE8vvqx/qg3w8KuX7Taf4S6Hq7tOKZd36w
kTvVJj9UAnFs27zrppsfZPCs73+bLIGoLuk2Z2Z9YUrsfV19tQQyuPfWZdOs
D3gi1afjOyRR8J/nEvsOeEOZhkz9oz2SSP5qoprAshf8MRCYg32SaFA+/pZ7
pRfctl9SjyFLogJ646+jhl5wPaFg/oiSJPrgVJUz5ucJRnMkrXATSSSk/YXf
7NcdkM9kWd2fIYlO3RFe9l1wBcfX05SebEnUn/cg92u9K+RX9OsG5ksileLm
GM8nriD1qSzja6kker6WpfLspCuIsNw+59coibaejeczj3eB7SZDLzt+M/1l
fyitfdYZxrdVXXA5QkWPFOuv8Yw7wnLpg/lAVSoS1wadv5WOsMXqXOSjU1SU
Tb0dL/XEEahvB+uKdKhow/zxWxY6jmBjtl551oqKRixNKLSyWzCSr7LHIYyK
cjQq5OoTHGDBmLXwbhQVqbVP/ON0c4BNm5q1Hj6mor9FtF9R+g5AvmAamJtK
Re5j+daWOxzAcr3n3EQx8/vAjaO7eW7CgG5p+7V+KqKsRE3T+OxhesXnhvsQ
FY3WPheM7rWDtbTTW4JHqSgl7fARh2Q7EFz+rJo5Q0XtnLp8oRQ7MH22kD/C
KoU4BXVvyh+yhe4ZuYeWMlKoOsowNNHlGnRG52qY+kihvB2vV5JUrkC35/Xj
ewKlUM296hfPdlyBAWvK4ZZgKfT2zNmnK8PWMKH0lKoaLYVSZxRN00KtYWNv
GLtYuhRiH3ei13+zAnmyfe1EgxR6flU9qS/GEg5tlSTSWqVQiv7gja92loAW
hksufZJC/4SkdYVPWoJmnUl6a78UOndtSCtswQKs7DQDi35LoawavvAuEwuI
KZI67rmXxsy3Xcp/iuaQ8OTnIQV+GtovPT8uvdMcUu49l50SpiHeNcp/Pxhm
8Mpon4gZlYbuqe/k5X9pBlWrW9afOEpDhnwuV5NJZjB34lcFhyUNKdSeElA5
eAnOfco8lJhLQxMl/N2yd43h+ia7poHXNFSn9W5DHzIGPwUZM0opk01Mg7g2
GEPho+LAgkoaejh3z31D8AXYbvChtfYLDaUXFzjzJBpBU1fPlTkWOhp91Ni5
7bMBDLMmLx1koyMBjT+73qQYwKqiRejdrXR0/dqrsU/XDYD2eLRgCw8dSdMz
413WGUCI4fyqMJ2OJqLf2lEP6IPaF65oLUM6Yk251BGdcQ4usbWLRZvQkRCb
24HTbufARfnR2y/mdGS4dOeftfo5SI/b129px/w/m3pk94QebLwgIXnHl47y
J1JTIpT14P1XVSI9m45unbpi6DWoA1/YN+lN5NORd3Je09VSHZg+WDd8oISO
6PZD7HkROiCccHZLOaYjnmNJ3TpIB7yNL+p3fKIjUeL9qwQ1bYgNERzl6aGj
f/VrXdwrZyGn7PudSwN0dHrH4wnlvLPQy2eTMvaLjg79ND8czHsW5s5IyclM
01Gqb112fosWcHpMVTvN0xF3zoizzz0tIGcXGJUt01H0sbzDi0e14H/v+9H/
3ff/D/smqkc=
      "]]}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJw113k0Vd37AHDTNRdJmr1kKMNFUSmvnsNbiYiUIrOopAyJigwlGkwhZUoy
ROqtZCiSfRLJHGUm03XDde+R3KIM3/2u9fv9cdZZn7XXOuvZez/7efaRd/Gy
dOPj4eGZx89/b72e86zFRTo5zp4zSM9gomc/sinjP3RyUxorSD+JiTaIt/1I
+EknhfWEMrtimUhUf+vcRjadPPDZPlc4hIm607niFt10cn9z/QcDeya66OKv
kVFEJ9tnle3kVzNR0dgFH8KdTp5dc1cqPWYEacwE/AptVSd7DrkkfPJmoFyn
1Y05kWqk2M6/B271D6JIsbwc32OqZMiKyQyHjK9I92DRql2aKmTQ5TPqadZd
yGPI2urp+EZyzKYzqt/pMxIuYd95/0aZPBonEDy4ux6pta1JD/ZVIpfS+kM1
1r9FK2OrDbZtViTHWFcZp1kxyDPSafSugAIZKZreMFJaCPs/J1inVsiTArFx
iUYxHyDB/lX9vhg58lBk96T8mhYwohW8HKmRJRVZYeY9+e1wnqngfIVYR+5+
u+SMQ1Av+JteWOHFWE2Kt08aN7UNwDXjP3bZVivJx+1yfvkBw6B4OaCrtVWa
XFnoLWcyNAIp2t22hwKlyDCPCLEg6htsF2gb2jYkQVYofTXw5YwBI79ktYei
GHn+VRzfRCwLtpeobfwqIUbWxntpX7rLglvvMnQO/hElM2muTN77LNDqvGW+
vVWUXCls0UTLZ0EQzTGcP1SULB5OutHxngUyTkLfU3tEyIBjAeUPf7LAaIX1
x4Z4YTKEmT9daz0B+SGz/hq8gmTdQGrhFgk25LvKHOBl00ikvNs1RJoNj421
lb500sgBhahrNavZkLf87OdLL2hkk+ITSRNFNuTmDmhWO9LIF3fWEat2sCG7
uWbUtkKAlMgf3tPpzIYHf909djOQn4xuuX988Dkb0gUKN9ud5Ce1bEUDU4qw
x5qFNQ/xk60M7uiBUuxCkVdfVPnJ14lHqh9XsiHNKGi5XDcfqVOFPDa2sSHF
y7WhRJePtEzJbjSYZcMdtGUXg8tDtqzm4eHoc6AsLzhn2TAP6XDWLrfWgAMD
cXXi8ImHdNr1Wv3hHg7QXV16kvN5yHPRVs3/mHHgg3D8RXNHHvIEN8ntqB0H
prKG3PziFhF3/tnxV5c4UP7R0XzD9nlUr7dH82gBB4RTf60PWjaPKrr+uf9v
EQeszsZOdLDmkH7s7a7FVxzgLEM3ozPmkILJ7jf3KjggbytbNSMyh6zswmUS
6zkQwe7Vbe79jQTWfq6tZnDAQurYhstXZ9CLsnn1ueUULOUonn5iM4PGW9Z/
F1pJQX0tVdCtNYOsoixeLV1Dwd4r4QY7+n8hHdl5oyVyFOhNvnD6qfcL3RYR
PdukRoFyk9ADby4X5Wha2DsZUsB43Mp80MBFpfziouv2UPAw/L5GcxYX6fif
L2g1omCdvnYF3ZKLZAeiOVpmFCx76tA3/mwaMZKGVhQcpWDuZvFat1M/UHvw
6zfIg4Iyt9Djd+AH8uQrG1D1pOCCwf4n72V+oA1Bn2Zve1MwNTOwc0P1FDqB
Xgoe8KPg20nxY/3yU+hkYUloZDAFrXuOJ9n0TKIG5qus5bcpMOffoJqWNoHc
nVWII88pcK33/e1qMYEyNaIkeQsoCEiorqMLTKAnfk4+uS8pyFZw90AeLLTs
rs8ws5iC3/+8eDq0cxyh8ZGrWuUUPLqGq0XXNzRq9CCYU0NBuentxe9R31Cc
PdKzrKWgRXqouYz4hlpSxsIL6vD8ssO9TfOYyCY4iXRppOBgdUOB14UR9MBD
uS62FY/TbHVKVgwj1YuGgtK9FEg1PRUIqR1CawqtgrX6KNh4d+GLUdAQOq3+
JHTfVwoslTP9uhiDaGtxqILHAAW5e8dK5goHkI10UV8AA8ezVO969ckBlN35
Mst3BK9He9TRmLUDSKapfuokk4LZRqdJEOxHnopVav+M4vkupc5FZPcihZO6
tCoWBc3p8ctXifciXX1PyeQJPH+NbUWPfXvQmq0VWe5svJ7mQdwGw240e1zX
ZZ6Dx2PFLi4f7EBtpZF61HcKlOSer8rZ14Ecd1y7ljFFgcULy9JtL9rRltaK
w2Y/KMj5lPzbJqQNKWum3EqexuPLNgVlrP+MXpIexnM/Kej3FPXQutGKsnzv
L7n+iwLPhglrcqoFuauwry2ZoSD6eoHOQM0nFNyeHrVkFufXSMIG7y2fUKLd
13XXsa3DJ+R4djajVfy/HOewE//4dQKjAVmr+y10/aagqGRnkWNpPWJ9Lbcx
/IPXx2cxNiSmDiUvyzz5CFty9IYR0v2IioskUp3nKNDIMlPoX1KDdv8MJl9j
mzlILS4MVaOdx2ySxOYpuPUltWRX9HuUd1PBMRs7L9Yp3sGlEn2oWTAbx64x
UfIM3v4OpS5ZR6ktUMCkjRuni5NoIeKcoTu2yFX7/Zffv0XzP2aNMrH3zjD5
ys++QYN5+Yvt2Nc8vcv+rCxFKjFB7kKLFLxjzProVZagOLkz4drYC8fCVALP
FCEJH+9Dtth6LeKDZTIvUZti+Odg7O8x6pu5V54jS78cWjr2mUDION+ej7S1
GsdeYzNPWkpMqz5CTq9mQpqxnQ67BfuGPER8vEofB7FLrkx2j+Qmo6v3zCsp
7CfHZEoZFrdReLK/5yx2aNOkXcy3QDRZmtSwgG2dSh2eGHWBcSjuXcSe0Jm0
1j91HToFG9LnsK8YWDb9JZYI9sF9UlzsqFPqk3Iu6eCQP647ht1gxC8S15sF
ixu4kl3YB20nk5fdyIMs6cWUKux2zz7VeO1/YQLEOp5gaxiM2N+kXkDZ7Lrq
mP/mW2f7yHhPIair6bqdxS463MoRSS2GBwedXxlh+3/dt71u8hU8Db7/Zj32
jlMo5NbeMthkPeVD4fWe+771o0laOSitOtFdjl0R+FRSbKoCJqMWp8OxM2sa
PlU3vQO9wneE2H/7Iy7MzZmthHPM3z41eL/dDv6zOkKxCvpaAqxCsDf1lDrv
DfgAUtr+2kM4f0TkuOHKOTWws0FQIxp73FUrX/DTR7ByFe7Rxv6X82jqg1I9
bNsWa3oe52OM9pBMrkUDdOloLFuK7X1xvd71wEZQKve5kYXzeQvfnTCjlmbQ
N5WLfYfzvfcWIeq6qxVUNFsWavH5uSUUv/3qQCu83OYVuxVb99qwa8bVz1Ai
eFEyDZ+3hKAI1PvhC9jxqTNsuBSYeDectzLvgHWHZ74k4fM7M7E+0/d7B9yl
3rF68Pl+5O7VHJfQCY/Vmn+sxuY9LqXa1NEFa1S0OiMoCl5bWffvdeqFFxEp
XAVcP060PhZ34+uD010iKbtwfZE2/7MjLLsPKoY/ah0ep8BrX/odNPoVVBzr
t3rjepSwfkox7fUALLmskOmB65mJpG63tMEwvDFt2OTdg/tL08CkQvww6Lnb
0XZ24/iibgppDw9D1B92+0Injk+kR+dgOAMqv/XZXGrH8fEHx0bXjoCwXRJj
cwuOj/t+t6DlKHS8PahfUE2BRNEZ2xVZo3BUfF2YahWO99yKc4rTo1A96fYu
vZKC5dSJB4Z3x0Bo1HfTRYTr0ajI7+DucZhdE57LeY3rXY/5818ubDCUDsoe
z6dg6Mbf4WnP2EBjJ27WfkxB+jYVW4Pf+N7jou58IZcCmXg+ocg4Drhdrcmb
yqJA0LjYQZak4O6X6IGiNArO2y65nrn0O2jVLlE7FU3Bkdpz22o9fsCpvb8k
o3B/1JFzf5ga9wOst//0MnbH/eKCo7jnqx+wK88wgO8kBU2KpkNS/NNQoCAY
f9KFAqNQpRi7lGmgPdzYMW+N93975wj1kQvXrpvw2uJ+vTpHP1FGeQYma+ag
G/f/iCme69LmM/B3/SavFnw/mIaqi1IXZ+BSkVbie2n8/W4Tu6V1M7DiVnNN
0lKc/5LWCoKes8Aje6RNkI8CTuC5Am7xb5i/mCww9Y0DVZa5jV/2zMNgt/oF
4Rcc2JJxGrV6zoNzKyvC8SkHHrDpBZ/uzUO6Fzu9II8DATeK7jSMzQOZvHZ8
70MOaFa8s62KXsBx0ha2xnMgRaV3rLBtEWbCKnOUfTmgGyocVruFlzjj4H3m
yGYOGJoZvhzdy0v8/bW9UoHOAdM1lweFbHkJjw8Oxyc2ccCpCNfYMF7CJtgv
3EuOAzdH2+cqP/MSm3yPOelLcKDbIse//DwfYRVmLDzDYsNlecPTz0v4iSaW
OS/nIRtQZeDBxJ2CROSOqnkaDxsyIyMmbcwEibYjPt1CfyYg4nBcrKyTIEG3
Sq+h/ZwAU+ajxtxwQWLzmFY7lzUB3SKtxmWfBIlli64djzom4KfFJsP+E0KE
DFKnmT2bAI3+L1tUEoQJvvVa/M/x/TntD136LUuUePlbfnzqIb6Ph0GnKq8k
YXFPpjNlaAzehl2XU3KWIlJjlZ330kbh9YEv2srF0kTfbiUN7/kRyL06Wdiv
t5I4n26//+CdYXi3WV3D6N5qwsmztWt64yA0G29fcVJpHeHnd0DMrKwXhPqc
f+rckCUiPcs/7CE64Oj9fS3XvOQIvSNbGqeftUA2z0f7/UXyRL2VfoPp9Ae4
Ln/026nhDYRQOWdLpWUJMH6a5Q6pKRJXwutMy+Ij0cXNNQ2OnkqElapN8nBJ
OXqoYkFppyoTckF2NiUCdWh3O7HPdXAjsbMrovS4zmd0mTBIZy1TIS5IyHbw
tXQilkSVjf/fqsR8ssr+f9d+RV8Snu23D1Uj7j9upi/UDKJDbXk7U5+pE/JH
BfxWqDHQadqZxoFCdYIa/Gut/VYGurpV00m5FI8X25g8BAZ6mVgcXvBenRhN
kNX/6zADSR6p/FTTqU40bK43/H6ZgRrbe05M89GJstF7RhONDGTUuTTB7Cid
8DeVk91ydgQ5CLcqJdjRieX9lbeE8D3OXzfxdacznRAYdjDuCh1Bj5LWfj1+
hk6skP9Y7HNnBAnYbFIJuEIncle7rfN/M4LedRmgR0/ohO6o+JN6YSbqFKFZ
TrygE15L8z3dpJhocsdHxuYSOrFfNt39z1omkks5IPqWpBNKfTMLazWZKPiY
rdXnNjpRfCn9kqYVE929JTu6qodOTAtK1+U7MNG/ZYMBDgN0Ymhb4+4Np5io
d82pjLFxOnHHLPolLZCJpk3UtDUn6cRlg6OZPteYSDyQU32eSye6p8aLu6KZ
SPFJgXXZbzqxO/YJS/8eE/3f/zvx///v/wOBZmSr
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None},
  PlotRange->{{-15, 15}, {0., 0.9999998334876125}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.760279168538741*^9, 3.7602791952948847`*^9}, 
   3.760279256291121*^9, {3.760281752713851*^9, 3.760281818941874*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Show", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{"-", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"x", "^", "2"}], "/", 
         RowBox[{"(", 
          RowBox[{"2", "*", 
           RowBox[{"6.0", "^", "2"}]}], ")"}]}], ")"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"-", "15"}], ",", "15"}], "}"}]}], "]"}], ",", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"Cos", "[", 
      RowBox[{
       RowBox[{"\[Pi]", "/", "2"}], "*", 
       RowBox[{"x", "/", "15.0"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", 
       RowBox[{"-", "15"}], ",", "15"}], "}"}]}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.760279192892232*^9, 3.760279193546547*^9}, {
  3.760279259983398*^9, 3.760279263872059*^9}, {3.7602826326687727`*^9, 
  3.760282660910832*^9}}],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJw12Hk4VV3bAHDzHJEklRehOOdQ8Ug8ubcSmTL0VEoh0RylkjJGIkKmnkJm
DmUejkj2VmQeEjLFcczDOducKd79Xtf3/bWv37X+ue+17nWvtZesvbOlIwcb
G5sIOxvb/77avQ+mNjcpmLawgdaYVzrkzKfihmsU7LZ3wimu6HSQE+qYj1yi
YD2JgUlqH9JB4Ohf6/uYFKzk8XnVup/p0BO/KGTeQ8FGyiNkvx+kgpu9q0pi
EQUz05vdbTNBhaKJR/eQGxTsmP2O2rKrmaCy/OS3TxsZkz577+ByXBa03vse
5NhExrjm9h/TK8sCl6l90ka1ZOzko++SmT+zgNbfrretgoz1G2VbfhbLhqPV
KhFpmWTshORkpGhQNphEMMj1PmSsTz6P5OqZAzfJRvbbVMnYRz5zlwbtPKDa
7WxKCyZhrptTkkdsCsAy1p1r8DkJ4+uSTmJzLoCNjl/au31J2L2HD8LbvQvg
rHHy+wg3EuZj8uJLUlIBcGmQAr0dSdinFUPhtpECsBM8evw8kLBilnBQ2N1C
2EmzKxWaV8bc3yUzqGFFECyYkXb/gjLmR32Iv12kgaZFkaSOqhKGPyoWnjMp
h1sMqzNZk/uwuKCFsrBXlcBHY0Z9/aSIDZ2GBB/9aiB1SMV73VfAIvXX3NJH
62BHWLWuxkF5LGLr7DUPsRZwCrYbf821F7Ncz4UqrTYw/hFpFVshi2VHvbpx
z6UDIi+VNJwMlcFG0sUGvIq7wIA7v2CkRhrz6x8rtJfugwejey8/RXZjp2Mm
6ijsdHA1ebTdeXgn1ikX8rZscBCeGa5dTD2zA8uck55zmhsCeY8n3W1t4tj7
JHebS0sjEKPWY33aXQw7t0F34akfg8NcHQwNhgim+Wl5Tuv9BAy/p+28JS+I
FYYby6WcmILDNNK+fhFBbO+fCxnaJlMQVJmobrEmgK20JFa1Wk7Bga4gs8Nt
Atg79FfBjO0UeHLb+nP6CGAh9Z8FeZ9MgYQd72xsLz8meHdoICh7Cgy2W9U2
RvBh3VLNP7tEp+G994qrCjsP1nP9XAy9mbCDxCl2Jjd2bcmw+Fb7NGQaqim0
d3FjNg6mtXPd05Cx7c6Px3ncmJlMYsPC8DRQqXTValtuzFzi6VrN6jSkttSM
W1dwYWOO7OkZCkxI+M/rCy/cObH6EsscxI0J8VyFBy9e48S0bVxSTngSnmjh
Uz3NiU3k5fyt70u4kL+kXZkTi7r0yELrJRPiDDy3yfRwYBWmLyqYCUyIcXZo
pGlyYGcp3N7e35gQhR7SGV5kw9wWsK9Ht7KgLMMrTXSIDQt01pO9Ks4Ceni9
ELSyYYIHtO8FSbKA4mDf+/Y9G7ZnRnXwmwwLvvFFuJnZsmFyr8yahQ+yYC6F
4fgwfBNVlo427jZnQXmtrZnc4T+o9ui7WJMQFvDF/t7jKfoHvbhvvAd/xYIz
d8Kmf06to1d5jnWERrGAJYq+CElcR18NfVhAY1kgay1dtcy/jmrxOOezMlnw
nNmn2dK3igaTT1d6VLPAXOyCnIfvMrrH812Y+QoLhFnyNz+cX0bLHo6mvV9n
QUMdnt9zYBmdXX3utbnJAv2n/rpHBn6jt1oqnN/y4KA9k2e3pE24QlU2dBsO
is28CXcXF9HDb8SPPSbjMJzZNprQuIi+z/DP9VLFIcn/nUpLyiLqaadf5XUI
h91H1Soolouo3dDpX06aOIhm2fyazFlAr5n5Jwnr4bD+oniX4/V5tDMlWMr4
Ag5ljj5XomAe9R3bdXz1Ig6PdI0/fJWYR78ZFqym2OIwt0zXkqueQxtJuqJj
DjiMXRO6MCA7h5ZnFBWRnHFoO3HlzfneGdREuCw12xcHM0455bi4aXR0rU0n
OA0Hh4b7qw7m0yjXgLzQdSoOTyKr6ylc0+i9YKtlyMQhde+NW+itKbQ4dFZp
MAuH1eN5WQytSbSKDXrGC3FIf0Z0i+4x9DXVtlSoEodyk1ebsy/HUCv6jGjs
Fxy+izNaypAxVMErzkO+isgv1f+uScYo+nPNzn9/DQ4W1Y35zo9G0Cm9OfPF
RmKc21qdtn0IDTxwWSvpJw5izVlc3nUMdFF92J7ehcO+1xvtBp4MVCSIO1Kq
BwdLxeSH3cODqBDSwevThwNVf4K2XkhH1bcKqIsMEvEIawdUX6OjioGtLxQZ
xHx0vjwXuouOeu7vZGgO4bDSZDcDPAOoe1Fs3KkRIl9h3OV5ah+qN+8hqzOB
Q0t8xDZJoT407ORbP8okkb+KRlHm/V7U4gppZMcUMZ9mnouNx3rQua7l1F/T
xHiYoNu2wZ9oDKVIkDKDg4JMrmTayZ/oUvLEpQXC5nmWpRp5nWjpxuUs2iwO
aa1vV897d6Anm3L+Js8T46L7PRP3/EDzGHqyvYs4DDgJ3DoQ2IYeDdXTfLSE
g1PjtBU29x011Zl/Ivwbh5CAfHV6TStaRjb1VVkm6mskUu7uoVaUnPZQv4Sw
lf+0DJtWC5r12uAv7RUcotcedsFwIxr8YDVcfRWHIppWkW1pA2oif3Mzk3Db
vc0w79B6VLM98bXUGg5bxwMNUM1a9Eb/G10mYZUU070DW2rQ8qDVf8zWcTC1
EdvcYFSjMb1XwrMIB7XH0nRCvqIRmunPrP7gkBFmF2Fj/wWdVDqoTSVcY6Tg
5HW4EvUL2ic1S3iUe9IwXghD8YZrshobOPD7XjL2+PoZLYloM3clrL88ylF+
5xPqQjPKyCf8zOlu2dqOUrRPt3jfOOHK4ZV72l9o6JTLSvPOTRw2Lvgpud8u
QullbPH6hLW/Cw2WSRSgFypoUU6EZ0PJBxef5qJXzvAXhhO+7Q6JDzrfoz7B
48u5hEevWYosKKejnxP0r9YRtvvH0eu+dxJK45Jc+0WY9nSmZ4T6Fk20O13C
JPzhgkTpsPkr1CJkNmaZsE/zzMXQMXdU/w0zbYOwVSz+z/S4PaR80evaJDyt
PmN19HoAaD1dOrhO+KmuZfN/BKMh0IOrcJ7wy+vkGRn7eCh55WI9SrjRgJM/
vC8F/m3SJ7UTtrCeeSsamAGxri/kPhPudPqlHKGWDe0cJ3STCKvojlx6gefB
xO7nAT7/y7feOt3wRCEIplgvXiBc9E8biz+2GFjpnUEHCLv2nzxcP1MCtDsC
J9kJH7mOegfplwHLWYjSRMzv+uxftUZx5fDw/MKRKMIV7llbBecqwIm/z+ks
4eSaxtbq5koQObLHopFYz2dCfItpK18g3LV4w5uwo8Xxnc/lq4B7qb5dhfD+
3tLL+k++gdRVzjVPol74ZRb9FdNqgP+Dq7E04UmHA+95WmvBpIe3upSot2xW
+tw3hQbgafhbnUHUZ6gaQ4Jq3ggP7x9TdiZ8122PdoB7E9B/KBn9Jur7EEeU
n8H3FuDMfcy3QtR/XxAi4KDTBiM6cf5fiP0TxBtx2JfeBm43boXsIqz5bMgh
0fcHvOdzHHUi9luk53O071s74KnGIVwLOBjdbXxwxuwnyM19Kp0i9u/y9J7k
+7M/YWpvwpkdhNNvOLeER3ZBjPfQiaM4DuxXxJSbf3bD9qcOCm5MHD6esRrQ
t+uDTY0vzEyin1xtyxRy5PgF81LiWhnjOIibrR3xS/0FVAW0P3EMB+eT8VHo
eD90D5xRe0r0o8g9c/JxH+mQY5u8c4NOxLNVs0dcdwhsJp0dJzqJ86WZPrM3
YggeRVd5h3QQ8b18was2NATa/snrpHYiPv5edQv/YbB9x5Kx+k7Ex+kVFlI3
Akr5u5XuNBDxLX7V47EcBxl/8nRQBQ4iRbett6eMQ/rlnpCOciJel+0u8gvj
cHvzSILUJxy24VcTjr2egEk/VDyqhOhH4/yrXj2T8JeJjaNeHtHves1yf9sz
YaPc6ElsIg6MwL/943KYsPZYpicvHod4DSVr3VUm6P4d5FwZh4NEBAdvcDgL
SOdyddrf4MBjWGwjjeGQp9wagL3C4YH1loBk4VlQ/Mbztsgbh7N1Lhp1t+Zh
sT2nSpQ4H9VlbiTFhs/DFaE95WHniPPika2QU8k83OZl8POfwaFZ3oQhxrkA
AlIS2UwzHAx8FEIvxixAyp29at4niPU/3DWC1y7CoIK7SRVxXu9MOxotobgM
ktlnowbYcHg+xxYgbrYMpl6BtwL/sGABqtzE3JZhm+/vZdIqC5p7jC4K1y9D
G7Py6JV5FjzbarWXx2kFDlltTY4YIe4n7i75i8WrkPNvhOtSLQuqLKlN7Sf+
QHvdHH0gmAWHEm+ibU5/QD3SXHUogAUJTEp+679/QPl0pMugHwueBBZFNU78
AfPPBvRmdxaoVlRaV4VsQHWQa9SdWyyIUeqbKOzYBNukYyfLDVmg6cPnV3eI
HZF9KRafw8GCY6bHCsb12ZGsSCE75Q0mmEh5DPJasyPCDoHBSStMsCsieqwf
O/JMlbLfd4YJL8Y717/8YEei7hd/FOpnQo95mmv5Aw7EZXeFruFHJnjIHruZ
S+NEvFpZhXdvMAH94m4RrcWDOBrdNuVGpyFujSL+eUoAId1LHWs4R9yX/aBL
mX0rMpVsYtkSPwGf/QJkFC6LIV3N+X7rv8bg46l2NcVicWTp0N3q8S2jQPWd
KRzQ3oEsBVlOIrzDUHmQrGLw706kROvlbNN/GNBieHj7NYXdiMFauIGbBB14
f11eUg+URvbE2XOc9eqDc+9Ofn/mLIN4P362RtnSDalstZeMi2SRSqnE9k8h
HRAge27s+pAcst9oi5pIcBsML5lSGSR5RCp4zaJEqQXcDtY02jopIPNH5PXv
rNZBkpI5rhariOS+y9bJLKoGvU7kpMPgPuTNu4nVgg+V4IHoxk+JKiFPRtb1
1AfKYUqk6rzr38qI6eXvurkPSmC1NHjRX1cZSW6vMJawLQEBh9Ph0frKiKSK
hniIYQkof2TUFpkrI2e8zaozpUvguh275ryDMqLTHT0WX0eD0Twdibshyoh1
i62sixwN6BalbTf6lZG5EZGi4IEiaI/MMb7kQ0LmbI52sT0vgB6Pm8cl/EmI
V+MUv+XDAqA7Kmq3vCAh4dlfr+c6FMD04Xhl3UgSIun07nXoceJ/sS+EXyGd
hDyaTBHpZy8Adfk7NdMNJOTGsJF1uU8+RBWRjnvsICMb22jOpgF5cLojQys2
h4wsnB/TXridDTe5bzfRC8lI3oG9n3adygbfv1TtFEvJSHQXJ+uSajYURBf7
538lI22X+FQOzWXB1rNfWmu6yMiKTL2l5OMsaOrsvbrAQUEiLU+JNQV/AIMu
4UjTcxSkLezm1LWqTLDha1OIvEhBtGM6c79lZIKrZvTHrssUZMHTSlEnJBPS
3+zqv3KbgigYbJl0PZsJXOf3Kz15SkHyHo+JCE9mQGW3Lpr+gYIkcAeuV23P
gC5+bsvpPApi2e/xkLpGhZkjtcMHaRTkq1ZSStIgFWRiTgl8xijIqEUOvS+b
Cl4XrM/86KAggT2i++4YUOF1kPS4ZC8FMfBKiRBUoUJ22eATGzoFucOmrFEp
ToU+qeuJE5MUpNvn0+g9RjosGJHUVGcoyKew5Pbbdekg5M6qfrBIQVRzNMq9
8tJB/kO+VdkqBXm5Hw+h/psO//e+gfz/+8Z/AcA44Ug=
      "]]}}, {{}, {}, 
    {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
     1.], LineBox[CompressedData["
1:eJw12Hk0Vd3/B3CSWYZCIyGUuCSZUn0OegypDNUTkSmaEyk85iFExYOoJyXJ
UJJMqUT7ICGzTF2Ea8xwzzXcEsp3t9bv99dZr7XXOmvvffZ+fz7ryDpftnJd
wcHBwcXJwfHnqdd9dWJ5mUYm3jNqHNT02Jc7m06ZLtLI3ccU26QSzEFOqH02
4TuNHBAv4OmKdwCBvZpLW6doZMmcfWV+/GWgp7CFLOg0ckORo1pcfBD4OHup
phbRyFjPI5vM4mOh6Ju3B3GORhYrBE5bxD0C1XnfH8GtKmTd9KtQ79svIctx
fUPGTWWSO//C65wzJNwUfJrheWI7uZZ1PpI8UAM6lkXr9qkpkcMz53rKmU1w
gWF9LGd8K8nOYIzQBdqAr3jqTuU7RfJjdgnN9UMnKLdvSAn0VCCZFhyOZoXd
sDa2Sl9LXZ6cvfwGqtz7wO2m41jSyi1kXdmIjd6ZATD7nGCd/F6WlHLgdbXe
OggJJ1/XmcTIkBcK2j2Ei4fAmDu/YLhamrSTtlfmUBqBqyNbnEKITWRpQNX4
rSej4HXQW+Ly0Hry0ku6weDkGFw3XbRLP7aWNNDY4WCtNQ7y/r5fWlvFybOq
nYcj7Sbgvgbd9ojfapJmd3tb+pFJ0F7ZztBiiJCVfQsJbcQUtEbnFUKTCLm4
xkpAaP8UXBS7dd2kVIRsyRor0DeegjRpQ8UTSSJkgS///axDUyCkm38u4IAI
+f5rUsnJE1PAuHSbVVkgTF6qUFzv6TkFMZ1//bYIW0U2HlOaI9KnYCi7eP0F
eUHyZa0vac/BBO1i5a1fRQTJFUlthB0XE6LLU3dZLgqQtV9oicd5mLCjK9pc
u1WAtF7ifGssxIQAbodwrmABsmRSbXTVOiZIOvJOJ3fzk1/KZHLV1ZhgLGFd
Ux/PR3K8j7vSbsuE7KCfXqqcPKRUOyoNzcV2kTzMOcVNCk6KmYzlMeGZqYZC
Wxc3KXzR+qVZIROerrn0+Z88bjJNa52Y0BsmZGX1q1U5cJMSULHJs5wJ6U3V
Y7bvV5J1vDo76J+Z8Ghz0okoPy5yKcFYWfAHE1JWFqrbneEiH6lcHFb+if2t
iU/tCBf5b5Wv14FF7EL+123buciO0vebQpaZ8MA4YI0MfQXZFVd9rY2XgvuX
XeqLdVaQUTLRumvWUXAH7dw3xOYgT9xXWcXSoqDkaWCG2CAH+fv6/pvPdSjo
j/skBM0cZFOYorzrbgpoLs7d/2VzkDypvRmteyn4yBfvY+7AQdrN7hhJ2k/B
zBOG67W4ZRRYH2f4wZKC0hoHczntX0g01YfMP08BX/IPqQCxX4hnUer0josU
HLsUO9k5sYTcKy5ueXGJAqYYirqduoR4ieyhVHcKZG2lP8zzL6FKcYUgDy8K
IqZ6dJp6FtA5L3+O1yEUWKw+IecfOo/s9/Q8bE2iQJgpf/65zTyaTRPiV7tH
QV0tlU/fMY+W+lZpRP1HgVFIuL5u3w/EqS1TqfWAAj1WnuN3vR9Ivone7PWY
AsVG3kfubDaKSWHcjn5OwdCz1pFH9WwUoD9Cr8ih4HH4Q9WmJ2x0v8lgeP4F
BZv2arynWbFR6d+56vZ5FIjl2PeO584hu3dTmateUbAU9Wqj69lZ9Ls+mrVc
hvfXNfjUHZhF5ZHr+MURBd76Zs8rJWfRwdt5vQok3r/5/t1yVTNIyye/xKCC
gtEzQif6ZGfQz85efaePFLT+deqeTTcLRcynXlVvpMCcS277gweTKN/Nakaq
hwKXOs8FF4tJJMLWXOrG9k2o+kRbOYmkZl4I3OulIH3LuQvowgQKT1fRFOij
YMEwL4exexyJyCigjgEKMq/jNPoyipLKLjwUHcXf7+C/y9O3RtHUlMNELnaL
OKOphBhFhzT37DUbw+tLD3c/+HQErTGMmAz4RoFlVX3+Ze9h5Ek/7lM3gce5
bXcVSwwi72LxvbwsClY35qwMqmUgoySiKhx7a9LvNuMABqqpqbZYMU2BlWLa
tS9DA2jKuNaDjZ1l9K14qbAfrY2qplfO4vkI60VWnelH9ouL/jvn8H503Doe
s7Ef7dWzlHuE/bPBkQU8fejubkOvK2y8XmHqSkR6Dwr4EGvA+YOCppT4NeuE
etALK/FfDtgLqlpFzzy70eE1kSWl2ObmAex6Azpia8vsc5/H47GCPmsGOlGD
ReQZ9JMCBZmX6zJMOtGXDoaO8AI+f3lWb7XyOhDrK+cqW+yM5v8WbILa0ZCX
dxnzz7jYtoBUqc/IC1Xs+71IQZ+bwIUdN1qRe/05pt4SBW71k9bkTAuaaAgu
8sa+HZm/q7+6GW0IZoV9w940nCDnvrMZdTkb3N78iwLr8EkZjt1NSCnle84R
7MTFa10wVI/uffhHrRi7qHh3kcPbOsSsM4gfwm71WI4NivmETF2cBcV+UyA6
dsMY6dSgqIr7hAu26pNDW/pWVaPumNylaOxD9quXfzOqkOhP4aaX2NFtycX7
blcioe85xbPYT2Md4+2dK5D/PyN1q5cpqD6g4BaoXY42+7v8UMMe4R43TREi
UQAo6Jph84eeNPOvLEOn5bbGuWAbzY+sKL30DrFSzy77YV93cy9ZXPsW+R8d
C4vDLh/66aFXUYzyZ9OlM7B/nwhT8rtYhIKsHjYVY+u1CA2USBYgKefmpI/Y
0zEq6uyQl0hYfe/VNuyLfpB6tSMbrZ9gnOn/M58zViJz2zORxRfSYxzb8ahr
oGfQYyTk3BE3g10cwqIPZ/2HZvi21MxjPz8h+XbI4l807pYp/gs7uJFlFzPq
h7ZbOXovY1snU0cnx5xBNcaC9ceTu1jWe89GQslG76Df2CH6Vo2bBROhW7RN
fhH71lkVloxzCoynnulnY9cbc/HH9TyBsE07C5jYlras/8RuPIWYaY37w9gd
br3b4zVewJ7nl+7RsVX1h09GUXmwcqYvp+HPej/ZZpr+VQjPbt7sfI9ddLSV
yZ/8CnbaeqzLxfb6aqL9ifUaZh3jLydj655FQdFGJRBeQvVEYC9Na9YceFAK
n9KiHd2x3/vliArOvAemh8vCcey06vrmqsZyGHpNd5f9832E+NgZPyvA08P3
EBe2q6Xh+gj5D2B714EYxOdhW/dbJyPfj1BDcZ57iM0vww5XzKgGY/PKFG/s
cZcd2TzNNSB5rXXcHPsFM3Pmo0IdPH05WLWAz2eMBkMyy6IeHnePH23AdveR
0ov0awCrJdOFh9g7V9wJM25pgqO3Nt7Qxe6JJgRc9rWCg5Nzng2+H9G88dqh
/a2QPxFetBZb5/qgS2roZ2iign0+4/uWEBCBej62QXSFKLEf+4B7/dVj5p3A
tbS1UQDfz/lJqTTP6U5gpCW1FOP7nXnuclNcQheQybyJjticp1Zvb+z8Aq+m
+exe4Dx4c8y6z8ixBxJE9k4o47w43fpMyHVFL9QKelBV3ykQN1/UDUvvBYmj
PYUnsS+bpNxBY1+h19grIALnUYLUjPyDN/2gLhYrXobz7ICoDl1cfxCKSxJs
3lO4vjT2s7bED8KQBrfrLuz5W1G8GoODIGOupf6UiefH373LMnwIzggsC0ZN
4flxBcberh0GiSqTejWct2/Ylft5rMbA5q5GiNgIBSJFF20lnozBtti7D08N
4/lekbgiPzcGIj8v+BUOUbCGOv3IIOkblH6963dwEOfRGP9CIH0cdMYdY0/1
47zrNn/5w3kKjBxdRBXpFDBu7Al/kDsFEgxHB+cvFKRoKdnqL0yB4ceykgdd
FEjGr+C9GceEXUa1SYKdFPCYvrKXxnWttzZkuOUzBVdtV0WmCU+D9NWCMY4G
Cv6uvaJVe2EWeDzlR8Zwfdwlc+5xctwsmKxj00dKcb3wdhByez0LhtRQL+Md
BY3yBxmrueaALuAs3P6WAuNghRi7+3NQF3t3YxqutzraXcNUDRssE4PZX3G9
Xp+xN1FScR5G+UXlhnD9j5jhiBQ3n4eU10qiUbg/mIMPPqt95sFuVPCN8l38
fvoBO+FP83D4Vozm+Tv4/Itab+Fx+wmFlsfH62Jwf+J3JZ/9agEu2H62lAuj
4INVVkPbX79grGSqcyPub3amnketbr8gq2dhp89ZCh5N0fKb7/6CPVpGZq2n
cT2+UXSn/tsvOL5LrCjoFAVq78ttP9z+DQ6stnPIDvdnSj3fCtuXQexOcEjr
YbyeYL6w2p2cRGv58b9M1CkwOGRQMGbEScB6v3BXNQoObvAf4LXlJHyjK5KD
aTjfinDGhnES2R77zfKUKIga61iq+MxJfC2te8uWpYBukeFVenUFYR8qO7tu
NQX+sgbnXxZzEXU2sc3lLCagCj/LxN08xIu0ueCMLCak3Yxg2RziITQ9j+w6
lcGEiKNxsdKOPATlpSa8+QkTDo5kNmSF8xDLhhOacSlMoPO3mpY08xCyZw/Z
OCQy4bvFNoO+07zE9rVs4SehTFDta9uplMBHHHDy1KzF/fODRZp42YQAoRdv
UXuPD/fjYdC1nVOUOCY50XFSbwrKwiJlFJxWE0VvN/u1nJyEN4fbNBRfiRPB
D+5rnDo3AVmhrMI+vbUEW5e0KzIYh3J1FVXju+sJXdPFeF2Jb9Bkqi1xRmET
wTu6+KGtYBR4e52+77ohTcT1Zzo5mI/A8YcmLdcvyxAR9UVi0DsE6Rw1J82K
ZInui6OraJqDECl7fPTsoByhXNu+tiZxAIa+H8piKMsTB3dIb7QI7gMf9ep6
BzcFIoho0a6o6IbHShaURrIisYc/ZzJOqQv2dxAmLgNbiUDTfYTztjbwJ/RT
JsSUCEbvrdXX/m6GCZEPNl57thOmgroj8yK10JaQa3YyWJn4ybte7lkBCUfa
n+5OzlUhTKwNxQcy8+E898WG/kIVwibE64GTYz6Eaqo5Kr5VIVLl37X0r8+H
gsRX4fmVKsTrLLZ19608EP27orm6S4WQ9Nw7EDqcCw0d3afnVtCI56oi0zn+
2WDcJZxw6DiNWLwXuCHh2WOw52tVSLCjEaYSPoZHNz0GL53EN11ONMLpfIG0
eGwqZN7b+PXURRrx8NQkTeZbCqy02abkG0IjfnSo+swF3ofyL/oo8zmNUDnS
F6mfHQdd/NxWk3k0IntRVzc99l9g6dYMqRfTiFX6G80Fx2NA5v5hgTKSRkwf
uC4XHRQNgSdsj31upxG1Ns9G3bNDIClaemxdN414Z5Lz6NB4ILwoGfC176cR
XxhcEq+DfKFnw9nUb+M0YvfDfcbXsz1g7oCyhhqLRvDmtzu2BJ0HIT9m1VU2
jbAPOqx6L9sJ5J/nW5cs0IhLvs5G77It4f/+DxD//3/gf+WdigI=
      "]]}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None},
  PlotRange->{{-15, 15}, {0., 0.9999999259944911}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.760279195324033*^9, {3.760282654748809*^9, 3.760282661524579*^9}}]
}, Open  ]]
},
WindowSize->{1360, 704},
WindowMargins->{{-1, Automatic}, {Automatic, -1}},
FrontEndVersion->"10.3 for Linux x86 (64-bit) (October 9, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 124, 2, 32, "Input"],
Cell[707, 26, 174, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[918, 33, 142, 3, 32, "Input"],
Cell[1063, 38, 193, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1293, 46, 230, 6, 32, "Input"],
Cell[1526, 54, 188, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1751, 61, 142, 3, 32, "Input"],
Cell[1896, 66, 193, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2126, 74, 141, 3, 32, "Input"],
Cell[2270, 79, 195, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2502, 87, 230, 6, 32, "Input"],
Cell[2735, 95, 165, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2937, 103, 145, 3, 32, "Input"],
Cell[3085, 108, 142, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3264, 116, 127, 2, 32, "Input"],
Cell[3394, 120, 130, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3561, 127, 239, 6, 32, "Input"],
Cell[3803, 135, 137, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3977, 142, 127, 2, 32, "Input"],
Cell[4107, 146, 130, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4274, 153, 131, 2, 32, "Input"],
Cell[4408, 157, 126, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4571, 164, 241, 6, 32, "Input"],
Cell[4815, 172, 140, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4992, 179, 948, 27, 32, "Input"],
Cell[5943, 208, 17234, 295, 243, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23214, 508, 896, 27, 32, "Input"],
Cell[24113, 537, 12166, 212, 276, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
