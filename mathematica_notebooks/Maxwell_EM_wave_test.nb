(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     14330,        446]
NotebookOptionsPosition[     12836,        389]
NotebookOutlinePosition[     13170,        404]
CellTagsIndexPosition[     13127,        401]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"kvec", "[", 
    RowBox[{"kx_", ",", "ky_", ",", "kz_"}], "]"}], "=", 
   RowBox[{"{", 
    RowBox[{"kx", ",", "ky", ",", "kz"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.654617558774081*^9, 3.654617582960882*^9}, 
   3.6546176273608637`*^9, {3.654618308486928*^9, 3.654618320874442*^9}, 
   3.654618369720213*^9, 3.654620486697485*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"kabs", "[", 
    RowBox[{"kx_", ",", "ky_", ",", "kz_"}], "]"}], "=", 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"kx", "^", "2"}], "+", 
     RowBox[{"ky", "^", "2"}], "+", 
     RowBox[{"kz", "^", "2"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.654969910654973*^9, 3.6549699401572123`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"xvec", "[", 
    RowBox[{"x_", ",", "y_", ",", "z_"}], "]"}], "=", 
   RowBox[{"{", 
    RowBox[{"x", ",", "y", ",", "z"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.654617558774081*^9, 3.654617582960882*^9}, 
   3.6546176273608637`*^9, {3.654618308486928*^9, 3.654618320874442*^9}, {
   3.654618369720213*^9, 3.6546184097879887`*^9}, 3.6546204848889933`*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"\[Omega]", "[", 
    RowBox[{"kx_", ",", "ky_", ",", "kz_"}], "]"}], "=", 
   RowBox[{
    RowBox[{"Norm", "[", 
     RowBox[{"kvec", "[", 
      RowBox[{"kx", ",", "ky", ",", "kz"}], "]"}], "]"}], "*", "c0"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.654617558774081*^9, 3.654617582960882*^9}, 
   3.6546176273608637`*^9, {3.654618308486928*^9, 3.654618320874442*^9}, {
   3.654618369720213*^9, 3.654618413453417*^9}, 3.6546204828765583`*^9, 
   3.6546281535484056`*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"fx", "[", 
   RowBox[{"x_", ",", "y_", ",", "z_", ",", "kx_", ",", "ky_", ",", "kz_"}], 
   "]"}], "=", "0"}]], "Input",
 CellChangeTimes->{{3.65564261255026*^9, 3.655642648901306*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.65564279238216*^9, 3.6556429035884943`*^9, 
  3.655644083315661*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"fy", "[", 
   RowBox[{"x_", ",", "y_", ",", "z_", ",", "kx_", ",", "ky_", ",", "kz_"}], 
   "]"}], "=", "0"}]], "Input",
 CellChangeTimes->{{3.655642652089808*^9, 3.655642652190156*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.655642792455596*^9, 3.655642903668104*^9, 
  3.655644083398408*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"fz", "[", 
   RowBox[{"x_", ",", "y_", ",", "z_", ",", "kx_", ",", "ky_", ",", "kz_"}], 
   "]"}], "=", 
  RowBox[{
   RowBox[{"Cos", "[", 
    RowBox[{
     RowBox[{"kvec", "[", 
      RowBox[{"kx", ",", "0", ",", "0"}], "]"}], ".", 
     RowBox[{"xvec", "[", 
      RowBox[{"x", ",", "y", ",", "z"}], "]"}]}], "]"}], "*", 
   RowBox[{"1", "/", "\[Pi]"}], "*", 
   RowBox[{"s", "/", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"s", "^", "2"}], "+", 
      RowBox[{"z", "^", "2"}]}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.6556426558832207`*^9, 3.655642785119516*^9}, {
  3.655642876442574*^9, 3.655642901749992*^9}, {3.655642966395907*^9, 
  3.6556429740294523`*^9}, {3.655644037462969*^9, 3.655644074948696*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"s", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"kx", " ", "x"}], "]"}]}], 
  RowBox[{"\[Pi]", " ", 
   RowBox[{"(", 
    RowBox[{
     SuperscriptBox["s", "2"], "+", 
     SuperscriptBox["z", "2"]}], ")"}]}]]], "Output",
 CellChangeTimes->{3.65564279253027*^9, 3.655642903678934*^9, 
  3.655642974617598*^9, 3.6556440834657793`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Efield", "[", 
   RowBox[{"x_", ",", "y_", ",", "z_", ",", "kx_", ",", "ky_", ",", "kz_"}], 
   "]"}], "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"fx", "[", 
     RowBox[{"x", ",", "y", ",", "z", ",", "kx", ",", "ky", ",", "kz"}], 
     "]"}], ",", 
    RowBox[{"fy", "[", 
     RowBox[{"x", ",", "y", ",", "z", ",", "kx", ",", "ky", ",", "kz"}], 
     "]"}], ",", 
    RowBox[{"fz", "[", 
     RowBox[{"x", ",", "y", ",", "z", ",", "kx", ",", "ky", ",", "kz"}], 
     "]"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.655640067473538*^9, 3.655640098983926*^9}, {
  3.6556401293813257`*^9, 3.655640154093807*^9}, {3.655640373059491*^9, 
  3.655640400599658*^9}, {3.655642368623993*^9, 3.655642373581102*^9}, {
  3.655642403762372*^9, 3.655642414761744*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", "0", ",", 
   FractionBox[
    RowBox[{"s", " ", 
     RowBox[{"Cos", "[", 
      RowBox[{"kx", " ", "x"}], "]"}]}], 
    RowBox[{"\[Pi]", " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["s", "2"], "+", 
       SuperscriptBox["z", "2"]}], ")"}]}]]}], "}"}]], "Output",
 CellChangeTimes->{{3.655640338020232*^9, 3.655640349815242*^9}, 
   3.655640402407855*^9, {3.655641019263283*^9, 3.655641041515781*^9}, 
   3.655641346683743*^9, {3.655641590689777*^9, 3.655641616769985*^9}, 
   3.655641669764888*^9, 3.655642792603785*^9, 3.655642903731001*^9, 
   3.655642975526525*^9, 3.6556440835524807`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Div", "[", 
  RowBox[{
   RowBox[{"Efield", "[", 
    RowBox[{"x", ",", "y", ",", "z", ",", "kx", ",", "ky", ",", "kz"}], "]"}],
    ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "y", ",", "z"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6556426010825243`*^9, 3.655642604023621*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"2", " ", "s", " ", "z", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"kx", " ", "x"}], "]"}]}], 
   RowBox[{"\[Pi]", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["s", "2"], "+", 
       SuperscriptBox["z", "2"]}], ")"}], "2"]}]]}]], "Output",
 CellChangeTimes->{3.655642792679842*^9, 3.655642903789465*^9, 
  3.655642976312199*^9, 3.6556440836312838`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Bfield", "[", 
   RowBox[{"x_", ",", "y_", ",", "z_", ",", "kx_", ",", "ky_", ",", "kz_"}], 
   "]"}], "=", 
  RowBox[{
   RowBox[{"1", "/", 
    RowBox[{"kabs", "[", 
     RowBox[{"kx", ",", "ky", ",", "kz"}], "]"}]}], "*", 
   RowBox[{"Cross", "[", 
    RowBox[{
     RowBox[{"kvec", "[", 
      RowBox[{"kx", ",", "ky", ",", "kz"}], "]"}], ",", 
     RowBox[{"Efield", "[", 
      RowBox[{"x", ",", "y", ",", "z", ",", "kx", ",", "ky", ",", "kz"}], 
      "]"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.655640162630199*^9, 3.6556401781451483`*^9}, {
  3.655640287208053*^9, 3.655640359071681*^9}, {3.655640427502386*^9, 
  3.6556404523545513`*^9}, {3.6556424209124107`*^9, 3.6556424272889643`*^9}, {
  3.6556428205776787`*^9, 3.65564285878588*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox[
    RowBox[{"ky", " ", "s", " ", 
     RowBox[{"Cos", "[", 
      RowBox[{"kx", " ", "x"}], "]"}]}], 
    RowBox[{
     SqrtBox[
      RowBox[{
       SuperscriptBox["kx", "2"], "+", 
       SuperscriptBox["ky", "2"], "+", 
       SuperscriptBox["kz", "2"]}]], " ", "\[Pi]", " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["s", "2"], "+", 
       SuperscriptBox["z", "2"]}], ")"}]}]], ",", 
   RowBox[{"-", 
    FractionBox[
     RowBox[{"kx", " ", "s", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"kx", " ", "x"}], "]"}]}], 
     RowBox[{
      SqrtBox[
       RowBox[{
        SuperscriptBox["kx", "2"], "+", 
        SuperscriptBox["ky", "2"], "+", 
        SuperscriptBox["kz", "2"]}]], " ", "\[Pi]", " ", 
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["s", "2"], "+", 
        SuperscriptBox["z", "2"]}], ")"}]}]]}], ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{{3.655640338086433*^9, 3.65564035961558*^9}, 
   3.6556404024684563`*^9, {3.6556404408243027`*^9, 3.655640452908684*^9}, {
   3.655641019336886*^9, 3.6556410415834713`*^9}, 3.655641346754719*^9, {
   3.655641590766631*^9, 3.655641616829032*^9}, 3.655641669829001*^9, 
   3.655642792746542*^9, 3.655642903849766*^9, 3.655644083705123*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Div", "[", 
  RowBox[{
   RowBox[{"Efield", "[", 
    RowBox[{"x", ",", "y", ",", "z"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "y", ",", "z"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.655640507629315*^9, 3.655640517057749*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Div", "::", "sclr"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"The scalar expression \[NoBreak]\\!\\(Efield[\\(\\(x, y, z\
\\)\\)]\\)\[NoBreak] does not have a divergence. \\!\\(\\*ButtonBox[\\\"\
\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/Div\\\", ButtonNote -> \
\\\"Div::sclr\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.6556440838732452`*^9}],

Cell[BoxData[
 TemplateBox[{RowBox[{"Efield", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}],RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}]},
  "Div"]], "Output",
 CellChangeTimes->{
  3.655640517705824*^9, {3.655641019390082*^9, 3.655641041634212*^9}, 
   3.655641346815939*^9, {3.655641590841358*^9, 3.65564161688671*^9}, 
   3.655641669893277*^9, 3.655642792814739*^9, 3.655642903909505*^9, 
   3.655644083876381*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Efield", "[", 
   RowBox[{"x_", ",", "y_", ",", "z_"}], "]"}], " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{"0", ",", "0", ",", 
    RowBox[{
     RowBox[{"Exp", "[", 
      RowBox[{"-", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"kvec", "[", 
           RowBox[{"kx", ",", "0", ",", "0"}], "]"}], ".", 
          RowBox[{"xvec", "[", 
           RowBox[{"x", ",", "y", ",", "z"}], "]"}]}], ")"}], "^", "2"}]}], 
      "]"}], "*", 
     RowBox[{"Exp", "[", 
      RowBox[{"-", 
       RowBox[{"(", 
        RowBox[{"y", "^", "2"}], ")"}]}], "]"}], "*", "Exp"}]}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.655640656482986*^9, 3.655640728544332*^9}, {
  3.655640781002664*^9, 3.6556407992469397`*^9}, {3.6556408768867493`*^9, 
  3.655640888226864*^9}, {3.6556413433375072`*^9, 3.6556413434895573`*^9}, {
  3.655641610466907*^9, 3.6556416225114*^9}, {3.65564176051891*^9, 
  3.655641760618905*^9}, {3.6556419459804077`*^9, 3.655641946442051*^9}, {
  3.65564232585877*^9, 3.655642327682314*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", "0", ",", 
   RowBox[{
    SuperscriptBox["\[ExponentialE]", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["kx", "2"]}], " ", 
       SuperscriptBox["x", "2"]}], "-", 
      SuperscriptBox["y", "2"]}]], " ", "Exp"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.655640889327693*^9, {3.655641019442202*^9, 3.6556410416853313`*^9}, 
   3.655641346873724*^9, {3.6556415909168158`*^9, 3.6556416235991793`*^9}, 
   3.655641669954486*^9, 3.655641761198317*^9, 3.655641947362268*^9, 
   3.655642792880396*^9, 3.655642903972755*^9, 3.655644083947043*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"DivE", "[", 
   RowBox[{"x_", ",", "y_", ",", "z_"}], "]"}], "=", 
  RowBox[{"Div", "[", 
   RowBox[{
    RowBox[{"Efield", "[", 
     RowBox[{"x", ",", "y", ",", "z"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", "y", ",", "z"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.655640735800296*^9, 3.6556407561653013`*^9}, {
  3.655640955149356*^9, 3.655640964154388*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.655640746231086*^9, 3.6556407569359217`*^9}, 
   3.6556408013136377`*^9, 3.655640890560092*^9, {3.65564101948363*^9, 
   3.655641041735565*^9}, 3.65564134688087*^9, {3.6556415909877033`*^9, 
   3.655641624814893*^9}, 3.655641670011929*^9, 3.655641762359767*^9, 
   3.655641950963729*^9, 3.655642792943664*^9, 3.65564290405068*^9, 
   3.655644084045714*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Bfield", "[", 
  RowBox[{"x", ",", "y", ",", "z"}], "]"}]], "Input",
 CellChangeTimes->{{3.6556419578645563`*^9, 3.655641964972732*^9}}],

Cell[BoxData[
 RowBox[{"Bfield", "[", 
  RowBox[{"x", ",", "y", ",", "z"}], "]"}]], "Output",
 CellChangeTimes->{{3.6556419608560534`*^9, 3.655641965599783*^9}, 
   3.6556427930049973`*^9, 3.655642904116002*^9, 3.655644084114025*^9}]
}, Open  ]]
},
WindowSize->{1551, 876},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.1 for Linux x86 (64-bit) (March 23, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 390, 9, 32, "Input"],
Cell[951, 31, 356, 10, 32, "Input"],
Cell[1310, 43, 412, 9, 32, "Input"],
Cell[1725, 54, 521, 13, 32, "Input"],
Cell[CellGroupData[{
Cell[2271, 71, 220, 5, 32, "Input"],
Cell[2494, 78, 118, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2649, 85, 221, 5, 32, "Input"],
Cell[2873, 92, 117, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3027, 99, 758, 20, 32, "Input"],
Cell[3788, 121, 370, 11, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4195, 137, 795, 19, 32, "Input"],
Cell[4993, 158, 661, 16, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5691, 179, 312, 8, 32, "Input"],
Cell[6006, 189, 447, 13, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6490, 207, 791, 19, 32, "Input"],
Cell[7284, 228, 1295, 36, 65, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8616, 269, 273, 7, 32, "Input"],
Cell[8892, 278, 465, 10, 23, "Message"],
Cell[9360, 290, 438, 9, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9835, 304, 1060, 27, 32, "Input"],
Cell[10898, 333, 622, 15, 44, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11557, 353, 420, 11, 32, "Input"],
Cell[11980, 366, 407, 6, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12424, 377, 160, 3, 32, "Input"],
Cell[12587, 382, 233, 4, 32, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
