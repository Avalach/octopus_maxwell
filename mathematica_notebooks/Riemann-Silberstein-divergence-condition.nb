(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     17128,        564]
NotebookOptionsPosition[     15698,        508]
NotebookOutlinePosition[     16036,        523]
CellTagsIndexPosition[     15993,        520]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"S1", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", 
      RowBox[{"-", "\[ImaginaryI]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "\[ImaginaryI]", ",", "0"}], "}"}]}], "}"}]}]], "Input",\

 CellChangeTimes->{{3.746017667204628*^9, 3.746017667426313*^9}, {
  3.746017810893313*^9, 3.746017828624819*^9}, {3.746064099041239*^9, 
  3.746064100870956*^9}, {3.746064232541758*^9, 3.746064233899787*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     RowBox[{"-", "\[ImaginaryI]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "\[ImaginaryI]", ",", "0"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.7460179223614407`*^9, 3.74606392986615*^9, 3.746064101237793*^9, {
   3.746064218240363*^9, 3.74606423538303*^9}, {3.746064798525054*^9, 
   3.746064820585005*^9}, {3.7460665206359587`*^9, 3.746066525956051*^9}, 
   3.746074537322678*^9, 3.746075863773645*^9, 3.746076098189127*^9, 
   3.74607614803841*^9, 3.746078515228776*^9, 3.746079267225449*^9, 
   3.746080179816896*^9, 3.746081793419878*^9, 3.746081871702907*^9, 
   3.746082220471796*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"S2", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "\[ImaginaryI]"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"-", "\[ImaginaryI]"}], ",", "0", ",", "0"}], "}"}]}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.7460178299336023`*^9, 3.746017848973267*^9}, {
  3.746064103312408*^9, 3.746064105420307*^9}, {3.74606422894701*^9, 
  3.7460642302264013`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "\[ImaginaryI]"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "\[ImaginaryI]"}], ",", "0", ",", "0"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.7460179224719887`*^9, 3.746063929932637*^9, 3.746064105853628*^9, {
   3.746064218335875*^9, 3.746064235485503*^9}, {3.7460647986508913`*^9, 
   3.746064820709639*^9}, {3.746066520751981*^9, 3.746066526073861*^9}, 
   3.7460745376188297`*^9, 3.7460758640443974`*^9, 3.746076098441888*^9, 
   3.7460761483301477`*^9, 3.746078515507168*^9, 3.746079267627225*^9, 
   3.7460801799659843`*^9, 3.74608179359041*^9, 3.746081871817347*^9, 
   3.746082220587694*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"S3", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"0", ",", 
      RowBox[{"-", "\[ImaginaryI]"}], ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[ImaginaryI]", ",", "0", ",", "0"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.746017849911335*^9, 3.746017874751644*^9}, {
  3.746064110007469*^9, 3.746064111742753*^9}, {3.746064225482217*^9, 
  3.746064227011417*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{"-", "\[ImaginaryI]"}], ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[ImaginaryI]", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.746017922588591*^9, 3.746063929959441*^9, 3.746064112124531*^9, {
   3.7460642183652487`*^9, 3.746064235519162*^9}, {3.74606479868257*^9, 
   3.746064820737947*^9}, {3.7460665207799797`*^9, 3.746066526108305*^9}, 
   3.746074537728677*^9, 3.746075864151575*^9, 3.7460760985679626`*^9, 
   3.7460761484444733`*^9, 3.746078515619458*^9, 3.746079267926057*^9, 
   3.7460801799958353`*^9, 3.7460817936133337`*^9, 3.746081871848565*^9, 
   3.746082220607126*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"S", "=", 
  RowBox[{"{", 
   RowBox[{"S1", ",", "S2", ",", "S3"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7460817647613707`*^9, 3.746081772806649*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", 
       RowBox[{"-", "\[ImaginaryI]"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "\[ImaginaryI]", ",", "0"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "\[ImaginaryI]"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "\[ImaginaryI]"}], ",", "0", ",", "0"}], "}"}]}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", 
       RowBox[{"-", "\[ImaginaryI]"}], ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\[ImaginaryI]", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0"}], "}"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.746081793635043*^9, 3.746081871882701*^9, 
  3.746082220627576*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"p", "=", 
  RowBox[{"{", 
   RowBox[{"px", ",", "py", ",", "pz"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.746081699745552*^9, 3.746081704869215*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"px", ",", "py", ",", "pz"}], "}"}]], "Output",
 CellChangeTimes->{3.7460817936390123`*^9, 3.7460818718866253`*^9, 
  3.7460822206639977`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Id3", "=", 
  RowBox[{"IdentityMatrix", "[", "3", "]"}]}]], "Input",
 CellChangeTimes->{{3.7460816799774218`*^9, 3.746081685685553*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "1", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "1"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.74608179366336*^9, 3.746081871913715*^9, 
  3.7460822206883507`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"EE", "/", "c"}], "*", "Id3"}], "-", 
      RowBox[{"p", ".", "S"}]}], ")"}], ".", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"EE", "/", "c"}], "*", "Id3"}], "+", 
      RowBox[{"p", ".", "S"}]}], ")"}]}], "-", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"px", "^", "2"}], ",", 
       RowBox[{"px", " ", "py"}], ",", 
       RowBox[{"px", " ", "pz"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"py", " ", "px"}], ",", 
       RowBox[{"py", "^", "2"}], ",", 
       RowBox[{"py", " ", "pz"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"pz", " ", "px"}], ",", 
       RowBox[{"py", " ", "pz"}], ",", 
       RowBox[{"pz", "^", "2"}]}], "}"}]}], "}"}]}], "//", 
  "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.7460816739090548`*^9, 3.746081694569992*^9}, 
   3.74608173283928*^9, {3.746081776440593*^9, 3.746081792022662*^9}, {
   3.746081829707192*^9, 3.746081877897029*^9}, {3.746082056181945*^9, 
   3.746082097618141*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       FractionBox[
        SuperscriptBox["EE", "2"], 
        SuperscriptBox["c", "2"]], "-", 
       SuperscriptBox["px", "2"], "-", 
       SuperscriptBox["py", "2"], "-", 
       SuperscriptBox["pz", "2"]}], "0", "0"},
     {"0", 
      RowBox[{
       FractionBox[
        SuperscriptBox["EE", "2"], 
        SuperscriptBox["c", "2"]], "-", 
       SuperscriptBox["px", "2"], "-", 
       SuperscriptBox["py", "2"], "-", 
       SuperscriptBox["pz", "2"]}], "0"},
     {"0", "0", 
      RowBox[{
       FractionBox[
        SuperscriptBox["EE", "2"], 
        SuperscriptBox["c", "2"]], "-", 
       SuperscriptBox["px", "2"], "-", 
       SuperscriptBox["py", "2"], "-", 
       SuperscriptBox["pz", "2"]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.7460817936951637`*^9, {3.7460818302048883`*^9, 3.7460818783290653`*^9}, 
   3.746082098708107*^9, 3.746082220720484*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"p", ".", "S"}], ")"}], ".", 
   RowBox[{"(", 
    RowBox[{"p", ".", "S"}], ")"}]}], "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.7460819850676327`*^9, 3.746082003287937*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       SuperscriptBox["py", "2"], "+", 
       SuperscriptBox["pz", "2"]}], 
      RowBox[{
       RowBox[{"-", "px"}], " ", "py"}], 
      RowBox[{
       RowBox[{"-", "px"}], " ", "pz"}]},
     {
      RowBox[{
       RowBox[{"-", "px"}], " ", "py"}], 
      RowBox[{
       SuperscriptBox["px", "2"], "+", 
       SuperscriptBox["pz", "2"]}], 
      RowBox[{
       RowBox[{"-", "py"}], " ", "pz"}]},
     {
      RowBox[{
       RowBox[{"-", "px"}], " ", "pz"}], 
      RowBox[{
       RowBox[{"-", "py"}], " ", "pz"}], 
      RowBox[{
       SuperscriptBox["px", "2"], "+", 
       SuperscriptBox["py", "2"]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.746081992449967*^9, 3.746082003677652*^9}, 
   3.746082220742111*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Ds", "=", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"px", "^", "2"}], ",", 
       RowBox[{"px", " ", "py"}], ",", 
       RowBox[{"px", " ", "pz"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"py", " ", "px"}], ",", 
       RowBox[{"py", "^", "2"}], ",", 
       RowBox[{"py", " ", "pz"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"pz", " ", "px"}], ",", 
       RowBox[{"py", " ", "pz"}], ",", 
       RowBox[{"pz", "^", "2"}]}], "}"}]}], "}"}], "//", 
   "MatrixForm"}]}]], "Input",
 CellChangeTimes->{{3.7460821799937963`*^9, 3.746082207076667*^9}, {
  3.74608233458475*^9, 3.746082336120364*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      SuperscriptBox["px", "2"], 
      RowBox[{"px", " ", "py"}], 
      RowBox[{"px", " ", "pz"}]},
     {
      RowBox[{"px", " ", "py"}], 
      SuperscriptBox["py", "2"], 
      RowBox[{"py", " ", "pz"}]},
     {
      RowBox[{"px", " ", "pz"}], 
      RowBox[{"py", " ", "pz"}], 
      SuperscriptBox["pz", "2"]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.746082220762782*^9, 3.746082336629108*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Ds", "-", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"p", ".", "S"}], ")"}], ".", 
    RowBox[{"(", 
     RowBox[{"p", ".", "S"}], ")"}]}]}], "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.746082208300227*^9, 3.7460822260026913`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       SuperscriptBox["px", "2"], "-", 
       SuperscriptBox["py", "2"], "-", 
       SuperscriptBox["pz", "2"]}], 
      RowBox[{"2", " ", "px", " ", "py"}], 
      RowBox[{"2", " ", "px", " ", "pz"}]},
     {
      RowBox[{"2", " ", "px", " ", "py"}], 
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["px", "2"]}], "+", 
       SuperscriptBox["py", "2"], "-", 
       SuperscriptBox["pz", "2"]}], 
      RowBox[{"2", " ", "py", " ", "pz"}]},
     {
      RowBox[{"2", " ", "px", " ", "pz"}], 
      RowBox[{"2", " ", "py", " ", "pz"}], 
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["px", "2"]}], "-", 
       SuperscriptBox["py", "2"], "+", 
       SuperscriptBox["pz", "2"]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.7460822207873697`*^9, 3.746082226545295*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Ds2", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"Id3", "*", 
     RowBox[{"p", ".", "p"}]}], "-", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"p", ".", "S"}], ")"}], ".", 
     RowBox[{"(", 
      RowBox[{"p", ".", "S"}], ")"}]}]}], "//", "MatrixForm"}]}]], "Input",
 CellChangeTimes->{{3.746082762783145*^9, 3.746082898540223*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      SuperscriptBox["px", "2"], 
      RowBox[{"px", " ", "py"}], 
      RowBox[{"px", " ", "pz"}]},
     {
      RowBox[{"px", " ", "py"}], 
      SuperscriptBox["py", "2"], 
      RowBox[{"py", " ", "pz"}]},
     {
      RowBox[{"px", " ", "pz"}], 
      RowBox[{"py", " ", "pz"}], 
      SuperscriptBox["pz", "2"]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.746082779820356*^9, 3.746082899131812*^9}}]
}, Open  ]]
},
WindowSize->{866, 752},
WindowMargins->{{302, Automatic}, {-69, Automatic}},
FrontEndVersion->"10.3 for Linux x86 (64-bit) (October 9, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 548, 14, 32, "Input"],
Cell[1131, 38, 777, 17, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1945, 60, 507, 14, 32, "Input"],
Cell[2455, 76, 790, 18, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3282, 99, 500, 13, 32, "Input"],
Cell[3785, 114, 787, 17, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4609, 136, 177, 4, 32, "Input"],
Cell[4789, 142, 1061, 32, 55, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5887, 179, 175, 4, 32, "Input"],
Cell[6065, 185, 181, 4, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6283, 194, 160, 3, 32, "Input"],
Cell[6446, 199, 350, 10, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6833, 214, 1135, 35, 77, "Input"],
Cell[7971, 251, 1434, 42, 116, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9442, 298, 248, 7, 32, "Input"],
Cell[9693, 307, 1282, 41, 80, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11012, 353, 718, 22, 32, "Input"],
Cell[11733, 377, 942, 28, 80, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12712, 410, 277, 8, 32, "Input"],
Cell[12992, 420, 1348, 39, 80, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14377, 464, 358, 11, 32, "Input"],
Cell[14738, 477, 944, 28, 137, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

