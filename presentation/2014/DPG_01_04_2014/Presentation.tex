\documentclass{beamer}
\usepackage[latin1]{inputenc}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{framed, color}
\usepackage{color}
\usepackage{braket}
\usepackage{extarrows}
\usepackage{booktabs}
\usepackage{array}
\usetheme{Singapore}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 1

\title[]{ Real-time propagation of coupled Maxwell-Schr\"odinger and time-dependent Kohn-Sham-Maxwell systems }
\author{Ren\'e Jest\"adt, Heiko Appel and  Angel Rubio}
\institute{Fritz-Haber-Institut der Max-Planck-Gesellschaft}
\date{ 1$^{st}$ April 2014}
\begin{document}

\begin{frame}
\titlepage
\begin{center}
 \includegraphics[width=3.3cm]{./Logo_Minerva_Max-Planck-Gesellschaft.pdf}
\end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 2
\begin{frame}{Motivation}

\begin{itemize}
  \item Tracing coupled time evolution of matter and electromagnetic field; relevant for applications in: Nanoplasmonics,
        tip-enhanced Raman spectroscopy, plasmon-polariton coupling
  \item Coupling time-depending Kohn-Sham systems with Maxwell systems and implementing the method in a real-time real-space
        DFT code like "Octopus" or "FHI AIMS"
  \item Near field resolution (going beyond dipole approximation)

% \item Since the Maxwell equations have a symplectic structure and are first order in time, it is possible to transform them,
%       by using the Riemann-Silberstein vector, into a matrix spinor representation similar to the Dirac equation
% \item Maxwell's equations in a Schr\"odinger like form means we can use same techniques for electromagnetic field calculations and time evolution
% \item 
\end{itemize}
 
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 3
\begin{frame}{Matrix representation of Maxwell's equations}

  \vspace{0.5cm}
  Riemann-Silberstein (R-S) vector in vacuum [1]:

  \vspace{-0.5cm}
  \begin{align*}
    \vec{F}_{\pm}^{(0)}(\vec{r},t) = \sqrt{\epsilon_0/2} \hspace{1ex} \vec{E}(\vec{r},t) \pm i \sqrt{1/(2\mu_0)} \hspace{1ex} \vec{B}(\vec{r},t)
  \end{align*}

  \vspace{0.5cm}
  Maxwell's equations in vacuum:

  \begin{minipage}[c]{1.0\textwidth}
    \hspace{-1.1cm}
    \begin{minipage}[c]{0.4\textwidth}
      \begin{align*}
        \vec{\nabla} \cdot  \vec{E}   &= \frac{1}{\epsilon_0} \rho      \\
        \vec{\nabla} \cdot  \vec{B}   &= 0
      \end{align*}
    \end{minipage}
    \hspace{2.0cm}
    \begin{minipage}[c]{0.5\textwidth}
      \begin{align*}          
        \vec{\nabla} \times \vec{E}  &= - \partial_t \vec{B}            \\
        \vec{\nabla} \times \vec{B}  &= \mu_0 \epsilon_0 \partial_t \vec{E} + \mu_0 \vec{j}
      \end{align*}
    \end{minipage}
  \end{minipage}

  \begin{align*}
    \hspace{-1.5cm} \Downarrow  \hspace{6.5cm} \Downarrow
  \end{align*}

   \vspace{-0.7cm}
   \begin{align*}
    \hspace{-0.2cm} 
    \vec{\nabla} \cdot \vec{F}_{\pm}^{(0)} = \frac{1}{\sqrt{2 \epsilon_0}} \rho   \hspace{1.8cm}
    i \partial_t \vec{F}_{\pm}^{(0)}(\vec{r},t) = \pm c_0 \vec{\nabla} \times \vec{F}_{\pm}^{(0)}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j}
  \end{align*}

  \footnotetext[1]{Bialynicki-Birula, Progress in Optics, Vol. XXXVI, 245-294}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 4
%\begin{frame}{Complex Maxwell's equations}
%
%  Maxwell's equations  in R-S representation:
%
%  \begin{align*}
%    \vec{\nabla} \cdot \vec{F}_{\pm}^{(0)} = \frac{1}{\sqrt{2 \epsilon_0}} \rho
%  \end{align*}
%
%  \begin{align*}
%    i \partial_t \vec{F}_{\pm}^{(0)}(\vec{r},t) = \pm c_0 \vec{\nabla} \times \vec{F}_{\pm}^{(0)}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j} \hspace{1ex} .
%  \end{align*}
%
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 5
\begin{frame}{Matrix representation of Maxwell's equations}

  Rotation operator represented by Dirac matrices:
  \begin{minipage}[c]{1.0\textwidth}
    \begin{align*}
      \vec{a} \times \vec {b} = - i (\vec{a} \cdot \vec{\bf{S}} ) \vec{b} \hspace{5ex}  with  \hspace{5ex}
      \vec{\bf{S}} = \Big( \bf{S}_{x} \hspace{1ex},\hspace{1ex} \bf{S}_{y} \hspace{1ex},\hspace{1ex} \bf{S}_{z} \Big)
    \end{align*}
  \end{minipage}

  \vspace{1cm}

  Corresponding Dirac matrices:

  \vspace{-0.3cm}
  \begin{align*}
           \bf{S}_{x} = \begin{bmatrix} 0 &  0 & 0 \\  0 & 0 & -i \\  0 & i & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{y} = \begin{bmatrix} 0 &  0 & i \\  0 & 0 &  0 \\ -i & 0 & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{z} = \begin{bmatrix} 0 & -i & 0 \\ -i & 0 &  0 \\  0 & 0 & 0 \end{bmatrix} 
  \end{align*}

  \begin{align*}
    \Rightarrow \vec{\nabla} \times \vec{F}_{\pm}^{(0)} = - i (\vec{\nabla} \cdot \vec{\bf{S}} ) \vec{F}_{\pm}^{(0)}
  \end{align*}
 
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 6
\begin{frame}{Matrix representation of Maxwell's equations}

%  Ordinary differential Maxwell's equations in R-S representation: 

%  \begin{align*}
%    \vec{\nabla} \cdot \vec{F}_{\pm}^{(0)} = \frac{1}{\sqrt{2 \epsilon_0}} \rho
%  \end{align*}

  \begin{align*}
    \Rightarrow \vec{\nabla} \times \vec{F}_{\pm}^{(0)} = - i (\vec{\nabla} \cdot \vec{\bf{S}} ) \vec{F}_{\pm}^{(0)}
  \end{align*}

  \vspace{1cm}
  Maxwell's equations in a Schr\"odinger like R-S representation:

  \begin{align*}
    i \partial_t \vec{F}_{\pm}^{(0)}(\vec{r},t) &= \pm c_0 \vec{\nabla} \times \vec{F}_{\pm}^{(0)}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j}                 \\
    i \hbar \partial_t \hspace{1ex} \vec{F}_{\pm}^{(0)}(\vec{r},t) &= \textcolor{red}{ \mp c_0 \Big( \vec{\bf{S}} \cdot \frac{\hbar}{i} \vec{\nabla} \Big) }
    \vec{F}_{\pm}^{(0)}(\vec{r},t) - \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j} 
  \end{align*}

  \begin{align*}
    \hat{\mathcal{H}}^{(0)} = \textcolor{red}{ \mp c_0 \Big( \vec{\bf{S}} \cdot \frac{\hbar}{i} \vec{\nabla} \Big) }
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 7
\begin{frame}{Matrix representation of Maxwell's equations}

  Maxwell equations in medium:
  \begin{minipage}[c]{1.0\textwidth}
    \begin{minipage}[c]{0.4\textwidth}
      \begin{align*}
        \vec{\nabla} \cdot  \vec{D}   &= \rho_{\rm{free}}               \\
        \vec{\nabla} \cdot  \vec{B}   &= 0                            
      \end{align*}
    \end{minipage}
    \hspace{3ex}
    \begin{minipage}[c]{0.5\textwidth}
      \begin{align*}          
        \vec{\nabla} \times \vec{E}  &= - \partial_t \vec{B}                          \\
        \vec{\nabla} \times \vec{H}  &=   \partial_t \vec{D} + \vec{j}_{\rm{free}} 
      \end{align*}
    \end{minipage}
  \end{minipage}

  \vspace{1cm}

  Linear medium:
  \begin{align*}
    \vec{D}(\vec{r},t) = \epsilon(\vec{r}) \vec{E}(\vec{r},t) \hspace{5ex}  {\mbox{and}} \hspace{5ex}
    \vec{H}(\vec{r},t) = \mu(\vec{r}) \vec{B}(\vec{r},t) \hspace{1ex}               
  \end{align*}

  \vspace{1cm}

  Riemann-Silberstein vector for linear time-independent medium:
  \begin{align*}
    \vec{F}_{\pm}(\vec{r},t) = \sqrt{\epsilon(\vec{r})/2} \hspace{1ex} \vec{E}(\vec{r},t) \pm i \sqrt{1/(2\mu(\vec{r}))} \hspace{1ex} \vec{B}(\vec{r},t)  
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 8
%\begin{frame}{Complex Maxwell's equations}
%
%  \begin{align*}
%    \vec{\nabla} \cdot \vec{F}_{\pm}(\vec{r},t) = \frac{1}{2 v(\vec{r})} \vec{F}_{\pm}(\vec{r},t) \cdot \vec{\nabla} v(\vec{r}) 
%                                                + \frac{1}{2 w(\vec{r})} \vec{F}_{\pm}(\vec{r},t) \cdot \vec{\nabla} w(\vec{r})
%  \end{align*}
%
%  \begin{align*}
%    i \hbar \partial_t \vec{F}_{\pm}(\vec{r},t) &= \sqrt{v(\vec{r})} \Big( \vec{\bf{S}} \cdot \frac{\hbar}{i} \vec{\nabla} \Big) \sqrt{v(\vec{r})} \vec{F}_{\pm}(\vec{r},t)       \\
%                                                &  \pm i \hbar \frac{v(\vec{r})}{2 w(\vec{r})} \Big( \vec{\bf{S}} \cdot \vec{\nabla} w(\vec{r}) \Big) \vec{F}_{\pm}(\vec{r},t)
%  \end{align*}
%
%  \begin{align*}
%    c(\vec{r}) = \sqrt{ \frac{1}{\mu(\vec{r}) \epsilon(\vec{r})} }
%    \hspace{6ex}
%    w(\vec{r}) = \sqrt{ \frac{\mu(\vec{r})}{\epsilon(\vec{r})} } 
%  \end{align*}
%
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 8
\begin{frame}{Matrix representation of Maxwell's equations}

  Riemann-Silberstein Spinor:

  \vspace{-0.5cm}
  \begin{align*}
    \mathcal{F}(\vec{r},t)    = \left( \begin{array}{c} \vec{F}_{+}(\vec{r},t) \\ \vec{F}_{-}(\vec{r},t) \end{array} \right)                       
  \end{align*}

  \vspace{0.3cm}
  Spinors for current and charge densities:

  \vspace{-0.5cm}
  \begin{align*}
    P(\vec{r},t)              = \frac{\hbar}{\sqrt{2 \epsilon}} \left( \begin{array}{c} \rho(\vec{r},t)    \\ \rho(\vec{r},t)    \end{array} \right)   \hspace{5ex}
    J(\vec{r},t)              = \frac{i \hbar}{\sqrt{2 \epsilon}} \left( \begin{array}{c} \vec{j}(\vec{r},t) \\ \vec{j}(\vec{r},t) \end{array} \right)
  \end{align*}

 % \begin{align*}
 %   J(\vec{r},t)              = \frac{i}{\sqrt{2 \epsilon}} \left( \begin{array}{c} \vec{j}(\vec{r},t) \\ \vec{j}(\vec{r},t) \end{array} \right)
 % \end{align*}

  \vspace{0.3cm}
  Auxiliary variables defining the geometry of the medium:

  \vspace{-0.5cm}
  \begin{align*}
    c(\vec{r}) = \sqrt{ \frac{1}{\mu(\vec{r}) \epsilon(\vec{r})} }
    \hspace{6ex}
    w(\vec{r}) = \sqrt{ \frac{\mu(\vec{r})}{\epsilon(\vec{r})} } 
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 10

\begin{frame}{Matrix representation of Maxwell's equations}

  Gau\ss \hspace{0.5mm} laws of the electromagnetic field in R-S representation in a linear medium:

  \begin{align*}
    \hat{\mathcal{D}}(\vec{r}) = \begin{bmatrix} 1 & \hspace{2.0mm} 0 \\ 0 & \hspace{2.0mm} 1 \end{bmatrix} \frac{\hbar}{2 v(\vec{r})} \big( \vec{\nabla} v(\vec{r}) \big)
                               + \begin{bmatrix} 0 &               -1 \\ 1 &                0 \end{bmatrix} \frac{\hbar}{2 w(\vec{r})} \big( \vec{\nabla} w(\vec{r}) \big)
  \end{align*}

  \begin{align*}
    \hbar \vec{\nabla} \cdot \mathcal{F}(\vec{r},t) = \begin{pmatrix} \hbar \vec{\nabla} \cdot \vec{F}_{+}(\vec{r},t) \\ \hbar \vec{\nabla} \cdot \vec{F}_{-}(\vec{r},t) \end{pmatrix}
                                              = \hat{\mathcal{D}} \cdot \mathcal{F}(\vec{r},t) + P(\vec{r},t)
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 11

\begin{frame}{Matrix representation of Maxwell's equations}

  Amp\`ere's and Faraday's laws in a Schr\"odinger like R-S representation in a linear medium:

  \begin{align*}
    \hat{\mathcal{H}}(\vec{r}) &= \sqrt{c(\vec{r})} 
                                  \begin{bmatrix} 1 & 0 \\ 0 & -1 \end{bmatrix} 
                                  \Big( \vec{\bf{S}} \cdot \frac{\hbar}{i} \vec{\nabla} \Big)
                                  \sqrt{c(\vec{r})}                                                                   \\[1mm]
                               &+ \hspace{0.5mm} \frac{\hbar c(\vec{r})}{2 w(\vec{r})}
                                  \begin{bmatrix} 0 & \hspace{0.5mm} -i \\ i & 0 \end{bmatrix} 
                                  \Big( \vec{\bf{S}} \cdot \big( \vec{\nabla} w(\vec{r}) \big) \Big) 
  \end{align*}

  \begin{align*}
    i \hbar \partial_t \mathcal{F}(\vec{r},t) = \hat{\mathcal{H}} \mathcal{F}(\vec{r},t) - J(\vec{r},t)
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 12
\begin{frame}{Time evolution of the electromagnetic field}

  The time evolution operator is equal to the one of an inhomogeneous Schr\"odinger equation:   \\[1cm]

  Time evolution operator without current density:
  \hspace{-0.3cm}
  \begin{align*}
    \hspace{-2ex}
    \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t) &= Exp \Big[ - \frac{i}{\hbar} \int_{t}^{t+\Delta t} \hat{\mathcal{H}}(\vec{r}) \Big]
                                           = Exp \Big[ - \frac{i}{\hbar} \hat{\mathcal{H}}(\vec{r}) \Delta t \Big]                  
  \end{align*}

  \hspace{0.5cm}

  Time evolution operator with current density as inhomogeneity:
  \hspace{-0.3cm}
  \begin{align*}
    \hat{U}_{\rm{em}}(t+\Delta t,t) &= \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t)
                                      \Big(\mathcal{F}(\vec{r},t)   
                                     - \int_{t}^{t+\Delta t} \hspace{-2mm} d \tau \hat{U}_{\rm{em}}^{(0)}(\tau,t) J(\vec{r},t) \Big)          
                          %          &\approx \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t) \mathcal{F}_{+}(\vec{r},t)                                                      \\
                          %          &- \frac{1}{2} \Big( J(\vec{r},t) + exp \Big[ \frac{i}{\hbar} \hat{\mathcal{H}}_{\rm{em}}^{(0)}(\vec{r}) \Delta t \Big] J(\vec{r},t+\Delta t) \Big)
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 13
\begin{frame}{Time evolution of the electromagnetic field}

  \includegraphics[trim=0.0ex 8.0ex 0.0ex 10.5ex, clip, width=66.0ex]{E_field_z_frame_1_final.png}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 14
\begin{frame}{Time evolution of the electromagnetic field}

  \includegraphics[trim=0.0ex 8.0ex 0.0ex 10.5ex, clip, width=66.0ex]{E_field_z_frame_2_final.png}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 15
\begin{frame}{Atomic Hamiltonian}

  Hamiltonian for an atom coupled to the electromagnetic field in dipole approximation:

  \vspace{-5mm}
  \begin{align*}
    \hat{H}_{\rm{at}}(\vec{r},t) = - \frac{\hbar^2}{2 m} \Delta_{\vec{r}} - \frac{e^2}{|\vec{r}-\vec{R}|} - e \vec{r} \cdot \vec{E}(\vec{r_0},t)
  \end{align*}

  \begin{align*}
    \hat{H}_{\rm{at}}^{(0)}(\vec{r},t) = - \frac{\hbar^2}{2 m} \Delta_{\vec{r}} - \frac{e^2}{|\vec{r}-\vec{R}|}
  \end{align*}

  \vspace{5mm}
  Time evolution operator for motion of the atomic electron:

  \vspace{-5mm}
  \begin{align*}
    \hat{U}_{\rm{at}}(t+\Delta t,t) = Exp \Big[ - \frac{i}{\hbar} \int_{t}^{t+\Delta t} d \tau \hat{H}_{\rm{at}}(\vec{r},\tau) \Big]
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 16
\begin{frame}{Multiscale grid}

 % \begin{center}
   \hspace{2cm} \includegraphics[scale=0.3]{Multiscale_system.png}
 % \end{center}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 16
\begin{frame}{wave function and electromagnetic field time evolution}

  \vspace{0.5cm}
  Time evolution operator for the atomic wave function:

  \textcolor{red}{
  \begin{align*}
    \hat{U}_{\rm{at}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t,t)_{\vec{E}(t)}^{\vec{E}(t + \Delta t)} & \approx 
    Exp \Big[ \hspace{-1mm} - \hspace{-1mm} \frac{i}{\hbar} \Delta t 
    \Big( \hat{H}_{\rm{at}}^{(0)}(\vec{r}) - e \vec{r} \cdot \frac{\vec{E}(t) \hspace{-0.5mm} + \hspace{-0.5mm} \vec{E}(t+\Delta t)}{2} \Big) \Big]   
  \end{align*}
  }

  \vspace{0.5cm}
  Time evolution operator for the electromagnetic field:
  
  \textcolor{blue}{
  \begin{align*}
    \hat{U}_{\rm{em}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t,t)_{\vec{J}(t)}^{\vec{J}(t + \Delta t)} 
    &   \approx \hat{U}_{\rm{em}}^{(0)}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t,t) \mathcal{F}(\vec{r},t)         \\
    & - \frac{1}{2} \Big( J(\vec{r},t) + exp \Big[ \frac{i}{\hbar} \hat{\mathcal{H}}_{\rm{em}}(\vec{r}) \Delta t \Big] J(\vec{r},t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) \Big)  
  \end{align*}
  }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 17
\begin{frame}{Predictor corrector method}

% \hspace{-5cm}
% \vspace{-1cm}
%  \begin{center}
  \vspace{-0.3cm}
  \begin{align*}
    t' = t_0 + \Delta t  \hspace{1cm} t'' = t_0 + 2 \Delta t
  \end{align*}
 
  \vspace{-1.2cm}
    \includegraphics[trim=6.0cm 0cm 2cm 3cm]{./Predictor_corrector_illustration/Predictor_corrector_illustration_2.pdf} 
%  \end{center}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 18
\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_1.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_1.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 19
\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_2.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_2.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 20
\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_3.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_3.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 21
\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_4.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_4.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 22
\begin{frame}{Summary and outlook}

  Summary:
  
  \begin{itemize}
    \item Riemann-Silberstein-formulation of Maxwell's quations
    \item Propagation in linear medium
    \item Multiscale mesh
    \item Predictor-corrector scheme
  \end{itemize}

  Outlook:
  
  \begin{itemize}
    \item Coupling of time-dependent Kohn-Sham equation to Maxwell's equations
    \item implementation of the method in real-time real-space DFT code like "Octopus" or "FHI AIMS"
    \item Coupling beyond dipole approximation (including quadrupole, octupole coupling)
    \item Applications: Nanoplasmonics, near-field spectroscopy (tip-enhanced Raman spectroscopy), Plasmon-polariton coupling
  \end{itemize}

\end{frame}


\end{document}
