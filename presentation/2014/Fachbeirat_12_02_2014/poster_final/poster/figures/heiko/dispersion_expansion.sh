#!/bin/bash

file=overlaps_nph_40_npt_01_dx_0.020_mass_5.00_ep_alpha_4.100_is_01_01_94+01_01_95+01_01_96_a_1.00_1.00_0.00.dat
gracebat -hdevice PNG \
         -graph 0 -block $file -bxy 1:2 \
         -graph 1 -block $file -bxy 1:4 \
         -param p3.par -printfile dispersion_expansion.png
