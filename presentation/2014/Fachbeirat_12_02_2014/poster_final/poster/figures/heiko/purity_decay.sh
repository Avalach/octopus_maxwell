#!/bin/bash

file1=purity_SSH_pid70387_a4_dt0.2e-2_o30.dat
file2=decay.dat
gracebat -hdevice PNG \
         -graph 0 -block $file1 -bxy 1:2 \
         -graph 0 -block $file2 -bxy 1:2 \
         -param purity_decay00.par -printfile purity_decay00.png
gracebat -hdevice PNG \
         -graph 0 -block $file1 -bxy 1:2 \
         -graph 0 -block $file2 -bxy 1:2 \
         -param purity_decay0.par -printfile purity_decay0.png
gracebat -hdevice PNG \
         -graph 0 -block $file1 -bxy 1:2 \
         -graph 0 -block $file2 -bxy 1:2 \
         -param purity_decay1.par -printfile purity_decay1.png
gracebat -hdevice PNG \
         -graph 0 -block $file1 -bxy 1:2 \
         -graph 0 -block $file2 -bxy 1:2 \
         -param purity_decay2.par -printfile purity_decay2.png

