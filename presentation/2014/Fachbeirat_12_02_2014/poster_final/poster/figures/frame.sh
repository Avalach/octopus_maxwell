#!/bin/bash

scalef=10;

# colorbar
echo "0 1" | h5fromtxt colorbar.h5
h5topng -c bluered -X 255 -Y 31 colorbar.h5
convert -transpose colorbar.png colorbar_t.png

# input file
infile=$1
infile_idx=$(echo $infile | perl -pe 's,.*_(.*).png,$1,')

#convert -size 640x550 xc:white -fill white canvas.png
convert -size 690x580 xc:white -fill white canvas.png
composite -compose over -geometry +76+0 $infile canvas.png frame_canvas.png
composite -compose over -geometry +600+1 colorbar_t.png frame_canvas.png frame_colorbar.png
#composite -compose over -geometry +0+0 frame.png frame_colorbar.png run_1_frame_colorbar_0220.png


rm -f frame.mvg
{
cat <<-EOF
push graphic-context
  viewbox 0 0 690 580
  image over 0,0 690,580 'frame_colorbar.png'
  push graphic-context
    stroke 'black'
    fill 'transparent'
    stroke-width 1
    rectangle 80,0 591,511
    rectangle 600,0 630,511
  pop graphic-context
  push graphic-context
    font-size 25
    stroke-width 1
EOF
} > frame.mvg

for idx in $(seq 0 8); do
  y=$[ idx*64 ]
  y2=$[ idx*62 - 10 ]
  y3=$[ idx*64 + 80 ]
  y4=$[ idx*64 + 65 ]
  ticklabelx=$(echo "$scalef*(($idx-0)*0.100-0.4)" | bc -l | awk '{printf "%3.1f\n", $0}')
  ticklabely=$(echo "$scalef*(($idx-0)*0.100-0.4)" | bc -l | awk '{printf "%3.1f\n", $0}')
  echo "line 80,$y 90,$y" >> frame.mvg
  echo "line $y3,0 $y3,10" >> frame.mvg
  echo "line $y3,501 $y3,511" >> frame.mvg
  echo "text $y4,510 '" >> frame.mvg
  echo "${ticklabelx}'" >> frame.mvg
  echo "text 32,$y2 '" >> frame.mvg
  echo "${ticklabely}'" >> frame.mvg
  echo "line 591,$y 581,$y" >> frame.mvg
done
#echo "text 2,263 'y [a.u.]'" >> frame.mvg
echo "text 320,570 'x [a.u.]'" >> frame.mvg

echo $infile_idx
timev=$(echo $infile_idx*0.01 | bc -l | awk '{printf "%3.1f\n", $0}')
echo "text 120,30 '" >> frame.mvg
echo "t=$timev [a.u.]'" >> frame.mvg

scalec=1

for idx in $(seq 1 15); do
  y=$[ idx*32 ]
  y2=$[ idx*32 - 10 ]
  ticklabely=$(echo "$scalec*(($idx-0)*0.100-0.8)" | bc -l | awk '{printf "%3.1f\n", $0}')
#  if [ $idx > 0 ]; then
    echo "line 600,$y 608,$y" >> frame.mvg
    echo "line 622,$y 630,$y" >> frame.mvg
#  fi
  echo "text 635,$y2 '" >> frame.mvg
  echo "${ticklabely}'" >> frame.mvg
done




{
cat <<-EOF
  pop graphic-context
pop graphic-context
EOF
} >> frame.mvg

convert frame.mvg frame.png
mv frame.png fcolorbar_${infile}


#composite -compose over -geometry +571+1 colorbar_t.png frame.png frame_colorbar.png
