#!/bin/bash

scalef=10;

# colorbar
echo "0 1" | h5fromtxt colorbar.h5
h5topng -c bluered -X 255 -Y 31 colorbar.h5
convert -transpose colorbar.png colorbar_t.png

# input file
infile1=$1
infile1_idx=$(echo $infile1 | perl -pe 's,.*_(.*).png,$1,')

infile2=$2
infile2_idx=$(echo $infile2 | perl -pe 's,.*_(.*).png,$1,')

infile3=$3
infile3_idx=$(echo $infile3 | perl -pe 's,.*_(.*).png,$1,')

run_idx=$4

echo $infile1_idx
echo $infile2_idx
echo $infile3_idx


#convert -size 640x550 xc:white -fill white canvas.png
convert -size 1770x580 xc:white -fill white canvas.png
composite -compose over -geometry +75+0 $infile1 canvas.png canvas1.png
composite -compose over -geometry +595+0 $infile2 canvas1.png canvas2.png
composite -compose over -geometry +1115+0 $infile3 canvas2.png frame_canvas.png
composite -compose over -geometry +1650+1 colorbar_t.png frame_canvas.png frame_colorbar.png

rm -f frame.mvg
{
cat <<-EOF
push graphic-context
  viewbox 0 0 1770 580
  image over 0,0 1770,580 'frame_colorbar.png'
  push graphic-context
    stroke 'black'
    fill 'transparent'
    stroke-width 1
    rectangle 80,0 591,511
    rectangle 600,0 1111,511
    rectangle 1120,0 1631,511
    rectangle 1650,0 1680,511
  pop graphic-context
  push graphic-context
    font-size 25
    stroke-width 1
EOF
} > frame.mvg

for idx in $(seq 1 7); do
  y=$[ idx*64 ]
  y2=$[ idx*64 - 20 ]
  y3=$[ idx*64 + 80 ]
  y23=$[ idx*64 + 600 ]
  y33=$[ idx*64 + 1120 ]
  y4=$[ idx*66 + 55 ]
  y24=$[ idx*66 + 575 ]
  y34=$[ idx*66 + 1095 ]
  ticklabelx=$(echo "$scalef*(($idx-0)*0.100-0.4)" | bc -l | awk '{printf "%3.1f\n", $0}')
  ticklabely=$(echo "$scalef*(($idx-0)*0.100-0.4)" | bc -l | awk '{printf "%3.1f\n", $0}')
  echo "line 80,$y 90,$y" >> frame.mvg
  echo "line 600,$y 610,$y" >> frame.mvg
  echo "line 1120,$y 1130,$y" >> frame.mvg
  echo "line $y3,0 $y3,10" >> frame.mvg
  echo "line $y23,0 $y23,10" >> frame.mvg
  echo "line $y33,0 $y33,10" >> frame.mvg
  echo "line $y3,501 $y3,511" >> frame.mvg
  echo "line $y23,501 $y23,511" >> frame.mvg
  echo "line $y33,501 $y33,511" >> frame.mvg
  echo "text $y4,510 '" >> frame.mvg
  echo "${ticklabelx}'" >> frame.mvg
  echo "text $y24,510 '" >> frame.mvg
  echo "${ticklabelx}'" >> frame.mvg
  echo "text $y34,510 '" >> frame.mvg
  echo "${ticklabelx}'" >> frame.mvg
  echo "text 32,$y2 '" >> frame.mvg
  echo "${ticklabely}'" >> frame.mvg
  echo "line 591,$y 581,$y" >> frame.mvg
  echo "line 1111,$y 1101,$y" >> frame.mvg
  echo "line 1631,$y 1621,$y" >> frame.mvg
done
echo "text 310,570 'x [a.u.]'" >> frame.mvg
echo "text 821,570 'x [a.u.]'" >> frame.mvg
echo "text 1332,570 'x [a.u.]'" >> frame.mvg

timev1=$(echo $infile1_idx*0.01 | bc -l | awk '{printf "%3.1f\n", $0}')
timev2=$(echo $infile2_idx*0.01 | bc -l | awk '{printf "%3.1f\n", $0}')
timev3=$(echo $infile3_idx*0.01 | bc -l | awk '{printf "%3.1f\n", $0}')
echo "text 120,30 '" >> frame.mvg
echo "t=$timev1 [a.u.]'" >> frame.mvg
echo "text 631,30 '" >> frame.mvg
echo "t=$timev2 [a.u.]'" >> frame.mvg
echo "text 1142,30 '" >> frame.mvg
echo "t=$timev3 [a.u.]'" >> frame.mvg



scalec=1

for idx in $(seq 1 15); do
  y=$[ idx*32 ]
  y2=$[ idx*32 - 20 ]
  ticklabely=$(echo "$scalec*(($idx-0)*0.100-0.8)" | bc -l | awk '{printf "%3.1f\n", $0}')
#  if [ $idx > 0 ]; then
    echo "line 1650,$y 1658,$y" >> frame.mvg
    echo "line 1672,$y 1680,$y" >> frame.mvg
#  fi
  echo "text 1685,$y2 '" >> frame.mvg
  echo "${ticklabely}'" >> frame.mvg
done


echo "translate 15,300 rotate 270 text 0,0 'y [a.u.]'" >> frame.mvg
echo "text -60,1750 'electric field [a.u.]'" >> frame.mvg
echo "rotate -270 translate -15,-300" >> frame.mvg


if [ "x$run_idx" == "x1" ]; then
 echo "push graphic-context" >> frame.mvg
   echo "line 280,311 321,270 stroke black fill black translate 321,270 rotate -45 scale 1,1 path 'M 0,0  l -15,-5  +5,+5  -5,+5  +15,-5 z'" >> frame.mvg
 echo "pop graphic-context" >> frame.mvg
fi
if [ "x$run_idx" == "x4" ]; then
 echo "push graphic-context" >> frame.mvg
   echo "line 260,331 291,300 stroke black fill black translate 291,300 rotate -45 scale 1,1 path 'M 0,0  l -15,-5  +5,+5  -5,+5  +15,-5 z'" >> frame.mvg
 echo "pop graphic-context" >> frame.mvg
 echo "push graphic-context" >> frame.mvg
   echo "line 391,331 360,300 stroke black fill black translate 360,300 rotate 225 scale 1,1 path 'M 0,0  l -15,-5  +5,+5  -5,+5  +15,-5 z'" >> frame.mvg
 echo "pop graphic-context" >> frame.mvg
fi
if [ "x$run_idx" == "x5" ]; then
 echo "push graphic-context" >> frame.mvg
   echo "line 330,360 330,300 stroke black fill black translate 330,300 rotate 270 scale 1,1 path 'M 0,0  l -15,-5  +5,+5  -5,+5  +15,-5 z'" >> frame.mvg
 echo "pop graphic-context" >> frame.mvg
fi

{
cat <<-EOF
  pop graphic-context
pop graphic-context
EOF
} >> frame.mvg

convert frame.mvg frame.png
mv frame.png fcolorbar_${infile1}


#composite -compose over -geometry +571+1 colorbar_t.png frame.png frame_colorbar.png
