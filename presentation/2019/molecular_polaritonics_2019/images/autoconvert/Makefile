CWD:=$(shell pwd)
EPS_TO_PDF_EMBED:=../../util/epstopdfembed
pdf/%.png: src/%.xcf | dst_dirs
	xcf2png -f -o $@ $<
ps/%.eps: src/%.xcf | dst_dirs
	xcf2pnm -f -o - $< | convert - $@
ps/%.eps: src/%.agr | dst_dirs
	-gracebat -hardcopy -hdevice EPS -printfile $@ $<
pdf/%.pdf: ps/%.eps src/%.agr  | dst_dirs
	$(EPS_TO_PDF_EMBED) --outfile=$@ $<
pdf/%.pdf: src/%.eps | dst_dirs
	$(EPS_TO_PDF_EMBED) --outfile=$@ $<
ps/%.eps: src/%.eps | dst_dirs
	ln -nsf $(realpath $<) $@
tex/%-pstex.tex: src/%-pstex.fig | dst_dirs
	fig2dev -L pstex_t -p $* $< > $@
ps/%-pstex.eps: src/%-pstex.fig | dst_dirs
	fig2dev -L pstex $< > $@
ps/%.eps: src/%.fig | dst_dirs
	fig2dev -L eps $< > $@
pdf/%.pdf: src/%.fig | dst_dirs
	fig2dev -L pdf $< > $@
pdf/%.pdf: src/%.svg | dst_dirs
	inkscape --export-pdf=$@ $<
pdf/%.pdf: src/%.odg | dst_dirs
	loffice --headless --convert-to pdf --outdir $(dir $@) $<
ps/%.eps: src/%.svg | dst_dirs
	inkscape --export-eps=$@ $<
pdf/%-force-raster-mono.png: ps/%.eps | dst_dirs
	rm -f temp-$@.eps
	$(EPS_TO_PDF_EMBED) --nogs --outfile=temp-$@.eps $<
	gs -dSAFER -dBATCH -dNOPAUSE \
	  -r100 \
	  -sDEVICE=pngmono -dGraphicsAlphaBits=1 \
	  -r150 -sOutputFile=$@ \
	  temp-$@.eps
	rm -f temp-$@.eps
ps/%-force-raster-mono.eps: pdf/%-force-raster-mono.png | dst_dirs
	convert $< $@
pdf/%.jpg: src/%.jpg | dst_dirs
	ln -nsf $(realpath $<) $@
pdf/%.pdf: src/%.pdf | dst_dirs
	ln -nsf $(realpath $<) $@
pdf/%.png: src/%.png | dst_dirs
	ln -nsf $(realpath $<) $@
ps/%.eps: src/%.jpg | dst_dirs
	convert $< $@
ps/%.eps: src/%.pdf | dst_dirs
	convert $< $@
ps/%.eps: src/%.png | dst_dirs
	convert $< $@
pdf/%.pdf: src/%.gpi | dst_dirs
	cd $(dir $<) ; gnuplot -e "set out '$(CWD)/$@'" $(notdir $<)
ps/%.eps: pdf/%.pdf src/%.gpi | dst_dirs
	pdftops -eps $< $@
pdf/%.png: pdf/%.pdf
	pdftoppm -r 300 -png $< > $@
pdf/%.pdf: src/%.tex
	cd $(dir $<); pdflatex -halt-on-error $(notdir $<); mv $(notdir $@) "$(CWD)/$@"
pdf/%.pdf: src/%.dia
	dia -e $@ $<
ps/%.eps: src/%.dia
	dia -e $@ $<
ps/%.eps: pdf/%.pdf | dst_dirs
	pdftops -eps $< $@

SOURCE_XCF:=$(wildcard src/*/*.xcf)
PDF_XCF:=$(patsubst src/%.xcf,pdf/%.png,$(SOURCE_XCF))
PS_XCF:=$(patsubst src/%.xcf,ps/%.eps,$(SOURCE_XCF))

SOURCE_AGR:=$(wildcard src/*/*.agr)
PS_AGR :=$(patsubst src/%.agr,ps/%.eps ,$(SOURCE_AGR))
PDF_AGR:=$(patsubst src/%.agr,pdf/%.pdf,$(SOURCE_AGR))

SOURCE_EPS:=$(wildcard src/*/*.eps)
PDF_EPS:=$(patsubst src/%.eps,pdf/%.pdf,$(SOURCE_EPS))
PS_EPS:=$(patsubst src/%.eps,ps/%.eps,$(SOURCE_EPS))

SOURCE_PFIG:=$(wildcard src/*/*-pstex.fig)
PS_PFIG:=$(patsubst src/%.fig,ps/%.eps,$(SOURCE_PFIG))
PDF_PFIG:=$(patsubst src/%.fig,pdf/%.pdf,$(SOURCE_PFIG))
TEX_PFIG:=$(patsubst src/%.fig,tex/%.pstex_t,$(SOURCE_PFIG))

SOURCE_FIG:=$(wildcard src/*/*.fig)
PS_FIG:=$(patsubst src/%.fig,ps/%.eps,$(SOURCE_FIG))
PDF_FIG:=$(patsubst src/%.fig,pdf/%.pdf,$(SOURCE_FIG))

SOURCE_SVG:=$(wildcard src/*/*.svg)
PS_SVG:=$(patsubst src/%.svg,ps/%.eps,$(SOURCE_SVG))
PDF_SVG:=$(patsubst src/%.svg,pdf/%.pdf,$(SOURCE_SVG))

SOURCE_ODG:=$(wildcard src/*/*.odg)
PS_ODG:=$(patsubst src/%.odg,ps/%.eps,$(SOURCE_ODG))
PDF_ODG:=$(patsubst src/%.odg,pdf/%.pdf,$(SOURCE_ODG))

SOURCE_PDF:=$(wildcard src/*/*.pdf)
PS_PDF:=$(patsubst src/%.pdf,ps/%.eps,$(SOURCE_PDF))
PDF_PDF:=$(patsubst src/%.pdf,pdf/%.pdf,$(SOURCE_PDF))

SOURCE_PNG:=$(wildcard src/*/*.png)
PS_PNG:=$(patsubst src/%.png,ps/%.eps,$(SOURCE_PNG))
PDF_PNG:=$(patsubst src/%.png,pdf/%.png,$(SOURCE_PNG))

SOURCE_JPG:=$(wildcard src/*/*.jpg)
PS_JPG:=$(patsubst src/%.jpg,ps/%.eps,$(SOURCE_JPG))
PDF_JPG:=$(patsubst src/%.jpg,pdf/%.jpg,$(SOURCE_JPG))

SOURCE_GPI:=$(wildcard src/*/*.gpi)
PS_GPI:=$(patsubst src/%.gpi,ps/%.eps,$(SOURCE_GPI))
PDF_GPI:=$(patsubst src/%.gpi,pdf/%.pdf,$(SOURCE_GPI))

SOURCE_TEX:=$(wildcard src/*/*.tex)
PS_TEX:=$(patsubst src/%.tex,ps/%.eps,$(SOURCE_TEX))
PDF_TEX:=$(patsubst src/%.tex,pdf/%.pdf,$(SOURCE_TEX))

SOURCE_DIA:=$(wildcard src/*/*.dia)
PS_DIA:=$(patsubst src/%.dia,ps/%.eps,$(SOURCE_DIA))
PDF_DIA:=$(patsubst src/%.dia,pdf/%.pdf,$(SOURCE_DIA))

PDF_RASTER:=

PDF_IMAGES:=$(PDF_XCF) $(PDF_AGR) $(PDF_EPS) $(PDF_PFIG) $(PDF_FIG) $(PDF_SVG) $(PDF_ODG) $(PDF_PDF) $(PDF_PNG) $(PDF_JPG) $(PDF_GPI) $(PDF_TEX) $(PDF_DIA) $(PDF_RASTER)
PS_IMAGES:=$(PS_XCF)  $(PS_AGR)  $(PS_EPS)  $(PS_PFIG)  $(PS_FIG)  $(PS_SVG) $(PS_ODG) $(PS_PDF)  $(PS_PNG)  $(PS_JPG)  $(PS_GPI) $(PS_DIA) $(PS_TEX)
DST_DIRS:=$(sort $(dir $(PDF_IMAGES) $(PS_IMAGES)))

all: pdf_images

# ps_images tex_images

pdf_images: $(PDF_IMAGES)
ps_images:  $(PS_IMAGES)
tex_images: $(TEX_PFIG)

dst_dirs:
	mkdir -p $(DST_DIRS)

clean:
	rm -f $(patsubst src/%.tex,src/%.log,$(SOURCE_TEX))
	rm -f $(patsubst src/%.tex,src/%.aux,$(SOURCE_TEX))
	rm -f ps/*/* pdf/*/*
	-rmdir ps/* pdf/* || true


.PHONY: clean
