%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[final]{beamer}
%\documentclass[draft]{beamer}
\graphicspath{ {images/}{images/autoconvert/pdf/} }
\usepackage[utf8]{inputenc}
\usepackage[orientation=portrait,size=a0,scale=1.2]{beamerposter}
\usetheme{MPSD}

\usepackage{blindtext}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Coupled Ehrenfest-Maxwell-Kohn-Sham Equations in Real-Time}
\author{\underline{Ren\'e Jest{\"a}dt}\textsuperscript{1}, Michael Ruggenthaler\textsuperscript{1}, Micael J.~T.~Oliveira\textsuperscript{1}, Angel Rubio\textsuperscript{1,2}, and Heiko Appel\textsuperscript{1}}
\institute{1) Max Planck Institute for the Structure and Dynamics of Matter, Hamburg, Germany\\[0.5ex]
2) Center for Computational Quantum Physics (CCQ), Flatiron Institute, 162 Fifth Avenue, New York NY 10010, USA}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Setup the logos appearing at the bottom of the poster
%
% this is a simple comma-separated list of logos, layout is done in theme
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\affiliatelogos{%
  {\includegraphics[height=\footlinelogoheight]{logos/eu_flag}},%
  {\includegraphics[height=\footlinelogoheight]{logos/erc_logo}},%
  {\includegraphics[height=\footlinelogoheight]{logos/mpsd-mini-logo}},%
  {\includegraphics[height=1.2\footlinelogoheight]{logos/minerva_logo_green}},%
  %{\includegraphics[height=\footlinelogoheight]{logos/logo_emmy_noether}},%
  %{\includegraphics[height=\footlinelogoheight]{logos/logo_alexander_von_humboldt_stiftung}},%
  %{\includegraphics[height=\footlinelogoheight]{logos/homer-simpson}},% just to ensure that the authors really checked the logos ;)
  {\includegraphics[height=\footlinelogoheight]{logos/cfel-mini-logo}},%
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Define the 'Boxes' as macros, to be used in actual document
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\PosterIntroduction{%
  \begin{block}{Introduction}
    \blindtext
    \cite{ruggenthaler2018quantum}

    \blindtext
    \subheading{subheading 1}
    %\blindtext
  \end{block}}

\def\PosterResults{%
  \begin{block}{Introduction}
    \blindtext
    \cite{pca_denoise}
  \end{block}}

\def\PosterReferences{{%
%  \setbeamercolor{block title}{bg=mpsd1,fg=mpsd1cont}
%  \setbeamercolor{block body}{bg=mpsd1cont,fg=mpsd1}
  \begin{block}{References}
    {%
    \setbeamertemplate{bibliography item}[text]
    \usebeamerfont{caption}
    \vspace{-6mm}
		\bibliography{poster.bib}\vspace{-2mm}
    }%
  \end{block}}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
% the whole poster is one beamer frame. use columns to partition
%
% Just layout the previously defined blocks here
% and add column titles if desired
%
\begin{frame}[t]
  % Columns environment, contains 3 columns
  \begin{columns}[t]
    \begin{column}{0.33\textwidth}
      % contents of left poster column
      %\columntitle{Left column title}
      \begin{block}{Introduction}
We present the theoretical foundations and the implementation details of a
density-functional approach for coupled photons, electrons, and effective
nuclei in non-relativistic quantum electrodynamics \cite{jestaedt2018,jestaedt2018phd}. Starting point of the
formalism is a generalization of the Pauli-Fierz field theory for which we
establish a one-to-one correspondence between external fields and internal
variables. Based on this correspondence, we introduce a Kohn-Sham construction
which provides a computationally feasible approach for ab-initio light-matter
interactions.  In the mean-field limit for the effective nuclei the formalism
reduces to coupled Ehrenfest-Maxwell-Pauli-Kohn-Sham equations. This allows for the
first time to treat nuclei, electrons, and photons in a self-consistent real-time
simulation.
      \end{block}


      \begin{block}{Riemann-Silberstein Vector}
  \vspace{0.5cm}
  Riemann-Silberstein (RS) vector in vacuum

  \vspace{-0.5cm}
  \begin{align*}
    \vec{F}(\vec{r},t) = \sqrt{\epsilon_0/2} \hspace{1ex} \vec{E}(\vec{r},t) + i \sqrt{1/(2\mu_0)} \hspace{1ex} \vec{B}(\vec{r},t)
  \end{align*}
  Maxwell's equations in vacuum

  \begin{minipage}[c]{1.0\textwidth}
    \hspace{-1.1cm}
    \begin{minipage}[c]{0.4\textwidth}
      \begin{align*}
        \vec{\nabla} \cdot  \vec{E}   &= \frac{1}{\epsilon_0} \rho      \\
        \vec{\nabla} \cdot  \vec{B}   &= 0
      \end{align*}
    \end{minipage}
    \hspace{2.0cm}
    \begin{minipage}[c]{0.5\textwidth}
      \begin{align*}          
       - \partial_t \vec{B} &=                \vec{\nabla} \times \vec{E}        \\
       \mu_0 \epsilon_0 \partial_t \vec{E} &= \vec{\nabla} \times \vec{B} - \mu_0 \vec{j} 
      \end{align*}
    \end{minipage}
  \end{minipage}

  \begin{align*}
    \hspace{-3.5cm} \Downarrow  \hspace{13.5cm} \Downarrow
  \end{align*}

  \vspace{-1.0cm}
   \begin{align*}
    \hspace{-0.2cm} 
    \vec{\nabla} \cdot \vec{F} = \frac{1}{\sqrt{2 \epsilon_0}} \rho   \hspace{1.8cm}
    i \partial_t \vec{F}(\vec{r},t) = \pm c_0 \vec{\nabla} \times \vec{F}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j}(\vec{r},t)
  \end{align*}

      \end{block}

      \begin{block}{Maxwell's Equations in RS Form}
  Curl of two vectors represented by Spin-1 matrices

  \begin{minipage}[c]{1.0\textwidth}
    \begin{align*}
      \vec{a} \times \vec {b} = - i (\vec{a} \cdot \vec{\bf{S}} ) \vec{b} \hspace{5ex}  with  \hspace{5ex}
      \vec{\bf{S}} = \Big( \bf{S}_{x} \hspace{1ex},\hspace{1ex} \bf{S}_{y} \hspace{1ex},\hspace{1ex} \bf{S}_{z} \Big)
    \end{align*}
  \end{minipage}
  Spin-1 matrices in analogy to Dirac $\gamma$ matrices

  \begin{align*}
           \bf{S}_{x} = \begin{bmatrix} 0 &  0 & 0 \\  0 & 0 & -i \\  0 & i & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{y} = \begin{bmatrix} 0 &  0 & i \\  0 & 0 &  0 \\ -i & 0 & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{z} = \begin{bmatrix} 0 & -i & 0 \\ -i & 0 &  0 \\  0 & 0 & 0 \end{bmatrix} 
  \end{align*}

  \begin{align*}
    \Rightarrow \vec{\nabla} \times \vec{F} = - i (\vec{\nabla} \cdot \vec{\bf{S}} ) \vec{F}
  \end{align*}
  Maxwell's equations in Schr\"odinger like RS representation

  \begin{align*}
    i \hbar \partial_t \hspace{1ex} \vec{F}(\vec{r},t) &= 
    \textcolor{blue}{ \underbrace {- i\hbar c_0 \Big( \vec{\bf{S}} \cdot \vec{\nabla} \Big) }_{\hat{H}_{\rm{Mx}} } }
    \vec{F}(\vec{r},t) - \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j}(\vec{r},t)
  \end{align*}

      \end{block}

      %\includegraphics[scale=0.7]{./images/plane_wave_and_pml_and_detector_boundaries/plane_wave_and_pml_and_detectors_boundaries_in_2D.png}

      \begin{block}{\mbox{Multiscale and predictor-corrector}}
        \vspace{-1.5cm}
        \begin{tabular}{cc}
          \begin{minipage}{0.38\textwidth}
            Different combinations of matter and Maxwell grids
            \vspace{-1.5cm}
            \begin{center}
              \includegraphics[scale=0.8]{./images/grid_types/multiscale_figures.png}
            \end{center}
          \end{minipage} &
          \hspace{1.0cm}
          \begin{minipage}{0.5\textwidth}
            \vspace{1.5cm}
            Only forward light-matter coupling
            \vspace{-1.2cm}
            \begin{center}
            \includegraphics[scale=0.75]{./images/predictor_corrector_method/predictor_corrector_maxwell_to_matter_coupling.png} \\
            \end{center}
            \vspace{2.5cm}
            \mbox{Self-consistent forward-backward} light-matter coupling
            \vspace{-1.2cm}
            \begin{center}
            \includegraphics[scale=0.75]{./images/predictor_corrector_method/predictor_corrector_full_coupling.png}
            \end{center}
          \end{minipage}
        \end{tabular}
        \vspace{0.4cm}
        \phantom{a}
      \end{block}

    \end{column}
    \begin{column}{0.33\textwidth}
      % contents of middle poster column

      \begin{block}{Comparison to FDTD}
        Comparison of our Octopus implementation with the finite-difference time-domain (FDTD) code MEEP
        \vspace{1.4cm}

        \begin{minipage}{0.4 \textwidth}
          Spatial form of external current
          \vspace{-3.0cm}

          \includegraphics[trim= 3cm 1cm 2cm 4cm, width=12cm, height=11cm]{./images/meep_reference/3D_gaussian_spatial_distribution_current_density.png}
          \includegraphics[scale=0.9]{./images/meep_reference/meep_reference_3D_gaussian_spatial_distribution_gaussian_cosine_pulse_current_at_05_00_00.png}
        \end{minipage}
      \end{block}

      %\includegraphics{./images/plane_wave_and_pml_and_detector_boundaries/plane_wave_and_pml_and_detectors_boundaries_in_2D.png}

      %\columntitle{Laser pulse propagation}
%      \begin{tabular}{cc}
%        \includegraphics[trim=1cm 0cm 7cm 0cm, clip, height=8cm]{./images/laser_pulse_scheme/laser_pulse_scheme_at_t0_in_2D.png} % & %\hspace{-6cm}
%        %\includegraphics[scale=0.7]{./images/laser_pulse_scheme/laser_pulse_scheme_at_t1_in_2D.png} \\
%        \includegraphics[trim=6cm 0cm 6cm 0cm, clip, height=8cm]{./images/laser_pulse_scheme/laser_pulse_scheme_at_t2_in_2D.png}  & %\hspace{-6cm}
%        \includegraphics[trim=8cm 0cm 4cm 0cm, clip, height=8cm]{./images/laser_pulse_scheme/laser_pulse_scheme_at_t3_in_2D.png}
%      \end{tabular}

      \begin{block}{Linear Medium Propagation}
        Propagation of the Riemann-Silberstein vector with Octopus for a geometry with a linear medium

        \begin{center}
          \includegraphics[scale=1.21]{./images/linear_medium/linear_medium.png}
        \end{center}
      \end{block}

      \begin{block}{Nanoplasmonic Dimer}
        We consider a nanoparticle dimer with 594 sodium atoms and default Troullier-Martins pseudopotentials in Octopus for distances $d_1 = 0.1$ nm and $d_2 = 0.5$ nm.
        As functional we use the time-dependent local density approximation and we enable self-consistent forward-backward coupling to Maxwell's equations in Riemann-Silberstein form.
        \vspace{-0.3cm}
          \begin{center}
            \includegraphics[scale=0.64]{./images/Na_297_dimer_hartree/Na_297_dimer_geometry.png}
          \end{center}
      \end{block}

    %  \columntitle{Lower middle column title}
    %  \PosterResults{}
    \end{column}
    % Right outer column is narrower
    \begin{column}{0.33\textwidth}
      %\columntitle{Na$_{297}$-dimer propagation}
      %\columntitle{}
%      \begin{block}{Real-time Ehrenfest-MKS results}
      \begin{block}{Results}
	Movie frame of real-time Ehrenfest-Maxwell-Pauli-Kohn-Sham simulation
at time 8.33 fs. The upper two panels show contour plots of 
the absolute value of the current density and the electron localization function
(ELF). The most relevant Maxwell field variables, the electric field along the
laser polarization direction z and the total Maxwell energy are presented in
the lower panels. In the top of the figure, we show the incident laser pulse.
\vspace{-0.9cm}
        \begin{center}
          \includegraphics[scale=0.4]{./images/run_snapshot/qpn_368_0000270_small.png}
        \end{center}
%\vspace{1.5cm}
Transverse and longitudinal electric field enhancements

        \begin{center}
%          \includegraphics[scale=0.7]{./images/Na_297_dimer_hartree/Na_297_dimer_0_1_nm_fixed_ions_electric_field_z.png}

          \includegraphics[scale=0.71]{./images/Na_297_dimer_hartree/Na_297_dimer_0_1_nm_fixed_ions_electric_field_z_decomposition.png}
        \end{center}
%\vspace{1.3cm}
Comparison of dipoles and electric field in the far-field

        \begin{center}
%          \includegraphics[scale=0.7]{./images/Na_297_dimer_hartree/Na_297_dimer_0_1_nm_fixed_ions_energy.png}

%          \includegraphics[scale=0.7]{./images/Na_297_dimer_hartree/Na_297_dimer_0_5_nm_fixed_ions_detector.png}
          \includegraphics[scale=0.71]{./images/Na_297_dimer_hartree/Na_297_dimer_0_5_nm_fixed_ions_multipoles.png}
%
%          \includegraphics[scale=0.7]{./images/Na_297_dimer_hartree/Na_297_dimer_0_1_nm_fixed_ions_lda_gga_pbe_comparison.png}
        \end{center}
      \end{block}
      \begin{block}{Summary and Outlook}
      \begin{itemize}
      \item The self-consistent coupling of light and matter allows to take the effect of induced matter
            currents on the electromagnetic field into account
      \item Electromagnetic near-field effects can be resolved
      \item In the far-field, electromagnetic detectors at the box boundary can collect outgoing spectroscopic signals
      \item Changes due to self-consistent Maxwell-matter coupling are larger than due to different functionals
      \item In the future we plan to combine our ab-initio method with the multi-trajectory approach from poster THEO 5
      \end{itemize}
\vspace{-0.7cm}
      \end{block}
      % contents of right poster column
      \PosterReferences{}
    \end{column}
  \end{columns}
\end{frame}
\end{document}
