
 CalculationMode                   = maxwell_ks

 UnitsInput                        = atomic
 UnitsOutput                       = atomic

 ParDomains                        = auto
 ParStates                         = no
 MaxwellParDomains                 = auto
 MaxwellParStates                  = no


# ----- Matter box variables ----------------------------------------------------------------------

 Dimensions                        = 3
 BoxShape                          = PARALLELEPIPED

 %Lsize
  000070.000 | 000070.000 | 000070.000
 %

 %Spacing
  000001.000 | 000001.000 | 000001.000
 %


# ----- Maxwell box variables ---------------------------------------------------------------------

 MaxwellDimensions                 = 3
 MaxwellBoxShape                   = PARALLELEPIPED

 %MaxwellLsize
  000070.000 | 000070.000 | 000070.000
 %
 
 %MaxwellSpacing
  000001.000 | 000001.000 | 000001.000
 %


# ----- Species variables -------------------------------------------------------------------------

 %Species
 'H'       | species_pseudo         | file | 'H.psf'
 'C'       | species_pseudo         | file | 'C.psf'
 %

 XYZCoordinates                    = "Na297_dimer.geometry_z_dir.xyz"


# ----- Matter calculation variables --------------------------------------------------------------

 ExtraStates                       = 20

 XCFunctional                      = lda_x + lda_c_gl

 EigenSolver                       = cg
 EigenSolverTolerance              = 1.00e-06
 EigensolverMaxIter                = 100
 ConvEnergy                        = 1.00e-06
 ConvRelDens                       = 1.00e-06
 MaximumIter                       = -1

 MoveIons                          = yes

 TDExpOrder                        = 4
 DerivativesStencil                = stencil_starplus
 DerivativesOrder                  = 8

 AbsorbingBoundaries               = mask
 ABWidth                           = 4.000


# ----- Maxwell calculation variables -------------------------------------------------------------

 MaxwellHamiltonianOperator        = spin_matrix
 MaxwellTDOperatorMethod           = maxwell_op_fd
 MaxwellTDPropagator               = maxwell_etrs

 MatterToMaxwellCoupling           = yes
 MaxwellToMatterCoupling           = yes
 MaxwellCouplingOrder              = electric_dipole_coupling + magnetic_dipole_coupling + electric_quadrupole_coupling 

 MaxwellTDExpOrder                 = 4
 MaxwellDerivativesStencil         = stencil_starplus
 MaxwellDerivativesOrder           = 8

 MaxwellFreePropagationTest        = no
 MaxwellTDEnergyCalculationIter    = 1

 MaxwellIncidentWavesEnergyCalc    = yes
 MaxwellIncidentWavesEnergyCalcIter= 10

 MaxwellABWidth                    = 4.000
 MaxwellAbsorbingBoundaries        = mask

 MaxwellTDTransEfieldCalculation   = no
 MaxwellTDEfieldTransVariance      = no
 MaxwellTDEfieldTransVarianceIter  = 10
 MaxwellTDEfieldDivergence         = no
 MaxwellTDEfieldDivergenceIter     = 10


# ----- Output variables --------------------------------------------------------------------------

 OutputFormat                      = plane_x + plane_y + plane_z + vtk + xyz


# ----- Matter output variables -------------------------------------------------------------------

 Output                            = density + current + geometry + forces + elf
 OutputInterval                    = 40
 TDOutput                          = energy + geometry + multipoles + laser
 TDMultipoleLmax                   = 2


# ----- Maxwell output variables ------------------------------------------------------------------

 MaxwellOutput                     = electric_field + magnetic_field + maxwell_energy_density + poynting_vector
 MaxwellOutputInterval             = 40
 MaxwellTDOutput                   = maxwell_energy + maxwell_fields


# ----- Time step variables -----------------------------------------------------------------------

 TDTimeStep                        = 0.1685240
 TDMaxSteps                        = 11867
 TDEnergyUpdateIter                = 1
 MaxwellTDIntervalSteps            = 40

 MaxwellTDETRSApprox               = no


# ----- Maxwell field variables -------------------------------------------------------------------
 pi=3.14159265359
 c=137.035999679

# ----- laser propagates in x direction ---------
 lambda      =  7687.7000
 omega       =  0.112000023380
 kx          =  0.000817303655
 Ez          =  0.000001000000
 pulse_width = 23063.1000
 pulse_shift = -78798.9000

 %UserDefinedMaxwellIncidentWaves
   incident_wave_mx_function | 0 | 0 | Ez | "wave_function" | plane_wave
 %

 %MaxwellFunctions
  "wave_function" | mxf_gaussian_wave | kx | 0 | 0 | pulse_shift | 0 | 0 | pulse_width 
 %

  # gaussian wave
 %UserDefinedMaxwellStates
  3 | formula | electric_field |        " Ez * cos( kx * ( x - pulse_shift ) ) * exp( -( ( x - pulse_shift )^2 / ( 2 * pulse_width^2 ) ) ) "
  2 | formula | magnetic_field | " -1/c * Ez * cos( kx * ( x - pulse_shift ) ) * exp( -( ( x - pulse_shift )^2 / ( 2 * pulse_width^2 ) ) ) "
 %

