\documentclass[professionalfonts,final]{beamer}
\usetheme{FHI-FB2014}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{amsmath, amssymb}
\usepackage{fontspec}
\usepackage[size=custom,width=113,height=84]{beamerposter}
\usepackage{tikz}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file MUST be compiled with LuaLaTeX or XeLaTeX to comply with the font restrictions.
% Ask your package manager, if you don't have them installed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \geometry{left=0mm,right=50mm}
%\setsansfont{Times New Roman}

\setbeamersize{sidebar width right =0.5cm}
\setbeamersize{text margin left=0.6cm}

%positioning grid, uncomment to display grid for help with alignment
% \setbeamertemplate{background}[grid][step=1cm,color=orange]

% possible colors for the headline: concepts, surfaces, defects, bio, interfaces, more
\renewcommand{\titlecolor}{concepts}
%switch this to black, if the headline template for your topic requires it
\renewcommand{\titletextcolor}{white}

\title{Real-time propagation of coupled Maxwell-Kohn-Sham systems}
\author{Ren\'e Jest\"adt \textsuperscript{1}, Heiko Appel \textsuperscript{1,2}, Angel Rubio \textsuperscript{1,2,3}}

\institute{1) Fritz-Haber-Institut der Max-Planck-Gesellschaft, Berlin, Germany -- 
           2) Max-Planck-Institut f\"ur Struktur und Dynamik der Materie, Hamburg, Germany  -- 
           3) Nano-bio Spectroscopy Group and ETSF, Departamento de Fisica de Materiales, Universidad del Pais Vasco, San Sebastian, Spain }
%abuse this to enter your poster ID, you can add \bfseries if required
\date{TH X}

%if your headline needs more than one extra author line, adjust these values to restore the vertical layout
% \setlength{\headlineheight}{10cm} %height of the title box
% \setlength{\ph}{71.8cm} %height of the poster columns

\begin{document}
\begin{frame}\vspace{0.5cm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% start changing stuff starting from here
% new commands in this template:
% \postercolumn{width}{content} - create a column of the given width, all content must be in the second argument
% \boxseperator - add a horizontal blue bar to divide a column in several boxes
% \heading{text} - a heading
% \subheading{text} - a subheading
% \lab{sub/superscript} - a shortcut to typeset equation sub- and superscripts in times new roman
% \reference{citationtext} - shortcut to typeset citation information as required
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \postercolumn{26.0cm}{

        \heading{Introduction}

          Based on a recent extension of time-dependent density functional theory to Quantum electrodynamics [1],
          we show first steps of an implementation of Maxwell's equations coupled to time-dependent Kohn-Sham equations.
          Our implementation utilizes the Riemann-Silberstein vector of the electromagnetic field which allows to
          write Maxwell's equations in a symplectic spinor representation similar to the Dirac equation. 
          This spinor representation allows us to use standard unitary propagation techniques [2] developed for the 
          solution of the Schr\"odinger equation. \\
          Our implementation in the real-space real-time code octopus [3] propagates the Maxwell and Kohn-Sham systems 
          coupled with a multipole interaction Hamiltonian. %trace dipole characteristics and near-field effects. 
          We illustrate our implementation for jellium spheres [4] with two different radii and for the $C_{60}$ molecule
          exposed to laser pulses.

      \boxseperator

        \heading{Maxwell's equation in Spinor representation}

          The classical Maxwell's equations in vacuum are given by

          \vspace{2ex}
          \begin{center}
           \begin{minipage}[t][4ex]{0.5\textwidth}
            \vspace{-2.0cm}
            \begin{minipage}[t]{0.2\textwidth}
             \begin{align}
              \vec{\nabla} \cdot  \vec{E}   &= 0 \hspace{1ex},   \label{MX_v_1}  \\
              \vec{\nabla} \cdot  \vec{B}   &= 0 \hspace{1ex},   \label{MX_v_2}
             \end{align}
            \end{minipage}
            \hspace{1.0cm}
            \begin{minipage}[t]{0.2\textwidth}
             \begin{align}          
              \vec{\nabla} \times \vec{E}  &= - \partial_t \vec{B}                 \hspace{1ex}    \label{MX_v_3} ,  \\
              \vec{\nabla} \times \vec{B}  &= \mu_0 \epsilon_0 \partial_t \vec{E}  \hspace{1ex}    \label{MX_v_4} .
             \end{align}
            \end{minipage}
           \end{minipage}
          \end{center}

          \vspace{0ex}
          The equations (\ref{MX_v_1}) and (\ref{MX_v_2}) and equally (\ref{MX_v_3}) and (\ref{MX_v_4}) can be combined to one complex equation.
          With the definition of the complex Riemann-Silberstein vector
          for the electromagnetic field 
 
          \vspace{-2ex}
          \begin{align}
           \vec{F}(\vec{r},t) = \sqrt{\epsilon_0/2} \hspace{1ex} \vec{E}(\vec{r},t) + i \sqrt{1/(2\mu_0)} \hspace{1ex} \vec{B}(\vec{r},t) \hspace{1ex} ,   \label{RS_v}
          \end{align}

          the two Maxwell's equations (\ref{MX_v_1}) and (\ref{MX_v_2}) are equivalent to

          \vspace{-2ex}
          \begin{align}
           \vec{\nabla} \cdot \vec{F} = 0 \hspace{1ex} .  \label{div_F_v}
          \end{align}

          \vspace{-1ex}
          The two remaining Maxwell's equations (\ref{MX_v_3}) and (\ref{MX_v_4}) can be formulated in terms of the Riemann-Silberstein vector in 
          equation (\ref{RS_v}) in the form 

          \vspace{-2ex}
          \begin{align*}
           i \partial_t \vec{F}^{(\rm{v})}(\vec{r},t) = c_0 \vec{\nabla} \times \vec{F}^{(\rm{v})}(\vec{r},t) \hspace{1ex} .
          \end{align*}

          \vspace{-0ex}
          Using the identity of the cross product
   
          \begin{center}
           \begin{minipage}[t][4ex]{0.5\textwidth}
            \vspace{-5ex}
            \begin{minipage}[t]{0.2\textwidth}
             \begin{align*} \hspace{5ex}
              \vec{a} \times \vec {b} = - i (\vec{a} \cdot \vec{\bf{S}} ) \vec{b} \hspace{10ex}  \rm{with}  
             \end{align*}
            \end{minipage}
            \vspace{-5ex} \hspace{5ex}
            \begin{minipage}[t]{0.2\textwidth}
             \begin{align*}
              \vec{\bf{S}} = \Big( \bf{S}_{x} \hspace{1ex},\hspace{1ex} \bf{S}_{y} \hspace{1ex},\hspace{1ex} \bf{S}_{z} \Big)
             \end{align*}
            \end{minipage}
           \end{minipage}
          \end{center}

          \begin{align*} \rm{and} \hspace{3ex}
           \bf{S}_{x} = \begin{bmatrix} 0 &  0 & 0 \\  0 & 0 & -i \\  0 & i & 0 \end{bmatrix} \hspace{1ex} , \hspace{3ex}
           \bf{S}_{y} = \begin{bmatrix} 0 &  0 & i \\  0 & 0 &  0 \\ -i & 0 & 0 \end{bmatrix} \hspace{1ex} , \hspace{3ex}
           \bf{S}_{z} = \begin{bmatrix} 0 & -i & 0 \\ -i & 0 &  0 \\  0 & 0 & 0 \end{bmatrix} \hspace{1ex} ,
          \end{align*}

          where $ \bf{S}_{x} $, $ \bf{S}_{y} $, and $ \bf{S}_{z} $ describe spin-1 matrices, 
          we can express Maxwell's equations (\ref{MX_v_3}) and (\ref{MX_v_4}) in a form equivalent to the Schr\"odinger 
          equation in quantum mechanics.

          \begin{minipage}[t]{0.5\textwidth}
           \begin{minipage}[t]{0.2\textwidth}
            \begin{align}
             i \hbar \partial_t \vec{F}(\vec{r},t) = \hat{\mathcal{H}} \vec{F}(\vec{r},t)    
            \end{align}
           \end{minipage} 
           \begin{minipage}[t]{0.2\textwidth}
            \begin{align}
             \hat{\mathcal{H}} = c_0 \Big( \vec{\bf{S}} \cdot \frac{\hbar}{i} \Big)
            \end{align}
           \end{minipage}
          \end{minipage}

      \boxseperator

        \heading{Jellium model Hamiltonian}

          The jellium model is a useful model to test our implementation for the coupling of Maxwell and Kohn-Sham equations. 

          \begin{align*}
           & \text{Hamiltonian of a jellium model:}
           && \hat{H}_{\mathrm{Jel}} = \hat{H}_{\mathrm{el}} + \hat{H}_{\mathrm{back}} + \hat{H}_{\mathrm{back-el}}                   \\[0.8cm]
           & \text{Electronic Hamiltonian:}
           && \hat{H}_{\mathrm{el}} = \sum_{i} \frac{  \vec{p}_{i} ^2 }{2 m } 
                                    + \frac{1}{2} \sum_{i,j} \frac{e^2}{|\vec{r}_{i}-\vec{r}_{j}|}                                    \\[0.5cm]
           & \text{Hamiltonian for positive background:}
           && \hat{H}_{\mathrm{back}} = \frac{e^2}{2} \bigg( \frac{N}{\Omega} \bigg)^2 \int_{\Omega} \mathrm{d} \vec{R}
                                      \int_{\Omega} \mathrm{d} \vec{R}' \frac{1}{|\vec{R}-\vec{R}'|}                                  \\[0.5cm]
           & \text{Interaction Hamiltonian:}
           && \hat{H}_{\mathrm{back-el}} = -e^2 \frac{N}{\Omega} \sum_{i} \int_{\Omega} \mathrm{d\vec{R}} \frac{1}{|\vec{r}_{i}-\vec{R}|}
          \end{align*}

      \boxseperator

        \heading{Kohn-Sham equation}

          Octopus uses DFT and TDDFT techniques to handle the many-body problem, where the complete system is mapped to $ N $ single
          particle Schr\"odinger equations. Without any external electromagnetic field the Kohn-Sham equations take the form 

          \begin{align*}
           i\partial_t \phi_{i}(\vec{r},t)  = \Big( \underbrace{ \frac{ \vec{\nabla}^2 }{2 m_e} + v_{KS}(\vec{r},t) }_{ \hat{H}_{\text{KS}} } \Big) \phi_{i}(\vec{r},t)  \hspace{2cm} 
           n(\vec{r},t) = \sum_{i=1}^{\text{N}_e} | \phi_{i}(\vec{r},t) |^2   .
          \end{align*}

          Here, $ N $ denotes the number of electrons in the system with the corresponding electron density $ n(\vec{r},t) $, 
          $ \phi_{i}(\vec{r},t) $ represent the Kohn-Sham orbitals, and the effective Kohn-Sham potential $ v_{KS} $ is determined self-consistently
          during the time-evolution.
      
    }%
    \postercolumn{26.0cm}{

        \heading{Maxwell-Kohn-Sham coupling}

          The coupling of matter to the electromagnetic field can be treated with a multipole expansion. In Coulomb gauge, the lowest-order
          terms of this expansion are given by electric-dipole, magnetic-dipole and electric-quadrupole couplings.

          \begin{align*}
           & \text{Total Hamiltonian of the system:}
           && \hat{H}_{\rm{tot}} = \hat{H}_{\rm{Jel}} + \hat{H}_{\rm{rad}} + \underbrace{\hat{H}_{\rm{ed}} + \hat{H}_{\rm{md}} + \hat{H}_{\rm{eq}} }_{\hat{H}_{\rm{int}} }  \\
           & \text{Radiation Hamiltonian:}
           && \hat{H}_{\rm{rad}} = \frac{1}{2} \int \Big( \epsilon_0 \vec{E}_{T}^2 + \mu_0^{-1} \vec{B}^2 \Big) \rm{d}\vec{r}                                               \\
           & \text{Electric dipole Hamiltonian:}
           && \hat{H}_{\rm{ed}} = - e \vec{D} \cdot \vec{E}_{T}(\vec{R}) = - e \sum_{i} \vec{r}_{i} \cdot \vec{E}_{T}(\vec{R})                                            \\
           & \text{Magnetic dipole Hamiltonian:}
           && \hat{H}_{\rm{md}} = - \frac{e}{2 m} \vec{M} \cdot \vec{B}(\vec{R}) 
                                = i \hbar \frac{e}{2 m} \sum_{i} \Big( \vec{r}_{i} \times \vec{B}(\vec{R}) \Big) \cdot \vec{\nabla}                                       \\
           & \text{Electric quadrupole Hamiltonian:}
           && \hat{H}_{\rm{eq}} = - \frac{e}{2} \vec{\nabla} \cdot Q \cdot \vec{E}(\vec{R}) 
                                = - \frac{e}{2} \sum_{i} \Big( \vec{r}_{i} \cdot \vec{\nabla} \Big) \Big( \vec{r}_{i} \cdot \vec{E}_{T}\bigg|_{\vec{R}} \Big)           
          \end{align*}

          All the examples considered in this work include electric-dipole, magnetic-dipole, and electric-quadrupole couplings.

      \boxseperator

        \heading{Time-evolution operators of the systems}

          The sum of the non-interacting Kohn-Sham Hamiltonian $H_{KS} $ and the interaction Hamiltonian $ H_{int}(\vec{r}) $ 
          leads to the fully coupled time evolution operator for the Kohn-Sham orbitals given by

          \textcolor{red}{
          \begin{align*}
           \hat{U}_{\rm{mat}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t,t) & \approx 
           Exp \Big[ \hspace{-1mm} - \hspace{-1mm} \frac{i}{\hbar} \Delta t 
           \Big( \hat{H}_{\rm{KS}}(\vec{r}) + \hat{H}_{\rm{int}}(\vec{r}) \Big) \Big]  . 
          \end{align*}
          }

          The propagation of the electromagnetic field is influenced by the current density $ \vec{j} $. This inhomogeneity, determined by
          the underlying electron motion, requires an expansion of the time evolution operator for the electromagnetic field [6],
          and takes the form
  
          \textcolor{blue}{
          \begin{align*}
           \hat{U}_{\rm{em}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t,t) 
           &   \approx Exp \Big[ - \frac{i}{\hbar} \Delta t \hat{\mathcal{H}} \Big] \vec{F}^{(v)}(\vec{r},t)         \\
           & - \frac{1}{2} \Big( \vec{J}(\vec{r},t) + Exp \Big[ \frac{i}{\hbar} \hat{\mathcal{H}}(\vec{r}) \Delta t \Big] 
               \vec{J}(\vec{r},t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) \Big)  .
          \end{align*}
          }

      \boxseperator

        \heading{Predictor-corrector method}

          The two coupled systems require a predictor-corrector step to obtain the correct self-consistent time evolution for both subsystems. The following 
          figure shows the predictor and corrector steps that we perform in our implementation. We start with the assumption of a constant current to get a 
          first order approximation for the Maxwell fields. Based on these electric and magnetic fields, the algorithm updates the current density of the electrons. 
          These steps are repeated for a given timestep until convergence has been reached.

          \includegraphics[trim= 15.0ex 57.0ex 10.0ex 13.0ex, width=0.23\textwidth, height=0.10\textwidth]{./Predictor_corrector_illustration/Predictor_corrector_illustration_2.pdf}

      \boxseperator

        \heading{Outlook}

          In the near future, we plan to increase the efficiency of our implementation. In particular, we plan to employ the inherent parallelization 
          of Octopus to simulate larger molecular systems. A particular focus will be on nanoplasmonics.
          In our present examples, we have considered only a simulation box with vacuum around the molecule and
          open boundary conditions. In future we plan to extend this in order to include linear electromagnetic media and other boundary conditions.

      \boxseperator
        \heading{References}
          \reference{[1] M. Ruggenthaler et al., Phys. Rev. A \textbf{90}, 012508.}
          \reference{[2] A. Castro et al., J. Chem. Phys. \textbf{121} (2004).}
          \reference{[3] X. Andrade, et al., Physical Chemistry Chemical Physics doi:10.1039/C5CP00351B (2015).}
          \reference{[4] M. Brack, Rev. Mod. Phys. \textbf{65}, 677 (1993).}
          \reference{[5] R. Loudon, The Quantum Theory of Light, Chapter 5, Second Edition (1983).}
          \reference{[6] I. Serban, Optimal control of time-dependent targets, Diploma thesis at FU Berlin (2004) .}


    }%
    \postercolumn{51.8cm}{%

        \heading{Light pulse hits a jellium sphere with radius 4 [a.u.]}

        \vspace{2cm}
        \begin{minipage}[t]{50cm}
         \begin{minipage}[t]{15cm}
          \vspace{-8cm}
          In the first example, a laser pulse (upper left figure) of an amplitude of 5 [a.u.] for the electric field and a wavelength of $ 2 \pi $ [a.u.]
          hits a jellium sphere with radius $ r=4 $ [a.u.], occupied by 4 electrons. 
          The corresponding electron density at time $ t=0 $ is shown in the lower left figure in the x-y-plane. The density is spherically symmetric
          and located in the vicinity of the positive background charge. 
          As time progresses, the light pulse propagates and hits the jellium sphere. After $ t=0.8 $ [a.u.] the center of the pulse has reached the center 
          of the jellium sphere. In 
          the upper right figure we illustrate the difference of the electric field in z-direction between the free pulse propagation and the coupled Maxwell-TDKS
          evolution. A small part of the electric field scatters at the jellium sphere.
          The electrons move due to the presence of the electromagnetic field an cause an electric current that influences the laser pulse. The 
          figure in the lower right shows the difference of the ground-state electron density at time $ t = 0 $ and the time evolved density at time $ t=0.8 $ [a.u.].
         \end{minipage}
         \hspace{1cm}
         \begin{minipage}[t]{34cm}
          \begin{minipage}[t][8.9cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-4.0_E-field-z_z=0_frame-1_high.png}
          \end{minipage}
          \begin{minipage}[t][8.9cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-4.0_E-field-z_quadrupole_diff.z=0.png}
          \end{minipage}
          \begin{minipage}[t][8.5cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-4.0_density_z=0_frame-1_high.png}
          \end{minipage}
          \begin{minipage}[t][8.5cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-4.0_density_quadrupole_diff.z=0.png}
          \end{minipage}  
         \end{minipage}
        \end{minipage}

       

     \boxseperator

        \heading{Light pulse hits a jellium sphere with radius 7 [a.u.]}

        \vspace{2cm}
        \begin{minipage}[t]{50cm}
         \begin{minipage}[t]{15cm}
          \vspace{-8cm}
          In this example, we use the same initial laser pulse as in the previous one, and also the total number of electrons in the jellium 
          system remains constant at N = 4. Here we fixed the radius of the jellium sphere at 7 [a.u.], almost twice as large as before. Since we
          keep the simulation box fixed, the box boundaries have now an effect on the groundstate electron density. In this case, the rotational symmetry is 
          broken and the density is not localized right at the center of the jellium. Due to the box boundary there are two peaks along the y-axis at $ z=0 $.
          This broken symmetry appears also in the scattered electromagnetic field demonstrated in the upper right figure. The difference of the
          electric fields of the freely propagated light pulse and the coupled pulse shows two small peaks along the y-axis, although the shape 
          of the scattered signal is still dominantly spherical.
          The behavior of the electron density is very different compared to the previous example. In the previous case, the difference of the density 
          is both, positive and negative. Here, the difference of the density increases in the center of the jellium. Since we subtract the perturbed
          system from the freely propagated one, the sink between the two density peaks becomes stronger and the two density maxima diverge slightly
          from each other.
         \end{minipage}
         \hspace{1cm}
         \begin{minipage}[t]{34cm}
          \begin{minipage}[t][8.9cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-7.0_E-field-z_z=0_frame-1_high.png}
          \end{minipage}
          \begin{minipage}[t][8.9cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-7.0_E-field-z_quadrupole_diff.z=0.png}
          \end{minipage}
          \begin{minipage}[t][8.5cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-7.0_density_z=0_frame-1_high.png}
          \end{minipage}
          \begin{minipage}[t][8.5cm]{18.5cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-7.0_density_quadrupole_diff.z=0.png}
          \end{minipage}  
         \end{minipage}
        \end{minipage}


    %    \vspace{2cm}
    %    \begin{minipage}[t][8cm]{0.6\textwidth}
    %     \begin{minipage}[t]{0.25\textwidth}
    %      bla bla bla
    %     \end{minipage}
    %     \begin{minipage}[t]{0.25\textwidth}
    %      \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-7.0_E-field-z_z=0_frame-1_high.png}
    %     \end{minipage}
    %     \begin{minipage}[t]{0.25\textwidth}
    %      \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-7.0_E-field-z_quadrupole_diff.z=0.png}
    %     \end{minipage}
    %    \end{minipage}  

    %    \begin{minipage}[t][8cm]{0.6\textwidth}
    %     \begin{minipage}[t]{0.25\textwidth}
    %      bla bla bla
    %     \end{minipage}
    %     \begin{minipage}[t]{0.25\textwidth}
    %      \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellim_radius-7.0_density_z=0_frame-1_high.png}
    %     \end{minipage}
    %     \begin{minipage}[t]{0.25\textwidth}
    %      \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{jellium_radius-7.0_density_quadrupole_diff.z=0.png}
    %     \end{minipage}
    %    \end{minipage}  
%
      \boxseperator

        \heading{Light pulse hits a fullerene molecule}

        \vspace{2cm}
        \begin{minipage}[t]{50cm}
         \begin{minipage}[t]{15cm}
          \vspace{-8cm}
          As final example, we consider a C$_{60}$  molecule exposed to a laser pulse.
          The initial laser pulse and conditions remain as before. The groundstate density is represented in the x-y-axis in the lower left figure. The 140 electrons of the system
          are mainly located around the C-atoms, but there are also larger contributions to the density between the C-atoms to bind the whole molecule. 
          The lower right figure shows that all peaks of the electron density decrease, when the light pulse reaches the center of the molecule at $ t=0.8 $ [a.u.].
          The light pulse scatters more fine-grained than in the two model examples before. This can be seen in the upper right picture where we show the electromagnetic field.
          Note, that in the present calculation, we fixed the ions and used a rather small gridspacing. Since Octopus can perform also molecular dynamics (e.g. Ehrenfest dynamics) this can be relaxed in future.
          
         \end{minipage}
         \hspace{1cm}
         \begin{minipage}[t]{34cm}
          \begin{minipage}[t][8.7cm]{18.1cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{timestep_1.E_field_3.z=0.png}
          \end{minipage}
          \begin{minipage}[t][8.7cm]{18.1cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{timestep_3.E_field_3.z=0.png}
          \end{minipage}
          \begin{minipage}[t][8.5cm]{18.1cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{timestep_1.density.z=0.png}
          \end{minipage}
          \begin{minipage}[t][8.5cm]{18.1cm}
           \includegraphics[trim= 1cm 2cm 3cm 4cm, width=0.8\textwidth, height=0.4\textwidth]{density_diff_timestep_3.z=0.png}
          \end{minipage}  
         \end{minipage}
        \end{minipage}

       
    }%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stop here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \end{frame}
\end{document}

    
