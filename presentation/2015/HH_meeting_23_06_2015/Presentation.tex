\documentclass{beamer}
\usepackage[latin1]{inputenc}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{framed, color}
\usepackage{color}
\usepackage{braket}
\usepackage{extarrows}
\usepackage{booktabs}
\usepackage{array}
\usetheme{Warsaw}
\usecolortheme{crane}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% title

\title[]{ Coupling of matter and electromagnetic fields in Octopus }
\author{Ren\'e Jest\"adt, Heiko Appel and  Angel Rubio}
%\institute{Fritz-Haber-Institut der Max-Planck-Gesellschaft}
\date{ 24$^{th}$ June 2015}
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 1

\begin{frame}
\titlepage
%\begin{center}
% \includegraphics[width=3.3cm]{./Logo_Minerva_Max-Planck-Gesellschaft.pdf}
%\end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 2

\begin{frame}{Introduction}

  \begin{itemize}
    \item Maxwell's equations transformed in Matrix representation with Riemann-Silberstein vectors
    \item Time-evolution operator for the electromagnetic field similar to the matter time-evolution operator in quantum mechanics
    \item Testing model: Jellium
    \item Multiscale grid
    \item Coupled Maxwell-matter Hamiltonian in Coulomb gauge
    \item Coupled Hamiltonian in dipole approximation for large wave length of the electromagnetic fields
    \item Coupled Hamiltonian in quadrupole approximation for near field effects
  \end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Matrix representation of Maxwell's equations}

  \vspace{0.5cm}
  Riemann-Silberstein (R-S) vector in vacuum [1]:

  \vspace{-0.5cm}
  \begin{align*}
    \vec{F}_{\pm}^{(0)}(\vec{r},t) = \sqrt{\epsilon_0/2} \hspace{1ex} \vec{E}(\vec{r},t) \pm i \sqrt{1/(2\mu_0)} \hspace{1ex} \vec{B}(\vec{r},t)
  \end{align*}

  \vspace{0.5cm}
  Maxwell's equations in vacuum:

  \begin{minipage}[c]{1.0\textwidth}
    \hspace{-1.1cm}
    \begin{minipage}[c]{0.4\textwidth}
      \begin{align*}
        \vec{\nabla} \cdot  \vec{E}   &= \frac{1}{\epsilon_0} \rho      \\
        \vec{\nabla} \cdot  \vec{B}   &= 0
      \end{align*}
    \end{minipage}
    \hspace{2.0cm}
    \begin{minipage}[c]{0.5\textwidth}
      \begin{align*}          
        \vec{\nabla} \times \vec{E}  &= - \partial_t \vec{B}            \\
        \vec{\nabla} \times \vec{B}  &= \mu_0 \epsilon_0 \partial_t \vec{E} + \mu_0 \vec{j}
      \end{align*}
    \end{minipage}
  \end{minipage}

  \vspace{-2ex}
  \begin{align*}
    \hspace{-1.5cm} \Downarrow  \hspace{6.5cm} \Downarrow
  \end{align*}

   \vspace{-6ex}
   \begin{align*}
    \hspace{-0.2cm} 
    \vec{\nabla} \cdot \vec{F}_{\pm}^{(0)} = \frac{1}{\sqrt{2 \epsilon_0}} \rho   \hspace{1.8cm}
    i \partial_t \vec{F}_{\pm}^{(0)}(\vec{r},t) = \pm c_0 \vec{\nabla} \times \vec{F}_{\pm}^{(0)}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j}
  \end{align*}

  \footnotetext[1]{Bialynicki-Birula, Progress in Optics, Vol. XXXVI, 245-294}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Matrix representation of Maxwell's equations}

  Rotation operator represented by Dirac matrices:
  \vspace{-2ex}
  \begin{minipage}[c]{1.0\textwidth}
    \begin{align*}
      \vec{a} \times \vec {b} = - i (\vec{a} \cdot \vec{\bf{S}} ) \vec{b} \hspace{5ex}  with  \hspace{5ex}
      \vec{\bf{S}} = \Big( \bf{S}_{x} \hspace{1ex},\hspace{1ex} \bf{S}_{y} \hspace{1ex},\hspace{1ex} \bf{S}_{z} \Big)
    \end{align*}
  \end{minipage}

  \vspace{4ex}
  Corresponding Dirac matrices:
  \vspace{-1ex}
  \begin{align*}
           \bf{S}_{x} = \begin{bmatrix} 0 &  0 & 0 \\  0 & 0 & -i \\  0 & i & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{y} = \begin{bmatrix} 0 &  0 & i \\  0 & 0 &  0 \\ -i & 0 & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{z} = \begin{bmatrix} 0 & -i & 0 \\ -i & 0 &  0 \\  0 & 0 & 0 \end{bmatrix} 
  \end{align*}

  \begin{align*}
    \Rightarrow \vec{\nabla} \times \vec{F}_{\pm}^{(0)} = - i (\vec{\nabla} \cdot \vec{\bf{S}} ) \vec{F}_{\pm}^{(0)}
  \end{align*}

  \vspace{2ex}
  Maxwell's equations in a Schr\"odinger like R-S representation:
  \vspace{-1ex}
  \begin{align*}
    i \hbar \partial_t \hspace{1ex} \vec{F}_{\pm}^{(0)}(\vec{r},t) &= 
    \textcolor{red}{ \underbrace { \mp c_0 \Big( \vec{\bf{S}} \cdot \frac{\hbar}{i} \vec{\nabla} \Big) }_{\hat{\mathcal{H}}^{(0)} } }
    \vec{F}_{\pm}^{(0)}(\vec{r},t) - \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j} 
  \end{align*}
 
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Time-evolution of the electromagnetic field}

  The time-evolution operator is equal to the one of an inhomogeneous Schr\"odinger equation:   \\[1cm]

  Time evolution operator without current density:
  \hspace{-0.3cm}
  \begin{align*}
    \hspace{-0ex}
    \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t) &= Exp \Big[ - \frac{i}{\hbar} \int_{t}^{t+\Delta t} \hat{\mathcal{H}^{(0)}}(\vec{r}) \Big]
                                           = Exp \Big[ - \frac{i}{\hbar} \hat{\mathcal{H}^{(0)}}(\vec{r}) \Delta t \Big]                  
  \end{align*}

  \hspace{0.5cm}

  Time evolution operator with current density as inhomogeneity[1]:
  \hspace{-0.3cm}
  \small
  \begin{align*}
    \hat{U}_{\rm{em}}(t+\Delta t,t) &= \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t)
                                       \Big(F^{(0)}(\vec{r},t)   
                                     - \int_{t}^{t+\Delta t} \hspace{-2mm} d \tau \hat{U}_{\rm{em}}^{(0)}(\tau,t) \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j}(\vec{r},t) \Big)          
                          %          &\approx \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t) \mathcal{F}_{+}(\vec{r},t)                                                      \\
                          %          &- \frac{1}{2} \Big( J(\vec{r},t) + exp \Big[ \frac{i}{\hbar} \hat{\mathcal{H}}_{\rm{em}}^{(0)}(\vec{r}) \Delta t \Big] J(\vec{r},t+\Delta t) \Big)
  \end{align*}
  \normalsize
 
  \footnotetext[1]{Joana Serban, Diploma thesis, FU-Berlin, (2004)}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 

\begin{frame}{Time-evolution of the electromagnetic field in linear medium}

  \includegraphics[trim=0.0ex 8.0ex 0.0ex 10.5ex, clip, width=66.0ex]{E_field_z_frame_1_final.png}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 

\begin{frame}{Time-evolution of the electromagnetic field in linear medium}

  \includegraphics[trim=0.0ex 8.0ex 0.0ex 10.5ex, clip, width=66.0ex]{E_field_z_frame_2_final.png}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Matter model: Jellium}

  Hamiltonian of a Jellium model:
  \begin{align*}
    \hat{H}_{\mathrm{Jel}} = \hat{H}_{\mathrm{el}} + \hat{H}_{\mathrm{back}} + \hat{H}_{\mathrm{back-el}}
  \end{align*}
  \vspace{1ex}

  Electronic Hamiltonian:
  \vspace{-1ex}
  \begin{align*} 
    \hat{H}_{\mathrm{el}} = \sum_{i} \frac{ ( \vec{p}^{(i)} )^2 }{2 m^{(i)} } 
                          + \frac{1}{2} \sum_{i,j} \frac{e^2}{|\vec{r}^{(i)}-\vec{r}^{(j)}|}
  \end{align*}

  \vspace{1ex}
  Hamiltonian of the positive background charge:
  \vspace{-1ex}
  \begin{align*}
    \hat{H}_{\mathrm{back}} = \frac{e^2}{2} \bigg( \frac{N}{\Omega} \bigg)^2 \int_{\Omega} \mathrm{d} \vec{R}
                              \int_{\Omega} \mathrm{d} \vec{R}' \frac{1}{|\vec{R}-\vec{R}'|}
  \end{align*}

  \vspace{1ex}
  Interaction Hamiltonian of the background charge with the electrons:
  \vspace{-1ex}
  \begin{align*}
    \hat{H}_{\mathrm{back-el}} = -e^2 \frac{N}{\Omega} \sum_{i} \int_{\Omega} \mathrm{d} \frac{1}{|\vec{r}^{(i)}-\vec{R}|}
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Separate grids for TDDFT and Maxwell propagation}

  \hspace{2cm} \centering \includegraphics[scale=0.25]{Multiscale_system.png}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Coupled electromagnetic-matter Hamiltonian}

  Lagrangian of a coupled electromagnetic-matter system with minimal coupling:

  \vspace{-2ex}

  \begin{align*}
    \hat{L}  = \underbrace{ \frac{1}{2} \sum_{i} m^{(i)} \dot{\vec{r}}^{(i)} - V }_{ \hat{L}_{\mathrm{mat}} }
             + \underbrace{ \frac{\epsilon_{0}}{2} \int \Big( \dot{\vec{A}}^2 - c^2 (\nabla \times \vec{A}) \Big) \mathrm{d}^3 \vec{r}' }_{ \hat{L}_{\mathrm{rad}} }
             + \underbrace{ \int \vec{j}^{\perp} \cdot \vec{A} \mathrm{d} \vec{r}' }_{ \hat{L}_{\mathrm{int}} }
  \end{align*} 
 
  \vspace{2ex}

  Alternative G\"oppert-Mayer Lagrangian to get a simpler interaction Hamiltonian later [1]:

  \begin{align*}
    \hat{L}_{\mathrm{GM}} &= \hat{L} - \frac{\mathrm{d}}{\mathrm{d} t} \int \vec{p}^{\perp}(\vec{r}) \cdot \vec{A}(\vec{r}) \mathrm{d}^3 \vec{r}  \\
                          &= \hat{L}_{\mathrm{mat}} + \hat{L}_{\mathrm{rad}} 
                           + \underbrace { \int \vec{j}^{\perp} \cdot \vec{A} \mathrm{d} \vec{r}'
                           - \frac{\mathrm{d}}{\mathrm{d} t} \int \vec{p}^{\perp}(\vec{r}) \cdot \vec{A}(\vec{r}) \mathrm{d}^3 \vec{r}' }_{\hat{L}_{\mathrm{int}}^{\mathrm{GM}} }
  \end{align*}

  \footnotetext[1]{D. P. Craig: Molecular Quantum Electrodynamics}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Dipole Approximation}

  The conjugate momentum of the Lagrangian $ \hat{L}_{\mathrm{GM}} $:
  \begin{align*}
 %  \vec{\Pi}(\vec{r}) = \epsilon_{0} \dot{\vec{A}}(\vec{r}) = - \epsilon_{0} \vec{E}^{\perp}(\vec{r})
    \vec{\Pi}(\vec{r}) = \epsilon_{0} \dot{\vec{A}}(\vec{r}) - \vec{p}^{\perp}(\vec{r}) = - \vec{D}^{\perp}(\vec{r})
  \end{align*}

  The transverse component of current density $ \vec{j} $ in dipole approximation:
  \begin{align*}
    j_{k}^{\perp}(\vec{r}) = - e \sum_{i} \sum_{l} \dot{q}_l^{(i)} \delta_{kl}^{\perp} (\vec{r} - \vec{R})
  \end{align*}

  \begin{align*}
    \delta_{kl}^{\perp} = \bigg( \frac{1}{2 \pi} \bigg)^3 \int (\delta_{kl} - \hat{k}_{k} \hat{k}_{l}) e^{i \vec{k} \cdot \vec{r}} \mathrm{d}^3 \vec{k}
  \end{align*}

  The transverse component of electric polarization $ \vec{p} $ in dipole approximation:
  \begin{align*}
    \frac{ \mathrm{d} \vec{p}^{\perp} }{ \mathrm{d} t} = \vec{j}^{\perp}(\vec{r})
  \end{align*}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Dipole Approximation [1]}

  The G\"opper-Mayer Lagrangian:
  \small
  \begin{align*}
    \hspace{-3ex}
    \hat{L}_{\mathrm{DA}}^{\mathrm{GM}} &= \underbrace{ \frac{1}{2} \sum_{i} m^{(i)} \dot{\vec{r}}^{(i)} - V }_{ \hat{L}_{\mathrm{mat}} }
                                         + \underbrace{ \frac{\epsilon_{0}}{2} \int \Big( \dot{\vec{A}}^2 - c^2 (\underbrace{ \nabla \times \vec{A}}_{\vec{B}}) \Big) 
                                           \mathrm{d}^3 \vec{r}' }_{ \hat{L}_{\mathrm{rad}} } 
                                         + \underbrace{ \int \vec{j}^{\perp} \cdot \vec{A} \mathrm{d} \vec{r}' }_{ \hat{L}_{\mathrm{int}} }  \\ 
                                        &  \hspace{2ex} - \int \vec{p}^{\perp}(\vec{r}) \cdot \dot{\vec{A}}(\vec{r}) \mathrm{d}^3 \vec{r}'
  \end{align*}
  \normalsize

  The matter Hamiltonian in dipole approximation:
  \small
  \begin{align*}
    \hat{H}_{\mathrm{DA}}^{\mathrm{GM}} &= \sum_{i} \vec{p}^{(i)} \cdot \dot{\vec{r}}^{(i)} + \int \Pi \cdot \dot{\vec{A}} \mathrm{d}^3 \vec{r}' - \hat{L}_{\mathrm{DA}}^{\mathrm{GM}}  \\
                                        &= \sum_{i} \Big[ \frac{ (\vec{p}^{(i)})^2 }{2 m^{(i)} } + V \Big] 
                                         + \frac{1}{2} \int \Big[ \frac{ (\vec{D}^{\perp})^2 }{\epsilon_{0}} + \epsilon_{0} c^2 \vec{B}^2 \Big] \mathrm{d} \vec{r}'  \\
                                        & \hspace{2ex} - \frac{1}{\epsilon_{0}} \int \vec{\mu} \cdot \vec{D}^{\perp}(\vec{r})
                                         + \frac{1}{2 \epsilon_{0}} \int |\vec{p}^{\perp}(\vec{r})|^2 \mathrm{d}^3 \vec{r}'
  \end{align*}
  \normalsize

  \footnotetext[1]{D. P. Craig: Molecular Quantum Electrodynamics}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_1.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_1.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_2.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_2.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_3.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_3.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Coupled Maxwell Schr\"odinger system}

 % Result of the Maxwell-Schroedinger propagation:
  \hspace{-1.0cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./E_field_z_coupled_frame_4.png}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[c]{0.5 \textwidth}
  \includegraphics[scale=0.31]{./wave_function_coupled_frame_4.png}
  \end{minipage}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Identical grids for TDDFT and Maxwell propagation}

  Using two identical grids to avoid a multiscale consideration.
  \vspace{3ex}

  \hspace{2cm} \centering \includegraphics[scale=0.25]{Multiscale_system_equal_grid.png}

  \vspace{3ex}
  $ \Rightarrow $ Coupling requires Multipole Approximation.

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Quadrupole Approximation [1]}

  Multipole approximation of $ j_{k}^{\perp} $:
  \small
  \begin{align*}
    j_{k}^{\perp}(\vec{r}) = - e \sum_{i} \dot{q}_l^{(i)} \delta_{kl}^{\perp} (\vec{r} - \vec{R})
                           +   e \sum_{i} \sum_{l,m} \dot{r}_{l}^{(i)} (\vec{r}^{(i)} - \vec{R})_m \nabla_m \delta_{kl}^{\perp} (\vec{r} - \vec{R})
  \end{align*}
  \normalsize

  \small
  Multipole approximation of $ p_{k}^{\perp} $:
  \begin{align*}
    \hspace{-4ex}
    p_{k}^{\perp}(\vec{r}) &= - e \sum_{i} \sum_{l} (\vec{r}^{(i)} - \vec{R})_{l} \delta_{kl}^{\perp}(\vec{r} - \vec{R})                                            \\   
                           &  \hspace{2.5ex} + \frac{e}{2} \sum_{i} \sum_{l,m} (\vec{r}^{(i)} - \vec{R})_{l} (\vec{r}^{(i)} - \vec{R})_{m} 
                              \nabla_m \delta_{kl}^{\perp} (\vec{r} - \vec{R})                                                                                      \\
                           &= \sum_{l} \mu_{l} \delta_{kl}^{\perp}(\vec{r}-\vec{R}) - \sum_{l,m} Q_{lm} \nabla_{m} \delta_{kl}^{\perp} (\vec{r} - \vec{R})
  \end{align*}
  \normalsize
  
  with electric dipole $ \vec{\mu} $ and quadrupole moment $ \boldsymbol{Q} $.

  \footnotetext[1]{D. P. Craig: Molecular Quantum Electrodynamics}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Quadrupole Approximation [1]}

  The G\"opper-Mayer Lagrangian:
  \small
  \begin{align*}
    \hspace{-3ex}
    \hat{L}_{\mathrm{DA}}^{\mathrm{GM}} &= \underbrace{ \frac{1}{2} \sum_{i} m^{(i)} \dot{\vec{r}}^{(i)} - V }_{ \hat{L}_{\mathrm{mat}} }                               
                                         + \underbrace{ \frac{\epsilon_{0}}{2} \int \Big( \dot{\vec{A}}^2 - c^2 (\underbrace{ \nabla \times \vec{A}}_{\vec{B}}) \Big) 
                                           \mathrm{d}^3 \vec{r}' }_{ \hat{L}_{\mathrm{rad}} }                                                                           
                                         + \underbrace{ \int \vec{j}^{\perp} \cdot \vec{A} \mathrm{d} \vec{r}' }_{ \hat{L}_{\mathrm{int}} }                            \\
                                        &  \hspace{2ex} - \int \vec{p}^{\perp}(\vec{r}) \cdot \dot{\vec{A}}(\vec{r}) \mathrm{d}^3 \vec{r}'
  \end{align*}
  \normalsize

  The matter Hamiltonian in quadrupole approximation:
  \small
  \begin{align*}
    \hat{H}_{\mathrm{DA}}^{\mathrm{GM}} &= \sum_{i} \frac{1}{2 m^{(i)}} (\vec{p}^{(i)})^2 + V 
                                         + \frac{1}{2} \int \Big[ \frac{ \vec{D}^{\perp} }{\epsilon_{0}} + \epsilon_{0} c^2 \vec{B}^2 \Big] \mathrm{d}^3 \vec{r}      \\
                                        & \hspace{2.5ex} - \frac{1}{\epsilon_{0}} \vec{\mu} \cdot \vec{D}^{\perp}(\vec{R}) 
                                         - \sum_{kl} Q_{kl} \nabla_{k} \vec{D}_{l}^{\perp}(\vec{R}) - \vec{m} \cdot \vec{B}(\vec{R})                                  \\
                                        & \hspace{2.5ex} + \sum_{i} \Big[ \frac{e^2}{8 m^{(i)}} (\vec{r}^{(i)} - \vec{R}) \times \vec{B}(\vec{R}) \Big]^2 
                                         + \frac{1}{2 \epsilon} \int |\vec{p}^{\perp}|^2 \mathrm{d}^3 \vec{r}'
  \end{align*}
  \normalsize

  \footnotetext[1]{D. P. Craig: Molecular Quantum Electrodynamics}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Time-evolution operators of the two coupled systems}

  The time-evolution operator of the electromagnetic field:
  \small
  \begin{align*}
    \hat{U}_{\rm{em}}(t+\Delta t,t) &= \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t)
                                      \Big(F^{(0)}(\vec{r},t)   
                                     - \int_{t}^{t+\Delta t} \hspace{-2mm} d \tau \hat{U}_{\rm{em}}^{(0)}(\tau,t) \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j}(\vec{r},t) \Big)          
  \end{align*}
  \normalsize

  \small
  \begin{align*}
    \hspace{-2ex}
    \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t) &= Exp \Big[ - \frac{i}{\hbar} \int_{t}^{t+\Delta t} \hat{\mathcal{H}}(\vec{r}) \Big]
                                           = Exp \Big[ - \frac{i}{\hbar} \hat{\mathcal{H}}(\vec{r}) \Delta t \Big]                  
  \end{align*}
  \normalsize

  \vspace{4ex}

  The time-evolution operator of the matter interacting with the electromagnetic field:
  \small
  \begin{align*}
    \hat{U}_{\rm{mat}}(t+\Delta t,t) = Exp \Big[ - \frac{i}{\hbar} \int_{t}^{t + \Delta t} \hat{H}^{\mathrm{GM}}_{\mathrm{MA}} \mathrm{d} t \Big]
  \end{align*}  

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Implementation of the electromagnetic field and matter coupling in Octopus}

  \begin{itemize}
    \item Reading matter wavefunction $ \Psi(\vec{r},t_0) $ e.g. the groundstate of a Jellium 
    \item Reading the initial Riemann-Silberstein vector $ \vec{F}(\vec{r},t_0) $, which have to be
          set in the input file and satisfies the Gau\ss{} conditions
    \item Update time-depending run routine for coupled Maxwell-matter systems using the already implemented
          predictor-corrector algorithm to get a self-consistent time evolution of Maxwell fields and matter.
  \end{itemize}


\end{frame}


\end{document}
