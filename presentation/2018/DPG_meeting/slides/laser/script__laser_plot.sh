#!/bin/bash

lambda=7600
amplitude=1e-6
shift=$( echo "-3.5*$lambda" | bc -l )
width=$( echo " 1.0*$lambda" | bc -l )
c=137
pi=3.14159265359
omega=$( echo "2*$pi*$c/$lambda" | bc -l )
kx=$( echo "2*$pi/$lambda" | bc -l )
for ii in $( seq 0 40 4200) 
do
  time=$( echo "$ii * 0.16" | bc -l )
  if [[ $( echo "$time < 100" | bc -l ) -eq 1 ]] && [[ $( echo "$time > 10" | bc -l) -eq 1 ]]
  then
    space=' '
    #time=$( printf "${space}$time\n" )
  elif [[ $( echo "$time < 10" | bc -l ) -eq 1 ]] && [[ $( echo "$time > 0" | bc -l) -eq 1 ]]
  then
    space='  '
    #time=$( printf "${space}${space}$time\n" )
  elif [[ $( echo "$time == 0" | bc -l ) -eq 1 ]]
  then
    space='   '
  fi
  #wave="${amplitude}*cos($kx*((x-$time)-$shift/$c))*exp(-((x-$time)-$shift/$c)**2/(2*($width/$c)**2))" 
  wave="${amplitude}*cos($kx*((x-$time*$c)-$shift))*exp(-((x-$time*$c)-$shift)**2/(2*$width**2))"
  cat << EOF > gnuplot_script_$ii.sh
#!/usr/bin/gnuplot

set terminal png size 1024,250
set output "laser_$ii.png"
set title "external laser propagation"
#set xlabel "time [a.u.]"
set xlabel "[a.u.]
set ylabel "electric field [a.u.]"
set samples 5000
f(x)=$wave
#set pm3d at s
#plot [0:410] [-1.5e-6:1.5e-6] f(x) t "external laser"
plot [-50000:50000] [-1.5e-6:1.5e-6] f(x) lw 1.5 lc rgb "red" t "external laser at time $( printf "%s$time" $space) [a.u.]" 
replot
EOF

  chmod 777 gnuplot_script_$ii.sh

done
