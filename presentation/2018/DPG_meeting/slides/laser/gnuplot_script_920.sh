#!/usr/bin/gnuplot

set terminal png size 1024,250
set output "laser_920.png"
set title "external laser propagation"
#set xlabel "time [a.u.]"
set xlabel "[a.u.]
set ylabel "electric field [a.u.]"
set samples 5000
f(x)=1e-6*cos(.00082673490883947368*((x-147.20*137)--26600.0))*exp(-((x-147.20*137)--26600.0)**2/(2*7600.0**2))
#set pm3d at s
#plot [0:410] [-1.5e-6:1.5e-6] f(x) t "external laser"
plot [-50000:50000] [-1.5e-6:1.5e-6] f(x) lw 1.5 lc rgb "red" t "external laser at time 147.20 [a.u.]" 
replot
