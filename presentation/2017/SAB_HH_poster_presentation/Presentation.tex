\documentclass{beamer}
\usepackage[latin1]{inputenc}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{framed, color}
\usepackage{color}
\usepackage{braket}
\usepackage{extarrows}
\usepackage{booktabs}
\usepackage{array}
\usetheme{Warsaw}
\usecolortheme{crane}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% title

\title[]{ Time-depended Coupled Maxwell-Kohn-Sham simulation of a sodium dimer }
\author{Ren\'e Jest\"adt}
%\institute{Fritz-Haber-Institut der Max-Planck-Gesellschaft}
\date{ 9$^{th}$ February 2016}
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 1

\begin{frame}
\titlepage
%\begin{center}
% \includegraphics[width=3.3cm]{./Logo_Minerva_Max-Planck-Gesellschaft.pdf}
%\end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 2

\begin{frame}{Introduction}

  \begin{itemize}
    \item Maxwell's equations transformed in Matrix representation with Riemann-Silberstein vectors
    \item Time-evolution operator for the electromagnetic field similar to the matter time-evolution operator in quantum mechanics
    \item Coupled Maxwell-matter Hamiltonian in Coulomb gauge
    \item Testing model: Sodium dimer
  \end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Matrix representation of Maxwell's equations}

  \vspace{0.5cm}
  Riemann-Silberstein (R-S) vector in vacuum [1]:

  \vspace{-0.5cm}
  \begin{align*}
    \vec{F}(\vec{r},t) = \sqrt{\epsilon_0/2} \hspace{1ex} \vec{E}(\vec{r},t) + i \sqrt{1/(2\mu_0)} \hspace{1ex} \vec{B}(\vec{r},t)
  \end{align*}

  \vspace{0.5cm}
  Maxwell's equations in vacuum:

  \begin{minipage}[c]{1.0\textwidth}
    \hspace{-1.1cm}
    \begin{minipage}[c]{0.4\textwidth}
      \begin{align*}
        \vec{\nabla} \cdot  \vec{E}   &= \frac{1}{\epsilon_0} \rho      \\
        \vec{\nabla} \cdot  \vec{B}   &= 0
      \end{align*}
    \end{minipage}
    \hspace{2.0cm}
    \begin{minipage}[c]{0.5\textwidth}
      \begin{align*}          
        \vec{\nabla} \times \vec{E}  &= - \partial_t \vec{B}            \\
        \vec{\nabla} \times \vec{B}  &= \mu_0 \epsilon_0 \partial_t \vec{E} + \mu_0 \vec{j}
      \end{align*}
    \end{minipage}
  \end{minipage}

  \vspace{-2ex}
  \begin{align*}
    \hspace{-1.5cm} \Downarrow  \hspace{6.5cm} \Downarrow
  \end{align*}

   \vspace{-6ex}
   \begin{align*}
    \hspace{-0.2cm} 
    \vec{\nabla} \cdot \vec{F} = \frac{1}{\sqrt{2 \epsilon_0}} \rho   \hspace{1.8cm}
    i \partial_t \vec{F}(\vec{r},t) = \pm c_0 \vec{\nabla} \times \vec{F}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j}
  \end{align*}

  \footnotetext[1]{Bialynicki-Birula, Progress in Optics, Vol. XXXVI, 245-294}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Matrix representation of Maxwell's equations}

  Rotation operator represented by Dirac matrices:
  \vspace{-2ex}
  \begin{minipage}[c]{1.0\textwidth}
    \begin{align*}
      \vec{a} \times \vec {b} = - i (\vec{a} \cdot \vec{\bf{S}} ) \vec{b} \hspace{5ex}  with  \hspace{5ex}
      \vec{\bf{S}} = \Big( \bf{S}_{x} \hspace{1ex},\hspace{1ex} \bf{S}_{y} \hspace{1ex},\hspace{1ex} \bf{S}_{z} \Big)
    \end{align*}
  \end{minipage}

  \vspace{4ex}
  Corresponding Dirac matrices:
  \vspace{-1ex}
  \begin{align*}
           \bf{S}_{x} = \begin{bmatrix} 0 &  0 & 0 \\  0 & 0 & -i \\  0 & i & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{y} = \begin{bmatrix} 0 &  0 & i \\  0 & 0 &  0 \\ -i & 0 & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{z} = \begin{bmatrix} 0 & -i & 0 \\ -i & 0 &  0 \\  0 & 0 & 0 \end{bmatrix} 
  \end{align*}

  \begin{align*}
    \Rightarrow \vec{\nabla} \times \vec{F} = - i (\vec{\nabla} \cdot \vec{\bf{S}} ) \vec{F}
  \end{align*}

  \vspace{2ex}
  Maxwell's equations in a Schr\"odinger like R-S representation:
  \vspace{-1ex}
  \begin{align*}
    i \hbar \partial_t \hspace{1ex} \vec{F}(\vec{r},t) &= 
    \textcolor{blue}{ \underbrace {c_0 \Big( \vec{\bf{S}} \cdot \frac{\hbar}{i} \vec{\nabla} \Big) }_{\hat{\mathcal{H}}_{\rm{em}} } }
    \vec{F}(\vec{r},t) - \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j} 
  \end{align*}
 
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Time-evolution of the electromagnetic field}

  The time-evolution operator is equal to the one of an inhomogeneous Schr\"odinger equation:   \\[1cm]

  Time evolution operator without current density:
  \hspace{-0.3cm}
  \textcolor{blue}{
  \begin{align*}
    \hspace{-0ex}
    \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t) &= Exp \Big[ - \frac{i}{\hbar} \int_{t}^{t+\Delta t} \hat{\mathcal{H}}(\vec{r}) \Big]
                                           = Exp \Big[ - \frac{i}{\hbar} \hat{\mathcal{H}}(\vec{r}) \Delta t \Big]                  
  \end{align*}
  }

  \hspace{0.5cm}

  Time evolution operator with current density as inhomogeneity [1]:
  \hspace{-0.3cm}
  \small
  \textcolor{blue}{
  \begin{align*}
    \hat{U}_{\rm{em}}(t+\Delta t,t) &= \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t)
                                       \Big(F^{(0)}(\vec{r},t)   
                                     - \int_{t}^{t+\Delta t} \hspace{-2mm} d \tau \hat{U}_{\rm{em}}^{(0)}(\tau,t) \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j}(\vec{r},t) \Big)          
                          %          &\approx \hat{U}_{\rm{em}}^{(0)}(t+\Delta t,t) \mathcal{F}_{+}(\vec{r},t)                                                      \\
                          %          &- \frac{1}{2} \Big( J(\vec{r},t) + exp \Big[ \frac{i}{\hbar} \hat{\mathcal{H}}_{\rm{em}}^{(0)}(\vec{r}) \Delta t \Big] J(\vec{r},t+\Delta t) \Big)
  \end{align*}
  }
  \normalsize

  \footnotetext[1]{Joana Serban, Diploma thesis, FU-Berlin, (2004)}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

\begin{frame}

  \begin{align*}
    U(t + \Delta t, t) F(t) = U^{(0)}(t+\Delta t) (F(t)-\int_{t}^{t+\Delta t} \diff \tau U^{(0)}(\tau,t) j(t))  \\
    U(t + \Delta t, t) F(t) = U^{(0)}(t+\Delta t) (F(t)-\int_{t}^{t+\Delta t} \diff \tau U^{(0)}(t, \tau) j(\tau))  \\
    F(t) = U^{(0)}(t,0) \Psi_0 - \int_0^{t} \diff \tau U^{(0)}(t,\tau) j(\tau)
  \end{align*}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Kohn-Sham-Pauli Hamiltonian [2]}

  Kohn-Sham-Pauli Hamiltonian:
  \begin{align*}
    \hat{H}_{\mathrm{KSP}} = \hat{H}_{\mathrm{KS}} + \hat{H}_{\mathrm{int}}                                                          %      \label{eq_MKSc_Kohn_Sham_plus_int_Hamiltonian}
  \end{align*}

  Kohn-Sham Hamiltonian:
  \begin{align*}
    \hat{H}_{\mathrm{KS}}(\vec{r},t) = - \frac{\mathrm{\hbar}^2}{2 m} \vec{p}^2 + v_{\mathrm{KS}}(\vec{r},t)                         %      \label{eq_MKSc_Kohn_Sham_Hamiltonian}
  \end{align*}

  Interaction Hamiltonian for the Maxwell field:
  \begin{align*}
   \hat{H}_{\mathrm{int}} = - \frac{e}{m c} \vec{p} \cdot \vec{A} + \frac{e^2}{2 m c^2} \vec{A}^2 + e \phi
                            - \frac{e \mathrm{\hbar}}{2 m c} \sigma^{k} \Big[ \nabla \times \vec{A} \Big]_{k}                        %      \label{eq_MKSc_interaction_Hamiltonian}
  \end{align*}

%  \begin{align*}
%      \hat{H}_{\mathrm{KSP}}(\vec{r},\vec{R},t) \phi_{i}(\vec{r},t) = \epsilon_{i}(\vec{r},t) \phi_{i}(\vec{r},t)                    %      \label{eq_MKSc_Kohn_Sham_Pauli_equation}
%  \end{align*}

  Power-Zienau-Woolley transformation:
  \begin{align*}
    \hat{H}_{\mathrm{KSP}}' = \hat{U}_{\mathrm{PZW}}^{\dagger} \hat{H}_{\mathrm{KSP}} \hat{U}_{\mathrm{PZW}}                         %      \label{eq_MKSc_PZW_trans_of_KSP_Hamiltonian}
  \end{align*}

  \begin{align*}
    \hat{U}_{\mathrm{PZW}}  = exp \Bigg( \frac{\mathrm{i}}{\mathrm{\hbar}} \int \vec{P}_{\perp}(\vec{r}) \cdot \vec{A}(\vec{r}) \Bigg) \mathrm{d} \vec{r} 
  \end{align*}

  \footnotetext[2]{A. Komech, Quantum Mechanics Genesis and Achievements, 2013}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Multipole expansion of the Hamiltonian}

  Multipole expansion of Kohn-Sham-Paule equation [3]:
  \begin{align*}
    \hat{H}_{\mathrm{KSP}}' = \hat{H}_{\mathrm{KS}} + \hat{H}_{\mathrm{ed}} + \hat{H}_{\mathrm{md}} + \hat{H}_{\mathrm{eq}} + ...    %      \label{eq_MKSc_PZW_trans_of_KSP_Hamiltonian_full}
  \end{align*}

  Electric dipole Hamiltonian:
  \begin{align*}
    \hat{H}_{\mathrm{ed}} = \vec{r} \cdot \vec{E}_{\perp}(\vec{R},t)                                                                 %      \label{eq_MKSc_electric_dipole_Hamiltonian_term}
  \end{align*}

  Magnetic dipole Hamiltonian:
  \begin{align*}
    \hat{H}_{\mathrm{md}}(\vec{r},\vec{R},t) &= - \mathrm{i} \frac{e \hbar}{2m} \vec{r}(t) \times \vec{B}(\vec{R},t) \cdot \vec{\nabla}   %   \label{eq_MKSc_magnetic_dipole_Hamiltonian_term}
  \end{align*}

  Electric quadrupole Hamiltonian:
  \begin{align*}
    \hat{H}_{\mathrm{eq}}(\vec{r},\vec{R},t) &= - \frac{1}{2} e \Big( \vec{r}(t) \cdot \vec{\nabla} \Big) 
                                                  \Big( \vec{r}(t) \cdot \vec{E}_{\perp}(\vec{R},t) \Big)                           %      \label{eq_MKSc_electric_quadrupole_Hamiltonian_term}
  \end{align*}

  \footnotetext[3]{R. Loudon, The Quantum Theory of Light, 1983 }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Wave function and electromagnetic field time evolution}

  Time evolution operator for the matter:
  \small
  \textcolor{red}{
  \begin{align*}
    \hat{U}_{\rm{ma}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t,t)_{\vec{F}(t)}^{\vec{F}(t + \Delta t)} & \approx 
    Exp \Big[ \hspace{-1mm} - \hspace{-1mm} \frac{i}{\hbar} \Delta t 
    \Big( \hat{H}_{\rm{KSP}}' \Big) \Big]   
  \end{align*}
  }

  \normalsize
  Time evolution operator for the electromagnetic field:

  \small
  \textcolor{blue}{
  \begin{align*}
    \hat{U}_{\rm{em}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t,t)_{\vec{J}(t)}^{\vec{J}(t + \Delta t)} 
    &   \approx Exp \Big[ - \frac{i}{\hbar} \hat{\mathcal{H}}_{\rm{em}}(\vec{r}) \Delta t \Big] \Big[ \mathcal{F}(\vec{r},t)         \\
    & - \frac{1}{2} \Big( J(\vec{r},t) + Exp \Big[ \frac{i}{\hbar} \hat{\mathcal{H}}_{\rm{em}}(\vec{r}) \Delta t \Big] J(\vec{r},t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) \Big) \Big] 
  \end{align*}
  }

  \normalsize
  Enforced time-reversed symmetry propagation:

  \small
  \textcolor{red}{
  \begin{align*}
  \hspace{-0.5cm}
      \hat{U}_{\rm{ma}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t/2,t)_{\vec{F}(t)}^{\vec{F}(t + \Delta t/2)} \psi(\vec{r}, t) 
    = \hat{U}_{\rm{ma}}(t + \Delta t/2,t + \Delta t)_{\vec{F}(t + \Delta t)}^{\vec{F}(t + \Delta t/2)} \psi(\vec{r}, t + \Delta t)
  \end{align*}
  }

  \small
  \textcolor{blue}{
  \begin{align*}
  \hspace{-0.5cm}
      \hat{U}_{\rm{em}}(t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t/2,t)_{j(t)}^{j(t + \Delta t/2)} \vec{F}(\vec{r}, t) 
    = \hat{U}_{\rm{em}}(t + \Delta t/2,t + \Delta t)_{j(t + \Delta t)}^{j(t + \Delta t/2)} \vec{F}(\vec{r}, t + \Delta t)
  \end{align*}
  }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 

\begin{frame}{Predictor corrector method}

  \vspace{-0.3cm}
  \begin{align*}
    t' = t_0 + \Delta t  \hspace{1cm} t'' = t_0 + 2 \Delta t
  \end{align*}
 
  \vspace{-1.2cm}
    \includegraphics[trim=6.0cm 0cm 2cm 3cm]{./Predictor_corrector_illustration/Predictor_corrector_illustration_2.pdf} 

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Sodium nanoparticle dimer [1]}

  \begin{itemize}
    \item One valence electron per Sodium ion
    \item 297 ions and valence electrons per Sodium monomer, 594 ions and valence elcetrons in total
    \item Electric field is polarized in x direction 
  \end{itemize}

  \begin{center}
    \includegraphics[scale=0.2]{./Sodium_dimer.pdf}
  \end{center}

  \footnotetext[1]{A. Varas, J. Phys. Chem. Lett., 2015, 6 (10), pp 1891–1898}

\end{frame}


\end{document}
