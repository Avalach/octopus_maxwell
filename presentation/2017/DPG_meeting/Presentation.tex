\documentclass{beamer}
\usepackage[latin1]{inputenc}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{framed, color}
\usepackage{color}
\usepackage{braket}
\usepackage{extarrows}
\usepackage{booktabs}
\usepackage{array}
\usetheme{Warsaw}
\usecolortheme{crane}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% title

\title[]{Real-time propagation of a $ Na_{297} $ dimer as a coupled Maxwell-Schr\"odinger and time-dependent Kohn-Sham-Maxwell system}
\author{\underline{Ren\'e Jest\"adt}, Heiko Appel and Angel Rubio}
%\institute{Fritz-Haber-Institut der Max-Planck-Gesellschaft}
\institute{Max-Planck-Institut f\"ur Struktur und Dynamik der Materie}
\date{ 7$^{th}$ March 2017}
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 1

\begin{frame}
\titlepage
%\begin{center}
% \includegraphics[width=3.3cm]{./Logo_Minerva_Max-Planck-Gesellschaft.pdf}
%\end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 2

\begin{frame}{Motivation and introduction}

  Motivation:
  \begin{itemize}
    \item Back-reaction of the matter motion to external electromagnetic fields is not taken into account in conventional calculations
    \item For large molecules or periodic systems the induced electric current density should produce non-negligible effects
  \end{itemize}

  \vspace{0.5cm}
  Introduction:
  \begin{itemize}
    \item Maxwell's equations transformed to matrix representation with Riemann-Silberstein vectors
    \item Time-evolution operator for the electromagnetic field similar to the matter time-evolution operator in quantum mechanics
    \item Coupled Maxwell-matter Hamiltonian in Coulomb gauge
    \item Simulation model: sodium dimer
  \end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Matrix representation of Maxwell's equations}

  \vspace{0.5cm}
  Riemann-Silberstein (R-S) vector in vacuum [1]:

  \vspace{-0.5cm}
  \begin{align*}
    \vec{F}(\vec{r},t) = \sqrt{\epsilon_0/2} \hspace{1ex} \vec{E}(\vec{r},t) + i \sqrt{1/(2\mu_0)} \hspace{1ex} \vec{B}(\vec{r},t)
  \end{align*}

  \vspace{0.5cm}
  Maxwell's equations in vacuum:

  \begin{minipage}[c]{1.0\textwidth}
    \hspace{-1.1cm}
    \begin{minipage}[c]{0.4\textwidth}
      \begin{align*}
        \vec{\nabla} \cdot  \vec{E}   &= \frac{1}{\epsilon_0} \rho      \\
        \vec{\nabla} \cdot  \vec{B}   &= 0
      \end{align*}
    \end{minipage}
    \hspace{2.0cm}
    \begin{minipage}[c]{0.5\textwidth}
      \begin{align*}          
        \vec{\nabla} \times \vec{E}  &= - \partial_t \vec{B}            \\
        \vec{\nabla} \times \vec{B}  &= \mu_0 \epsilon_0 \partial_t \vec{E} + \mu_0 \vec{j}
      \end{align*}
    \end{minipage}
  \end{minipage}

  \vspace{-2ex}
  \begin{align*}
    \hspace{-1.5cm} \Downarrow  \hspace{6.5cm} \Downarrow
  \end{align*}

   \vspace{-6ex}
   \begin{align*}
    \hspace{-0.2cm} 
    \vec{\nabla} \cdot \vec{F} = \frac{1}{\sqrt{2 \epsilon_0}} \rho   \hspace{1.8cm}
    i \partial_t \vec{F}(\vec{r},t) = \pm c_0 \vec{\nabla} \times \vec{F}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j}
  \end{align*}

  \footnotetext[1]{Bialynicki-Birula, Progress in Optics, Vol. XXXVI, 245-294}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Matrix representation of Maxwell's equations}

  Curl of two vectors represented by Dirac matrices:
  \vspace{-2ex}
  \begin{minipage}[c]{1.0\textwidth}
    \begin{align*}
      \vec{a} \times \vec {b} = - i (\vec{a} \cdot \vec{\bf{S}} ) \vec{b} \hspace{5ex}  with  \hspace{5ex}
      \vec{\bf{S}} = \Big( \bf{S}_{x} \hspace{1ex},\hspace{1ex} \bf{S}_{y} \hspace{1ex},\hspace{1ex} \bf{S}_{z} \Big)
    \end{align*}
  \end{minipage}

  \vspace{4ex}
  Corresponding Dirac matrices:
  \vspace{-1ex}
  \begin{align*}
           \bf{S}_{x} = \begin{bmatrix} 0 &  0 & 0 \\  0 & 0 & -i \\  0 & i & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{y} = \begin{bmatrix} 0 &  0 & i \\  0 & 0 &  0 \\ -i & 0 & 0 \end{bmatrix} \hspace{3ex}
           \bf{S}_{z} = \begin{bmatrix} 0 & -i & 0 \\ -i & 0 &  0 \\  0 & 0 & 0 \end{bmatrix} 
  \end{align*}

  \begin{align*}
    \Rightarrow \vec{\nabla} \times \vec{F} = - i (\vec{\nabla} \cdot \vec{\bf{S}} ) \vec{F}
  \end{align*}

  \vspace{2ex}
  Maxwell's equations in a Schr\"odinger like R-S representation:
  \vspace{-1ex}
  \begin{align*}
    i \hbar \partial_t \hspace{1ex} \vec{F}(\vec{r},t) &= 
    \textcolor{blue}{ \underbrace {c_0  \frac{\hbar}{\mathrm{i}} \Big( \vec{\bf{S}} \cdot \vec{\nabla} \Big) }_{\hat{H}_{\rm{Mx}} } }
    \vec{F}(\vec{r},t) - \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j} 
  \end{align*}
 
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Time-evolution of the electromagnetic field}

  The time-evolution operator is equal to the one of an inhomogeneous Schr\"odinger equation:   \\[0.5cm]

  Time evolution operator without current density:

  \vspace{-3ex}
  \textcolor{blue}{
  \begin{align*}
    \hspace{-19ex}
    \vec{F}(\vec{r},t+\Delta t) &= \hat{U}_{\rm{Mx}}^{\rm{hom}}(t+\Delta t,t) \vec{F}(\vec{r},t)
  \end{align*}
  }
  \vspace{-5ex}
  \textcolor{blue}{
  \begin{align*}
    \hspace{-2ex}
    \hat{U}_{\rm{Mx}}^{\rm{hom}}(t+\Delta t,t) &= Exp \Big[ - \frac{i}{\hbar} \int_{t}^{t+\Delta t} \hat{H}_{\rm{Mx}} \Big]
                                                = Exp \Big[ - \frac{i}{\hbar} \hat{H}_{\rm{Mx}} \Delta t \Big]                  
  \end{align*}
  }

  \hspace{0.5cm}

  Time evolution with current density as inhomogeneity [1]:
  \hspace{-0.3cm}
  \small
  \textcolor{blue}{
  \begin{align*}
    \vec{F}(\vec{r},t+\Delta t) &= \hat{U}_{\rm{Mx}}^{\rm{hom}}(t+\Delta t,t)
                                       \Big( F(\vec{r},t)   
                                     - \int_{t}^{t+\Delta t} \hspace{-2mm} d \tau \hat{U}_{\rm{Mx}}^{\rm{hom}}(t,\tau) \frac{i \hbar}{\sqrt{2 \epsilon_0}} \vec{j}(\vec{r},t) \Big)
  \end{align*}
  }
  \normalsize

  \footnotetext[1]{Ioana Serban, Diploma thesis, FU-Berlin, (2004)}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

%\begin{frame}{Time-evolution of the electromagnetic field}
%
%  Calculation of the electric field in z-direction at point (5 5 0) for an external current density 
%  with Meep (MIT Electromagnetic Equation Propagation) and Octopus
%
%  \vspace{-0.5cm}
%  \begin{align*}
%    \vec{j}(\vec{r},t) = \vec{e}_z \hspace{1.0mm} j \hspace{1.0mm}  exp(-x^2/2-y^2/2) exp(-(t-5)^2)
%  \end{align*}
%
%  \centering
%  \vspace{-0.3cm}
%  \includegraphics[scale=0.20]{./octopus_and_meep_reference_gaussian_broadened_current.png}
%
%  \tiny
%  meep units: $ \epsilon_0 = \mu_0 = c_0 = 1 $
%
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Kohn-Sham Hamiltonian}

  Kohn-Sham Hamiltonian:
  \textcolor{red}{
  \begin{align*}
    \hat{H}_{\mathrm{KS}}(\vec{r},t) = - \frac{\mathrm{\hbar}^2}{2 m} \vec{p}^2 + v_{\mathrm{KS}}(\vec{r},t)                         %      \label{eq_MKSc_Kohn_Sham_Hamiltonian}
  \end{align*}
  }

  Interaction Hamiltonian for the Maxwell field:
  \textcolor{red}{
  \begin{align*}
   \hat{H}_{\mathrm{int}} = - \frac{e}{m c} \vec{p} \cdot \vec{A} + \frac{e^2}{2 m c^2} \vec{A}^2 + e \phi
                            - \frac{e \mathrm{\hbar}}{2 m c} \sigma^{k} \Big[ \nabla \times \vec{A} \Big]_{k}                        %      \label{eq_MKSc_interaction_Hamiltonian}
  \end{align*}
  }

  Power-Zienau-Woolley transformation:
  \textcolor{red}{
  \begin{align*}
    \hat{H}_{\mathrm{KS}+int}' = \hat{U}_{\mathrm{PZW}}^{\dagger} \big( \hat{H}_{\mathrm{KS}} + \hat{H}_{\mathrm{int}} \big) \hat{U}_{\mathrm{PZW}}  %  \label{eq_MKSc_PZW_trans_of_KSP_Hamiltonian}
  \end{align*}
  }

  \vspace{-6ex}
  \textcolor{red}{
  \begin{align*}
    \hat{U}_{\mathrm{PZW}}  = exp \Bigg( \frac{\mathrm{i}}{\mathrm{\hbar}} \int \vec{P}_{\perp}(\vec{r}) \cdot \vec{A}(\vec{r}) \Bigg) \mathrm{d} \vec{r} 
  \end{align*}
  }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Multipole expansion of the Hamiltonian}

  Multipole expansion for light-matter interaction [3]:
  \textcolor{red}{
  \begin{align*}
    \hat{H}_{\mathrm{KS+int}}' = \hat{H}_{\mathrm{KS}} + \hat{H}_{\mathrm{ed}} + \hat{H}_{\mathrm{md}} + \hat{H}_{\mathrm{eq}} + ...    %      \label{eq_MKSc_PZW_trans_of_KSP_Hamiltonian_full}
  \end{align*}
  }

  Electric dipole Hamiltonian:
  \textcolor{red}{
  \begin{align*}
    \hat{H}_{\mathrm{ed}} = \vec{r} \cdot \vec{E}_{\perp}(\vec{R},t)                                                                 %      \label{eq_MKSc_electric_dipole_Hamiltonian_term}
  \end{align*}
  }

  Magnetic dipole Hamiltonian:
  \textcolor{red}{
  \begin{align*}
    \hat{H}_{\mathrm{md}}(\vec{r},\vec{R},t) &= - \mathrm{i} \frac{e \hbar}{2m} \vec{r}(t) \times \vec{B}(\vec{R},t) \cdot \vec{\nabla}   %  \label{eq_MKSc_magnetic_dipole_Hamiltonian_term}
  \end{align*}
  }

  Electric quadrupole Hamiltonian:
  \textcolor{red}{
  \begin{align*}
    \hat{H}_{\mathrm{eq}}(\vec{r},\vec{R},t) &= - \frac{1}{2} e \Big( \vec{r}(t) \cdot \vec{\nabla} \Big) 
                                                  \Big( \vec{r}(t) \cdot \vec{E}_{\perp}(\vec{R},t) \Big)                      %      \label{eq_MKSc_electric_quadrupole_Hamiltonian_term}
  \end{align*}
  }

  \footnotetext[3]{R. Loudon, The Quantum Theory of Light, 1983 }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Wave function and electromagnetic field time evolution}

  Time evolution operator for the matter:
  \small
  \textcolor{red}{
  \begin{align*}
    \phi_i(\vec{r},t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) & \approx 
    Exp \Big[ \hspace{-1mm} - \hspace{-1mm} \frac{i}{\hbar} \Delta t 
    \Big( \hat{H}_{\rm{KS+int}}'(t) \Big) \Big] \phi_i(\vec{r},t)
  \end{align*}
  }

  \normalsize
  Time evolution operator for the electromagnetic field:

  \small
  \textcolor{blue}{
  \begin{align*}
    \vec{F}(\vec{r}, t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) 
    &   \approx Exp \Big[ - \frac{i}{\hbar} \hat{H}_{\rm{Mx}} \Delta t \Big] \vec{F}(\vec{r},t)         \\
    & - \frac{1}{2} \Big( Exp \Big[ - \frac{i}{\hbar} \hat{H}_{\rm{Mx}} \Delta t \Big] \vec{j}(\vec{r},t)
      + \vec{j}(\vec{r},t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) \Big) 
  \end{align*}
  }

  \normalsize
  Enforced time-reversal symmetry propagation:

  \small
  \textcolor{red}{
  \begin{align*}
  \hspace{-0.5cm}
    \phi_i(\vec{r},t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) & \approx 
    \hat{U}_{\rm{ma}}(t + \Delta t/2,t + \Delta t) \psi(\vec{r}, t + \Delta t)
  \end{align*}
  }

  \small
  \textcolor{blue}{
  \begin{align*}
  \hspace{-0.5cm}
    \vec{F}(\vec{r}, t \hspace{-0.5mm} + \hspace{-0.5mm} \Delta t) 
    & \approx \hat{U}_{\rm{em}}(t + \Delta t/2,t + \Delta t)_{j(t + \Delta t)}^{j(t + \Delta t/2)} \vec{F}(\vec{r}, t + \Delta t)
  \end{align*}
  }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 

\begin{frame}{Predictor corrector method}

  \includegraphics[scale=0.5]{./predictor_corrector_method/predictor_corrector_full_coupling.pdf} 

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

%\begin{frame}{Multiscale systems}
%
%  \includegraphics[scale=0.22]{Multiscale_system_equal_grid.png}
%  \includegraphics[scale=0.22]{Multiscale_system.png}
%
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie 

%\begin{frame}{Simulation scheme}
%
%  \includegraphics[scale=0.4]{Boundary_scheme.pdf}
%
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Sodium nanoparticle dimer}

  \begin{itemize}
    \item One valence electron per sodium ion
    \item 297 ions and valence electrons per sodium monomer, 594 ions and valence electrons in total
    \item Electric field is polarized in z direction 
  \end{itemize}

  \begin{center}
    \includegraphics[scale=0.35]{./Sodium_dimer_2.png}
  \end{center}

 % \footnotetext{A. Varas, J. Phys. Chem. Lett., 2015, 6 (10), pp 1891–1898}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{Na$_{297}$ dimer hit by a gaussian laser pulse}
  %The electric field amplitude of the incident laser is 1e-6 a.u.:
  1e-6 a.u. electric field amplitude for the incident laser pulse:
  \includegraphics[scale=0.27]{./Na_297_dimer_electric_field.png}
  %1e-6 a.u. electric field amplitude of the incident laser
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie

\begin{frame}{conclusion}

  Conclusion:
  \begin{itemize}
    \item In nanoparticle systems a large induced current density
          causes a significant electromagnetic field enhancement
    \item The arising induced electromagnetic field occurs mainly in the near-field region of
          the nanoparticle 
  \end{itemize}

  \vspace{0.5cm}
  \begin{center}
    \huge{Thank you for your attention!}
  \end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie
%
%\begin{frame}[fragile]{maxwell\_ks input file}
%
%  Calculation mode and variables for parallelization and Maxwell-matter system:
%
%  \TINY
%  \begin{verbatim}
% CalculationMode                   = maxwell_ks
% ParDomains                        = auto
% ParStates                         = no
% MaxwellParDomains                 = auto
% MaxwellParStates                  = no
%
%
%# ----- Matter box variables ----------------------------------------------------------------------
%
% Dimensions                        = 3
% BoxShape                          = PARALLELEPIPED
%
% %Lsize
%  000070.000 | 000070.000 | 000070.000
% %
%
% %Spacing
%  000001.000 | 000001.000 | 000001.000
% %
%
%
%# ----- Maxwell box variables ---------------------------------------------------------------------
%
% MaxwellDimensions                 = 3
% MaxwellBoxShape                   = PARALLELEPIPED
%
% %MaxwellLsize
%  000070.000 | 000070.000 | 000070.000
% %
% 
% %MaxwellSpacing
%  000001.000 | 000001.000 | 000001.000
% %
%  \end{verbatim}
%
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Folie
%
%\begin{frame}[fragile]{maxwell\_ks input file}
%
%  Variables for Maxwell propagation, Maxwell-matter coupling and output:
%
%  \TINY
%  \begin{verbatim}
%# ----- Maxwell calculation variables -------------------------------------------------------------
%
% MaxwellHamiltonianOperator        = spin_matrix
% MaxwellTDOperatorMethod           = maxwell_op_fd
% MaxwellTDPropagator               = maxwell_etrs
%
% MatterToMaxwellCoupling           = yes
% MaxwellToMatterCoupling           = yes
% MaxwellCouplingOrder              = electric_dipole_coupling + magnetic_dipole_coupling + electric_quadrupole_coupling 
%
% MaxwellTDExpOrder                 = 4
% MaxwellDerivativesStencil         = stencil_starplus
% MaxwellDerivativesOrder           = 8
%
% MaxwellABWidth                    = 4.000
% MaxwellAbsorbingBoundaries        = mask
%
%
%# ----- Maxwell output variables ------------------------------------------------------------------
%
% MaxwellOutput                     = electric_field + magnetic_field + maxwell_energy_density + poynting_vector
% MaxwellOutputInterval             = 40
% MaxwellTDOutput                   = maxwell_energy + maxwell_fields + maxwell_current%
%
%
%# ----- Time step variables -----------------------------------------------------------------------
%
% TDTimeStep                        = 0.1685240
% TDMaxSteps                        = 11867
% TDEnergyUpdateIter                = 1
% MaxwellTDIntervalSteps            = 40
% MaxwellTDETRSApprox               = no
%
%  \end{verbatim}
%
%
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5%
% Folie
%
%\begin{frame}[fragile]{maxwell\_ks input file}\
%
%  Input variables for Maxwell incident waves and Maxwell states in the simulation box:
%
%  \TINY
%  \begin{verbatim}
%# ----- Maxwell field variables -------------------------------------------------------------------
%
% pi=3.14159265359
% c=137.035999679
%
%
%# ----- laser propagates in x direction ---------
%
% lambda      =  7687.7000
% kx          =  0.000817303655
% Ez          =  0.000001000000
% pulse_width = 23063.1000
% pulse_shift = -78798.9000
%
% %UserDefinedMaxwellIncidentWaves
%   incident_wave_mx_function | 0 | 0 | Ez | "wave_function" | plane_wave
% %
%
% %MaxwellFunctions
%  "wave_function" | mxf_gaussian_wave | kx | 0 | 0 | pulse_shift | 0 | 0 | pulse_width 
% %
%
%  # gaussian wave
% %UserDefinedMaxwellStates
%  3 | formula | electric_field |        " Ez * cos( kx * (x-pulse_shift) ) * exp( -((x-pulse_shift)^2/(2*pulse_width^2)) ) "
%  2 | formula | magnetic_field | " -1/c * Ez * cos( kx * (x-pulse_shift) ) * exp( -((x-pulse_shift)^2/(2*pulse_width^2)) ) "
% %
%  \end{verbatim}
%
%\end{frame}


\end{document}
