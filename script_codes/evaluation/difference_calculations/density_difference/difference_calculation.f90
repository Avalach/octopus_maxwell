program maxwell_field_difference

! Beispiel zum Einlesen von Datensaetzen aus Dateien
! mit Fehlererkennung

implicit none


character(len=200) :: filename_1, filename_2, filename_out, line_1, line_2
integer :: number_lines_1, number_lines_2, skipped_lines_1, skipped_lines_2           
                                                           
integer :: status_open, status_open_1, status_open_1s, status_open_2, status_open_2s
integer :: status_read, status_read_1, status_read_1s, status_read_2, status_read_2s
integer :: status_write, free_line_number

real(8) :: x_1, y_1, z_1, x_2, y_2, z_2, epsilon

epsilon = 1E-10


open(unit=15, file='filename_list', status='old', action='read', iostat=status_open)

if (status_open == 0) then
  do

    number_lines_1  = 0
    number_lines_2  = 0
    skipped_lines_1 = 0
    skipped_lines_2 = 0
    read(15,*,iostat=status_read) filename_1, filename_2, filename_out
    write(*,*) 'filename_1   = ', filename_1
    write(*,*) 'filename_2   = ', filename_2
    write(*,*) 'filename_out = ', filename_out

    ! OEffnen der Dateien mit Abfangen von I/O-Fehlern
    open(unit=20, file=filename_1, status='old', action='read', iostat=status_open_1)
    open(unit=25, file=filename_2, status='old', action='read', iostat=status_open_2)
    open(unit=30, file=filename_out, status='new', action='write', iostat=status_write)

    if ( (status_open_1 == 0) .and. (status_open_2 == 0) ) then
      do
        read (20,'(a)',iostat=status_read_1s) line_1
        read (25,'(a)',iostat=status_read_2s) line_2

        if ((status_read_1s == 0) .and. (status_read_2s ==0)) then
          if ((line_1 == "") .and. (line_2 == "")) then
            skipped_lines_1 = skipped_lines_1 + 1
            skipped_lines_2 = skipped_lines_2 + 1
            write(30,'(a)') ""
          else
            read (20,*,iostat=status_read_1) x_1, y_1, z_1   ! Einlesen des Wertes
            read (25,*,iostat=status_read_2) x_2, y_2, z_2   ! Einlesen des Wertes
            if (status_read_1 == 0) then         
              number_lines_1 = number_lines_1 + 1
            end if
            if (status_read_2 == 0) then
              number_lines_2 = number_lines_2 + 1
            end if
            if (status_read_2 > 0) then
            end if
            if (number_lines_1 == number_lines_2) then
              if (status_write ==0) then 
                if ( (abs(x_1-x_2) < epsilon) .and. (abs(y_1-y_2) < epsilon) ) then
                  write(30,*) x_1, y_1, z_1 - z_2
                else
                  write(*,*) (abs(x_1-x_2) < epsilon), (abs(y_1-y_2) < epsilon), epsilon
                  write(*,*) 'FATAL ERROR: X and Y values of both files do not correspond!'
                  write(*,*) 'x_1 - x_2 = ', abs(x_1-x_2), 'y_1 - y_2 = ', abs(y_1-y_2)
                  exit
                end if
              else
                write(*,*) 'FATAL ERROR: The values are not written correctley into the file!'
                exit
              end if
            else
              write(*,*) 'FATAL ERROR: Line number of both files do not correspond!'
              write(*,*) 'number_lines_1 = ', number_lines_1, 'number_lines_2 = ', number_lines_2
              exit
            end if
            if ((status_read_1 < 0) .or. (status_read_2 < 0)) then
              write(*,*) 'END OF FILE:' 
              write(*,*) 'Number of lines 1 and 2        : ', number_lines_1, number_lines_2
              write(*,*) 'Number of skipped lines 1 and 2: ', skipped_lines_1, skipped_lines_2
              exit
            end if
          end if
        end if

        if ((status_read_1s < 0) .or. (status_read_2s < 0)) then
          write(*,*) 'END OF FILE:' 
          write(*,*) 'Number of lines 1 and 2        : ', number_lines_1, number_lines_2
          write(*,*) 'Number of skipped lines 1 and 2: ', skipped_lines_1, skipped_lines_2
          exit
        end if

      end do
    else 
      write(*,*) 'Beim Oeffnen der Datei trat Systemfehler Nr. ', status_open_1, status_open_2,' auf'
    end if

    close(unit=20) 
    close(unit=25)
    close(unit=30)
    write(*,*) 'All three io-units are closed.'
    write(*,*)

    if (status_read < 0) then
      exit
    end if

  end do

end if

close(unit=15)

end program maxwell_field_difference
