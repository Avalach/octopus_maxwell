#!/bin/bash

rm filename_list

count=0

for ii in ../output_iter/td.000*/density.z\=0 
do
  if [ $count -ne 0 ] 
  then
    jj=$(printf '%07d' "$count")  
    echo \'$ii\'  "     '../output_iter/td.0000000/density.z=0'     '../output_iter/td.$jj/density_difference.z=0'  "  >>  filename_list
  fi
  count=$(( $count+1 ))
done
