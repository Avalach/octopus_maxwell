#!/bin/bash

rm filename_list

while read run_dir
do

  run_dir_1=$( echo $run_dir | awk -F" " '{print $1}')
  run_dir_2=$( echo $run_dir | awk -F" " '{print $2}')
  count=0
  for ii in $run_dir_1/output_iter/td.000*/density.z=0 
  do
    if [ $count -ne 0 ] 
    then
      jj=$(printf '%07d' "$count")
      ii_2=$run_dir_2/output_iter/td.0000000/density.z=0
      ii_3=$run_dir_1/output_iter/td.$jj/density_difference.z=0
      echo \'$ii\'  "     '$ii_2'     '$ii_3'  "  >> filename_list
    fi
    count=$(( $count+1 ))
  done

done < run_dir_list

