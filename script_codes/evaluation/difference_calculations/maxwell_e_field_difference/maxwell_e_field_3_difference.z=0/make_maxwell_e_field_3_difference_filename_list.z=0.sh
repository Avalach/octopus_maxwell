#!/bin/bash

rm filename_list

while read run_dir
do

  run_dir_1=$( echo $run_dir | awk -F" " '{print $1}')
  run_dir_2=$( echo $run_dir | awk -F" " '{print $2}')
  count=0
  for ii in $run_dir_1/output_iter/td.000*/maxwell_e_field_3.z\=0 
  do
    if [ $count -ne 0 ] 
    then
      jj=$(printf '%07d' "$count")  
      echo \'$ii\'  "     '$run_dir_2/output_iter/td.$jj/maxwell_e_field_3.z=0'     '$run_dir_1/output_iter/td.$jj/maxwell_e_field_3_difference.z=0'  "  >> filename_list
    fi
    count=$(( $count+1 ))
  done

done < run_dir_list
