#!/bin/bash

var_1_ref="Calculation started on"
var_2_ref="Info: Finished writing Maxwell states."
var_3_ref="Info: Number of nodes in par_domains group:"
count=0

rm core_scaling.dat

while read -r output_file
do

  file_no=$( echo $file_no + 1 | bc)
  elapsed_time_dat_file=elapsed_time_"$(printf '%04d' "$file_no")".dat
  rm $elapsed_time_dat_file

  echo ""
  echo "Evaluation file: $output_file"
  echo ""

  while read -r line
  do
    count=$((count+1)) 

    var_1=$(echo $line | awk '{ print $1 " " $2 " " $3 }')
    var_2=$(echo $line | awk '{ print $1 " " $2 " " $3 " " $4 " " $5 }')
    var_3=$(echo $line | awk '{ print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7 }')

    if [ "$var_1" == "$var_1_ref" ]
    then 
      echo "Found start time:"
      var_start_date=$(echo $line | awk '{ print $4 }')
      var_start_time=$(echo $line | awk '{ print $6 }')
      var_start_h=$(echo $var_start_time | awk -F':' '{print $1 }')
      var_start_min=$(echo $var_start_time | awk -F':' '{print $2 }')
      var_start_sec=$(echo $var_start_time | awk -F':' '{print $3 }')
      var_start_seconds=$(echo "3600*$var_start_h + 60*$var_start_min + $var_start_sec" | bc -l)
      echo $var_start_date "  " $var_start_time "  " $var_start_seconds
      echo ""
    fi

    if [ "$var_2" == "$var_2_ref" ]
    then
      echo "Found calculation step time:"
      var_end_date=$(echo $line | awk '{ print $6 }')
      var_end_time=$(echo $line | awk '{ print $8 }')
      var_end_h=$(echo $var_end_time | awk -F':' '{print $1 }')
      var_end_min=$(echo $var_end_time | awk -F':' '{print $2 }')
      var_end_sec=$(echo $var_end_time | awk -F':' '{print $3 }')
      var_end_seconds=$(echo "3600*$var_end_h + 60*$var_end_min + $var_end_sec" | bc -l)
      echo $var_end_date "  " $var_end_time "  " $var_end_seconds
    fi

    if [ "$var_3" == "$var_3_ref" ]
    then
      echo "Found core number:"
      var_core_number=$(echo $line | awk '{ print $8 }')
      echo $var_core_number
      echo ""
    fi

  done < $output_file

  echo ""
  if [ $var_end_seconds -gt $var_start_seconds ] 
  then
    var_calc_time=$(echo " $var_start_seconds - $var_end_seconds " | bc -l)
  else
    var_calc_time=$(echo " $var_end_seconds - $var_start_seconds " | bc -l)
  fi
  echo "N number of cores and elapsed time for running the program:"
  echo $var_core_number " " $var_calc_time
  echo ""
  echo "Writing core number N and elapsed time in core_scaling.dat"
  echo $var_core_number " " $var_calc_time >> core_scaling.dat
  echo ""
  echo ""

done < octopus_output_file_list


