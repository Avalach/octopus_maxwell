#!/bin/bash

count=0
while read -r line
do
    count=$((count+1)) 
  if [ $count -eq 5 ] 
  then
       awk '$line { print $2 " " $3 " " $5 " " $6 }' > energy_list.dat ;
  fi
done < ./td.general/energy
