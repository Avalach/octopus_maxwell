!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! $Id: space.F90 6963 2010-08-25 09:10:42Z xavier $

#include "global.h"

module mx_densities_m

  use datasets_m
  use global_m
  use messages_m
  use mpi_m
  use mx_geometry_m
  use mx_space_m
  use octcl_kernel_m
  use opencl_m
  use parser_m 
  use profiling_m

  implicit none


contains

  subroutine mx_densities(mx_gr)

  

  end subroutine mx_densities


end module mx_densities_m
