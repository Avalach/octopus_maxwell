#define VALUES_OUTPUT_GEO                                                                                                      \
    write(*,*) '======================================================================================'  ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '---------------------------------------------------------------'                         ; _newline_           \
    write(*,*) 'mx_sys%space values:'                                                                    ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%space%dim:                  ', mx_sys%space%dim                                   ; _newline_           \
    write(*,*) '---------------------------------------------------------------'                         ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '---------------------------------------------------------------'                         ; _newline_           \
    write(*,*) 'mx_sys$geo values:'                                                                      ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%geo%natoms:                 ', mx_sys%geo%natoms                                  ; _newline_           \
    write(*,*) 'mx_sys%geo%ncatoms:                ', mx_sys%geo%ncatoms                                 ; _newline_           \
    write(*,*) 'mx_sys%geo%nspecies:               ', mx_sys%geo%nspecies                                ; _newline_           \
    write(*,*) 'mx_sys%geo%only_user_def:          ', mx_sys%geo%only_user_def                           ; _newline_           \
    write(*,*) 'mx_sys%geo%species_time_dependent: ', mx_sys%geo%species_time_dependent                  ; _newline_           \
    write(*,*) 'mx_sys%geo%kinetic_energy:         ', mx_sys%geo%kinetic_energy                          ; _newline_           \
    write(*,*) 'mx_sys%geo%nlpp:                   ', mx_sys%geo%nlpp                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%nlcc:                   ', mx_sys%geo%nlcc                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%reduced_coordinates:    ', mx_sys%geo%reduced_coordinates                     ; _newline_           \
    write(*,*) '---------------------------------------------------------------'                         ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '---------------------------------------------------------------'                         ; _newline_           \
    write(*,*) 'mx_sys%geo%space variables:'                                                             ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%geo%space:                  ', mx_sys%geo%space                                   ; _newline_           \
    write(*,*) 'mx_sys%geo%space%dim:              ', mx_sys%geo%space%dim                               ; _newline_           \
    write(*,*) '---------------------------------------------------------------'                         ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1) values:'                                                              ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \ 
    write(*,*) 'mx_sys%geo%atom(1)%label:          ', 'not inizialized'                                  ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%x:              ', 'not inizialized'                                  ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%v:              ', 'not inizialized'                                  ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%f:              ', 'not inizialized'                                  ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%move:           ', 'not inizialized'                                  ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)spec values:'                                                          ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%index:       ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%label:       ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%type:        ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%z:           ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%z_val:       ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%weight:      ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%has_density: ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%user_def:    ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%omega:       ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%filename:    ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%jradius:     ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%jradius:     ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%jthick:      ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%sc_alpha:    ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%nlcc:        ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%sigma:       ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%rho:         ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%def_rsize:   ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%def_h:       ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%niwfs:       ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%lmax:        ', 'private variable'                               ; _newline_           \
    write(*,*) 'mx_sys%geo%atom(1)%spec%lloc:        ', 'private variable'                               ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%geo%catom(1):'                                                                    ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%geo%catom(1)%label:           ', 'not inizialized'                                ; _newline_           \
    write(*,*) 'mx_sys%geo%catom(1)%x:               ', 'not inizialized'                                ; _newline_           \
    write(*,*) 'mx_sys%geo%catom(1)%v:               ', 'not inizialized'                                ; _newline_           \
    write(*,*) 'mx_sys%geo%catom(1)%f:               ', 'not inizialized'                                ; _newline_           \
    write(*,*) 'mx_sys%geo%catom(1)%charge:          ', 'not inizialized'                                ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%geo%spec values:'                                                                 ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%geo%species%index:       ', 'privale variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%label:       ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%type:        ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%z:           ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%z_val:       ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%weight:      ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%has_density: ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%user_def:    ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%omega:       ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%filename:    ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%jradius:     ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%jradius:     ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%jthick:      ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%sc_alpha:    ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%nlcc:        ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%sigma:       ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%rho:         ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%def_rsize:   ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%def_h:       ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%niwfs:       ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%lmax:        ', 'private variable'                                    ; _newline_           \
    write(*,*) 'mx_sys%geo%species%lloc:        ', 'private variable'                                    ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist values:'                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%start:       ', mx_sys%geo%atoms_dist%start                        ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%end:         ', mx_sys%geo%atoms_dist%end                          ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%nlocal:      ', mx_sys%geo%atoms_dist%nlocal                       ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%nglobal:     ', mx_sys%geo%atoms_dist%nglobal                      ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%parallel:    ', mx_sys%geo%atoms_dist%parallel                     ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%node(:):     ', mx_sys%geo%atoms_dist%node(:)                      ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%range(:,:):  ', mx_sys%geo%atoms_dist%range(:,:)                   ; _newline_           \
    write(*,*) 'mx_sys%geo%atoms_dist%num(:):      ', mx_sys%geo%atoms_dist%num(:)                       ; _newline_           \
    write(*,*) 'mx_sys%geo%ionic_interaction_type(:,:):', mx_sys%geo%ionic_interaction_type(:,:)         ; _newline_           \
    write(*,*) 'mx_sys%geo%ionic_interaction_parameter(:,:,:):', mx_sys%geo%ionic_interaction_parameter(:,:,:) ; _newline_     \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '======================================================================================'  


#define VALUES_OUTPUT_GRID_SB                                                                            ; _newline_           \
    write(*,*) '======================================================================================'  ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%gr%sb values: '                                                                   ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%box_shape:               ', mx_sys%gr%sb%box_shape                          ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%box_offset(:):           ', mx_sys%gr%sb%box_offset(:)                      ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%rsize:                   ', mx_sys%gr%sb%rsize                              ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%xsize:                   ', mx_sys%gr%sb%xsize                              ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%lsize:                   ', mx_sys%gr%sb%lsize                              ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%user_def:                ', 'not inizialized'                               ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%mr_flag:                 ', mx_sys%gr%sb%mr_flag                            ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%rlattice_primitive(:,:): ', mx_sys%gr%sb%rlattice_primitive(:,:)            ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%rlattice(:,:):           ', mx_sys%gr%sb%rlattice                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%klattice_primitive(:,:): ', mx_sys%gr%sb%klattice_primitive                 ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%klattice(:,:):           ', mx_sys%gr%sb%klattice                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%volume_element:          ', mx_sys%gr%sb%volume_element                     ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%rcell_volume:            ', mx_sys%gr%sb%rcell_volume                       ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%dim:                     ', mx_sys%gr%sb%dim                                ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%periodic_dim:            ', mx_sys%gr%sb%periodic_dim                       ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%transport_dim:           ', mx_sys%gr%sb%transport_dim                      ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%image_size(1:2):         ', mx_sys%gr%sb%image_size(:)                      ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%filename:                ', 'not inizialized'                               ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%complex_boundaries:      ', mx_sys%gr%sb%complex_boundaries                 ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%symm values: '                                                              ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%symm%nops:               ', mx_sys%gr%sb%symm%nops                          ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%symm%breakdir:           ', mx_sys%gr%sb%symm%breakdir                      ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%symm%space_group:        ', mx_sys%gr%sb%symm%space_group                   ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%symm%ops values: '                                                          ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%symm%ops(1)%rot(:,:):       ', 'private variable'                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%symm%ops(1)%trans(:):       ', 'private variable'                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints values: '                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%method:             ', mx_sys%gr%sb%kpoints%method                  ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%use_symmetries:     ', mx_sys%gr%sb%kpoints%use_symmetries          ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%use_time_reversal:  ', mx_sys%gr%sb%kpoints%use_time_reversal       ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%nik_axis(:):        ', mx_sys%gr%sb%kpoints%nik_axis(:)             ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%shifts(:):          ', mx_sys%gr%sb%kpoints%shifts(:)               ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%symmetry_ops(:,:):  ', mx_sys%gr%sb%kpoints%symmetry_ops(:,:)       ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%num_symmetry_ops(:):', mx_sys%gr%sb%kpoints%num_symmetry_ops(:)     ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%klattice(:,:):      ', mx_sys%gr%sb%kpoints%klattice(:,:)           ; _newline_           \ 
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%full and mx_sys%gr%sb%kpoints%reduced values: '                     ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%full%point(:,:):       ', mx_sys%gr%sb%kpoints%full%point(:,:)      ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%full%red_point(:,:):   ', mx_sys%gr%sb%kpoints%full%red_point(:,:)  ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%full%weight(:):        ', mx_sys%gr%sb%kpoints%full%weight(:)       ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%full%npoints:          ', mx_sys%gr%sb%kpoints%full%npoints         ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%full%dim:              ', mx_sys%gr%sb%kpoints%full%dim             ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%reduced%point(:,:):    ', mx_sys%gr%sb%kpoints%reduced%point(:,:)   ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%reduced%red_point(:,:):', mx_sys%gr%sb%kpoints%reduced%red_point(:,:)  ; _newline_        \
    write(*,*) 'mx_sys%gr%sb%kpoints%reduced%weight(:):     ', mx_sys%gr%sb%kpoints%reduced%weight(:)    ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%reduced%npoints:       ', mx_sys%gr%sb%kpoints%reduced%npoints      ; _newline_           \
    write(*,*) 'mx_sys%gr%sb%kpoints%reduced%dim:           ', mx_sys%gr%sb%kpoints%reduced%dim          ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '======================================================================================'  


#define VALUES_OUTPUT_GRID_MESH                                                                          ; _newline_           \ 
    write(*,*) '======================================================================================'  ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \   
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh values: '                                                                 ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%use_curvilinear:             ', mx_sys%gr%mesh%use_curvilinear            ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%spacing(:):                  ', mx_sys%gr%mesh%spacing(:)                 ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%np:                          ', mx_sys%gr%mesh%np                         ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%np_part:                     ', mx_sys%gr%mesh%np_part                    ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%np_global:                   ', mx_sys%gr%mesh%np_global                  ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%np_part_global:              ', mx_sys%gr%mesh%np_part_global             ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%parallel_in_domains:         ', mx_sys%gr%mesh%parallel_in_domains        ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%x(:,:):                      ', 'The (local) \b points'                   ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%resolution(:,:,:):           ', mx_sys%gr%mesh%resolution(:,:,:)          ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%volume_element:              ', mx_sys%gr%mesh%volume_element             ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%vol_pp(:):                   ', mx_sys%gr%mesh%vol_pp(:)                  ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%idx values: '                                                             ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%idx%nr(2,:):                 ', mx_sys%gr%mesh%idx%nr(:,:)                ; _newline_           \    
    write(*,*) 'mx_sys%gr%mesh%idx%ll(:):                   ', mx_sys%gr%mesh%idx%ll(:)                  ; _newline_           \
    do ii=1, mx_sys%gr%mesh%np                                                                           ; _newline_           \
      write(*,*) 'mx_sys%gr%mesh%idx%lxyz(:,:):               ', mx_sys%gr%mesh%idx%lxyz(ii,:)           ; _newline_           \
    end do                                                                                               ; _newline_           \
    do ii= mx_sys%gr%mesh%idx%nr(1,1), mx_sys%gr%mesh%idx%nr(2,1)                                        ; _newline_           \
      do jj= mx_sys%gr%mesh%idx%nr(1,2), mx_sys%gr%mesh%idx%nr(2,2)                                      ; _newline_           \
        do kk= mx_sys%gr%mesh%idx%nr(1,3), mx_sys%gr%mesh%idx%nr(2,3)                                    ; _newline_           \
          write(*,*) 'mx_sys%gr%mesh%idx%lxyz_inv(:,:,:):         ', mx_sys%gr%mesh%idx%lxyz_inv(ii,jj,kk)  ; _newline_        \
        end do                                                                                           ; _newline_           \
      end do                                                                                             ; _newline_           \
    end do                                                                                               ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%idx%enlarge(:):              ', mx_sys%gr%mesh%idx%enlarge(:)             ; _newline_           \
    write(*,*) 'mx_sys%gr%mesh%idx%checksum:                ', mx_sys%gr%mesh%idx%checksum               ; _newline_           \
    write(*,*) '----------------------------------------------------------------'                        ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*)                                                                                           ; _newline_           \
    write(*,*) '======================================================================================'  

