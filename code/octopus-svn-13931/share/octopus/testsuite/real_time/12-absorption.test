# -*- coding: utf-8 mode: shell-script -*-
# $Id: 12-absorption.test 13244 2015-02-28 23:19:28Z dstrubbe $

Test       : Absorption spectrum
Program    : octopus
TestGroups : short-run, real_time
Enabled    : Yes

# Note: the results differ from 13-absorption-spin.test because here we are doing
# an odd number of electrons without spin-polarization, which is wrong.

# ground state
Processors : 1
Input      : 12-absorption.01-gs.inp
match ; Initial energy ; GREPFIELD(static/info, 'Total       =', 3) ; -5.81823431

Processors : 4
Input      : 12-absorption.02-td.inp
match ; Energy [step   1] ; LINEFIELD(td.general/energy, -101, 3) ; -5.818234898217e+00
match ; Energy [step  25] ; LINEFIELD(td.general/energy, -76, 3) ; -5.817853879374e+00
match ; Energy [step  50] ; LINEFIELD(td.general/energy, -51, 3) ; -5.817853879374e+00
match ; Energy [step  75] ; LINEFIELD(td.general/energy, -26, 3) ; -5.817853865170e+00
match ; Energy [step 100] ; LINEFIELD(td.general/energy, -1, 3) ; -5.817853846909e+00

Processors : 4
Input      : 12-absorption.03-td-restart.inp
match ; Energy [step 100] ; LINEFIELD(td.general/energy, -101, 3) ; -5.817853846909e+00
match ; Energy [step 125] ; LINEFIELD(td.general/energy, -76, 3) ; -5.817853835472e+00
match ; Energy [step 150] ; LINEFIELD(td.general/energy, -51, 3) ; -5.817853816430e+00
match ; Energy [step 175] ; LINEFIELD(td.general/energy, -26, 3) ; -5.817853803632e+00
match ; Energy [step 200] ; LINEFIELD(td.general/energy, -1, 3) ; -5.817853785846e+00

Util       : oct-propagation_spectrum
Input      : 12-absorption.04-spectrum.inp
match ; Electronic sum rule ; GREPFIELD(cross_section_vector.1, 'Electronic sum rule', 6) ; 0.964355
match ; Polarizability sum rule ; GREPFIELD(cross_section_vector.1, 'Polarizability (sum rule)', 6) ; 16.432380

Precision : 2.0e-7

match ; Energy      1 ; LINEFIELD(cross_section_tensor, -91, 1) ; 1.0
match ; Sigma       1 ; LINEFIELD(cross_section_tensor, -91, 2) ; 0.61262767E-01
match ; Anisotropy  1 ; LINEFIELD(cross_section_tensor, -91, 3) ; 0.70692055E-01

match ; Energy      2 ; LINEFIELD(cross_section_tensor, -81, 1) ; 2.0
match ; Sigma       2 ; LINEFIELD(cross_section_tensor, -81, 2) ; 0.20880255E+00
match ; Anisotropy  2 ; LINEFIELD(cross_section_tensor, -81, 3) ; 0.22854887E+00

match ; Energy      3 ; LINEFIELD(cross_section_tensor, -71, 1) ; 3.0
match ; Sigma       3 ; LINEFIELD(cross_section_tensor, -71, 2) ; 0.34753682E+00
match ; Anisotropy  3 ; LINEFIELD(cross_section_tensor, -71, 3) ; 0.35454189E+00

match ; Energy      4 ; LINEFIELD(cross_section_tensor, -61, 1) ; 4.0
match ; Sigma       4 ; LINEFIELD(cross_section_tensor, -61, 2) ; 0.37737616E+00
match ; Anisotropy  4 ; LINEFIELD(cross_section_tensor, -61, 3) ; 0.35813927E+00

match ; Energy      5 ; LINEFIELD(cross_section_tensor, -51, 1) ; 5.0
match ; Sigma       5 ; LINEFIELD(cross_section_tensor, -51, 2) ; 0.27374827E+00
match ; Anisotropy  5 ; LINEFIELD(cross_section_tensor, -51, 3) ; 0.24415583E+00

match ; Energy      6 ; LINEFIELD(cross_section_tensor, -41, 1) ; 6.0
match ; Sigma       6 ; LINEFIELD(cross_section_tensor, -41, 2) ; 0.11555323E+00
match ; Anisotropy  6 ; LINEFIELD(cross_section_tensor, -41, 3) ; 0.10017048E+00

match ; Energy      7 ; LINEFIELD(cross_section_tensor, -31, 1) ; 7.0
match ; Sigma       7 ; LINEFIELD(cross_section_tensor, -31, 2) ; 0.10434345E-01
match ; Anisotropy  7 ; LINEFIELD(cross_section_tensor, -31, 3) ; 0.15451907E-01

match ; Energy      8 ; LINEFIELD(cross_section_tensor, -21, 1) ; 8.0
match ; Sigma       8 ; LINEFIELD(cross_section_tensor, -21, 2) ; -0.70988598E-02
match ; Anisotropy  8 ; LINEFIELD(cross_section_tensor, -21, 3) ; 0.91010657E-02

match ; Energy      9 ; LINEFIELD(cross_section_tensor, -11, 1) ; 9.0
match ; Sigma       9 ; LINEFIELD(cross_section_tensor, -11, 2) ; 0.13334515E-01
match ; Anisotropy  9 ; LINEFIELD(cross_section_tensor, -11, 3) ; 0.19612328E-01

match ; Energy     10 ; LINEFIELD(cross_section_tensor, -1, 1) ; 10.0
match ; Sigma      10 ; LINEFIELD(cross_section_tensor, -1, 2) ; 0.17456193E-01
match ; Anisotropy 10 ; LINEFIELD(cross_section_tensor, -1, 3) ; 0.17247498E-01

Util       : oct-propagation_spectrum
Input      : 12-absorption.05-spectrum_compressed_sensing.inp
Precision : 1e-2
match ; Electronic sum rule ; GREPFIELD(cross_section_vector.1, 'Electronic sum rule', 6) ; 0.996601
Precision : 1e-1
match ; Polarizability sum rule ; GREPFIELD(cross_section_vector.1, 'Polarizability (sum rule)', 6) ; 12.603324

#match ; Energy      1 ; LINE(cross_section_tensor, 230, 1)  ; 1.0
#$match ; Sigma       1 ; LINE(cross_section_tensor, 23, 21) ;
#match ; Anisotropy  1 ; LINE(cross_section_tensor, 23, 41) ; 

#match ; Energy      2 ; LINE(cross_section_tensor, 39, 1)  ; 0.27000000E+01
#match ; Sigma       2 ; LINE(cross_section_tensor, 39, 21) ; 0.24611830E+01
#match ; Anisotropy  2 ; LINE(cross_section_tensor, 39, 41) ; 0.47660604E+01

#match ; Energy      3 ; LINE(cross_section_tensor, 347, 1)  ; 0.33400000E+01
#match ; Sigma       3 ; LINE(cross_section_tensor, 347, 21) ; 0.33193123E+02
#match ; Anisotropy  3 ; LINE(cross_section_tensor, 347, 41) ; 0.40653000E+02

#match ; Energy      4 ; LINE(cross_section_tensor, 82, 1)  ; 0.70000000E+01
#match ; Sigma       4 ; LINE(cross_section_tensor, 82, 21) ; 0.10806835E+00
#match ; Anisotropy  4 ; LINE(cross_section_tensor, 82, 41) ; 0.12072535E+00

#match ; Energy      5 ; LINE(cross_section_tensor, 118, 1) ; 0.10600000E+02
#match ; Sigma       5 ; LINE(cross_section_tensor, 118, 21); 0.52145360E-01
#match ; Anisotropy  5 ; LINE(cross_section_tensor, 118, 41); 0.10097905E+00

#match ; Energy      6 ; LINE(cross_section_tensor, 163, 1) ; 0.15100000E+02
#match ; Sigma       6 ; LINE(cross_section_tensor, 163, 21); 0.42107780E-01
#match ; Anisotropy  6 ; LINE(cross_section_tensor, 163, 41); 0.81541365E-01


Util       : oct-propagation_spectrum
Input      : 12-absorption.06-power_spectrum.inp
match ; Energy  0 x ; LINEFIELD(dipole_power, 13, 2) ; 0.66225480E-01
match ; Energy  0 y ; LINEFIELD(dipole_power, 13, 3) ; 0.35939672E-01
match ; Energy  0 z ; LINEFIELD(dipole_power, 13, 4) ; 0.12022952E-28

match ; Energy  1 x ; LINEFIELD(dipole_power, 23, 2) ; 0.62685953E-01
match ; Energy  1 y ; LINEFIELD(dipole_power, 23, 3) ; 0.35928656E-01
match ; Energy  1 z ; LINEFIELD(dipole_power, 23, 4) ; 0.11241176E-28

match ; Energy 10 x ; LINEFIELD(dipole_power, 113, 2) ; 0.31239081E-03
match ; Energy 10 y ; LINEFIELD(dipole_power, 113, 3) ; 0.31644760E-03
match ; Energy 10 z ; LINEFIELD(dipole_power, 113, 4) ; 0.61450976E-31

