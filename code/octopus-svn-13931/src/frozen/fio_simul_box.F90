#include "global.h"

module fio_simul_box_m

  use global_m
  use messages_m
  use profiling_m

  use geometry_m,   only: geometry_t
  use json_m,       only: JSON_OK, json_object_t, json_get
  use kpoints_m,    only: kpoints_init
  use mpi_m,        only: mpi_world
  use symmetries_m, only: symmetries_init

  use simul_box_m, only:   &
    HYPERCUBE,             &
    simul_box_load,        &
    simul_box_lookup_init

  use simul_box_m, only:            &
    fio_simul_box_t => simul_box_t

  use simul_box_m, only:                  &
    fio_simul_box_copy => simul_box_copy, &
    fio_simul_box_end  => simul_box_end

  implicit none

  private
  public ::          &
    fio_simul_box_t

  public ::             &
    fio_simul_box_init, &
    fio_simul_box_copy, &
    fio_simul_box_end
  
contains

  ! ---------------------------------------------------------
  subroutine fio_simul_box_init(this, geo, config)
    type(fio_simul_box_t), intent(out) :: this
    type(geometry_t),      intent(in)  :: geo
    type(json_object_t),   intent(in)  :: config

    character(len=MAX_PATH_LEN) :: dir, file
    integer                     :: ierr

    PUSH_SUB(fio_simul_box_init)

    call json_get(config, "dir", dir, ierr)
    ASSERT(ierr==JSON_OK)
    call json_get(config, "file", file, ierr)
    ASSERT(ierr==JSON_OK)
    call simul_box_load(this, dir, file, mpi_world, ierr)
    if(ierr==0)then
      ASSERT(this%box_shape/=HYPERCUBE)
      call simul_box_lookup_init(this, geo)
      ASSERT(.not.this%mr_flag)
      nullify(this%hr_area%radius, this%hr_area%interp%posi, this%hr_area%interp%ww)
      ASSERT(this%periodic_dim==0)
      call symmetries_init(this%symm, geo, this%dim, this%periodic_dim, this%rlattice)
      call kpoints_init(this%kpoints, this%symm, this%dim, this%rlattice, this%klattice, .true.)
    else
      message(1)="Error reading the simulation box info file: '"//trim(adjustl(file))//"'"
      message(2)="from the directory: '"//trim(adjustl(dir))//"'"
      write(unit=message(3), fmt="(a,i10)") "I/O Error: ", ierr
      call messages_fatal(3)
    end if

    POP_SUB(fio_simul_box_init)
  end subroutine fio_simul_box_init

end module fio_simul_box_m

!! Local Variables:
!! mode: f90
!! End:
