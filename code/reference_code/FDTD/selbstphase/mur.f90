module mur

contains

subroutine murbl( ez , bx , lb0m1 , lb1m1 , lb0 , lb1 , lb1p1 , lb0p1 , beta0 , v1 , v2 )
implicit none
REAL*8 ez(0:161000)
REAL*8 bx(0:161000)
REAL*8 beta0 , v1 , v2
REAL*8 lb0m1 , lb1m1 , lb0 , lb1 , lb0p1 , lb1p1
lb0m1 = lb0
lb1m1 = lb1
lb0 = bx(0)
lb1 = bx(1)
lb1p1 = bx(1) - beta0 * ( ez(2) - ez(1) )
lb0p1 = - lb1m1 + v1 *( lb1p1 + lb0m1 ) + v2 * ( lb0 + lb1 )
bx(1) = lb1p1
bx(0) = lb0p1
return
end subroutine

subroutine murel( ez , bx , le0m1 , le1m1 , le0 , le1 , le1p1 , le0p1 , alpha0 , v1 , v2 )
implicit none
REAL*8 ez(0:161000)
REAL*8 bx(0:161000)
REAL*8 alpha0 , v1 , v2
REAL*8 le0m1 , le1m1 , le0 , le1 , le0p1 , le1p1
le0m1 = le0
le1m1 = le1
le0 = ez(0)
le1 = ez(1)
le1p1 = ez(1) - alpha0 * ( bx(1) - bx(0) )
le0p1 = - le1m1 + v1 * ( le1p1 + le0m1 ) + v2 * ( le0 + le1 )
ez(1) = le1p1
ez(0) = le0p1
return
end subroutine

subroutine murbr( ez , bx , rb0m1 , rb1m1 , rb0 , rb1 , rb1p1 , rb0p1 , beta0 , v1 , v2 , zsize )
implicit none
integer zsize
REAL*8 ez(0:161000)
REAL*8 bx(0:161000)
REAL*8 beta0 , v1 , v2
REAL*8 rb0m1 , rb1m1 , rb0 , rb1 , rb0p1 , rb1p1
rb0m1 = rb0
rb1m1 = rb1
rb0 = bx(zsize+1)
rb1 = bx(zsize)
rb1p1 = bx(zsize) - beta0 * ( ez(zsize+1) - ez(zsize) )
rb0p1 = - rb1m1 + v1 * ( rb1p1 + rb0m1 ) + v2 * ( rb0 + rb1 )
bx(zsize) = rb1p1
bx(zsize+1) = rb0p1
return
end subroutine

subroutine murer( ez , bx , re0m1 , re1m1 , re0 , re1 , re1p1 , re0p1 , alpha0 , v1 , v2 , zsize )
implicit none
integer zsize
REAL*8 ez(0:161000)
REAL*8 bx(0:161000)
REAL*8 alpha0, v1 , v2
REAL*8 re0m1 , re1m1 , re0 , re1 , re0p1 , re1p1
re0m1 = re0
re1m1 = re1
re0 = ez(zsize+1)
re1 = ez(zsize)
re1p1 = ez(zsize) - alpha0 * ( bx(zsize) - bx(zsize-1) )
re0p1 = - re1m1 + v1 * ( re1p1 + re0m1 ) + v2 * ( re0 + re1 )
ez(zsize) = re1p1
ez(zsize+1) = re0p1
return
end subroutine

subroutine mur_init( lb0m1 , lb1m1 , lb0 , lb1 , lb0p1 , lb1p1 , le0m1 , le1m1 , &
	& le0 , le1 , le0p1 , le1p1 , rb0m1 , rb1m1 , rb0 , &
	& rb1 , rb0p1 , rb1p1 , re0m1 , re1m1 , re0 , re1 , re0p1 , re1p1 )
implicit none
REAL*8 rb0m1 , rb1m1 , rb0 , rb1 , rb1p1 , rb0p1
REAL*8 lb0m1 , lb1m1 , lb0 , lb1 , lb0p1 , lb1p1
REAL*8 re0m1 , re1m1 , re0 , re1 , re1p1 , re0p1
REAL*8 le0m1 , le1m1 , le0 , le1 , le0p1 , le1p1
rb0m1 = 0.0
rb1m1 = 0.0
rb1 = 0.0
rb0 = 0.0
rb1p1 = 0.0
rb0p1 = 0.0
lb0m1 = 0.0
lb1m1 = 0.0
lb1 = 0.0
lb0 = 0.0
lb1p1 = 0.0
lb0p1 = 0.0
re0m1 = 0.0
re1m1 = 0.0
re1 = 0.0
re0 = 0.0
re1p1 = 0.0
re0p1 = 0.0
le0m1 = 0.0
le1m1 = 0.0
le1 = 0.0
le0 = 0.0
le1p1 = 0.0
le0p1 = 0.0
return
end subroutine

end module