program selbstphase

use mur
use runge_kutta

implicit none
character filename*64,quest*4
integer z,t,n,w,zsize,moden,timelimit,spaminterval,messtrans,messquell,pic_num
integer qort,DOFOU,omegasteps,matstart,matende,datwrtime,calctime,breiteQW,firstQW,lastQW,numQW
REAL*8 omega,lightspd,PI,lz,x,psi,scatter,bohrrad,spam,k,alpha0,beta0,v1,v2,brech2,cn2,bruzz,dipmat
REAL*8 eps0,hquer,emasse,eladung,radfac,xez,xbx,dZ,dT,maxtstep,offset,breit,filfouw,lambda
REAL*8 ez(0:161000),bx(0:161000),bxold(0:161000),ezold(0:161000),ishift(0:161000),alpha(0:161000)
REAL*8 e0(0:161000) , e1(0:161000) , e2(0:161000) , f(0:161000), p(0:161000), pold(0:161000)
REAL*8 edichte2d,edichte3d,epsR,erho,gapomega,gamma0,rkfac1,rkfac2
REAL*8 domega,omegamin,omegamax
REAL*8 lb0m1,lb1m1,lb0,lb1,lb0p1,lb1p1
REAL*8 le0m1,le1m1,le0,le1,le0p1,le1p1
REAL*8 rb0m1,rb1m1,rb0,rb1,rb0p1,rb1p1
REAL*8 re0m1,re1m1,re0,re1,re0p1,re1p1
COMPLEX*8 ezfouquell(0:161000),ezfoutrans(0:161000),foutra,I
REAL*8 in0(0:161000),in1(0:161000),in2(0:161000),dtphi(0:161000),dtoldphi(0:161000),pez(0:161000)
COMPLEX*8 a1(0:161000),b1(0:161000),c1(0:161000),d1(0:161000)
COMPLEX*8 a2(0:161000),b2(0:161000),c2(0:161000),d2(0:161000)

pic_num = 1

quest = 'y'

zsize = 30000

calctime = 5000
timelimit = 80000
spaminterval = 1000
datwrtime = 100

DOFOU = 0

omega = 2.309			![1/fs]

PI = 4d0*atan(1d0)		!:)
radfac = Pi/180.		!not in use
lightspd = 299.792458		![nm/fs]

eps0 = 0.0615106
hquer = 0.658274
!emasse = 9.109E-31		!kg  <>  5.486E-4 !u
!eladung = 1.602E-19		!C   <>  eV ?!!?!?!
!bohrrad = 0.592E-9		!BohrRadius in nm
scatter = 20.0
lambda = 2*PI*lightspd/omega
dZ = lambda/scatter
dT = dZ/(2*lightspd)
maxtstep = calctime/dT

brech2 = 2.5d0
cn2 = lightspd/brech2

qort=10000
bruzz=9.5d0
offset=10d0
breit=150d0

firstQW = qort + 11000
numQW = 150
breiteQW = scatter/2
lastQW = firstQW+(numQW*breiteQW)

gapomega = 2.309					!GapFrequenz im Material =-> Resonanz
dipmat = 0.42						!Dipolmatrixelement
gamma0 = 0.55						!D�mpfung der Elektronen
epsR = 20.1						!relative Permeabilit�t
erho = 4.1E12						!Elektronendichte im Material
edichte2d = erho * 1E-14
edichte3d = edichte2d / dZ
	
alpha0 = (dT/dZ)*lightspd*lightspd
beta0 = dT/dZ
v1 = (lightspd*dT-dZ)/(lightspd*dT+dZ)
v2 = 2.*dZ/(lightspd*dT+dZ)

I = (0d0,1d0)
domega = 2d0*PI/calctime					![1/fs] Frequenzaufl�sung (Shannon)
omegamin  = omega-(0.45d0/dT)				![1/fs] min -> (omega-(2.*PI)/(dT)) .2
omegamax  = omega+(0.45d0/dT)				![1/fs] max -> (omega+(2.*PI)/(dT)) .3
omegasteps = (omegamax-omegamin)/domega			![1/fs] Frequenzschritte
messtrans = 23000 !qort + 500					![GRID] Messpunkt Fourier-Transmit
messquell = qort					![GRID] Messpunkt Fourier-QuellSignal
!messrefle = firstQW - 500.				![GRID] Messounkt Fourier-Reflex

matstart = 11000
matende = 20000

ezfouquell = (0d0,0d0)
ezfoutrans = (0d0,0d0)

ez=0d0
bx=0d0
dtphi=0d0
dtoldphi=0d0
ishift=0d0
alpha=alpha0

CALL mur_init( lb0m1 , lb1m1 , lb0 , lb1 , lb0p1 , lb1p1 , le0m1 , le1m1 , &
	& le0 , le1 , le0p1 , le1p1 , rb0m1 , rb1m1 , rb0 , rb1 , rb0p1 , &
	& rb1p1 , re0m1 , re1m1 , re0 , re1 , re0p1 , re1p1 )

write(*,*) 'ZSIZE		',zsize
write(*,*) 'ZSIZE ABS	',zsize/dZ,' nm'
write(*,*) 'CalCTIME	',calctime,' fs'
write(*,*) 'TstepZ		',maxtstep
write(*,*) 'ABS Nonlin-Z	',matende/dZ-matstart/dZ,' nm'
write(*,*) ''
!write(*,*) 'dT		',dT
write(*,*) 'alpha0		',alpha0
write(*,*) ''
write(*,*) 'omegastepZ	',omegasteps
if(DOFOU.eq.0) then
	write(*,*) ''
	write(*,*) '	---> NO FouTra done <---'
	write(*,*) ''
endif
!write(*,*) ''
!write(*,*) 'omegaBG	',omega/cn2
!write(*,*) 'rkfactor	',omega/cn2*brech2
write(*,*) ''
write(*,*) '1stQW		' , firstQW
write(*,*) 'nQW		' , numQW
write(*,*) 'lastQW		' , lastQW
write(*,*) 'QW_size	' , breiteQW

if(quest.eq.'y') then
do t=0,maxtstep

CALL murbl( ez , bx , lb0m1 , lb1m1 , lb0 , lb1 , lb1p1 , lb0p1 , beta0 , v1 , v2 )

do z=2,zsize-1
	bx(z) = bx(z) - beta0 * ( ez(z+1) - ez(z) )
enddo

CALL murbr( ez , bx , rb0m1 , rb1m1 , rb0 , rb1 , rb1p1 , rb0p1 , beta0 , v1 , v2 , zsize )
CALL murel( ez , bx , le0m1 , le1m1 , le0 , le1 , le1p1 , le0p1 , alpha0 , v1 , v2 )

CALL runkut( ez , pez , dtphi , dT , omega , zsize , a1 , b1 , c1 , d1 , I , in0 , in1 , in2 , cn2 , brech2)

do z=matstart,matende
	ishift(z)=dtphi(z)-dtoldphi(z)
	alpha(z)=dT/(dZ*(1.-(ishift(z)*ishift(z))))*lightspd*lightspd
	dtoldphi(z)=dtphi(z)
enddo

do z=2,zsize-1
	ez(z) = ez(z) - alpha(z) * ( bx(z) - bx(z-1) )
enddo

!CALL runkut2( ez , pez , p , f , dT , gapomega , rkfac1 , gamma0 , firstQW , breiteQW , &
!	& numQW , a1 , b1 , c1 , d1 , a2 , b2 , c2 , d2 , I , e0 , e1 , e2 , n )

!do n = 1, numQW
!	ez(firstQW+(n*breiteQW)) = ez(firstQW+(n*breiteQW)) - 2.*rkfac2*(real(p(n))-real(pold(n)))
!	pold(n) = p(n)
!enddo

CALL murer( ez , bx , re0m1 , re1m1 , re0 , re1 , re1p1 , re0p1 , alpha0 , v1 , v2 , zsize )

xez = omega*(t+.5)*dT-omega/lightspd*(qort+0.5)*dZ
!xbx = omega*t*dT-omega/lightspd*qort*dZ
ez(qort)=ez(qort)+bruzz*exp(-(xez/omega-offset)*(xez/omega-offset)/(breit*breit))!*sin(xez)
!bx(qort)=bx(qort)+bruzz*exp(-(xbx/omega-offset)*(xbx/omega-offset)/(breit*breit))			!sin(xbx)*

!xez = omega*(t-.5)*dT-omega/lightspd*(qort+0.5)*dZ
!ez(qort-1)=ez(qort-1)-bruzz*exp(-(xez/omega-offset)*(xez/omega-offset)/(breit*breit))

if (DOFOU.eq.1) then
do w=0,omegasteps
	foutra = exp( I * ( omegamin + (domega * w) ) * (t/1.) * dT ) * dT
	ezfouquell(w) = ezfouquell(w) + foutra * ez(messquell)
	ezfoutrans(w) = ezfoutrans(w) + foutra * ez(messtrans)
enddo
endif

if(mod(t,spaminterval).eq.0) then
  write(*,*) ''
  write(*,*) '	-------------------------'
  write(*,*) 'SpamTime		',t,' StepZ'
  write(*,*) 'SpamTime ABS		',t*dT,' fs'
  write(*,*) ''
  write(*,*) 'Mur - EL<->ER		',ez(0),' <->',ez(zsize+1)
  write(*,*) 'Mur - BL<->BR		',bx(0),' <->',bx(zsize+1)
  write(*,*) ''
  write(*,*) 'MessStart		',ez(11000),' @',matstart/dZ,' nm'
  write(*,*) 'MessEnde		',ez(15000),' @',matende/dZ,' nm'
  write(*,*) 'ez(messquell-100)	',ez(messquell-100)
  write(*,*) 'ez(12000)		',ez(12000)
  write(*,*) 'a0			',alpha0
  write(*,*) 'a(12000)		',alpha(12000)
  write(*,*) '1-ishift(12000)^2	',1.-ishift(12000)**2
  write(*,*) ''
  write(*,*) 'E-1st-QW		',ez(firstQW)
  write(*,*) 'E-last-QW		',ez(lastQW)
  write(*,*) 'f(21000)		',(abs(f(21000))*abs(f(21000)))
  write(*,*) ''
endif

if(t>13000 .and. mod(t,datwrtime).eq.0) then
  write(filename,'(I8.8,A5)') pic_num,'.edat'
  open(51,FILE=filename)
  do z=0,zsize+1
    spam = z/dZ
    k = ez(z)
    write(51,*) spam,"	",k
  enddo
  close(51)
  pic_num = pic_num + 1

if(DOFOU.eq.1) then
  filename = 'Quellsignal-Fourier.dat'
  open(51,FILE=filename)
  do w = 0, omegasteps
    filfouw = abs(ezfouquell(w)) * abs(ezfouquell(w))
    spam = hquer * ( omegamin + (w * domega) ) * 1000.
    write(51,*) spam , "	" , filfouw, ( abs(ezfoutrans(w))*abs(ezfoutrans(w)) )
  enddo
  close(51)

  filename = 'Transmit-Fourier.dat'
  open(51,FILE=filename)
  do w = 0, omegasteps
	filfouw = ( abs(ezfoutrans(w))*abs(ezfoutrans(w)) ) / ( abs(ezfouquell(w))*abs(ezfouquell(w)) )
	spam = hquer * ( omegamin + (w * domega) ) * 1000.
	write(51,*) spam , "	" , filfouw
  enddo
  close(51)
endif

!write(filename,*) t,'.pdat'
!	open(51,FILE=filename)
!	do z = 0, zsize+1
!		!filfouw = abs(dtphi(z)) * abs(dtphi(z))
!		spam = z/dZ
!		write(51,*) spam , "	", ishift(z)! , "	" , filfouw1
!	enddo
!close(51)

!  write(*,*) ''
!  write(*,'(A16,I7.7)') 'data written @	',t
!  write(*,*) ''
endif

  if(t>timelimit) then
	exit
  endif

enddo	!END TIMELOOF
endif	!END IFQUEST
	
end program