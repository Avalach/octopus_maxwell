module runge_kutta

contains

subroutine runkut( ez , pez , dtphi , dT , omega , zsize , a1 , b1 , c1 , d1 , I , in0 , in1 , in2 , cn2 , brech2 )
implicit none
integer o,zsize
REAL*8 ez(0:161000),pez(0:161000)
REAL*8 dT,omega,cn2,brech2
REAL*8 in0(0:161000),in1(0:161000),in2(0:161000),dtphi(0:161000)
COMPLEX*8 a1(0:161000),b1(0:161000),c1(0:161000),d1(0:161000)
!COMPLEX*8 a2(0:161000) b2(0:161000),c2(0:161000),d2(0:161000)
COMPLEX*8 I
do o = 2, zsize-1
	in0(o) = abs(ez(o)) * abs(ez(o))
	in1(o) = abs(ez(o)) * abs(ez(o)) + ( abs(ez(o)) * abs(ez(o)) - pez(o) ) / 2.
	in2(o) = abs(ez(o)) * abs(ez(o)) + ( abs(ez(o)) * abs(ez(o)) - pez(o) )
	a1(o) = dT * ( omega/cn2 * brech2 * in0(o) )
enddo
do o = 2, zsize-1
	b1(o) = dT * ( omega/cn2 * brech2 * ( in1(o) + a1(o)/2. ) )
enddo
do o = 2, zsize-1
	c1(o) = dT * ( omega/cn2 * brech2 * ( in1(o) + b1(o)/2. ) )
enddo
do o = 2, zsize-1
	d1(o) = dT * ( omega/cn2 * brech2 * ( in2(o) + c1(o) ) )
end do
do o = 2, zsize-1
	dtphi(o) = dtphi(o) + ( a1(o) + b1(o) + b1(o) + c1(o) + c1(o) + d1(o) ) / 6.
	pez(o) = in0(o)
enddo
return
end subroutine
	
subroutine runkut2( ez , pez , p , f , dT , gapomega , rkfac1 , gamma0 , firstQW , & 
	& breiteQW , numQW , a1 , b1 , c1 , d1 , a2 , b2 , c2 , d2 , I , e0 , e1 , e2 , n )
implicit none
integer n, firstQW , breiteQW , numQW
REAL*8 ez(0:161000) , pez(0:161000)
REAL*8 dT , gapomega , rkfac1 , gamma0
REAL*8 e0(0:161000) , e1(0:161000) , e2(0:161000)
COMPLEX*8 a1(0:161000) , b1(0:161000) , c1(0:161000) , d1(0:161000)
COMPLEX*8 a2(0:161000) , b2(0:161000) , c2(0:161000) , d2(0:161000)
COMPLEX*8 p(0:161000) , f(0:161000) , I
do n = 1, numQW
	e0(n) = ez(firstQW + (n*breiteQW))
	e1(n) = ez(firstQW + (n*breiteQW)) + ( ez(firstQW + (n*breiteQW)) - pez(n) ) / 2.
	e2(n) = ez(firstQW + (n*breiteQW)) + ( ez(firstQW + (n*breiteQW)) - pez(n) )
	a1(n) = dT * ( - I * gapomega * p(n) + I * rkfac1 * e0(n) * (1.-2.*f(n)) - gamma0 * p(n) )
	a2(n) = dT * 2. * rkfac1 * imag( e0(n)*p(n) )
enddo
do n = 1, numQW
	b1(n) = dT * ( - I * gapomega * ( p(n) + a1(n) / 2. ) + I * rkfac1 * e1(n) * (1. - 2. * (f(n) + a2(n)/2.) ) - gamma0 * p(n) )
	b2(n) = dT * 2. * rkfac1 * imag( e1(n) * (p(n)+a1(n)/2.) )
enddo
do n = 1, numQW
	c1(n) = dT * ( - I * gapomega * ( p(n) + b1(n) / 2. ) + I * rkfac1 * e1(n) * (1. - 2. * (f(n) + b2(n)/2.) ) - gamma0 * p(n) )
	c2(n) = dT * 2. * rkfac1 * imag( e1(n) * (p(n)+b1(n)/2.) )
enddo
do n = 1, numQW
	d1(n) = dT * ( - I * gapomega * ( p(n) + c1(n) ) + I * rkfac1 * e2(n) * (1. - 2. * (f(n) + c2(n)) ) - gamma0 * p(n) )
	d2(n) = dT * 2. * rkfac1 * imag( e2(n) * (p(n)+c1(n)) )
end do
do n = 1, numQW
	p(n) = p(n) + ( a1(n) + b1(n) + b1(n) + c1(n) + c1(n) + d1(n) ) / 6.
	f(n) = f(n) + ( a2(n) + b2(n) + b2(n) + c2(n) + c2(n) + d2(n) ) / 6.
	pez(n) = e0(n)
enddo
return
end subroutine

end module