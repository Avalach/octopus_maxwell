<?php

$output_resolution = "-w 1280 -n 1024";

$material ="plastic";
$shader = "tr 0.0 re 0.0 sp 0.1 di 0.1 sh 4 ex 0.1 ri 1.0";
$color = "20 70 205 1"; # r g b inh
$radius = "0.25";
$base_tr = 1.0;
$thickness = 0.025;
$zoom_factor = 2.0;

####################			remove all previous input/output files
$call = "rm -f 3d.db input3d go3d go3d.pix goxz goxz.pix goxy goxy.pix goyz goyz.pix";
system($call);

####################			look if we are running and where we are now if we do
$fp = @fopen("run3d","r");
if(!$fp) {
	$pic_count = 0;
	}
	else {
	$pic_count = fgets($fp);
	}
fclose($fp);

####################			generate new input from dat-files
print("\n\nStarting generation of input for PIC ".$pic_count.".\n");

$fp = fopen("input3d","w");
fwrite($fp,"units mm\n");

$brdr_count = 13;

#for($i=0;$i<=4;$i++) {
#	$ws  = "in brdr".$brdr_count.".r rcc 0 ".($i*10)." 0 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc ".($i*10)." 0 0 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc ".($i*10)." 0 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc ".($i*10)." 40 40 0 0 -40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 40 ".($i*10)." 40 -40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc ".($i*10)." 40 40 0 -40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc ".($i*10)." 0 0 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc ".($i*10)." 0 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 0 ".($i*10)." 0 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 0 ".($i*10)." 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 40 ".($i*10)." 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 40 0 ".($i*10)." 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 40 40 ".($i*10)." -40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 0 0 ".($i*10)." 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	$ws .= "in brdr".$brdr_count.".r rcc 0 0 ".($i*10)." 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
#	$brdr_count++;
#	fwrite($fp,$ws);
#	}



$ws  = "in brdr1.r rcc 0 0 0 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr2.r rcc 0 0 0 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr3.r rcc 0 0 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr4.r rcc 40 40 40 -40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr5.r rcc 40 40 40 0 -40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr6.r rcc 40 40 40 0 0 -40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr7.r rcc 40 0 0 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr8.r rcc 40 0 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr9.r rcc 0 40 0 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr10.r rcc 0 40 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr11.r rcc 0 0 40 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in brdr12.r rcc 0 0 40 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "Z\n";
fwrite($fp,$ws);

$ws = "in middle_line1.r rcc 0 30 30 60 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line2.r rcc 30 30 0 0 0 60 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line3.r rcc 30 0 30 0 60 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";

$ws .= "in middle_line4.r rcc 30 0 20 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line5.r rcc 20 0 30 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line6.r rcc 0 20 30 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line7.r rcc 0 30 20 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line8.r rcc 20 30 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line9.r rcc 30 20 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";

$ws .= "in middle_line10.r rcc 10 0 20 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line11.r rcc 20 0 10 0 40 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line12.r rcc 0 20 10 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line13.r rcc 0 10 20 40 0 0 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line14.r rcc 20 10 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";
$ws .= "in middle_line15.r rcc 10 20 0 0 0 40 ".$thickness." 0 0 0 1 0 1 0 0 0 1 0\n";

$ws .= "Z\n";
fwrite($fp,$ws);

$sphere_count = 0;

for ($ii=1;$ii<10;$ii++) {
	for ($i=1;$i<10;$i++) {
		$y_count = 0;
		$result = nil;
		$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_000".$ii."-000".$i.".dat";
		exec($call, &$result);
		foreach( $result as $v ) {
		  $tr_val = $base_tr;
		  $radius = 0.01 + floatval($v);
		  if( $radius > 0.1 ) {
			  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$ii." ".$y_count." ".$radius."\n";
#		  $write_string .= "r c_s_".$sphere_count.".c u sp".$sphere_count.".s\n";
#		  $write_string .= "mater c_s_".$sphere_count.".c \"plastic tr ".$tr_val." re 0.2 sp 0.9 di 0.9 sh 10 ex 0.9 ri 1.0\" ".$color."\n";
			  $write_string .= "Z\n";
#		  $write_string .= "draw c_s_".$sphere_count.".c\n";
			  fwrite($fp,$write_string);
			  $sphere_count++;
		  }
		  $y_count++;
		}

	$region_string = "r region_".($i-1)."_".($ii-1).".r";
	for($j=( (($i-1)*$y_count) + ( ($ii-1) * 100 * 400) );$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
	fwrite($fp,$region_string);
	fwrite($fp,"Z\n");
#	fwrite($fp,"draw region_".($i-1)."_".($ii-1).".r\n");
	}

	for ($i=10;$i<=60;$i++) {
		$y_count = 0;
		$result = nil;
		$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_000".$ii."-00".$i.".dat";
		exec($call, &$result);
		foreach( $result as $v ) {
		  $tr_val = $base_tr;
		  $radius = 0.01 + floatval($v);
		  if( $radius > 0.1 ) {
			  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$ii." ".$y_count." ".$radius."\n";
#		  $write_string .= "r c_s_".$sphere_count.".c u sp".$sphere_count.".s\n";
#		  $write_string .= "mater c_s_".$sphere_count.".c \"plastic tr ".$tr_val." re 0.2 sp 0.9 di 0.9 sh 10 ex 0.9 ri 1.0\" ".$color."\n";
			  $write_string .= "Z\n";
#		  $write_string .= "draw c_s_".$sphere_count.".c\n";
			  fwrite($fp,$write_string);
			  $sphere_count++;
		  }
		  $y_count++;
		}

	$region_string = "r region_".($i-1)."_".($ii-1).".r";
	for($j=( (($i-1)*$y_count) + ( ($ii-1) * 100 * 400) );$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
	fwrite($fp,$region_string);
	fwrite($fp,"Z\n");
#	fwrite($fp,"draw region_".($i-1)."_".($ii-1).".r\n");
	}
}

for ($ii=10;$ii<100;$ii++) {
	for ($i=1;$i<10;$i++) {
		$y_count = 0;
		$result = nil;
		$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_00".$ii."-000".$i.".dat";
		exec($call, &$result);
		foreach( $result as $v ) {
		  $tr_val = $base_tr;
		  $radius = 0.01 + floatval($v);
		  if( $radius > 0.1 ) {
			  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$ii." ".$y_count." ".$radius."\n";
#		  $write_string .= "r c_s_".$sphere_count.".c u sp".$sphere_count.".s\n";
#		  $write_string .= "mater c_s_".$sphere_count.".c \"plastic tr ".$tr_val." re 0.2 sp 0.9 di 0.9 sh 10 ex 0.9 ri 1.0\" ".$color."\n";
			  $write_string .= "Z\n";
#		  $write_string .= "draw c_s_".$sphere_count.".c\n";
			  fwrite($fp,$write_string);
			  $sphere_count++;
		  }
		  $y_count++;
		}

	$region_string = "r region_".($i-1)."_".($ii-1).".r";
	for($j=( (($i-1)*$y_count) + ( ($ii-1) * 100 * 400) );$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
	fwrite($fp,$region_string);
	fwrite($fp,"Z\n");
#	fwrite($fp,"draw region_".($i-1)."_".($ii-1).".r\n");
	}

	for ($i=10;$i<60;$i++) {
		$y_count = 0;
		$result = nil;
		$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_00".$ii."-00".$i.".dat";
		exec($call, &$result);
		foreach( $result as $v ) {
		  $tr_val = $base_tr;
		  $radius = 0.01 + floatval($v);
		  if( $radius > 0.1 ) {
			  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$ii." ".$y_count." ".$radius."\n";
#		  $write_string .= "r c_s_".$sphere_count.".c u sp".$sphere_count.".s\n";
#		  $write_string .= "mater c_s_".$sphere_count.".c \"plastic tr ".$tr_val." re 0.2 sp 0.9 di 0.9 sh 10 ex 0.9 ri 1.0\" ".$color."\n";
			  $write_string .= "Z\n";
#		  $write_string .= "draw c_s_".$sphere_count.".c\n";
			  fwrite($fp,$write_string);
			  $sphere_count++;
		  }
		  $y_count++;
		}

	$region_string = "r region_".($i-1)."_".($ii-1).".r";
	for($j=( (($i-1)*$y_count) + ( ($ii-1) * 100 * 400) );$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
	fwrite($fp,$region_string);
	fwrite($fp,"Z\n");
#	fwrite($fp,"draw region_".($i-1)."_".($ii-1).".r\n");

	}
}

for ($ii=1;$ii<=100;$ii++) {
	$comb_string = "comb net_".$ii;
	for ($i=1;$i<=40;$i++) {
		$comb_string .= " u region_".($i-1)."_".($ii-1).".r";
	}
	$comb_string .= "\n";
	$comb_string .= "mater net_".$ii." \"".$material." ".$shader."\" ".$color."\n";
	fwrite($fp,$comb_string);
}

$ws  = "comb container";
for($i=1;$i<$brdr_count;$i++) {
	$ws .= " u brdr".$i.".r";
	}
$ws .= "\n";
#$ws .= "mater container \"camo l 2.17 H 1.0 o 8.0 s 1 v 1.0/1.0/1.0 d 1000/1000/1000 t1 -0.25 t2 0.25 c1 120/120/120 c2 140/140/140 c3 160/160/160\" 0 0 0 0\n";
$ws .= "mater container \"plastic tr 0.1 re 0.1 sp 0.1 di 0.1 sh 20\" 85 85 85 0\n";
fwrite($fp,$ws);

$ws  = "comb hair_lines";
for($i=1;$i<=3;$i++) {
	$ws .= " u middle_line".$i.".r";
	}
$ws .= "\n";
#$ws .= "mater container \"camo l 2.17 H 1.0 o 8.0 s 1 v 1.0/1.0/1.0 d 1000/1000/1000 t1 -0.25 t2 0.25 c1 120/120/120 c2 140/140/140 c3 160/160/160\" 0 0 0 0\n";
$ws .= "mater hair_lines \"plastic tr 0.1 re 0.1 sp 0.1 di 0.1 sh 20\" 65 65 65 0\n";
fwrite($fp,$ws);

#############			Draw and savesettings now

$ws  = "Z\n";
$ws .= "draw hair_lines\n";
fwrite($fp,$ws);
for ($ii=1;$ii<=100;$ii++) {
	$draw_string = "draw net_".$ii."\n";
	fwrite($fp,$draw_string);
}
$ws  = "view quat 0.248097349046 0.476590573266 0.748097349046 0.389434830518\n";
$ws .= "view center 30 30 30\n";
$ws .= "zoom ".$zoom_factor."\n";
$ws .= "saveview go3d ".$output_resolution."\n";
fwrite($fp,$ws);

$ws  = "Z\n";
$ws .= "draw hair_lines\n";
fwrite($fp,$ws);
for ($ii=1;$ii<=100;$ii++) {
	$draw_string = "draw net_".$ii."\n";
	fwrite($fp,$draw_string);
}
$ws  = "view quat 0.5 0.5 0.5 0.5\n";
$ws .= "view center 30 30 30\n";
$ws .= "zoom ".$zoom_factor."\n";
$ws .= "saveview goyz ".$output_resolution."\n";
fwrite($fp,$ws);

$ws  = "Z\n";
$ws .= "draw hair_lines\n";
fwrite($fp,$ws);
for ($ii=1;$ii<=100;$ii++) {
	$draw_string = "draw net_".$ii."\n";
	fwrite($fp,$draw_string);
}
$ws  = "view quat 0 0.707106781187 0.707106781187 0\n";
$ws .= "view center 30 30 30\n";
$ws .= "zoom ".$zoom_factor."\n";
$ws .= "saveview goxz ".$output_resolution."\n";
fwrite($fp,$ws);

$ws  = "Z\n";
$ws .= "draw hair_lines\n";
fwrite($fp,$ws);
for ($ii=1;$ii<=100;$ii++) {
	$draw_string = "draw net_".$ii."\n";
	fwrite($fp,$draw_string);
}
$ws  = "view quat 0 0 0 1\n";
$ws .= "view center 30 30 30\n";
$ws .= "zoom ".$zoom_factor."\n";
$ws .= "saveview goxy ".$output_resolution."\n";
fwrite($fp,$ws);

fclose($fp);

####################			invoke MGED to create the DB for the renderer
$call = "nice -n 10 mged 3d.db < input3d";
system($call);

####################			run the generated script
print("\n\n --- Starting RT 3D now ---\n");
$call = "nice -n 10 ./go3d";
system($call);
print(" --- Starting RT XY now ---\n");
$call = "nice -n 10 ./goxy";
system($call);
print(" --- Starting RT YZ now ---\n");
$call = "nice -n 10 ./goyz";
system($call);
print(" --- Starting RT XZ now ---\n");
$call = "nice -n 10 ./goxz";
system($call);
print("\n\n --- Finished RT's ---\n");

####################			now gen the .png from the .pix file
$call = "nice -n 10 pix-png ".$output_resolution." go3d.pix > 3d/".$pic_count.".png";
system($call);
$call = "nice -n 10 pix-png ".$output_resolution." goxy.pix > 3d/xy".$pic_count.".png";
system($call);
$call = "nice -n 10 pix-png ".$output_resolution." goxz.pix > 3d/xz".$pic_count.".png";
system($call);
$call = "nice -n 10 pix-png ".$output_resolution." goyz.pix > 3d/yz".$pic_count.".png";
system($call);

####################			finally advance pic_count and note it for the next run
$pic_count++;
$fp = fopen("run3d","w");
fwrite($fp,$pic_count);
fclose($fp);
?>
