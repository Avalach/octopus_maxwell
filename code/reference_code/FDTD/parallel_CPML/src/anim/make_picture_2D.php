<?php

$output_resolution = "-w 1280 -n 1024";

$material ="plastic";
$shader = "tr 0.0 re 0.8 sp 0.9 di 0.9 sh 20 ex 0.0 ri 1.0";
$color = "50 130 202"; # <- checkin'
$radius = "0.75";

####################			remove all previous input/output files
$call = "rm -f 2d.db input2d go2d go2d.pix";
system($call);

####################			look if we are running and where we are now if we do
$fp = @fopen("run2d","r");
if(!$fp) {
	$pic_count = 0;
	}
	else {
	$pic_count = fgets($fp);
	}
fclose($fp);

####################			generate new input from dat-files
print("\n\n\nStarting generation of input for PIC ".$pic_count.".\n\n");

$fp = fopen("input2d","w");
fwrite($fp,"units mm\n");
$sphere_count = 0;

for ($i=1;$i<10;$i++) {
	$y_count = 0;
	$result = nil;
	$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_000".$i.".dat";
	exec($call, &$result);
	foreach( $result as $v ) {
	  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$y_count." ".$v." ".$radius."\n";
	  fwrite($fp,$write_string);
	  $y_count++;
	  $sphere_count++;
	}
	$region_string = "r region".($i-1).".r";
	for($j=(($i-1)*$y_count);$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
#	$region_string .= "mater region".($i-1).".r ".$material." ".$color." 0\n";
	$region_string .= "Z\n";
	$region_string .= "draw region".($i-1).".r\n";
	fwrite($fp,$region_string);
	$y_count = 0;
}

for ($i=10;$i<100;$i++) {
	$y_count = 0;
	$result = nil;
	$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_00".$i.".dat";
	exec($call, &$result);
	foreach( $result as $v ) {
	  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$y_count." ".$v." ".$radius."\n";
	  fwrite($fp,$write_string);
	  $y_count++;
	  $sphere_count++;
	}
	$region_string = "r region".($i-1).".r";
	for($j=(($i-1)*$y_count);$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
#	$region_string .= "mater region".($i-1).".r ".$material." ".$color." 0\n";
	$region_string .= "Z\n";
	$region_string .= "draw region".($i-1).".r\n";
	fwrite($fp,$region_string);
	$y_count = 0;
}

for ($i=100;$i<=100;$i++) {
	$y_count = 0;
	$result = nil;
	$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_0".$i.".dat";
	exec($call, &$result);
	foreach( $result as $v ) {
	  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$y_count." ".$v." ".$radius."\n";
	  fwrite($fp,$write_string);
	  $y_count++;
	  $sphere_count++;
	}
	$region_string = "r region".($i-1).".r";
	for($j=(($i-1)*$y_count);$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
#	$region_string .= "mater region".($i-1).".r ".$material." ".$color." 0\n";
	$region_string .= "Z\n";
	$region_string .= "draw region".($i-1).".r\n";
	fwrite($fp,$region_string);
	$y_count = 0;
}

$comb_string = "comb net";
for ($i=1;$i<=100;$i++) {
	$comb_string .= " u region".($i-1).".r";
}
$comb_string .= "\n";
fwrite($fp,$comb_string);

fwrite($fp,"mater net \"".$material." ".$shader."\" ".$color." 1\n");
fwrite($fp,"view quat 0.248097349046 0.476590573266 0.748097349046 0.389434830518\n");
fwrite($fp,"Z\n");
fwrite($fp,"draw net\n");
fwrite($fp,"view center 50 200 0\n");
fwrite($fp,"zoom 1.05\n");
fwrite($fp,"saveview go2d ".$output_resolution);
fclose($fp);

####################			invoke MGED to create the DB for the renderer
$call = "nice -n 10 mged -c 2d.db < input2d";
system($call);

####################			run the generated script
$call = "nice -n 10 ./go2d";
system($call);

####################			now gen the .png from the .pix file
$call = "nice -n 10 pix-png ".$output_resolution." go2d.pix > 2d/".$pic_count.".png";
system($call);

####################			finally advance pic_count and note it for the next run
$pic_count++;
$fp = fopen("run2d","w");
fwrite($fp,$pic_count);
fclose($fp);
?>
