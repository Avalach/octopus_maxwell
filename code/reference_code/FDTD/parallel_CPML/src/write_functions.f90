module write_functions
use vars
implicit none

contains

subroutine output_init_data()
implicit none
open(10,file='Initial_Data_Spam.dat')

write(10,*) "--------------------------------------------------------------------"
write(10,'(A30,F17.4,A3)') "                         dx = ",dx," nm"
write(10,'(A30,F17.4,A3)') "                         dy = ",dy," nm"
write(10,'(A30,F17.4,A3)') "                         dz = ",dz," nm"
write(10,'(A30,EN17.6,A3)') "                         dt = ",dt," fs"
write(10,*) ""
write(10,'(A30,F14.2,A3,F14.2,A3,F14.2,A17)') "                Grid Size is:  ", &
		Imax*dx," x ", Jmax*dy," x ", Kmax*dz," cubic nanometers"
write(10,'(A30,I14.2,A3,I14.2,A3,I14.2,A15)') "                Grid Size is:  ", &
		Imax," x ", Jmax," x ", Kmax," cubic grids ;)"
write(10,*) ""
write(10,'(A30,I10.2,A3)') "             Simulation Time: ", Tmax," fs"
write(10,'(A30,I10.2)') "                 MAX_N_STEPS: ", Nmax
write(10,'(A30,I14.2)') "        Working precision is: ", wp
write(10,*) ""
write(10,'(A30)') "      Source-Data:       "
!write(*,'(A30,I10.2)') "           Peaking at T_STEP: ", int(source_offset*dt)
write(10,'(A30,F10.4,A5,A16,F10.4,A3)') "                       Omega: ", omega," 1/fs", "   Free Lambda:",lambda_free," nm"
!write(*,'(A30,F10.4,A3)') "                Free-Lambda: ", lambda_free," nm"
write(10,'(A30,F10.4,A10,F10.4,A8,F6.2,A6)') "             Backgnd-Lambda: ", lambda," nm, c -> ",cn, &
		" nm/fs (",(cn/c0)*100d0," % c0)"
write(10,'(A30,F10.4,A10,F10.4,A8,F6.2,A6)') "               Media-Lambda: ", lambda1," nm, c -> ", cn1, &
		" nm/fs (",(cn1/c0)*100d0," % c0)"
write(10,*) ""
write(10,'(A30)') "       FT-Info:          "
write(10,'(A30,I10.2)') "           Freq-Steps in DFT: ", nw
write(10,*) ""
!write(*,*) "",jsource+20+(num_plates*2*J_size)
write(10,'(A30,F14.4,A10)') "           Laufzeit bis Cube:          ", ((dx/dt)/c0)*(jsource+1150.0_wp), "[N-Steps]"
write(10,*) ""
write(10,'(A30,I14.2)') "   Number of Field Variables:  ", &
		3*(Imax*Jmax*Kmax)+8*nxPML_1*(Imax*Jmax+Imax*Kmax+Jmax*Kmax)-16*(Imax+Jmax+Kmax)+(24*nxPML_1)**2
write(10,*) "--------------------------------------------------------------------"
!write(*,*) "last plate at:", first_plate + ( num_plates * J_SIZE * 2 )

close(10)
end subroutine

subroutine spam_output()
implicit none
integer	:: o

open(10,file='Initial_Data_Spam.dat',access='append')
write(10,'(I7.7,EN20.8,EN20.8,EN20.8)') n, ez(isource,jsource,ksource), ez(isource,550,ksource), ez(isource,1140,ksource)
close(10)

!do o=1, Kmax
!  write(51,*) o, test_interferenz_ex_z(Imax/2,60,o) + test_interferenz_ey_z(Imax/2,60,o) + test_interferenz_ez_z(Imax/2,60,o), &
!  		 test_interferenz_ex_z(Imax/2,100,o) + test_interferenz_ey_z(Imax/2,100,o) + test_interferenz_ez_z(Imax/2,100,o), &
!  		 test_interferenz_ex_z(Imax/2,150,o) + test_interferenz_ey_z(Imax/2,150,o) + test_interferenz_ez_z(Imax/2,150,o), &
!  		 test_interferenz_ex_z(Imax/2,200,o) + test_interferenz_ey_z(Imax/2,200,o) + test_interferenz_ez_z(Imax/2,200,o), &
!  		 test_interferenz_ex_z(Imax/2,250,o) + test_interferenz_ey_z(Imax/2,250,o) + test_interferenz_ez_z(Imax/2,250,o), &
!  		 test_interferenz_ex_z(Imax/2,300,o) + test_interferenz_ey_z(Imax/2,300,o) + test_interferenz_ez_z(Imax/2,300,o), &
!  		 test_interferenz_ex_z(Imax/2,350,o) + test_interferenz_ey_z(Imax/2,350,o) + test_interferenz_ez_z(Imax/2,350,o), &
!  		 test_interferenz_ex_z(Imax/2,400,o) + test_interferenz_ey_z(Imax/2,400,o) + test_interferenz_ez_z(Imax/2,400,o), &
!  		 test_interferenz_ex_z(Imax/2,450,o) + test_interferenz_ey_z(Imax/2,450,o) + test_interferenz_ez_z(Imax/2,450,o), &
!  		 test_interferenz_ex_z(Imax/2,500,o) + test_interferenz_ey_z(Imax/2,500,o) + test_interferenz_ez_z(Imax/2,500,o), &
!  		 test_interferenz_ex_z(Imax/2,550,o) + test_interferenz_ey_z(Imax/2,550,o) + test_interferenz_ez_z(Imax/2,550,o)
!enddo
!close(51)
end subroutine

subroutine gen_render()
implicit none
integer	:: o, p, q

!sys_call = "rm -f /mnt/numerik/fortran/parallel_CPML/src/Test_*"
!call system(sys_call)

if(pic_type==2) then

do p=1, Imax
    write(filename,100) "Test_out_",p,".dat"
100 format(A9,I4.4,A4)
    open(51,file=filename)
    do o=1, Jmax
	if( ( ( o.gt.(Jsource-6) ) .and. ( o.lt.(Jsource+6) ) ) .and. ( ( p.gt.(Isource-6) ) .and. ( p.lt.(Isource+6) ) ) ) then
		write(51,*) 50.0_wp * ez(p,o,Ksource)
	else
	write(51,*) 10000.0_wp * ez(p,o,Ksource)
	endif
    enddo
  close(51)
enddo

sys_call = "cd /mnt/numerik/fortran/parallel_CPML/src/anim && php make_picture_2D.php"
call system(sys_call)

elseif(pic_type==3) then

do q=1, Kmax
do p=1, Imax
    write(filename,110) "Test_out_",p,"-",q,".dat"
110 format(A9,I4.4,A1,I4.4,A4)
    open(51,file=filename)
    do o=1, Jmax
	if( ( ( o.gt.(Jsource-2) ) .and. ( o.lt.(Jsource+2) ) ) .and. &
	    ( ( p.gt.(Isource-2) ) .and. ( p.lt.(Isource+2) ) ) .and. &
	    ( ( q.gt.(Isource-2) ) .and. ( q.lt.(Isource+2) ) ) ) then
		write(51,"(F14.12)") ( 1.0_wp / 1.0_wp ) * abs(ez(p,o,q))
	else
	write(51,"(F14.12)") ( 1.0_wp / 1.0_wp ) * abs(ez(p,o,q))
	endif
    enddo
  close(51)
enddo
enddo

sys_call = "cd /mnt/numerik/fortran/parallel_CPML/src/anim && php make_picture_3D.php"
call system(sys_call)

endif

end subroutine

subroutine make_all_movies()
implicit none

sys_call = "cd /mnt/numerik/fortran/parallel_CPML/src/anim && php make_movie_2d.php"
call system(sys_call)
sys_call = "cd /mnt/numerik/fortran/parallel_CPML/src/anim && php make_movie_3d.php"
call system(sys_call)

end subroutine

subroutine write_overtime()
use update_functions
implicit none

write(1,*) n,	source_amp_gauss * sin_source(((1.0_wp*n)+0.5_wp)*dt,source_offset,source_width,omega), &
		ez(isource,jsource,ksource), ez(Imax/2,1100,Kmax/2), ez(Imax/2,Jmax-20,Kmax/2)

write(2,*) n,	ez(20,1120,ksource), ez(20,1130,ksource), ez(20,1140,ksource), ez(20,1150,ksource)

write(3,*) n,	ez(40,1120,ksource), ez(40,1130,ksource), ez(40,1140,ksource), ez(40,1150,ksource)

end subroutine

subroutine output_step_data(edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
			    edach_mess_4, edach_mess_5, edach_mess_6)
implicit none
complex(wp), dimension(:),intent(in)	:: edach_source, &
					   edach_mess_1, edach_mess_2, edach_mess_3, &
					   edach_mess_4, edach_mess_5, edach_mess_6


complex(wp)	::  spek_max_src, spek_max_mess

spek_max_src = (0.0_wp,0.0_wp)
spek_max_mess = (0.0_wp,0.0_wp)
do w=1, nw
	if( abs(spek_max_mess).lt.abs(edach_mess_6(w)) ) then
		spek_max_mess = edach_mess_6(w)
	endif
	if( abs(spek_max_src).lt.abs(edach_source(w)) ) then
		spek_max_src = edach_source(w)
	endif
enddo

open(51,file='Fourier.dat')
do w=1,nw
  write(51,*) (wmin+w*dw), abs(edach_source(w))**2, &
   abs(edach_mess_1(w))**2, abs(edach_mess_2(w))**2, abs(edach_mess_3(w))**2, &
   abs(edach_mess_4(w))**2, abs(edach_mess_5(w))**2, abs(edach_mess_6(w))**2
enddo
close(51)

open(51,file='Spektrum.dat')
do w=1,nw
  write(51,*) (wmin+w*dw), abs(edach_mess_1(w))**2 / abs(edach_source(w))**2, &
		     abs(edach_mess_2(w))**2 / abs(edach_source(w))**2, &
		     abs(edach_mess_3(w))**2 / abs(edach_source(w))**2, &
		     abs(edach_mess_4(w))**2 / abs(edach_source(w))**2, &
		     abs(edach_mess_5(w))**2 / abs(edach_source(w))**2, &
		     abs(edach_mess_6(w))**2 / abs(edach_source(w))**2
enddo
close(51)

end subroutine output_step_data

subroutine output_fourier_data()
implicit none
complex(wp)	::  spek_max_src, spek_max_mess_1, spek_max_mess_2, &
			spek_max_mess_3, spek_max_mess_4, spek_max_mess_5, spek_max_mess_6


spek_max_src = (0.0_wp,0.0_wp)
spek_max_mess_1 = (0.0_wp,0.0_wp)
spek_max_mess_2 = (0.0_wp,0.0_wp)
spek_max_mess_3 = (0.0_wp,0.0_wp)
spek_max_mess_4 = (0.0_wp,0.0_wp)
spek_max_mess_5 = (0.0_wp,0.0_wp)
spek_max_mess_6 = (0.0_wp,0.0_wp)
do w=1, nw
	if( (abs(spek_max_mess_1)**2).lt.(abs(edach_x_mess_1(w))**2) ) then
		spek_max_mess_1 = edach_x_mess_1(w)
	endif
	if( (abs(spek_max_mess_2)**2).lt.(abs(edach_x_mess_2(w))**2) ) then
		spek_max_mess_2 = edach_x_mess_2(w)
	endif
	if( (abs(spek_max_mess_3)**2).lt.(abs(edach_x_mess_3(w))**2) ) then
		spek_max_mess_3 = edach_x_mess_3(w)
	endif
	if( (abs(spek_max_mess_4)**2).lt.(abs(edach_x_mess_4(w))**2) ) then
		spek_max_mess_4 = edach_x_mess_4(w)
	endif
	if( (abs(spek_max_mess_5)**2).lt.(abs(edach_x_mess_5(w))**2) ) then
		spek_max_mess_5 = edach_x_mess_5(w)
	endif
	if( (abs(spek_max_mess_6)**2).lt.(abs(edach_x_mess_6(w))**2) ) then
		spek_max_mess_6 = edach_x_mess_6(w)
	endif
	if( (abs(spek_max_src)**2).lt.(abs(edach_x_source(w))**2) ) then
		spek_max_src = edach_x_source(w)
	endif
enddo

open(51,file='Verhaeltnisse.dat',access='append')
if( (abs(spek_max_src)**2).eq.0.0_wp ) then
	write(51,'(I7.7,EN20.8,EN20.8,EN20.8,EN20.8,EN20.8,EN20.8)') n, abs(spek_max_src)**2, abs(spek_max_src)**2, &
		abs(spek_max_src)**2, abs(spek_max_src)**2, abs(spek_max_src)**2, abs(spek_max_src)**2
else
	write(51,'(I7.7,EN20.8,EN20.8,EN20.8,EN20.8,EN20.8,EN20.8)') n, &
			( abs(spek_max_mess_1)**2 / abs(spek_max_src)**2 ), &
			( abs(spek_max_mess_2)**2 / abs(spek_max_src)**2 ), &
			( abs(spek_max_mess_3)**2 / abs(spek_max_src)**2 ), &
			( abs(spek_max_mess_4)**2 / abs(spek_max_src)**2 ), &
			( abs(spek_max_mess_5)**2 / abs(spek_max_src)**2 ), &
			( abs(spek_max_mess_6)**2 / abs(spek_max_src)**2 )
endif
close(51)

open(51,file='Spektrum-Left_Echo.dat')
do w=1,nw
  write(51,*) (wmin+w*dw), abs(edach_x_mess_1(w))**2 / abs(edach_x_source(w))**2 - &
				( ( ( abs(spek_max_mess_1)**2 / abs(spek_max_src)**2 ) ) * abs(edach_x_source(w))**2 ), &
			     abs(edach_x_mess_2(w))**2 / abs(edach_x_source(w))**2 - &
				( ( ( abs(spek_max_mess_2)**2 / abs(spek_max_src)**2 ) ) * abs(edach_x_source(w))**2 ), &
			     abs(edach_x_mess_3(w))**2 / abs(edach_x_source(w))**2 - &
				( ( ( abs(spek_max_mess_3)**2 / abs(spek_max_src)**2 ) ) * abs(edach_x_source(w))**2 ), &
			     abs(edach_x_mess_4(w))**2 / abs(edach_x_source(w))**2 - &
				( ( ( abs(spek_max_mess_4)**2 / abs(spek_max_src)**2 ) ) * abs(edach_x_source(w))**2 ), &
			     abs(edach_x_mess_5(w))**2 / abs(edach_x_source(w))**2 - &
				( ( ( abs(spek_max_mess_5)**2 / abs(spek_max_src)**2 ) ) * abs(edach_x_source(w))**2 ), &
			     abs(edach_x_mess_6(w))**2 / abs(edach_x_source(w))**2 - &
				( ( ( abs(spek_max_mess_6)**2 / abs(spek_max_src)**2 ) ) * abs(edach_x_source(w))**2 )
enddo
close(51)


spek_max_src = (0.0_wp,0.0_wp)
spek_max_mess_1 = (0.0_wp,0.0_wp)
spek_max_mess_2 = (0.0_wp,0.0_wp)
spek_max_mess_3 = (0.0_wp,0.0_wp)
spek_max_mess_4 = (0.0_wp,0.0_wp)
spek_max_mess_5 = (0.0_wp,0.0_wp)
spek_max_mess_6 = (0.0_wp,0.0_wp)

do w=1, nw
	if( (abs(spek_max_mess_1)**2).lt.(abs(edach_y_mess_1(w))**2) ) then
		spek_max_mess_1 = edach_y_mess_1(w)
	endif
	if( (abs(spek_max_mess_2)**2).lt.(abs(edach_y_mess_2(w))**2) ) then
		spek_max_mess_2 = edach_y_mess_2(w)
	endif
	if( (abs(spek_max_mess_3)**2).lt.(abs(edach_y_mess_3(w))**2) ) then
		spek_max_mess_3 = edach_y_mess_3(w)
	endif
	if( (abs(spek_max_mess_4)**2).lt.(abs(edach_y_mess_4(w))**2) ) then
		spek_max_mess_4 = edach_y_mess_4(w)
	endif
	if( (abs(spek_max_mess_5)**2).lt.(abs(edach_y_mess_5(w))**2) ) then
		spek_max_mess_5 = edach_y_mess_5(w)
	endif
	if( (abs(spek_max_mess_6)**2).lt.(abs(edach_y_mess_6(w))**2) ) then
		spek_max_mess_6 = edach_y_mess_6(w)
	endif
	if( (abs(spek_max_src)**2).lt.(abs(edach_y_source(w))**2) ) then
		spek_max_src = edach_y_source(w)
	endif
enddo

open(51,file='Spektrum-Right_Echo.dat')
do w=1,nw
  write(51,*) (wmin+w*dw), abs(edach_y_mess_1(w))**2 / abs(edach_y_source(w))**2 - &
				( ( abs(spek_max_mess_1)**2 / abs(spek_max_src)**2 ) * abs(edach_y_source(w))**2 ), &
			     abs(edach_y_mess_2(w))**2 / abs(edach_y_source(w))**2 - &
				( ( abs(spek_max_mess_2)**2 / abs(spek_max_src)**2 ) * abs(edach_y_source(w))**2 ), &
			     abs(edach_y_mess_3(w))**2 / abs(edach_y_source(w))**2 - &
				( ( abs(spek_max_mess_3)**2 / abs(spek_max_src)**2 ) * abs(edach_y_source(w))**2 ), &
			     abs(edach_y_mess_4(w))**2 / abs(edach_y_source(w))**2 - &
				( ( abs(spek_max_mess_4)**2 / abs(spek_max_src)**2 ) * abs(edach_y_source(w))**2 ), &
			     abs(edach_y_mess_5(w))**2 / abs(edach_y_source(w))**2 - &
				( ( abs(spek_max_mess_5)**2 / abs(spek_max_src)**2 ) * abs(edach_y_source(w))**2 ), &
			     abs(edach_y_mess_6(w))**2 / abs(edach_y_source(w))**2 - &
				( ( abs(spek_max_mess_6)**2 / abs(spek_max_src)**2 ) * abs(edach_y_source(w))**2 )
enddo
close(51)

open(51,file='Fourier-Left.dat')
do w=1,nw
    write(51,*) (wmin+w*dw), abs(edach_x_source(w))**2, &
		   abs(edach_x_mess_1(w))**2, abs(edach_x_mess_2(w))**2, abs(edach_x_mess_3(w))**2, &
		   abs(edach_x_mess_4(w))**2, abs(edach_x_mess_5(w))**2, abs(edach_x_mess_6(w))**2
enddo
close(51)

!  open(51,file='Spektrum-X.dat')
!  do w=1,nw
!    write(51,*) (wmin+w*dw), abs(edach_x_mess_1(w))**2 / abs(edach_x_source(w))**2, &
!			     abs(edach_x_mess_2(w))**2 / abs(edach_x_source(w))**2, &
!			     abs(edach_x_mess_3(w))**2 / abs(edach_x_source(w))**2, &
!			     abs(edach_x_mess_4(w))**2 / abs(edach_x_source(w))**2, &
!			     abs(edach_x_mess_5(w))**2 / abs(edach_x_source(w))**2, &
!			     abs(edach_x_mess_6(w))**2 / abs(edach_x_source(w))**2
!  enddo
!  close(51)

!  open(51,file='Fourier-Y.dat')
!  do w=1,nw
!    write(51,*) (wmin+w*dw), abs(edach_y_source(w))**2, &
!		   abs(edach_y_mess_1(w))**2, abs(edach_y_mess_2(w))**2, abs(edach_y_mess_3(w))**2, &
!		   abs(edach_y_mess_4(w))**2, abs(edach_y_mess_5(w))**2, abs(edach_y_mess_6(w))**2
!  enddo
!  close(51)

!  open(51,file='Spektrum-Y.dat')
!  do w=1,nw
!    write(51,*) (wmin+w*dw), abs(edach_y_mess_1(w))**2 / abs(edach_y_source(w))**2, &
!			     abs(edach_y_mess_2(w))**2 / abs(edach_y_source(w))**2, &
!			     abs(edach_y_mess_3(w))**2 / abs(edach_y_source(w))**2, &
!			     abs(edach_y_mess_4(w))**2 / abs(edach_y_source(w))**2, &
!			     abs(edach_y_mess_5(w))**2 / abs(edach_y_source(w))**2, &
!			     abs(edach_y_mess_6(w))**2 / abs(edach_y_source(w))**2
!  enddo
!  close(51)

!  open(51,file='Fourier-Z.dat')
!  do w=1,nw
!    write(51,*) (wmin+w*dw), abs(edach_z_source(w))**2, &
!		   abs(edach_z_mess_1(w))**2, abs(edach_z_mess_2(w))**2, abs(edach_z_mess_3(w))**2, &
!		   abs(edach_z_mess_4(w))**2, abs(edach_z_mess_5(w))**2, abs(edach_z_mess_6(w))**2
!  enddo
!  close(51)

open(51,file='Spektrum-Z.dat')
do w=1,nw
  write(51,*) (wmin+w*dw), abs(edach_z_mess_1(w))**2 / abs(edach_z_source(w))**2, &
			     abs(edach_z_mess_2(w))**2 / abs(edach_z_source(w))**2, &
			     abs(edach_z_mess_3(w))**2 / abs(edach_z_source(w))**2, &
			     abs(edach_z_mess_4(w))**2 / abs(edach_z_source(w))**2, &
			     abs(edach_z_mess_5(w))**2 / abs(edach_z_source(w))**2, &
			     abs(edach_z_mess_6(w))**2 / abs(edach_z_source(w))**2
enddo
close(51)
end subroutine output_fourier_data

end module