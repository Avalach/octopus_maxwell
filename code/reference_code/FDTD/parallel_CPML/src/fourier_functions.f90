module fourier_functions
use omp_lib
use vars
implicit none

contains

subroutine fourier_x_axis(tf, e_field, edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
		      edach_mess_4, edach_mess_5, edach_mess_6)
  implicit none
  integer, intent(in)		:: tf
  real(wp), dimension(:,:,:)	:: e_field
  complex(wp)			:: fou
  complex(wp), dimension(:)	:: edach_source, &
				   edach_mess_1, edach_mess_2, edach_mess_3, &
				   edach_mess_4, edach_mess_5, edach_mess_6
  do w=1,nw
    fou = exp(im*(wmin+w*dw)*(dble(tf)*dt))*dt
    edach_source(w) = edach_source(w) + fou * e_field(isource,jsource,ksource)
    edach_mess_1(w) = edach_mess_1(w) + fou * e_field(20,Jmax/2,Kmax/2)! * (42-isource)**2
    edach_mess_2(w) = edach_mess_2(w) + fou * e_field(40,Jmax/2,Kmax/2)! * (44-isource)**2
    edach_mess_3(w) = edach_mess_3(w) + fou * e_field(50,Jmax/2,Kmax/2)! * (46-isource)**2
    edach_mess_4(w) = edach_mess_4(w) + fou * e_field(70,Jmax/2,Kmax/2)! * (48-isource)**2
    edach_mess_5(w) = edach_mess_5(w) + fou * e_field(80,Jmax/2,Kmax/2)! * (50-isource)**2
    edach_mess_6(w) = edach_mess_6(w) + fou * e_field(100,Jmax/2,Kmax/2)! * (52-isource)**2
  enddo
end subroutine fourier_x_axis

subroutine fourier_y_axis(tf, e_field, edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
		      edach_mess_4, edach_mess_5, edach_mess_6)
  implicit none
  integer, intent(in)		:: tf
  real(wp), dimension(:,:,:)	:: e_field
  complex(wp)			:: fou
  complex(wp), dimension(:)	:: edach_source, &
				   edach_mess_1, edach_mess_2, edach_mess_3, &
				   edach_mess_4, edach_mess_5, edach_mess_6
!$omp parallel do default(shared) private(w,fou)
  do w=1,nw
    fou = exp(im*(wmin+w*dw)*(dble(tf)*dt))*dt
    edach_source(w) = edach_source(w) + fou * e_field(isource,jsource,ksource)
    edach_mess_1(w) = edach_mess_1(w) + fou * e_field(Imax/2,120,Kmax/2)
    edach_mess_2(w) = edach_mess_2(w) + fou * e_field(Imax/2,240,Kmax/2)
    edach_mess_3(w) = edach_mess_3(w) + fou * e_field(Imax/2,460,Kmax/2)
    edach_mess_4(w) = edach_mess_4(w) + fou * e_field(Imax/2,680,Kmax/2)
    edach_mess_5(w) = edach_mess_5(w) + fou * e_field(Imax/2,800,Kmax/2)
    edach_mess_6(w) = edach_mess_6(w) + fou * e_field(Imax/2,1140,Kmax/2)
  enddo
!$omp end parallel do
end subroutine fourier_y_axis

subroutine fourier_z_axis(tf, e_field, edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
		      edach_mess_4, edach_mess_5, edach_mess_6)
  implicit none
  integer, intent(in)		:: tf
  real(wp), dimension(:,:,:)	:: e_field
  complex(wp)			:: fou
  complex(wp), dimension(:)	:: edach_source, &
				   edach_mess_1, edach_mess_2, edach_mess_3, &
				   edach_mess_4, edach_mess_5, edach_mess_6
  do w=1,nw
    fou = exp(im*(wmin+w*dw)*(dble(tf)*dt))*dt
    edach_source(w) = edach_source(w) + fou * e_field(isource,jsource,ksource)
    edach_mess_1(w) = edach_mess_1(w) + fou * e_field(Imax/2,Jmax/2,20)! * (42-isource)**2
    edach_mess_2(w) = edach_mess_2(w) + fou * e_field(Imax/2,Jmax/2,40)! * (44-isource)**2
    edach_mess_3(w) = edach_mess_3(w) + fou * e_field(Imax/2,Jmax/2,50)! * (46-isource)**2
    edach_mess_4(w) = edach_mess_4(w) + fou * e_field(Imax/2,Jmax/2,70)! * (48-isource)**2
    edach_mess_5(w) = edach_mess_5(w) + fou * e_field(Imax/2,Jmax/2,80)! * (50-isource)**2
    edach_mess_6(w) = edach_mess_6(w) + fou * e_field(Imax/2,Jmax/2,100)! * (52-isource)**2
  enddo
end subroutine fourier_z_axis

subroutine fourier_right_echo(tf, e_field, edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
		      edach_mess_4, edach_mess_5, edach_mess_6)
  implicit none
  integer, intent(in)		:: tf
  real(wp), dimension(:,:,:)	:: e_field
  complex(wp)			:: fou
  complex(wp), dimension(:)	:: edach_source, &
				   edach_mess_1, edach_mess_2, edach_mess_3, &
				   edach_mess_4, edach_mess_5, edach_mess_6
!$omp parallel do default(shared) private(w,fou)
  do w=1,nw
    fou = exp(im*(wmin+w*dw)*(dble(tf)*dt))*dt
    edach_source(w) = edach_source(w) + fou * e_field(isource,jsource,ksource)
    edach_mess_1(w) = edach_mess_1(w) + fou * e_field(30,120,Kmax/2)
    edach_mess_2(w) = edach_mess_2(w) + fou * e_field(30,240,Kmax/2)
    edach_mess_3(w) = edach_mess_3(w) + fou * e_field(30,460,Kmax/2)
    edach_mess_4(w) = edach_mess_4(w) + fou * e_field(30,680,Kmax/2)
    edach_mess_5(w) = edach_mess_5(w) + fou * e_field(30,800,Kmax/2)
    edach_mess_6(w) = edach_mess_6(w) + fou * e_field(30,1140,Kmax/2)
  enddo
!$omp end parallel do
end subroutine fourier_right_echo

subroutine fourier_left_echo(tf, e_field, edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
		      edach_mess_4, edach_mess_5, edach_mess_6)
  implicit none
  integer, intent(in)		:: tf
  real(wp), dimension(:,:,:)	:: e_field
  complex(wp)			:: fou
  complex(wp), dimension(:)	:: edach_source, &
				   edach_mess_1, edach_mess_2, edach_mess_3, &
				   edach_mess_4, edach_mess_5, edach_mess_6
!$omp parallel do default(shared) private(w,fou)
  do w=1,nw
    fou = exp(im*(wmin+w*dw)*(dble(tf)*dt))*dt
    edach_source(w) = edach_source(w) + fou * e_field(isource,jsource,ksource)
    edach_mess_1(w) = edach_mess_1(w) + fou * e_field(15,120,Kmax/2)
    edach_mess_2(w) = edach_mess_2(w) + fou * e_field(15,240,Kmax/2)
    edach_mess_3(w) = edach_mess_3(w) + fou * e_field(15,460,Kmax/2)
    edach_mess_4(w) = edach_mess_4(w) + fou * e_field(15,680,Kmax/2)
    edach_mess_5(w) = edach_mess_5(w) + fou * e_field(15,800,Kmax/2)
    edach_mess_6(w) = edach_mess_6(w) + fou * e_field(15,1140,Kmax/2)
  enddo
!$omp end parallel do
end subroutine fourier_left_echo

subroutine fourier_planediag(tf, e_field, edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
		      edach_mess_4, edach_mess_5, edach_mess_6)
  implicit none
  integer, intent(in)		:: tf
  real(wp), dimension(:,:,:)	:: e_field
  complex(wp)			:: fou
  complex(wp), dimension(:)	:: edach_source, &
				   edach_mess_1, edach_mess_2, edach_mess_3, &
				   edach_mess_4, edach_mess_5, edach_mess_6
!$omp parallel do default(shared) private(w,fou)
  do w=1,nw
    fou = exp(im*(wmin+w*dw)*(dble(tf)*dt))*dt
    edach_source(w) = edach_source(w) + fou * e_field(isource,jsource,ksource)
    edach_mess_1(w) = edach_mess_1(w) + fou * e_field(20,20,Kmax/2)
    edach_mess_2(w) = edach_mess_2(w) + fou * e_field(40,40,Kmax/2)
    edach_mess_3(w) = edach_mess_3(w) + fou * e_field(50,50,Kmax/2)
    edach_mess_4(w) = edach_mess_4(w) + fou * e_field(70,70,Kmax/2)
    edach_mess_5(w) = edach_mess_5(w) + fou * e_field(80,80,Kmax/2)
    edach_mess_6(w) = edach_mess_6(w) + fou * e_field(100,100,Kmax/2)
  enddo
!$omp end parallel do
end subroutine fourier_planediag

subroutine fourier_raumdiag(tf, e_field, edach_source, edach_mess_1, edach_mess_2, edach_mess_3, &
		      edach_mess_4, edach_mess_5, edach_mess_6)
  implicit none
  integer, intent(in)		:: tf
  real(wp), dimension(:,:,:)	:: e_field
  complex(wp)			:: fou
  complex(wp), dimension(:)	:: edach_source, &
				   edach_mess_1, edach_mess_2, edach_mess_3, &
				   edach_mess_4, edach_mess_5, edach_mess_6
!$omp parallel do default(shared) private(w,fou)
  do w=1,nw
    fou = exp(im*(wmin+w*dw)*(dble(tf)*dt))*dt
    edach_source(w) = edach_source(w) + fou * e_field(isource,jsource,ksource)
    edach_mess_1(w) = edach_mess_1(w) + fou * e_field(20,20,20)
    edach_mess_2(w) = edach_mess_2(w) + fou * e_field(40,40,22)
    edach_mess_3(w) = edach_mess_3(w) + fou * e_field(50,50,25)
    edach_mess_4(w) = edach_mess_4(w) + fou * e_field(70,70,35)
    edach_mess_5(w) = edach_mess_5(w) + fou * e_field(80,80,38)
    edach_mess_6(w) = edach_mess_6(w) + fou * e_field(100,100,40)
  enddo
!$omp end parallel do
end subroutine fourier_raumdiag

end module