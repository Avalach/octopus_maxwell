module vars
implicit none

integer, parameter	:: wp = 4   ! 4 / 8 / 10 bit selection for working precision (real and complex)

intrinsic		:: atan, sin, cos, exp

!  Set Fundamental Constants in Units: eV & nm & fs !!!!
real(wp), parameter	:: pi = 4.0_wp*atan(1.0_wp), & ! 355/113
			   c0 = 299.792458_wp, &
			   hbar = 0.658274_wp, &
			   eps0 = 0.0615106_wp, &
			   mu0 = 1.0_wp/(eps0*(c0*c0))

!  ..................................
!  Specify Background Material Relative Permittivity and Conductivity
real(wp), parameter	:: epsR = 1.0_wp, &!12.7, &
			   sigM = 0.0_wp, &
			   cn = c0/sqrt(epsR)
!  ..................................
!  Specify Inlay Material Relative Permittivity and Conductivity
!  Positions and Electron Densities are below
real(wp), parameter	:: epsR1 = 20.1_wp, &
			   sigM1 = 0.0_wp, &  ! Dielektrikum
			   cn1 = c0/sqrt(epsR1), &
			   omega_gap = 1.85_wp, &
			   dima = 0.3_wp  ! Matrixelement of the Dipole
!  ..................................
!  Specify Simulation Time, Grid Size and Grid Parameters
integer, parameter	:: Tmax = 8000, &  ! total simulation time [fs]
			   Imax = 60, & ! total X GRID-CELLS (incl PML!)
			   Jmax = 1200, & ! total Y GRID-CELLS (incl PML!)
			   Kmax = 60, & ! total Z GRID-CELLS (incl PML!)
			   steps_per_lambda = 5.0_wp, &
			   do_fdtd_routine = 1, &
			   do_field_update = 1, &
			   spam_interval = 10, &
			   data_interval = 100, &
			   snap_interval = 5, &
			   pic_type = 2
!  ..................................
!  Specify the Source
integer, parameter	:: gauss_puls = 1, &
			   sech_puls = 0, &
			   soliton_puls = 0
real(wp), parameter	:: source_width = 105.0_wp, &
			   source_offset = 10.1_wp * source_width, & !27.2 for smooth in 8 oder 10 bit real mode
			   source_active_time = 0.0_wp, &
			   omega = 1.85_wp, &
			   tc = sqrt( (8.0_wp*pi*eps0*epsR*cn*hbar)/(4.1E12 * omega**2 * dima**2) ), &
			   u = 0.4_wp, &
			   sol_alpha = sqrt(2.0_wp/(tc**2 * (1.0_wp-u**2))), &
			   source_amp_gauss = 2.0_wp * pi * (hbar/dima) / sqrt(pi*source_width**2), &
			   source_amp_sech = hbar/dima * 2.0_wp / source_width, &
			   lambda_free = 2.0_wp*pi*c0/omega, &
			   lambda = 2.0_wp*pi*cn/omega, &
			   lambda1 = 2.0_wp*pi*cn1/omega
integer, parameter	:: isource = 15, &
			   jsource = 40, &
			   ksource = Kmax/2
!  ..................................
!  Specify Grid Cell Size in Each Direction and Calculate the
!  Resulting Courant-Stable Time Step and N_MAX_STEPS
real(wp), parameter	:: dx = lambda/steps_per_lambda, &!1.0E-3, &
			   dy = lambda/steps_per_lambda, &!1.0E-3, &
			   dz = lambda/steps_per_lambda, &!1.0E-3, &  ! cell size in each direction
			   dt = 0.99_wp / (c0*sqrt(1.0_wp/dx**2+1.0_wp/dy**2+1.0_wp/dz**2)) ! time step increment
integer*8, parameter	:: Nmax = Tmax / dt
!  ..................................
! Electron Densities and Position of Inlay are specified here
real(wp), parameter	:: elec_dens_3d_x = 4.1e12 * 1e-14 / dx, &
			   elec_dens_3d_y = 4.1e12 * 1e-14 / dy, &
			   elec_dens_3d_z = 4.1e12 * 1e-14 / dz
integer, parameter	:: num_plates = 20, &
			   I_size = 5, &
			   J_size = lambda_free/2/dx, &
			   K_size = 5, &
			   first_plate = 40
!  ..................................
!  Specify DFT Parameters
real(wp), parameter     :: dw = 2.0_wp*pi/(dble(tmax)), &
                           wmin = omega-(0.08_wp/dt), &
                           wmax = omega+(0.13_wp/dt)
integer, parameter      :: nw = (wmax-wmin)/dw ! steps in fourier trafo
!  ..................................
!  Specify the Thickness in Each Direction,
!  the Order and other stuff for the CPML
integer, parameter	:: nxPML_1 = 11, &
			   nxPML_2 = 11, &
			   nyPML_1 = 11, &
			   nyPML_2 = 11, &
			   nzPML_1 = 11, &
			   nzPML_2 = 11, &
			   m = 3, &
			   ma = 1
real(wp), parameter	:: sig_x_max = 0.75_wp * (0.8_wp*(m+1)/(dx*sqrt(mu0/eps0*epsR))), &
			   sig_y_max = 0.75_wp * (0.8_wp*(m+1)/(dy*sqrt(mu0/eps0*epsR))), &
			   sig_z_max = 0.75_wp * (0.8_wp*(m+1)/(dz*sqrt(mu0/eps0*epsR))), &
			   alpha_x_max = 0.24_wp, &
			   alpha_y_max = 0.24_wp, &
			   alpha_z_max = 0.24_wp, &
			   kappa_x_max = 15.0_wp, &
			   kappa_y_max = 15.0_wp, &
			   kappa_z_max = 15.0_wp
!  ..................................
!  From down here are only variables
integer			:: i, j, k, ii, jj, kk, n, w
character*128		:: filename, sys_call
complex(wp), parameter	:: im = ( 0.0_wp, 1.0_wp )
real(wp)		:: P1, P2, const_active, DA, DB
!  ..................................
!  TE & TM field components
real(wp), dimension(Imax, Jmax, Kmax-1)		::  Ez, CA, CB, sig, eps
real(wp), dimension(Imax-1, Jmax, Kmax-1)	::  Hy
real(wp), dimension(Imax,Jmax-1, Kmax-1)	::  Hx
real(wp), dimension(Imax-1, Jmax-1, Kmax)	::  Hz
real(wp), dimension(Imax-1, Jmax, Kmax)		::  Ex
real(wp), dimension(Imax,Jmax-1, Kmax)		::  Ey
!  ..................................
!  PML stuff
real(wp), dimension(nxPML_1,Jmax,Kmax)		::  psi_Ezx_1
real(wp), dimension(nxPML_2,Jmax,Kmax)		::  psi_Ezx_2
real(wp), dimension(nxPML_1-1,Jmax,Kmax)	::  psi_Hyx_1
real(wp), dimension(nxPML_2-1,Jmax,Kmax)	::  psi_Hyx_2
real(wp), dimension(Imax,nyPML_1,Kmax)		::  psi_Ezy_1
real(wp), dimension(Imax,nyPML_2,Kmax)		::  psi_Ezy_2
real(wp), dimension(Imax,nyPML_1-1,Kmax)	::  psi_Hxy_1
real(wp), dimension(Imax,nyPML_2-1,Kmax)	::  psi_Hxy_2
real(wp), dimension(Imax,Jmax-1,nzPML_1-1)	::  psi_Hxz_1
real(wp), dimension(Imax,Jmax-1,nzPML_2-1)	::  psi_Hxz_2
real(wp), dimension(Imax-1,Jmax,nzPML_1-1)	::  psi_Hyz_1
real(wp), dimension(Imax-1,Jmax,nzPML_2-1)	::  psi_Hyz_2
real(wp), dimension(Imax-1,Jmax,nzPML_1)	::  psi_Exz_1
real(wp), dimension(Imax-1,Jmax,nzPML_2)	::  psi_Exz_2
real(wp), dimension(Imax,Jmax-1,nzPML_1)	::  psi_Eyz_1
real(wp), dimension(Imax,Jmax-1,nzPML_2)	::  psi_Eyz_2
real(wp), dimension(nxPML_1-1,Jmax-1,Kmax)	::  psi_Hzx_1
real(wp), dimension(nxPML_1,Jmax-1,Kmax)	::  psi_Eyx_1
real(wp), dimension(nxPML_2-1,Jmax-1,Kmax)	::  psi_Hzx_2
real(wp), dimension(nxPML_2,Jmax-1,Kmax)	::  psi_Eyx_2
real(wp), dimension(Imax-1,nyPML_1-1,Kmax)	::  psi_Hzy_1
real(wp), dimension(Imax-1,nyPML_1,Kmax)	::  psi_Exy_1
real(wp), dimension(Imax-1,nyPML_2-1,Kmax)	::  psi_Hzy_2
real(wp), dimension(Imax-1,nyPML_2,Kmax)	::  psi_Exy_2

real(wp), dimension(nxPML_1)	::  be_x_1, ce_x_1, alphae_x_PML_1, sige_x_PML_1, kappae_x_PML_1
real(wp), dimension(nxPML_1-1)	::  bh_x_1, ch_x_1, alphah_x_PML_1, sigh_x_PML_1, kappah_x_PML_1
real(wp), dimension(nxPML_2)	::  be_x_2, ce_x_2, alphae_x_PML_2, sige_x_PML_2, kappae_x_PML_2
real(wp), dimension(nxPML_2-1)	::  bh_x_2, ch_x_2, alphah_x_PML_2, sigh_x_PML_2, kappah_x_PML_2
real(wp), dimension(nyPML_1)	::  be_y_1, ce_y_1, alphae_y_PML_1, sige_y_PML_1, kappae_y_PML_1
real(wp), dimension(nyPML_1-1)	::  bh_y_1, ch_y_1, alphah_y_PML_1, sigh_y_PML_1, kappah_y_PML_1
real(wp), dimension(nyPML_2)	::  be_y_2, ce_y_2, alphae_y_PML_2, sige_y_PML_2, kappae_y_PML_2
real(wp), dimension(nyPML_2-1)	::  bh_y_2, ch_y_2, alphah_y_PML_2, sigh_y_PML_2, kappah_y_PML_2
real(wp), dimension(nzPML_1)	::  be_z_1, ce_z_1, alphae_z_PML_1, sige_z_PML_1, kappae_z_PML_1
real(wp), dimension(nzPML_1-1)	::  bh_z_1, ch_z_1, alphah_z_PML_1, sigh_z_PML_1, kappah_z_PML_1
real(wp), dimension(nzPML_2)	::  be_z_2, ce_z_2, alphae_z_PML_2, sige_z_PML_2, kappae_z_PML_2
real(wp), dimension(nzPML_2-1)	::  bh_z_2, ch_z_2, alphah_z_PML_2, sigh_z_PML_2, kappah_z_PML_2
real(wp), dimension(Imax-1)	::  den_ex, den_hx
real(wp), dimension(Jmax-1)	::  den_ey, den_hy
real(wp), dimension(Kmax-1)	::  den_ez, den_hz

complex(wp), dimension(nw)	:: edach_x_source, edach_y_source, edach_z_source, &
				   edach_x_mess_1, edach_x_mess_2, edach_x_mess_3, &
				   edach_x_mess_4, edach_x_mess_5, edach_x_mess_6, &
				   edach_y_mess_1, edach_y_mess_2, edach_y_mess_3, &
				   edach_y_mess_4, edach_y_mess_5, edach_y_mess_6, &
				   edach_z_mess_1, edach_z_mess_2, edach_z_mess_3, &
				   edach_z_mess_4, edach_z_mess_5, edach_z_mess_6, &
				   edach_signal_x, edach_signal_y, edach_signal_z

real(wp), dimension(num_plates)		:: pe_x, pe_y, pe_z
complex(wp), dimension(num_plates)	:: a1, a2, b1, b2, c1, c2, d1, d2, &
					   p_x, f_x, p_y, f_y, p_z, f_z, &
					   pold_x, fold_x, pold_y, fold_y, pold_z, fold_z

real(wp), dimension(Imax, Jmax, Kmax)	:: test_interferenz_ex_x, test_interferenz_ex_y, test_interferenz_ex_z, &
					   test_interferenz_ey_x, test_interferenz_ey_y, test_interferenz_ey_z, &
					   test_interferenz_ez_x, test_interferenz_ez_y, test_interferenz_ez_z
real(wp), dimension(Imax)		:: total_int_x, total_int_y, total_int_z

contains

subroutine init_vars()
implicit none

  P1 = 0.0_wp; P2 = 0.0_wp
  sig(:,:,:) = sigM
  eps(:,:,:) = epsR*eps0

  do i = 1,nxPML_1
     sige_x_PML_1(i) = sig_x_max * ( (nxPML_1 - i) / (nxPML_1 - 1.0_wp) )**m
     alphae_x_PML_1(i) = alpha_x_max*((i-1.0_wp)/(nxPML_1-1.0_wp))**ma
     kappae_x_PML_1(i) = 1.0_wp+(kappa_x_max-1.0_wp)* &
                                ((nxPML_1 - i) / (nxPML_1 - 1.0_wp))**m
     be_x_1(i) = exp(-(sige_x_PML_1(i) / kappae_x_PML_1(i) + &
                                alphae_x_PML_1(i))*dt/eps0)
     if ((sige_x_PML_1(i) == 0.0_wp) .and. &
        (alphae_x_PML_1(i) == 0.0_wp) .and. (i == nxPML_1)) then
        ce_x_1(i) = 0.0_wp
     else
        ce_x_1(i) = sige_x_PML_1(i)*(be_x_1(i)-1.0_wp)/ &
           (sige_x_PML_1(i)+kappae_x_PML_1(i)*alphae_x_PML_1(i)) &
           / kappae_x_PML_1(i)
     endif
  enddo
  do i = 1,nxPML_1-1
     sigh_x_PML_1(i) = sig_x_max * ( (nxPML_1 - i - 0.5_wp)/(nxPML_1-1.0_wp))**m
     alphah_x_PML_1(i) = alpha_x_max*((i-0.5_wp)/(nxPML_1-1.0_wp))**ma
     kappah_x_PML_1(i) = 1.0_wp+(kappa_x_max-1.0_wp)* &
                           ((nxPML_1 - i - 0.5_wp) / (nxPML_1 - 1.0_wp))**m
     bh_x_1(i) = exp(-(sigh_x_PML_1(i) / kappah_x_PML_1(i) + &
                                alphah_x_PML_1(i))*dt/eps0)
     ch_x_1(i) = sigh_x_PML_1(i)*(bh_x_1(i)-1.0_wp)/ &
                 (sigh_x_PML_1(i)+kappah_x_PML_1(i)*alphah_x_PML_1(i)) &
                 / kappah_x_PML_1(i)
  enddo
  do i = 1,nxPML_2
     sige_x_PML_2(i) = sig_x_max * ( (nxPML_2 - i) / (nxPML_2 - 1.0_wp) )**m
     alphae_x_PML_2(i) = alpha_x_max*((i-1.0_wp)/(nxPML_2-1.0_wp))**ma
     kappae_x_PML_2(i) = 1.0_wp+(kappa_x_max-1.0_wp)* &
                                ((nxPML_2 - i) / (nxPML_2 - 1.0_wp))**m
     be_x_2(i) = exp(-(sige_x_PML_2(i) / kappae_x_PML_2(i) + &
                                alphae_x_PML_2(i))*dt/eps0)
     if ((sige_x_PML_2(i) == 0.0_wp) .and. &
        (alphae_x_PML_2(i) == 0.0_wp) .and. (i == nxPML_2)) then
        ce_x_2(i) = 0.0_wp
     else
        ce_x_2(i) = sige_x_PML_2(i)*(be_x_2(i)-1.0_wp)/ &
           (sige_x_PML_2(i)+kappae_x_PML_2(i)*alphae_x_PML_2(i)) &
           / kappae_x_PML_2(i)
     endif
  enddo
  do i = 1,nxPML_2-1
     sigh_x_PML_2(i) = sig_x_max * ( (nxPML_2 - i - 0.5_wp)/(nxPML_2-1.0_wp))**m
     alphah_x_PML_2(i) = alpha_x_max*((i-0.5_wp)/(nxPML_2-1.0_wp))**ma
     kappah_x_PML_2(i) = 1.0_wp+(kappa_x_max-1.0_wp)* &
                           ((nxPML_2 - i - 0.5_wp) / (nxPML_2 - 1.0_wp))**m
     bh_x_2(i) = exp(-(sigh_x_PML_2(i) / kappah_x_PML_2(i) + &
                                alphah_x_PML_2(i))*dt/eps0)
     ch_x_2(i) = sigh_x_PML_2(i)*(bh_x_2(i)-1.0_wp)/ &
                 (sigh_x_PML_2(i)+kappah_x_PML_2(i)*alphah_x_PML_2(i)) &
                 / kappah_x_PML_2(i)
  enddo
  do j = 1,nyPML_1
     sige_y_PML_1(j) = sig_y_max * ( (nyPML_1 - j ) / (nyPML_1 - 1.0_wp) )**m
     alphae_y_PML_1(j) = alpha_y_max*(((j*1.0_wp)-1.0_wp)/(nyPML_1-1.0_wp))**ma
     kappae_y_PML_1(j) = 1.0_wp+(kappa_y_max-1.0_wp)* &
                                ((nyPML_1 - j) / (nyPML_1 - 1.0_wp))**m
     be_y_1(j) = exp(-(sige_y_PML_1(j) / kappae_y_PML_1(j) + &
                                alphae_y_PML_1(j))*dt/eps0)
     if ((sige_y_PML_1(j) == 0.0_wp) .and. &
        (alphae_y_PML_1(j) == 0.0_wp) .and. (j == nyPML_1)) then
        ce_y_1(j) = 0.0_wp
     else
        ce_y_1(j) = sige_y_PML_1(j)*(be_y_1(j)-1.0_wp)/ &
           (sige_y_PML_1(j)+kappae_y_PML_1(j)*alphae_y_PML_1(j)) &
           / kappae_y_PML_1(j)
     endif
  enddo
  do j = 1,nyPML_1-1
     sigh_y_PML_1(j) = sig_y_max * ( (nyPML_1 - j - 0.5_wp)/(nyPML_1-1.0_wp))**m
     alphah_y_PML_1(j) = alpha_y_max*((j-0.5_wp)/(nyPML_1-1.0_wp))**ma
     kappah_y_PML_1(j) = 1.0_wp+(kappa_y_max-1.0_wp)* &
                           ((nyPML_1 - j - 0.5_wp) / (nyPML_1 - 1.0_wp))**m
     bh_y_1(j) = exp(-(sigh_y_PML_1(j) / kappah_y_PML_1(j) + &
                                alphah_y_PML_1(j))*dt/eps0)
     ch_y_1(j) = sigh_y_PML_1(j)*(bh_y_1(j)-1.0_wp)/ &
                 (sigh_y_PML_1(j)+kappah_y_PML_1(j)*alphah_y_PML_1(j)) &
                 / kappah_y_PML_1(j)
  enddo
  do j = 1,nyPML_2
     sige_y_PML_2(j) = sig_y_max * ( (nyPML_2 - j ) / (nyPML_2 - 1.0_wp) )**m
     alphae_y_PML_2(j) = alpha_y_max*(((j*1.0_wp)-1.0_wp)/(nyPML_2-1d0))**ma
     kappae_y_PML_2(j) = 1.0_wp+(kappa_y_max-1.0_wp)* &
                                ((nyPML_2 - j) / (nyPML_2 - 1.0_wp))**m
     be_y_2(j) = exp(-(sige_y_PML_2(j) / kappae_y_PML_2(j) + &
                                alphae_y_PML_2(j))*dt/eps0)
     if ((sige_y_PML_2(j) == 0.0_wp) .and. &
        (alphae_y_PML_2(j) == 0.0_wp) .and. (j == nyPML_2)) then
        ce_y_2(j) = 0.0_wp
     else
        ce_y_2(j) = sige_y_PML_2(j)*(be_y_2(j)-1.0_wp)/ &
           (sige_y_PML_2(j)+kappae_y_PML_2(j)*alphae_y_PML_2(j)) &
           / kappae_y_PML_2(j)
     endif
  enddo
  do j = 1,nyPML_2-1
     sigh_y_PML_2(j) = sig_y_max * ( (nyPML_2 - j - 0.5_wp)/(nyPML_2-1.0_wp))**m
     alphah_y_PML_2(j) = alpha_y_max*((j-0.5_wp)/(nyPML_2-1.0_wp))**ma
     kappah_y_PML_2(j) = 1.0_wp+(kappa_y_max-1.0_wp)* &
                           ((nyPML_2 - j - 0.5_wp) / (nyPML_2 - 1.0_wp))**m
     bh_y_2(j) = exp(-(sigh_y_PML_2(j) / kappah_y_PML_2(j) + &
                                alphah_y_PML_2(j))*dt/eps0)
     ch_y_2(j) = sigh_y_PML_2(j)*(bh_y_2(j)-1.0_wp)/ &
                 (sigh_y_PML_2(j)+kappah_y_PML_2(j)*alphah_y_PML_2(j)) &
                 / kappah_y_PML_2(j)
  enddo
  do k = 1,nzPML_1
     sige_z_PML_1(k) = sig_z_max * ( (nzPML_1 - k ) / (nzPML_1 - 1.0_wp) )**m
     alphae_z_PML_1(k) = alpha_z_max*((k-1)/(nzPML_1-1.0_wp))**ma
     kappae_z_PML_1(k) = 1.0_wp+(kappa_z_max-1.0_wp)* &
                                ((nzPML_1 - k) / (nzPML_1 - 1.0_wp))**m
     be_z_1(k) = exp(-(sige_z_PML_1(k) / kappae_z_PML_1(k) + &
                                alphae_z_PML_1(k))*dt/eps0)
     if ((sige_z_PML_1(k) == 0.0_wp) .and. &
        (alphae_z_PML_1(k) == 0.0_wp) .and. (k == nzPML_1)) then
        ce_z_1(k) = 0.0_wp
     else
        ce_z_1(k) = sige_z_PML_1(k)*(be_z_1(k)-1.0_wp)/ &
           (sige_z_PML_1(k)+kappae_z_PML_1(k)*alphae_z_PML_1(k)) &
           / kappae_z_PML_1(k)
     endif
  enddo
  do k = 1,nzPML_1-1
     sigh_z_PML_1(k) = sig_z_max * ( (nzPML_1 - k - 0.5_wp)/(nzPML_1-1.0_wp))**m
     alphah_z_PML_1(k) = alpha_z_max*((k-0.5_wp)/(nzPML_1-1.0_wp))**ma
     kappah_z_PML_1(k) = 1.0_wp+(kappa_z_max-1.0_wp)* &
                           ((nzPML_1 - k - 0.5_wp) / (nzPML_1 - 1.0_wp))**m
     bh_z_1(k) = exp(-(sigh_z_PML_1(k) / kappah_z_PML_1(k) + &
                                alphah_z_PML_1(k))*dt/eps0)
     ch_z_1(k) = sigh_z_PML_1(k)*(bh_z_1(k)-1.0_wp)/ &
                 (sigh_z_PML_1(k)+kappah_z_PML_1(k)*alphah_z_PML_1(k)) &
                 / kappah_z_PML_1(k)
  enddo
  do k = 1,nzPML_2
     sige_z_PML_2(k) = sig_z_max * ( (nzPML_2 - k ) / (nzPML_2 - 1.0_wp) )**m
     alphae_z_PML_2(k) = alpha_z_max*((k-1)/(nzPML_2-1.0_wp))**ma
     kappae_z_PML_2(k) = 1.0_wp+(kappa_z_max-1.0_wp)* &
                                ((nzPML_2 - k) / (nzPML_2 - 1.0_wp))**m
     be_z_2(k) = exp(-(sige_z_PML_2(k) / kappae_z_PML_2(k) + &
                                alphae_z_PML_2(k))*dt/eps0)
     if ((sige_z_PML_2(k) == 0.0_wp) .and. &
        (alphae_z_PML_2(k) == 0.0_wp) .and. (k == nzPML_2)) then
        ce_z_2(k) = 0.0_wp
     else
        ce_z_2(k) = sige_z_PML_2(k)*(be_z_2(k)-1.0_wp)/ &
           (sige_z_PML_2(k)+kappae_z_PML_2(k)*alphae_z_PML_2(k)) &
           / kappae_z_PML_2(k)
     endif
  enddo
  do k = 1,nzPML_2-1
     sigh_z_PML_2(k) = sig_z_max * ( (nzPML_2 - k - 0.5_wp)/(nzPML_2-1.0_wp))**m
     alphah_z_PML_2(k) = alpha_z_max*((k-0.5_wp)/(nzPML_2-1.0_wp))**ma
     kappah_z_PML_2(k) = 1.0_wp+(kappa_z_max-1.0_wp)* &
                           ((nzPML_2 - k - 0.5_wp) / (nzPML_2 - 1.0_wp))**m
     bh_z_2(k) = exp(-(sigh_z_PML_2(k) / kappah_z_PML_2(k) + &
                                alphah_z_PML_2(k))*dt/eps0)
     ch_z_2(k) = sigh_z_PML_2(k)*(bh_z_2(k)-1.0_wp)/ &
                 (sigh_z_PML_2(k)+kappah_z_PML_2(k)*alphah_z_PML_2(k)) &
                 / kappah_z_PML_2(k)
  enddo
  DA = 1.0_wp
  DB = (dt/(mu0)) 
  do i = 1,Imax
    do j = 1,Jmax
      do k = 1,Kmax-1
         CA(i,j,k) = (1.0_wp - sig(i,j,k)*dt / (2.0_wp*eps(i,j,k))) / &
            (1.0_wp + sig(i,j,k) * dt / (2.0_wp*eps(i,j,k)))
         CB(i,j,k) = (dt/(eps(i,j,k))) / &
            (1.0_wp + sig(i,j,k)*dt / (2.0_wp*eps(i,j,k)))
      enddo
    enddo
  enddo
  ii = nxPML_2-1
  do i = 1,Imax-1
     if (i <= nxPML_1-1) then
        den_hx(i) = 1.0_wp/(kappah_x_PML_1(i)*dx)
     elseif (i >= Imax+1-nxPML_2) then
        den_hx(i) = 1.0_wp/(kappah_x_PML_2(ii)*dx)
        ii = ii-1
     else
        den_hx(i) = 1.0_wp/dx
     endif
  enddo
  jj = nyPML_2-1
  do j = 1,Jmax-1
     if (j <= nyPML_1-1) then
        den_hy(j) = 1.0_wp/(kappah_y_PML_1(j)*dy)
     elseif (j >= Jmax+1-nyPML_2) then
        den_hy(j) = 1.0_wp/(kappah_y_PML_2(jj)*dy)
        jj = jj-1
     else
        den_hy(j) = 1.0_wp/dy
     endif
  enddo
  kk = nzPML_2-1
  do k = 1,Kmax-1
     if (k <= nzPML_1-1) then
        den_hz(k) = 1.0_wp/(kappah_z_PML_1(k)*dz)
     elseif (k >= Kmax+1-nzPML_2) then
        den_hz(k) = 1.0_wp/(kappah_z_PML_2(kk)*dz)
        kk = kk - 1
     else
        den_hz(k) = 1.0_wp/dz
     endif
  enddo
  ii = nxPML_2
  do i = 1,Imax-1
     if (i <= nxPML_1) then
        den_ex(i) = 1.0_wp/(kappae_x_PML_1(i)*dx)
     elseif (i >= Imax+1-nxPML_2) then
        den_ex(i) = 1.0_wp/(kappae_x_PML_2(ii)*dx)
        ii = ii-1
     else
        den_ex(i) = 1.0_wp/dx
     endif
  enddo
  jj = nyPML_2
  do j = 1,Jmax-1
     if (j <= nyPML_1) then
        den_ey(j) = 1.0_wp/(kappae_y_PML_1(j)*dy)
     elseif (j >= Jmax+1-nyPML_2) then
        den_ey(j) = 1.0_wp/(kappae_y_PML_2(jj)*dy)
        jj = jj-1
     else
        den_ey(j) = 1.0_wp/dy
     endif
  enddo
  kk = nzPML_2
  do k = 1,Kmax-1
     if (k <= nzPML_1) then
        den_ez(k) = 1.0_wp/(kappae_z_PML_1(k)*dz)
     elseif (k >= Kmax+1-nzPML_2) then
        den_ez(k) = 1.0_wp/(kappae_z_PML_2(kk)*dz)
        kk = kk - 1
     else
        den_ez(k) = 1.0_wp/dz
     endif
  enddo

end subroutine




end module
