module update_functions
use omp_lib
use vars
implicit none

contains

real(wp) function sin_source(ts,so_off,so_w,so_om)
implicit none
  real(wp), intent(in)	:: ts, so_off, so_w, so_om
  sin_source = sin(so_om*ts) * exp(-( (ts-so_off)/(so_w) )**2 )
end function sin_source

real(wp) function cos_source(ts,so_off,so_w,so_om)
implicit none
  real(wp), intent(in)	:: ts, so_off, so_w, so_om
  cos_source = cos(so_om*ts) * exp(-( (ts-so_off)/(so_w) )**2 )
end function cos_source

real(wp) function cont_sin_source(ts,so_off,so_w,so_om)
implicit none
  real(wp), intent(in)	:: ts, so_off, so_w, so_om
  if((ts-so_off).le.0.0_wp) then
	cont_sin_source = sin(so_om*ts) * exp(-( (ts-so_off)/(so_w) )**2 )
  else
	cont_sin_source = sin(so_om*ts)
  endif
end function cont_sin_source

real(wp) function cont_cos_source(ts,so_off,so_w,so_om)
implicit none
  real(wp), intent(in)	:: ts, so_off, so_w, so_om
  if((ts-so_off).le.0.0_wp) then
	cont_cos_source = cos(so_om*ts) * exp(-( (ts-so_off)/(so_w) )**2 )
  else
	cont_cos_source = cos(so_om*ts)
  endif
end function cont_cos_source

subroutine update_h_fields()
implicit none
  !$omp parallel sections &
  !$omp private(i,j,k,ii,jj,kk) &
  !$omp shared(ex,ey,ez,hx,hy,hz,den_ex,den_ey,den_ez,den_hx,den_hy,den_hz) &
  !$omp shared(bh_z_1,ch_z_1,bh_z_2,ch_z_2,bh_y_1,ch_y_1,bh_y_2,ch_y_2,bh_x_1,ch_x_1,bh_x_2,ch_x_2) &
  !$omp shared(be_z_1, ce_z_1, be_z_2, ce_z_2, be_y_1, ce_y_1, be_y_2, ce_y_2, be_x_1, ce_x_1, be_x_2, ce_x_2) &
  !$omp shared(psi_Ezx_1, psi_Ezx_2, psi_Hyx_1, psi_Hyx_2, psi_Ezy_1, psi_Ezy_2, psi_Hxy_1) &
  !$omp shared(psi_Hxy_2, psi_Hxz_1, psi_Hxz_2, psi_Hyz_1, psi_Hyz_2, psi_Exz_1, psi_Exz_2) &
  !$omp shared(psi_Eyz_1, psi_Eyz_2, psi_Hzx_1, psi_Eyx_1, psi_Hzx_2, psi_Eyx_2, psi_Hzy_1) &
  !$omp shared(psi_Exy_1, psi_Hzy_2, psi_Exy_2)

  !$omp section

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  UPDATE Hx
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   DO k = 1,Kmax-1
      DO i = 1,Imax-1
	   DO j = 1,Jmax-1
	      Hx(i,j,k) = DA * Hx(i,j,k) + DB *       &
			( (Ez(i,j,k) - Ez(i,j+1,k))*den_hy(j)  +    &
			  (Ey(i,j,k+1) - Ey(i,j,k))*den_hz(k) )
	   ENDDO
	ENDDO 
      DO i = 1,Imax-1
!.....................................................................
!  PML for bottom Hx, j-direction
!.....................................................................
         DO j = 1,nyPML_1-1
  	      psi_Hxy_1(i,j,k) = bh_y_1(j)*psi_Hxy_1(i,j,k)                 &
	 			+ ch_y_1(j) *(Ez(i,j,k) - Ez(i,j+1,k))/dy
            Hx(i,j,k) = Hx(i,j,k) + DB*psi_Hxy_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Hx, j-direction
!.....................................................................
         jj = nyPML_2-1
         DO j = Jmax+1-nyPML_2,Jmax-1
  	      psi_Hxy_2(i,jj,k) = bh_y_2(jj)*psi_Hxy_2(i,jj,k)       &
	 			+ ch_y_2(jj) *(Ez(i,j,k) -       &
                                Ez(i,(j+1),k))/dy
            Hx(i,j,k) = Hx(i,j,k) + DB*psi_Hxy_2(i,jj,k)
            jj = jj-1
         ENDDO
	ENDDO
   ENDDO
   DO i = 1,Imax-1
      DO j = 1,Jmax-1
!.....................................................................
!  PML for bottom Hx, k-direction
!.....................................................................
         DO k = 1,nzPML_1-1
  	      psi_Hxz_1(i,j,k) = bh_z_1(k)*psi_Hxz_1(i,j,k)            &
	 			+ ch_z_1(k) *(Ey(i,j,k+1) - Ey(i,j,k))/dz
            Hx(i,j,k) = Hx(i,j,k) + DB*psi_Hxz_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Hx, k-direction
!.....................................................................
         kk = nzPML_2-1
         DO k = Kmax+1-nzPML_2,Kmax-1
  	      psi_Hxz_2(i,j,kk) = bh_z_2(kk)*psi_Hxz_2(i,j,kk)             &
	 			+ ch_z_2(kk) *(Ey(i,j,k+1) -       &
                                Ey(i,j,k))/dz
            Hx(i,j,k) = Hx(i,j,k) + DB*psi_Hxz_2(i,j,kk)
           kk = kk-1
         ENDDO
	ENDDO
   ENDDO

  !$omp section

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  UPDATE Hy
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   DO k = 1,Kmax-1
      DO i = 1,Imax-1
	   DO j = 1,Jmax-1
            Hy(i,j,k) = DA * Hy(i,j,k) + DB *             &
			( (Ez(i+1,j,k) - Ez(i,j,k))*den_hx(i) +      &
			  (Ex(i,j,k) - Ex(i,j,k+1))*den_hz(k) )
	   ENDDO 
      ENDDO
      DO j = 1,Jmax-1
!.....................................................................
!  PML for bottom Hy, i-direction
!.....................................................................
         DO i = 1,nxPML_1-1
	      psi_Hyx_1(i,j,k) = bh_x_1(i)*psi_Hyx_1(i,j,k)                   &
				+ ch_x_1(i)*(Ez(i+1,j,k) - Ez(i,j,k))/dx
	      Hy(i,j,k) = Hy(i,j,k) + DB*psi_Hyx_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Hy, i-direction
!.....................................................................
         ii = nxPML_2-1
         DO i = Imax+1-nxPML_2,Imax-1
	      psi_Hyx_2(ii,j,k) = bh_x_2(ii)*psi_Hyx_2(ii,j,k)     &
				+ ch_x_2(ii)*(Ez(i+1,j,k) -    &
                                Ez(i,j,k))/dx
	      Hy(i,j,k) = Hy(i,j,k) + DB*psi_Hyx_2(ii,j,k)
            ii = ii-1
	   ENDDO
     ENDDO
   ENDDO
   DO i = 1,Imax-1
      DO j = 1,Jmax-1
!.....................................................................
!  PML for bottom Hy, k-direction
!.....................................................................
         DO k = 1,nzPML_1-1
	      psi_Hyz_1(i,j,k) = bh_z_1(k)*psi_Hyz_1(i,j,k)          &
				+ ch_z_1(k)*(Ex(i,j,k) - Ex(i,j,k+1))/dz
	      Hy(i,j,k) = Hy(i,j,k) + DB*psi_Hyz_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Hy, k-direction
!.....................................................................
         kk = nzPML_2-1
         DO k = Kmax+1-nzPML_2,Kmax-1
	    psi_Hyz_2(i,j,kk) = bh_z_2(kk)*psi_Hyz_2(i,j,kk)               &
				+ ch_z_2(kk)*(Ex(i,j,k) -    &
                                Ex(i,j,k+1))/dz
	    Hy(i,j,k) = Hy(i,j,k) + DB*psi_Hyz_2(i,j,kk)
            kk = kk-1
         ENDDO
     ENDDO
   ENDDO

  !$omp section

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  UPDATE Hz
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   DO k = 2,Kmax-1
      DO i = 1,Imax-1
        DO j = 1,Jmax-1
            Hz(i,j,k) = DA * Hz(i,j,k) + DB       &
                  * ((Ey(i,j,k) - Ey(i+1,j,k))*den_hx(i) +        &
			    (Ex(i,j+1,k) - Ex(i,j,k))*den_hy(j))
	   ENDDO
      ENDDO
      DO j = 1,Jmax-1
!.....................................................................
!  PML for bottom Hz, x-direction
!.....................................................................
         DO i = 1,nxPML_1-1
   	      psi_Hzx_1(i,j,k) = bh_x_1(i)*psi_Hzx_1(i,j,k)                 &
	 			+ ch_x_1(i) *(Ey(i,j,k) - Ey(i+1,j,k))/dx
	      Hz(i,j,k) = Hz(i,j,k) + DB*psi_Hzx_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Hz, x-direction
!.....................................................................
         ii = nxPML_2-1
         DO i = Imax+1-nxPML_2,Imax-1
   	      psi_Hzx_2(ii,j,k) = bh_x_2(ii)*psi_Hzx_2(ii,j,k)            &
	 			+ ch_x_2(ii) *(Ey(i,j,k) -       &
                                Ey(i+1,j,k))/dx
	      Hz(i,j,k) = Hz(i,j,k) + DB*psi_Hzx_2(ii,j,k)
            ii = ii-1
	   ENDDO
      ENDDO
      DO i = 1,Imax-1
!.....................................................................
!  PML for bottom Hz, y-direction
!.....................................................................
         DO j = 1,nyPML_1-1
            psi_Hzy_1(i,j,k) = bh_y_1(j)*psi_Hzy_1(i,j,k)                   &
				+ ch_y_1(j)*(Ex(i,j+1,k) - Ex(i,j,k))/dy
	      Hz(i,j,k) = Hz(i,j,k) + DB*psi_Hzy_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Hz, y-direction
!.....................................................................
         jj = nyPML_2-1
         DO j = Jmax+1-nyPML_2,Jmax-1
            psi_Hzy_2(i,jj,k) = bh_y_2(jj)*psi_Hzy_2(i,jj,k)               &
				+ ch_y_2(jj)*(Ex(i,j+1,k) -    &
                                Ex(i,j,k))/dy
	      Hz(i,j,k) = Hz(i,j,k) + DB*psi_Hzy_2(i,jj,k)
            jj = jj-1
	   ENDDO
      ENDDO
   ENDDO

  !$omp flush(hx,hy,hz)
  !$omp end parallel sections
end subroutine

subroutine update_e_fields()
implicit none
  !$omp parallel sections &
  !$omp private(i,j,k,ii,jj,kk) &
  !$omp shared(ex,ey,ez,hx,hy,hz,den_ex,den_ey,den_ez,den_hx,den_hy,den_hz) &
  !$omp shared(bh_z_1,ch_z_1,bh_z_2,ch_z_2,bh_y_1,ch_y_1,bh_y_2,ch_y_2,bh_x_1,ch_x_1,bh_x_2,ch_x_2) &
  !$omp shared(be_z_1, ce_z_1, be_z_2, ce_z_2, be_y_1, ce_y_1, be_y_2, ce_y_2, be_x_1, ce_x_1, be_x_2, ce_x_2) &
  !$omp shared(psi_Ezx_1, psi_Ezx_2, psi_Hyx_1, psi_Hyx_2, psi_Ezy_1, psi_Ezy_2, psi_Hxy_1) &
  !$omp shared(psi_Hxy_2, psi_Hxz_1, psi_Hxz_2, psi_Hyz_1, psi_Hyz_2, psi_Exz_1, psi_Exz_2) &
  !$omp shared(psi_Eyz_1, psi_Eyz_2, psi_Hzx_1, psi_Eyx_1, psi_Hzx_2, psi_Eyx_2, psi_Hzy_1) &
  !$omp shared(psi_Exy_1, psi_Hzy_2, psi_Exy_2)

  !$omp section

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  UPDATE Ex
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   DO k = 2,Kmax-1
      DO i = 1,Imax-1
	   DO j = 2,Jmax-1
	         Ex(i,j,k) = CA(i,j,k) * Ex(i,j,k) + CB(i,j,k) *       &
  			( (Hz(i,j,k) - Hz(i,j-1,k))*den_ey(j)  +    &
			  (Hy(i,j,k-1) - Hy(i,j,k))*den_ez(k) )
	   ENDDO
	ENDDO 
      DO i = 1,Imax-1
!.....................................................................
!  PML for bottom Ex, j-direction
!.....................................................................
         DO j = 2,nyPML_1
  	      psi_Exy_1(i,j,k) = be_y_1(j)*psi_Exy_1(i,j,k)                 &
	 			+ ce_y_1(j) *(Hz(i,j,k) - Hz(i,j-1,k))/dy
            Ex(i,j,k) = Ex(i,j,k) + CB(i,j,k)*psi_Exy_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Ex, j-direction
!.....................................................................
         jj = nyPML_2
         DO j = Jmax+1-nyPML_2,Jmax-1
  	      psi_Exy_2(i,jj,k) = be_y_2(jj)*psi_Exy_2(i,jj,k)       &
	 			+ ce_y_2(jj) *(Hz(i,j,k) -       &
                                Hz(i,(j-1),k))/dy
            Ex(i,j,k) = Ex(i,j,k) + CB(i,j,k)*psi_Exy_2(i,jj,k)
            jj = jj-1
         ENDDO
	ENDDO
   ENDDO
   DO i = 1,Imax-1
      DO j = 2,Jmax-1
!.....................................................................
!  PML for bottom Ex, k-direction
!.....................................................................
         DO k = 2,nzPML_1
  	      psi_Exz_1(i,j,k) = be_z_1(k)*psi_Exz_1(i,j,k)                 &
	 			+ ce_z_1(k) *(Hy(i,j,k-1) - Hy(i,j,k))/dz
            Ex(i,j,k) = Ex(i,j,k) + CB(i,j,k)*psi_Exz_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Ex, k-direction
!.....................................................................
         kk = nzPML_2
         DO k = Kmax+1-nzPML_2,Kmax-1
  	      psi_Exz_2(i,j,kk) = be_z_2(kk)*psi_Exz_2(i,j,kk)             &
	 			+ ce_z_2(kk) *(Hy(i,j,k-1) -       &
                                Hy(i,j,k))/dz
            Ex(i,j,k) = Ex(i,j,k) + CB(i,j,k)*psi_Exz_2(i,j,kk)
            kk = kk-1
         ENDDO
	ENDDO
   ENDDO


  !$omp section

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  UPDATE Ey
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   DO k = 2,Kmax-1
      DO i = 2,Imax-1
	   DO j = 1,Jmax-1
                 Ey(i,j,k) = CA(i,j,k) * Ey(i,j,k) + CB(i,j,k) *    &
			( (Hz(i-1,j,k) - Hz(i,j,k))*den_ex(i) +         &
			  (Hx(i,j,k) - Hx(i,j,k-1))*den_ez(k) )
	   ENDDO 
      ENDDO
      DO j = 1,Jmax-1
!.....................................................................
!  PML for bottom Ey, i-direction
!.....................................................................
         DO i = 2,nxPML_1
	      psi_Eyx_1(i,j,k) = be_x_1(i)*psi_Eyx_1(i,j,k)                   &
				+ ce_x_1(i)*(Hz(i-1,j,k) - Hz(i,j,k))/dx
	      Ey(i,j,k) = Ey(i,j,k) + CB(i,j,k)*psi_Eyx_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Ey, i-direction
!.....................................................................
         ii = nxPML_2
         DO i = Imax+1-nxPML_2,Imax-1
	      psi_Eyx_2(ii,j,k) = be_x_2(ii)*psi_Eyx_2(ii,j,k)              &
				+ ce_x_2(ii)*(Hz(i-1,j,k) -    &
                               Hz(i,j,k))/dx
	      Ey(i,j,k) = Ey(i,j,k) + CB(i,j,k)*psi_Eyx_2(ii,j,k)
            ii = ii-1
	   ENDDO
     ENDDO
   ENDDO
   DO i = 2,Imax-1
      DO j = 1,Jmax-1
!.....................................................................
!  PML for bottom Ey, k-direction
!.....................................................................
         DO k = 2,nzPML_1
	      psi_Eyz_1(i,j,k) = be_z_1(k)*psi_Eyz_1(i,j,k)                   &
				+ ce_z_1(k)*(Hx(i,j,k) - Hx(i,j,k-1))/dz
	      Ey(i,j,k) = Ey(i,j,k) + CB(i,j,k)*psi_Eyz_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Ey, k-direction
!.....................................................................
         kk = nzPML_2
         DO k = Kmax+1-nzPML_2,Kmax-1
	    psi_Eyz_2(i,j,kk) = be_z_2(kk)*psi_Eyz_2(i,j,kk)               &
				+ ce_z_2(kk)*(Hx(i,j,k) -    &
                                Hx(i,j,k-1))/dz
	    Ey(i,j,k) = Ey(i,j,k) + CB(i,j,k)*psi_Eyz_2(i,j,kk)
            kk = kk-1
         ENDDO
     ENDDO
   ENDDO

  !$omp section

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  UPDATE Ez
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   DO k = 1,Kmax-1
      DO i = 2,Imax-1
         DO j = 2,Jmax-1
            Ez(i,j,k) = CA(i,j,k) * Ez(i,j,k) + CB(i,j,k)       &
                  * ((Hy(i,j,k) - Hy(i-1,j,k))*den_ex(i) +        &
			    (Hx(i,j-1,k) - Hx(i,j,k))*den_ey(j))
	   ENDDO
      ENDDO
      DO j = 2,Jmax-1
!.....................................................................
!  PML for bottom Ez, x-direction
!.....................................................................
         DO i = 2,nxPML_1
   	      psi_Ezx_1(i,j,k) = be_x_1(i)*psi_Ezx_1(i,j,k)             &
	 			+ ce_x_1(i) *(Hy(i,j,k) - Hy(i-1,j,k))/dx
	      Ez(i,j,k) = Ez(i,j,k) + CB(i,j,k)*psi_Ezx_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Ez, x-direction
!.....................................................................
         ii = nxPML_2
         DO i = Imax+1-nxPML_2,Imax-1
   	      psi_Ezx_2(ii,j,k) = be_x_2(ii)*psi_Ezx_2(ii,j,k)       &
	 			+ ce_x_2(ii) *(Hy(i,j,k) -       &
                                Hy(i-1,j,k))/dx
	      Ez(i,j,k) = Ez(i,j,k) + CB(i,j,k)*psi_Ezx_2(ii,j,k)
            ii = ii-1
	   ENDDO
      ENDDO
      DO i = 2,Imax-1
!.....................................................................
!  PML for bottom Ez, y-direction
!.....................................................................
         DO j = 2,nyPML_1
            psi_Ezy_1(i,j,k) = be_y_1(j)*psi_Ezy_1(i,j,k)            &
				+ ce_y_1(j)*(Hx(i,j-1,k) - Hx(i,j,k))/dy
	      Ez(i,j,k) = Ez(i,j,k) + CB(i,j,k)*psi_Ezy_1(i,j,k)
         ENDDO
!.....................................................................
!  PML for top Ez, y-direction
!.....................................................................
         jj = nyPML_2
         DO j = Jmax+1-nyPML_2,Jmax-1
            psi_Ezy_2(i,jj,k) = be_y_2(jj)*psi_Ezy_2(i,jj,k)         &
				+ ce_y_2(jj)*(Hx(i,j-1,k) -    &
                                Hx(i,j,k))/dy
	      Ez(i,j,k) = Ez(i,j,k) + CB(i,j,k)*psi_Ezy_2(i,jj,k)
            jj = jj-1
	   ENDDO
      ENDDO
   ENDDO

  !$omp flush(ex,ey,ez)
  !$omp end parallel sections
end subroutine

subroutine runge_kutta_wells_x(e_in,p_in,f_in,pe_in)
  implicit none
  integer					:: o
  real(wp), dimension(:)			:: pe_in
  real(wp), dimension(:,:,:)			:: e_in
  real(wp), dimension(num_plates)		:: e0, e1, e2
  complex(wp), dimension(:)			:: p_in, f_in

  do o=1,num_plates
    e0(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2)
    e1(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) + &
	  ( e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) - pe_in(o) ) / 2.0_wp
    e2(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) + &
	  ( e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) - pe_in(o) )
    a1(o) = dt * ( -im * omega_gap * p_in(o) + im * dima/hbar * e0(o) * ( 1.0_wp - 2.0_wp * f_in(o) ) )
    a2(o) = dt * 2.0_wp * dima/hbar * imag( e0(o)*p_in(o) )
  enddo
  do o=1,num_plates
    b1(o) = dt * ( -im * omega_gap * ( p_in(o) + a1(o)/2.0_wp ) &
            + im * dima/hbar * e1(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + a2(o)/2.0_wp ) ) )
    b2(o) = dt * 2.0_wp * dima/hbar * imag( e1(o) * ( p_in(o) + a2(o)/2.0_wp ) )
  enddo
  do o=1,num_plates
    c1(o) = dt * ( -im * omega_gap * ( p_in(o) + b1(o)/2.0_wp ) &
            + im * dima/hbar * e1(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + b2(o)/2.0_wp ) ) )
    c2(o) = dt * 2.0_wp * dima/hbar * imag( e1(o) * ( p_in(o) + b2(o)/2.0_wp ) )
  enddo
  do o=1,num_plates
    d1(o) = dt * ( -im * omega_gap * ( p_in(o) + c1(o) ) &
            + im * dima/hbar * e2(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + c2(o) ) ) )
    d2(o) = dt * 2.0_wp * dima/hbar * imag( e2(o) * ( p_in(o) + c2(o)) )
  end do
  do o=1,num_plates
    p_in(o) = p_in(o) + ( a1(o) + (2.0_wp * b1(o)) + (2.0_wp * c1(o)) + d1(o) ) / 6.0_wp
    f_in(o) = f_in(o) + ( a2(o) + (2.0_wp * b2(o)) + (2.0_wp * c2(o)) + d2(o) ) / 6.0_wp
    pe_in(o) = e0(o)
  enddo
end subroutine

subroutine runge_kutta_wells_y(e_in,p_in,f_in,pe_in)
  implicit none
  integer					:: o
  real(wp), dimension(:)			:: pe_in
  real(wp), dimension(:,:,:)			:: e_in
  real(wp), dimension(num_plates)		:: e0, e1, e2
  complex(wp), dimension(:)			:: p_in, f_in

  do o=1,num_plates
    e0(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2)
    e1(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) + &
	  ( e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) - pe_in(o) ) / 2.0_wp
    e2(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) + &
	  ( e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) - pe_in(o) )
    a1(o) = dt * ( -im * omega_gap * p_in(o) + im * dima/hbar * e0(o) * ( 1.0_wp - 2.0_wp * f_in(o) ) )
    a2(o) = dt * 2.0_wp * dima/hbar * imag( e0(o)*p_in(o) )
  enddo
  do o=1,num_plates
    b1(o) = dt * ( -im * omega_gap * ( p_in(o) + a1(o)/2.0_wp ) &
            + im * dima/hbar * e1(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + a2(o)/2.0_wp ) ) )
    b2(o) = dt * 2.0_wp * dima/hbar * imag( e1(o) * ( p_in(o) + a2(o)/2.0_wp ) )
  enddo
  do o=1,num_plates
    c1(o) = dt * ( -im * omega_gap * ( p_in(o) + b1(o)/2.0_wp ) &
            + im * dima/hbar * e1(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + b2(o)/2.0_wp ) ) )
    c2(o) = dt * 2.0_wp * dima/hbar * imag( e1(o) * ( p_in(o) + b2(o)/2.0_wp ) )
  enddo
  do o=1,num_plates
    d1(o) = dt * ( -im * omega_gap * ( p_in(o) + c1(o) ) &
            + im * dima/hbar * e2(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + c2(o) ) ) )
    d2(o) = dt * 2.0_wp * dima/hbar * imag( e2(o) * ( p_in(o) + c2(o)) )
  end do
  do o=1,num_plates
    p_in(o) = p_in(o) + ( a1(o) + (2.0_wp * b1(o)) + (2.0_wp * c1(o)) + d1(o) ) / 6.0_wp
    f_in(o) = f_in(o) + ( a2(o) + (2.0_wp * b2(o)) + (2.0_wp * c2(o)) + d2(o) ) / 6.0_wp
    pe_in(o) = e0(o)
  enddo
end subroutine

subroutine runge_kutta_wells_z(e_in,p_in,f_in,pe_in)
  implicit none
  integer					:: o
  real(wp), dimension(:)			:: pe_in
  real(wp), dimension(:,:,:)			:: e_in
  real(wp), dimension(num_plates)		:: e0, e1, e2
  complex(wp), dimension(:)			:: p_in, f_in

  do o=1,num_plates
    e0(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2)
    e1(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) + &
	  ( e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) - pe_in(o) ) / 2.0_wp
    e2(o) = e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) + &
	  ( e_in(Imax/2,first_plate + (2*(o-1)*J_size),Kmax/2) - pe_in(o) )
    a1(o) = dt * ( -im * omega_gap * p_in(o) + im * dima/hbar * e0(o) * ( 1.0_wp - 2.0_wp * f_in(o) ) )
    a2(o) = dt * 2.0_wp * dima/hbar * imag( e0(o)*p_in(o) )
  enddo
  do o=1,num_plates
    b1(o) = dt * ( -im * omega_gap * ( p_in(o) + a1(o)/2.0_wp ) &
            + im * dima/hbar * e1(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + a2(o)/2.0_wp ) ) )
    b2(o) = dt * 2.0_wp * dima/hbar * imag( e1(o) * ( p_in(o) + a2(o)/2.0_wp ) )
  enddo
  do o=1,num_plates
    c1(o) = dt * ( -im * omega_gap * ( p_in(o) + b1(o)/2.0_wp ) &
            + im * dima/hbar * e1(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + b2(o)/2.0_wp ) ) )
    c2(o) = dt * 2.0_wp * dima/hbar * imag( e1(o) * ( p_in(o) + b2(o)/2.0_wp ) )
  enddo
  do o=1,num_plates
    d1(o) = dt * ( -im * omega_gap * ( p_in(o) + c1(o) ) &
            + im * dima/hbar * e2(o) * ( 1.0_wp - 2.0_wp * ( f_in(o) + c2(o) ) ) )
    d2(o) = dt * 2.0_wp * dima/hbar * imag( e2(o) * ( p_in(o) + c2(o)) )
  end do
  do o=1,num_plates
    p_in(o) = p_in(o) + ( a1(o) + (2.0_wp * b1(o)) + (2.0_wp * c1(o)) + d1(o) ) / 6.0_wp
    f_in(o) = f_in(o) + ( a2(o) + (2.0_wp * b2(o)) + (2.0_wp * c2(o)) + d2(o) ) / 6.0_wp
    pe_in(o) = e0(o)
  enddo
end subroutine

subroutine runge_kutta_ex_update(e_in,p_in,pold_in,elec_dens_3d_in)
  implicit none
  integer			:: o
  real(wp), intent(in)		:: elec_dens_3d_in
  real(wp), dimension(:,:,:)	:: e_in
  complex(wp), dimension(:)	:: p_in, pold_in

  do o=1, num_plates
	e_in( Imax/2 - I_size : Imax/2 + I_size, &
	      first_plate + (2*(o-1)*J_size) - J_size/2 : first_plate + (2*(o-1)*J_size) + J_size/2, &
	      Kmax/2 - K_size : Kmax/2 + K_size ) = &
	e_in( Imax/2 - I_size : Imax/2 + I_size, &
	      first_plate + (2*(o-1)*J_size) - J_size/2 : first_plate + (2*(o-1)*J_size) + J_size/2, &
	      Kmax/2 - K_size : Kmax/2 + K_size ) - &
	2.0_wp * dima * elec_dens_3d_in / (eps0 * epsR1) * ( real(p_in(o)) - real(pold_in(o)) )
    pold_in(o) = p_in(o)
  enddo
end subroutine runge_kutta_ex_update

subroutine runge_kutta_ey_update(e_in,p_in,pold_in,elec_dens_3d_in)
  implicit none
  integer			:: o
  real(wp), intent(in)		:: elec_dens_3d_in
  real(wp), dimension(:,:,:)	:: e_in
  complex(wp), dimension(:)	:: p_in, pold_in

  do o=1, num_plates
	e_in( Imax/2 - I_size : Imax/2 + I_size, &
	      first_plate + (2*(o-1)*J_size) - J_size/2 : first_plate + (2*(o-1)*J_size) + J_size/2, &
	      Kmax/2 - K_size : Kmax/2 + K_size ) = &
	e_in( Imax/2 - I_size : Imax/2 + I_size, &
	      first_plate + (2*(o-1)*J_size) - J_size/2 : first_plate + (2*(o-1)*J_size) + J_size/2, &
	      Kmax/2 - K_size : Kmax/2 + K_size ) - &
	2.0_wp * dima * elec_dens_3d_in / (eps0 * epsR1) * ( real(p_in(o)) - real(pold_in(o)) )
    pold_in(o) = p_in(o)
  enddo
end subroutine runge_kutta_ey_update

subroutine runge_kutta_ez_update(e_in,p_in,pold_in,elec_dens_3d_in)
  implicit none
  integer			:: o
  real(wp), intent(in)		:: elec_dens_3d_in
  real(wp), dimension(:,:,:)	:: e_in
  complex(wp), dimension(:)	:: p_in, pold_in

  do o=1, num_plates
	e_in( Imax/2 - I_size : Imax/2 + I_size, &
	      first_plate + (2*(o-1)*J_size) - J_size/2 : first_plate + (2*(o-1)*J_size) + J_size/2, &
	      Kmax/2 - K_size : Kmax/2 + K_size ) = &
	e_in( Imax/2 - I_size : Imax/2 + I_size, &
	      first_plate + (2*(o-1)*J_size) - J_size/2 : first_plate + (2*(o-1)*J_size) + J_size/2, &
	      Kmax/2 - K_size : Kmax/2 + K_size ) - &
	2.0_wp * dima * elec_dens_3d_in / (eps0 * epsR1) * ( real(p_in(o)) - real(pold_in(o)) )
    pold_in(o) = p_in(o)
  enddo
end subroutine runge_kutta_ez_update

subroutine PEC_slits()
implicit none

ex(1:29,68:72,1:Kmax) = 0.0_wp
ey(1:29,68:72,1:Kmax) = 0.0_wp
ez(1:29,68:72,1:Kmax-1) = 0.0_wp

ex(31:49,68:72,1:Kmax) = 0.0_wp
ey(31:49,68:72,1:Kmax) = 0.0_wp
ez(31:49,68:72,1:Kmax-1) = 0.0_wp

ex(51:Imax-1,68:72,1:Kmax) = 0.0_wp
ey(51:Imax,68:72,1:Kmax) = 0.0_wp
ez(51:Imax,68:72,1:Kmax-1) = 0.0_wp

!ex(Imax/2 + 5:Imax,68:72,1:Kmax) = 0.0_wp
!ey(Imax/2 + 5:Imax,68:72,1:Kmax) = 0.0_wp
!ez(Imax/2 + 5:Imax,68:72,1:Kmax) = 0.0_wp

end subroutine PEC_slits

subroutine PEC_cube()
implicit none

ex(1:Imax-1,1149:1151,1:Kmax) = 0.0_wp
ey(1:Imax,1149:1151,1:Kmax) = 0.0_wp
ez(1:Imax,1149:1151,1:Kmax-1) = 0.0_wp

!ex(31:49,68:72,1:Kmax) = 0.0_wp
!ey(31:49,68:72,1:Kmax) = 0.0_wp
!ez(31:49,68:72,1:Kmax-1) = 0.0_wp

!ex(51:Imax-1,68:72,1:Kmax) = 0.0_wp
!ey(51:Imax,68:72,1:Kmax) = 0.0_wp
!ez(51:Imax,68:72,1:Kmax-1) = 0.0_wp

!ex(Imax/2 + 5:Imax,68:72,1:Kmax) = 0.0_wp
!ey(Imax/2 + 5:Imax,68:72,1:Kmax) = 0.0_wp
!ez(Imax/2 + 5:Imax,68:72,1:Kmax) = 0.0_wp

end subroutine PEC_cube

subroutine mess_radar_echo(ex_in,ey_in,ez_in)
  implicit none
  real(wp), dimension(:,:,:), intent(in)	:: ex_in, ey_in, ez_in

!  test_interferenz_ex_x(:,260,Kmax/2) = test_interferenz_ex_x(:,260,Kmax/2) + abs(ex_in(:,260,Kmax/2))**2
!  test_interferenz_ey_x(:,260,Kmax/2) = test_interferenz_ey_x(:,260,Kmax/2) + abs(ey_in(:,260,Kmax/2))**2
!  test_interferenz_ez_x(:,260,Kmax/2) = test_interferenz_ez_x(:,260,Kmax/2) + abs(ez_in(:,260,Kmax/2))**2

!  test_interferenz_ex_y(:,100,Kmax/2) = test_interferenz_ex_x(:,100,Kmax/2) + abs(ex_in(:,100,Kmax/2))**2
!  test_interferenz_ey_y(:,100,Kmax/2) = test_interferenz_ex_x(:,100,Kmax/2) + abs(ey_in(:,100,Kmax/2))**2
!  test_interferenz_ez_y(:,100,Kmax/2) = test_interferenz_ex_x(:,100,Kmax/2) + abs(ez_in(:,100,Kmax/2))**2

!  test_interferenz_ex_z(Imax/2,:,:) = test_interferenz_ex_z(Imax/2,:,:) + abs(ex_in(Imax/2,:,:))**2
!  test_interferenz_ey_z(Imax/2,:,:) = test_interferenz_ey_z(Imax/2,:,:) + abs(ey_in(Imax/2,:,:))**2
!  test_interferenz_ez_z(Imax/2,:,:) = test_interferenz_ez_z(Imax/2,:,:) + abs(ez_in(Imax/2,:,:))**2
end subroutine mess_radar_echo

subroutine mess_interferenz(ex_in,ey_in,ez_in)
  implicit none
  real(wp), dimension(:,:,:), intent(in)	:: ex_in, ey_in, ez_in

!  test_interferenz_ex_x(:,260,Kmax/2) = test_interferenz_ex_x(:,260,Kmax/2) + abs(ex_in(:,260,Kmax/2))**2
!  test_interferenz_ey_x(:,260,Kmax/2) = test_interferenz_ey_x(:,260,Kmax/2) + abs(ey_in(:,260,Kmax/2))**2
!  test_interferenz_ez_x(:,260,Kmax/2) = test_interferenz_ez_x(:,260,Kmax/2) + abs(ez_in(:,260,Kmax/2))**2

!  test_interferenz_ex_y(:,100,Kmax/2) = test_interferenz_ex_x(:,100,Kmax/2) + abs(ex_in(:,100,Kmax/2))**2
!  test_interferenz_ey_y(:,100,Kmax/2) = test_interferenz_ex_x(:,100,Kmax/2) + abs(ey_in(:,100,Kmax/2))**2
!  test_interferenz_ez_y(:,100,Kmax/2) = test_interferenz_ex_x(:,100,Kmax/2) + abs(ez_in(:,100,Kmax/2))**2

  test_interferenz_ex_z(Imax/2,:,:) = test_interferenz_ex_z(Imax/2,:,:) + abs(ex_in(Imax/2,:,:))**2
  test_interferenz_ey_z(Imax/2,:,:) = test_interferenz_ey_z(Imax/2,:,:) + abs(ey_in(Imax/2,:,:))**2
  test_interferenz_ez_z(Imax/2,:,:) = test_interferenz_ez_z(Imax/2,:,:) + abs(ez_in(Imax/2,:,:))**2
end subroutine mess_interferenz

end module