program cpml_fdtd
use omp_lib
use vars
use update_functions
use fourier_functions
use write_functions
implicit none

call init_vars()
call output_init_data()

if(do_fdtd_routine==1) then
  open(1,file='OverTime.dat')
  open(2,file='Left_Radar_Echo.dat')
  open(3,file='Right_Radar_Echo.dat')
  do n=0, Nmax

  if(do_field_update==1) then

    call update_h_fields()

!    call runge_kutta_wells(ex,p_x,f_x,pe_x)
!    call runge_kutta_wells_y(ey,p_y,f_y,pe_y)
!    call runge_kutta_wells(ez,p_z,f_z,pe_z)

    call update_e_fields()

!    call PEC_slits()
!    call mess_interferenz(ex,ey,ez)

  endif !do fieldupdate

!  call runge_kutta_e_update(ex,p_x,pold_x,elec_dens_3d_x)
!  call runge_kutta_ey_update(ey,p_y,pold_y,elec_dens_3d_y)
!  call runge_kutta_e_update(ez,p_z,pold_z,elec_dens_3d_z)

  if(mod(n,spam_interval)==0) then; call spam_output(); endif

  if( (n.gt.4450).and.(n.lt.7000) ) then
	if(mod(n,snap_interval)==0) then; call gen_render(); endif
  endif

  ey(isource,jsource,ksource) = ey(isource,jsource,ksource) + &
	omega * cont_sin_source(((1.0_wp*n)+0.5_wp)*dt,source_offset,source_width,omega)
  hz(isource,jsource,ksource) = hz(isource,jsource,ksource) - &
	(omega/c0) * cont_cos_source(((1.0_wp*n))*dt,source_offset,source_width,omega)
!		source_amp_gauss * +0.5_wp

!  call fourier_left_echo(n, ez, edach_x_source, edach_x_mess_1, edach_x_mess_2, edach_x_mess_3, &
!			     edach_x_mess_4, edach_x_mess_5, edach_x_mess_6)
!  call fourier_right_echo(n, ez, edach_y_source, edach_y_mess_1, edach_y_mess_2, edach_y_mess_3, &
!			     edach_y_mess_4, edach_y_mess_5, edach_y_mess_6)

!  call fourier_y_axis(n, ez, edach_z_source, edach_z_mess_1, edach_z_mess_2, edach_z_mess_3, &
!			     edach_z_mess_4, edach_z_mess_5, edach_z_mess_6)

!  call PEC_cube()

!  if(mod(n,data_interval)==0) then
!	call output_fourier_data()
!	!call output_step_data(edach_z_source, edach_z_mess_1, edach_z_mess_2, edach_z_mess_3, &
!	!		      edach_z_mess_4, edach_z_mess_5, edach_z_mess_6)
!  endif

  call write_overtime()
!  if(n>2) then 
!	call make_all_movies()
!	exit
!  endif
  enddo !TimeLoop

!  call output_fourier_data()

  close(1)
  close(2)
  close(3)
  call make_all_movies()
  endif !do FDTD

close(10)
end program
