<?php

####################			remove all previous input/output files
$call = "rm -f new.db input go*";
system($call);

####################			look if we are running and where we are now if we do
$fp = @fopen("run","r");
if(!$fp) {
	$pic_count = 0;
	}
	else {
	$pic_count = fgets($fp);
	}
fclose($fp);

####################			generate new input from dat-files
print("Starting generation of input for PIC ".$pic_count.".\n\n");

$material ="plastic";
#$color = "204 127 50"; # <- golden
$color = "50 110 202"; # <- checkin'
$radius = "1.1";

$fp = fopen("input","w");
fwrite($fp,"units mm\n");
$sphere_count = 0;

for ($i=1;$i<10;$i++) {
	$y_count = 0;
	$result = nil;
	$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_000".$i.".dat";
	exec($call, &$result);
	foreach( $result as $v ) {
	  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$y_count." ".$v." ".$radius."\n";
	  fwrite($fp,$write_string);
	  $y_count++;
	  $sphere_count++;
	}
	$region_string = "r region".($i-1).".r";
	for($j=(($i-1)*$y_count);$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
	$region_string .= "mater region".($i-1).".r ".$material." ".$color." 0\n";
	$region_string .= "Z\n";
	$region_string .= "draw region".($i-1).".r\n";
	fwrite($fp,$region_string);
	$y_count = 0;
}

for ($i=10;$i<=80;$i++) {
	$y_count = 0;
	$result = nil;
	$call = "cat /mnt/numerik/fortran/parallel_CPML/src/Test_out_00".$i.".dat";
	exec($call, &$result);
	foreach( $result as $v ) {
	  $write_string = "in sp".$sphere_count.".s sph ".$i." ".$y_count." ".$v." ".$radius."\n";
	  fwrite($fp,$write_string);
	  $y_count++;
	  $sphere_count++;
	}
	$region_string = "r region".($i-1).".r";
	for($j=(($i-1)*$y_count);$j<$sphere_count;$j++) {
		$region_string .= " u sp".$j.".s";
	}
	$region_string .= "\n";
	$region_string .= "mater region".($i-1).".r ".$material." ".$color." 0\n";
	$region_string .= "Z\n";
	$region_string .= "draw region".($i-1).".r\n";
	fwrite($fp,$region_string);
	$y_count = 0;
}

$comb_string = "comb net";
for ($i=1;$i<=80;$i++) {
	$comb_string .= " u region".($i-1).".r";
}
$comb_string .= "\n";
fwrite($fp,$comb_string);

#fwrite($fp,"view quat 0.25 0.25 0.25 0\n");
fwrite($fp,"view quat 0.248097349046 0.476590573266 0.748097349046 0.389434830518\n");
fwrite($fp,"Z\n");
fwrite($fp,"draw net\n");
fwrite($fp,"autoview\n");
fwrite($fp,"zoom 7.98\n");
fwrite($fp,"saveview go");
fclose($fp);

####################			invoke MGED to create the DB for the renderer
$call = "nice -n 10 mged -c new.db < input";
system($call);

####################			run the generated script
$call = "nice -n 10 ./go";
system($call);

####################			now gen the .png from the .pix file
$call = "nice -n 10 pix-png go.pix > ".$pic_count.".png";
system($call);

####################			finally advance pic_count and note it for the next run
$pic_count++;
$fp = fopen("run","w");
fwrite($fp,$pic_count);
fclose($fp);
?>