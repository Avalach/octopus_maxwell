(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     16715,        448]
NotebookOptionsPosition[     15631,        404]
NotebookOutlinePosition[     15966,        419]
CellTagsIndexPosition[     15923,        416]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"\[Phi]1", "[", 
   RowBox[{"r_", ",", "Z_", ",", "a0_"}], "]"}], "=", 
  RowBox[{
   RowBox[{"Sqrt", "[", 
    RowBox[{"1", "/", 
     RowBox[{"(", 
      RowBox[{"4", "*", "\[Pi]"}], ")"}]}], "]"}], "*", 
   RowBox[{"Sqrt", "[", 
    RowBox[{"4", " ", 
     RowBox[{
      RowBox[{"Z", "^", "3"}], "/", 
      RowBox[{"a0", "^", "3"}]}]}], "]"}], "*", 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", "Z"}], " ", 
     RowBox[{"r", "/", "a0"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.60482646383425*^9, 3.604826544545178*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"r", " ", "Z"}], "a0"]}]], " ", 
   SqrtBox[
    FractionBox[
     SuperscriptBox["Z", "3"], 
     SuperscriptBox["a0", "3"]]]}], 
  SqrtBox["\[Pi]"]]], "Output",
 CellChangeTimes->{3.604826847815946*^9, 3.604826962404131*^9, 
  3.604827773579564*^9, 3.604827809932605*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Phi]2", "[", 
   RowBox[{"r_", ",", "Z_", ",", "a0_"}], "]"}], "=", 
  RowBox[{
   RowBox[{"Sqrt", "[", 
    RowBox[{"1", "/", 
     RowBox[{"(", 
      RowBox[{"4", "*", "\[Pi]"}], ")"}]}], "]"}], "*", 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"Z", "^", "3"}], "/", 
     RowBox[{"(", 
      RowBox[{"8", " ", 
       RowBox[{"a0", "^", "3"}]}], ")"}]}], "]"}], 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "Z"}], " ", 
      RowBox[{"r", "/", "a0"}]}], "+", "2"}], ")"}], "*", 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", "Z"}], " ", 
     RowBox[{"r", "/", 
      RowBox[{"(", 
       RowBox[{"2", " ", "a0"}], ")"}]}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.604826546216237*^9, 3.6048265837703657`*^9}, {
  3.604826760922179*^9, 3.6048267968720427`*^9}, {3.604827527300747*^9, 
  3.604827529575281*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"r", " ", "Z"}], 
      RowBox[{"2", " ", "a0"}]]}]], " ", 
   SqrtBox[
    FractionBox[
     SuperscriptBox["Z", "3"], 
     SuperscriptBox["a0", "3"]]], " ", 
   RowBox[{"(", 
    RowBox[{"2", "-", 
     FractionBox[
      RowBox[{"r", " ", "Z"}], "a0"]}], ")"}]}], 
  RowBox[{"4", " ", 
   SqrtBox[
    RowBox[{"2", " ", "\[Pi]"}]]}]]], "Output",
 CellChangeTimes->{3.604826847850568*^9, 3.60482696248869*^9, 
  3.604827530515655*^9, 3.604827773697176*^9, 3.6048278100497026`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"4", " ", "\[Pi]", " ", 
    RowBox[{"r", "^", "2"}], " ", 
    RowBox[{"\[Phi]1", "[", 
     RowBox[{"r", ",", "1", ",", "1"}], "]"}], "*", 
    RowBox[{"\[Phi]1", "[", 
     RowBox[{"r", ",", "1", ",", "1"}], "]"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"r", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.604826810516637*^9, 3.604826846065377*^9}, {
  3.604827323466609*^9, 3.604827324921155*^9}, {3.6048278075088167`*^9, 
  3.604827808468424*^9}}],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{3.6048268486963778`*^9, 3.604826962577643*^9, 
  3.604827325602207*^9, 3.6048275319930677`*^9, 3.60482777376877*^9, 
  3.604827810116354*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{"Sin", "[", "\[Theta]", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"\[Theta]", ",", "0", ",", "\[Pi]"}], "}"}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Phi]", ",", "0", ",", 
     RowBox[{"2", " ", "\[Pi]"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.604826906967577*^9, 3.604826982598337*^9}}],

Cell[BoxData[
 RowBox[{"4", " ", "\[Pi]"}]], "Output",
 CellChangeTimes->{{3.604826962650247*^9, 3.604826983281836*^9}, 
   3.604827532229619*^9, 3.60482777380802*^9, 3.604827810198019*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"4", " ", "\[Pi]", " ", 
    RowBox[{"r", "^", "2"}], "*", 
    RowBox[{"\[Phi]2", "[", 
     RowBox[{"r", ",", "1", ",", "1"}], "]"}], "*", 
    RowBox[{"\[Phi]2", "[", 
     RowBox[{"r", ",", "1", ",", "1"}], "]"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"r", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6048270700047903`*^9, 3.60482707286524*^9}, {
  3.604827329931579*^9, 3.604827331063314*^9}, {3.60482741857576*^9, 
  3.604827419380515*^9}, {3.60482779742837*^9, 3.6048277995298367`*^9}}],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{3.6048270754477*^9, 3.604827332841249*^9, 
  3.6048275343056*^9, 3.604827773863804*^9, 3.6048278102591887`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"\[CapitalPsi]", "[", 
   RowBox[{"r_", ",", "Z_", ",", "a0_"}], "]"}], "=", 
  RowBox[{
   RowBox[{"1", "/", 
    RowBox[{"Sqrt", "[", "2", "]"}]}], 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"\[Phi]1", "[", 
      RowBox[{"r", ",", "Z", ",", "a0"}], "]"}], "+", 
     RowBox[{"\[Phi]2", "[", 
      RowBox[{"r", ",", "Z", ",", "a0"}], "]"}]}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.604827670017166*^9, 3.6048277190031757`*^9}, {
  3.604827815710384*^9, 3.60482784780198*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   FractionBox[
    RowBox[{
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"-", 
       FractionBox[
        RowBox[{"r", " ", "Z"}], "a0"]}]], " ", 
     SqrtBox[
      FractionBox[
       SuperscriptBox["Z", "3"], 
       SuperscriptBox["a0", "3"]]]}], 
    SqrtBox["\[Pi]"]], "+", 
   FractionBox[
    RowBox[{
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"-", 
       FractionBox[
        RowBox[{"r", " ", "Z"}], 
        RowBox[{"2", " ", "a0"}]]}]], " ", 
     SqrtBox[
      FractionBox[
       SuperscriptBox["Z", "3"], 
       SuperscriptBox["a0", "3"]]], " ", 
     RowBox[{"(", 
      RowBox[{"2", "-", 
       FractionBox[
        RowBox[{"r", " ", "Z"}], "a0"]}], ")"}]}], 
    RowBox[{"4", " ", 
     SqrtBox[
      RowBox[{"2", " ", "\[Pi]"}]]}]]}], 
  SqrtBox["2"]]], "Output",
 CellChangeTimes->{
  3.6048277738686867`*^9, {3.6048278103126383`*^9, 3.6048278482930403`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{"4", " ", "\[Pi]", " ", 
    RowBox[{"r", "^", "2"}], "*", 
    RowBox[{"\[CapitalPsi]", "[", 
     RowBox[{"r", ",", "1", ",", "1"}], "]"}], "*", 
    RowBox[{"\[CapitalPsi]", "[", 
     RowBox[{"r", ",", "1", ",", "1"}], "]"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"r", ",", "0", ",", "\[Infinity]"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6048277247499723`*^9, 3.604827771441237*^9}, {
  3.604827803192234*^9, 3.604827832557165*^9}}],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{
  3.6048277741517963`*^9, {3.604827810534956*^9, 3.6048278495507727`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"\[CapitalPsi]", "[", 
    RowBox[{"r", ",", "1", ",", "1"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"r", ",", "0", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6048281322788887`*^9, 3.604828190807928*^9}, {
  3.604828266324904*^9, 3.6048282833765907`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwV13c8V98fB3B7j4/P/VApqw9pGZU0xH1TKUVFNEQyy/4ooig763PtIkIk
UWRkrxORPZKVVV+JqGSVVfnd31/38Xzcc+95nMf7fV7nXhkLZwNrNhYWlkus
LCz/v2pemvWuSfqBE06WnT6mzTifTID8x/0zuK5s+i3Hzw1446EopbtHZ3Ar
GncRo70Bv3cheZ+EwQzeFzH8xL+sAWeLLD12yX4Gv8ri6NQf0YCv/Pt+tS9p
Bg8NenT7kHoD/m3IKLODbRZ/zjTPGEt4i7fGy2973TqLG6k2vLhmXo+HU1q2
p16Zx4UMPRbm17/By/ZJFJ1ymsctU99d3Mj1Bh+77Iz/8ZzHey9oSxvM1+Jq
OZjh+bh5fOry6cc/2mrxrzomdwTb53EzvkiJQf9a/LD/j45bagu40cj6/MC5
Gnzpl7CbgdgvnPF4XLNx4DVuMWiI2Ft/45/99MKUs6pwvsT/HF0+/Mbt+OUf
CoRV4QXGTps+jv/GY46/4GB1rMLZB4JulbMu4lOaLVm7d1Xh6f3le1z2LeJn
Puyhl5RX4uM90pkjaYu4f2JHiUpXBW7X+T2y7NYSHrZ8jxFALcdd6gMsnOVX
8OWzuSkF3cW45Jg21+89KzhHftfrmqpivIWN97kXrOC8jdlmoxnFuBwQsyEX
SWuajp2/VYx/KL/vkx62gif4hmdTpYpxzbyM5A8/V/B6znMOlY5FuEhi48Dh
0lVcIm6DKHNdIZ7PEDi7XucvPuOti8Uk5OOtCditnHN/8QBNtQ0C9/Lx8Trx
FC2rv7inh7/eA5d8XHzDtm8O3n9x5ZA8/pHj+XhAzVH/mqK/OKdBvt2BpTz8
HNX7ld3mf7jnc09VmnEevvJqFqta/YdzuOUZsxi+xFM97aeUz7NA93y7KOdM
Fq78mncpyJQFDh4x7vjTmYW/5sjk/GjJAqVqHxQFC7LwT8SYNMFggYiPXPHu
rlm45GPT85MhLGDMd0TaZSUTT6g7XZdayQJfeI8lTvBl4tGCKsnUzazgoqRu
Y6uegful/NFf+M4Kne1HrGW+pOFnlEtddsyzwvT+twuHWtNwyZobURbLrCCR
oaDl8CoNLx+d6uzkZINlODHC6ZeGz8l9OJ0jyQZ62fIxdOk03CK7SM/mDBsE
5Ii3/DFLxTXLnHT6X7FBY5JGfPhgCs7y/hNU3mYHrjXFsTd3EvCAyerU277s
YOoZsxStn4DzsiSxHQhmh8L7CuPBcgk4VeFiXdF9dnC4fbT7a9tDXC7o3bHc
PHaQ7h5+5Sb9ED+hVnsqbZwdlvdIhWa1xuExT56YBBtwgFGV+vWXcB/f4mrj
YbidEzLjLleJR0XiCgc/nk1R5gR/9fYPmxiRuArLBaUpVU4Ic2lqOXg6Etdi
6ox7H+aE9qmTH5sEI/HL6TsNX5hwwqNpQWnr9RH4g+45JbYIcry1rfaBTQTO
udd7IneeE8TtFMLunwzBx37FneOv4oKMksrpSfDD0zT33Bh+wwV/z7lHqXD7
4WZEe0RuMxdIpKo9L2rzxQdkOZvO9nPBfvu/kcaXfPF3Z6+rPZrngjQtW5/t
t3zw6nxdaYXt3JC7JOvmeecOHufIOnUqjhuO1EllSPx3Ezcse8Qlk8wNbwNk
K3Nu38SpnPvp8+ncMPalye4x7SYe/sjJJK6AG97lDZzP1nHDA1oG2z+2ccOb
VzcnjEpv4Ixtxa8YHDxwgJg3yH7GwE98sbsT7cIDM5vXi2THW+Pad+jZRzx4
oIDt69GEcStcS3Ro4PddHvj37Yqryl4r/OBRvf2XmOT9mCNpdt0W+PanSvP0
Zzwwu6c6pkP8Cs5vvXCtaIgHJNzv6QqHG+GtY3cM+4/xAgM3/spQwfBGL1X/
0FO84Nc+x2Nlh6rraD/zDxnxQtKyrrHOJhqqPHJFONWCF9Ykpre9iFBA2ela
zbZ3eKFumkuhmVsTEVbcsFrAC1/f71LunNZHp8Yid0hK8oHe48eLT+fNUeuO
sW4TWT749PII5YSbBTp5Y9/dxO18MPa6zezGbwt0nG2kc/0+PuhTjxA6v2iJ
tKR33MTO8MHGiOkQh3lrtNek/jWPHx8M8D3cLNljizZ1LxvNf+EDdZXnuM9j
BkrYqLe26zsf7DyreH71AwNtsHycyZjjA1XTzZ4amAsSm9Ne/fGPD3YkJIZL
BLogCiX28dd1/OBSAraHba4jdl3Fb8M6/HBT50C5vqQrmnpj4dOYww8beAst
omzckVRUv114IT+YiVsdykhyR4aXTxkZVvBDh76snHe3O6peOrD9UyM/CIZS
xu9oeqBoBZHuxc/8cLpgm2Ph+ltILQ7Jy4sLQGcNGClV3kZMu00dgfcEQPGB
usmT/juoZl902UlCAPK7we8+z130m4MnXSRWAKzujQTo77+LrqTMeySlCsBV
k4BO+bi7SKW7eXNRpQAs5FudMjDwRsPqt26OzQnALZq3s4WZD1IU6ZU4bCYI
HVo7qtJLfdE4Me5331oQYGhxlb3VFyXxLU5M2AtCVtzojN5HXyTAsb6A6SEI
fVHmGmmcfmjq90Xt3ihByJ1+pnhK3w89HRp2sq0ThKW29hs1Y35oU+YXFLFN
COQq3lf6//FH3bK/ZUeVhKA0SCbsgmAACkvlClVRFYJa3WAOcckAtJIgb/hB
SwhaUixULPEA1E/Yft1sIgRLfdpbKb4BKObGD0pxuBAU9R3R+I8lEPHhv8yH
5oXgh73pbN1cIPLQ3MpTvCIEG7fra/uy3UPjhy+9jGAVBp5X8hzbqPdQ7fGa
FS1hYZiz4GRR23UP3TYgYrK2C8OYmf6QndM99M1arv6muTAkSR8U5/1yD7Uy
jbaKdAjD4GHNhMI3QehgRHD7VI8wtBlsrv3XGYQyoypc64aE4dOspY3qSBAK
eCBT4z4lDG6XFSydloKQZOfvnT/ZKOA8ovyFbWcw6v1QW28kSQHB5zrJLRHB
6NgP48XNRhTI+1CKaZ8IQck0//feFyig39v3c5tRCFpQe5E7dIkCvVcN6v6a
haDHoatXH1hQQHgiienmFoJW5B/18zIosBK6jfYsJQRlWwyXzYRS4IHnw+cS
syGI0m/mVf2aAk2RxkcLg0ORzVrQhY11FCje+6AkLyoUVW7JU/FooICKjVZN
ckIosnVj+bGrnQLzKdqtp7NDUS011fTpIAWGWezXc3eEIle9UQ3mbwrk2u7c
0iMShvprrViNd4pA7rm2zVpEGJrRHBC/oyQClUHUnQKxYYi75rTK490iEOjC
Nd+cEIZU0cGr4/tFYOyh48iWzDAUW0Fpu35UBNLP/956sTYMnS6sjA+7LALd
Cv9Z5y6EIZs9uwtemouAmdQpv6aVMHS34FnLOysRcD9k8K2fhYmy86L/rbMX
AeVff+v7BJiIL+eaVbo7OZ6ZGHlOlonePqUpV0WKwK9HiX/M9JloRDZU51OM
CMRB67euc0z068maBXucCFgdPJanZsJEsmlT93WSRGBgz82BaRsm8kt+vdqT
JQL1Jz4nDnkykUacQ8N0rQi4Jq9jHX7CREXBdZelf4nAXM6RuKzvTJR5omV9
0JIItH7tHdWcZaIEga6uH6sioBJmfq3jFxP5RH7UrmCjgmaxxvHmf0ykG7+i
eI5ChQZtpi1QCDT6THktbAcVnHwrCdVdBOqx3Vc2p0gFv9rQxS17CdS4Q+PG
xd1UCAlx2SN4gEA5uScnthygQnFGlHYFEMijxKajRpsKQ+tbBpNPE0io4VHK
ojkV+j0sS8PsCcQa8uTiZWsqRFltYA47EWjhxHOs/hoVykL7a+WuE+hDe0lQ
tDMVTIwrvB56ECi9t8tZ4Q4VzPfGrBMOJNDBcR6wjKfCG3sb74OPCKSQKbzS
lEgF0VzV+N/JBJK2EytUTqFC91u55KxUAnFO07f+e0oFuWd/oxcyCNT5S4Py
8BUVZFxc+aTyCWTD6fapvZ0KYkqmdm1vCCQu1dS3+o4KhPkdu756ArXvl+jY
2kOFgxnj8x8aCKTqUF/lN0iFGceMysYWAnG9F01U/UqFpuUzg3rvCVT+3Tba
8hsVHgroD27qIZATV3VI5DQVhpcPx4/1Eqj3gI3H1AIVPsbctbg8QKCnj4uN
klkxWEnImlz9RKAL5Xx6LRwYpE++8YsaJZBA9+UjS9wYTHfq80uPEciVm3uP
gRAGzLV89q0TBDridJ7CtRGDY9iUxth3Ai0FveDeI4kB5yF5Vq1pAmWnrv0z
k8FgVEJ1LP4ngWg9z36UyWOQqEKX2z1HoDG1pWZHFQySz5WrYYsEijfSrU3Y
h4G0lOzLg0sE0nV+XNZwEIO/9qkul5YJVJh2PFNGE4NK63T2kFUCBfA+DOzR
w+BGyO3l2DUC7af/8GLTx2AkQy8hiCUcfT+k6apkiMHyZ7OKG6zhyJAxaRFi
jEHd4Xv397OHI9m+A6B+FQNXp7aTblzhqH+G2Gdnh4HovueRKtzhiMk3qhjn
SM5H/Of/g/SCeqjE7A0MCsUkmbq84Sjz/AhN0h2DLZwe2XOkTVx2C5y8Ta5n
rScqmi8c1aUPrKT7YOC0T7wW8Ycjj2rFuXf+GGgcUVmvJxCOdvb7Tf67h0E7
a7lON+lY/h39FwgMcg0OG3UKhqPjct4dgZEYWMoaHzwqFI7+aLx/WxCDwdFt
7CKFpC2vexYJJGAQPsWZ5S0cjlR3jxRtS8KAAqH5A6T55qBY+zEGR2RG/ihR
wtFw/pNiyycYBHdJR3qTznPhKvHJwKD5uyOjibT/LtuSpCwMJK8MpwmKhKNz
sy0l5dkYKN3036lLelu+YmlfLgZN3QxqIOk/jKjShQIMTvsUXCwl3aG8UCpS
jMH+EX3ucdJpM+fKFMswiCoxlBaihqObeWVlJysxsFGrylImrcPYVH4NYaDL
GpqpR3qTsnd5YC0G1fzFMtakZ37+V55Wj8HT5v3i7qTf5B6pQI1kPSS44vxJ
P3B+VjHUgoHAeaGkUNK2SnyVy+0Y6Klr7GKSPvTToVKsC4MLQt4XgklTcjsq
9/RgcN+3eJ036c9Ou6vO9GNg0fz2BoN0seL9KsdBDK4nhLuYkA6dXqwKHcEA
l50UPUza9KVx9bP/MHCQeGktS1rZqaq6bgwDiZp8axbS7IrS6L8JDKy76sT7
yPX3/vBD/6YweJRcGJJJOivnC9o4jUHV2okSV9Jejsdf75/FoO3yqUw10qcV
Xrw2WsCAXuxr/pesx+YfgjXXFzHoikmfKCPd5PC+JvsvBmz8GY500o92qtY2
sdBAsXTuZidZb8b3+NpxdhrkrOO/4kFazOHyG2k+GvgNG42Vkv0yuaPmjbog
DZQNBoP1SVd+o9cZU2jgm9Al9YXsNwv7ybpYMRpEzMTt+0v2494duvX5G2gw
fSa5xpc0z7eX9e2baJCrGHSGlXSunetbHjoNSh7qRsyS/f7Hdq3BS4kGu0PY
KXE8ZD9ss2h8uJsG+6yiYYHcP2mTdY3Fe2lwclo7QZe0jm1o04waDVw6OQan
Ocl6XhNtsTpOg+aOi0IrbGQ9t3q0+J6kQfdQAccu0oe+DrQkn6LBthdPj1mS
+3n06uPWfkMa1PD31pWR+1/p6o52XXMaSExJiSn9IdCIrZRzjhUNtk5Weu8k
84LpgFGErtGg/YxOqOwKgSZdVvU7nGggOMMWz0HmzROvlh59Lxp8dG9VCJwn
0Lpo++FzcTQI4GHMMycJ9DbW7G5JAg2kr07Nqn4l8zDurNT6ZBqk1xSOD44T
6N0jNfMP6TQoE2RpFCPzMuwZ/5dLBTSIVV9dPjNCoLXKF9+vtNGgb+x+9OA7
Ar1EKURNJw3E9/w22ddJIJPaGMXN3TQovfnDKbydQGUNnozPAzRIKLh6R5HM
+xtdJxesv9JA/6t0u2odgSYmvq3asYuCp2nuXYciAnVQd/K7HRCFrpwSIyKW
QBqOjpOJh0RB9HIhn280OV/Dy4ZaXBT2T0VtcYkkULjXrgCKtihcY3inajPJ
vB1X/fvCQBTm3L14G/wJ1Fym+XPUXhQUXdbeSJPnY/2V8+/PJIuCnKKL7Rld
AqlUxOe7p4oCRwze26dDnpeiAxHJ6aKQpumwweQYmc/NJrrfn4vCspqc8iUt
AmmpWNQHlZDOdljds59Ar7kdS6o7RWHbJbYxAzqBKl76Je5kFwMv09uGJotM
xKVrJB7HJQa5ndSCYwtMpD8p/5CVTwx8Mg50KpHfFxP0tge9FDE49yja9PcU
E2EP10X7SIrB5z5LedMRJrL3zw7uPiAGik4/62LrmGjjhV43L4YYLKglz9eH
M5En6/YzrcNiEKv5ZHRBnPx+Grp93KlwHdiMj1VslAhDXzJkYlO81sOL45+s
fNaHotitGzjKjDZAozxIXfsejCITbBrMpcUB63sYpdEchIxjTdsFJsShWnj1
vXrWPbR05OlySvVG6EuJOTHqFIg+f9HyqA3aBHHqG40kdANQmvqujW3mEiCi
6s8XLOaPcgLdRK4qSAKLXVHO2z++KMjI8iT7L0kYWJm1HJrxQXuHUrOC2qRg
a+JQo0++N4rflpjTdlIazuygRk9X3EHer4n4k2XScDYy9mZNoSfS22j0tFlW
Bp6KE6sXOm8hTaeGSyIhMpDHV5GgR/dAKZssZFt/ycB2Y533k3Y3keybZw9O
XdoM3+8vFDeMu6JIhdrtXdWb4ZCspOtV7xvIZp7jXdN2Oui75O0LoFxHFvlz
0iERdDCOc90r0sFADn5n/5yIooOulFtMWhMDuZ0t7BOIoYO7zHPXvXUMFPzL
LTzyAR2+e6YiszIGenlgeTUuiQ4lI2cYb58w0HLNWm/GC/L91l0sQx4MFNkl
QNQ10CH9xciqnCz5//rE8dq9JjpA4AW1CUkGeuLafvh4Cx1Cr1r/ytrAQMVi
kSst7XTYNSvVv0eYgYaMsWvve+jQe7DnccGCM5L/vOHw6Gc6aBAeST2pzki5
8LZk+hc6SLwe2RkU54wOBg4uW0/QgT8rMVONcEa68kl5k1N0iBYftcjxcEbX
7aUlZ2fpUGi67v2r087I65DvcsE8Har6s4q8jjqjQMHRbtdfdMh0l/E5ruaM
4nOfhC0tkc8HBjZ/3eKM0nw4rpav0MHI3lCnapMzeqFvreX1hw7fLNpyYqnO
qHDzWwmNf3TY4mi+6MTjjKrntyyvrdGh8YavnO4/J/Q/my1aQg==
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{0, 10}, {-0.014683814289837694`, 0.20597849059310072`}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{
  3.604828191728423*^9, {3.60482826886683*^9, 3.60482828398805*^9}}]
}, Open  ]]
},
WindowSize->{794, 842},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"9.0 for Linux x86 (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 575, 18, 32, "Input"],
Cell[1157, 42, 400, 13, 85, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1594, 60, 890, 28, 55, "Input"],
Cell[2487, 90, 610, 20, 85, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3134, 115, 547, 13, 32, "Input"],
Cell[3684, 130, 189, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3910, 138, 421, 11, 32, "Input"],
Cell[4334, 151, 189, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4560, 159, 592, 13, 32, "Input"],
Cell[5155, 174, 159, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5351, 181, 517, 14, 32, "Input"],
Cell[5871, 197, 947, 34, 91, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6855, 236, 510, 12, 32, "Input"],
Cell[7368, 250, 123, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7528, 257, 335, 8, 32, "Input"],
Cell[7866, 267, 7749, 134, 259, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
