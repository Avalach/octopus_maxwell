(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      5673,        212]
NotebookOptionsPosition[      4390,        160]
NotebookOutlinePosition[      4725,        175]
CellTagsIndexPosition[      4682,        172]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"\[HBar]", "=", 
  RowBox[{"6.62606957", "*", 
   RowBox[{"10", "^", 
    RowBox[{"(", 
     RowBox[{"-", "34"}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.611579204377224*^9, 3.611579209490456*^9}, {
  3.611579398232753*^9, 3.611579411001442*^9}}],

Cell[BoxData["6.626069570000001`*^-34"], "Output",
 CellChangeTimes->{3.611579427903557*^9, 3.6115955852195663`*^9, 
  3.6116390851314507`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"m", "=", 
  RowBox[{"9.10938291", "*", 
   RowBox[{"10", "^", 
    RowBox[{"(", 
     RowBox[{"-", "31"}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.611579210936376*^9, 3.611579212065144*^9}, {
  3.6115793567932987`*^9, 3.611579372878314*^9}}],

Cell[BoxData["9.109382910000002`*^-31"], "Output",
 CellChangeTimes->{3.611579427983938*^9, 3.611595585283235*^9, 
  3.611639085285511*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"e", "=", 
  RowBox[{"1.602176565", "*", 
   RowBox[{"10", "^", 
    RowBox[{"(", 
     RowBox[{"-", "19"}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.611579213762361*^9, 3.611579220261671*^9}, {
  3.611579305051071*^9, 3.6115793227304*^9}}],

Cell[BoxData["1.6021765649999998`*^-19"], "Output",
 CellChangeTimes->{3.611579428015565*^9, 3.611595585319724*^9, 
  3.6116390852963037`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"a", "=", 
  RowBox[{"0.52917721092", "*", 
   RowBox[{"10", "^", 
    RowBox[{"(", 
     RowBox[{"-", "10"}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.6115792217986603`*^9, 3.6115792496132193`*^9}}],

Cell[BoxData["5.2917721092`*^-11"], "Output",
 CellChangeTimes->{3.61157942805427*^9, 3.611595585351077*^9, 
  3.61163908534128*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"\[HBar]", "^", "2"}], "/", 
  RowBox[{"(", 
   RowBox[{"m", "*", "e", "*", 
    RowBox[{"a", "^", "3"}]}], ")"}]}]], "Input",
 CellChangeTimes->{{3.611579414747553*^9, 3.611579426244606*^9}}],

Cell[BoxData["2.0300617621431465`*^13"], "Output",
 CellChangeTimes->{3.611579428065248*^9, 3.611595585378208*^9, 
  3.61163908538918*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Eh", "=", 
  RowBox[{"4.35974434", "*", 
   RowBox[{"10", "^", 
    RowBox[{"(", 
     RowBox[{"-", "18"}], ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.611595537246976*^9, 3.6115955607153893`*^9}}],

Cell[BoxData["4.35974434`*^-18"], "Output",
 CellChangeTimes->{3.611595585410617*^9, 3.6116390854328413`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[HBar]", "/", "Eh"}]], "Input",
 CellChangeTimes->{{3.611595565609559*^9, 3.611595572490693*^9}}],

Cell[BoxData["1.519829846261123`*^-16"], "Output",
 CellChangeTimes->{3.611595585421377*^9, 3.611639085477892*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"6700", "*", "0.01"}]], "Input",
 CellChangeTimes->{{3.6116389780890827`*^9, 3.611639005008162*^9}}],

Cell[BoxData["67.`"], "Output",
 CellChangeTimes->{{3.611638983784781*^9, 3.611639005681253*^9}, 
   3.61163908548699*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"67", "*", 
  RowBox[{"\[HBar]", "/", "Eh"}]}]], "Input",
 CellChangeTimes->{{3.611639073061284*^9, 3.611639083681686*^9}, {
  3.6116391171544724`*^9, 3.6116391297249603`*^9}}],

Cell[BoxData["1.0182859969949525`*^-14"], "Output",
 CellChangeTimes->{
  3.611639085530897*^9, {3.611639118750617*^9, 3.6116391304156857`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"1", "/", "a"}], "*", 
  RowBox[{"10", "^", 
   RowBox[{"(", 
    RowBox[{"-", "6"}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.611639818240328*^9, 3.6116398303315277`*^9}}],

Cell[BoxData["18897.261245650618`"], "Output",
 CellChangeTimes->{{3.611639820317247*^9, 3.611639831320588*^9}}]
}, Open  ]]
},
WindowSize->{1551, 876},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Linux x86 (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 274, 7, 32, "Input"],
Cell[856, 31, 143, 2, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1036, 38, 270, 7, 32, "Input"],
Cell[1309, 47, 139, 2, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1485, 54, 267, 7, 32, "Input"],
Cell[1755, 63, 142, 2, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1934, 70, 226, 6, 32, "Input"],
Cell[2163, 78, 132, 2, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2332, 85, 226, 6, 32, "Input"],
Cell[2561, 93, 138, 2, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2736, 100, 222, 6, 32, "Input"],
Cell[2961, 108, 109, 1, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3107, 114, 122, 2, 32, "Input"],
Cell[3232, 118, 114, 1, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3383, 124, 123, 2, 32, "Input"],
Cell[3509, 128, 122, 2, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3668, 135, 199, 4, 32, "Input"],
Cell[3870, 141, 144, 2, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4051, 148, 208, 6, 32, "Input"],
Cell[4262, 156, 112, 1, 65, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
