(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     13161,        307]
NotebookOptionsPosition[     12382,        274]
NotebookOutlinePosition[     12718,        289]
CellTagsIndexPosition[     12675,        286]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"f", "[", 
   RowBox[{"x_", ",", "y_"}], "]"}], "="}]], "Input",
 CellChangeTimes->{{3.595246443764515*^9, 3.5952464699000998`*^9}, 
   3.595840940910453*^9, {3.595840982038939*^9, 3.595840983216496*^9}}],

Cell[BoxData[
 RowBox[{"Cos", "[", 
  FractionBox[
   RowBox[{"\[Pi]", " ", "x"}], "75"], "]"}]], "Output",
 CellChangeTimes->{3.595246491664878*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"g", "[", 
   RowBox[{"x_", ",", "y_"}], "]"}], "="}]], "Input",
 CellChangeTimes->{{3.595247276169512*^9, 3.5952473026749477`*^9}, {
  3.595840988425563*^9, 3.5958409950630417`*^9}}],

Cell[BoxData[
 SuperscriptBox["\[ExponentialE]", 
  RowBox[{"-", 
   FractionBox[
    SuperscriptBox["x", "2"], "900"]}]]], "Output",
 CellChangeTimes->{3.595247303690379*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"g", "[", "x_", "]"}], "=", 
  RowBox[{"Sin", "[", " ", 
   RowBox[{"2", "*", 
    RowBox[{"\[Pi]", "/", "150"}], " ", "*", " ", "x"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.5952464716651382`*^9, 3.595246489774014*^9}}],

Cell[BoxData[
 RowBox[{"Sin", "[", 
  FractionBox[
   RowBox[{"\[Pi]", " ", "x"}], "75"], "]"}]], "Output",
 CellChangeTimes->{3.595246493576806*^9, 3.5952473060954247`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"w", "[", "y_", "]"}], "=", 
  RowBox[{"Convolve", "[", 
   RowBox[{
    RowBox[{"f", "[", "x", "]"}], ",", 
    RowBox[{"g", "[", "x", "]"}], ",", "x", ",", "y"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.5952472369179163`*^9, 3.595247251864162*^9}, {
  3.595247353686542*^9, 3.5952473587878237`*^9}}],

Cell[BoxData[
 RowBox[{"30", " ", 
  SuperscriptBox["\[ExponentialE]", 
   RowBox[{"-", 
    FractionBox[
     SuperscriptBox["\[Pi]", "2"], "25"]}]], " ", 
  SqrtBox["\[Pi]"], " ", 
  RowBox[{"Sin", "[", 
   FractionBox[
    RowBox[{"\[Pi]", " ", "y"}], "75"], "]"}]}]], "Output",
 CellChangeTimes->{3.595247256124536*^9, 3.5952473073463697`*^9, 
  3.595247360261856*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"w", "[", "y", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"y", ",", 
     RowBox[{"-", "75"}], ",", "75"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5952473112400618`*^9, 3.595247313278571*^9}, {
  3.5952473620700703`*^9, 3.595247375185892*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwt2Xk8VP/3B3BbKYUsqaQUQnaprDNz7p0ZW4Um2SpJlK1IokIqFFGhosgn
JUUkpGTtUIjKEiW7qESSpdImv/f38fj95fF8mLnzvu97zuu85zErXX157gJ8
fHz3+Pn4/vd31tzVkzOPeejOd2hci9tSoSpdvU6snIcPD9vrs+7mP7aU33VY
roSHk7TtKZm7lY/PrbsybVDIwz1ON2/9zml+LOM6d45/Jg9lss2SG3LePVYt
HZJ7H8fDWJmnipAz/thy3x3TZ7t4aKHzjbXoDj/GNasnxwny8NOJb/mh1yWw
cFThmpn+ZmyVv29xUl8B44/+4kYdtsGPpu0CQYVqaKBVlSwabI0Shz1PtFvr
4okFR93tP2zCbn7n8jHrdWjcNHaw0GsjmisMcdweGKCKoQ3TuckS44vKRcu/
G6MN61irZL0lFitF9UTzm+BRbo5X7RNLXH386gcrURN8sXlOkk6RJdoXnY2u
VDJBPy/8KvCfJQ4/C3xpvsUES1J0rt/2tsRHCvcPduWa4MZ/C4TGZ1viujq5
FQIuDPR70lwXzrDAOqrqTGoWE8Wrxe7e1LdAutx0oqiAibk1G+KqdS1wZqvN
SH0pE0fqqu2ElS3wgLfG+Y6XTNzbVDxwRswCSztneGnjTHTuvj4d32uOJXI4
9kSfhZumDuimnTDH+bbJzdsrWDjyM1e6Mtgcx2988o6sYeGZ35+n3h0yxx1h
fHa3Glj4bNq9QtHLHIue5Ek097CQI+S04TbPHKMXBI3V/WOhiQTtnqtojvvn
7mtIXw6opi55pazaDFc4djSscQLc38i85/zYDAWEZDIWOQMWHPSu5is2QzcF
o5apXYCGZU/HOTlmmDbWsuaOJ6D5piDLlwlmGHq7su/NYUB3364/3TvNkME9
KWydCJh2/5bzzC9TlHSrMr3/EvC9fUvA9UlTfBD3bwF/M6Dq35kz7C+mONF5
QNKyFfAex7HodJ8pUovpopcdgBWv5y1YUGuK+yLv708ZBOyc8qtccdEUGROZ
9dZ8FMqYGCvRWqY4kLhbU0aLwqbOwNM2qqaYE3xmT4oOhdHBBcPOCqZ4kH1+
u5wehX9LVucHy5hi7euiEAkDCvsNFzMf/ONiZbF7yDOKwlz97/aqjVyMO1A4
6WFLoUebTun6Oi6Kno0pyLKjcGWQz3LuEy6Wf21S+uhA4cWHA+93FXGxusN3
JW8HhUfXvTqQco2LEwal3j/cKeTq3YsR8+Oisf34S99ACv+9Gh6V8+Ji2tGj
4d6HKXzkr8xTd+PiK2Wp965HKVQrSF1s7sDFiJHjNznHKFygG5txArjopfRZ
ujWSwi4tz8ffFnDxGMNro8JFChMbbioIzuOi8p+nR99eotB6f1+kxCwuDh4c
2HgmicKqu/YbtX5x8PXeio6+ZAozNUzb977jIDvTJ97xOoUBaoqTHfkcvPLB
SPLIXQrv83ZzhXM4OPpXaUl/LoUTR9OT9G5xUK7dfsg0j0K/54omsckclK3l
vBe8T6G3j1IkI5yDQ2JaxhseUXjnolubZygHvTstwhOKKfxUdnN1YhAHr21X
SHhdQuEe0VUNo97k//FvBKzLKXTNXbUozZaDV4vkVcWrKLz+xt3zhRUHQyVG
U9c+obDvX0bpT3MORh7RfLv1KYXO1soumxkc/Fg7cjmmhkKnMeUsAWUOMtQV
OU/rKdy8RtXYfYqNHRX7RLxeURjn5HE2fpyN5aN6WRtaKGw4mdlb/pmNVTcv
zVndSuHGFtUImT42MjIc57S9ptA8YPXL2mdsXFjiPJuvncLTVz3lv1Wx8W5t
wM3HxDVPsw6sKGeje+FV/uAOCjkL1WSO5LNx/mW/n+87KYSHajvVktn43OJU
S2QPhWE9Xvl2F9n44tcJd8VeCitmZwuGn2NjEJo+KCM2sVfP7DzJxrkByWf7
+ygMDvP+LRzKxtvbBhb6vqOw5Hb2xrVBbPSx9tw2Raz/U3081pusPzh72Z9+
CgNX+LCL3dm4q2F1cuAAhYXmOZc+7GSj3S7Pli/Ea65oGDFt2XguszS48T2F
Byp9Yr2s2KiUdHnY8AOFeUM5PYnmbKw2er4wjVjLSDP8qwkbZdjNaTs/Uig5
a8ec1PVszFDn34vEPxpjz1rqsJF5KFVFbpDCjuQyyZ+r2VjhGtvvT1zuPpKU
ocjGrGsPkp4Sp+nILduyjI3rQcBM4hOFEX823OBbxMbNldvHHIg3xmfnbBMh
13cb0Wkn1tneqTtXiI3hsdO1EkMUSqnMK3o4TSMnrdWBS/xj3MjEbYrGCtXd
AweJO8q8KiXGaUxZdHVvKnHF6WTTx8M07qw9+KGS+Aav/rnPexr3/end/o74
1LLfNrI9NAr1tb38Q+z5afWb2jYazUVsDSSGyXruO2471Ewj47DV1ZXEOsei
+xSe0/gqruqXBrG0RbF701MaOxTv2ugR/5QaGg6toFHMa/71tcRdPYv91B/R
KD2rb1iH+HGW+fe3+TQGzqhqqRLfCDh89FQ2jdkvhrxkiU+xMmf0Mmicb6F8
XZjYS+RtxLv/aOwpHmj6StZn9VpY5PxlGk1fqv5+RbwmTf+8SQKNNsnTcvnE
C733Sg/H0DiY5m14hvjnuqQrSZE0ttSFWjsTd/HVLueG0Vi+B3ZqEuPzH+kT
h2mUW1PpMUX2+2ai8uo0fxppa3GfMuKoXXa5m3zI9S2NPUOIfTRO6f1xp1Fr
sb2LPrHN1INHmTtpPCcTuPkLeb56VR8Ydo409s67a/IfsczZhU8Et9BopSKp
YEn8255rnr+RxjK+cv5xUj9VX27y5gONPg4WedrEtx61thUb0vhQYldYLam/
6HChHXv1aJz5p2bhRGyzxG1vlTKNfz03v/Qj9dvLUQw5LErj2vkpKbtI/VeJ
b+FXFqaxVuoLu5n0x62Ok6da+GhsaxH9ZEy8368/TusbhXLH3JQESH/9Tbme
8Z70bYa5rT+vm+TLnma1BJIDe4X5/l3povCpLn8eq4HCk5Ljp7pIP8fUupQk
V1L4WuxIPI+8T3ZSvtEmk+SkvcV5gTaSn8XO171uUDjvDG2o+4bCl2GpByOu
kj469u/0NpInx+cvXfwojvTZ8TjpdJI/H1fJ7JI/QqGRzcKx6UaSFyO2eoYH
SR2ELayZS3yl4MKsLfsoFMUkWoKsYyNL4s6pXRT+2iSjI/WCwnyH+ZNfLCiM
F+of6K+lUER+Q7Uwh8K2wuZvz0keun2ITlrJpPBBlWluXjV5Hv7CJlvXkDlX
/WbpPpKnITECkWWyFAp5uw7lV5C8q/gpE/MZMPQFFeddSOozQn/o5gfA/jHT
jeIk3/9YBpZW9AJ6H09/kJtP5mPb5M6JFsBw97L4PjIfpMdHMx3LAF/9kz2y
IIvsp+IHI9WzgLsS92+9TvYhKLrZuVoL8PJYpcCZ4ySvJmZ2u6gChqXHqAST
+RawTcvzz0rAu+byZntDyHzRjDmouxBwxG3OER0yH1/diF5gPMVCzx2jj075
Uvhwjd/uz3EsvG+mtJ5N5u0xHmPuxqdMzKg27eBbR+avUrLUySImag6/SeGR
+w768WPZoztMtJ81f/81bXL95Lw1SvFMNBkPM1NTI/OnX3H73x1MzI+78l5I
nkJTf5F7OVMMXG9yNqRBmEKxhDZbUXUGPmsVX1j8GlDmEIMRtpyBgUOyv0Nf
AS5zSF81LsFA2e9SBsxGQI3lvj9af5qg017PqrxngBZ3Zl++WmOCez4uVXMu
BYyoWtep7krOnTszFnZeA/w9cWGX5RVj3HnNd4WbO+DHLTb7TwsbYXuYZURH
NQvbNVvHmv8YYvbZhB1F5Sx8IexwUG7MEP1EtS7GPmBhQenOw/lvDfGUAGPl
igwWhir6nuzMMsSVtToTn8NZKD15NlF7oyEmXVyuuxhYyE54XtEWZ4B/Kt+c
bybn07QmU3FVWX3sPjhvEXWSgV1fs6YCRPXRZHvtrn1BDFwiLtpXyaeP44W9
W+N9GJiwqSVv2+B6bOzyN0U7BkbU7+SdL1yP8g1V10rUGOjx9PClH1brsePi
w6+KzSao8+jO0urwdVgwpuLWu8gEH18TU3X9ooc6CecWFJ41wsrVts+qfujg
uHlr5JizPlrPf4NHTqgjZ1PJ4ksRenjWMvb7dr1VuKz3rubpZ1q4YjrYXzZ9
OXq9mZpQvKKMJblK7X3vJLAvwG3oTK4sBmpLBM36MPVYNWwsdfNnfgzQ0eDx
Nfxg7Wz9t9pWlh/CZS/d7twhCe17Y6RM3ZeCCyNTLlV7OfQNotlrU2XYOs/Y
MSRsFXy7cFsqRlsbov2vyBvXqUPSbf7+W/l68Ff3mLddiw5ED1dnnFmvD9YO
zo2H+NfCDqvzqR+uG8Hfd40FW0XXwqJ7u7c03DWCO95U4rola+FrwRHfvGIj
mB2mtOO7zlrgFWU1uDQbQUXG8HCAy1r4+U+r7QCfMWhMBs4OeLwWZvO9O8Xb
YQxzzp1n+IeuA+ORXo/fEiaATzB7/6/1INYtK2K5hwG7TVbnS87ShybNaQG2
HwNmP4h/+HCBPkybLfi79igDNt5yrfynog8Da7d+EDzHgLYoobZzdvrAN3iW
t+kBA0Y3mQvcu68Pg1Xt52IFmCD3ttHh6z4DCGwMSctJYsJjGwPnC0cMwH/I
RL0yjQmudWm79SMNQDHs2b6GLCZklhzYH3bVAO6IZUq0lDJBL1UqQvy5AXiG
378c0csES1f7e9qqhnD065znp1ax4PBIzyy/fkOoUVu99Vc2C5rlPTIkvhpC
vou43KtCFqhtGefc/2MIP17dFbhRzoLuYsGIH9JGUFqrv0qlkQX0aVWBUDMj
GBiYG1wzzgJRBf9/0TlGoJ0vfTtbFuCG/ayp9EBj6A42SH3rBPDnzLlEbrgx
yEVl5xU6A9hWLFo/eN4YtM+Oa8W6AgivUjuklmUM2SF39mp6AfhMWE3mdRqD
eY1gjsERgPVnL38tBxOQ89RMPJAI8LxK/VObCANuXdYdcH8JYDCT+CVNhuyj
13HWoSaAW8YCk54KDFDXbj55vAXgZOHb6T+GDPj65UTpmXYAw1uRUis8GRA/
eKVv3weA29G9TI9aBvwqL3HY/xcg3ObCpZ/hTGi23qS1RYWCsdh/yZXnmbBI
y1msZzUFznWeaWdSmKAkePuxmwYFRjRky91nwmdd7wvuuhSM631B6GdCjlrl
6FpjCnYuMhuJAhbgWdmxuVYUGPf8ppdMs+CV1ULtaD8K8rzY41+FAHYfq+Y8
8qeA3jOX2jIPoGuFybb+AAr27LjUt2QxwCZfwSiVIxTkbrgrf1sXwIv1q/HQ
CQpYqt2plW4AO1W1E1fGU/BKMf2LkjdAspXwAb4LFLjJezKiDgBErtjL7rhI
QZTM9y6rYwAj+9JeHLtMQZOQqFxXEkDsrFDXmGsUuLwzSf5RD+Ci9VCjIIeC
iS6BYcdmgN53l9iQS0HE22eG5W0Ae/y1LevuUZDZaNse/h6gZlej0osCcv/l
Posl/gHUsxoELz0iry9e4xEgRF4XfCz2dzEFMg9+FrWJULC9MpZ/WynZv5wI
+/8WUVCxVPfe/AoKTiZfTVQn+7jZeVhU7wkF0omuH8/pU6Aq9mrU9ykFt+JV
108wKBDmX1J8u5qC+qjC148syfV+usyZ84wCyaAX0qZuFAg1+kXFvKAgfcvf
BJckChgfZZ6ltlKAW+5fsk+l4MvAuuwjryno2uJ12SqdgrTXXNnNb8jn2b5N
NSH3JX7SNWWijYJTtoWZi2spWF0zocjXScENW+9s8ZcUXHP97vKIuMJWIXd2
CwVLSh1VfboomLKNu/+th6xT7dLWmm4KPLb6VDT9oEg9rl3F7SP7sVWxsvYv
BcHzkvPaia9v7XhSIUCDc96Sas93FHRsNa/LEaNB5XRbVEg/BT+2zjxPl6bB
Uq5Zf4ZY0u5hQ7IsDRdgm2/oAAUb7JRao5RpuEvdDPJ5Tz7PrvNNmAYNf/ap
mHcTR9gltAeuoeHQsydpFh8oKLPj63Vj0vB2pGtI+iMFb+2K3m3j0OAxqdxx
kPi73f73PEsawq8r8RqItey7hsCOhsLCd28PDVJgaX9hRH87DV6coMEq4j32
ll+1XGmYbPIJnfeJPD97/olVHjQoG+UlWxNfs3/0TW4/DQ9CdVjnicvsfaek
AmhQL+r2qid+a6/8W+QoDVPyWcv4hyj4Zt/9l/84DZ7ikfZ6xAscLs78jKSh
7Lfb4l3EGg4bBMZiaJA+xHAh522wcBCYNRhPw0ltIe17xHscioV7kmgQ+XUv
opH4pIOfyOtUGtIqDHZ9Jv7PQUX0RToN2jFJTwWGKShx6BF/kkVD0uma3IXE
bxwuSZbco+Heb5RXIp502Lgw/wENZhbhi7SIFzgKLs4spUHzh0gi+f4BGo4l
stcqaVC6YZdCvn+AueOBZYm1NIzM26OiS+zmqLri7Esa6AVGRquJjzv2KkS0
0NCZ1doiR5zqmLgquJ2GveMG3+b9bz2Om1T9e2mIW+ad8oOs942jkLrnBxp+
nfSr6yKecCzVdPlMg8FF62MVxGJO/jr24zTpT/GSFGI1p9V6VlM07JctCQkg
NnPqW8edpsFJxKHGnNjNKcnARJANgle/JS4mPu5kZaw3lw0lbSmjA+R5PHIq
g5UL2bBICPT2EVu/2m/7W5YN1OBooAbxR4uVHi0r2KAvaLt/kDx/acNT5yM1
2JC2NuKwDXF2nkG68xo2vIg8GPOP1A+t+vmhvgEbtE7v4mUS+y6y6Rlis8HF
6+zfUVKPs88LTFRZsEHm2VRvDPHV2Q9mXbVmQ9XL4iAl4vrvSzSttrHBYEXW
U3NSz8qtA8EF/mxwr9Fo20H6oXxD4vmYw2x4sIDr8pb0j+0T83S3Y2zo3Xf5
PyvikwV362XOsEHnziFjLdJv3XFBS4KvsyGj/dOvYtKfAXPUNG1vs2G3Xr6O
JLHI8S7QvMuG6Z/ty/aQfjbwpTx6H7Fhi+DkUn7S7xc3zStiN7FBgTcnR/At
6evq8nq5N2z4LJWQbEjyosrEr+d7JxskggbAm+TJmPrrWZmDbFDm3nxQSfJn
g8g12/kzbIgNt+AaN1PQf2KzxwchDiy9Xjhl1UTB4V+CIRUiHDiyPW63cyPJ
kU8e6X4yHIjOOeHhR3JHoHbNRKsmB9wUuzV31VFwhflhVq4eB+wP2BfwSP7p
PExactqQA79ma06wSG45Z/wFQy4HItsmboqSvCwJrzmfup0DF+a49BxCCg6A
k+aeGA5kjrb0HnlI6mPhMrGKeA6Eye12EnlAgd1w7+jCyxwQ929uvXSf5PVF
97zqmxyIqB+vT80j6/nkp7eqggMzBUufHbhDQcL504bvxzjQlBzlqUZyNsLN
UtZkigNG474y3ikUBBqK/rkwzYE7T/3rb12hwGkgoYwtwoVGvecW4okUKOr/
x0pX5ELO9KLCm+coKOwp5LraceHJZch9d4zsx/0g5ZLtXOhTaJrXEkLB5Sgj
YcndXCj9svzI46MUhKzBZ5W+XICVtpHRgRRwTz3fsDKaCzzhvoD+faSfNPs3
95VyyTlA2ThzG8nrEHFn55WmcH4mQiBKj9SD3ZNxCxVTqJN8YSNC5tCYdlDk
Ok1TeEcdcYvSouBTf0/OfENTuHr1WE8QOQ90mOf+KbExBYZUmZaSPMk36U1X
Fh03BeOca40Fc0n95ZxpbeoxhXLfdTU5nQD/IpkeZe9NwcGXJZz1FuDozok/
t4dNoUl6n+L11wAHJZ0Uw36YAl+N9lhEI4Db4dX+mmJmsGTSf0z2CYAZ55n4
GYYZ8MmbGZzIApjfNWsD+6oZFP0eDTE5CPBesj777g0zUGA//+rpC1BqcW7+
4iwzUL9KpSeQc4FXkUzDyAMzWC62v+n1boBnCSqbExvNQC5Uc7eiLUCEhYX9
kIA5nHKZ9uzSA5h+GLv7vKc5lE9lhF/tZcFovFRI53oLmKWdbvWVnHsAWr6o
MCzgi72ihsYdJiSMJuwMYFvAoZyuMBdyXl2/QZItamMBigofL96LZUKYkIQI
y9MC+sTNqwbdmLAgSOzyjRQL2L391pUqKSbobp9b6MlnCVJ3Huus92ZAgPLM
56k6S2i7OZ9Vxm8C0d3qi/npjVAQUCO0SdQQfD77RpW3b4L/jHfkvvdeB91L
vxfusbOGwBjX/vZ7unBkocaCencbUMH352oi1MHLfGivkc5mqKnTEnOIVoD0
+bXLVn7ZDPJGMXU3i6Tg4kW5Kz5jm+HdbelktqcUnJLzl340uRli3q463iUr
BV4ay+ZZ/94Ml5wOjfwNlQSdjQd/hgjzwDmePfCbLQFlMfItb1fyoKcbD/H9
JwqtIoej4ux4EDx4qyXviSDUJLwU7HLkQd7R7jtNqwShSFYxTGUHD2bWq+UM
nxaAZLWGQxW7eXBBLvKL6EZ+cLFU2j3ix4OqVxrOmpnTrJHoJoZ5DA8+K7je
eFgzyeqRUC6+cI4H/cIZRR5nJliNV4LX9sbzwHDMSXmh1TirIEtZPfAyD4SY
JxXL73xhHa4LWXwzgwe36vc6hNQOsLw2v7rwNZMHGz6F6qZYvWNta1cRN87h
QTMjNsozu5vFHHo161UBD7akfF/7sLaFpXNA9cSyhzzInaw+lpH9kqXwK/Sv
RzEPUgcfRb2ufcKSPtESVFjGg0UHqmRmau+z/v/3Lfj/37eY/wc+VMmN
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  Method->{},
  PlotRange->{{-75, 75}, {-35.829731112538965`, 35.82973043184418}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{3.5952473760124683`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"diskrete", " ", "Faltung"}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.5953153319555893`*^9, 3.595315338182803*^9}}]
},
WindowSize->{1551, 876},
WindowMargins->{{-7, Automatic}, {Automatic, 0}},
FrontEndVersion->"9.0 for Linux x86 (64-bit) (January 25, 2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 238, 5, 32, "Input"],
Cell[820, 29, 149, 4, 46, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1006, 38, 217, 5, 32, "Input"],
Cell[1226, 45, 175, 5, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1438, 55, 256, 6, 32, "Input"],
Cell[1697, 63, 173, 4, 46, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1907, 72, 336, 8, 32, "Input"],
Cell[2246, 82, 372, 11, 54, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2655, 98, 310, 8, 32, "Input"],
Cell[2968, 108, 9230, 158, 238, "Output"]
}, Open  ]],
Cell[12213, 269, 165, 3, 32, "Input"]
}
]
*)

(* End of internal cache information *)
