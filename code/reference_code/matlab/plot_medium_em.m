% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_medium_em(calc_space_em, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, ep_tensor, mu_tensor, h_tensor, v_tensor, contour_resolution, fontsize_em);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plotting signal in 1D

    if ( (calc_space_em == 'x') | (calc_space_em == 'y') | (calc_space_em == 'z') )

      if (calc_space_em == 'x')
        for ii=1:n_x_em;
          plot_axis = r_sp_mesh_em{1};
          ep_plot(ii) = ep_tensor(ii,1,1);
          mu_plot(ii) = mu_tensor(ii,1,1);
          h_plot(ii)  = h_tensor(ii,1,1);
          v_plot(ii)  = v_tensor(ii,1,1);
        end
      elseif (calc_space_em == 'y')
        for jj=1:n_y_em;
          plot_axis = r_sp_mesh_em{2};
          ep_plot(jj) = ep_tensor(1,jj,1);
          mu_plot(jj) = mu_tensor(1,jj,1);
          h_plot(jj)  = h_tensor(1,jj,1);
          v_plot(jj)  = v_tensor(1,jj,1);
        end
      elseif (calc_space_em == 'z')
        for kk=1:n_z_em;
          plot_axis = r_sp_mesh_em{3};
          ep_plot(kk) = ep_tensor(1,1,kk);
          mu_plot(kk) = mu_tensor(1,1,kk);
          h_plot(kk)  = h_tensor(1,1,kk);
          v_plot(kk)  = v_tensor(1,1,kk);
        end
      end

      fig1=figure(1);
      set(gcf,'Visible', 'off'); 
  
      plot(plot_axis,ep_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/ep_tensor.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/ep_tensor.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/ep_tensor.png');
      end
      print(fig1,'-dpng',filename);
   
      plot(plot_axis,mu_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/mu_tensor.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/mu_tensor.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/mu_tensor.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,h_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/h_tensor.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/h_tensor.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/h_tensor.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,v_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/v_tensor.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/v_tensor.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/v_tensor.png');
      end
      print(fig1,'-dpng',filename);

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % plotting signal in 2D

    if ( (calc_space_em == 'xy') | (calc_space_em == 'xz') | (calc_space_em == 'yz') )

      if (calc_space_em == 'xy')
        for ii=1:n_x_em
          for jj=1:n_y_em
            ep_plot(jj,ii) = ep_tensor(ii,jj,1);
            mu_plot(jj,ii) = mu_tensor(ii,jj,1);
            h_plot(jj,ii)  = h_tensor(ii,jj,1);
            v_plot(jj,ii)  = v_tensor(ii,jj,1);
          end
        end
      elseif (calc_space_em == 'xz')
        for ii=1:n_x_em 
          for kk=1:n_z_em
            ep_plot(kk,ii) = ep_tensor(ii,1,kk);
            mu_plot(kk,ii) = mu_tensor(ii,1,kk);
            h_plot(kk,ii)  = h_tensor(ii,1,kk);
            v_plot(kk,ii)  = v_tensor(ii,1,kk);
          end
        end
      elseif (calc_space_em == 'yz')
        for jj=1:n_y_em
          for kk=1:n_z_em
            ep_plot(kk,jj) = ep_tensor(1,jj,kk);
            mu_plot(kk,jj) = mu_tensor(1,jj,kk);
            h_plot(kk,jj)  = h_tensor(1,jj,kk);
            v_plot(kk,jj)  = v_tensor(1,jj,kk);
          end
        end
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % contour plots    

      if (calc_space_em == 'xy')
        vec_1              = r_sp_mesh_em{1};
        vec_2              = r_sp_mesh_em{2};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'y [a.u]';
        vec_1_range        = [r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))];
        vec_2_range        = [r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))];
        file_name_ep       = './figures/2D_plots/contour/xy_propagation/ep_tensor.png';
        file_name_mu       = './figures/2D_plots/contour/xy_propagation/mu_tensor.png';
        file_name_h        = './figures/2D_plots/contour/xy_propagation/h_tensor.png';
        file_name_v        = './figures/2D_plots/contour/xy_propagation/v_tensor.png';
      elseif (calc_space_em == 'xz')
        vec_1              = r_sp_mesh_em{1};
        vec_2              = r_sp_mesh_em{3};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'z [a.u]';
        vec_1_range        = [r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))];
        vec_2_range        = [r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))];
        file_name_ep       = './figures/2D_plots/contour/xz_propagation/ep_tensor.png';
        file_name_mu       = './figures/2D_plots/contour/xz_propagation/mu_tensor.png';
        file_name_h        = './figures/2D_plots/contour/xz_propagation/h_tensor.png';
        file_name_v        = './figures/2D_plots/contour/xz_propagation/v_tensor.png';
      elseif (calc_space_em == 'yz')
        vec_1           = r_sp_mesh_em{2};
        vec_2           = r_sp_mesh_em{3};
        vec_1_label     = 'y [a.u]';
        vec_2_label     = 'z [a.u]';
        vec_1_range     = [r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))];
        vec_2_range     = [r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))]; 
        file_name_ep    = './figures/2D_plots/contour/yz_propagation/ep_tensor.png';
        file_name_mu    = './figures/2D_plots/contour/yz_propagation/mu_tensor.png';
        file_name_h     = './figures/2D_plots/contour/yz_propagation/h_tensor.png';
        file_name_v     = './figures/2D_plots/contour/yz_propagation/v_tensor.png';
      end
      plot_matrix_ep  = ep_plot;
      plot_matrix_mu  = mu_plot;
      plot_matrix_h   = h_plot;
      plot_matrix_v   = v_plot;

      plot_contour(vec_1, vec_2, plot_matrix_ep, contour_resolution, filename_ep, 'ep_tensor', vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_mu, contour_resolution, filename_mu, 'mu_tensor', vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_h,  contour_resolution, filename_h,  'h_tensor',  vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_v,  contour_resolution, filename_v,  'v_tensor',  vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
 
    end

