% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = plot_contour(vector_1, vector_2, matrix, plot_resolution, filename, titlename, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize) 

     fig1=figure(1);
     set(gcf,'Visible', 'off'); 
     contour(vector_1, vector_2, matrix, plot_resolution);
     xlim(vec_1_range);
     ylim(vec_2_range);
     xlabel(vec_1_label,'Fontsize',fontsize); ylabel(vec_2_label,'Fontsize',fontsize);
     set(gca,'FontSize',fontsize);
     title(titlename,'Fontsize',fontsize); 
     axis('equal');
     pbaspect([2 1 1]);  
     c = colorbar;
     set(c,'Fontsize',fontsize);

     combinedStr = strcat(filename,'.png');
     print(fig1,'-dpng','-r250',combinedStr);
     combinedStr = strcat(filename,'.eps');
     print(fig1,'-depsc',combinedStr);



