% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at] ...
            = split_operator_propagator_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, m_sp_mesh_at, ...
              exp_approx_switch_at, exp_approx_order_at, dt, m_e, q_e, Z_at, h, pot_const_at);

    for pp=1:n_at
      r_sp_atom_mesh_at{1}(pp) = atom_pos{pp}(1);
      r_sp_atom_mesh_at{2}(pp) = atom_pos{pp}(2);
      r_sp_atom_mesh_at{3}(pp) = atom_pos{pp}(3);
    end

    % time evolution operator for kinetic energy in Fourier space
    for ii=1:n_x_at
      for jj=1:n_y_at
        for kk=1:n_z_at
          H_kin_at = (m_sp_mesh_at{1}(ii)^2) + (m_sp_mesh_at{2}(jj)^2) + (m_sp_mesh_at{3}(kk)^2);
          exp_time_ev_fft_A_at(ii,jj,kk) = exp(-i*dt/(2*m_e*h) * H_kin_at);
        end
      end
    end

    % time evolution operator for potential energy in real space
    for ii=1:n_x_at
      for jj=1:n_y_at
        for kk=1:n_z_at
          rr_square = (r_sp_mesh_at{1}(ii)-r_sp_atom_mesh_at{1}(1))^2 + (r_sp_mesh_at{2}(jj)-r_sp_atom_mesh_at{2}(1))^2 + (r_sp_mesh_at{3}(kk)-r_sp_atom_mesh_at{3}(1))^2;
          H_pot_at  = Z_at{1}*(q_e^2) / sqrt( rr_square + pot_const_at^2 );
          potential_at(ii,jj,kk) = H_pot_at;
          exp_time_ev_B_at(ii,jj,kk) = exp(i/h*dt*H_pot_at);
        end
      end
    end

    % time evolution operator of the commutator of the kinetic and potential energy operators
    for ii=1:n_x_at
      for jj=1:n_y_at
        for kk=1:n_z_at
          rr_square = (r_sp_mesh_at{1}(ii)-r_sp_atom_mesh_at{1}(1))^2 + (r_sp_mesh_at{2}(jj)-r_sp_atom_mesh_at{2}(1))^2 + (r_sp_mesh_at{3}(kk)-r_sp_atom_mesh_at{3}(1))^2;
          H_com_at  = - 3/(4*m_e) * Z_at{1}*(q_e^2) * pot_const_at^2 / ( rr_square + pot_const_at^2 )^(5/2);
          exp_time_ev_com_AB_at(ii,jj,kk) = exp(dt^2*H_com_at);
        end
      end
    end  

%    fig2 = figure(2);
%    plot(r_sp_mesh_at{1},potential_at);
