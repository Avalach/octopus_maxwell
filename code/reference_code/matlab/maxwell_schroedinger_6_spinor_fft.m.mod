% Copyright (C) 2013 Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
more off;

warning('off');

fprintf('\n');
fprintf('Starting program ...\n\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculation options
calc_ma    = 0;
calc_em    = 1;
time_watch = 1;

x_prop   = 0;
y_prop   = 0;
z_prop   = 0;
xy_prop  = 1;
xz_prop  = 0;
yz_prop  = 0;
xyz_prop = 0;

conv_switch      = 1;
exp_approx       = 1;
exp_approx_order = 4;

medium_shape     = 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters of the real space grid mesh
% matter:
boxlength_x_ma     = 1;
boxlength_y_ma     = 1;
boxlength_z_ma     = 1;
n_x_ma             = 1;
n_y_ma             = 1;
n_z_ma             = 1;
% electromagnetic field:
boxlength_x_em     = 100;
boxlength_y_em     = 50;
boxlength_z_em     = 1;
n_x_em             = 20;
n_y_em             = 10;
n_z_em             = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameter for the time evolution calculation
timesteps          = 4000;
dt                 = 0.0005;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% physical parameters

alpha = 7.2973525698*10^(-3);
h=1;
c_0           = 1/alpha;
ep_0          = 1/(4*pi);
mu_0          = 1/(c_0^2*ep_0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% medium parameter
mu_m              = mu_0;
ep_m              = 4 * ep_0;
medium_image_load = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters for the initial EM signal

signallength_x_em  = 30;
signallength_y_em  = 30;
signallegnth_z_em  = 30;
sigma_x_em         = 4;
sigma_y_em         = 5;
sigma_z_em         = 6;
k_x_em             = 0.85;
k_y_em             = 0;
k_z_em             = 0;
signal_pos_x_em    = -35.0;
signal_pos_y_em    = 0;
signal_pos_z_em    = 0;
em_wave_shape      = 3; % 1 = sinus shape, 2 = gauss shape, 3 = sinus and gauss envelope

contour_res        = 4000;
contour_min_neg    = -1.0;
contour_max_neg    = -0.08;
contour_min_pos    =  0.08;
contour_max_pos    =  1.0;
contour_step       =  0.001;
plot_step          =  10;
plot_res           = '-r500';
fontsize           =  14.5;


%calc_ma       = 0; % 1 = calculation of matter on, 0 = calculation of matter off
%calc_em       = 1; % 1 = calculation of EM field on, 0 = calculation of EM field off
%em_wave_shape = 2; % 1 = sinus shape, 2 = gauss shape, 3 = sinus and gauss envelope
%boundary_em   = 0; % 1 = PEC, 2 = PMC, 3 = PML
%prop_mode_em  = 3; % 0 = update equation, 1 = crank nicholson, 2 = fourth order taylor of exp, 3 = fast Fourier transform


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% system calculated parameters

dx_em        = double(boxlength_x_em)/double(n_x_em);
dy_em        = double(boxlength_y_em)/double(n_y_em);
dz_em        = double(boxlength_z_em)/double(n_z_em);
dx_ma        = double(boxlength_x_ma)/double(n_x_ma);
dy_ma        = double(boxlength_y_ma)/double(n_y_ma);
dz_ma        = double(boxlength_z_ma)/double(n_z_ma);

min_x_pnt_em = -double(boxlength_x_em)/2+dx_em/2; 
min_y_pnt_em = -double(boxlength_y_em)/2+dy_em/2;
min_z_pnt_em = -double(boxlength_z_em)/2+dz_em/2;

max_x_pnt_em =  double(boxlength_x_em)/2-dx_em/2;
max_y_pnt_em =  double(boxlength_y_em)/2-dy_em/2;
max_z_pnt_em =  double(boxlength_z_em)/2-dz_em/2;

min_x_pnt_ma = -double(boxlength_x_ma)/2+dx_ma/2; 
min_y_pnt_ma = -double(boxlength_y_ma)/2+dy_ma/2;
min_z_pnt_ma = -double(boxlength_z_ma)/2+dz_ma/2;

max_x_pnt_ma =  double(boxlength_x_ma)/2-dx_ma/2;
max_y_pnt_ma =  double(boxlength_y_ma)/2-dy_ma/2;
max_z_pnt_ma =  double(boxlength_z_ma)/2-dz_ma/2;

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup of figures

set(0,'Units','pixels') 
scnsize = get(0,'ScreenSize');
scnsize(3) = scnsize(3)*0.96;
fig1=figure(1);
set(gcf,'Visible', 'off'); 
clf
fig2=figure(2);
set(gcf,'Visible', 'off'); 
clf
fig3=figure(3);
set(gcf,'Visible', 'off'); 
clf
fig4=figure(4);
set(gcf,'Visible', 'off'); 
clf
fig5=figure(5);
set(gcf,'Visible', 'off'); 
clf
fig6=figure(6);
set(gcf,'Visible', 'off');
clf
fig7=figure(7);
set(gcf,'Visible', 'off');
clf

position = get(fig1,'Position');
outerpos = get(fig1,'OuterPosition');
border = outerpos-position;
posshift = 50;

pos1 = [0,...
        scnsize(4)*(1/4),...
        scnsize(3)*(1/3)-border(4)/6,...
        scnsize(4)/2];
pos2 = [scnsize(3)*(1/3)+posshift,...
        scnsize(4)/2-5*border(3),...
        pos1(3),...
        pos1(4)-border(4)/4];
pos3 = [pos2(1),...
        5*border(3),...
        pos1(3),...
        pos2(4)];
pos4 = [scnsize(3)*(2/3)+posshift,...
        pos2(2),...
        pos1(3),...
        pos2(4)];
pos5 = [pos4(1),...
        pos3(2),...
        pos1(3),...
        pos2(4)];
set(fig1,'OuterPosition',pos1); 
set(fig2,'OuterPosition',pos2);
set(fig3,'OuterPosition',pos3);
set(fig4,'OuterPosition',pos4);
set(fig5,'OuterPosition',pos5);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup real-space mesh

fprintf('Setting up real-space mesh ...\n\n');

r_sp_mesh_em{1} = min_x_pnt_em:dx_em:max_x_pnt_em;
r_sp_mesh_em{2} = min_y_pnt_em:dy_em:max_y_pnt_em;
r_sp_mesh_em{3} = min_z_pnt_em:dz_em:max_z_pnt_em;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup momentum-space mesh

fprintf('Setting up momentum-space mesh ...\n\n');

if (n_x_em > 1)
  for jj=0:(n_x_em/2)
    m_sp_mesh_em{1}(jj+1) = 2*pi/(double(boxlength_x_em)) * double(jj);
  end
  for jj=(n_x_em/2+1):(n_x_em-1)
    m_sp_mesh_em{1}(jj+1) = 2*pi/(double(boxlength_x_em)) * double(jj-n_x_em);
  end
else
  m_sp_mesh_em{1}(1) = r_sp_mesh_em{1}(1);
end

if (n_y_em > 1)
  for jj=0:(n_y_em/2)
    m_sp_mesh_em{2}(jj+1) = 2*pi/(double(boxlength_y_em)) * double(jj);
  end
  for jj=(n_y_em/2+1):(n_y_em-1)
    m_sp_mesh_em{2}(jj+1) = 2*pi/(double(boxlength_y_em)) * double(jj-n_y_em);
  end
else
  m_sp_mesh_em{2}(1) = r_sp_mesh_em{2}(1);
end

if (n_z_em > 1)
  for jj=0:(n_z_em/2)
    m_sp_mesh_em{3}(jj+1) = 2*pi/(double(boxlength_z_em)) * double(jj);
  end
  for jj=(n_z_em/2+1):(n_z_em-1)
    m_sp_mesh_em{3}(jj+1) = 2*pi/(double(boxlength_z_em)) * double(jj-n_z_em);
  end
else
  m_sp_mesh_em{3}(1) = r_sp_mesh_em{3}(1);
end

% setup fft of divergence, first and second derivative operators
for ii=1:n_x_em
  for jj=1:n_y_em
    for kk=1:n_z_em
      fft_div_op(ii,jj,kk) = i/h * ( m_sp_mesh_em{1}(ii) + m_sp_mesh_em{2}(jj) + m_sp_mesh_em{3}(kk) );
      fft_del_z_op(kk)   =  i/h * m_sp_mesh_em{3}(kk);
      fft_del_2_z_op(kk) = -1/(h^2) * (m_sp_mesh_em{3}(kk))^2;
    end
    fft_del_y_op(jj)   =  i/h * m_sp_mesh_em{2}(jj);
    fft_del_2_y_op(jj) = -1/(h^2) * (m_sp_mesh_em{2}(jj))^2;
  end
  fft_del_x_op(ii)   =  i/h * m_sp_mesh_em{1}(ii);
  fft_del_2_x_op(ii) = -1/(h^2) * (m_sp_mesh_em{1}(ii))^2;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup mu and epsilon arrays on the mesh

fprintf('Setting up mu and epsilon values of the medium ...\n\n');

if (time_watch ==1) tic; end

%if ((n_y_em <= 1) & (n_z_em <= 1))
%  if ( (ii<n_x_em/4) | (ii>n_x_em-n_x_em/4) )
%    mu_tensor(ii,jj,kk) = mu_m;
%    ep_tensor(ii,jj,kk) = ep_m;
%    v_tensor(ii,jj,kk)  = 1/sqrt(mu_m*ep_0);
%    h_tensor(ii,jj,kk)  = sqrt(mu_m/ep_m);
%  else
%    mu_tensor(ii,jj,kk) = mu_0;
%    ep_tensor(ii,jj,kk) = ep_0;
%    v_tensor(ii,jj,kk)  = 1/sqrt(mu_0*ep_0);
%    h_tensor(ii,jj,kk)  = sqrt(mu_0/ep_0);
%  end
%elseif (n_z_em <= 1)
%  if (medium_image_load == 0)
%    if ( (ii<n_x_em/4) | (ii>n_x_em-n_x_em/4) )
%      mu_tensor(ii,jj,kk) = mu_m;
%      ep_tensor(ii,jj,kk) = ep_m;
%      v_tensor(ii,jj,kk)  = 1/sqrt(mu_m*ep_0);
%      h_tensor(ii,jj,kk)  = sqrt(mu_m/ep_m);
%    else
%      mu_tensor(ii,jj,kk) = mu_0;
%      ep_tensor(ii,jj,kk) = ep_0;
%      v_tensor(ii,jj,kk)  = 1/sqrt(mu_0*ep_0);
%      h_tensor(ii,jj,kk)  = sqrt(mu_0/ep_0);
%    end
%  elseif (medium_image_load == 1)
%    mu_image = imread('mu_tensor_input_2D.jpg'); 
%    ep_image = imread('em_tensor_input_2D.jpg');
%  end
%else


% 1D medium initialization
idx_vac = 0;
idx_med = 0;

if ( (x_prop == 1) | (y_prop == 1) | (z_prop == 1) )
  for ii=1:n_x_em
    for jj=1:n_y_em
      for kk=1:n_z_em
        if ( (ii<n_x_em*0.25) | (ii>n_x_em-n_x_em*0.25) )
          idx_med = idx_med+1;
          mu_tensor(ii,jj,kk)    = mu_m;
          ep_tensor(ii,jj,kk)    = ep_m;
          v_tensor(ii,jj,kk)     = 1/sqrt(mu_m*ep_m);
          h_tensor(ii,jj,kk)     = sqrt(mu_m/ep_m);
          medium_point(ii,jj,kk) = 1;          
        else
          idx_vac = idx_vac+1;
          mu_tensor(ii,jj,kk)    = mu_0;
          ep_tensor(ii,jj,kk)    = ep_0;
          v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
          h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
          medium_point(ii,jj,kk) = 0;
        end
      end
    end
  end
end

% 2D medium initialization
medium_plot = zeros(n_y_em,n_x_em);
if (xy_prop == 1) 
  if (medium_shape == 1)
    x_1 = -15; x_2 = -7; x_3 = 7; x_4 = 15;
    y_1 = -15; y_2 = -7; y_3 = 7; y_4 = 15;
    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em
          if ( (r_sp_mesh_em{1}(ii)>x_1) & (r_sp_mesh_em{1}(ii)<x_4) & (r_sp_mesh_em{2}(jj)>y_1) & (r_sp_mesh_em{2}(jj)<y_4) )
            if ( (r_sp_mesh_em{1}(ii)>x_2) & (r_sp_mesh_em{1}(ii)<x_3) & (r_sp_mesh_em{2}(jj)>y_2) & (r_sp_mesh_em{2}(jj)<y_3) )
              idx_vac = idx_vac+1;
              mu_tensor(ii,jj,kk)    = mu_0;
              ep_tensor(ii,jj,kk)    = ep_0;
              v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
              h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
              medium_point(ii,jj,kk) = 0;
            else
              idx_med = idx_med+1;
              mu_tensor(ii,jj,kk)    = mu_m;
              ep_tensor(ii,jj,kk)    = ep_m;
              v_tensor(ii,jj,kk)     = 1/sqrt(mu_m*ep_m);
              h_tensor(ii,jj,kk)     = sqrt(mu_m/ep_m);
              medium_point(ii,jj,kk) = 1;
            end
          else
            idx_vac = idx_vac+1;
            mu_tensor(ii,jj,kk)    = mu_0;
            ep_tensor(ii,jj,kk)    = ep_0;
            v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
            h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0); 
            medium_point(ii,jj,kk) = 0;
          end
        end
        v_mesh_plot(jj,ii) = v_tensor(ii,jj,kk);
        h_mesh_plot(jj,ii) = h_tensor(ii,jj,kk);
      end
    end
  elseif (medium_shape == 2)
    r_1 = 15;
    r_2 =  7;
%    x_1 = -15; x_2 = -7; x_3 = 7; x_4 = 15;
%    y_1 = -15; y_2 = -7; y_3 = 7; y_4 = 15;
    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em
          if ( r_sp_mesh_em{1}(ii)^2 + r_sp_mesh_em{2}(jj)^2 < r_1^2 )
            if ( r_sp_mesh_em{1}(ii)^2 + r_sp_mesh_em{2}(jj)^2 < r_2^2 )
              idx_vac = idx_vac+1;
              mu_tensor(ii,jj,kk)    = mu_0;
              ep_tensor(ii,jj,kk)    = ep_0;
              v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
              h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
              medium_point(ii,jj,kk) = 0;
            else
              idx_med = idx_med+1;
              mu_tensor(ii,jj,kk)    = mu_m;
              ep_tensor(ii,jj,kk)    = ep_m;
              v_tensor(ii,jj,kk)     = 1/sqrt(mu_m*ep_m);
              h_tensor(ii,jj,kk)     = sqrt(mu_m/ep_m);
              medium_point(ii,jj,kk) = 1;
            end
          else
            idx_vac = idx_vac+1;
            mu_tensor(ii,jj,kk)    = mu_0;
            ep_tensor(ii,jj,kk)    = ep_0;
            v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
            h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0); 
            medium_point(ii,jj,kk) = 0;
          end
        end
        v_mesh_plot(jj,ii) = v_tensor(ii,jj,kk);
        h_mesh_plot(jj,ii) = h_tensor(ii,jj,kk);
      end
    end
  end
end

vac_grid_n_em = idx_vac;
med_grid_n_em = idx_med;

% ploting medium by v_tensor
if (xy_prop == 1)
  fig10 = figure(10);
  set(gcf,'Visible', 'off'); 
  contour(r_sp_mesh_em{1},r_sp_mesh_em{2},v_mesh_plot,contour_res); 
  axis('equal');
  xlim([min_x_pnt_em max_x_pnt_em]);
  ylim([min_y_pnt_em max_y_pnt_em]);
  xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  daspect([1 1 1]);
  filename = './figures/v_contour.png';
  print(fig10,'-dpng',filename);
 % fig10 = figure(10);
 % set(gcf,'Visible','on');
 % mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},v_mesh_plot);
 % filename = './figures/v_mesh.png';
 % print(fig10,'-dpng',filename);
elseif ( (x_prop == 1) | (y_prop == 1) )
  fig10 = figure(10);
  if (x_prop ==1)
    plot(r_sp_mesh_em{1},v_tensor(:,1,1));
  end
  if (y_prop ==1)
    plot(r_sp_mesh_em{2},v_tensor(1,:,1));
  end
end

if (time_watch ==1) toc; fprintf(' \n'); end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fourier transform tensor of v_tensor and h_tensor and their derivative tensors

fprintf('Setting up v_tensor, h_tensor and their derivative tensors ...\n\n');

if (time_watch ==1) tic; end

fft_v_tensor = fftn(v_tensor);
fft_h_tensor = fftn(h_tensor);

for ii=1:n_x_em
  for jj=1:n_y_em
    for kk=1:n_z_em
      fft_div_v_tensor(ii,jj,kk)     = fft_div_op(ii,jj,kk) * fft_v_tensor(ii,jj,kk);
      fft_div_h_tensor(ii,jj,kk)     = fft_div_op(ii,jj,kk) * fft_h_tensor(ii,jj,kk);
      fft_del_x_v_tensor(ii,jj,kk)   = fft_del_x_op(ii)     * fft_v_tensor(ii,jj,kk);
      fft_del_y_v_tensor(ii,jj,kk)   = fft_del_y_op(jj)     * fft_v_tensor(ii,jj,kk);
      fft_del_z_v_tensor(ii,jj,kk)   = fft_del_z_op(kk)     * fft_v_tensor(ii,jj,kk);
      fft_del_2_x_v_tensor(ii,jj,kk) = fft_del_2_x_op(ii)   * fft_v_tensor(ii,jj,kk);
      fft_del_2_y_v_tensor(ii,jj,kk) = fft_del_2_y_op(jj)   * fft_v_tensor(ii,jj,kk);
      fft_del_2_z_v_tensor(ii,jj,kk) = fft_del_2_z_op(kk)   * fft_v_tensor(ii,jj,kk);
      fft_del_x_h_tensor(ii,jj,kk)   = fft_del_x_op(ii)     * fft_h_tensor(ii,jj,kk);
      fft_del_y_h_tensor(ii,jj,kk)   = fft_del_y_op(jj)     * fft_h_tensor(ii,jj,kk);
      fft_del_z_h_tensor(ii,jj,kk)   = fft_del_z_op(kk)     * fft_h_tensor(ii,jj,kk);
      fft_del_2_x_h_tensor(ii,jj,kk) = fft_del_2_x_op(ii)   * fft_h_tensor(ii,jj,kk);
      fft_del_2_y_h_tensor(ii,jj,kk) = fft_del_2_y_op(jj)   * fft_h_tensor(ii,jj,kk);
      fft_del_2_z_h_tensor(ii,jj,kk) = fft_del_2_z_op(kk)   * fft_h_tensor(ii,jj,kk);
    end
  end
end

div_v_tensor     = ifftn(fft_div_v_tensor);
div_h_tensor     = ifftn(fft_div_h_tensor);

del_x_v_tensor   = ifftn(fft_del_x_v_tensor);
del_y_v_tensor   = ifftn(fft_del_y_v_tensor);
del_z_v_tensor   = ifftn(fft_del_z_v_tensor);

del_2_x_v_tensor = ifftn(fft_del_2_x_v_tensor);
del_2_y_v_tensor = ifftn(fft_del_2_y_v_tensor);
del_2_z_v_tensor = ifftn(fft_del_2_z_v_tensor);

del_x_h_tensor   = ifftn(fft_del_x_h_tensor);
del_y_h_tensor   = ifftn(fft_del_y_h_tensor);
del_z_h_tensor   = ifftn(fft_del_z_h_tensor);

del_2_x_h_tensor = ifftn(fft_del_2_x_h_tensor);
del_2_y_h_tensor = ifftn(fft_del_2_y_h_tensor);
del_2_z_h_tensor = ifftn(fft_del_2_z_h_tensor);

if (time_watch ==1) toc; fprintf(' \n'); end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial electromagnetic field in 1D with propagation into x direction

if (time_watch ==1) tic; end

if ( (x_prop == 1) | (y_prop == 1) | (z_prop == 1) )

fprintf('Setting up electromagnetic field ...\n\n');

  % initialization of the electromagnetic field
  Ex = zeros(n_x_em,n_y_em,n_z_em);
  Ey = zeros(n_x_em,n_y_em,n_z_em);
  Ez = zeros(n_x_em,n_y_em,n_z_em);
  Bx = zeros(n_x_em,n_y_em,n_z_em);
  By = zeros(n_x_em,n_y_em,n_z_em);
  Bz = zeros(n_x_em,n_y_em,n_z_em);
  for ii=1:n_x_em
    for jj=1:n_y_em
      for kk=1:n_z_em
        if (em_wave_shape == 1)
          if (x_prop ==1)
            sinus_wave   = sin(k_x_em*(r_sp_mesh_em{1}(ii)-signal_pos_x_em));
            Ez(ii,jj,kk) =                         sinus_wave;
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * sinus_wave;       
          elseif (y_prop ==1)
            sinus_wave   = sin(k_y_em*(r_sp_mesh_em{2}(jj)-signal_pos_y_em));
            Ez(ii,jj,kk) =                         sinus_wave;
            Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave;
          elseif (z_prop ==1)
            sinus_wave   = sin(k_z_em*(r_sp_mesh_em{3}(kk)-signal_pos_z_em));
            Ex(ii,jj,kk) =                         sinus_wave;
            By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave;
          end
        elseif (em_wave_shape == 2)
          if (x_prop ==1) 
            gauss_dist   = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
            Ez(ii,jj,kk) =                         gauss_dist;
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * gauss_dist;
          elseif (y_prop ==1)
            gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
            Ez(ii,jj,kk) =                         gauss_dist;
            Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * gauss_dist;
          elseif (z_prop ==1)
            gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
            Ex(ii,jj,kk) =                         gauss_dist;
            By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * gauss_dist;
          end
        elseif (em_wave_shape == 3)
          if (x_prop ==1)
            sinus_wave   = sin(k_x_em*r_sp_mesh_em{1}(ii));
            gauss_dist   = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
            Ez(ii,jj,kk) =                         sinus_wave * gauss_dist;  
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * sinus_wave * gauss_dist;
          elseif (y_prop ==1)
            sinus_wave = sin(k_y_em*r_sp_mesh_em{2}(jj));
            gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
            Ez(ii,jj,kk) =                         sinus_wave * gauss_dist;  
            Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave * gauss_dist;
          elseif (z_prop ==1)
            sinus_wave = sin(k_z_em*r_sp_mesh_em{3}(kk));
            gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
            Ex(ii,jj,kk) =                         sinus_wave * gauss_dist;  
            By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave * gauss_dist;
          end
        end
      end
    end
  end


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Plotting inital signal

  fprintf('Plotting initial signals  ...\n\n');

  %figure(1);
  %set(gcf,'Visible', 'off'); 
  %plot(r_sp_mesh_em{1},abs(Phi_xx_ma));
  %filename = './figures/wavefunction/wave_packet_t=0.png';
  %print(fig1,'-dpng',filename);

  if (x_prop == 1)
    for ii=1:n_x_em;
      plot_axis = r_sp_mesh_em{1};
      Ex_plot(ii) = Ex(ii,1,1);
      Ey_plot(ii) = Ey(ii,1,1);
      Ez_plot(ii) = Ez(ii,1,1);
      Bx_plot(ii) = Bx(ii,1,1);
      By_plot(ii) = By(ii,1,1);
      Bz_plot(ii) = By(ii,1,1);
    end
  elseif (y_prop == 1)
    for jj=1:n_y_em;
      plot_axis = r_sp_mesh_em{2};
      Ex_plot(jj) = Ex(1,jj,1);
      Ey_plot(jj) = Ey(1,jj,1);
      Ez_plot(jj) = Ez(1,jj,1);
      Bx_plot(jj) = Bx(1,jj,1);
      By_plot(jj) = By(1,jj,1);
      Bz_plot(jj) = By(1,jj,1);
    end
  elseif (z_prop == 1)
    for kk=1:n_z_em;
      plot_axis = r_sp_mesh_em{3};
      Ex_plot(kk) = Ex(1,1,kk);
      Ey_plot(kk) = Ey(1,1,kk);
      Ez_plot(kk) = Ez(1,1,kk);
      Bx_plot(kk) = Bx(1,1,kk);
      By_plot(kk) = By(1,1,kk);
      Bz_plot(kk) = By(1,1,kk);
    end
  end

  figure(2);
  set(gcf,'Visible', 'off'); 
  plot(plot_axis,Ex_plot);
  if (x_prop == 1)
    filename = './figures/1D_plots/x_propagation/E_field_x/E_field_x_t=0.png';
  elseif (y_prop == 1)
    filename = './figures/1D_plots/y_propagation/E_field_x/E_field_x_t=0.png';
  elseif (z_prop == 1)
    filename = './figures/1D_plots/z_propagation/E_field_x/E_field_x_t=0.png';
  end
  print(fig2,'-dpng',filename);

  figure(3);
  set(gcf,'Visible', 'off'); 
  plot(plot_axis,Ey_plot);
  if (x_prop == 1)
    filename = './figures/1D_plots/x_propagation/E_field_y/E_field_y_t=0.png';
  elseif (y_prop == 1)
    filename = './figures/1D_plots/y_propagation/E_field_y/E_field_y_t=0.png';
  elseif (z_prop == 1)
    filename = './figures/1D_plots/z_propagation/E_field_y/E_field_y_t=0.png';
  end
  print(fig3,'-dpng',filename);

  figure(4);
  set(gcf,'Visible', 'off'); 
  plot(plot_axis,Ez_plot);
  if (x_prop == 1)
    filename = './figures/1D_plots/x_propagation/E_field_z/E_field_z_t=0.png';
  elseif (y_prop == 1)
    filename = './figures/1D_plots/y_propagation/E_field_z/E_field_z_t=0.png';
  elseif (z_prop == 1)
    filename = './figures/1D_plots/z_propagation/E_field_z/E_field_z_t=0.png';
  end
  print(fig4,'-dpng',filename);

  figure(5);
  set(gcf,'Visible', 'off'); 
  plot(plot_axis,Bx_plot);
  if (x_prop == 1)
    filename = './figures/1D_plots/x_propagation/B_field_x/B_field_x_t=0.png';
  elseif (y_prop == 1)
    filename = './figures/1D_plots/y_propagation/B_field_x/B_field_x_t=0.png';
  elseif (z_prop == 1)
    filename = './figures/1D_plots/z_propagation/B_field_x/B_field_x_t=0.png';
  end
  print(fig5,'-dpng',filename);

  figure(6);
  set(gcf,'Visible', 'off'); 
  plot(plot_axis,By_plot);
  if (x_prop == 1)
    filename = './figures/1D_plots/x_propagation/B_field_y/B_field_y_t=0.png';
  elseif (y_prop == 1)
    filename = './figures/1D_plots/y_propagation/B_field_y/B_field_y_t=0.png';
  elseif (z_prop == 1)
    filename = './figures/1D_plots/z_propagation/B_field_y/B_field_y_t=0.png';
  end
  print(fig6,'-dpng',filename);

  figure(7);
  set(gcf,'Visible', 'off'); 
  plot(plot_axis,Bz_plot);
  if (x_prop == 1)
    filename = './figures/1D_plots/x_propagation/B_field_z/B_field_z_t=0.png';
  elseif (y_prop == 1)
    filename = './figures/1D_plots/y_propagation/B_field_z/B_field_z_t=0.png';
  elseif (z_prop == 1)
    filename = './figures/1D_plots/z_propagation/B_field_z/B_field_z_t=0.png';
  end
  print(fig7,'-dpng',filename);

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial electromagnetic field in 2D 

if ( (xy_prop == 1) | (xz_prop == 1) | (yz_prop == 1) )

  fprintf('Setting up electromagnetic field ...\n\n');

  % initialization of electromagnetic field
  Ex = zeros(n_x_em,n_y_em,n_z_em);
  Ey = zeros(n_x_em,n_y_em,n_z_em);
  Ez = zeros(n_x_em,n_y_em,n_z_em);
  Bx = zeros(n_x_em,n_y_em,n_z_em);
  By = zeros(n_x_em,n_y_em,n_z_em);
  Bz = zeros(n_x_em,n_y_em,n_z_em);

  for ii=1:n_x_em
    for jj=1:n_y_em
      for kk=1:n_z_em
        if (em_wave_shape == 1)
          if (xy_prop == 1)
            sinus_wave_x = sin(k_x_em*(r_sp_mesh_em{1}(ii)-signal_pos_x_em));
            sinus_wave_y = sin(k_y_em*(r_sp_mesh_em{2}(jj)-signal_pos_y_em));
            Ez(ii,jj,kk) = sinus_wave_x * sinus_wave_y ;
            Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
          elseif (xz_prop == 1)
            sinus_wave_x = sin(k_x_em*(r_sp_mesh_em{1}(ii)-signal_pos_x_em));
            sinus_wave_z = sin(k_z_em*(r_sp_mesh_em{3}(kk)-signal_pos_z_em));
            Ey(ii,jj,kk) = sinus_wave_x * sinus_wave_z ;
            Bx(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
            Bz(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
          elseif (yz_prop == 1)
            sinus_wave_y = sin(k_y_em*(r_sp_mesh_em{2}(jj)-signal_pos_y_em));
            sinus_wave_z = sin(k_z_em*(r_sp_mesh_em{3}(kk)-signal_pos_z_em));
            Ex(ii,jj,kk) = sinus_wave_y * sinus_wave_z ;
            By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
            Bz(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
          end
        elseif (em_wave_shape == 2) 
          if (xy_prop == 1)
            gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
            gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2)); 
            Ez(ii,jj,kk) = gauss_dist_x * gauss_dist_y ;
            Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
          elseif (xz_prop == 1)
            gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
            gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2)); 
            Ey(ii,jj,kk) = gauss_dist_x * gauss_dist_z ;
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
            Bz(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
          elseif (yz_prop == 1)
            gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
            gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2)); 
            Ex(ii,jj,kk) = gauss_dist_y * gauss_dist_z ;
            By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
            Bz(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
          end
        elseif (em_wave_shape == 3)
          if (xy_prop == 1)
            sinus_wave_x = sin(k_x_em*r_sp_mesh_em{1}(ii));
            sinus_wave_y = 1;
            gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
            gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
            Ez(ii,jj,kk) =  sinus_wave_x * sinus_wave_y * gauss_dist_x * gauss_dist_y ;  
            Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
          elseif (xz_prop == 1)
            sinus_wave_x = sin(k_x_em*r_sp_mesh_em{1}(ii));
            sinus_wave_z = 1;
            gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
            gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
            Ey(ii,jj,kk) =  sinus_wave_x * sinus_wave_z * gauss_dist_x * gauss_dist_z ;  
            Bx(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
            By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
          elseif (yz_prop == 1)
            sinus_wave_y = sin(k_y_em*r_sp_mesh_em{2}(jj));
            sinus_wave_z = 1;
            gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
            gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
            Ex(ii,jj,kk) =  sinus_wave_y * sinus_wave_z * gauss_dist_y * gauss_dist_z ;  
            By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
            Bz(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
          end
        end
      end
    end
  end


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Plotting inital signal

  fprintf('Plotting initial signals  ...\n\n');

  if (xy_prop == 1)
    for ii=1:n_x_em
      for jj=1:n_y_em
        Ex_plot(jj,ii) = Ex(ii,jj,1);
        Ey_plot(jj,ii) = Ey(ii,jj,1);
        Ez_plot(jj,ii) = Ez(ii,jj,1);
        Bx_plot(jj,ii) = Bx(ii,jj,1);
        By_plot(jj,ii) = By(ii,jj,1);
        Bz_plot(jj,ii) = Bz(ii,jj,1);
      end
    end
  elseif (xz_prop == 1)
    for ii=1:n_x_em 
      for kk=1:n_z_em
        Ex_plot(kk,ii) = Ex(ii,1,kk);
        Ey_plot(kk,ii) = Ey(ii,1,kk);
        Ez_plot(kk,ii) = Ez(ii,1,kk);
        Bx_plot(kk,ii) = Bx(ii,1,kk);
        By_plot(kk,ii) = By(ii,1,kk);
        Bz_plot(kk,ii) = Bz(ii,1,kk);
      end
    end
  elseif (yz_prop == 1)
    for jj=1:n_y_em
      for kk=1:n_z_em
        Ex_plot(kk,jj) = Ex(1,jj,kk);
        Ey_plot(kk,jj) = Ey(1,jj,kk);
        Ez_plot(kk,jj) = Ez(1,jj,kk);
        Bx_plot(kk,jj) = Bx(1,jj,kk);
        By_plot(kk,jj) = By(1,jj,kk);
        Bz_plot(kk,jj) = Bz(1,jj,kk);
      end
    end
  end

  %figure(1);
  %set(gcf,'Visible', 'off'); 
  %plot(r_sp_mesh_em{1},abs(Phi_xx_ma));
  %filename = './figures/wavefunction/wave_packet_t=0.png';
  %print(fig1,'-dpng',filename);


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % contour plots

  figure(2);
  set(gcf,'Visible', 'off'); 
  if (xy_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Ex_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Ex_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Ex_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Electric');
  axis('equal');
  pbaspect([2 1 1]);
  c = colorbar;
  set(c,'Fontsize',fontsize);
  if (xy_prop == 1)
    filename_png = './figures/2D_plots/contour/xy_propagation/E_field_x/E_field_x_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xy_propagation/E_field_x/E_field_x_t=0.eps';    
  elseif (xz_prop == 1)
    filename_png = './figures/2D_plots/contour/xz_propagation/E_field_x/E_field_x_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xz_propagation/E_field_x/E_field_x_t=0.eps';
  elseif (yz_prop == 1)
    filename_png = './figures/2D_plots/contour/yz_propagation/E_field_x/E_field_x_t=0.png';
%    filename_eps = './figures/2D_plots/contour/yz_propagation/E_field_x/E_field_x_t=0.eps';
  end
  print(fig2,'-dpng','-r300',filename_png);
%  print(fig2,'-depsc',filename_eps);

  figure(3);
  set(gcf,'Visible', 'off'); 
  if (xy_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Ey_plot,contour_res);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Ey_plot,contour_res);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Ey_plot,contour_res);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('electric field','Fontsize',fontsize);
  axis('equal'); 
  pbaspect([2 1 1]);
  c = colorbar;
  set(c,'Fontsize',fontsize);
  if (xy_prop == 1)
    filename_png = './figures/2D_plots/contour/xy_propagation/E_field_y/E_field_y_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xy_propagation/E_field_y/E_field_y_t=0.eps';
  elseif (xz_prop == 1)
    filename_png = './figures/2D_plots/contour/xz_propagation/E_field_y/E_field_y_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xz_propagation/E_field_y/E_field_y_t=0.eps';
  elseif (yz_prop == 1)
    filename_png = './figures/2D_plots/contour/yz_propagation/E_field_y/E_field_y_t=0.png';
%    filename_eps = './figures/2D_plots/contour/yz_propagation/E_field_y/E_field_y_t=0.eps';
  end
  print(fig3,'-dpng','-r300',filename_png);
%  print(fig3,'-depsc',filename_eps);

  figure(4);
  set(gcf,'Visible', 'off'); 
  if (xy_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Ez_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Ez_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Ez_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Electric field','Fontsize',fontsize); 
  axis('equal');
  pbaspect([2 1 1]);  
  c = colorbar;
  set(c,'Fontsize',fontsize);
  if (xy_prop == 1)
    filename_png = './figures/2D_plots/contour/xy_propagation/E_field_z/E_field_z_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xy_propagation/E_field_z/E_field_z_t=0.eps';
  elseif (xz_prop == 1)
    filename_png = './figures/2D_plots/contour/xz_propagation/E_field_z/E_field_z_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xz_propagation/E_field_z/E_field_z_t=0.eps';
  elseif (yz_prop == 1)
    filename_png = './figures/2D_plots/contour/yz_propagation/E_field_z/E_field_z_t=0.png';
%    filename_eps = './figures/2D_plots/contour/yz_propagation/E_field_z/E_field_z_t=0.eps';
  end
  print(fig4,'-dpng','-r300',filename_png);
%  print(fig4,'-depsc',filename_eps);

  figure(5);
  set(gcf,'Visible', 'off');  
  if (xy_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Bx_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Bx_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Bx_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Magnetic field','Fontsize',fontsize); 
  axis('equal');
  pbaspect([2 1 1]);
  c = colorbar;
  set(c,'Fontsize',fontsize);
  if (xy_prop == 1)
    filename_png = './figures/2D_plots/contour/xy_propagation/B_field_x/B_field_x_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xy_propagation/B_field_x/B_field_x_t=0.eps';
  elseif (xz_prop == 1) 
    filename_png = './figures/2D_plots/contour/xz_propagation/B_field_x/B_field_x_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xz_propagation/B_field_x/B_field_x_t=0.eps';
  elseif (yz_prop == 1)
    filename_png = './figures/2D_plots/contour/yz_propagation/B_field_x/B_field_x_t=0.png';
%    filename_eps = './figures/2D_plots/contour/yz_propagation/B_field_x/B_field_x_t=0.eps';
  end    
  print(fig5,'-dpng','-r300',filename_png);
%  print(fig5,'-depsc',filename_eps);

  figure(6);
  set(gcf,'Visible', 'off'); 
  if (xy_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{2},By_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{3},By_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    contour(r_sp_mesh_em{2},r_sp_mesh_em{3},By_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Magnetic field','Fontsize',fontsize); 
  axis('equal');
  pbaspect([2 1 1]);
  c = colorbar;
  set(c,'Fontsize',fontsize);
  if (xy_prop == 1)
    filename_png = './figures/2D_plots/contour/xy_propagation/B_field_y/B_field_y_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xy_propagation/B_field_y/B_field_y_t=0.eps';
  elseif (xz_prop == 1)
    filename_png = './figures/2D_plots/contour/xz_propagation/B_field_y/B_field_y_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xz_propagation/B_field_y/B_field_y_t=0.eps';
  elseif (yz_prop == 1)
    filename_png = './figures/2D_plots/contour/yz_propagation/B_field_y/B_field_y_t=0.png';
%    filename_eps = './figures/2D_plots/contour/yz_propagation/B_field_y/B_field_y_t=0.eps';
  end
  print(fig6,'-dpng','-r300',filename_png);
%  print(fig6,'-depsc',filename_eps);

  figure(7);
  set(gcf,'Visible', 'off');  
  if (xy_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Bz_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Bz_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Bz_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Magnetic field','Fontsize',fontsize); 
  axis('equal');
  pbaspect([2 1 1]);
  c = colorbar;
  set(c,'Fontsize',fontsize);
  if (xy_prop == 1)
    filename_png = './figures/2D_plots/contour/xy_propagation/B_field_z/B_field_z_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xy_propagation/B_field_z/B_field_z_t=0.eps';
  elseif (xz_prop == 1)
    filename_png = './figures/2D_plots/contour/xz_propagation/B_field_z/B_field_z_t=0.png';
%    filename_eps = './figures/2D_plots/contour/xz_propagation/B_field_z/B_field_z_t=0.eps';
  elseif (yz_prop == 1)
    filename_png = './figures/2D_plots/contour/yz_propagation/B_field_z/B_field_z_t=0.png';
%    filename_eps = './figures/2D_plots/contour/yz_propagation/B_field_z/B_field_z_t=0.eps';
  end
  print(fig7,'-dpng','-r300',filename_png);
%  print(fig7,'-depsc',filename_eps);


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % mesh plots

  figure(2);
  set(gcf,'Visible', 'off'); 
  if (xy_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Ex_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Ex_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Ex_plot);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('electric field','Fontsize',fontsize);
  if (xy_prop == 1)
    filename = './figures/2D_plots/mesh/xy_propagation/E_field_x/E_field_x_t=0.png';
  elseif (xz_prop == 1)
    filename = './figures/2D_plots/mesh/xz_propagation/E_field_x/E_field_x_t=0.png';
  elseif (yz_prop == 1)
    filename = './figures/2D_plots/mesh/yz_propagation/E_field_x/E_field_x_t=0.png';
  end
%  print(fig2,'-dpng',filename);

  figure(3);
  set(gcf,'Visible', 'off'); 
  if (xy_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Ey_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Ey_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Ey_plot);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('electric field','Fontsize',fontsize);
  if (xy_prop == 1)
    filename = './figures/2D_plots/mesh/xy_propagation/E_field_y/E_field_y_t=0.png';
  elseif (xz_prop == 1)
    filename = './figures/2D_plots/mesh/xz_propagation/E_field_y/E_field_y_t=0.png';
  elseif (yz_prop == 1)
    filename = './figures/2D_plots/mesh/yz_propagation/E_field_y/E_field_y_t=0.png';
  end
%  print(fig3,'-dpng',filename);

  figure(4);
  set(gcf,'Visible', 'off'); 
  if (xy_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Ez_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Ez_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Ez_plot);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('electric field','Fontsize',fontsize); 
  if (xy_prop == 1)
    filename = './figures/2D_plots/mesh/xy_propagation/E_field_z/E_field_z_t=0.png';
  elseif (xz_prop == 1)
    filename = './figures/2D_plots/mesh/xz_propagation/E_field_z/E_field_z_t=0.png';
  elseif (yz_prop == 1)
    filename = './figures/2D_plots/mesh/yz_propagation/E_field_z/E_field_z_t=0.png';
  end
%  print(fig4,'-dpng',filename);

  figure(5);
  set(gcf,'Visible', 'off');  
  if (xy_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Bx_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Bx_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Bx_plot);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Magnetic field','Fontsize',fontsize); 
  if (xy_prop == 1)
    filename = './figures/2D_plots/mesh/xy_propagation/B_field_x/B_field_x_t=0.png';
  elseif (xz_prop == 1)
    filename = './figures/2D_plots/mesh/xz_propagation/B_field_x/B_field_x_t=0.png';
  elseif (yz_prop == 1)
    filename = './figures/2D_plots/mesh/yz_propagation/B_field_x/B_field_x_t=0.png';
  end
%  print(fig5,'-dpng',filename);

  figure(6);
  set(gcf,'Visible', 'off');  
  if (xy_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},By_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},By_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},By_plot);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Magnetic field','Fontsize',fontsize); 
  if (xy_prop == 1)
    filename = './figures/2D_plots/mesh/xy_propagation/B_field_y/B_field_y_t=0.png';
  elseif (xz_prop == 1)
    filename = './figures/2D_plots/mesh/xz_propagation/B_field_y/B_field_y_t=0.png';
  elseif (yz_prop == 1)
    filename = './figures/2D_plots/mesh/yz_propagation/B_field_y/B_field_y_t=0.png';
  end
%  print(fig5,'-dpng',filename);

  figure(5);
  set(gcf,'Visible', 'off');  
  if (xy_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Bz_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_y_pnt_em max_y_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
  elseif (xz_prop == 1)
    mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Bz_plot);
    xlim([min_x_pnt_em max_x_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  elseif (yz_prop == 1)
    mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Bz_plot);
    xlim([min_y_pnt_em max_y_pnt_em]);
    ylim([min_z_pnt_em max_z_pnt_em]);
    xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
  end
  set(gca,'FontSize',fontsize);
  title('Magnetic field','Fontsize',fontsize); 
  if (xy_prop == 1)
    filename = './figures/2D_plots/mesh/xy_propagation/B_field_z/B_field_z_t=0.png';
  elseif (xz_prop == 1)
    filename = './figures/2D_plots/mesh/xz_propagation/B_field_z/B_field_z_t=0.png';
  elseif (yz_prop == 1)
    filename = './figures/2D_plots/mesh/yz_propagation/B_field_z/B_field_z_t=0.png';
  end
%  print(fig5,'-dpng',filename);


end

if (time_watch ==1) toc; fprintf(' \n'); end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creating two seperate vectors and mappings: one for the vacuuam and one for the medium grid points
%idx_vac = 0;
%idx_med = 0;
%for ii=1:n_x_em
%  for jj=1:n_y_em
%    for kk=1:n_z_em
%      if (medium_point(ii,jj,kk) == 1)
%        idx_vac = idx_vac+1;
%        Phi_vac_vector_em(                idx_vac) = sqrt(ep_tensor(ii,jj,kk)/2) * Ex(ii,jj,kk) + i/sqrt(2*mu_tensor(ii,jj,kk)) * Bx(ii,jj,kk);
%        Phi_vac_vector_em(  vac_grid_n_em+idx_vac) = sqrt(ep_tensor(ii,jj,kk)/2) * Ey(ii,jj,kk) + i/sqrt(2*mu_tensor(ii,jj,kk)) * By(ii,jj,kk);
%        Phi_vac_vector_em(2*vac_grid_n_em+idx_vac) = sqrt(ep_tensor(ii,jj,kk)/2) * Ez(ii,jj,kk) + i/sqrt(2*mu_tensor(ii,jj,kk)) * Bz(ii,jj,kk);
%        vac_map_em_ii(idx_v) = ii;
%        vac_map_em_jj(idx_v) = jj;
%        vac_map_em_kk(idx_v) = kk;
%      else
%        idx_med = idx_med+1;
%        Phi_med_vector_em(                idx_med) = sqrt(ep_tensor(ii,jj,kk)/2) * Ex(ii,jj,kk) + i/sqrt(2*mu_tensor(ii,jj,kk)) * Bx(ii,jj,kk);
%        Phi_med_vector_em(  med_grid_n_em+idx_med) = sqrt(ep_tensor(ii,jj,kk)/2) * Ey(ii,jj,kk) + i/sqrt(2*mu_tensor(ii,jj,kk)) * By(ii,jj,kk);
%        Phi_med_vector_em(2*med_grid_n_em+idx_med) = sqrt(ep_tensor(ii,jj,kk)/2) * Ez(ii,jj,kk) + i/sqrt(2*mu_tensor(ii,jj,kk)) * Bz(ii,jj,kk);
%        Phi_med_vector_em(3*med_grid_n_em+idx_med) = sqrt(ep_tensor(ii,jj,kk)/2) * Ex(ii,jj,kk) - i/sqrt(2*mu_tensor(ii,jj,kk)) * Bx(ii,jj,kk);
%        Phi_med_vector_em(4*med_grid_n_em+idx_med) = sqrt(ep_tensor(ii,jj,kk)/2) * Ey(ii,jj,kk) - i/sqrt(2*mu_tensor(ii,jj,kk)) * By(ii,jj,kk);
%        Phi_med_vector_em(5*med_grid_n_em+idx_med) = sqrt(ep_tensor(ii,jj,kk)/2) * Ez(ii,jj,kk) - i/sqrt(2*mu_tensor(ii,jj,kk)) * Bz(ii,jj,kk);
%        med_map_em_ii(idx_m) = ii;
%        med_map_em_jj(idx_m) = jj;
%        med_map_em_kk(idx_m) = kk;
%    end
%  end
%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialization of vacuum Hamiltonian
%for idx_vac=1:vac_grid_n_em
%  matrix   = zeros(3:3);
%  matrix(1,2)
%  matrix(1,3)
%  matrix(2,1)
%  matrix(2,3)
%  matrix(3,1)
%  matrix(3,2)
%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Riemann-Silberstein vector   

fprintf('Setting up Riemann-Silberstein vector and spinor vector  ...\n\n');
 
for ii=1:n_x_em
  for jj=1:n_y_em
    for kk=1:n_z_em
      Fx_plus( ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ex(ii,jj,kk) + i / sqrt(2*mu_tensor(ii,jj,kk)) * Bx(ii,jj,kk);
      Fx_minus(ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ex(ii,jj,kk) - i / sqrt(2*mu_tensor(ii,jj,kk)) * Bx(ii,jj,kk);
      Fy_plus( ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ey(ii,jj,kk) + i / sqrt(2*mu_tensor(ii,jj,kk)) * By(ii,jj,kk);
      Fy_minus(ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ey(ii,jj,kk) - i / sqrt(2*mu_tensor(ii,jj,kk)) * By(ii,jj,kk);
      Fz_plus( ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ez(ii,jj,kk) + i / sqrt(2*mu_tensor(ii,jj,kk)) * Bz(ii,jj,kk);
      Fz_minus(ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ez(ii,jj,kk) - i / sqrt(2*mu_tensor(ii,jj,kk)) * Bz(ii,jj,kk);
    end
  end
end

  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialization of the spinor vector
  
for ii=1:n_x_em
  for jj=1:n_y_em
    for kk=1:n_z_em
      % plus Spinor
      Phi_em{1}(ii,jj,kk) = Fx_plus(ii,jj,kk); 
      Phi_em{2}(ii,jj,kk) = Fy_plus(ii,jj,kk); 
      Phi_em{3}(ii,jj,kk) = Fz_plus(ii,jj,kk); 
      % minus Spinor
      Phi_em{4}(ii,jj,kk) = Fx_minus(ii,jj,kk);
      Phi_em{5}(ii,jj,kk) = Fy_minus(ii,jj,kk);
      Phi_em{6}(ii,jj,kk) = Fz_minus(ii,jj,kk);
    end
  end
end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization of exp_time_ev_fft_A, exp_time_ev_B, and exp_time_ev_com_AB with zeros

fprintf('Initializing arrays for time evolution ...\n\n');

if (conv_switch ==1)
  for pp=1:6
    for qq=1:6
     % for ii=1:n_x_em
     %   for jj=1:n_y_em
     %     for kk=1:n_z_em
     %       exp_time_ev_fft_A{pp}{qq}{ii}{jj}{kk}  = zeros(n_x_em,n_y_em,n_z_em);
     %     end
     %   end
     % end
      exp_time_ev_B{pp}{qq}      = zeros(n_x_em,n_y_em,n_z_em);
      exp_time_ev_com_AB{pp}{qq} = zeros(n_x_em,n_y_em,n_z_em);
    end
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lookup table for using constant v and p for the partial matrix A of the total EM Hamiltonian

if (conv_switch == 0)

  if (time_watch ==1) tic; end

  fprintf('Setting lookup table of Fourier transformed v and real space p for the time evolution exponential matrix of corresponding to matrix A ...\n\n');

  for ii=1:n_x_em
    for jj=1:n_y_em
      for kk=1:n_z_em

        matrix = zeros(6,6);
       
        matrix(1,2) = - i * v_tensor(ii,jj,kk) * m_sp_mesh_em{3}(kk) ;
        matrix(1,3) =   i * v_tensor(ii,jj,kk) * m_sp_mesh_em{2}(jj) ;
        matrix(2,1) =   i * v_tensor(ii,jj,kk) * m_sp_mesh_em{3}(kk) ;
        matrix(2,3) = - i * v_tensor(ii,jj,kk) * m_sp_mesh_em{1}(ii) ;
        matrix(3,1) = - i * v_tensor(ii,jj,kk) * m_sp_mesh_em{2}(jj) ;
        matrix(3,2) =   i * v_tensor(ii,jj,kk) * m_sp_mesh_em{1}(ii) ;
        matrix(4,5) =   i * v_tensor(ii,jj,kk) * m_sp_mesh_em{3}(kk) ;
        matrix(4,6) = - i * v_tensor(ii,jj,kk) * m_sp_mesh_em{2}(jj) ;
        matrix(5,4) = - i * v_tensor(ii,jj,kk) * m_sp_mesh_em{3}(kk) ;
        matrix(5,6) =   i * v_tensor(ii,jj,kk) * m_sp_mesh_em{1}(ii) ;
        matrix(6,4) =   i * v_tensor(ii,jj,kk) * m_sp_mesh_em{2}(jj) ;
        matrix(6,5) = - i * v_tensor(ii,jj,kk) * m_sp_mesh_em{1}(ii) ;

        exp_matrix = expm( -i/h * dt * matrix);
  
        for pp=1:6
          for qq=1:6
            exp_time_ev_fft_A{pp}{qq}(ii,jj,kk) = exp_matrix(pp,qq);
          end
        end

      end
    end
  end

  if (time_watch ==1) toc; fprintf(' \n'); end

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lookup table for using convolution of v and p for the partial matrix A of the total EM Hamiltonian

if (conv_switch == 1)

  if (time_watch ==1) tic; end

  fprintf('Setting lookup table of convolved Fourier transformed v and real space p for the time evolution exponential matrix of corresponding to matrix A ...\n\n');

  fprintf('Calculating auxiliary convolution array ...\n\n');

  
  % convolution calculation via matrix vector product
 
  idx_1 = 0;
  for idx_ii=1:2*n_x_em-1
    for idx_jj=1:2*n_y_em-1
      for idx_kk=1:2*n_z_em-1
        idx_1 = idx_1+1;
        map_diff(idx_ii,idx_jj,idx_kk) = idx_1;
        idx_2 = 0;
        for ii=1:n_x_em
          for jj=1:n_y_em 
            for kk=1:n_z_em
              idx_2 = idx_2 + 1;
              conv_exp_matrix(idx_1,idx_2) = 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em)           ...
                                           * exp(-i*2*pi*(ii-1)*(idx_ii-n_x_em)/n_x_em)                 ... 
                                           * exp(-i*2*pi*(jj-1)*(idx_jj-n_y_em)/n_y_em)                 ... 
                                           * exp(-i*2*pi*(kk-1)*(idx_kk-n_z_em)/n_z_em) ;
            end
          end
        end
      end
    end
  end

  idx_2 = 0;
  for ii=1:n_x_em
    for jj=1:n_y_em
      for kk=1:n_z_em
        idx_2=idx_2+1;
        v_vector(idx_2,1) = v_tensor(ii,jj,kk) ; 
      end
    end
  end

  conv_aux_res_vector = conv_exp_matrix * v_vector;

  idx_1 = 0;
  for ii=1:n_x_em
    for jj=1:n_y_em
      for kk=1:n_z_em
        idx_2 = 0;
        idx_1 = idx_1+1;
        for ii_kk=1:n_x_em
          diff_ii = (ii-1)-(ii_kk-1);
          idx_ii = n_x_em+diff_ii;
          for jj_kk=1:n_y_em
            diff_jj = (jj-1)-(jj_kk-1);
            idx_jj = n_y_em+diff_jj;
            for kk_kk=1:n_z_em
              idx_2 = idx_2 + 1;
              diff_kk = (kk-1)-(kk_kk-1);
              idx_kk = n_z_em+diff_kk;
     %         conv_aux_array{ii}{jj}{kk}(ii_kk,jj_kk,kk_kk) = conv_aux_res_vector(map_diff(idx_ii,idx_jj,idx_kk),1);
              conv_matrix{1}(idx_1,idx_2) = conv_aux_res_vector(map_diff(idx_ii,idx_jj,idx_kk,1)) * m_sp_mesh_em{1}(ii_kk);
              conv_matrix{2}(idx_1,idx_2) = conv_aux_res_vector(map_diff(idx_ii,idx_jj,idx_kk,1)) * m_sp_mesh_em{2}(jj_kk);
              conv_matrix{3}(idx_1,idx_2) = conv_aux_res_vector(map_diff(idx_ii,idx_jj,idx_kk,1)) * m_sp_mesh_em{3}(kk_kk);
            end
          end
        end
      end
    end
  end

  if (time_watch ==1) toc; fprintf(' \n'); end

  fprintf('Calculating convolution ...\n\n');

  if (time_watch ==1) tic; end

  idx_1 = 0;
  for qq=1:6
    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em
          idx_2 = 0;
          idx_1 = idx_1+1;
          map_qq(idx_1) = qq;
          map_ii(idx_1) = ii;
          map_jj(idx_1) = jj;
          map_kk(idx_1) = kk;
        end
      end
    end
  end

  matrix_6x6{1} = zeros(6,6);
  matrix_6x6{2} = zeros(6,6);
  matrix_6x6{3} = zeros(6,6);

  matrix_6x6{3}(1,2) = -i;
  matrix_6x6{2}(1,3) =  i;
  matrix_6x6{3}(2,1) =  i;
  matrix_6x6{1}(2,3) = -i;
  matrix_6x6{2}(3,1) = -i;
  matrix_6x6{1}(3,2) =  i;
  matrix_6x6{3}(4,5) =  i;
  matrix_6x6{2}(4,6) = -i;
  matrix_6x6{3}(5,4) = -i;
  matrix_6x6{1}(5,6) =  i;
  matrix_6x6{2}(6,4) =  i;
  matrix_6x6{1}(6,5) = -i;

  conv_A_matrix = kron(matrix_6x6{1},conv_matrix{1}) + kron(matrix_6x6{2},conv_matrix{2}) +  kron(matrix_6x6{3},conv_matrix{3});

  if exp_approx == 0
    conv_A_matrix_exp = sqrt(n_x_em)*sqrt(n_y_em)*sqrt(n_z_em) * expm(-i/h * dt * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) * conv_A_matrix);
  end
  
  if exp_approx == 1
    for nn=1:exp_approx_order
      if (nn == 1)
        conv_A_matrix_list{1}  = conv_A_matrix ;
%      elseif (nn == 2)
%        conv_A_matrix_list{2}  = conv_A_matrix^2 * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
      else
        conv_A_matrix_list{nn} = conv_A_matrix_list{nn-1} * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
      end
    end
    conv_A_matrix_exp = sqrt(n_x_em)*sqrt(n_y_em)*sqrt(n_z_em)* eye(6*n_x_em*n_y_em*n_z_em,6*n_x_em*n_y_em*n_z_em);
    for nn=1:exp_approx_order
      factor = 1/factorial(nn);
      conv_A_matrix_exp = conv_A_matrix_exp + factor * (-i/h*dt)^nn * conv_A_matrix_list{nn};
    end
    
%    conv_A_matrix_2   = conv_A_matrix^2                 * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
%    conv_A_matrix_3   = conv_A_matrix_2 * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
%    conv_A_matrix_4   = conv_A_matrix_3 * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
%    conv_A_matrix_5   = conv_A_matrix_4 * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
%    conv_A_matrix_6   = conv_A_matrix_5 * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
%    conv_A_matrix_7   = conv_A_matrix_6 * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
%    conv_A_matrix_8   = conv_A_matrix_7 * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
%    conv_A_matrix_exp = sqrt(n_x_em)*sqrt(n_y_em)*sqrt(n_z_em)* eye(6*n_x_em*n_y_em*n_z_em,6*n_x_em*n_y_em*n_z_em) ...
%                      +          (-i/h*dt)   * conv_A_matrix   + 1/2     * (-i/h*dt)^2 * conv_A_matrix_2          ...
%                      + 1/6    * (-i/h*dt)^3 * conv_A_matrix_3 + 1/24    * (-i/h*dt)^4 * conv_A_matrix_4          ...
%                      + 1/120  * (-i/h*dt)^5 * conv_A_matrix_5 + 1/720   * (-i/h*dt)^6 * conv_A_matrix_6          ...
%                      + 1/5040 * (-i/h*dt)^7 * conv_A_matrix_7 + 1/40320 * (-i/h*dt)^8 * conv_A_matrix_8          ;

%    for idx_1=1:6*n_x_em*n_y_em*n_z_em
%      for idx_2=1:6*n_x_em*n_y_em*n_z_em
%        conv_A_matrix_exp(idx_1,idx_2) - conv_A_matrix_exp_test(idx_1,idx_2)
%      end
%    end

  end

  for idx_1=1:6*n_x_em*n_y_em*n_z_em
    for idx_2=1:6*n_x_em*n_y_em*n_z_em
      exp_time_ev_fft_A{map_qq(idx_1)}{map_qq(idx_2)}{map_ii(idx_1)}{map_jj(idx_1)}{map_kk(idx_1)}(map_ii(idx_2),map_jj(idx_2),map_kk(idx_2)) = conv_A_matrix_exp(idx_1,idx_2);
    end
  end

  if (time_watch ==1) toc; fprintf(' \n'); end

end







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lookup table for the partial B of the total EM Hamiltonian

fprintf('Setting lookup table for the time evolution exponential matrix corresponding to matrix B  ...\n\n');

if (time_watch ==1) tic; end

for ii=1:n_x_em
  for jj=1:n_y_em
    for kk=1:n_z_em

      h_factor = v_tensor(ii,jj,kk)/h_tensor(ii,jj,kk);
      matrix = zeros(6,6);

      matrix(1,2) = -h * 1/2 * del_z_v_tensor(ii,jj,kk);
      matrix(1,3) =  h * 1/2 * del_y_v_tensor(ii,jj,kk);
      matrix(1,5) = -h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
      matrix(1,6) =  h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;

      matrix(2,1) =  h * 1/2 * del_z_v_tensor(ii,jj,kk);
      matrix(2,3) = -h * 1/2 * del_x_v_tensor(ii,jj,kk);
      matrix(2,4) =  h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
      matrix(2,6) = -h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;

      matrix(3,1) = -h * 1/2 * del_y_v_tensor(ii,jj,kk);
      matrix(3,2) =  h * 1/2 * del_x_v_tensor(ii,jj,kk);
      matrix(3,4) = -h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;
      matrix(3,5) =  h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;

      matrix(4,2) =  h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
      matrix(4,3) = -h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;
      matrix(4,5) =  h * 1/2 * del_z_v_tensor(ii,jj,kk);
      matrix(4,6) = -h * 1/2 * del_y_v_tensor(ii,jj,kk);
 
      matrix(5,1) = -h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
      matrix(5,3) =  h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;
      matrix(5,4) = -h * 1/2 * del_z_v_tensor(ii,jj,kk);
      matrix(5,6) =  h * 1/2 * del_x_v_tensor(ii,jj,kk);

      matrix(6,1) =  h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;
      matrix(6,2) = -h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;
      matrix(6,4) =  h * 1/2 * del_y_v_tensor(ii,jj,kk);
      matrix(6,5) = -h * 1/2 * del_x_v_tensor(ii,jj,kk);

      exp_matrix = expm( -i/h * dt * matrix);

      for pp=1:6
        for qq=1:6
          exp_time_ev_B{pp}{qq}(ii,jj,kk) = exp_matrix(pp,qq); 
        end 
      end

    end
  end
end

if (time_watch ==1) toc; fprintf(' \n'); end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lookup table for Hamiltonian commutator of A and B

fprintf('Setting lookup table for the time evolution exponential matrix corresponding to matrix [A,B]  ...\n\n');

if (time_watch ==1) tic; end

for ii=1:n_x_em
  for jj=1:n_y_em
    for kk=1:n_z_em

      matrix = zeros(6,6);

      matrix(1,2) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
      matrix(1,3) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
      matrix(2,1) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
      matrix(2,3) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;
      matrix(3,1) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
      matrix(3,2) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;
      matrix(4,5) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
      matrix(4,6) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
      matrix(5,4) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
      matrix(5,6) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;
      matrix(6,4) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
      matrix(6,5) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;

      exp_matrix = expm( 1/(2*(h^2)) * dt^2 * matrix );

      for pp=1:6
        for qq=1:6
          exp_time_ev_com_AB{pp}{qq}(ii,jj,kk) = exp_matrix(pp,qq);
        end
      end

    end
  end
end

if (time_watch ==1) toc; fprintf(' \n'); end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup mask function for photons

rel_border = 0.1;
min_border_x = round(rel_border*n_x_em);
max_border_x = round(n_x_em-min_border_x);
min_border_y = round(rel_border*n_y_em);
max_border_y = round(n_y_em-min_border_y);
min_border_z = round(rel_border*n_z_em);
max_border_z = round(n_z_em-min_border_z);
nx_mask      = double(min_border_x);
ny_mask      = double(min_border_y);
nz_mask      = double(min_border_z);
for qq=1:6
  for ii=1:n_x_em
    for jj=1:n_y_em
      for kk=1:n_z_em
        if  (ii<min_border_x | ii>max_border_x) 
          if (ii<min_border_x)
            mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(ii-1)/nx_mask))));
          elseif (ii>max_border_x)
            mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(n_x_em-ii)/nx_mask))));
          end
        elseif (jj<min_border_y | jj>max_border_y)
          if (jj<min_border_y)
            mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(jj-1)/ny_mask))));
          elseif (jj>max_border_y)
            mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(n_y_em-jj)/ny_mask))));
          end
        elseif (kk<min_border_z | kk>max_border_z)
          if (kk<min_border_z)
            mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(kk-1)/nz_mask))));
          elseif (kk>max_border_z)
            mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(n_z_em-kk)/nz_mask))));
          end
        else
          mask{qq}(ii,jj,kk) = 1.0;
        end
      end
    end
  end
end


% absxmask_pt = rs_spo_mask_function(n_x_em, max_x_pnt_em, max_y_pnt_em)
% absymask_pt = rs_spo_mask_function(n_y_em, max_x_pnt_em, max_y_pnt_em);
% abszmask_pt = rs_spo_mask_function(n_z_em, max_x_pnt_em, max_y_pnt_em);
%% setup mask function for atom
% absxmask_at = rs_spo_mask_function(1, 1, 1);
% absymask_at = rs_spo_mask_function(1, 1, 1);

%% global mask function
% absxyzmask_1pt = reshape(kron(absymask_at, kron(absxmask_at, kron(abszmask_pt, kron(absymask_pt, absxmask_pt)))), n_x_em, n_y_em, n_z_em, n_x_ma, n_y_ma);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Schroedinger Hamiltonian

%for ii=1:n_ma_x
%  Phi_xx_ma(ii,1) = 2 *(pi)^(-1/4)/(sqrt(sigma_ma)) * exp(-(xx_ma(ii)^2)/(4*sigma_ma^2));
%end
%Hm_ma    = -h^2/(2*m)*second_deriv_fd;
%unitm_ma = sparse(eye(nx_ma));



 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time evolution

fprintf('Schroedinger and electromagnetic field propagation ...\n\n');

for t_st=1:timesteps

  if (time_watch ==1) tic; end

  if (mod(t_st,10) == 0)
    steps_string = int2str(t_st);
    combinedStr = strcat('Timesteps done:    ',steps_string,' ...\n\n');
    fprintf(combinedStr)
  end

  % calculation of matter wave function
  if (calc_ma == 1) 

    % Schroedinger propagation
%    Phi_xx_ma_old = Phi_xx_ma;
%    b         = (unitm_ma - i * Hm_ma*dt) * Phi_xx_ma;
%    Phi_xx_ma = (unitm_ma + i * Hm_ma*dt) \ b;
%
%    time_str = int2str(ii);
%    filename = './figures/wavefunction/wave_packet_t='
%    combinedStr = strcat(filename,time_str,'.png');
%    print(fig1,'-dpng',combinedStr);
  
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Maxwell propagation
  if (calc_em == 1)

    % save old Phi_em
    Phi_em_old = Phi_em;

    

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculation of time evolution operator acting on electromagnetic field wave function Phi_em

    fprintf('Multiplying electromagnetic field wavefunction with exp_time_ev_com_AB ...\n\n');

    % multiply with matrix exp_time_ev_com_AB
    for pp=1:6
      tmp_Phi_em{pp} = zeros(n_x_em,n_y_em,n_z_em);
      for qq=1:6
        tmp_Phi_em{pp} = tmp_Phi_em{pp} + exp_time_ev_com_AB{pp}{qq}.*Phi_em{qq};
      end
    end
    for pp=1:6
      Phi_em{pp} = tmp_Phi_em{pp};
    end

   fprintf('Multiplying electromagnetic field wavefunction with exp_time_ev_B ...\n\n');

    % multiply with matrix exp_time_ev_B
    for pp=1:6
      tmp_Phi_em{pp} = zeros(n_x_em,n_y_em,n_z_em);
      for qq=1:6
        tmp_Phi_em{pp} = tmp_Phi_em{pp} + exp_time_ev_B{pp}{qq}.*Phi_em{qq};
      end
    end
    for pp=1:6
      Phi_em{pp} = tmp_Phi_em{pp};
    end

    if ( conv_switch == 1 )

      fprintf('Fourrier transform of the electromagnetic field wavefunction ...\n\n');

      % forward Fourier f%ransformation of Phi_em 
      for pp=1:6
        fft_Phi_em{pp} = 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) * fftn(Phi_em{pp});
      end

      % creating fft_Phi_em_vector for convolution
 %     for pp=1:6
 %       idx = 0;
 %       for ii=1:n_x_em
 %         for jj=1:n_y_em
 %           for kk=1:n_z_em
 %             idx= idx + 1;
 %             fft_Phi_em_vector{pp}(idx) = fft_Phi_em{pp}(ii,jj,kk);
 %           end
 %         end
 %       end
 %     end

      fprintf('Multiplying electric field wavefunction with exp_time_ev_com_A ...\n\n');

      if (time_watch ==1) tic; end

      idx = 0;
      for qq=1:6
        for ii=1:n_x_em
          for jj=1:n_y_em
            for kk=1:n_z_em
              idx = idx+1;
              fft_Phi_em_vector(idx,1) = fft_Phi_em{qq}(ii,jj,kk);
             end
          end
        end
      end

      tmp_fft_Phi_em_vector = conv_A_matrix_exp * fft_Phi_em_vector;

      for idx=1:6*n_x_em*n_y_em*n_z_em
        tmp_Phi_em{map_qq(idx)}(map_ii(idx),map_jj(idx),map_kk(idx)) = tmp_fft_Phi_em_vector(idx,1);
      end
      
%      if (time_watch ==1) toc; end

%      if (time_watch ==1) tic; end
%
%      % multiply with matrix exp_time_ev_fft_A
%      for pp=1:6
%        tmp_Phi_em{pp} = zeros(n_x_em,n_y_em,n_z_em);
%      end
%
%      for ii=1:n_x_em
%        for jj=1:n_y_em
%          for kk=1:n_z_em
%
%            for pp=1:6            
%              for qq=1:6         
% 
%                for ii_kk=1:n_x_em
%                  for jj_kk=1:n_y_em
%                    for kk_kk=1:n_z_em
%                      tmp_Phi_em{pp}(ii,jj,kk) = tmp_Phi_em{pp}(ii,jj,kk) + exp_time_ev_fft_A{pp}{qq}{ii}{jj}{kk}(ii_kk,jj_kk,kk_kk)*fft_Phi_em{qq}(ii_kk,jj_kk,kk_kk);
%                    end
%                  end
%                end
%
%              end
%            end
%
%          end
%        end
%      end
%
%      if (time_watch ==1) toc; fprintf(' \n\n'); end

      for pp=1:6
        fft_Phi_em{pp} = tmp_Phi_em{pp};
      end

      % backward Fourier transformation of fft_phi_em
      for pp=1:6
        Phi_em{pp} = ifftn(fft_Phi_em{pp});
      end

    elseif (conv_switch == 0)

      % forward Fourier f%ransformation of Phi_em 
      for ii=1:6
        fft_Phi_em{ii} = fftn(Phi_em{ii});
      end

      % multiply with matrix exp_time_ev_fft_A
      for ii=1:6
        tmp_Phi_em{ii} = zeros(n_x_em,n_y_em,n_z_em);
        for jj=1:6
          tmp_Phi_em{ii} = tmp_Phi_em{ii} + exp_time_ev_fft_A{ii}{jj}.*fft_Phi_em{jj};
        end
      end
      for ii=1:6
        fft_Phi_em{ii} = tmp_Phi_em{ii};
      end

      % backward Fourier transformation of fft_phi_em
      for ii=1:6
        Phi_em{ii} = ifftn(fft_Phi_em{ii});
      end
  
    end

    % multiply with mask function
    for ii=1:6
      Phi_em{ii} = mask{ii}.*Phi_em{ii};
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculation of E-field and B-field afer every timestep
    
    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em
          Ex(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{1}(ii,jj,kk) );
          Ey(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{2}(ii,jj,kk) );
          Ez(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{3}(ii,jj,kk) );
          Bx(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{1}(ii,jj,kk) );
          By(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{2}(ii,jj,kk) );
          Bz(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{3}(ii,jj,kk) );
        end
      end
    end
     

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculation of total time and the EM field energy for each time step and plot

    time_array(t_st)   = double(t_st)*dt;

%    energy = 0;
%    for ii=1:n_x_em 
%      for jj=1:n_y_em
%        for kk=1:n_z_em
%          energy = energy + ( conj(Fx_plus(ii,jj,kk))*Fx_plus(ii,jj,kk) + conj(Fy_plus(ii,jj,kk))*Fy_plus(ii,jj,kk) + conj(Fz_plus(ii,jj,kk))*Fz_plus(ii,jj,kk) )  ...
%                              * dx_em * dy_em * dz_em; 
%        end
%      end
%    end
%    energy_array(t_st) = energy;

%    figure(6);
%    set(gcf,'Visible','off');
%    plot(time_array,energy_array);
%    filename = './figures/energy.png';
%    print(fig6,'-dpng',filename); 
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % plotting E field and B field for 1D calculation

    if (mod(t_st,plot_step) == 0)

    fprintf('Plotting electromagnetic field ...\n\n');

    if ( (x_prop == 1) | (y_prop == 1) | (z_prop == 1) )

      if (x_prop == 1)
        for ii=1:n_x_em
          plot_axis = r_sp_mesh_em{1};
          Ex_plot(ii) = Ex(ii,1,1);
          Ey_plot(ii) = Ey(ii,1,1);
          Ez_plot(ii) = Ez(ii,1,1);
          Bx_plot(ii) = Bx(ii,1,1);
          By_plot(ii) = By(ii,1,1);
          Bz_plot(ii) = By(ii,1,1);
        end
      elseif (y_prop == 1)
        for jj=1:n_y_em
          plot_axis = r_sp_mesh_em{2};
          Ex_plot(jj) = Ex(1,jj,1);
          Ey_plot(jj) = Ey(1,jj,1);
          Ez_plot(jj) = Ez(1,jj,1);
          Bx_plot(jj) = Bx(1,jj,1);
          By_plot(jj) = By(1,jj,1);
          Bz_plot(jj) = By(1,jj,1);
        end
      elseif (z_prop == 1)
        for kk=1:n_z_em
          plot_axis = r_sp_mesh_em{3};
          Ex_plot(kk) = Ex(1,1,kk);
          Ey_plot(kk) = Ey(1,1,kk);
          Ez_plot(kk) = Ez(1,1,kk);
          Bx_plot(kk) = Bx(1,1,kk);
          By_plot(kk) = By(1,1,kk);
          Bz_plot(kk) = By(1,1,kk);
        end
      end


      figure(2);
      set(gcf,'Visible', 'off'); 
      plot(plot_axis,Ex_plot);
      time_str = int2str(t_st);
      if (x_prop == 1)
        filename = './figures/1D_plots/x_propagation/E_field_x/E_field_x_t=';
      elseif (y_prop == 1)
        filename = './figures/1D_plots/y_propagation/E_field_x/E_field_x_t=';
      elseif (z_prop == 1)
        filename = './figures/1D_plots/z_propagation/E_field_x/E_field_x_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig2,'-dpng',combinedStr);
 
      figure(3);
      set(gcf,'Visible', 'off'); 
      plot(plot_axis,Ey_plot);
      time_str = int2str(t_st);
      if (x_prop == 1)
        filename = './figures/1D_plots/x_propagation/E_field_y/E_field_y_t=';
      elseif (y_prop == 1)
        filename = './figures/1D_plots/y_propagation/E_field_y/E_field_y_t=';
      elseif (z_prop == 1)
        filename = './figures/1D_plots/z_propagation/E_field_y/E_field_y_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig3,'-dpng',combinedStr);
 
      figure(4);
      set(gcf,'Visible', 'off'); 
      plot(plot_axis,Ez_plot);
      time_str = int2str(t_st);
      if (x_prop == 1)
        filename = './figures/1D_plots/x_propagation/E_field_z/E_field_z_t=';
      elseif (y_prop == 1)
        filename = './figures/1D_plots/y_propagation/E_field_z/E_field_z_t=';
      elseif (z_prop == 1)
        filename = './figures/1D_plots/z_propagation/E_field_z/E_field_z_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig4,'-dpng',combinedStr);

      figure(5);
      set(gcf,'Visible', 'off'); 
      plot(plot_axis,Bx_plot);
      time_str = int2str(t_st);
      if (x_prop == 1)
        filename = './figures/1D_plots/x_propagation/B_field_x/B_field_x_t=';
      elseif (y_prop == 1)
        filename = './figures/1D_plots/y_propagation/B_field_x/B_field_x_t=';
      elseif (z_prop == 1)
        filename = './figures/1D_plots/z_propagation/B_field_x/B_field_x_t=';
      end
      combinedStr =  strcat(filename,time_str,'.png');
      print(fig5,'-dpng',combinedStr);

      figure(6);
      set(gcf,'Visible', 'off');
      plot(plot_axis,By_plot);
      time_str = int2str(t_st);
      if (x_prop == 1)
        filename = './figures/1D_plots/x_propagation/B_field_y/B_field_y_t=';
      elseif (y_prop == 1)
        filename = './figures/1D_plots/y_propagation/B_field_y/B_field_y_t=';
      elseif (z_prop == 1)
        filename = './figures/1D_plots/z_propagation/B_field_y/B_field_y_t=';
      end
      combinedStr =  strcat(filename,time_str,'.png');
      print(fig6,'-dpng',combinedStr);

      figure(7);
      set(gcf,'Visible', 'off');
      plot(plot_axis,Bz_plot);
      time_str = int2str(t_st);
      if (x_prop == 1)
        filename = './figures/1D_plots/x_propagation/B_field_z/B_field_z_t=';
      elseif (y_prop == 1)
        filename = './figures/1D_plots/y_propagation/B_field_z/B_field_z_t=';
      elseif (z_prop == 1)
        filename = './figures/1D_plots/z_propagation/B_field_z/B_field_z_t=';
      end
      combinedStr =  strcat(filename,time_str,'.png');
      print(fig7,'-dpng',combinedStr);
   
    end

    if ( (xy_prop == 1) | (xz_prop == 1) | (yz_prop == 1) )

      if (xy_prop == 1)
        for ii=1:n_x_em
          for jj=1:n_y_em
            Ex_plot(jj,ii) = Ex(ii,jj,1);
            Ey_plot(jj,ii) = Ey(ii,jj,1);
            Ez_plot(jj,ii) = Ez(ii,jj,1);
            Bx_plot(jj,ii) = Bx(ii,jj,1);
            By_plot(jj,ii) = By(ii,jj,1);
            Bz_plot(jj,ii) = Bz(ii,jj,1);
          end
        end
      elseif (xz_porp == 1)
        for ii=1:n_x_em
          for kk=1:n_z_em
            Ex_plot(kk,ii) = Ex(ii,1,kk);
            Ey_plot(kk,ii) = Ey(ii,1,kk);
            Ez_plot(kk,ii) = Ez(ii,1,kk);
            Bx_plot(kk,ii) = Bx(ii,1,kk);
            By_plot(kk,ii) = By(ii,1,kk);
            Bz_plot(kk,ii) = Bz(ii,1,kk);
          end
        end
      elseif (yz_prop == 1)
        for jj=1:n_x_em 
          for kk=1:n_z_em
            Ex_plot(kk,jj) = Ex(1,jj,kk);
            Ey_plot(kk,jj) = Ey(1,jj,kk);
            Ez_plot(kk,jj) = Ez(1,jj,kk);
            Bx_plot(kk,jj) = Bx(1,jj,kk);
            By_plot(kk,jj) = By(1,jj,kk);
            Bz_plot(kk,jj) = Bz(1,jj,kk);
          end
        end
      end
        
   
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % contour plots
 
      figure(2);
      set(gcf,'Visible', 'off'); 
      if (xy_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Ex_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_y_pnt_em max_y_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
      elseif (xz_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Ex_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      elseif (yz_prop == 1)
        contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Ex_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_y_pnt_em max_y_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      end
      set(gca,'FontSize',fontsize);
      title('electric field','Fontsize',fontsize); 
      axis('equal');
      pbaspect([2 1 1]);  
      c = colorbar;
      set(c,'Fontsize',fontsize);
      time_str = int2str(t_st);
      if (xy_prop == 1)
        filename = './figures/2D_plots/contour/xy_propagation/E_field_x/E_field_x_t=';
      elseif (xz_prop == 1)
        filename = './figures/2D_plots/contour/xz_propagation/E_field_x/E_field_x_t=';
      elseif (yz_prop == 1)
        filename = './figures/2D_plots/contour/yz_propagation/E_field_x/E_field_x_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig2,'-dpng','-r300',combinedStr);
%      combinedStr = strcat(filename,time_str,'.eps');
%      print(fig2,'-depsc',combinedStr);
 
      figure(3);
      set(gcf,'Visible', 'off'); 
      if (xy_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Ey_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_y_pnt_em max_y_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
      elseif (xz_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Ey_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      elseif (yz_prop == 1)
        contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Ey_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_y_pnt_em max_y_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      end
      title('electric field','Fontsize',fontsize); 
      axis('equal');
      pbaspect([2 1 1]); 
      c = colorbar;
      set(c,'Fontsize',fontsize);
      time_str = int2str(t_st);
      if (xy_prop == 1)
        filename = './figures/2D_plots/contour/xy_propagation/E_field_y/E_field_y_t=';
      elseif (xz_prop == 1)
        filename = './figures/2D_plots/contour/xz_propagation/E_field_y/E_field_y_t=';
      elseif (yz_prop == 1)
        filename = './figures/2D_plots/contour/yz_propagation/E_field_y/E_field_y_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig3,'-dpng','-r300',combinedStr);
%      combinedStr = strcat(filename,time_str,'.eps');
%      print(fig3,'-depsc',combinedStr);
  
      figure(4);
      set(gcf,'Visible', 'off'); 
      if (xy_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Ez_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_y_pnt_em max_y_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
      elseif (xz_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Ez_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      elseif (yz_prop == 1)
        contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Ez_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_y_pnt_em max_y_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      end
      set(gca,'FontSize',fontsize);
      title('electric field','Fontsize',fontsize); 
      axis('equal');
      pbaspect([2 1 1]);  
      c = colorbar;
      set(c,'Fontsize',fontsize);
      time_str = int2str(t_st);
      if (xy_prop == 1)
        filename = './figures/2D_plots/contour/xy_propagation/E_field_z/E_field_z_t=';
      elseif (xz_prop == 1)
        filename = './figures/2D_plots/contour/xz_propagation/E_field_z/E_field_z_t=';
      elseif (yz_prop == 1)
        filename = './figures/2D_plots/contour/yz_propagation/E_field_z/E_field_z_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig4,'-dpng','-r300',combinedStr);
%      combinedStr = strcat(filename,time_str,'.eps');
%      print(fig4,'-depsc',combinedStr);
 
      figure(5);
      set(gcf,'Visible', 'off'); 
      if (xy_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Bx_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_y_pnt_em max_y_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
      elseif (xz_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Bx_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      elseif (yz_prop == 1)
        contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Bx_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_y_pnt_em max_y_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      end
      title('Magnetic field','Fontsize',fontsize); 
      axis('equal');
      pbaspect([2 1 1]);  
      c = colorbar;
      set(c,'Fontsize',fontsize);
      time_str = int2str(t_st);
      if (xy_prop == 1)
        filename = './figures/2D_plots/contour/xy_propagation/B_field_x/B_field_x_t=';
      elseif (xz_prop == 1)
        filename = './figures/2D_plots/contour/xz_propagation/B_field_x/B_field_x_t=';
      elseif (yz_prop == 1)
        filename = './figures/2D_plots/contour/yz_propagation/B_field_x/B_field_x_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig5,'-dpng','-r300',combinedStr);
%      combinedStr = strcat(filename,time_str,'.eps');
%      print(fig5,'-depsc',combinedStr); 

      figure(6);
      set(gcf,'Visible', 'off'); 
      if (xy_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{2},By_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_y_pnt_em max_y_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
      elseif (xz_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{3},By_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      elseif (yz_prop == 1)
        contour(r_sp_mesh_em{2},r_sp_mesh_em{3},By_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_y_pnt_em max_y_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      end
      title('Magnetic field','Fontsize',fontsize); 
      axis('equal');
      pbaspect([2 1 1]);  
      c = colorbar;
      set(c,'Fontsize',fontsize);
      time_str = int2str(t_st);
      if (xy_prop == 1)
        filename = './figures/2D_plots/contour/xy_propagation/B_field_y/B_field_y_t=';
      elseif (xz_prop == 1)
        filename = './figures/2D_plots/contour/xz_propagation/B_field_y/B_field_y_t=';
      elseif (yz_prop == 1)
        filename = './figures/2D_plots/contour/yz_propagation/B_field_y/B_field_y_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig6,'-dpng','-r300',combinedStr);
%      combinedStr = strcat(filename,time_str,'.eps');
%      print(fig6,'-depsc',combinedStr);

      figure(7);
      set(gcf,'Visible', 'off'); 
      if (xy_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{2},Bz_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_y_pnt_em max_y_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
      elseif (xz_prop == 1)
        contour(r_sp_mesh_em{1},r_sp_mesh_em{3},Bz_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_x_pnt_em max_x_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      elseif (yz_prop == 1)
        contour(r_sp_mesh_em{2},r_sp_mesh_em{3},Bz_plot,[contour_min_neg:contour_step:contour_max_neg, contour_min_pos:contour_step:contour_max_pos]);
        xlim([min_y_pnt_em max_y_pnt_em]);
        ylim([min_z_pnt_em max_z_pnt_em]);
        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
      end
      title('electric field','Fontsize',fontsize); 
      axis('equal');
      pbaspect([2 1 1]);  
      c = colorbar;
      set(c,'Fontsize',fontsize);
      time_str = int2str(t_st);
      if (xy_prop == 1)
        filename = './figures/2D_plots/contour/xy_propagation/B_field_z/B_field_z_t=';
      elseif (xz_prop == 1)
        filename = './figures/2D_plots/contour/xz_propagation/B_field_z/B_field_z_t=';
      elseif (yz_prop == 1)
        filename = './figures/2D_plots/contour/yz_propagation/B_field_z/B_field_z_t=';
      end
      combinedStr = strcat(filename,time_str,'.png');
      print(fig4,'-dpng','-r300',combinedStr);
%      combinedStr = strcat(filename,time_str,'.eps');
%      print(fig4,'-depsc',combinedStr);


      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % mesh plots
 
%      figure(2);
%      set(gcf,'Visible', 'off'); 
%      if (xy_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Ex_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_y_pnt_em max_y_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
%      elseif (xz_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Ex_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      elseif (yz_prop == 1)
%        mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Ex_plot);
%        xlim([min_y_pnt_em max_y_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      end
%      title('electric field','Fontsize',fontsize);
%      time_str = int2str(t_st);
%      if (xy_prop == 1)
%        filename = './figures/2D_plots/mesh/xy_propagation/E_field_x/E_field_x_t=';
%      elseif (xz_prop == 1)
%        filename = './figures/2D_plots/mesh/xz_propagation/E_field_x/E_field_x_t=';
%      elseif (yz_prop == 1)
%        filename = './figures/2D_plots/mesh/yz_propagation/E_field_x/E_field_x_t=';
%      end
%      combinedStr = strcat(filename,time_str,'.png');
%      print(fig2,'-dpng',combinedStr);
 
%      figure(3);
%      set(gcf,'Visible', 'off'); 
%      if (xy_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Ey_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_y_pnt_em max_y_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
%      elseif (xz_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Ey_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      elseif (yz_prop == 1)
%        mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Ey_plot);
%        xlim([min_y_pnt_em max_y_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      end
%      title('electric field','Fontsize',fontsize);
%      time_str = int2str(t_st);
%      if (xy_prop == 1)
%        filename = './figures/2D_plots/mesh/xy_propagation/E_field_y/E_field_y_t=';
%      elseif (xz_prop == 1)
%        filename = './figures/2D_plots/mesh/xz_propagation/E_field_y/E_field_y_t=';
%      elseif (yz_prop == 1)
%        filename = './figures/2D_plots/mesh/yz_propagation/E_field_y/E_field_y_t=';
%      end
%      combinedStr = strcat(filename,time_str,'.png');
%      print(fig3,'-dpng',combinedStr);
% 
%      figure(4);
%      set(gcf,'Visible', 'off'); 
%      if (xy_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Ez_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_y_pnt_em max_y_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
%      elseif (xz_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Ez_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      elseif (yz_prop == 1)
%        mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Ez_plot);
%        xlim([min_y_pnt_em max_y_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      end
%      title('electric field','Fontsize',fontsize);
%      time_str = int2str(t_st);
%      if (xy_prop == 1)
%        filename = './figures/2D_plots/mesh/xy_propagation/E_field_z/E_field_z_t=';
%      elseif (xz_prop == 1)
%        filename = './figures/2D_plots/mesh/xz_propagation/E_field_z/E_field_z_t=';
%      elseif (yz_prop == 1)
%        filename = './figures/2D_plots/mesh/yz_propagation/E_field_z/E_field_z_t=';
%      end
%      combinedStr = strcat(filename,time_str,'.png');
%      print(fig4,'-dpng',combinedStr);
% 
%      figure(5);
%      set(gcf,'Visible', 'off'); 
%      if (xy_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Bx_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_y_pnt_em max_y_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
%      elseif (xz_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Bx_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      elseif (yz_prop == 1)
%        mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Bx_plot);
%        xlim([min_y_pnt_em max_y_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      end
%      title('Magnetic field','Fontsize',fontsize);
%      time_str = int2str(t_st);
%       if (xy_prop == 1)
%        filename = './figures/2D_plots/mesh/xy_propagation/B_field_x/B_field_x_t=';
%      elseif (xz_prop == 1)
%        filename = './figures/2D_plots/mesh/xz_propagation/B_field_x/B_field_x_t=';
%      elseif (yz_prop == 1)
%        filename = './figures/2D_plots/mesh/yz_propagation/B_field_x/B_field_x_t=';
%      end
%      combinedStr =  strcat(filename,time_str,'.png');
%      print(fig5,'-dpng',combinedStr);
%
%      figure(6);
%      set(gcf,'Visible', 'off'); 
%      if (xy_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},By_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_y_pnt_em max_y_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
%      elseif (xz_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},By_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      elseif (yz_prop == 1)
%        mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},By_plot);
%        xlim([min_y_pnt_em max_y_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      end
%      title('Magnetic field','Fontsize',fontsize);
%      time_str = int2str(t_st);
%       if (xy_prop == 1)
%        filename = './figures/2D_plots/mesh/xy_propagation/B_field_y/B_field_y_t=';
%      elseif (xz_prop == 1)
%        filename = './figures/2D_plots/mesh/xz_propagation/B_field_y/B_field_y_t=';
%      elseif (yz_prop == 1)
%        filename = './figures/2D_plots/mesh/yz_propagation/B_field_y/B_field_y_t=';
%      end
%      combinedStr =  strcat(filename,time_str,'.png');
%      print(fig5,'-dpng',combinedStr);
%
%      figure(7);
%      set(gcf,'Visible', 'off'); 
%      if (xy_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{2},Bz_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_y_pnt_em max_y_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('y [a.u.]','Fontsize',fontsize);
%      elseif (xz_prop == 1)
%        mesh(r_sp_mesh_em{1},r_sp_mesh_em{3},Bz_plot);
%        xlim([min_x_pnt_em max_x_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('x [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      elseif (yz_prop == 1)
%        mesh(r_sp_mesh_em{2},r_sp_mesh_em{3},Bz_plot);
%        xlim([min_y_pnt_em max_y_pnt_em]);
%        ylim([min_z_pnt_em max_z_pnt_em]);
%        xlabel('y [a.u.]','Fontsize',fontsize); ylabel('z [a.u.]','Fontsize',fontsize);
%      end
%      title('Magnetic field','Fontsize',fontsize);
%      time_str = int2str(t_st);
%       if (xy_prop == 1)
%        filename = './figures/2D_plots/mesh/xy_propagation/B_field_z/B_field_z_t=';
%      elseif (xz_prop == 1)
%        filename = './figures/2D_plots/mesh/xz_propagation/B_field_z/B_field_z_t=';
%      elseif (yz_prop == 1)
%        filename = './figures/2D_plots/mesh/yz_propagation/B_field_z/B_field_z_t=';
%      end
%      combinedStr =  strcat(filename,time_str,'.png');
%      print(fig5,'-dpng',combinedStr);

    end

    end
  
  end

if (time_watch ==1) toc; fprintf(' \n\n'); end

end





