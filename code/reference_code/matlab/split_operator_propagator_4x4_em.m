% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [exp_time_ev_fft_A] = split_operator_propagator_4x4_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, dt, c_0, h)

    fprintf('Initializing arrays for time evolution ...\n\n');

    for pp=1:4
      for qq=1:4
        exp_time_ev_A{pp}{qq}      = zeros(n_x_em,n_y_em,n_z_em);
      end
    end  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % lookup table for using convolution of v and p for the partial matrix A of the total EM Hamiltonian

    fprintf('Setting lookup table of time evolution operator in momentum space ...\n\n');

    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em

          matrix = zeros(4,4);

          matrix(1,1) =  c_0 *   m_sp_mesh_em{3}(kk);
          matrix(1,3) =  c_0 * ( m_sp_mesh_em{1}(ii) - i * m_sp_mesh_em{2}(jj) );

          matrix(2,2) =  c_0 *   m_sp_mesh_em{3}(kk);
          matrix(2,4) =  c_0 * ( m_sp_mesh_em{1}(ii) - i * m_sp_mesh_em{2}(jj) );

          matrix(3,1) =  c_0 * ( m_sp_mesh_em{1}(ii) + i * m_sp_mesh_em{2}(jj) );
          matrix(3,3) = -c_0 *   m_sp_mesh_em{3}(kk);

          matrix(4,2) =  c_0 * ( m_sp_mesh_em{1}(ii) + i * m_sp_mesh_em{2}(jj) );
          matrix(4,4) = -c_0 *   m_sp_mesh_em{3}(kk);
   
          exp_matrix = expm( -i/h * dt * matrix);

          for pp=1:4
            for qq=1:4
              exp_time_ev_fft_A{pp}{qq}(ii,jj,kk) = exp_matrix(pp,qq); 
            end 
          end

        end
      end
    end
