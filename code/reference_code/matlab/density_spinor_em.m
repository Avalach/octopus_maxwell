% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dens_spinor_em] = density_spinor_em(spinor_calc_mode, jx_em, jy_em, jz_em, rho_em, ep_tensor, v_tensor, n_x_em, n_y_em, n_z_em)

    if (spinor_calc_mode == 1)
      dens_spinor_em = 0;
    elseif (spinor_calc_mode == 2)
      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            dens_spinor_em{1}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * ( -jx_em(ii,jj,kk) + i * jy_em(ii,jj,kk) );
            dens_spinor_em{2}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jz_em(ii,jj,kk) - v_tensor(ii,jj,kk) * rho_em(ii,jj,kk) );
            dens_spinor_em{3}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jz_em(ii,jj,kk) + v_tensor(ii,jj,kk) * rho_em(ii,jj,kk) );
            dens_spinor_em{4}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jx_em(ii,jj,kk) + i * jy_em(ii,jj,kk) );
          end
        end
      end
    elseif (spinor_calc_mode == 3)
      dens_spinor_em = 0;
    elseif (spinor_calc_mode == 4)
      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            dens_spinor_em{1}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * ( -jx_em(ii,jj,kk) + i * jy_em(ii,jj,kk) );
            dens_spinor_em{2}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jz_em(ii,jj,kk) - v_tensor(ii,jj,kk) * rho_em(ii,jj,kk) );
            dens_spinor_em{3}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jz_em(ii,jj,kk) + v_tensor(ii,jj,kk) * rho_em(ii,jj,kk) );
            dens_spinor_em{4}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jx_em(ii,jj,kk) + i * jy_em(ii,jj,kk) );
            dens_spinor_em{5}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * ( -jx_em(ii,jj,kk) - i * jy_em(ii,jj,kk) );
            dens_spinor_em{6}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jz_em(ii,jj,kk) - v_tensor(ii,jj,kk) * rho_em(ii,jj,kk) );
            dens_spinor_em{7}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jz_em(ii,jj,kk) + v_tensor(ii,jj,kk) * rho_em(ii,jj,kk) );
            dens_spinor_em{8}(ii,jj,kk) = 1/sqrt(2*ep_tensor(ii,jj,kk)) * (  jx_em(ii,jj,kk) - i * jy_em(ii,jj,kk) );
          end
        end
      end
    end
