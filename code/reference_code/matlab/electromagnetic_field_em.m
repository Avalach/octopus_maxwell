% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Ex, Ey, Ez, Bx, By, Bz] = electromagnetic_field_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, ep_tensor, mu_tensor, Phi_em)

    if (spinor_calc_mode == 1)

      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            Ex(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{1}(ii,jj,kk) );
            Ey(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{2}(ii,jj,kk) );
            Ez(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{3}(ii,jj,kk) );
            Bx(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{1}(ii,jj,kk) );
            By(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{2}(ii,jj,kk) );
            Bz(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{3}(ii,jj,kk) );
          end
        end
      end

    elseif (spinor_calc_mode == 2)

      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            Ex(ii,jj,kk) = real(       sqrt(2/ep_tensor(ii,jj,kk)) * (-Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            Ey(ii,jj,kk) = real( - i * sqrt(2/ep_tensor(ii,jj,kk)) * ( Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            Ez(ii,jj,kk) = real(       sqrt(2/ep_tensor(ii,jj,kk)) * ( Phi_em{2}(ii,jj,kk) + Phi_em{3}(ii,jj,kk))/2 );
            Bx(ii,jj,kk) = imag(       sqrt(2*mu_tensor(ii,jj,kk)) * (-Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            By(ii,jj,kk) = imag( - i * sqrt(2*mu_tensor(ii,jj,kk)) * ( Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            Bz(ii,jj,kk) = imag(       sqrt(2*mu_tensor(ii,jj,kk)) * ( Phi_em{2}(ii,jj,kk) + Phi_em{3}(ii,jj,kk))/2 );
          end
        end
      end

    elseif (spinor_calc_mode == 3)

      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            Ex(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{1}(ii,jj,kk) );
            Ey(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{2}(ii,jj,kk) );
            Ez(ii,jj,kk) = real( sqrt(2/ep_tensor(ii,jj,kk)) * Phi_em{3}(ii,jj,kk) );
            Bx(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{1}(ii,jj,kk) );
            By(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{2}(ii,jj,kk) );
            Bz(ii,jj,kk) = imag( sqrt(2*mu_tensor(ii,jj,kk)) * Phi_em{3}(ii,jj,kk) );
          end
        end
      end

    elseif (spinor_calc_mode == 4)

      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            Ex(ii,jj,kk) = real(       sqrt(2/ep_tensor(ii,jj,kk)) * (-Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            Ey(ii,jj,kk) = real( - i * sqrt(2/ep_tensor(ii,jj,kk)) * ( Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            Ez(ii,jj,kk) = real(       sqrt(2/ep_tensor(ii,jj,kk)) * ( Phi_em{2}(ii,jj,kk) + Phi_em{3}(ii,jj,kk))/2 );
            Bx(ii,jj,kk) = imag(       sqrt(2*mu_tensor(ii,jj,kk)) * (-Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            By(ii,jj,kk) = imag( - i * sqrt(2*mu_tensor(ii,jj,kk)) * ( Phi_em{1}(ii,jj,kk) + Phi_em{4}(ii,jj,kk))/2 );
            Bz(ii,jj,kk) = imag(       sqrt(2*mu_tensor(ii,jj,kk)) * ( Phi_em{2}(ii,jj,kk) + Phi_em{3}(ii,jj,kk))/2 );
          end
        end
      end

    end
