% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [jx_ext_em, jy_ext_em, jz_ext_em, rho_ext_em] = external_em_densities(calc_space_em, n_x_em, n_y_em, n_z_em, t_st, j_t_st_0_em, j_sigma_t_em, j_shape_em)

   jx_ext_em  = zeros(n_x_em, n_y_em, n_z_em);
   jy_ext_em  = zeros(n_x_em, n_y_em, n_z_em);
   jz_ext_em  = zeros(n_x_em, n_y_em, n_z_em);
   rho_ext_em = zeros(n_x_em, n_y_em, n_z_em);

   if (calc_space_em == 'x')
     if (j_shape_em == 1)
       if (t_st >= j_t_st_0_em)
         jz_ext_em(n_x_em/4,1,1) = 5.0;
       end
     elseif (j_shape_em == 2)
       jz_ext_em(n_x_em/4,1,1)  = - exp( -( (t_st-j_t_st_0_em)/j_sigma_t_em )^(2) );
     end
   elseif (calc_space_em == 'y')
   
   elseif (calc_space_em == 'z')
   
   end
