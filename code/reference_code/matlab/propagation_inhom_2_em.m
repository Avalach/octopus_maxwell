% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Phi_em] = propagation_inhom_em(calc_space_em, spinor_calc_mode, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_minus_em, ...
                                         exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, ...
                                         exp_time_ev_com_AB_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, jx_ext_old_em, jx_ext_new_em, jy_ext_old_em, jy_ext_new_em, ...
                                         jz_ext_old_em, jz_ext_new_em, rho_ext_old_em, rho_ext_new_em, jx_old_em, jx_new_em, jy_old_em, jy_new_em, jz_old_em, jz_new_em, ...
                                         rho_old_em, rho_new_em, Phi_em, ep_tensor, v_tensor, dt, m_e, q_e, h)

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % time evolution of the electromagnetic field without electromagnetic densities

 %   [Phi_em] = propagation_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, exp_time_ev_com_AB_em,  ...
 %                             map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, Phi_em);


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % time evolution of the electromagnetic densities

    if ( (spinor_calc_mode == 2) | (spinor_calc_mode == 4) )

      jx_tot_old_em  = jx_old_em;
      jx_tot_new_em  = jx_new_em;
      jy_tot_old_em  = jy_old_em;
      jy_tot_new_em  = jy_new_em;
      jz_tot_old_em  = jz_old_em;
      jz_tot_new_em  = jz_new_em;
      rho_tot_old_em = rho_old_em;
      rho_tot_new_em = rho_new_em;

 %     [Phi_em] = propagation_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, exp_time_ev_com_AB_em,  ...
 %                               map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, Phi_em);

      [dens_spinor_old_em] = density_spinor_em(spinor_calc_mode, jx_tot_old_em, jy_tot_old_em, jz_tot_old_em, rho_tot_old_em, ep_tensor, v_tensor, n_x_em, n_y_em, n_z_em);
      [dens_spinor_new_em] = density_spinor_em(spinor_calc_mode, jx_tot_new_em, jy_tot_new_em, jz_tot_new_em, rho_tot_new_em, ep_tensor, v_tensor, n_x_em, n_y_em, n_z_em);

 %     [dens_spinor_tilde_old_em] = propagation_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, ...
 %                                  exp_time_ev_com_AB_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, dens_spinor_old_em);

      [dens_spinor_tilde_new_em] = propagation_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, exp_time_ev_fft_A_minus_em, exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, ...
                                   exp_time_ev_com_AB_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, dens_spinor_new_em);

      if (spinor_calc_mode == 2)
        pp_limit = 4;
      elseif (spinor_calc_mode == 4)
        pp_limit = 8;
      end
      for pp=1:pp_limit
        dens_spinor_em{pp} = -1/2 * dt * ( dens_spinor_old_em{pp} + dens_spinor_tilde_new_em{pp} );
      end
 %     for pp=1:pp_limit
 %       dens_spinor_em{pp} = -1/2 * dt * ( dens_spinor_new_em{pp} + dens_spinor_tilde_old_em{pp} );
 %     end


      for pp=1:pp_limit
        Phi_em{pp} = Phi_em{pp} + dens_spinor_em{pp};
      end

    [Phi_em] = propagation_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, exp_time_ev_com_AB_em,  ...
                              map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, Phi_em);

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % complete time evolution of the electromagnetic field with electromagnetic densities

 %   if ( (spinor_calc_mode == 2) | (spinor_calc_mode == 4) )
 %     if (spinor_calc_mode == 2)
 %       pp_limit = 4;
 %     elseif (spinor_calc_mode == 4)
 %       pp_limit = 8;
 %     end
 %     for pp=1:pp_limit
 %       Phi_em{pp} = Phi_em{pp} + dens_spinor_em{pp};
 %     end
 %     for pp=1:pp_limit
 %       Phi_em{pp} = mask_em{pp}.*Phi_em{pp};
 %     end
 %   end

