% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = plot_em_field(calc_space_em, n_x_em, n_y_em, n_z_em, Ex, Ey, Ez, Bx, By, Bz, t_step, r_sp_mesh_em, contour_resolution, fontsize_em)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plotting signal in 1D

    if ( (calc_space_em == 'x') | (calc_space_em == 'y') | (calc_space_em == 'z') )

      if (calc_space_em == 'x')
        for ii=1:n_x_em;
          plot_axis = r_sp_mesh_em{1};
          Ex_plot(ii) = Ex(ii,1,1);
          Ey_plot(ii) = Ey(ii,1,1);
          Ez_plot(ii) = Ez(ii,1,1);
          Bx_plot(ii) = Bx(ii,1,1);
          By_plot(ii) = By(ii,1,1);
          Bz_plot(ii) = Bz(ii,1,1);
        end
      elseif (calc_space_em == 'y')
        for jj=1:n_y_em;
          plot_axis = r_sp_mesh_em{2};
          Ex_plot(jj) = Ex(1,jj,1);
          Ey_plot(jj) = Ey(1,jj,1);
          Ez_plot(jj) = Ez(1,jj,1);
          Bx_plot(jj) = Bx(1,jj,1);
          By_plot(jj) = By(1,jj,1);
          Bz_plot(jj) = Bz(1,jj,1);
        end
      elseif (calc_space_em == 'z')
        for kk=1:n_z_em;
          plot_axis = r_sp_mesh_em{3};
          Ex_plot(kk) = Ex(1,1,kk);
          Ey_plot(kk) = Ey(1,1,kk);
          Ez_plot(kk) = Ez(1,1,kk);
          Bx_plot(kk) = Bx(1,1,kk);
          By_plot(kk) = By(1,1,kk);
          Bz_plot(kk) = Bz(1,1,kk);
        end
      end

      fig1=figure(1);
      set(gcf,'Visible', 'off'); 
  
      plot(plot_axis,Ex_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.png');
      end
      print(fig1,'-dpng',filename);
   
      plot(plot_axis,Ey_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,Ez_plot);
      ylim([-0.01 0.01]);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,Bx_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,By_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,Bz_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.png');
      end
      print(fig1,'-dpng',filename);

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % plotting signal in 2D

    if ( (calc_space_em == 'xy') | (calc_space_em == 'xz') | (calc_space_em == 'yz') )

      if (calc_space_em == 'xy')
        for ii=1:n_x_em
          for jj=1:n_y_em
            Ex_plot(jj,ii) = Ex(ii,jj,1);
            Ey_plot(jj,ii) = Ey(ii,jj,1);
            Ez_plot(jj,ii) = Ez(ii,jj,1);
            Bx_plot(jj,ii) = Bx(ii,jj,1);
            By_plot(jj,ii) = By(ii,jj,1);
            Bz_plot(jj,ii) = Bz(ii,jj,1);
          end
        end
      elseif (calc_space_em == 'xz')
        for ii=1:n_x_em 
          for kk=1:n_z_em
            Ex_plot(kk,ii) = Ex(ii,1,kk);
            Ey_plot(kk,ii) = Ey(ii,1,kk);
            Ez_plot(kk,ii) = Ez(ii,1,kk);
            Bx_plot(kk,ii) = Bx(ii,1,kk);
            By_plot(kk,ii) = By(ii,1,kk);
            Bz_plot(kk,ii) = Bz(ii,1,kk);
          end
        end
      elseif (calc_space_em == 'yz')
        for jj=1:n_y_em
          for kk=1:n_z_em
            Ex_plot(kk,jj) = Ex(1,jj,kk);
            Ey_plot(kk,jj) = Ey(1,jj,kk);
            Ez_plot(kk,jj) = Ez(1,jj,kk);
            Bx_plot(kk,jj) = Bx(1,jj,kk);
            By_plot(kk,jj) = By(1,jj,kk);
            Bz_plot(kk,jj) = Bz(1,jj,kk);
          end
        end
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % contour plots    

      if (calc_space_em == 'xy')
        vec_1              = r_sp_mesh_em{1};
        vec_2              = r_sp_mesh_em{2};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'y [a.u]';
        vec_1_range        = [r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))];
        vec_2_range        = [r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))];
        file_name_pref_Ex  = './figures/2D_plots/contour/xy_propagation/E_field_x/E_field_x_t_';
        file_name_pref_Ey  = './figures/2D_plots/contour/xy_propagation/E_field_y/E_field_y_t_';
        file_name_pref_Ez  = './figures/2D_plots/contour/xy_propagation/E_field_z/E_field_z_t_';
        file_name_pref_Bx  = './figures/2D_plots/contour/xy_propagation/B_field_x/B_field_x_t_';
        file_name_pref_By  = './figures/2D_plots/contour/xy_propagation/B_field_y/B_field_y_t_';
        file_name_pref_Bz  = './figures/2D_plots/contour/xy_propagation/B_field_z/B_field_z_t_';
      elseif (calc_space_em == 'xz')
        vec_1              = r_sp_mesh_em{1};
        vec_2              = r_sp_mesh_em{3};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'z [a.u]';
        vec_1_range        = [r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))];
        vec_2_range        = [r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))];
        file_name_pref_Ex  = './figures/2D_plots/contour/xz_propagation/E_field_x/E_field_x_t_';
        file_name_pref_Ey  = './figures/2D_plots/contour/xz_propagation/E_field_y/E_field_y_t_';
        file_name_pref_Ez  = './figures/2D_plots/contour/xz_propagation/E_field_z/E_field_z_t_';
        file_name_pref_Bx  = './figures/2D_plots/contour/xz_propagation/B_field_x/B_field_x_t_';
        file_name_pref_By  = './figures/2D_plots/contour/xz_propagation/B_field_y/B_field_y_t_';
        file_name_pref_Bz  = './figures/2D_plots/contour/xz_propagation/B_field_z/B_field_z_t_';
      elseif (calc_space_em == 'yz')
        vec_1           = r_sp_mesh_em{2};
        vec_2           = r_sp_mesh_em{3};
        vec_1_label     = 'y [a.u]';
        vec_2_label     = 'z [a.u]';
        vec_1_range     = [r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))];
        vec_2_range     = [r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))]; 
        file_name_pref_Ex  = './figures/2D_plots/contour/yz_propagation/E_field_x/E_field_x_t_';
        file_name_pref_Ey  = './figures/2D_plots/contour/yz_propagation/E_field_y/E_field_y_t_';
        file_name_pref_Ez  = './figures/2D_plots/contour/yz_propagation/E_field_z/E_field_z_t_';
        file_name_pref_Bx  = './figures/2D_plots/contour/yz_propagation/B_field_x/B_field_x_t_';
        file_name_pref_By  = './figures/2D_plots/contour/yz_propagation/B_field_y/B_field_y_t_';
        file_name_pref_Bz  = './figures/2D_plots/contour/yz_propagation/B_field_z/B_field_z_t_';
      end
      time_str = num2str(0,'%04d');
      plot_matrix_Ex  = Ez_plot;
      plot_matrix_Ey  = Ey_plot;
      plot_matrix_Ez  = Ez_plot;
      plot_matrix_Bx  = Bx_plot;
      plot_matrix_By  = By_plot;
      plot_matrix_Bz  = Bz_plot;
      title_E         = 'Electric field';
      title_B         = 'Magnetic field';
      filename_Ex     = strcat(file_name_pref_Ex,time_str);
      filename_Ey     = strcat(file_name_pref_Ey,time_str);
      filename_Ez     = strcat(file_name_pref_Ez,time_str);
      filename_Bx     = strcat(file_name_pref_Bx,time_str);
      filename_By     = strcat(file_name_pref_By,time_str);
      filename_Bz     = strcat(file_name_pref_Bz,time_str);

      plot_contour(vec_1, vec_2, plot_matrix_Ex, contour_resolution, filename_Ex, title_E, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_Ey, contour_resolution, filename_Ey, title_E, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_Ez, contour_resolution, filename_Ez, title_E, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_Bx, contour_resolution, filename_Bx, title_B, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_By, contour_resolution, filename_By, title_B, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_Bz, contour_resolution, filename_Bz, title_B, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
 
    end

