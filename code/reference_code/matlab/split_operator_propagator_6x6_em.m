% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [exp_time_ev_fft_A, exp_time_ev_B, exp_time_ev_com_AB, exp_time_ev_fft_A_matrix, map_qq, map_ii, map_jj, map_kk] ...
         = split_operator_propagator_6x6_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, dt, v_tensor, h_tensor, ... 
           exp_approx_switch_em, exp_approx_order_em, h)

    fprintf('Initializing arrays for time evolution ...\n\n');

    for pp=1:6
      for qq=1:6
        exp_time_ev_B{pp}{qq}      = zeros(n_x_em,n_y_em,n_z_em);
        exp_time_ev_com_AB{pp}{qq} = zeros(n_x_em,n_y_em,n_z_em);
      end
    end


    [div_v_tensor, div_h_tensor, del_x_v_tensor, del_2_x_v_tensor, del_y_v_tensor, del_2_y_v_tensor, del_z_v_tensor, del_2_z_v_tensor, ...
                                 del_x_h_tensor, del_2_x_h_tensor, del_y_h_tensor, del_2_y_h_tensor, del_z_h_tensor, del_2_z_h_tensor] ...
        = derivatives_of_v_h_tensors(n_x_em, n_y_em, n_z_em, v_tensor, h_tensor, m_sp_mesh_em, h);
  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % lookup table for using convolution of v and p for the partial matrix A of the total EM Hamiltonian

    fprintf('Setting lookup table of convolved Fourier transformed v and momentum space p for the time evolution exponential matrix A ...\n\n');

    fprintf('Reading file "convolution_matrix_6x6.txt" ...\n\n');

    if (exist('./input/convolution_matrix_6x6.txt') == 2)
      exp_time_ev_fft_A_matrix = load('./input/convolution_matrix_6x6.txt');
      if ( length(exp_time_ev_fft_A_matrix) == 6*n_x_em*n_y_em*n_z_em )  
        conv_A_matrix_exp_check = 1;      
        fprintf('Found file "convolution_matrix_6x6.txt" and matrix length agrees with number of grid points ...\n\n');
      else
        conv_A_matrix_exp_check = 0;
        exp_time_ev_fft_A_matrix = zeros(6*n_x_em*n_y_em*n_z_em,6*n_x_em*n_y_em*n_z_em);
        fprintf('Found file "convolution_matrix_6x6.txt" but matrix length does not agree with number of grid points ...\n\n');
      end
    else 
      conv_A_matrix_exp_check = 0;
      exp_time_ev_fft_A_matrix = zeros(6*n_x_em*n_y_em*n_z_em,6*n_x_em*n_y_em*n_z_em);
      fprintf('File "convolution_matrix_6x6.txt" does not exisct ...\n\n');
    end

    if (conv_A_matrix_exp_check ~= 1)

      fprintf('Calculating auxiliary convolution array ...\n\n');
  
      idx_1 = 0;
      for idx_ii=1:2*n_x_em-1
        for idx_jj=1:2*n_y_em-1
          for idx_kk=1:2*n_z_em-1
            idx_1 = idx_1+1;
            map_diff(idx_ii,idx_jj,idx_kk) = idx_1;
            idx_2 = 0;
            for ii=1:n_x_em
              for jj=1:n_y_em 
                for kk=1:n_z_em
                  idx_2 = idx_2 + 1;
                  conv_exp_matrix(idx_1,idx_2) = 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em)           ...
                                               * exp(-i*2*pi*(ii-1)*(idx_ii-n_x_em)/n_x_em)                 ... 
                                               * exp(-i*2*pi*(jj-1)*(idx_jj-n_y_em)/n_y_em)                 ... 
                                               * exp(-i*2*pi*(kk-1)*(idx_kk-n_z_em)/n_z_em) ;
                end
              end
            end  
          end
        end
      end
 
      idx_2 = 0;
      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            idx_2=idx_2+1;
            v_vector(idx_2,1) = v_tensor(ii,jj,kk) ; 
          end
        end
      end

      conv_aux_res_vector = conv_exp_matrix * v_vector;
 
      idx_1 = 0;
      for ii=1:n_x_em
         for jj=1:n_y_em
          for kk=1:n_z_em
            idx_2 = 0;
            idx_1 = idx_1+1;
            for ii_kk=1:n_x_em
              diff_ii = (ii-1)-(ii_kk-1);
              idx_ii = n_x_em+diff_ii;
              for jj_kk=1:n_y_em
                diff_jj = (jj-1)-(jj_kk-1);
                idx_jj = n_y_em+diff_jj;
                for kk_kk=1:n_z_em
                  idx_2 = idx_2 + 1;
                  diff_kk = (kk-1)-(kk_kk-1);
                  idx_kk = n_z_em+diff_kk;
                  conv_matrix{1}(idx_1,idx_2) = conv_aux_res_vector(map_diff(idx_ii,idx_jj,idx_kk,1)) * m_sp_mesh_em{1}(ii_kk);
                  conv_matrix{2}(idx_1,idx_2) = conv_aux_res_vector(map_diff(idx_ii,idx_jj,idx_kk,1)) * m_sp_mesh_em{2}(jj_kk);
                  conv_matrix{3}(idx_1,idx_2) = conv_aux_res_vector(map_diff(idx_ii,idx_jj,idx_kk,1)) * m_sp_mesh_em{3}(kk_kk);
                end
              end
            end
          end
        end
      end
    
      fprintf('Calculating convolution ...\n\n');
    
      idx_1 = 0;
      for qq=1:6
        for ii=1:n_x_em
          for jj=1:n_y_em
            for kk=1:n_z_em
              idx_2 = 0;
              idx_1 = idx_1+1;
              map_qq(idx_1) = qq;
              map_ii(idx_1) = ii;
              map_jj(idx_1) = jj;
              map_kk(idx_1) = kk;
            end
          end
        end
      end
  
      matrix_6x6{1} = zeros(6,6);
      matrix_6x6{2} = zeros(6,6);
      matrix_6x6{3} = zeros(6,6);

      matrix_6x6{1}(2,3) = -i;
      matrix_6x6{1}(3,2) =  i;
      matrix_6x6{1}(5,6) =  i;
      matrix_6x6{1}(6,5) = -i;

      matrix_6x6{2}(1,3) =  i;
      matrix_6x6{2}(3,1) = -i;
      matrix_6x6{2}(4,6) = -i;
      matrix_6x6{2}(6,4) =  i;
    
      matrix_6x6{3}(1,2) = -i;
      matrix_6x6{3}(2,1) =  i;
      matrix_6x6{3}(4,5) =  i;
      matrix_6x6{3}(5,4) = -i;
   
      conv_A_matrix = kron(matrix_6x6{1},conv_matrix{1}) + kron(matrix_6x6{2},conv_matrix{2}) +  kron(matrix_6x6{3},conv_matrix{3});
    
      if exp_approx_switch_em == 0
        exp_time_ev_fft_A_matrix = sqrt(n_x_em)*sqrt(n_y_em)*sqrt(n_z_em) * expm(-i/h * dt * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) * conv_A_matrix);
      elseif exp_approx_switch_em == 1
        for nn=1:exp_approx_order_em
          if (nn == 1)
            exp_time_ev_fft_A_matrix_list{1}  = conv_A_matrix ;
          else
            exp_time_ev_fft_A_matrix_list{nn} = exp_time_ev_fft_A_matrix_list{nn-1} * conv_A_matrix * 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) ;
          end
        end
        exp_time_ev_fft_A_matrix = sqrt(n_x_em)*sqrt(n_y_em)*sqrt(n_z_em)* eye(6*n_x_em*n_y_em*n_z_em,6*n_x_em*n_y_em*n_z_em);
        for nn=1:exp_approx_order_em
          factor = 1/factorial(nn);
          exp_time_ev_fft_A_matrix = exp_time_ev_fft_A_matrix + factor * (-i/h*dt)^nn * exp_time_ev_fft_A_matrix_list{nn};
        end
      end

 
   
      save './input/convolution_matrix_6x6.txt' exp_time_ev_fft_A_matrix -ascii;
  
    else
  
      idx_1 = 0;
      for qq=1:6
        for ii=1:n_x_em
          for jj=1:n_y_em
            for kk=1:n_z_em
              idx_2 = 0;
              idx_1 = idx_1+1;
              map_qq(idx_1) = qq;
              map_ii(idx_1) = ii;
              map_jj(idx_1) = jj;
              map_kk(idx_1) = kk;
            end
          end
        end
      end
  
    end
    
    for idx_1=1:6*n_x_em*n_y_em*n_z_em
      for idx_2=1:6*n_x_em*n_y_em*n_z_em
        exp_time_ev_fft_A{map_qq(idx_1)}{map_qq(idx_2)}{map_ii(idx_1)}{map_jj(idx_1)}{map_kk(idx_1)}(map_ii(idx_2),map_jj(idx_2),map_kk(idx_2)) = exp_time_ev_fft_A_matrix(idx_1,idx_2);
      end
    end
  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % lookup table for the partial B of the total EM Hamiltonian

    fprintf('Setting lookup table for the time evolution exponential matrix corresponding to matrix B  ...\n\n');

    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em

          h_factor = v_tensor(ii,jj,kk)/h_tensor(ii,jj,kk);
          matrix = zeros(6,6);

          matrix(1,2) = -h * 1/2 * del_z_v_tensor(ii,jj,kk);
          matrix(1,3) =  h * 1/2 * del_y_v_tensor(ii,jj,kk);
          matrix(1,5) = -h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
          matrix(1,6) =  h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;

          matrix(2,1) =  h * 1/2 * del_z_v_tensor(ii,jj,kk);
          matrix(2,3) = -h * 1/2 * del_x_v_tensor(ii,jj,kk);
          matrix(2,4) =  h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
          matrix(2,6) = -h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;

          matrix(3,1) = -h * 1/2 * del_y_v_tensor(ii,jj,kk);
          matrix(3,2) =  h * 1/2 * del_x_v_tensor(ii,jj,kk);
          matrix(3,4) = -h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;
          matrix(3,5) =  h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;
 
          matrix(4,2) =  h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
          matrix(4,3) = -h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;
          matrix(4,5) =  h * 1/2 * del_z_v_tensor(ii,jj,kk);
          matrix(4,6) = -h * 1/2 * del_y_v_tensor(ii,jj,kk);
 
          matrix(5,1) = -h * 1/2 * del_z_h_tensor(ii,jj,kk) * h_factor;
          matrix(5,3) =  h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;
          matrix(5,4) = -h * 1/2 * del_z_v_tensor(ii,jj,kk);
          matrix(5,6) =  h * 1/2 * del_x_v_tensor(ii,jj,kk);

          matrix(6,1) =  h * 1/2 * del_y_h_tensor(ii,jj,kk) * h_factor;
          matrix(6,2) = -h * 1/2 * del_x_h_tensor(ii,jj,kk) * h_factor;
          matrix(6,4) =  h * 1/2 * del_y_v_tensor(ii,jj,kk);
          matrix(6,5) = -h * 1/2 * del_x_v_tensor(ii,jj,kk);
   
          exp_matrix = expm( -i/h * dt * matrix);

          for pp=1:6
            for qq=1:6
              exp_time_ev_B{pp}{qq}(ii,jj,kk) = exp_matrix(pp,qq); 
            end 
          end

        end
      end
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % lookup table for Hamiltonian commutator of A and B

    fprintf('Setting lookup table for the time evolution exponential matrix corresponding to matrix [A,B]  ...\n\n');
  
    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em

          matrix = zeros(6,6);

          matrix(1,2) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
          matrix(1,3) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
          matrix(2,1) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
          matrix(2,3) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;
          matrix(3,1) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
          matrix(3,2) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;
          matrix(4,5) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
          matrix(4,6) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
          matrix(5,4) = 1/2 * h^2 * ( (del_z_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_z_v_tensor(ii,jj,kk) ) ;
          matrix(5,6) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;
          matrix(6,4) = 1/2 * h^2 * ( (del_y_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_y_v_tensor(ii,jj,kk) ) ;
          matrix(6,5) = 1/2 * h^2 * ( (del_x_v_tensor(ii,jj,kk))^2/v_tensor(ii,jj,kk) - del_2_x_v_tensor(ii,jj,kk) ) ;

          exp_matrix = expm( 1/(2*(h^2)) * dt^2 * matrix );

          for pp=1:6
            for qq=1:6
              exp_time_ev_com_AB{pp}{qq}(ii,jj,kk) = exp_matrix(pp,qq);
            end
          end

        end
      end
    end

