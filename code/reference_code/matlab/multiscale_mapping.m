% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [idx_at_of_em, idx_em_of_at, delta_at_of_em, delta_em_of_at, step_at_of_em, step_em_of_at, number_at_of_em, number_em_of_at] = ...
          multiscale_mapping(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, r_sp_mesh_em, r_sp_mesh_at, dx_em, dy_em, dz_em, dx_at, dy_at, dz_at)

    number_at_of_em = zeros(n_x_at,n_y_at,n_z_at);
    number_em_of_at = zeros(n_x_em,n_y_em,n_z_em);

    box_diag_em = sqrt( (r_sp_mesh_em{1}(1)-r_sp_mesh_em{1}(n_x_em))^2 + (r_sp_mesh_em{2}(1)-r_sp_mesh_em{2}(n_y_em))^2 + (r_sp_mesh_em{3}(1)-r_sp_mesh_em{3}(n_z_em))^2 );
    box_diag_at = sqrt( (r_sp_mesh_at{1}(1)-r_sp_mesh_at{1}(n_x_at))^2 + (r_sp_mesh_at{2}(1)-r_sp_mesh_at{2}(n_y_at))^2 + (r_sp_mesh_at{3}(1)-r_sp_mesh_at{3}(n_z_at))^2 );

    box_diag = max(box_diag_em,box_diag_at);

    if ( (box_diag_em == box_diag_at) & (n_x_at == n_x_em) & (n_y_at == n_y_em) & (n_z_at == n_z_em) )
      for ii_em=1:n_x_em
        for jj_em=1:n_y_em
          for kk_em=1:n_z_em
            idx_at_of_em{1}(ii_em,jj_em,kk_em)   = ii_em;
            idx_at_of_em{2}(ii_em,jj_em,kk_em)   = jj_em;
            idx_at_of_em{3}(ii_em,jj_em,kk_em)   = kk_em;
            delta_at_of_em{1}(ii_em,jj_em,kk_em) = 0;
            delta_at_of_em{2}(ii_em,jj_em,kk_em) = 0;
            delta_at_of_em{3}(ii_em,jj_em,kk_em) = 0;
            step_at_of_em{1}(ii_em,jj_em,kk_em)  = 0;
            step_at_of_em{2}(ii_em,jj_em,kk_em)  = 0;
            step_at_of_em{3}(ii_em,jj_em,kk_em)  = 0;
          end
        end
      end
      for ii_at=1:n_x_em
        for jj_at=1:n_y_em
          for kk_at=1:n_z_em
            idx_em_of_at{1}(ii_at,jj_at,kk_at)   = ii_at;
            idx_em_of_at{2}(ii_at,jj_at,kk_at)   = jj_at;
            idx_em_of_at{3}(ii_at,jj_at,kk_at)   = kk_at;
            delta_em_of_at{1}(ii_at,jj_at,kk_at) = 0;
            delta_em_of_at{2}(ii_at,jj_at,kk_at) = 0;
            delta_em_of_at{3}(ii_at,jj_at,kk_at) = 0;
            step_em_of_at{1}(ii_at,jj_at,kk_at)  = 0;
            step_em_of_at{2}(ii_at,jj_at,kk_at)  = 0;
            step_em_of_at{3}(ii_at,jj_at,kk_at)  = 0;
          end
        end
      end
    else
      for ii_em=1:n_x_em 
        for jj_em=1:n_y_em
          for kk_em=1:n_z_em
            value_ii_em = r_sp_mesh_em{1}(ii_em);
            value_jj_em = r_sp_mesh_em{2}(jj_em);
            value_kk_em = r_sp_mesh_em{3}(kk_em);
            dist_min = box_diag;
            for ii_at=1:n_x_at
              for jj_at=1:n_y_at
                for kk_at=1:n_z_at
                  value_ii_at = r_sp_mesh_at{1}(ii_at);
                  value_jj_at = r_sp_mesh_at{2}(jj_at);
                  value_kk_at = r_sp_mesh_at{3}(kk_at);
                  dist = sqrt( (value_ii_em-value_ii_at)^2 + (value_jj_em-value_jj_at)^2 + (value_kk_em-value_kk_at)^2 );
                  if (dist < dist_min)
                    dist_min = dist;
                    idx_at_of_em{1}(ii_em,jj_em,kk_em)   = ii_at;
                    idx_at_of_em{2}(ii_em,jj_em,kk_em)   = jj_at; 
                    idx_at_of_em{3}(ii_em,jj_em,kk_em)   = kk_at;
                    delta_at_of_em{1}(ii_em,jj_em,kk_em) = value_ii_em-value_ii_at;
                    delta_at_of_em{2}(ii_em,jj_em,kk_em) = value_jj_em-value_jj_at;
                    delta_at_of_em{3}(ii_em,jj_em,kk_em) = value_kk_em-value_kk_at;
                    if ( (idx_at_of_em{1}(ii_em,jj_em,kk_em) == 1) | (idx_at_of_em{1}(ii_em,jj_em,kk_em) == n_x_em) )
                      step_at_of_em{1}(ii_em,jj_em,kk_em) = 0;
                    else
                      if ( (value_ii_em-value_ii_at) > 0 )
                        step_at_of_em{1}(ii_em,jj_em,kk_em) =  1;
                      elseif ( (value_ii_em-value_ii_at) < 0 )
                        step_at_of_em{1}(ii_em,jj_em,kk_em) = -1;
                      end
                    end
                    if ( (idx_at_of_em{2}(ii_em,jj_em,kk_em) == 1) | (idx_at_of_em{2}(ii_em,jj_em,kk_em) == n_y_em) )
                      step_at_of_em{2}(ii_em,jj_em,kk_em) = 0;
                    else
                      if ( (value_jj_em-value_jj_at) > 0 )
                        step_at_of_em{2}(ii_em,jj_em,kk_em) =  1;
                      elseif ( (value_jj_em-value_jj_at) < 0)
                        step_at_of_em{2}(ii_em,jj_em,kk_em) = -1;
                      end
                    end
                    if ( (idx_at_of_em{3}(ii_em,jj_em,kk_em) == 1) | (idx_at_of_em{3}(ii_em,jj_em,kk_em) == n_z_em) )
                      step_at_of_em{3}(ii_em,jj_em,kk_em) = 0;
                    else
                      if ( (value_kk_em-value_kk_at) > 0 )
                        step_at_of_em{3}(ii_em,jj_em,kk_em) =  1;
                      elseif ( (value_kk_em-value_kk_at) < 0)
                        step_at_of_em{3}(ii_em,jj_em,kk_em) = -1;
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
      for ii_at=1:n_x_at
        for jj_at=1:n_y_at
          for kk_at=1:n_z_at
            value_ii_at = r_sp_mesh_at{1}(ii_at);
            value_jj_at = r_sp_mesh_at{2}(jj_at);
            value_kk_at = r_sp_mesh_at{3}(kk_at);
            dist_min = box_diag;
            for ii_em=1:n_x_em
              for jj_em=1:n_y_em
                for kk_em=1:n_z_em
                  value_ii_em = r_sp_mesh_em{1}(ii_em);
                  value_jj_em = r_sp_mesh_em{2}(jj_em);
                  value_kk_em = r_sp_mesh_em{3}(kk_em);
                  dist = sqrt( (value_ii_em-value_ii_at)^2 + (value_jj_em-value_jj_at)^2 + (value_kk_em-value_kk_at)^2 );
                  if (dist < dist_min)
                    dist_min = dist;
                    idx_em_of_at{1}(ii_at,jj_at,kk_at)   = ii_em;
                    idx_em_of_at{2}(ii_at,jj_at,kk_at)   = jj_em;
                    idx_em_of_at{3}(ii_at,jj_at,kk_at)   = kk_em;
                    delta_em_of_at{1}(ii_at,jj_at,kk_at) = value_ii_at-value_ii_em;
                    delta_em_of_at{2}(ii_at,jj_at,kk_at) = value_jj_at-value_jj_em;
                    delta_em_of_at{3}(ii_at,jj_at,kk_at) = value_kk_at-value_kk_em;
                    if ( (idx_em_of_at{1}(ii_at,jj_at,kk_at) == 1) | (idx_em_of_at{1}(ii_at,jj_at,kk_at) == n_x_at) )
                      step_em_of_at{1}(ii_at,jj_at,kk_at) = 0;
                    else
                      if ( (value_ii_at-value_ii_em) > 0)
                        step_em_of_at{1}(ii_at,jj_at,kk_at) =  1;
                      elseif ( (value_ii_at-value_ii_em) < 0)
                        step_em_of_at{1}(ii_at,jj_at,kk_at) = -1;
                      end
                    end
                    if ( (idx_em_of_at{2}(ii_at,jj_at,kk_at) == 1) | (idx_em_of_at{2}(ii_at,jj_at,kk_at) == n_y_at) )
                      step_em_of_at{2}(ii_at,jj_at,kk_at) = 0; 
                    else
                      if ( (value_jj_at-value_jj_em) > 0)
                        step_em_of_at{2}(ii_at,jj_at,kk_at) =  1;
                      elseif ( (value_jj_at-value_jj_em) < 0)
                        step_em_of_at{2}(ii_at,jj_at,kk_at) = -1;
                      end
                    end
                    if ( (idx_em_of_at{3}(ii_at,jj_at,kk_at) == 1) | (idx_em_of_at{3}(ii_at,jj_at,kk_at) == n_z_at) )
                      step_em_of_at{3}(ii_at,jj_at,kk_at) = 0;
                    else
                      if ( (value_kk_at-value_kk_em) > 0)
                        step_em_of_at{3}(ii_at,jj_at,kk_at) =  1;
                      elseif ( (value_kk_at-value_kk_em) < 0)
                        step_em_of_at{3}(ii_at,jj_at,kk_at) = -1;
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
      for ii_at=1:n_x_at
        for jj_at=1:n_y_at
          for kk_at=1:n_z_at
            number_em_of_at(idx_em_of_at{1}(ii_at,jj_at,kk_at), idx_em_of_at{2}(ii_at,jj_at,kk_at), idx_em_of_at{3}(ii_at,jj_at,kk_at)) = ...
              number_em_of_at(idx_em_of_at{1}(ii_at,jj_at,kk_at), idx_em_of_at{2}(ii_at,jj_at,kk_at), idx_em_of_at{3}(ii_at,jj_at,kk_at)) + 1;
          end
        end
      end
      for ii_em=1:n_x_em
        for jj_em=1:n_y_em
          for kk_em=1:n_z_em
            number_at_of_em(idx_at_of_em{1}(ii_em,jj_em,kk_em), idx_at_of_em{2}(ii_em,jj_em,kk_em), idx_at_of_em{3}(ii_em,jj_em,kk_em)) = ... 
              number_at_of_em(idx_at_of_em{1}(ii_em,jj_em,kk_em), idx_at_of_em{2}(ii_em,jj_em,kk_em), idx_at_of_em{3}(ii_em,jj_em,kk_em)) + 1;
          end
        end
      end
    end

