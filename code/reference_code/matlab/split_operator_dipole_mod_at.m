% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [exp_time_ev_B_at] = split_operator_dipole_mod_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, Z_at, pot_const_at, Ex_old_at, Ex_new_at, ...
                                                        Ey_old_at, Ey_new_at, Ez_old_at, Ez_new_at, dt, q_e, h);

    for pp=1:n_at
      r_sp_atom_mesh_at{1}(pp) = atom_pos{pp}(1);
      r_sp_atom_mesh_at{2}(pp) = atom_pos{pp}(2);
      r_sp_atom_mesh_at{3}(pp) = atom_pos{pp}(3);
    end

    for ii=1:n_x_at
      for jj=1:n_y_at
        for kk=1:n_z_at

          rr_square = (r_sp_mesh_at{1}(ii)-r_sp_atom_mesh_at{1}(1))^2 + (r_sp_mesh_at{2}(jj)-r_sp_atom_mesh_at{2}(1))^2 + (r_sp_mesh_at{3}(kk)-r_sp_atom_mesh_at{3}(1))^2;
          H_pot_at  = Z_at{1}*(q_e^2) / sqrt( rr_square + pot_const_at^2 );

          em_at_coupling = q_e/2 ... 
                         * ( r_sp_mesh_at{1}(ii) * (Ex_old_at(ii,jj,kk) + Ex_new_at(ii,jj,kk)) ...
                           + r_sp_mesh_at{2}(jj) * (Ey_old_at(ii,jj,kk) + Ey_new_at(ii,jj,kk)) ...
                           + r_sp_mesh_at{3}(kk) * (Ez_old_at(ii,jj,kk) + Ez_new_at(ii,jj,kk)) );

          exp_time_ev_B_at(ii,jj,kk) = exp( i * dt/h * (H_pot_at+em_at_coupling) );

        end
      end
    end
          
