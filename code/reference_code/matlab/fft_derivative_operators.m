% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [fft_div_op, fft_del_x_op, fft_del_2_x_op, fft_del_y_op, fft_del_2_y_op, fft_del_z_op, fft_del_2_z_op] = fft_derivative_operators(n_x, n_y, n_z, m_sp_mesh, h)

     for ii=1:n_x
       for jj=1:n_y
         for kk=1:n_z
           fft_div_op(ii,jj,kk) = i/h * ( m_sp_mesh{1}(ii) + m_sp_mesh{2}(jj) + m_sp_mesh{3}(kk) );
           fft_del_z_op(kk)   =  i/h * m_sp_mesh{3}(kk);
           fft_del_2_z_op(kk) = -1/(h^2) * (m_sp_mesh{3}(kk))^2;
         end
         fft_del_y_op(jj)   =  i/h * m_sp_mesh{2}(jj);
         fft_del_2_y_op(jj) = -1/(h^2) * (m_sp_mesh{2}(jj))^2;
       end
       fft_del_x_op(ii)   =  i/h * m_sp_mesh{1}(ii);
       fft_del_2_x_op(ii) = -1/(h^2) * (m_sp_mesh{1}(ii))^2;
     end

