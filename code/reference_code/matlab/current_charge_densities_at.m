% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [jx_at, jy_at, jz_at, rho_at] = current_charge_densities_at(calc_space_at, Phi_at, n_x_at, n_y_at, n_z_at, m_sp_mesh_at, m_e, q_e, h)

    [fft_div_op_at, fft_del_x_op_at, fft_del_2_x_op_at, fft_del_y_op_at, fft_del_2_y_op_at, fft_del_z_op_at, fft_del_2_z_op_at] ...
          = fft_derivative_operators(n_x_at, n_y_at, n_z_at, m_sp_mesh_at, h);

    fft_Phi_at = fftn(Phi_at);
    fft_conj_Phi_at = fftn(conj(Phi_at));

    for ii=1:n_x_at
      for jj=1:n_y_at
        for kk=1:n_z_at
          fft_del_x_Phi_at(ii,jj,kk)      = fft_del_x_op_at(ii) * fft_Phi_at(ii,jj,kk);
          fft_del_y_Phi_at(ii,jj,kk)      = fft_del_y_op_at(jj) * fft_Phi_at(ii,jj,kk);
          fft_del_z_Phi_at(ii,jj,kk)      = fft_del_z_op_at(kk) * fft_Phi_at(ii,jj,kk);
          fft_del_x_conj_Phi_at(ii,jj,kk) = fft_del_x_op_at(ii) * fft_conj_Phi_at(ii,jj,kk);
          fft_del_y_conj_Phi_at(ii,jj,kk) = fft_del_y_op_at(jj) * fft_conj_Phi_at(ii,jj,kk);
          fft_del_z_conj_Phi_at(ii,jj,kk) = fft_del_z_op_at(kk) * fft_conj_Phi_at(ii,jj,kk);
        end
      end
    end

    del_x_Phi_at      = ifftn(fft_del_x_Phi_at);
    del_y_Phi_at      = ifftn(fft_del_y_Phi_at);
    del_z_Phi_at      = ifftn(fft_del_z_Phi_at);
    del_x_conj_Phi_at = ifftn(fft_del_x_conj_Phi_at);
    del_y_conj_Phi_at = ifftn(fft_del_y_conj_Phi_at);
    del_z_conj_Phi_at = ifftn(fft_del_z_conj_Phi_at);

    rho_at = q_e * (conj(Phi_at).*Phi_at);

    jx_at = real( q_e*h/(i*2*m_e) * ( conj(Phi_at) .* del_x_Phi_at - Phi_at .* del_x_conj_Phi_at ) );

    jy_at = real( q_e*h/(i*2*m_e) * ( conj(Phi_at) .* del_y_Phi_at - Phi_at .* del_y_conj_Phi_at ) );
 
    jz_at = real( q_e*h/(i*2*m_e) * ( conj(Phi_at) .* del_z_Phi_at - Phi_at .* del_z_conj_Phi_at ) );


