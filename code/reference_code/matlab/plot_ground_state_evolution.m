% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_ground_state_evolution(calc_space_at, n_x_at, n_y_at, n_z_at, Phi_at, t_step, r_sp_mesh_at, contour_resolution_at, fontsize_at)

    if ( (calc_space_at == 'x') | (calc_space_at == 'y') | (calc_space_at == 'z') )

      if (calc_space_at == 'x')
        for ii=1:n_x_at
          plot_Phi_at(ii) = Phi_at(ii,1,1);
        end
        filename = strcat('./figures/1D_plots/graph/x_propagation/gs_wave_function_ev/wave_function_t_',num2str(t_step,'%04d'),'.png');
        plot_axis = r_sp_mesh_at{1};
      elseif (calc_space_at == 'y')
        for jj=1:n_y_at
          plot_Phi_at(jj) = Phi_at(1,jj,1);
        end
        filename = strcat('./figures/1D_plots/graph/y_propagation/gs_wave_function_ev/wave_function_t_',num2str(t_step,'%04d'),'.png');
        plot_axis = r_sp_mesh_at{2};
      elseif (calc_space_at == 'z')
        for kk=1:n_z_at
          plot_Phi_at(kk) = Phi_at(1,1,kk);
        end
        filename = strcat('./figures/1D_plots/graph/z_propagation/gs_wave_function_ev/wave_function_t_',num2str(t_step,'%04d'),'.png');
        plot_axis = r_sp_mesh_at{3};
      end
      fig1=figure(1);
      set(gcf,'Visible', 'off'); 
      plot(plot_axis, plot_Phi_at);
      print(fig1,'-dpng',filename);

    end

    if ( (calc_space_at == 'xy') | (calc_space_at == 'xz') | (calc_space_at == 'yz') )

      if (calc_space_em == 'xy')
        for ii=1:n_x_em
          for jj=1:n_y_em
            Phi_at_plot(jj,ii) = Phi_at(ii,jj,1);
          end
        end
      elseif (calc_space_em == 'xz')
        for ii=1:n_x_em 
          for kk=1:n_z_em
            Phi_at_plot(kk,ii) = Phi_at(ii,1,kk);
          end
        end
      elseif (calc_space_em == 'yz')
        for jj=1:n_y_em
          for kk=1:n_z_em
            Phi_at_plot(kk,jj) = Phi_at(1,jj,kk);
          end
        end
      end
      if (calc_space_em == 'xy')
        vec_1              = r_sp_mesh_at{1};
        vec_2              = r_sp_mesh_at{2};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'y [a.u]';
        vec_1_range        = [r_sp_mesh_at{1}(1) r_sp_mesh_at{1}(length(r_sp_mesh_at{1}))];
        vec_2_range        = [r_sp_mesh_at{2}(1) r_sp_mesh_at{2}(length(r_sp_mesh_at{2}))];
        file_name_pref     = './figures/2D_plots/contour/xy_propagation/gs_wave_function_ev/wave_function_t_';
      elseif (calc_space_em == 'xz')
        vec_1              = r_sp_mesh_at{1};
        vec_2              = r_sp_mesh_at{3};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'z [a.u]';
        vec_1_range        = [r_sp_mesh_at{1}(1) r_sp_mesh_at{1}(length(r_sp_mesh_at{1}))];
        vec_2_range        = [r_sp_mesh_at{3}(1) r_sp_mesh_at{3}(length(r_sp_mesh_at{3}))];
        file_name_pref     = './figures/2D_plots/contour/xz_propagation/gs_wave_function_ev/wave_function_t_';
      elseif (calc_space_em == 'yz')
        vec_1              = r_sp_mesh_at{2};
        vec_2              = r_sp_mesh_at{3};
        vec_1_label        = 'y [a.u]';
        vec_2_label        = 'z [a.u]';
        vec_1_range        = [r_sp_mesh_at{2}(1) r_sp_mesh_at{2}(length(r_sp_mesh_at{2}))];
        vec_2_range        = [r_sp_mesh_at{3}(1) r_sp_mesh_at{3}(length(r_sp_mesh_at{3}))]; 
        file_name_pref     = './figures/2D_plots/contour/yz_propagation/gs_wave_function_ev/wave_function_t_';
      end
      time_str = num2str(0,'%04d');
      title           = 'wave_function';
      filename        = strcat(file_name_pref,time_str);

      plot_contour(vec_1, vec_2, Phi_at_plot, contour_resolution_at, filename, title, vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_at);

    end
