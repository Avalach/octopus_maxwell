% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ep_tensor, mu_tensor, v_tensor, h_tensor] = ...
          classical_medium(calc_space_em, spinor_calc_mode, load_medium_image_switch_em, medium_shape_em, ...
          ep_0, ep_m, mu_0, mu_m, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, plot_output_switch_em)

    if ( (spinor_calc_mode == 1) | (spinor_calc_mode == 2) )

      for ii=1:n_x_em
        for jj=1:n_x_em
          for kk=1:n_y_em
            mu_tensor(ii,jj,kk) = mu_0;
            ep_tensor(ii,jj,kk) = ep_0;
            v_tensor(ii,jj,kk)  = 1/sqrt(mu_0*ep_0);
            h_tensor(ii,jj,kk)  = sqrt(mu_0/ep_0);
          end
        end
      end

    elseif ( (spinor_calc_mode == 3) | (spinor_calc_mode == 4) )

      if (load_medium_image_switch_em == 0)

        if ( (calc_space_em == 'x') | (calc_space_em == 'y') | (calc_space_em == 'z') )
          for ii=1:n_x_em
            for jj=1:n_y_em
              for kk=1:n_z_em
                if ( (ii<n_x_em*0.25) | (ii>n_x_em-n_x_em*0.25) )
                  mu_tensor(ii,jj,kk)    = mu_m;
                  ep_tensor(ii,jj,kk)    = ep_m;
                  v_tensor(ii,jj,kk)     = 1/sqrt(mu_m*ep_m);
                  h_tensor(ii,jj,kk)     = sqrt(mu_m/ep_m);
                else
                  mu_tensor(ii,jj,kk)    = mu_0;
                  ep_tensor(ii,jj,kk)    = ep_0;
                  v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
                  h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
                end
              end
            end
          end
        end

        % 2D medium initialization
        medium_plot = zeros(n_y_em,n_x_em);
        if (calc_space_em == 'xy') 
          if (medium_shape_em == 1)
            x_1 = -15; x_2 = -7; x_3 = 7; x_4 = 15;
            y_1 = -15; y_2 = -7; y_3 = 7; y_4 = 15;
            for ii=1:n_x_em
              for jj=1:n_y_em
                for kk=1:n_z_em
                  if ( (r_sp_mesh_em{1}(ii)>x_1) & (r_sp_mesh_em{1}(ii)<x_4) & (r_sp_mesh_em{2}(jj)>y_1) & (r_sp_mesh_em{2}(jj)<y_4) )
                    if ( (r_sp_mesh_em{1}(ii)>x_2) & (r_sp_mesh_em{1}(ii)<x_3) & (r_sp_mesh_em{2}(jj)>y_2) & (r_sp_mesh_em{2}(jj)<y_3) )
                      mu_tensor(ii,jj,kk)    = mu_0;
                      ep_tensor(ii,jj,kk)    = ep_0;
                      v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
                      h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
                    else
                      idx_med = idx_med+1;
                      mu_tensor(ii,jj,kk)    = mu_m;
                      ep_tensor(ii,jj,kk)    = ep_m;
                      v_tensor(ii,jj,kk)     = 1/sqrt(mu_m*ep_m);
                      h_tensor(ii,jj,kk)     = sqrt(mu_m/ep_m);
                    end
                  else
                    idx_vac = idx_vac+1;
                    mu_tensor(ii,jj,kk)    = mu_0;
                    ep_tensor(ii,jj,kk)    = ep_0;
                    v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
                    h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
                  end
                end
                v_mesh_plot(jj,ii) = v_tensor(ii,jj,kk);
                h_mesh_plot(jj,ii) = h_tensor(ii,jj,kk);
              end
            end
          elseif(medium_shape_em == 2)
            r_1 = 15^2;;
            r_2 = 10^2;
            for ii=1:n_x_em
              for jj=1:n_y_em
                for kk=1:n_z_em
                  if ( r_sp_mesh_em{1}(ii)^2 + r_sp_mesh_em{2}(jj)^2 < r_1 )
                    if ( r_sp_mesh_em{1}(ii)^2 + r_sp_mesh_em{2}(jj)^2 < r_2 )
                      mu_tensor(ii,jj,kk)    = mu_0;
                      ep_tensor(ii,jj,kk)    = ep_0;
                      v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
                      h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
                    else
                      mu_tensor(ii,jj,kk)    = mu_m;
                      ep_tensor(ii,jj,kk)    = ep_m;
                      v_tensor(ii,jj,kk)     = 1/sqrt(mu_m*ep_m);
                      h_tensor(ii,jj,kk)     = sqrt(mu_m/ep_m);
                    end
                  else
                    mu_tensor(ii,jj,kk)    = mu_0;
                    ep_tensor(ii,jj,kk)    = ep_0;
                    v_tensor(ii,jj,kk)     = 1/sqrt(mu_0*ep_0);
                    h_tensor(ii,jj,kk)     = sqrt(mu_0/ep_0);
                  end
                end
              end
              v_mesh_plot(jj,ii) = v_tensor(ii,jj,kk);
              h_mesh_plot(jj,ii) = h_tensor(ii,jj,kk);
            end
          end
        end
      
      elseif (load_medium_image_switch_em == 1)
  
        mu_image = imread('./input/mu_tensor_input_2D.jpg'); 
        ep_image = imread('./input/ep_tensor_input_2D.jpg');
    
        if (calc_space_em == 'xy')
          for ii=1:n_x_em
            for jj=1:n_y_em
              mu_tensor(ii,jj,1) = mu_0 + ( 1 - (double(mu_image(jj,ii))/255) ) * mu_m;
              ep_tensor(ii,jj,1) = ep_0 + ( 1 - (double(ep_image(jj,ii))/255) ) * ep_m;
              v_tensor(ii,jj,1)  = 1/sqrt(mu_tensor(ii,jj,1) * ep_tensor(ii,jj,1));
              h_tensor(ii,jj,1)  =   sqrt(mu_tensor(ii,jj,1) * ep_tensor(ii,jj,1));
              v_mesh_plot(jj,ii) = v_tensor(ii,jj,1);
              h_mesh_plot(jj,ii) = h_tensor(ii,jj,1);
            end
          end
        elseif (calc_space_em == 'xz')
          for ii=1:n_x_em
            for kk=1:n_z_em
              mu_tensor(ii,1,kk) = mu_0 + ( 1 - (double(mu_image(kk,ii))/255) ) * mu_m;
              ep_tensor(ii,1,kk) = ep_0 + ( 1 - (double(ep_image(kk,ii))/255) ) * ep_m;
              v_tensor(ii,1,kk)  = 1/sqrt(mu_tensor(ii,1,kk) * ep_tensor(ii,1,kk));
              h_tensor(ii,1,kk)  =   sqrt(mu_tensor(ii,1,kk) * ep_tensor(ii,1,kk));
              v_mesh_plot(kk,ii) = v_tensor(ii,1,kk);
              h_mesh_plot(kk,ii) = h_tensor(ii,1,kk);
            end
          end
        elseif (calc_space_em == 'yz')
          for jj=1:n_y_em
            for kk=1:n_z_em
              mu_tensor(1,jj,kk) = mu_0 + ( 1 - (double(mu_image(kk,jj))/255) ) * mu_m;
              ep_tensor(1,jj,kk) = ep_0 + ( 1 - (double(ep_image(kk,jj))/255) ) * ep_m;
              v_tensor(1,jj,kk)  = 1/sqrt(mu_tensor(1,jj,kk) * ep_tensor(1,jj,kk));
              h_tensor(1,jj,kk)  =   sqrt(mu_tensor(1,jj,kk) * ep_tensor(1,jj,kk));
              v_mesh_plot(kk,jj) = v_tensor(1,jj,kk);
              h_mesh_plot(kk,jj) = h_tensor(1,jj,kk);
            end
          end
        end
  
      end
  
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % ploting medium by v_tensor
      if ( (calc_space_em == 'xy') | (calc_space_em == 'xz') | (calc_space_em == 'yz') )
        filename_v_png = './figures/2D_plots/v_tensor.png';
        filename_h_png = './figures/2D_plots/h_tensor.png';
        filename_v_h5  = './figures/2D_plots/v_tensor.h5';
        filename_h_h5  = './figures/2D_plots/h_tensor.h5';
        if (plot_output_switch_em == 1)
          fig1 = figure(1);
          set(gcf,'Visible','off');
          if (calc_space_em == 'xy')
            imagesc(r_sp_mesh_em{1},r_sp_mesh_em{2},v_mesh_plot);
            colormap(gray);
            axis('equal');
            xlim([r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))]);
            ylim([r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))]);
            xlabel('x [a.u.]'); ylabel('y [a.u.]');
            daspect([1 1 1]);
            print(fig1,'-dpng',filename_v_png);
            imagesc(r_sp_mesh_em{1},r_sp_mesh_em{2},h_mesh_plot);
            colormap(gray);
            axis('equal');
            xlim([r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))]);
            ylim([r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))]);
            xlabel('x [a.u.]'); ylabel('y [a.u.]');
            daspect([1 1 1]);
            print(fig1,'-dpng',filename_h_png);
          elseif (calc_space_em == 'xz')
            imagesc(r_sp_mesh_em{1},r_sp_mesh_em{3},v_mesh_plot);
            colormap(gray);
            axis('equal');
            xlim([r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))]);
            ylim([r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))]);
            xlabel('x [a.u.]'); ylabel('z [a.u.]');
            daspect([1 1 1]);
            print(fig1,'-dpng',filename_v_png);
            imagesc(r_sp_mesh_em{1},r_sp_mesh_em{3},h_mesh_plot);
            colormap(gray);
            axis('equal');
            xlim([r_sp_mesh_em{1}(1) r_sp_mesh_em{1}(length(r_sp_mesh_em{1}))]);
            ylim([r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))]);
            xlabel('x [a.u.]'); ylabel('z [a.u.]');
            daspect([1 1 1]);
            print(fig1,'-dpng',filename_h_png);
          elseif (calc_space_em == 'yz')
            imagesc(r_sp_mesh_em{2},r_sp_mesh_em{3},v_mesh_plot);
            colormap(gray);
            axis('equal');
            xlim([r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))]);
            ylim([r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))]);
            xlabel('y [a.u.]'); ylabel('z [a.u.]');
            daspect([1 1 1]);
            print(fig1,'-dpng',filename_v_png);
            imagesc(r_sp_mesh_em{2},r_sp_mesh_em{3},h_mesh_plot);
            colormap(gray);
            axis('equal');
            xlim([r_sp_mesh_em{2}(1) r_sp_mesh_em{2}(length(r_sp_mesh_em{2}))]);
            ylim([r_sp_mesh_em{3}(1) r_sp_mesh_em{3}(length(r_sp_mesh_em{3}))]);
            xlabel('y [a.u.]'); ylabel('z [a.u.]');
            daspect([1 1 1]);
            print(fig1,'-dpng',filename_h_png);
          end
        end
        delete(filename_v_h5); delete(filename_h_h5);
        if exist(filename_v_h5, 'file') ~= 2  
          h5create(filename_v_h5, '/v_tensor', size(v_mesh_plot));
        end
        if exist(filename_h_h5, 'file') ~= 2  
          h5create(filename_h_h5, '/h_tensor', size(h_mesh_plot));
        end
        h5write(filename_v_h5, '/v_tensor', v_mesh_plot);
        h5write(filename_h_h5, '/h_tensor', h_mesh_plot);
     
      elseif ( (calc_space_em == 'x') | (calc_space_em == 'y') | (calc_space_em == 'z') )
        filename_v_png = './figures/1D_plots/v_tensor.png';
        filename_h_png = './figures/1D_plots/h_tensor.png';
        filename_v_h5  = './figures/1D_plots/v_tensor.h5';
        filename_h_h5  = './figures/1D_plots/h_tensor.h5';
        if (plot_output_switch_em == 1)
          fig1 = figure(1);
          set(gcf,'Visible', 'off');
          if (calc_space_em == 'x')
            plot(r_sp_mesh_em{1},v_tensor(:,1,1));
            print(fig1,'-dpng',filename_v_png);
            plot(r_sp_mesh_em{1},h_tensor(:,1,1));
            print(fig1,'-dpng',filename_h_png);
          end
          if (calc_space_em == 'y')
            plot(r_sp_mesh_em{2},v_tensor(1,:,1));
            print(fig1,'-dpng',filename_v_png);
            plot(r_sp_mesh_em{2},h_tensor(1,:,1));
            print(fig1,'-dpng',filename_h_png);
          end
          if (calc_space_em == 'z')
            plot(r_sp_mesh_em{3},v_tensor(1,1,:));
            print(fig1,'-dpng',filename_v_png);
            plot(r_sp_mesh_em{3},h_tensor(1,1,:));
            print(fig1,'-dpng',filename_h_png);
          end
        end
        delete(filename_v_h5); delete(filename_h_h5);
        if exist(filename_v_h5, 'file') ~= 2  
          h5create(filename_v_h5, '/v_tensor', size(v_tensor));
        end
        if exist(filename_h_h5, 'file') ~= 2  
          h5create(filename_h_h5, '/h_tensor', size(h_tensor));
        end
        h5write(filename_v_h5, '/v_tensor', v_tensor);
        h5write(filename_h_h5, '/h_tensor', h_tensor);
      end

    end  
