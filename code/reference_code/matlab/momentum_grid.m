% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [m_sp_mesh] = momentum_grid(boxlength_x, boxlength_y, boxlength_z, n_x, n_y, n_z, r_sp_mesh) 

    if (n_x > 1)
      for jj=0:(n_x/2)
        m_sp_mesh{1}(jj+1) = 2*pi/(double(boxlength_x)) * double(jj);
      end
      for jj=(n_x/2+1):(n_x-1)
        m_sp_mesh{1}(jj+1) = 2*pi/(double(boxlength_x)) * double(jj-n_x);
      end
    else
      m_sp_mesh{1}(1) = r_sp_mesh{1}(1);
    end

    if (n_y > 1)
      for jj=0:(n_y/2)
        m_sp_mesh{2}(jj+1) = 2*pi/(double(boxlength_y)) * double(jj);
      end
      for jj=(n_y/2+1):(n_y-1)
        m_sp_mesh{2}(jj+1) = 2*pi/(double(boxlength_y)) * double(jj-n_y);
      end
    else
      m_sp_mesh{2}(1) = r_sp_mesh{2}(1);
    end

    if (n_z > 1)
      for jj=0:(n_z/2)
        m_sp_mesh{3}(jj+1) = 2*pi/(double(boxlength_z)) * double(jj);
      end
      for jj=(n_z/2+1):(n_z-1)
        m_sp_mesh{3}(jj+1) = 2*pi/(double(boxlength_z)) * double(jj-n_z);
      end
    else
      m_sp_mesh{3}(1) = r_sp_mesh{3}(1);
    end

