% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [div_v_tensor, div_h_tensor, del_x_v_tensor, del_2_x_v_tensor, del_y_v_tensor, del_2_y_v_tensor, del_z_v_tensor, del_2_z_v_tensor, ...
                             del_x_h_tensor, del_2_x_h_tensor, del_y_h_tensor, del_2_y_h_tensor, del_z_h_tensor, del_2_z_h_tensor] ...
              = derivatives_of_v_h_tensors(n_x_em, n_y_em, n_z_em, v_tensor, h_tensor, m_sp_mesh_em, h);

     [fft_div_op_em, fft_del_x_op_em, fft_del_2_x_op_em, fft_del_y_op_em, fft_del_2_y_op_em, fft_del_z_op_em, fft_del_2_z_op_em] ...
          = fft_derivative_operators(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, h);

     fft_v_tensor = fftn(v_tensor);
     fft_h_tensor = fftn(h_tensor);
  
     for ii=1:n_x_em
       for jj=1:n_y_em
         for kk=1:n_z_em
           fft_div_v_tensor(ii,jj,kk)     = fft_div_op_em(ii,jj,kk) * fft_v_tensor(ii,jj,kk);
           fft_div_h_tensor(ii,jj,kk)     = fft_div_op_em(ii,jj,kk) * fft_h_tensor(ii,jj,kk);
           fft_del_x_v_tensor(ii,jj,kk)   = fft_del_x_op_em(ii)     * fft_v_tensor(ii,jj,kk);
           fft_del_y_v_tensor(ii,jj,kk)   = fft_del_y_op_em(jj)     * fft_v_tensor(ii,jj,kk);
           fft_del_z_v_tensor(ii,jj,kk)   = fft_del_z_op_em(kk)     * fft_v_tensor(ii,jj,kk);
           fft_del_2_x_v_tensor(ii,jj,kk) = fft_del_2_x_op_em(ii)   * fft_v_tensor(ii,jj,kk);
           fft_del_2_y_v_tensor(ii,jj,kk) = fft_del_2_y_op_em(jj)   * fft_v_tensor(ii,jj,kk);
           fft_del_2_z_v_tensor(ii,jj,kk) = fft_del_2_z_op_em(kk)   * fft_v_tensor(ii,jj,kk);
           fft_del_x_h_tensor(ii,jj,kk)   = fft_del_x_op_em(ii)     * fft_h_tensor(ii,jj,kk);
           fft_del_y_h_tensor(ii,jj,kk)   = fft_del_y_op_em(jj)     * fft_h_tensor(ii,jj,kk);
           fft_del_z_h_tensor(ii,jj,kk)   = fft_del_z_op_em(kk)     * fft_h_tensor(ii,jj,kk);
           fft_del_2_x_h_tensor(ii,jj,kk) = fft_del_2_x_op_em(ii)   * fft_h_tensor(ii,jj,kk);
           fft_del_2_y_h_tensor(ii,jj,kk) = fft_del_2_y_op_em(jj)   * fft_h_tensor(ii,jj,kk);
           fft_del_2_z_h_tensor(ii,jj,kk) = fft_del_2_z_op_em(kk)   * fft_h_tensor(ii,jj,kk);
         end
       end
     end

     div_v_tensor     = ifftn(fft_div_v_tensor);
     div_h_tensor     = ifftn(fft_div_h_tensor);

     del_x_v_tensor   = ifftn(fft_del_x_v_tensor);
     del_y_v_tensor   = ifftn(fft_del_y_v_tensor);
     del_z_v_tensor   = ifftn(fft_del_z_v_tensor);

     del_2_x_v_tensor = ifftn(fft_del_2_x_v_tensor);
     del_2_y_v_tensor = ifftn(fft_del_2_y_v_tensor);
     del_2_z_v_tensor = ifftn(fft_del_2_z_v_tensor);
 
     del_x_h_tensor   = ifftn(fft_del_x_h_tensor);
     del_y_h_tensor   = ifftn(fft_del_y_h_tensor);
     del_z_h_tensor   = ifftn(fft_del_z_h_tensor);

     del_2_x_h_tensor = ifftn(fft_del_2_x_h_tensor);
     del_2_y_h_tensor = ifftn(fft_del_2_y_h_tensor);
     del_2_z_h_tensor = ifftn(fft_del_2_z_h_tensor);
