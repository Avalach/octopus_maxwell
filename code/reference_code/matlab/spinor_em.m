% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Phi_em] = spinor_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, Ex, Ey, Ez, Bx, By, Bz, ep_tensor, mu_tensor)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Riemann-Silberstein vector   

    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em
          Fx_plus( ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ex(ii,jj,kk) + i / sqrt(2*mu_tensor(ii,jj,kk)) * Bx(ii,jj,kk);
          Fx_minus(ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ex(ii,jj,kk) - i / sqrt(2*mu_tensor(ii,jj,kk)) * Bx(ii,jj,kk);
          Fy_plus( ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ey(ii,jj,kk) + i / sqrt(2*mu_tensor(ii,jj,kk)) * By(ii,jj,kk);
          Fy_minus(ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ey(ii,jj,kk) - i / sqrt(2*mu_tensor(ii,jj,kk)) * By(ii,jj,kk);
          Fz_plus( ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ez(ii,jj,kk) + i / sqrt(2*mu_tensor(ii,jj,kk)) * Bz(ii,jj,kk);
          Fz_minus(ii,jj,kk) = sqrt(ep_tensor(ii,jj,kk)/2) * Ez(ii,jj,kk) - i / sqrt(2*mu_tensor(ii,jj,kk)) * Bz(ii,jj,kk);
        end
      end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % initialization of the spinor vector

    if (spinor_calc_mode == 1)
      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            Phi_em{1}(ii,jj,kk) = Fx_plus(ii,jj,kk);
            Phi_em{2}(ii,jj,kk) = Fy_plus(ii,jj,kk);
            Phi_em{3}(ii,jj,kk) = Fz_plus(ii,jj,kk);
          end
        end
      end
    elseif (spinor_calc_mode == 2)
      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            Phi_em{1}(ii,jj,kk) = -Fx_plus(ii,jj,kk) + i * Fy_plus(ii,jj,kk);
            Phi_em{2}(ii,jj,kk) =  Fz_plus(ii,jj,kk);
            Phi_em{3}(ii,jj,kk) =  Fz_plus(ii,jj,kk);
            Phi_em{4}(ii,jj,kk) =  Fx_plus(ii,jj,kk) + i * Fy_plus(ii,jj,kk);
          end
        end
      end
    elseif (spinor_calc_mode == 3)
      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            % plus Spinor
            Phi_em{1}(ii,jj,kk) = Fx_plus(ii,jj,kk); 
            Phi_em{2}(ii,jj,kk) = Fy_plus(ii,jj,kk); 
            Phi_em{3}(ii,jj,kk) = Fz_plus(ii,jj,kk); 
            % minus Spinor
            Phi_em{4}(ii,jj,kk) = Fx_minus(ii,jj,kk);
            Phi_em{5}(ii,jj,kk) = Fy_minus(ii,jj,kk);
            Phi_em{6}(ii,jj,kk) = Fz_minus(ii,jj,kk);
          end
        end
      end
    elseif (spinor_calc_mode == 4)
      for ii=1:n_x_em
        for jj=1:n_y_em
          for kk=1:n_z_em
            % plus Spinor
            Phi_em{1}(ii,jj,kk) = -Fx_plus(ii,jj,kk)  + i * Fy_plus(ii,jj,kk);
            Phi_em{2}(ii,jj,kk) =  Fz_plus(ii,jj,kk);
            Phi_em{3}(ii,jj,kk) =  Fz_plus(ii,jj,kk);
            Phi_em{4}(ii,jj,kk) =  Fx_plus(ii,jj,kk)  + i * Fy_plus(ii,jj,kk);
            % minus Spinor
            Phi_em{5}(ii,jj,kk) = -Fx_minus(ii,jj,kk) - i * Fy_minus(ii,jj,kk);
            Phi_em{6}(ii,jj,kk) =  Fz_minus(ii,jj,kk);
            Phi_em{7}(ii,jj,kk) =  Fz_minus(ii,jj,kk);
            Phi_em{8}(ii,jj,kk) =  Fx_minus(ii,jj,kk) - i * Fy_minus(ii,jj,kk);
          end
        end
      end
    end
        
