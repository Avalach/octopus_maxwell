% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Ex, Ey, Ez, Bx, By, Bz] = initial_laser_em(calc_space_em, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, k_x_em, k_y_em, k_z_em, sigma_x_em, sigma_y_em, sigma_z_em, ...
                                                     signal_pos_x_em, signal_pos_y_em, signal_pos_z_em, wave_shape_em, v_tensor)


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initial electromagnetic field in 1D
    if ( (calc_space_em == 'x') | (calc_space_em == 'y') | (calc_space_em == 'z') )

      % initialization of the electromagnetic field
      Ex = zeros(n_x_em,n_y_em,n_z_em);
      Ey = zeros(n_x_em,n_y_em,n_z_em);
      Ez = zeros(n_x_em,n_y_em,n_z_em);
      Bx = zeros(n_x_em,n_y_em,n_z_em);
      By = zeros(n_x_em,n_y_em,n_z_em);
      Bz = zeros(n_x_em,n_y_em,n_z_em);

      amplitude = 1^(1);

      if (wave_shape_em ~= 0)

        for ii=1:n_x_em
          for jj=1:n_y_em
            for kk=1:n_z_em
              if (wave_shape_em == 1)
                if (calc_space_em == 'x')
                  sinus_wave   = amplitude * sin(k_x_em*(r_sp_mesh_em{1}(ii)-signal_pos_x_em));
                  Ez(ii,jj,kk) =                         sinus_wave;
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * sinus_wave;       
                elseif (calc_space_em == 'y')
                  sinus_wave   = amplitude * sin(k_y_em*(r_sp_mesh_em{2}(jj)-signal_pos_y_em));
                  Ez(ii,jj,kk) =                         sinus_wave;
                  Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave;
                elseif (calc_space_em == 'z')
                  sinus_wave   = amplitude * sin(k_z_em*(r_sp_mesh_em{3}(kk)-signal_pos_z_em));
                  Ex(ii,jj,kk) =                         sinus_wave;
                  By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave;
                end
              elseif (wave_shape_em == 2)
                if (calc_space_em == 'x') 
                  gauss_dist   = amplitude * 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
                  Ez(ii,jj,kk) =                         gauss_dist;
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * gauss_dist;
                elseif (calc_space_em == 'y')
                  gauss_dist   = amplitude * 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
                  Ez(ii,jj,kk) =                         gauss_dist;
                  Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * gauss_dist;
                elseif (calc_space_em == 'z')
                  gauss_dist   = amplitude * 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
                  Ex(ii,jj,kk) =                         gauss_dist;
                  By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * gauss_dist;
                end
              elseif (wave_shape_em == 3)
                if (calc_space_em == 'x')
                  sinus_wave   = sin(k_x_em*r_sp_mesh_em{1}(ii));
                  gauss_dist   = amplitude * 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
                  Ez(ii,jj,kk) =                         sinus_wave * gauss_dist;  
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * sinus_wave * gauss_dist;
                elseif (calc_space_em == 'y')
                  sinus_wave = sin(k_y_em*r_sp_mesh_em{2}(jj));
                  gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
                  Ez(ii,jj,kk) =                         sinus_wave * gauss_dist;  
                  Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave * gauss_dist;
                elseif (calc_space_em == 'z')
                  sinus_wave = sin(k_z_em*r_sp_mesh_em{3}(kk));
                  gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
                  Ex(ii,jj,kk) =                         sinus_wave * gauss_dist;  
                  By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * sinus_wave * gauss_dist;
                end
              end
            end
          end
        end

      end
 
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initial electromagnetic field in 2D 

    if ( (calc_space_em == 'xy') | (calc_space_em == 'xz') | (calc_space_em == 'yz') )

      % initialization of electromagnetic field
      Ex = zeros(n_x_em,n_y_em,n_z_em);
      Ey = zeros(n_x_em,n_y_em,n_z_em);
      Ez = zeros(n_x_em,n_y_em,n_z_em);
      Bx = zeros(n_x_em,n_y_em,n_z_em);
      By = zeros(n_x_em,n_y_em,n_z_em);
      Bz = zeros(n_x_em,n_y_em,n_z_em);

      if (wave_spape_em ~= 0)

        for ii=1:n_x_em
          for jj=1:n_y_em
            for kk=1:n_z_em
              if (wave_shape_em == 1)
                if (calc_space_em == 'xy')
                  sinus_wave_x = sin(k_x_em*(r_sp_mesh_em{1}(ii)-signal_pos_x_em));
                  sinus_wave_y = sin(k_y_em*(r_sp_mesh_em{2}(jj)-signal_pos_y_em));
                  Ez(ii,jj,kk) = sinus_wave_x * sinus_wave_y ;
                  Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
                elseif (calc_space_em == 'xz')
                  sinus_wave_x = sin(k_x_em*(r_sp_mesh_em{1}(ii)-signal_pos_x_em));
                  sinus_wave_z = sin(k_z_em*(r_sp_mesh_em{3}(kk)-signal_pos_z_em));
                  Ey(ii,jj,kk) = sinus_wave_x * sinus_wave_z ;
                  Bx(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
                  Bz(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
                elseif (calc_space_em == 'yz')
                  sinus_wave_y = sin(k_y_em*(r_sp_mesh_em{2}(jj)-signal_pos_y_em));
                  sinus_wave_z = sin(k_z_em*(r_sp_mesh_em{3}(kk)-signal_pos_z_em));
                  Ex(ii,jj,kk) = sinus_wave_y * sinus_wave_z ;
                  By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
                  Bz(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
                end
              elseif (wave_shape_em == 2) 
                if (calc_space_em == 'xy')
                  gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
                  gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2)); 
                  Ez(ii,jj,kk) = gauss_dist_x * gauss_dist_y ;
                  Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
                elseif (calc_space_em == 'xz')
                  gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
                  gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2)); 
                  Ey(ii,jj,kk) = gauss_dist_x * gauss_dist_z ;
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
                  Bz(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
                elseif (calc_space_em == 'yz')
                  gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
                  gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2)); 
                  Ex(ii,jj,kk) = gauss_dist_y * gauss_dist_z ;
                  By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
                  Bz(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
                end
              elseif (wave_shape_em == 3)
                if (calc_space_em == 'xy')
                  sinus_wave_x = sin(k_x_em*r_sp_mesh_em{1}(ii));
                  sinus_wave_y = 1;
                  gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
                  gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
                  Ez(ii,jj,kk) =  sinus_wave_x * sinus_wave_y * gauss_dist_x * gauss_dist_y ;  
                  Bx(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_y_em^2) * Ez(ii,jj,kk) ;
                elseif (calc_space_em == 'xz')
                  sinus_wave_x = sin(k_x_em*r_sp_mesh_em{1}(ii));
                  sinus_wave_z = 1;
                  gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_em)) * exp(-((r_sp_mesh_em{1}(ii)-signal_pos_x_em)^2)/(4*sigma_x_em^2));
                  gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
                  Ey(ii,jj,kk) =  sinus_wave_x * sinus_wave_z * gauss_dist_x * gauss_dist_z ;  
                  Bx(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
                  By(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_x_em/sqrt(k_x_em^2+k_z_em^2) * Ey(ii,jj,kk) ;
                elseif (calc_space_em == 'yz')
                  sinus_wave_y = sin(k_y_em*r_sp_mesh_em{2}(jj));
                  sinus_wave_z = 1;
                  gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_em)) * exp(-((r_sp_mesh_em{2}(jj)-signal_pos_y_em)^2)/(4*sigma_y_em^2));
                  gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_em)) * exp(-((r_sp_mesh_em{3}(kk)-signal_pos_z_em)^2)/(4*sigma_z_em^2));
                  Ex(ii,jj,kk) =  sinus_wave_y * sinus_wave_z * gauss_dist_y * gauss_dist_z ;  
                  By(ii,jj,kk) = -1/v_tensor(ii,jj,kk) * k_z_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
                  Bz(ii,jj,kk) =  1/v_tensor(ii,jj,kk) * k_y_em/sqrt(k_y_em^2+k_z_em^2) * Ex(ii,jj,kk) ;
                end
              end
            end
          end
        end

      end
 
    end
