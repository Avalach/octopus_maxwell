% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Phi_at] = ground_state_evolution(calc_space_at, Phi_at, n_x_at, n_y_at, n_z_at, gs_t_st_max_at, exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at, ...
                                           gs_check_steps_at, gs_error_at, dx_at, dy_at, dz_at, mask_at, plot_output_switch_at, r_sp_mesh_at, contour_resolution_at, fontsize_at)

%    if( (n_x_at==1) | (n_y_at==1) | (n_z_at==1) )
%      Phi_at = Phi_at;
%    else

      fprintf('Reading file "ground_state.txt" ...\n\n');

      if (exist('./input/ground_state.txt') == 2)
        Phi_at_matrix = load('./input/ground_state.txt');
        if (calc_space_at == 'x')
          for ii=1:n_x_at
            Phi_at(ii,1,1) = Phi_at_matrix(ii);
          end
        elseif (calc_space_at == 'y')
          for jj=1:n_y_at
            Phi_at(1,jj,1) = Phi_at_matrix(jj);
          end
        elseif (calc_space_at == 'z')
          for kk=1:n_z_at
            Phi_at(1,1,kk) = Phi_at_matrix(kk);
          end
        end
        if ( ( length(Phi_at(:,1,1)) == n_x_at ) & ( length(Phi_at(1,:,1)) == n_y_at ) & ( length(Phi_at(1,1,:)) == n_z_at ) )
          Phi_at_check = 1;      
          fprintf('Found file "ground_state.txt" and array lengths agree with number of grid points ...\n\n');
        else
          Phi_at_check = 0;
          fprintf('Found file "ground_state.txt" but array lengths do not agree with number of grid points ...\n\n');
        end
      else 
        Phi_at_check = 0;
        fprintf('File "ground_state.txt" does not exisct ...\n\n');
      end
 
      if (Phi_at_check == 0 )

        for gs_t_st_at=1:gs_t_st_max_at
 
          if (mod(gs_t_st_at,10) == 0)
            steps_string = int2str(gs_t_st_at);
            combinedStr = strcat('Timesteps done for imaginary time evolution to obtain the ground state :    ',steps_string,' ...\n');
            fprintf(combinedStr);
          end

          if (gs_t_st_at <= gs_check_steps_at)
            Phi_at_convergence{gs_t_st_at} = Phi_at;
          end

          if (gs_t_st_at > gs_check_steps_at)
            for pp=1: (gs_check_steps_at-1)
              Phi_at_convergence{pp} = Phi_at_convergence{pp+1};
            end
            Phi_at_convergence{gs_check_steps_at} = Phi_at;
            error_Phi_at = sum( abs(Phi_at_convergence{1}-Phi_at) ) / (n_x_at*n_y_at*n_z_at);
            if (error_Phi_at < gs_error_at)
              fprintf('\n');
              break;
            end
          end
 
          [Phi_at] = propagation_at(exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at, mask_at, Phi_at);

          [norm_at, renorm_Phi_at] = norm_wave_function(Phi_at, dx_at, dy_at, dz_at);
          Phi_at = renorm_Phi_at;

          Phi_abs_square_at = conj(Phi_at).*Phi_at;
 
          if (plot_output_switch_at == 1)
            plot_ground_state_evolution(calc_space_at, n_x_at, n_y_at, n_z_at, Phi_abs_square_at, gs_t_st_at, r_sp_mesh_at, contour_resolution_at, fontsize_at);
          end

        end
 
        idx=0;
        for ii=1:n_x_at
          for jj=1:n_y_at
            for kk=1:n_z_at
              idx = idx + 1;
              Phi_at_matrix(idx) = Phi_at(ii,jj,kk);
            end
          end
        end

        save './input/ground_state.txt' Phi_at_matrix -ascii;

      end

%    end
