% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function write_h5_file_em_field(calc_space_em, n_x_em, n_y_em, n_z_em, Ex, Ey, Ez, Bx, By, Bz, t_step)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Electromagnetic field in 1D

    if ( (calc_space_em == 'x') | (calc_space_em == 'y') | (calc_space_em == 'z') )

      if (calc_space_em == 'x')
        for ii=1:n_x_em;
          Ex_plot(ii) = Ex(ii,1,1);
          Ey_plot(ii) = Ey(ii,1,1);
          Ez_plot(ii) = Ez(ii,1,1);
          Bx_plot(ii) = Bx(ii,1,1);
          By_plot(ii) = By(ii,1,1);
          Bz_plot(ii) = By(ii,1,1);
        end
      elseif (calc_space_em == 'y')
        for jj=1:n_y_em;
          Ex_plot(jj) = Ex(1,jj,1);
          Ey_plot(jj) = Ey(1,jj,1);
          Ez_plot(jj) = Ez(1,jj,1);
          Bx_plot(jj) = Bx(1,jj,1);
          By_plot(jj) = By(1,jj,1);
          Bz_plot(jj) = By(1,jj,1);
        end
      elseif (calc_space_em == 'z')
        for kk=1:n_z_em;
          Ex_plot(kk) = Ex(1,1,kk);
          Ey_plot(kk) = Ey(1,1,kk);
          Ez_plot(kk) = Ez(1,1,kk);
          Bx_plot(kk) = Bx(1,1,kk);
          By_plot(kk) = By(1,1,kk);
          Bz_plot(kk) = By(1,1,kk);
        end
      end

      if (calc_space_em == 'x')
        filename_Ex  = strcat('./figures/1D_plots/h5_files/x_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ey  = strcat('./figures/1D_plots/h5_files/x_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ez  = strcat('./figures/1D_plots/h5_files/x_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bx  = strcat('./figures/1D_plots/h5_files/x_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_By  = strcat('./figures/1D_plots/h5_files/x_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bz  = strcat('./figures/1D_plots/h5_files/x_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.h5');    
      elseif (calc_space_em == 'y')
        filename_Ex  = strcat('./figures/1D_plots/h5_files/y_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ey  = strcat('./figures/1D_plots/h5_files/y_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ez  = strcat('./figures/1D_plots/h5_files/y_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bx  = strcat('./figures/1D_plots/h5_files/y_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_By  = strcat('./figures/1D_plots/h5_files/y_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bz  = strcat('./figures/1D_plots/h5_files/y_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.h5'); 
      elseif (calc_space_em == 'z')
        filename_Ex  = strcat('./figures/1D_plots/h5_files/z_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ey  = strcat('./figures/1D_plots/h5_files/z_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ez  = strcat('./figures/1D_plots/h5_files/z_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bx  = strcat('./figures/1D_plots/h5_files/z_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_By  = strcat('./figures/1D_plots/h5_files/z_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bz  = strcat('./figures/1D_plots/h5_files/z_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.h5'); 
      end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Electromagnetic field in 2D 

    if ( (calc_space_em == 'xy') | (calc_space_em == 'xz') | (calc_space_em == 'yz') )
 
      if (calc_space_em == 'xy')
        for ii=1:n_x_em
          for jj=1:n_y_em
            Ex_plot(jj,ii) = Ex(ii,jj,1);
            Ey_plot(jj,ii) = Ey(ii,jj,1);
            Ez_plot(jj,ii) = Ez(ii,jj,1);
            Bx_plot(jj,ii) = Bx(ii,jj,1);
            By_plot(jj,ii) = By(ii,jj,1);
            Bz_plot(jj,ii) = Bz(ii,jj,1);
          end
        end
      elseif (calc_space_em == 'xz')
        for ii=1:n_x_em 
          for kk=1:n_z_em
            Ex_plot(kk,ii) = Ex(ii,1,kk);
            Ey_plot(kk,ii) = Ey(ii,1,kk);
            Ez_plot(kk,ii) = Ez(ii,1,kk);
            Bx_plot(kk,ii) = Bx(ii,1,kk);
            By_plot(kk,ii) = By(ii,1,kk);
            Bz_plot(kk,ii) = Bz(ii,1,kk);
          end
        end
      elseif (calc_space_em == 'yz')
        for jj=1:n_y_em
          for kk=1:n_z_em
            Ex_plot(kk,jj) = Ex(1,jj,kk);
            Ey_plot(kk,jj) = Ey(1,jj,kk);
            Ez_plot(kk,jj) = Ez(1,jj,kk);
            Bx_plot(kk,jj) = Bx(1,jj,kk);
            By_plot(kk,jj) = By(1,jj,kk);
            Bz_plot(kk,jj) = Bz(1,jj,kk);
          end
        end
      end

      if (calc_space_em == 'xy')
        filename_Ex  = strcat('./figures/2D_plots/h5_files/xy_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ey  = strcat('./figures/2D_plots/h5_files/xy_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ez  = strcat('./figures/2D_plots/h5_files/xy_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bx  = strcat('./figures/2D_plots/h5_files/xy_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_By  = strcat('./figures/2D_plots/h5_files/xy_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bz  = strcat('./figures/2D_plots/h5_files/xy_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.h5');    
      elseif (calc_space_em == 'xz')
        filename_Ex  = strcat('./figures/2D_plots/h5_files/xz_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ey  = strcat('./figures/2D_plots/h5_files/xz_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ez  = strcat('./figures/2D_plots/h5_files/xz_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bx  = strcat('./figures/2D_plots/h5_files/xz_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_By  = strcat('./figures/2D_plots/h5_files/xz_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bz  = strcat('./figures/2D_plots/h5_files/xz_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.h5'); 
      elseif (calc_space_em == 'yz')
        filename_Ex  = strcat('./figures/2D_plots/h5_files/yz_propagation/E_field_x/E_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ey  = strcat('./figures/2D_plots/h5_files/yz_propagation/E_field_y/E_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Ez  = strcat('./figures/2D_plots/h5_files/yz_propagation/E_field_z/E_field_z_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bx  = strcat('./figures/2D_plots/h5_files/yz_propagation/B_field_x/B_field_x_t_',num2str(t_step,'%04d'),'.h5');
        filename_By  = strcat('./figures/2D_plots/h5_files/yz_propagation/B_field_y/B_field_y_t_',num2str(t_step,'%04d'),'.h5');
        filename_Bz  = strcat('./figures/2D_plots/h5_files/yz_propagation/B_field_z/B_field_z_t_',num2str(t_step,'%04d'),'.h5'); 
      end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Writing h5 files

    delete(filename_Ex);
    if exist(filename_Ex, 'file') ~= 2  
      h5create(filename_Ex, '/Ex', size(Ex_plot));
    end
    delete(filename_Ey);
    if exist(filename_Ey, 'file') ~= 2
      h5create(filename_Ey, '/Ey', size(Ey_plot));
    end
    delete(filename_Ez);
    if exist(filename_Ez, 'file') ~= 2
      h5create(filename_Ez, '/Ez', size(Ez_plot));
    end
    delete(filename_Bx);
    if exist(filename_Bx, 'file') ~= 2
      h5create(filename_Bx, '/Bx', size(Bx_plot));
    end
    delete(filename_By);
    if exist(filename_By, 'file') ~= 2
      h5create(filename_By, '/By', size(By_plot));
    end
    delete(filename_Bz);
    if exist(filename_Bz, 'file') ~= 2
      h5create(filename_Bz, '/Bz', size(Bz_plot));
    end
    h5write(filename_Ex, '/Ex', Ex_plot);
    h5write(filename_Ey, '/Ey', Ey_plot);
    h5write(filename_Ez, '/Ez', Ez_plot); 
    h5write(filename_Bx, '/Bx', Bx_plot);
    h5write(filename_By, '/By', By_plot);
    h5write(filename_Bz, '/Bz', Bz_plot);


