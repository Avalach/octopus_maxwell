% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [field_match_at] = multiscale_match_em(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, dx_em, dy_em, dz_em, dx_at, dy_at, dz_at, ...
                                                idx_em_of_at, delta_em_of_at, step_em_of_at, number_at_of_em, idx_at_of_em, average_switch, field_em)

    field_match_at = zeros(n_x_at,n_y_at,n_z_at);

  if (average_switch == 1)
    for ii_em=1:n_x_em
      for jj_em=1:n_y_em
        for kk_em=1:n_z_em
          field_match_at(idx_at_of_em{1}(ii_em,jj_em,kk_em),idx_at_of_em{2}(ii_em,jj_em,kk_em),idx_at_of_em{3}(ii_em,jj_em,kk_em)) = ...
               field_match_at(idx_at_of_em{1}(ii_em,jj_em,kk_em),idx_at_of_em{2}(ii_em,jj_em,kk_em),idx_at_of_em{3}(ii_em,jj_em,kk_em)) ...
             + 1/number_at_of_em(idx_at_of_em{1}(ii_em,jj_em,kk_em),idx_at_of_em{2}(ii_em,jj_em,kk_em),idx_at_of_em{3}(ii_em,jj_em,kk_em)) ...
             * field_em(ii_em,jj_em,kk_em);
        end
      end
    end
  elseif(average_switch == 0)
    for ii_at=1:n_x_at
      for jj_at=1:n_y_at
        for kk_at=1:n_z_at
 %         if (number_at_of_em(ii_at,jj_at,kk_at) > 2)
              field_match_em(ii_at,jj_at,kk_at) = field_match_at(ii_at,jj_at,kk_at);
 %         elseif (number_at_of_em(ii_at,jj_at,kk_at) > 0)
            field_em_0 = field_em(idx_em_of_at{1}(ii_at,jj_at,kk_at), idx_em_of_at{2}(ii_at,jj_at,kk_at), idx_em_of_at{3}(ii_at,jj_at,kk_at));
            if ( (ii_at==1) | (ii_at==n_x_at) )
              field_em_x = 0;
            else
              field_em_x = field_em(idx_em_of_at{1}(ii_at,jj_at,kk_at)+step_em_of_at{1}(ii_at,jj_at,kk_at), idx_em_of_at{2}(ii_at,jj_at,kk_at), idx_em_of_at{3}(ii_at,jj_at,kk_at));
            end
            if ( (jj_at==1) | (jj_at==n_y_at) )
              field_em_y = 0;
            else
              field_em_y = field_em(idx_em_of_at{1}(ii_at,jj_at,kk_at), idx_em_of_at{2}(ii_at,jj_at,kk_at)+step_em_of_at{2}(ii_at,jj_at,kk_at), idx_em_of_at{3}(ii_at,jj_at,kk_at));
            end
            if ( (kk_at==1) | (kk_at==n_z_at) )
              field_em_z = 0;
            else
              field_em_z = field_em(idx_em_of_at{1}(ii_at,jj_at,kk_at), idx_em_of_at{2}(ii_at,jj_at,kk_at), idx_em_of_at{3}(ii_at,jj_at,kk_at)+step_em_of_at{3}(ii_at,jj_at,kk_at));
            end
            df_x       = (field_em_x-field_em_0)/dx_em * delta_em_of_at{1}(ii_at,jj_at,kk_at);
            df_y       = (field_em_y-field_em_0)/dy_em * delta_em_of_at{2}(ii_at,jj_at,kk_at);
            df_z       = (field_em_z-field_em_0)/dz_em * delta_em_of_at{3}(ii_at,jj_at,kk_at);
            field_at   = field_em_0 + df_x + df_y + df_z;
            field_match_at(ii_at,jj_at,kk_at) = field_at;
 %         end
        end
      end
    end
  end
