% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [mask] = mask_function_em(spinor_calc_mode, n_x, n_y, n_z)

    if (spinor_calc_mode == 1)
      qq_limit = 3;
    elseif (spinor_calc_mode == 2)
      qq_limit = 4;
    elseif (spinor_calc_mode == 3)
      qq_limit = 6;
    elseif (spinor_calc_mode == 4)
      qq_limit = 8;
    end

    rel_border = 0.1;
    min_border_x = round(rel_border*n_x);
    max_border_x = round(n_x-min_border_x);
    min_border_y = round(rel_border*n_y);
    max_border_y = round(n_y-min_border_y);
    min_border_z = round(rel_border*n_z);
    max_border_z = round(n_z-min_border_z);
    nx_mask      = double(min_border_x);
    ny_mask      = double(min_border_y);
    nz_mask      = double(min_border_z);
    for qq=1:qq_limit
      for ii=1:n_x
        for jj=1:n_y
          for kk=1:n_z
            if  (ii<min_border_x | ii>max_border_x) 
              if (ii<min_border_x)
                mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(ii-1)/nx_mask))));
              elseif (ii>max_border_x)
                mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(n_x-ii)/nx_mask))));
              end
            elseif (jj<min_border_y | jj>max_border_y)
              if (jj<min_border_y)
                mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(jj-1)/ny_mask))));
              elseif (jj>max_border_y)
                mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(n_y-jj)/ny_mask))));
              end
            elseif (kk<min_border_z | kk>max_border_z)
              if (kk<min_border_z)
                mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(kk-1)/nz_mask))));
              elseif (kk>max_border_z)
                mask{qq}(ii,jj,kk) = sqrt(sqrt(abs(sin(0.5*pi*(n_z-kk)/nz_mask))));
              end
            else
              mask{qq}(ii,jj,kk) = 1.0;
            end
          end
        end
      end
    end


