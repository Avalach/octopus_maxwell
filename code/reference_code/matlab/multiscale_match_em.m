% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [field_match_em] = multiscale_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, dx_at, dy_at, dz_at, dx_em, dy_em, dz_em, ...
                                                idx_at_of_em, delta_at_of_em, step_at_of_em, number_em_of_at, idx_em_of_at, average_switch, field_at)   

    field_match_em = zeros(n_x_em, n_y_em, n_z_em);

  if (average_switch == 1)
    for ii_at=1:n_x_at
      for jj_at=1:n_y_at
        for kk_at=1:n_z_at
          field_match_em(idx_em_of_at{1}(ii_at,jj_at,kk_at),idx_em_of_at{2}(ii_at,jj_at,kk_at),idx_em_of_at{3}(ii_at,jj_at,kk_at)) = ...
               field_match_em(idx_em_of_at{1}(ii_at,jj_at,kk_at),idx_em_of_at{2}(ii_at,jj_at,kk_at),idx_em_of_at{3}(ii_at,jj_at,kk_at)) ...
             + 1/number_em_of_at(idx_em_of_at{1}(ii_at,jj_at,kk_at),idx_em_of_at{2}(ii_at,jj_at,kk_at),idx_em_of_at{3}(ii_at,jj_at,kk_at)) ...
             * field_at(ii_at,jj_at,kk_at);
        end
      end
    end
  elseif (average_switch == 0)
    for ii_em=1:n_x_em
      for jj_em=1:n_y_em
        for kk_em=1:n_z_em
 %         if (number_em_of_at(ii_em,jj_em,kk_em) > 2)
            field_match_em(ii_em,jj_em,kk_em) = field_match_em(ii_em,jj_em,kk_em);
 %         elseif (number_em_of_at(ii_em,jj_em,kk_em) > 0)
            field_at_0 = field_at(idx_at_of_em{1}(ii_em,jj_em,kk_em), idx_at_of_em{2}(ii_em,jj_em,kk_em), idx_at_of_em{3}(ii_em,jj_em,kk_em));
            if ( (ii_em==1) | (ii_em==n_x_em) )
              field_at_x = 0;
            else
              field_at_x = field_at(idx_at_of_em{1}(ii_em,jj_em,kk_em)+step_at_of_em{1}(ii_em,jj_em,kk_em), idx_at_of_em{2}(ii_em,jj_em,kk_em), idx_at_of_em{3}(ii_em,jj_em,kk_em));
            end
            if ( (jj_em==1) | (jj_em==n_y_em) )
              field_at_y = 0;
            else
              field_at_y = field_at(idx_at_of_em{1}(ii_em,jj_em,kk_em), idx_at_of_em{2}(ii_em,jj_em,kk_em)+step_at_of_em{2}(ii_em,jj_em,kk_em), idx_at_of_em{3}(ii_em,jj_em,kk_em));
            end
            if ( (kk_em==1) | (kk_em==n_z_em) )
              field_at_z = 0;
            else
              field_at_z = field_at(idx_at_of_em{1}(ii_em,jj_em,kk_em), idx_at_of_em{2}(ii_em,jj_em,kk_em), idx_at_of_em{3}(ii_em,jj_em,kk_em)+step_at_of_em{3}(ii_em,jj_em,kk_em));
            end
            df_x       = (field_at_x-field_at_0)/dx_at * delta_at_of_em{1}(ii_em,jj_em,kk_em);
            df_y       = (field_at_y-field_at_0)/dy_at * delta_at_of_em{2}(ii_em,jj_em,kk_em);
            df_z       = (field_at_z-field_at_0)/dz_at * delta_at_of_em{3}(ii_em,jj_em,kk_em);
            field_em   = field_at_0 + df_x + df_y + df_z;
            field_match_em(ii_em,jj_em,kk_em) = field_em;
 %         end
        end
      end
    end
  end
