% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_ext_densities_em(calc_space_at, n_x_at, n_y_at, n_z_at, jx_at, jy_at, jz_at, rho_at, r_sp_mesh_at, t_st, contour_resolution, fontsize_em)

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plotting signal in 1D

    if ( (calc_space_at == 'x') | (calc_space_at == 'y') | (calc_space_at == 'z') )

      if (calc_space_at == 'x')
        for ii=1:n_x_at;
          plot_axis = r_sp_mesh_at{1};
          jx_plot(ii)   = jx_at(ii,1,1);
          jy_plot(ii)   = jy_at(ii,1,1);
          jz_plot(ii)   = jz_at(ii,1,1);
          rho_plot(ii)  = rho_at(ii,1,1);
        end
      elseif (calc_space_at == 'y')
        for jj=1:n_y_at;
          plot_axis = r_sp_mesh_at{2};
          jx_plot(jj)   = jx_at(1,jj,1);
          jy_plot(jj)   = jy_at(1,jj,1);
          jz_plot(jj)   = jz_at(1,jj,1);
          rho_plot(jj)  = rho_at(1,jj,1);
        end
      elseif (calc_space_at == 'z')
        for kk=1:n_z_at;
          plot_axis = r_sp_mesh_em{3};
          jx_plot(kk)   = jx_at(1,1,kk);
          jy_plot(kk)   = jy_at(1,1,kk);
          jz_plot(kk)   = jz_at(1,1,kk);
          rho_plot(kk)  = rho_at(1,1,kk);
        end
      end

      fig1=figure(1);
      set(gcf,'Visible', 'off'); 
  
      plot(plot_axis,jx_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/at_jx/jx_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/at_jx/jx_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/at_jx/jx_at_',num2str(t_st),'.png');
      end
      print(fig1,'-dpng',filename);
   
      plot(plot_axis,jy_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/at_jy/jy_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/at_jy/jy_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/at_jy/jy_at_',num2str(t_st),'.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,jz_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/at_jz/jz_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/at_jz/jz_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/at_jz/jz_at_',num2str(t_st),'.png');
      end
      print(fig1,'-dpng',filename);

      plot(plot_axis,rho_plot);
      if (calc_space_em == 'x')
        filename = strcat('./figures/1D_plots/graph/x_propagation/at_rho/rho_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'y')
        filename = strcat('./figures/1D_plots/graph/y_propagation/at_rho/rho_at_',num2str(t_st),'.png');
      elseif (calc_space_em == 'z')
        filename = strcat('./figures/1D_plots/graph/z_propagation/at_rho/rho_at_',num2str(t_st),'.png');
      end
      print(fig1,'-dpng',filename);

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % plotting signal in 2D

    if ( (calc_space_at == 'xy') | (calc_space_at == 'xz') | (calc_space_at == 'yz') )

      if (calc_space_at == 'xy')
        for ii=1:n_x_at
          for jj=1:n_y_at
            jx_plot(jj,ii)   = jx_at(ii,jj,1);
            jy_plot(jj,ii)   = jy_at(ii,jj,1);
            jz_plot(jj,ii)   = jz_at(ii,jj,1);
            rho_plot(jj,ii)  = rho_at(ii,jj,1);
          end
        end
      elseif (calc_space_at == 'xz')
        for ii=1:n_x_at 
          for kk=1:n_z_at
            jx_plot(kk,ii)   = jx_ext_at(ii,1,kk);
            jy_plot(kk,ii)   = jy_ext_at(ii,1,kk);
            jz_plot(kk,ii)   = jz_ext_at(ii,1,kk);
            rho_plot(kk,ii)  = rho_ext_at(ii,1,kk);
          end
        end
      elseif (calc_space_at == 'yz')
        for jj=1:n_y_at
          for kk=1:n_z_at
            jx_plot(kk,jj)   = jx_at(1,jj,kk);
            jy_plot(kk,jj)   = jy_at(1,jj,kk);
            jz_plot(kk,jj)   = jz_at(1,jj,kk);
            rho_plot(kk,jj)  = rho_at(1,jj,kk);
          end
        end
      end

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % contour plots    

      if (calc_space_at == 'xy')
        vec_1              = r_sp_mesh_at{1};
        vec_2              = r_sp_mesh_at{2};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'y [a.u]';
        vec_1_range        = [r_sp_mesh_at{1}(1) r_sp_mesh_at{1}(length(r_sp_mesh_at{1}))];
        vec_2_range        = [r_sp_mesh_at{2}(1) r_sp_mesh_at{2}(length(r_sp_mesh_at{2}))];
        file_name_jx       = strcat('./figures/2D_plots/contour/xy_propagation/at_jx/jx_at_',num2str(t_st),'.png';
        file_name_jy       = strcat('./figures/2D_plots/contour/xy_propagation/at_jy/jy_at_',num2str(t_st),'.png';
        file_name_jz       = strcat('./figures/2D_plots/contour/xy_propagation/at_jz/jz_at_',num2str(t_st),'.png';
        file_name_rho      = strcat('./figures/2D_plots/contour/xy_propagation/at_rho/rho_at_',num2str(t_st),'.png';
      elseif (calc_space_at == 'xz')
        vec_1              = r_sp_mesh_at{1};
        vec_2              = r_sp_mesh_at{3};
        vec_1_label        = 'x [a.u]';
        vec_2_label        = 'z [a.u]';
        vec_1_range        = [r_sp_mesh_at{1}(1) r_sp_mesh_at{1}(length(r_sp_mesh_at{1}))];
        vec_2_range        = [r_sp_mesh_at{3}(1) r_sp_mesh_at{3}(length(r_sp_mesh_at{3}))];
        file_name_jx       = './figures/2D_plots/contour/xz_propagation/at_jx/jx_at_',num2str(t_st),'.png';
        file_name_jy       = './figures/2D_plots/contour/xz_propagation/at_jy/jy_at_',num2str(t_st),'.png';
        file_name_jz       = './figures/2D_plots/contour/xz_propagation/at_jz/jz_at_',num2str(t_st),'.png';
        file_name_rho      = './figures/2D_plots/contour/xz_propagation/at_rho/rho_at_',num2str(t_st),'.png';
      elseif (calc_space_at == 'yz')
        vec_1           = r_sp_mesh_at{2};
        vec_2           = r_sp_mesh_at{3};
        vec_1_label     = 'y [a.u]';
        vec_2_label     = 'z [a.u]';
        vec_1_range     = [r_sp_mesh_at{2}(1) r_sp_mesh_at{2}(length(r_sp_mesh_at{2}))];
        vec_2_range     = [r_sp_mesh_at{3}(1) r_sp_mesh_at{3}(length(r_sp_mesh_at{3}))]; 
        file_name_jx    = './figures/2D_plots/contour/yz_propagation/at_jx/jx_at_',num2str(t_st),'.png';
        file_name_jy    = './figures/2D_plots/contour/yz_propagation/at_jy/jy_at_',num2str(t_st),'.png';
        file_name_jz    = './figures/2D_plots/contour/yz_propagation/at_jz/jz_at_',num2str(t_st),'.png';
        file_name_rho   = './figures/2D_plots/contour/yz_propagation/at_rho/rho_at_',num2str(t_st),'.png';
      end
      plot_matrix_jx  = jx_plot;
      plot_matrix_jy  = jy_plot;
      plot_matrix_jz  = jz_plot;
      plot_matrix_rho = rho_plot;

      plot_contour(vec_1, vec_2, plot_matrix_jx,  contour_resolution, filename_jx, 'current density', vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_jy,  contour_resolution, filename_jy, 'current density', vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_jz,  contour_resolution, filename_jz, 'current density', vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
      plot_contour(vec_1, vec_2, plot_matrix_rho, contour_resolution, filename_rho,'charge density',  vec_1_label, vec_2_label, vec_1_range, vec_2_range, fontsize_em);
 
    end

