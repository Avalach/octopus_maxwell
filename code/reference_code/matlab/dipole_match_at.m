% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [E_at] = dipole_match_at(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, E_em)

    if (n_x_em>1)
      x_idx_em = n_x_em/2+1;
    else
      x_idx_em = 1;
    end
 
    if (n_y_em>1)
      y_idx_em = n_y_em/2+1;
    else
      y_idx_em = 1;
    end
 
    if (n_z_em>1)
      z_idx_em = n_z_em/2+1;
    else
      z_idx_em = 1;
    end

    for ii_at=1:n_x_at
      for jj_at=1:n_y_at
        for kk_at=1:n_z_at
          E_at(ii_at,jj_at,kk_at) = E_em(x_idx_em, y_idx_em, z_idx_em);
        end
      end
    end
