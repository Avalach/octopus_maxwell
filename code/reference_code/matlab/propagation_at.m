% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Phi_at] = propagation_at(exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at, mask_at, Phi_at)

    Phi_at     = exp_time_ev_com_AB_at.*Phi_at;
    Phi_at     = exp_time_ev_B_at.*Phi_at;

    fft_Phi_at = fftn(Phi_at);
    fft_Phi_at = exp_time_ev_fft_A_at.*fft_Phi_at;
    Phi_at     = ifftn(fft_Phi_at);

    Phi_at     = mask_at.*Phi_at;

