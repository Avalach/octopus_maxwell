% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Phi_at] = initial_wave_function(calc_space_at, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, sigma_x_at, sigma_y_at, sigma_z_at, ...
                                                     signal_pos_x_at, signal_pos_y_at, signal_pos_z_at, wave_shape_at)


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initial wave function in 1D
    if ( (calc_space_at == 'x') | (calc_space_at == 'y') | (calc_space_at == 'z') )

      for ii=1:n_x_at
        for jj=1:n_y_at
          for kk=1:n_z_at
            if (calc_space_at == 'x')
              if (wave_shape_at == 1) 
                gauss_dist   = (2*pi)^(-1/4)/(sqrt(sigma_x_at)) * exp(-((r_sp_mesh_at{1}(ii)-signal_pos_x_at)^2)/(4*sigma_x_at^2));
                Phi_at(ii,jj,kk) = gauss_dist;
              elseif (wave_shape_at == 3)
                rr = abs(r_sp_mesh_at{1}(ii));
                H1 = 1/sqrt(pi) * exp(- rr);
                H2 = 1/(4*sqrt(2*pi)) * exp(- rr/2) * (2 - rr);
                Phi_at(ii,jj,kk) = 1/sqrt(2) * (H1 + H2);
              elseif (wave_shape_at == 4)
                rr = r_sp_mesh_at{1}(ii);
                test = exp(-rr^2) + i * exp(-2*rr^2);
                Phi_at(ii,jj,kk) = test;
              end
            elseif (calc_space_at == 'y')
              if (wave_shape_at == 1)
                gauss_dist = (2*pi)^(-1/4)/(sqrt(sigma_y_at)) * exp(-((r_sp_mesh_at{2}(jj)-signal_pos_y_at)^2)/(4*sigma_y_at^2));
                Phi_at(ii,jj,kk) = gauss_dist;
              elseif (wave_shape_at == 3)
                rr = r_sp_mesh_at{2}(jj);
                H1_H2 = 1/(2*sqrt(81+32*sqrt(2))) * exp(-rr) * (24-3*sqrt(2) * exp(rr/2) * (rr-2));
                Phi_at(ii,jj,kk) = H1_H2;
              end
            elseif (calc_space_at == 'z')
              if (wave_shape_at == 1)
                gauss_dist = (2*pi)^(-1/4)/(sqrt(sigma_z_at)) * exp(-((r_sp_mesh_at{3}(kk)-signal_pos_z_at)^2)/(4*sigma_z_at^2));
                Phi_at(ii,jj,kk) =                         gauss_dist;
              elseif (wave_shape_at == 3)
                rr = r_sp_mesh_at{3}(kk);
                H1_H2 = 1/(2*sqrt(81+32*sqrt(2))) * exp(-rr) * (24-3*sqrt(2) * exp(rr/2) * (rr-2));
                Phi_at(ii,jj,kk) = H1_H2;
              end
            end
          end
        end
      end

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initial wave function in 2D

    if ( (calc_space_at == 'xy') | (calc_space_at == 'xz') | (calc_space_at == 'yz') )

      for ii=1:n_x_at
        for jj=1:n_y_at
          for kk=1:n_z_at
            if (calc_space_at == 'xy')
              gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_at)) * exp(-((r_sp_mesh_at{1}(ii)-signal_pos_x_at)^2)/(4*sigma_x_at^2));
              gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_at)) * exp(-((r_sp_mesh_at{2}(jj)-signal_pos_y_at)^2)/(4*sigma_y_at^2)); 
              Phi_at(ii,jj,kk) = gauss_dist_x * gauss_dist_y ;
            elseif (calc_space_at == 'xz')
              gauss_dist_x = 2 *(pi)^(-1/4)/(sqrt(sigma_x_at)) * exp(-((r_sp_mesh_at{1}(ii)-signal_pos_x_at)^2)/(4*sigma_x_at^2));
              gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_at)) * exp(-((r_sp_mesh_at{3}(kk)-signal_pos_z_at)^2)/(4*sigma_z_at^2)); 
              Phi_at(ii,jj,kk) = gauss_dist_x * gauss_dist_z ;
            elseif (calc_space_at == 'yz')
              gauss_dist_y = 2 *(pi)^(-1/4)/(sqrt(sigma_y_at)) * exp(-((r_sp_mesh_at{2}(jj)-signal_pos_y_at)^2)/(4*sigma_y_at^2));
              gauss_dist_z = 2 *(pi)^(-1/4)/(sqrt(sigma_z_at)) * exp(-((r_sp_mesh_at{3}(kk)-signal_pos_z_at)^2)/(4*sigma_z_at^2)); 
              Phi_at(ii,jj,kk) = gauss_dist_y * gauss_dist_z ;
            end
          end
        end
      end

    end

