% Copyright (C) 2013 Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% starting program

clear;
more off;

warning('off');

fprintf('\n');
fprintf('Starting program ...\n\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load physical constants
% natural constants in atomic units

alpha              = 7.2973525698*10^(-3);
h                  = 1;
m_e                = 1;
q_e                = 1;
c_0                = 1/alpha;
ep_0               = 1/(4*pi);
mu_0               = 1/(c_0^2*ep_0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% general options


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% switch for the time watch

time_watch = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameter for the time evolution calculation

timesteps          = 500;
dt                 = 0.03;

prop_mode = 2;

pred_corr_method   = 1;  % 1 = predictor-corrector method over cross, 2 = predictor-corrector method with same 
pred_corr_steps    = 2;  % number of predictor corrector steps


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% options for atomic calculation


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% atomic calculation switch

% 1 -> propagation of the wave function is 'on'
% 2 -> propagation of the wave function is 'off'

calc_switch_at    = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculation space:

% 'x'   -> 1D x direction
% 'y'   -> 1D y direction
% 'z'   -> 1D z direction
% 'xy'  -> 2D xy plane
% 'xz'  -> 2D xz plane
% 'yz'  -> 2D yz plane
% 'xyz' -> 3D space

calc_space_at = 'z' ;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculation optins

gs_calculation_switch = 1;
gs_t_st_max_at        = 5000;
pot_const_at          = 1.0;
exp_approx_switch_at  = 0;
exp_approx_order_at   = 4;
gs_dt_at              = 0.003;
gs_check_steps_at     = 10;
gs_error_at           = 0.0001;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% position of the atom 
n_at                  = 1;
atom_pos{1}           = [0.0,0.0,0.0];
Z_at{1}               = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters of the atomic real space grid
boxlength_x_at     = 1;
boxlength_y_at     = 1;
boxlength_z_at     = 30.0;
n_x_at             = 1;
n_y_at             = 1;
n_z_at             = 100;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot options of the atomic wave function
plot_output_switch_at = 1;
contour_resolution_at = [-1.0:0.001:-0.08 , 0.08:0.001:1.0];
plot_step_at          = 1;
fontsize_at           = 14.5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% shape of the initial wave function

% 0 -> no initial wave function
% 1 -> gauss shape
% 2 -> S State Hydrogen shape
% 3 -> S and P State Hydrogen shape

wave_shape_at      = 1; 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters of the initial wave function

sigma_x_at         = 1;
sigma_y_at         = 1;
sigma_z_at         = 1;
signal_pos_x_at    = 0;
signal_pos_y_at    = 0;
signal_pos_z_at    = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% option for electromagnetic calculation


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% electromagnetic calculation switch

% 1 -> propagation of electromagnetic field is 'on'
% 0 -> propagation of electromagnetic field is 'off'
calc_switch_em    = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% spinor calculation mode

% 1 -> free electromagnetic propagation in vacuum without atomic charge and current densities
% 2 -> free electromagentic propagation in vacuum cuppled to atomic charge and current densities
% 3 -> electromagnetic propagation with classical medium without atomic charge and current densities
% 4 -> electromagnetic propagation with classical medium cuppled to atomic charge and current densities

spinor_calc_mode  = 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculation space:

% 'x'   -> 1D x direction
% 'y'   -> 1D y direction
% 'z'   -> 1D z direction
% 'xy'  -> 2D xy plane
% 'xz'  -> 2D xz plane
% 'yz'  -> 2D yz plane% 'xyz' -> 3D space

calc_space_em = 'x' ;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters of the electromagnetic real space grid

boxlength_x_em     = 1000;
boxlength_y_em     = 1;
boxlength_z_em     = 1;
n_x_em             = 400;
n_y_em             = 1;
n_z_em             = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculation options for electromagnetic propagation

exp_approx_switch_em        = 1;
exp_approx_order_em         = 4;
load_medium_image_switch_em = 0;
medium_shape_em             = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% elelctromagnetic medium parameters

mu_m               = mu_0;
ep_m               = ep_0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% plot options for electromagnetic fields

plot_output_switch_em = 1;
contour_resolution_em = [-1.0:0.001:-0.08 , 0.08:0.001:1.0] ;
plot_step_em          =  1;
fontsize_em           =  14.5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% shape of the initial electromagnetic signal

% 0 -> no initial electromagnetic wave
% 1 -> sinuse shape
% 2 -> gauss shape
% 3 -> sinus shape and gauss envelope

wave_shape_em      = 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters of the initial electromagnetic signal

sigma_x_em         = 30;
sigma_y_em         = 5;
sigma_z_em         = 6;
k_x_em             = 0.10;
k_y_em             = 0;
k_z_em             = 0;
signal_pos_x_em    = -250.0;
signal_pos_y_em    = 0;
signal_pos_z_em    = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% shape of the external electromagnetic densities

% 0 -> densitiy is equal to zeros in the whole calculation box
% 1 -> constant value after a certain time step
% 2 -> gauss shape signal in time with a maximum at certain time step

j_shape_em   = 0;
rho_shape_em = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameter for the external electromagnetic densities

j_t_st_0_em         = 500;
j_sigma_t_em        = 100;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Starting calculation


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% system calculated parameters

dx_em        = double(boxlength_x_em)/double(n_x_em);
dy_em        = double(boxlength_y_em)/double(n_y_em);
dz_em        = double(boxlength_z_em)/double(n_z_em);
dx_at        = double(boxlength_x_at)/double(n_x_at);
dy_at        = double(boxlength_y_at)/double(n_y_at);
dz_at        = double(boxlength_z_at)/double(n_z_at);

min_x_pnt_em = -double(boxlength_x_em)/2+dx_em/2; 
min_y_pnt_em = -double(boxlength_y_em)/2+dy_em/2;
min_z_pnt_em = -double(boxlength_z_em)/2+dz_em/2;

max_x_pnt_em =  double(boxlength_x_em)/2-dx_em/2;
max_y_pnt_em =  double(boxlength_y_em)/2-dy_em/2;
max_z_pnt_em =  double(boxlength_z_em)/2-dz_em/2;

min_x_pnt_at = -double(boxlength_x_at)/2+dx_at/2; 
min_y_pnt_at = -double(boxlength_y_at)/2+dy_at/2;
min_z_pnt_at = -double(boxlength_z_at)/2+dz_at/2;

max_x_pnt_at =  double(boxlength_x_at)/2-dx_at/2;
max_y_pnt_at =  double(boxlength_y_at)/2-dy_at/2;
max_z_pnt_at =  double(boxlength_z_at)/2-dz_at/2;

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup of figures

set(0,'Units','pixels') 
scnsize = get(0,'ScreenSize');
scnsize(3) = scnsize(3)*0.96;
fig1=figure(1);
set(gcf,'Visible', 'off'); 
clf
fig2=figure(2);
set(gcf,'Visible', 'off'); 
clf
fig3=figure(3);
set(gcf,'Visible', 'off'); 
clf
fig4=figure(4);
set(gcf,'Visible', 'off'); 
clf
fig5=figure(5);
set(gcf,'Visible', 'off'); 
clf
fig6=figure(6);
set(gcf,'Visible', 'off');
clf
fig7=figure(7);
set(gcf,'Visible', 'off');
clf

position = get(fig1,'Position');
outerpos = get(fig1,'OuterPosition');
border = outerpos-position;
posshift = 50;

pos1 = [0,...
        scnsize(4)*(1/4),...
        scnsize(3)*(1/3)-border(4)/6,...
        scnsize(4)/2];
pos2 = [scnsize(3)*(1/3)+posshift,...
        scnsize(4)/2-5*border(3),...
        pos1(3),...
        pos1(4)-border(4)/4];
pos3 = [pos2(1),...
        5*border(3),...
        pos1(3),...
        pos2(4)];
pos4 = [scnsize(3)*(2/3)+posshift,...
        pos2(2),...
        pos1(3),...
        pos2(4)];
pos5 = [pos4(1),...
        pos3(2),...
        pos1(3),...
        pos2(4)];
set(fig1,'OuterPosition',pos1); 
set(fig2,'OuterPosition',pos2);
set(fig3,'OuterPosition',pos3);
set(fig4,'OuterPosition',pos4);
set(fig5,'OuterPosition',pos5);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup real-space mesh

fprintf('Setting up real-space mesh ...\n\n');

r_sp_mesh_em{1} = min_x_pnt_em:dx_em:max_x_pnt_em;
r_sp_mesh_em{2} = min_y_pnt_em:dy_em:max_y_pnt_em;
r_sp_mesh_em{3} = min_z_pnt_em:dz_em:max_z_pnt_em;

r_sp_mesh_at{1} = min_x_pnt_at:dx_at:max_x_pnt_at;
r_sp_mesh_at{2} = min_y_pnt_at:dy_at:max_y_pnt_at;
r_sp_mesh_at{3} = min_z_pnt_at:dz_at:max_z_pnt_at;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup momentum-space mesh

fprintf('Setting up momentum-space mesh ...\n\n');

[m_sp_mesh_em] = momentum_grid(boxlength_x_em, boxlength_y_em, boxlength_z_em, n_x_em, n_y_em, n_z_em, r_sp_mesh_em);
[m_sp_mesh_at] = momentum_grid(boxlength_x_at, boxlength_y_at, boxlength_z_at, n_x_at, n_y_at, n_z_at, r_sp_mesh_at);
                                                                                                      

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup mu and epsilon arrays on the mesh

fprintf('Setting up mu and epsilon values of the mesh ...\n\n');

[ep_tensor, mu_tensor, v_tensor, h_tensor] ...
    = classical_medium(calc_space_em, spinor_calc_mode, load_medium_image_switch_em, medium_shape_em, ep_0, ep_m, mu_0, mu_m, ...
      n_x_em, n_y_em, n_z_em, r_sp_mesh_em, plot_output_switch_em);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial electromagnetic laser

fprintf('Setting up electromagnetic field ...\n\n');

[Ex_laser_em, Ey_laser_em, Ez_laser_em, Bx_laser_em, By_laser_em, Bz_laser_em] = initial_laser_em(calc_space_em, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, k_x_em, k_y_em, k_z_em, ...
                                                                                 sigma_x_em, sigma_y_em, sigma_z_em, signal_pos_x_em, signal_pos_y_em, signal_pos_z_em, ...
                                                                                 wave_shape_em, v_tensor);

Ex_em = Ex_laser_em;
Ey_em = Ey_laser_em;
Ez_em = Ez_laser_em;
Bx_em = Bx_laser_em;
By_em = By_laser_em;
Bz_em = Bz_laser_em;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial electromagnetic densities

fprintf('Setting up external electromagnetic densities ...\n\n');

[jx_ext_em, jy_ext_em, jz_ext_em, rho_ext_em] = external_em_densities(calc_space_em, n_x_em, n_y_em, n_z_em, 0, j_t_st_0_em, j_sigma_t_em, j_shape_em);
jx_em  = jx_ext_em;
jy_em  = jy_ext_em;
jz_em  = jz_ext_em;
rho_em = rho_ext_em;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial wave function

fprintf('Setting up wave function ...\n\n');

[Phi_at] = initial_wave_function(calc_space_at, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, ...
           sigma_x_at, sigma_y_at, sigma_z_at, signal_pos_x_at, signal_pos_y_at, signal_pos_z_at, wave_shape_at);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Charge and current densities

fprintf('calculation of initial current and charge densities ... \n\n');

[jx_at, jy_at, jz_at, rho_at] = current_charge_densities_at(calc_space_at, Phi_at, n_x_at, n_y_at, n_z_at, m_sp_mesh_at, m_e, q_e, h);

% rho_at = zeros(n_x_at,n_y_at,n_z_at);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization of the Riemann-Silberstein spinor

fprintf('Setting up Riemann-Silberstein spinor ...\n\n');

[Phi_em] = spinor_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, Ex_em, Ey_em, Ez_em, Bx_em, By_em, Bz_em, ep_tensor, mu_tensor);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialization of the Riemann-Silberstein density spinor

if ( (spinor_calc_mode == 2) | (spinor_calc_mode == 4) )

  fprintf('Setting up Riemann-Silberstein density spinor ...\n\n');

  [dens_spinor_em] = density_spinor_em(spinor_calc_mode, jx_ext_em, jy_ext_em, jz_ext_em, rho_ext_em, ep_tensor, v_tensor, n_x_em, n_y_em, n_z_em);

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculation of exp_time_ev_fft_A_em, exp_time_ev_B_em, and exp_time_ev_com_AB_em

if (spinor_calc_mode == 1)
  [exp_time_ev_fft_A_em] = split_operator_propagator_3x3_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, dt, c_0, h);
   exp_time_ev_B_em = 0;
   exp_time_ev_com_AB_em = 0;
   exp_time_ev_fft_A_matrix_em = 0;
   map_qq_em = 0;
   map_ii_em = 0;
   map_jj_em = 0;
   map_kk_em = 0;
elseif (spinor_calc_mode == 2)
  if (prop_mode == 1)
    [exp_time_ev_fft_A_em] = split_operator_propagator_4x4_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, dt, c_0, h);
    [exp_time_ev_fft_A_minus_em] = split_operator_propagator_4x4_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, (-dt), c_0, h);
  elseif (prop_mode == 2)
    [exp_time_ev_fft_A_em] = split_operator_propagator_4x4_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, dt/2, c_0, h);
    [exp_time_ev_fft_A_minus_em] = split_operator_propagator_4x4_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, (-dt/2), c_0, h);
  end
   exp_time_ev_B_em = 0;
   exp_time_ev_com_AB_em = 0;
   exp_time_ev_fft_A_matrix_em = 0;
   map_qq_em = 0;
   map_ii_em = 0;
   map_jj_em = 0;
   map_kk_em = 0;
elseif (spinor_calc_mode == 3)
  [exp_time_ev_fft_A_em, exp_time_ev_B_em, exp_time_ev_com_AB_em, exp_time_ev_fft_A_matrix_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em] ...
      = split_operator_propagator_6x6_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, dt, v_tensor, h_tensor, exp_approx_switch_em, exp_approx_order_em, h);
elseif (spinor_calc_mode == 4)
  [exp_time_ev_fft_A_em, exp_time_ev_B_em, exp_time_ev_com_AB_em, exp_time_ev_fft_A_matrix_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em] ...
      = split_operator_propagator_8x8_em(n_x_em, n_y_em, n_z_em, m_sp_mesh_em, dt, v_tensor, h_tensor, exp_approx_switch_em, exp_approx_order_em, h);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup mask function for photons

[mask_em] = mask_function_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup mask function for the wave function

[mask_at] = mask_function_at(n_x_at, n_y_at, n_z_at);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Potential of the Hamiltonian

[potential_at] = hamiltonian_potential_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, q_e, Z_at, h, pot_const_at);

if (plot_output_switch_at == 1)
  plot_potential(calc_space_at, n_x_at, n_y_at, n_z_at, potential_at, r_sp_mesh_at, contour_resolution_at, fontsize_at);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculation of exp_time_ev_fft_A_at, exp_time_ev_B_at, and exp_time_ev_com_AB_at with imaginary time -i*dt

[exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at] ...
    = split_operator_propagator_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, m_sp_mesh_at, ...
      exp_approx_switch_at, exp_approx_order_at, (-i*gs_dt_at), m_e, q_e, Z_at, h, pot_const_at);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initial electric field at atomic scale

Ex_at = zeros(n_x_at,n_y_at,n_z_at);
Ey_at = zeros(n_x_at,n_y_at,n_z_at);
Ez_at = zeros(n_x_at,n_y_at,n_z_at);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculating groundstate wave function by imaginary time evolution of the initial wave function

if (calc_switch_at == 1)
  if (gs_calculation_switch == 1)

    [Phi_at] = ground_state_evolution(calc_space_at, Phi_at, n_x_at, n_y_at, n_z_at, gs_t_st_max_at, exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at, ...
                                      gs_check_steps_at, gs_error_at, dx_at, dy_at, dz_at, mask_at, plot_output_switch_at, r_sp_mesh_at, contour_resolution_at, fontsize_at);

  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculation of exp_time_ev_fft_A_at, exp_time_ev_B_at, and exp_time_ev_com_AB_at 

if (prop_mode == 1) 
  [exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at] ...
        = split_operator_propagator_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, m_sp_mesh_at, ...
          exp_approx_switch_at, exp_approx_order_at, dt, m_e, q_e, Z_at, h, pot_const_at);
elseif (prop_mode == 2)
  [exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at] ...
        = split_operator_propagator_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, m_sp_mesh_at, ...
          exp_approx_switch_at, exp_approx_order_at, dt/2, m_e, q_e, Z_at, h, pot_const_at);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mapping for multiscaling

[idx_at_of_em, idx_em_of_at, delta_at_of_em, delta_em_of_at, step_at_of_em, step_em_of_at, number_at_of_em, number_em_of_at] = ...
     multiscale_mapping(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, r_sp_mesh_em, r_sp_mesh_at, dx_em, dy_em, dz_em, dx_at, dy_at, dz_at);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting outputs of the initial situation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting inital electromagnetic signal

fprintf('Plotting electromagnetic functions ...\n\n');

if (plot_output_switch_em == 1)
  plot_em_field(calc_space_em, n_x_em, n_y_em, n_z_em, Ex_em, Ey_em, Ez_em, Bx_em, By_em, Bz_em, 0, r_sp_mesh_em, contour_resolution_em, fontsize_em);
  plot_medium_em(calc_space_em, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, ep_tensor, mu_tensor, h_tensor, v_tensor, contour_resolution_em, fontsize_em);
  plot_ext_densities_em(calc_space_em, n_x_em, n_y_em, n_z_em, jx_ext_em, jy_ext_em, jz_ext_em, rho_ext_em, r_sp_mesh_em, 0, contour_resolution_em, fontsize_em);
  plot_densities_em(calc_space_em, n_x_em, n_y_em, n_z_em, jx_em, jy_em, jz_em, rho_em, r_sp_mesh_em, 0, contour_resolution_em, fontsize_em);
end
%write_h5_file_em_field(calc_space_em, n_x_em, n_y_em, n_z_em, Ex_em, Ey_em, Ez_em, Bx_em, By_em, Bz_em, 0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting initial wave function

fprintf('Plotting atomic functions ...\n\n');

if (plot_output_switch_at == 1)
  Phi_at_abs_square = conj(Phi_at).*Phi_at;
  plot_at(calc_space_at, n_x_at, n_y_at, n_z_at, Phi_at_abs_square, 0, r_sp_mesh_at, contour_resolution_at, fontsize_at);
  plot_densities_at(calc_space_at, n_x_at, n_y_at, n_z_at, jx_at, jy_at, jz_at, rho_at, r_sp_mesh_at, 0, contour_resolution_at, fontsize_at);
end



 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time evolution

fprintf('Schroedinger and electromagnetic field propagation ...\n\n\n\n');

for t_st=1:timesteps

  if (time_watch ==1) tic; end

  if (mod(t_st,10) == 0)
    steps_string = int2str(t_st);
    combinedStr = strcat('Timesteps done:    ',steps_string,' ...\n');
    fprintf(combinedStr)
  end

  % initialization prediction time steps by current values
  Ex_old_em      = Ex_em;
  Ex_new_em      = Ex_em;
  Ey_old_em      = Ey_em;
  Ey_new_em      = Ey_em;
  Ez_old_em      = Ez_em;
  Ez_new_em      = Ez_em;

  Bx_old_em      = Bx_em;
  Bx_new_em      = Bx_em;
  By_old_em      = By_em;
  By_new_em      = By_em;
  Bz_old_em      = Bz_em;
  Bz_new_em      = Bz_em;

  jx_old_at      = jx_at;
  jx_new_at      = jx_at;
  jy_old_at      = jy_at;
  jy_new_at      = jy_at;
  jz_old_at      = jz_at;
  jz_new_at      = jz_at;

  rho_old_at     = rho_at;
  rho_new_at     = rho_at;

  jx_old_em      = jx_em;
  jx_new_em      = jx_em;
  jy_old_em      = jy_em;
  jy_new_em      = jy_em;
  jz_old_em      = jz_em;
  jz_new_em      = jz_em;

  rho_old_em     = rho_em;
  rho_new_em     = rho_em;

  jx_ext_old_em  = jx_ext_em;
  jx_ext_new_em  = jx_ext_em;
  jy_ext_old_em  = jy_ext_em;
  jy_ext_new_em  = jy_ext_em;
  jz_ext_old_em  = jz_ext_em;
  jz_ext_new_em  = jz_ext_em;

  rho_ext_old_em = rho_ext_em;
  rho_ext_new_em = rho_ext_em;
  
  Phi_old_at = Phi_at;
  Phi_new_at = Phi_at;
  Phi_old_em = Phi_em;
  Phi_new_em = Phi_em;

  for pred_corr=1:pred_corr_steps

    Phi_new_1_at = Phi_new_at;
    Ex_new_1_em = Ex_new_em;
    Ey_new_1_em = Ey_new_em;
    Ez_new_1_em = Ez_new_em;
    Bx_new_1_em = Bx_new_em;
    By_new_1_em = By_new_em;
    Bz_new_1_em = Bz_new_em;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % atomic propagation

    % calculation of atomic wave function

    [Ex_old_at] = dipole_match_at(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, Ex_old_em);
    [Ex_new_at] = dipole_match_at(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, Ex_new_em);
    [Ey_old_at] = dipole_match_at(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, Ey_old_em);
    [Ey_new_at] = dipole_match_at(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, Ey_new_em);
    [Ez_old_at] = dipole_match_at(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, Ez_old_em);
    [Ez_new_at] = dipole_match_at(n_x_at, n_y_at, n_z_at, n_x_em, n_y_em, n_z_em, Ez_new_em);

    if (prop_mode == 1)
      % calculation of the exp_time_ev_B_at with coupled schroedinger maxwell dipole approximation
      [exp_time_ev_B_at] = split_operator_dipole_mod_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, Z_at, pot_const_at, ...
                                                        Ex_old_at, Ex_new_at, Ey_old_at, Ey_new_at, Ez_old_at, Ez_new_at, dt, q_e, h);
      % time evolution of the atomic wave function after one time step
      [Phi_new_at] = propagation_at(exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at, mask_at, Phi_old_at);
    elseif (prop_mode == 2)
      % calculation of the exp_time_ev_B_at with coupled schroedinger maxwell dipole approximation with old field
      [exp_time_ev_B_at] = split_operator_dipole_mod_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, Z_at, pot_const_at, ...
                                                        Ex_old_at, Ex_old_at, Ey_old_at, Ey_old_at, Ez_old_at, Ez_old_at, dt/2, q_e, h);
      % time evolution of the atomic wave function after one half time step                                                   
      [Phi_new_at] = propagation_at(exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at, mask_at, Phi_old_at);
      % calculation of the exp_time_ev_B_at with coupled schroedinger maxwell dipole approximation with new field
      [exp_time_ev_B_at] = split_operator_dipole_mod_at(n_at, atom_pos, n_x_at, n_y_at, n_z_at, r_sp_mesh_at, Z_at, pot_const_at, ...
                                                        Ex_new_at, Ex_new_at, Ey_new_at, Ey_new_at, Ez_new_at, Ez_new_at, dt/2, q_e, h);
      % time evolution of the atomic wave function after second half time step
      [Phi_new_at] = propagation_at(exp_time_ev_fft_A_at, exp_time_ev_B_at, exp_time_ev_com_AB_at, mask_at, Phi_new_at);
    end

    % corrected current and charge densities if predictor-corrector method is 1.
    if (pred_corr_method == 1)
      % current and charge densities after time evolution
      [jx_new_at, jy_new_at, jz_new_at, rho_new_at] = current_charge_densities_at(calc_space_at, Phi_new_at, n_x_at, n_y_at, n_z_at, m_sp_mesh_at, m_e, q_e, h);
    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Maxwell propagation

    % calculation of external current and charge densities
    [jx_ext_em, jy_ext_em, jz_ext_em, rho_ext_em] = external_em_densities(calc_space_em, n_x_em, n_y_em, n_z_em, t_st, j_t_st_0_em, j_sigma_t_em, j_shape_em);

    [jx_old_em]  = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, jx_old_em, jx_old_at);
    [jx_new_em]  = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, jx_new_em, jx_new_at);
    [jy_old_em]  = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, jy_old_em, jy_old_at);
    [jy_new_em]  = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, jy_new_em, jy_new_at);
    [jz_old_em]  = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, jz_old_em, jz_old_at);
    [jz_new_em]  = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, jz_new_em, jz_new_at);
    [rho_old_em] = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, rho_old_em, rho_old_at);
    [rho_new_em] = dipole_match_em(n_x_em, n_y_em, n_z_em, n_x_at, n_y_at, n_z_at, rho_new_em, rho_new_at);

    if (prop_mode == 1)
      [Phi_new_em] = propagation_inhom_2_em(calc_space_em, spinor_calc_mode, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_minus_em, ...
                                          exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, ...
                                          exp_time_ev_com_AB_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, jx_ext_old_em, jx_ext_new_em, jy_ext_old_em, jy_ext_new_em, ...
                                          jz_ext_old_em, jz_ext_new_em, rho_ext_old_em, rho_ext_new_em, jx_old_em, jx_new_em, jy_old_em, jy_new_em, jz_old_em, jz_new_em, ...
                                          rho_old_em, rho_new_em, Phi_old_em, ep_tensor, v_tensor, dt, m_e, q_e, h);
    elseif (prop_mode == 2)
      [Phi_new_em] = propagation_inhom_2_em(calc_space_em, spinor_calc_mode, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_minus_em, ...
                                          exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, ...
                                          exp_time_ev_com_AB_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, jx_ext_old_em, jx_ext_old_em, jy_ext_old_em, jy_ext_old_em, ...
                                          jz_ext_old_em, jz_ext_old_em, rho_ext_old_em, rho_ext_old_em, jx_old_em, jx_old_em, jy_old_em, jy_old_em, jz_old_em, jz_old_em, ...
                                          rho_old_em, rho_old_em, Phi_old_em, ep_tensor, v_tensor, dt/2, m_e, q_e, h);
      [Phi_new_em] = propagation_inhom_2_em(calc_space_em, spinor_calc_mode, n_x_em, n_y_em, n_z_em, r_sp_mesh_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_minus_em, ...
                                          exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, ...
                                          exp_time_ev_com_AB_em, map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, jx_ext_new_em, jx_ext_new_em, jy_ext_new_em, jy_ext_new_em, ...
                                          jz_ext_new_em, jz_ext_new_em, rho_ext_new_em, rho_ext_new_em, jx_new_em, jx_new_em, jy_new_em, jy_new_em, jz_new_em, jz_new_em, ...
                                          rho_new_em, rho_new_em, Phi_new_em, ep_tensor, v_tensor, dt/2, m_e, q_e, h);
    end

    % corrected current and charge densities if predictor-corrector method is 2.
    if (pred_corr_method == 2)
      % current and charge densities after time evolution
      [jx_new_at, jy_new_at, jz_new_at, rho_new_at] = current_charge_densities_at(calc_space_at, Phi_new_at, n_x_at, n_y_at, n_z_at, m_sp_mesh_at, m_e, q_e, h);
    end

    [Ex_new_em, Ey_new_em, Ez_new_em, Bx_new_em, By_new_em, Bz_new_em] = electromagnetic_field_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, ep_tensor, mu_tensor, Phi_new_em);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % predictor relative variance

    Ex_em  = Ex_new_em;
    Ey_em  = Ey_new_em;
    Ez_em  = Ez_new_em;
    Bx_em  = Bx_new_em;
    By_em  = By_new_em;
    Bz_em  = Bz_new_em;
    Phi_em = Phi_new_em;
    Phi_at = Phi_new_at;

    Phi_new_2_at = Phi_new_at;
    Ex_new_2_em = Ex_new_em;
    Ey_new_2_em = Ey_new_em;
    Ez_new_2_em = Ez_new_em;
    Bx_new_2_em = Bx_new_em;
    By_new_2_em = By_new_em;
    Bz_new_2_em = Bz_new_em;

    for ii=1:n_x_em
      for jj=1:n_y_em
        for kk=1:n_z_em
          Ex_var_em(ii,jj,kk) = (Ex_new_2_em(ii,jj,kk)-Ex_new_1_em(ii,jj,kk))^2;
          Ey_var_em(ii,jj,kk) = (Ey_new_2_em(ii,jj,kk)-Ey_new_1_em(ii,jj,kk))^2;
          Ez_var_em(ii,jj,kk) = (Ez_new_2_em(ii,jj,kk)-Ez_new_1_em(ii,jj,kk))^2;
          Bx_var_em(ii,jj,kk) = (Bx_new_2_em(ii,jj,kk)-Bx_new_1_em(ii,jj,kk))^2;
          By_var_em(ii,jj,kk) = (By_new_2_em(ii,jj,kk)-By_new_1_em(ii,jj,kk))^2;
          Bz_var_em(ii,jj,kk) = (Bz_new_2_em(ii,jj,kk)-Bz_new_1_em(ii,jj,kk))^2;
        end
      end
    end
    for ii=1:n_x_at
      for jj=1:n_y_at
        for kk=1:n_z_at
          Phi_rel_var_at(ii,jj,kk) = abs((Phi_new_2_at(ii,jj,kk)-Phi_new_1_at(ii,jj,kk)))^2;
        end
      end
    end

    Phi_rel_var_tot_em = sum(Ex_var_em) + sum(Ey_var_em) + sum(Ez_var_em) + sum(Bx_var_em) + sum(By_var_em) + sum(Bz_var_em);
    Phi_rel_var_tot_at = sum( Phi_rel_var_at );

    if (pred_corr > 1) 
      Phi_rel_var_tot_array_em(t_st,pred_corr-1) = Phi_rel_var_tot_em;
      Phi_rel_var_tot_array_at(t_st,pred_corr-1) = Phi_rel_var_tot_at;
    end
    Ez_pred_corr_em(pred_corr)  = Ez_em(n_x_em/2+1,1,1);
    Phi_pred_corr_at(pred_corr) = abs(Phi_at(1,1,n_z_at/2+1))^2;
    jz_old_pred_corr_em(pred_corr) = jz_old_em(n_x_em/2+1,1,1);
    jz_new_pred_corr_em(pred_corr) = jz_new_em(n_x_em/2+1,1,1);
    Ez_old_pred_corr_em(pred_corr) = Ez_old_em(n_x_em/2+1,1,1);
    Ez_new_pred_corr_em(pred_corr) = Ez_new_em(n_x_em/2+1,1,1);

 %  fprintf('for %d Phi_rel_var_tot_em = %8.10f \n', pred_corr, Phi_rel_var_tot_em)
 %  fprintf('for %d Phi_rel_var_tot_at = %8.10f \n', pred_corr, Phi_rel_var_tot_at)

  end

%  fig1=figure(2);
%  set(gcf, 'Visible', 'on');
%  plot(Ez_pred_corr_em)

%  fig1=figure(3);
%  set(gcf, 'Visible', 'on');
%  plot(Phi_pred_corr_at)

%  fig1=figure(4);
%  set(gcf, 'Visible', 'on');
%  plot(jz_old_pred_corr_em)

%  fig1=figure(5);
%  set(gcf, 'Visible', 'on');
%  plot(jz_new_pred_corr_em)

%  fig1=figure(6);
%  set(gcf, 'Visible', 'on');
%  plot(Ez_old_pred_corr_em)

%  fig1=figure(7);
%  set(gcf, 'Visible', 'on');
%  plot(Ez_new_pred_corr_em)

  Phi_at = Phi_new_at;
  jx_at  = jx_new_at;
  jy_at  = jy_new_at;
  jz_at  = jz_new_at;
  rho_at = rho_new_at;
  
  Phi_em = Phi_new_em;
  jx_em  = jx_new_em;
  jy_em  = jy_new_em;
  jz_em  = jz_new_em;
  rho_em = rho_new_em;

  Phi_abs_square_at = conj(Phi_at).*Phi_at;

  if (mod(t_st,plot_step_at) == 0)

    fprintf('Plotting wave function ...\n');

    if (plot_output_switch_at == 1)
      plot_at(calc_space_at, n_x_at, n_y_at, n_z_at, Phi_abs_square_at, t_st, r_sp_mesh_at, contour_resolution_at, fontsize_at);
      plot_densities_at(calc_space_at, n_x_at, n_y_at, n_z_at, jx_at, jy_at, jz_at, rho_at, r_sp_mesh_at, t_st, contour_resolution_at, fontsize_at);
    end

  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % calculation of E-field and B-field afer every timestep

  [Ex_em, Ey_em, Ez_em, Bx_em, By_em, Bz_em] = electromagnetic_field_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, ep_tensor, mu_tensor, Phi_em);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % calculation of total time and the EM field energy for each time step and plot

  time_array(t_st)   = double(t_st)*dt;
    
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % plotting E field and B field for 1D calculation

  if (mod(t_st,plot_step_em) == 0)

    fprintf('Plotting electromagnetic field ...\n');

    if (plot_output_switch_em == 1)
      plot_em_field(calc_space_em, n_x_em, n_y_em, n_z_em, Ex_em, Ey_em, Ez_em, Bx_em, By_em, Bz_em, t_st, r_sp_mesh_em, contour_resolution_em);
      plot_ext_densities_em(calc_space_em, n_x_em, n_y_em, n_z_em, jx_ext_em, jy_ext_em, jz_ext_em, rho_ext_em, r_sp_mesh_em, t_st, contour_resolution_em, fontsize_em);
      plot_densities_em(calc_space_em, n_x_em, n_y_em, n_z_em, jx_em, jy_em, jz_em, rho_em, r_sp_mesh_em, t_st, contour_resolution_em, fontsize_em);
    end

 %   write_h5_file_em_field(calc_space_em, n_x_em, n_y_em, n_z_em, Ex_em, Ey_em, Ez_em, Bx_em, By_em, Bz_em, t_st);
  
  end

end

if (pred_corr_method == 1)
  save './output/Phi_rel_var_tot_method_1_array_em.dat' Phi_rel_var_tot_array_em -ascii;
  save './output/Phi_rel_var_tot_method_1_array_at.dat' Phi_rel_var_tot_array_at -ascii;
elseif (pred_corr_method == 2)
  save './output/Phi_rel_var_tot_method_2_array_em.dat' Phi_rel_var_tot_array_em -ascii;
  save './output/Phi_rel_var_tot_method_2_array_at.dat' Phi_rel_var_tot_array_at -ascii;
end





