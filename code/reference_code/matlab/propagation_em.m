% Copyright (C) 2013 - Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Phi_em] = propagation_em(spinor_calc_mode, n_x_em, n_y_em, n_z_em, exp_time_ev_fft_A_em, exp_time_ev_fft_A_matrix_em, exp_time_ev_B_em, exp_time_ev_com_AB_em,  ...
                                   map_qq_em, map_ii_em, map_jj_em, map_kk_em, mask_em, Phi_em)


    if (spinor_calc_mode == 1)
      pp_qq_limit = 3;
    elseif (spinor_calc_mode == 2)
      pp_qq_limit = 4;
    elseif (spinor_calc_mode == 3)
      pp_qq_limit = 6;
    elseif (spinor_calc_mode == 4)
      pp_qq_limit = 8;
    end

    if ( (spinor_calc_mode == 3) | ( spinor_calc_mode == 4) )

      % multiply with matrix exp_time_ev_com_AB
      for pp=1:pp_qq_limit
        tmp_Phi_em{pp} = zeros(n_x_em,n_y_em,n_z_em);
        for qq=1:pp_qq_limit
          tmp_Phi_em{pp} = tmp_Phi_em{pp} + exp_time_ev_com_AB_em{pp}{qq}.*Phi_em{qq};
        end
      end
      for pp=1:pp_qq_limit
        Phi_em{pp} = tmp_Phi_em{pp};
      end
  
      % multiply with matrix exp_time_ev_B
      for pp=1:pp_qq_limit
        tmp_Phi_em{pp} = zeros(n_x_em,n_y_em,n_z_em);
        for qq=1:pp_qq_limit
          tmp_Phi_em{pp} = tmp_Phi_em{pp} + exp_time_ev_B_em{pp}{qq}.*Phi_em{qq};
        end
      end
      for pp=1:pp_qq_limit
        Phi_em{pp} = tmp_Phi_em{pp};
      end
  
      % forward Fourier transformation of Phi_em 
      for pp=1:pp_qq_limit
        fft_Phi_em{pp} = 1/sqrt(n_x_em) * 1/sqrt(n_y_em) * 1/sqrt(n_z_em) * fftn(Phi_em{pp});
      end

      idx = 0;
      for qq=1:pp_qq_limit
        for ii=1:n_x_em
          for jj=1:n_y_em
            for kk=1:n_z_em
              idx = idx+1;
              fft_Phi_em_vector(idx,1) = fft_Phi_em{qq}(ii,jj,kk);
            end
          end
        end
      end

      fft_Phi_em_vector = exp_time_ev_fft_A_matrix_em * fft_Phi_em_vector;
  
      for idx=1:pp_qq_limit*n_x_em*n_y_em*n_z_em
        tmp_Phi_em{map_qq_em(idx)}(map_ii_em(idx),map_jj_em(idx),map_kk_em(idx)) = fft_Phi_em_vector(idx,1);
      end
      
      for pp=1:pp_qq_limit
        fft_Phi_em{pp} = tmp_Phi_em{pp};
      end

      % backward Fourier transformation of fft_Phi_em
      for pp=1:pp_qq_limit
        Phi_em{pp} = ifftn(fft_Phi_em{pp});
      end

      % multiply with mask function
      for pp=1:pp_qq_limit
        Phi_em{pp} = mask_em{pp}.*Phi_em{pp};
      end

    elseif ( (spinor_calc_mode == 1) | ( spinor_calc_mode == 2) )

      % forward Fourier transformation of Phi_em
      for pp=1:pp_qq_limit 
        fft_Phi_em{pp} = fftn(Phi_em{pp});
      end

      for pp=1:pp_qq_limit
        tmp_fft_Phi_em{pp} = zeros(n_x_em,n_y_em,n_z_em);
        for qq=1:pp_qq_limit
          tmp_fft_Phi_em{pp} = tmp_fft_Phi_em{pp} + exp_time_ev_fft_A_em{pp}{qq}.*fft_Phi_em{qq};
        end
      end

      % backward Fourier transformation of fft_Phi_em
      for pp=1:pp_qq_limit
        fft_Phi_em{pp} = tmp_fft_Phi_em{pp};
        Phi_em{pp} = ifftn(fft_Phi_em{pp});
      end

      % multiply with mask function
      for pp=1:pp_qq_limit
        Phi_em{pp} = mask_em{pp}.*Phi_em{pp};
      end

    end
