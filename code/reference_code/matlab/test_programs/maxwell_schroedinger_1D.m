% Copyright (C) 2013 Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
more off;

fprintf('\n');
fprintf('Starting program ...\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
order = 2;
boxlength_ma     = 100;
boxlength_em     = 100;
borderlength_em  = 50;
signallength     = 30;
signal_pos       = 0;
dx_em        = 25.0;
dx_ma        = 10.0;
timesteps    = 1000;
const_em     = 1.0;
const_ma     = 1.0;
alpha = 7.2973525698*10^(-3);
h=1;
m=1;
sigma_ma      = 1/2;
sigma_em      = 3;
k_em          = 1/2;
calc_ma       = 0; % 1 = calculation of matter on, 0 = calculation of matter off
calc_em       = 1; % 1 = calculation of EM field on, 0 = calculation of EM field off
em_wave_shape = 2; % 1 = sinus shape, 2 = gauss shape, 3 = sinus and gauss envelope
helicity      = 1; % 1 = positive, -1 = negative helicity
boundary_em   = 0; % 1 = PEC, 2 = PMC, 3 = PML
prop_mode_em  = 1; % 0 = update equation, 1 = crank nicholson, 2 = fourth order taylor of exp
c_0           = 1; % /alpha;
epsilon_0     = 8*dx_em^2; % 1/(4*pi);
mu_0          = 1/(8*dx_em^2); % 1/(c_0^2*epsilon_0);
epsilon_b     = 2*dx_em^2; % epsilon_0;
mu_b          = 1/(2*dx_em^2); % mu_0;
c_b           = 1/sqrt(epsilon_b*mu_b);
medium_smooth = 1;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% system calculated parameters

border_em = int64(borderlength_em/dx_em)
nx_em     = int64(boxlength_em/dx_em)
nx_em_b   = nx_em+2*border_em

nx_ma     = int64(boxlength_ma/dx_ma);

%dt = dx_em/c_0;
dt = 0.005;
Sc = c_0*dt/dx_em



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup of figures
set(0,'Units','pixels') 
scnsize = get(0,'ScreenSize');
scnsize(3) = scnsize(3)*0.96;
fig1=figure(1);
set(gcf,'Visible', 'off'); 
clf
fig2=figure(2);
set(gcf,'Visible', 'off'); 
clf
fig3=figure(3);
set(gcf,'Visible', 'off'); 
clf
fig4=figure(4);
set(gcf,'Visible', 'off'); 
clf
fig5=figure(5);
set(gcf,'Visible', 'off'); 
clf
fig6=figure(6);
set(gcf,'Visible', 'off');
clf
position = get(fig1,'Position');
outerpos = get(fig1,'OuterPosition');
border = outerpos-position;
posshift = 50;

pos1 = [0,...
        scnsize(4)*(1/4),...
        scnsize(3)*(1/3)-border(4)/6,...
        scnsize(4)/2];
pos2 = [scnsize(3)*(1/3)+posshift,...
        scnsize(4)/2-5*border(3),...
        pos1(3),...
        pos1(4)-border(4)/4];
pos3 = [pos2(1),...
        5*border(3),...
        pos1(3),...
        pos2(4)];
pos4 = [scnsize(3)*(2/3)+posshift,...
        pos2(2),...
        pos1(3),...
        pos2(4)];
pos5 = [pos4(1),...
        pos3(2),...
        pos1(3),...
        pos2(4)];
set(fig1,'OuterPosition',pos1); 
set(fig2,'OuterPosition',pos2);
set(fig3,'OuterPosition',pos3);
set(fig4,'OuterPosition',pos4);
set(fig5,'OuterPosition',pos5);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup real-space mesh
fprintf('Setting up real-space mesh ...\n\n');
xx_em   = zeros(nx_em,1);
xx_ma   = zeros(nx_ma,1);
xx_em_b = zeros(nx_em_b,1);

xh = (double(nx_em)-1)/2;
for jj=1:nx_em
  xx_em(jj) = (double(jj)-1-xh)*dx_em;
end

xhb= (double(nx_em)+2*double(border_em)-1)/2;
for jj=1:(nx_em+2*border_em)
  xx_em_b(jj) = (double(jj)-1-xhb)*dx_em;
end

yh = (double(nx_ma)-1)/2;
for jj=1:nx_ma
  xx_ma(jj) = (double(jj)-1-yh)*dx_ma;
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup mu and epsilon arrays on the mesh
for jj=1:border_em;
  mu_array_b(                     jj) = mu_b;
  mu_array_b(     border_em+nx_em+jj) = mu_b; 
  epsilon_array_b(                jj) = epsilon_b;
  epsilon_array_b(border_em+nx_em+jj) = epsilon_b; 
  c_array_b(                      jj) = c_b;
  c_array_b(      border_em+nx_em+jj) = c_b;
end

for jj=1:nx_em
  mu_array(                 jj) = mu_0;
  mu_array_b(     border_em+jj) = mu_0;
  epsilon_array_b(border_em+jj) = epsilon_0;
  epsilon_array(            jj) = epsilon_0;
  c_array_b(      border_em+jj) = c_0;
  c_array(                  jj) = c_0;
end

figure(6);
set(gcf,'Visible','off');
plot(xx_em_b,epsilon_array_b);
filename = './figures/epsilon.png';
print(fig6,'-dpng',filename); 

figure(6);
set(gcf,'Visible','off');
plot(xx_em_b,mu_array_b);
filename = './figures/mu.png';
print(fig6,'-dpng',filename); 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finite difference operator matrises of the E-field

% finite difference operator for EM hamiltonian inside the box  
for jj=1:(nx_em)
  if (jj < nx_em),
    first_deriv_fd_E_b(jj+border_em,jj+border_em+1) =   c_array_b(jj+border_em) * sqrt(epsilon_array_b(jj+border_em)/2) * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_E_b(jj+border_em,jj+border_em-1) = - c_array_b(jj+border_em) * sqrt(epsilon_array_b(jj+border_em)/2) * 1/(2*dx_em);
  end
end
first_deriv_fd_E_mirror_b = first_deriv_fd_E_b;

% finite difference operator for E-field  
if(border_em > 0),
  first_deriv_fd_E_b(border_em        ,border_em      +1) =  c_array_b(border_em        ) * sqrt(epsilon_array_b(border_em        )/2) * 1/(2*dx_em);
  first_deriv_fd_E_b(border_em+nx_em  ,border_em+nx_em+1) =  c_array_b(border_em+nx_em  ) * sqrt(epsilon_array_b(border_em+nx_em  )/2) * 1/(2*dx_em);
  first_deriv_fd_E_b(border_em      +1,border_em        ) = -c_array_b(border_em      +1) * sqrt(epsilon_array_b(border_em      +1)/2) * 1/(2*dx_em);
  first_deriv_fd_E_b(border_em+nx_em+1,border_em+nx_em  ) = -c_array_b(border_em+nx_em+1) * sqrt(epsilon_array_b(border_em+nx_em+1)/2) * 1/(2*dx_em);
end
for jj=1:(border_em)
  if (jj < border_em),
    first_deriv_fd_E_b(jj,jj+1) = c_array_b(jj) * sqrt(epsilon_array_b(jj)/2) * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em < 2*border_em+nx_em)
    first_deriv_fd_E_b(jj+border_em+nx_em,jj+border_em+nx_em+1) = c_array_b(jj+border_em+nx_em) * sqrt(epsilon_array_b(jj+border_em+nx_em)/2) * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_E_b(jj,jj-1) = - c_array_b(jj) * sqrt(epsilon_array_b(jj)/2) * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em > border_em+nx_em+1)
    first_deriv_fd_E_b(jj+border_em+nx_em,jj+border_em+nx_em-1) = - c_array_b(jj+border_em+nx_em) * sqrt(epsilon_array_b(jj+border_em+nx_em)/2) * 1/(2*dx_em);
  end
end
% finite difference operator for mirror condition using Crank-Nicholson propagation
for jj=1:border_em
  first_deriv_fd_E_mirror_b(                jj,                jj) = 0;
  first_deriv_fd_E_mirror_b(border_em+nx_em+jj,border_em+nx_em+jj) = 0;
end
first_deriv_fd_E_mirror_b(border_em      +1,border_em)         = -c_array_b(border_em+    1) * sqrt(epsilon_array_b(border_em+    1)/2) * 1/(2*dx_em);
first_deriv_fd_E_mirror_b(border_em+nx_em  ,border_em+nx_em+1) =  c_array_b(border_em+nx_em) * sqrt(epsilon_array_b(border_em+nx_em)/2) * 1/(2*dx_em);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finite difference operator matrises of the B-field

% finite difference operator for B-field inside the box
for jj=1:(nx_em)
  if (jj < nx_em),
    first_deriv_fd_B_b(jj+border_em,jj+border_em+1) =   c_array_b(jj+border_em) * 1/sqrt(mu_array_b(jj+border_em)*2) * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_B_b(jj+border_em,jj+border_em-1) = - c_array_b(jj+border_em) * 1/sqrt(mu_array_b(jj+border_em)*2) * 1/(2*dx_em);
  end
end
first_deriv_fd_B_mirror_b = first_deriv_fd_E_b;

% finite difference operator for B-field outside the box
if(border_em > 0),
  first_deriv_fd_B_b(border_em        ,border_em      +1) =  c_array_b(border_em        ) * 1/sqrt(mu_array_b(border_em        )*2) * 1/(2*dx_em);
  first_deriv_fd_B_b(border_em+nx_em  ,border_em+nx_em+1) =  c_array_b(border_em+nx_em  ) * 1/sqrt(mu_array_b(border_em+nx_em  )*2) * 1/(2*dx_em);
  first_deriv_fd_B_b(border_em      +1,border_em        ) = -c_array_b(border_em      +1) * 1/sqrt(mu_array_b(border_em      +1)*2) * 1/(2*dx_em);
  first_deriv_fd_B_b(border_em+nx_em+1,border_em+nx_em  ) = -c_array_b(border_em+nx_em+1) * 1/sqrt(mu_array_b(border_em+nx_em+1)*2) * 1/(2*dx_em);
end
for jj=1:(border_em)
  if (jj < border_em),
    first_deriv_fd_B_b(jj,jj+1) = c_array_b(jj) * 1/sqrt(mu_array_b(jj)*2) * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em < 2*border_em+nx_em)
    first_deriv_fd_B_b(jj+border_em+nx_em,jj+border_em+nx_em+1) = c_array_b(jj+border_em+nx_em) * 1/sqrt(mu_array_b(jj+border_em+nx_em)*2) * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_B_b(jj,jj-1) = - c_array_b(jj) * 1/sqrt(mu_array_b(jj)*2) * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em > border_em+nx_em+1)
    first_deriv_fd_B_b(jj+border_em+nx_em,jj+border_em+nx_em-1) = - c_array_b(jj+border_em+nx_em) * 1/sqrt(mu_array_b(jj+border_em+nx_em)*2) * 1/(2*dx_em);
  end
end
% finite difference operator for mirror condition using Crank-Nicholson propagation
for jj=1:border_em
  first_deriv_fd_B_mirror_b(                jj,                jj) = 0;
  first_deriv_fd_B_mirror_b(border_em+nx_em+jj,border_em+nx_em+jj) = 0;
end
first_deriv_fd_B_mirror_b(border_em      +1,border_em        ) = -c_array_b(border_em+    1) * 1/sqrt(mu_array_b(border_em+    1)*2) * 1/(2*dx_em);
first_deriv_fd_B_mirror_b(border_em+nx_em  ,border_em+nx_em+1) =  c_array_b(border_em+nx_em) * 1/sqrt(mu_array_b(border_em+nx_em)*2) * 1/(2*dx_em);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finite difference operator matrises of the H-field

% finite difference operator for H-field inside the box  
for jj=1:(nx_em)
  if (jj < nx_em),
    first_deriv_fd_H_b(jj+border_em,jj+border_em+1) =   c_array_b(jj+border_em) * sqrt(mu_array_b(jj+border_em)/2) * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_H_b(jj+border_em,jj+border_em-1) = - c_array_b(jj+border_em) * sqrt(mu_array_b(jj+border_em)/2) * 1/(2*dx_em);
  end
end
first_deriv_fd_H_mirror_b = first_deriv_fd_H_b;

% finite difference operator for H-field outside the box 
if(border_em > 0),
  first_deriv_fd_H_b(border_em        ,border_em      +1) =  c_array_b(border_em        ) * sqrt(mu_array_b(border_em        )/2) * 1/(2*dx_em);
  first_deriv_fd_H_b(border_em+nx_em  ,border_em+nx_em+1) =  c_array_b(border_em+nx_em  ) * sqrt(mu_array_b(border_em+nx_em  )/2) * 1/(2*dx_em);
  first_deriv_fd_H_b(border_em      +1,border_em        ) = -c_array_b(border_em      +1) * sqrt(mu_array_b(border_em      +1)/2) * 1/(2*dx_em);
  first_deriv_fd_H_b(border_em+nx_em+1,border_em+nx_em  ) = -c_array_b(border_em+nx_em+1) * sqrt(mu_array_b(border_em+nx_em+1)/2) * 1/(2*dx_em);
end
for jj=1:(border_em)
  if (jj < border_em),
    first_deriv_fd_H_b(jj,jj+1) = c_array_b(jj) * sqrt(mu_array_b(jj)/2) * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em < 2*border_em+nx_em)
    first_deriv_fd_H_b(jj+border_em+nx_em,jj+border_em+nx_em+1) = c_array_b(jj+border_em+nx_em) * sqrt(mu_array_b(jj+border_em+nx_em)/2) * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_H_b(jj,jj-1) = - c_array_b(jj) * sqrt(mu_array_b(jj)/2) * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em > border_em+nx_em+1)
    first_deriv_fd_H_b(jj+border_em+nx_em,jj+border_em+nx_em-1) = - c_array_b(jj+border_em+nx_em) * sqrt(mu_array_b(jj+border_em+nx_em)/2) * 1/(2*dx_em);
  end
end
% finite difference operator for mirror condition using Crank-Nicholson propagation
for jj=1:border_em
  first_deriv_fd_H_mirror_b(                jj,                jj) = 0;
  first_deriv_fd_H_mirror_b(border_em+nx_em+jj,border_em+nx_em+jj) = 0;
end
first_deriv_fd_H_mirror_b(border_em      +1,border_em)         = -c_array_b(border_em+    1) * sqrt(mu_array_b(border_em+    1)/2) * 1/(2*dx_em);
first_deriv_fd_H_mirror_b(border_em+nx_em  ,border_em+nx_em+1) =  c_array_b(border_em+nx_em) * sqrt(mu_array_b(border_em+nx_em)/2) * 1/(2*dx_em);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% boundary conditions 
%if (boundary_em == 0)
%  E_first_deriv_fd_b = first_deriv_fd_b;
%  B_first_deriv_fd_b = first_deriv_fd_b;
%elseif (boundary_em == 1)
%  E_first_deriv_fd_b = first_deriv_fd_b;
%  B_first_deriv_fd_b = first_deriv_fd_mirror;
%elseif (boundary_em == 2)
%  E_first_deriv_fd_b = first_deriv_fd_mirror;
%  B_first_deriv_fd_b = first_deriv_fd_b;
%end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finite difference operator for matter Hamiltonian
for jj=1:nx_ma
  if (jj < nx_ma),
    second_deriv_fd(jj,jj+1) = 1/dx_ma;
  end
  second_deriv_fd(jj,jj) = -2/dx_ma;
  if (jj > 1),
    second_deriv_fd(jj,jj-1) = 1/(dx_ma);
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Schroedinger Hamiltonian

for ii=1:nx_ma
  Phi_xx_ma(ii,1) = 2 *(pi)^(-1/4)/(sqrt(sigma_ma)) * exp(-(xx_ma(ii)^2)/(4*sigma_ma^2));
end
Hm_ma    = -h^2/(2*m)*second_deriv_fd;
unitm_ma = sparse(eye(nx_ma));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Maxwell equations in Hamiltonian formalism

fprintf('Setting up electromagnetic field ...\n\n');

% start index of the initial signal
for ii=1:nx_em
  if xx_em(ii) > (signal_pos - double(signallength)/2),
    sig_idx_1 = ii;
    break;
  end
end
% end indexo of the initial signal
for ii=1:nx_em
  if xx_em(ii) > (signal_pos + double(signallength)/2),
    sig_idx_2 = ii-1;
    break;
  end
end

% initialization of the electromagnetic field
Ex = zeros(nx_em,1);
Ey = zeros(nx_em,1);
Ez = zeros(nx_em,1);
Bx = zeros(nx_em,1);
By = zeros(nx_em,1);
Bz = zeros(nx_em,1);
Hx = zeros(nx_em,1);
Hy = zeros(nx_em,1);
Hz = zeros(nx_em,1);

Ex_b = zeros(nx_em_b,1);
Ey_b = zeros(nx_em_b,1);
Ez_b = zeros(nx_em_b,1);
Bx_b = zeros(nx_em_b,1);
By_b = zeros(nx_em_b,1);
Bz_b = zeros(nx_em_b,1);
Hx_b = zeros(nx_em_b,1);
Hy_b = zeros(nx_em_b,1);
Hz_b = zeros(nx_em_b,1);


for ii=sig_idx_1:sig_idx_2
  if (em_wave_shape == 1)
    sinus_wave = sin(k_em*(xx_em(ii)-signal_pos));
    Ey(ii,1) = 0;
    Ez(ii,1) =          sinus_wave;
    By(ii,1) = -1/c_0 * sinus_wave;
    Bz(ii,1) = 0;
    Hy(ii,1) = 1/mu_array(ii) * By(ii,1);
    Hz(ii,1) = 1/mu_array(ii) * Bz(ii,1);
  end
  if (em_wave_shape == 2) 
    gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_em)) * exp(-((xx_em(ii)-signal_pos)^2)/(4*sigma_em^2));
    Ey(ii,1) = 0; 
    Ez(ii,1) =          gauss_dist;
    By(ii,1) = -1/c_0 * gauss_dist;
    Bz(ii,1) = 0;
    Hy(ii,1) = 1/mu_array(ii) * By(ii,1);
    Hz(ii,1) = 1/mu_array(ii) * Bz(ii,1);
  end
  if (em_wave_shape == 3)
    sinus_wave = sin(k_em*xx_em(ii));
    gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_em)) * exp(-((xx_em(ii)-signal_pos)^2)/(4*sigma_em^2));
    Ey(ii,1) = 0;
    Ez(ii,1) =          sinus_wave * gauss_dist;  
    By(ii,1) = -1/c_0 * sinus_wave * gauss_dist;
    Bz(ii,1) = 0;
    Hy(ii,1) = 1/mu_array(ii) * By(ii,1);
    Hz(ii,1) = 1/mu_array(ii) * Bz(ii,1);
  end
end

for ii=1:nx_em
  Ey_b(ii+border_em,1) = Ey(ii,1);
  Ez_b(ii+border_em,1) = Ez(ii,1);
  By_b(ii+border_em,1) = By(ii,1);
  Bz_b(ii+border_em,1) = Bz(ii,1);
  Hy_b(ii+border_em,1) = Hy(ii,1);
  Hz_b(ii+border_em,1) = Hz(ii,1);
end


% Riemann-Silberstein vector    
for ii=1:nx_em_b
  Fy_plus_b( ii,1) = sqrt(epsilon_array_b(ii)/2) * Ey_b(ii) + i / sqrt(2*mu_array_b(ii)) * By_b(ii);
  Fz_plus_b( ii,1) = sqrt(epsilon_array_b(ii)/2) * Ez_b(ii) + i / sqrt(2*mu_array_b(ii)) * Bz_b(ii);
end


% initialization of the spinor vector
for ii=1:nx_em_b
  % plus Spinor
  Phi_xx_em_b(          ii,1) =   i * Fy_plus_b(ii,1); 
  Phi_xx_em_b(  nx_em_b+ii,1) =       Fz_plus_b(ii,1); 
  Phi_xx_em_b(2*nx_em_b+ii,1) =       Fz_plus_b(ii,1); 
  Phi_xx_em_b(3*nx_em_b+ii,1) =   i * Fy_plus_b(ii,1);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initial EM-field plot
figure(1);
set(gcf,'Visible', 'off'); 
plot(xx_ma,abs(Phi_xx_ma));
filename = './figures/wavefunction/wave_packet_t=0.png';
print(fig1,'-dpng',filename);
figure(2);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Ez_b);
filename = './figures/E_field_z_b/E_field_z_b_t=0.png';
print(fig2,'-dpng',filename);
figure(3);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Bz_b);
filename = './figures/B_field_z_b/B_field_z_b_t=0.png';
print(fig3,'-dpng',filename);
figure(4);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Ey_b);
time_str = int2str(0);
filename = './figures/E_field_y_b/E_field_y_b_t=0.png';
print(fig4,'-dpng',filename);
figure(5);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,By_b);
filename = './figures/B_field_y_b/B_field_y_b_t=0.png';
print(fig5,'-dpng',filename);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Crank Nicolson for Schroedinger and maxwell equation 

fprintf('Schroedinger and electromagnetic field propagation ...\n\n');

for ii=1:timesteps

  if (mod(ii,10) == 0)
    steps_string = int2str(ii);
    combinedStr = strcat('Timesteps done:    ',steps_string,' ...\n\n');
    fprintf(combinedStr)
  end
 

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Schroedinger propagation
  if (calc_ma == 1)  

    Phi_xx_ma_old = Phi_xx_ma;
    b         = (unitm_ma - i * Hm_ma*dt) * Phi_xx_ma;
    Phi_xx_ma = (unitm_ma + i * Hm_ma*dt) \ b;

    time_str = int2str(ii);
    filename = './figures/wavefunction/wave_packet_t='
    combinedStr = strcat(filename,time_str,'.png');
    print(fig1,'-dpng',combinedStr);
  
  end


  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Maxwell propagation
  if (calc_em == 1)

    % Maxwell propagation
    Phi_xx_em_b_old = Phi_xx_em_b;
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update equation 
    if (prop_mode_em == 0) 
      for jj=2:(nx_em_b-1)
        Phi_xx_em_b(          jj,1) = Phi_xx_em_b(          jj,1) - dt*c/dx_em * (Phi_xx_em_b(2*nx_em_b+jj+1,1) - Phi_xx_em_b(2*nx_em_b+jj-1,1));
        Phi_xx_em_b(  nx_em_b+jj,1) = Phi_xx_em_b(  nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(3*nx_em_b+jj+1,1) - Phi_xx_em_b(3*nx_em_b+jj-1,1));
        Phi_xx_em_b(2*nx_em_b+jj,1) = Phi_xx_em_b(2*nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(          jj+1,1) - Phi_xx_em_b(          jj-1,1));
        Phi_xx_em_b(3*nx_em_b+jj,1) = Phi_xx_em_b(3*nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(  nx_em_b+jj+1,1) - Phi_xx_em_b(  nx_em_b+jj-1,1));
      end
    end
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % crank nicolson  
    if (prop_mode_em == 1)

      unitm_em_b = sparse(eye(4*nx_em_b));
      H_em_Phi_1 =                           - i * first_deriv_fd_E_b * Ez_b +     first_deriv_fd_H_b * Hz_b
      H_em_Phi_2 = first_deriv_fd_E_b * Ey_b - i * first_deriv_fd_E_b * Ex_b + i * first_deriv_fd_H_b * Hy_b
      H_em_Phi_3 = first_deriv_fd_E_b * Ey_b + i * first_deriv_fd_E_b * Ex_b + i * first_deriv_fd_H_b * Hy_b
      H_em_Phi_4 =                           - i * first_deriv_fd_E_b * Ez_b +     first_deriv_fd_H_b * Hz_b

      for jj=1:nx_em_b
        H_em_Phi_b(jj          ,1) = H_em_Phi_1(jj,1);
        H_em_Phi_b(jj+  nx_em_b,1) = H_em_Phi_2(jj,1);
        H_em_Phi_b(jj+2*nx_em_b,1) = H_em_Phi_3(jj,1);
        H_em_Phi_b(jj+3*nx_em_b,1) = H_em_Phi_4(jj,1);
      end 
 
%      b           = (unitm_em_b - i*(H_em_b)*dt) * Phi_xx_em_b;
      b           = Phi_xx_em_b - i * H_em_Phi_b * dt
      Phi_xx_em_b = (unitm_em_b + i * (H_em_b)*dt) \ b;

      for jj=1:nx_em_b
        Fx_plus_b(jj)     = -1/2*Phi_xx_em_b(jj)             + 1/2*Phi_xx_em_b(3*nx_em_b+jj);
        Fy_plus_b(jj)     = -i/2*Phi_xx_em_b(jj)             - i/2*Phi_xx_em_b(3*nx_em_b+jj);
        Fz_plus_b(jj)     =  1/2*Phi_xx_em_b(nx_em_b+jj)     + 1/2*Phi_xx_em_b(2*nx_em_b+jj);
      end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % fourth order taylor expansion
    if(prop_mode_em == 2)

      taylor_order = 4;
      zfact = 1;
      h_exp_psi = Phi_xx_em_b;
      % Taylor series for matrix exponential acting on a vector
      for jj = 1:taylor_order
        zfact = zfact*(-sqrt(-1)*dt)/jj;
        hzpsi = Hm_em_b*Phi_xx_em_b; % Hm_em_E*Phi_E + Hm_em_B*Phi_B;
        h_exp_psi = h_exp_psi + zfact*hzpsi;
        if(jj ~= taylor_order) 
          zpsi = hzpsi; 
        end
      end
      Phi_xx_em_b = h_exp_psi; 

      for jj=1:nx_em_b
        Phi_E_b(          jj,1) =  i/2 * imag(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_E_b(  nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_E_b(2*nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_E_b(3*nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_B_b(          jj,1) =  1/2 * real(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_B_b(  nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_B_b(2*nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));       
        Phi_B_b(3*nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
      end

    end

    for jj=1:nx_em_b
      Fx_plus_b(jj)     = -1/2*Phi_xx_em_b(jj)             + 1/2*Phi_xx_em_b(3*nx_em_b+jj);
      Fy_plus_b(jj)     = -i/2*Phi_xx_em_b(jj)             - i/2*Phi_xx_em_b(3*nx_em_b+jj);
      Fz_plus_b(jj)     =  1/2*Phi_xx_em_b(nx_em_b+jj)     + 1/2*Phi_xx_em_b(2*nx_em_b+jj);
    end

    for jj=1:nx_em
      Fx_plus(jj) = Fx_b_plus(jj+border_em);
      Fy_plus(jj) = Fy_b_plus(jj+border_em);
      Fz_plus(jj) = Fz_b_plus(jj+border_em);
    end

    for jj=1:nx_em_b
      Ex_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fx_plus_b(jj));
      Ey_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fy_plus_b(jj));
      Ez_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fz_plus_b(jj));
      Bx_b(jj) = imag(2*mu_array_b(jj)*Fx_plus_b(jj));
      By_b(jj) = imag(2*mu_array_b(jj)*Fy_plus_b(jj));
      Bz_b(jj) = imag(2*mu_array_b(jj)*Fz_plus_b(jj));
    end
   
    for jj=1:nx_em
      Ex(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fx_plus_b(jj+border_em));
      Ey(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fy_plus_b(jj+border_em));
      Ez(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fz_plus_b(jj+border_em));
      Bx(jj) = imag(2*mu_array_b(jj+border_em)*Fx_plus_b(jj+border_em));
      By(jj) = imag(2*mu_array_b(jj+border_em)*Fy_plus_b(jj+border_em));
      Bz(jj) = imag(2*mu_array_b(jj+border_em)*Fz_plus_b(jj+border_em));
    end

    time_array(ii)     = double(ii)*dt;

    energy_b           = dot(Fx_plus_b,Fx_plus_b)+dot(Fy_plus_b,Fy_plus_b)+dot(Fz_plus_b,Fz_plus_b) * dx_em; 
    energy_b_array(ii) = energy_b;


    figure(6);
    set(gcf,'Visible','off');
    plot(time_array,energy_b_array);
    filename = './figures/energy.png';
    print(fig6,'-dpng',filename); 
    

    figure(2);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Ez);
    time_str = int2str(ii);
    filename = './figures/E_field_z/E_field_z_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig2,'-dpng',combinedStr);

    figure(3);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Bz);
    time_str = int2str(ii);
    filename = './figures/B_field_z/B_field_z_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig3,'-dpng',combinedStr);

    figure(4);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Ey);
    time_str = int2str(ii);
    filename = './figures/E_field_y/field_y_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig4,'-dpng',combinedStr);

    figure(5);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,By);
    time_str = int2str(ii);
    filename = './figures/B_field_y/B_field_y_t=';
    combinedStr =  strcat(filename,time_str,'.png');
    print(fig5,'-dpng',combinedStr);

    
    figure(2);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Ez_b);
    time_str = int2str(ii);
    filename = './figures/E_field_z_b/E_field_z_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig2,'-dpng',combinedStr);

    figure(3);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Bz_b);
    time_str = int2str(ii);
    filename = './figures/B_field_z_b/B_field_z_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig3,'-dpng',combinedStr);

    figure(4);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Ey_b);
    time_str = int2str(ii);
    filename = './figures/E_field_y_b/E_field_y_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig4,'-dpng',combinedStr);

    figure(5);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,By_b);
    time_str = int2str(ii);
    filename = './figures/B_field_y_b/B_field_y_b_t=';
    combinedStr =  strcat(filename,time_str,'.png');
    print(fig5,'-dpng',combinedStr);
  
  end

end





