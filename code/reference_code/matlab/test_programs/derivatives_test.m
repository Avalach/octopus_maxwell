% Copyright (C) 2013 Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
more off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
finite_difference_order = 8;   % finite difference order for laplacian
nx = 400;
ny = nx;
dx = 0.05;
dy = dx;

%nx = 4 %100;
%ny = nx %2*nx;
%dx = 1 %0.05;
%dy = dx % dx/2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup 2D cartesian Laplacian with zero boundary condition
fprintf('Setting up Laplacian ...\n\n');
unit_nx = sparse(eye(nx));
unit_ny = sparse(eye(ny));

% get finite-difference matrices
[first_derivative_nx, first_derivative_periodic_nx] = fd_first_derivative(nx, dx, finite_difference_order);
[first_derivative_ny, first_derivative_periodic_ny] = fd_first_derivative(ny, dy, finite_difference_order);

[second_derivative_nx, second_derivative_periodic_nx] = fd_second_derivative(nx, dx, finite_difference_order);
[second_derivative_ny, second_derivative_periodic_ny] = fd_second_derivative(ny, dy, finite_difference_order);


laplacian_2d =  kron(unit_ny, second_derivative_nx) ...
              + kron(second_derivative_ny, unit_nx);

fprintf('Total number of gridpoints: %d\n', length(laplacian_2d));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup real-space mesh
fprintf('Setting up real-space mesh ...\n\n');
xx = zeros(nx,1);
yy = zeros(ny,1);

xh = (nx-1)/2;
for jj=1:nx
  xx(jj) = (jj-1-xh)*dx;
end
yh = (ny-1)/2;
for jj=1:ny
  yy(jj) = (jj-1-yh)*dy;
end

[xm,ym] = meshgrid(xx,yy);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup test function and second derivative
fprintf('Setting up test function ...\n\n');
test_function = zeros(nx,ny);
sigma = 1/2;
for ii=1:nx
  for jj=1:ny
    test_function(ii,jj) = exp(-(xx(ii)-yy(jj))^2/sigma^2)*exp(-xx(ii)^2-yy(jj)^2);
    test_function_2nd_deriv(ii,jj) = 4/sigma^4*exp(-(xx(ii)-yy(jj))^2/sigma^2)*exp(-xx(ii)^2-yy(jj)^2) ...
                                              *(sigma^2*(-1 + 2*(xx(ii) - yy(jj))^2) + 2*(xx(ii) - yy(jj))^2 + sigma^4*(-1 + xx(ii)^2 + yy(jj)^2));
  end
end

%sigma = 1/2;
%nx = 100;
%ny = 2*nx;
%dx = 0.05;
%dy = dx/2;
%fprintf('Difference to reference values: \n');
%test_function(nx/2,ny/2) - 1/(exp(9/6400))
%test_function_2nd_deriv(nx/2,ny/2) - (-6391/(320*exp(9/6400)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% numerical second derivative of test function
test_function_2nd_deriv_fd = reshape(laplacian_2d*reshape(test_function,nx*ny,1),nx,ny);
fprintf('L2 norm of difference between exact and numerical derivative: %12.10e\n', norm(test_function_2nd_deriv - test_function_2nd_deriv_fd));
mesh(xm',ym',test_function_2nd_deriv - test_function_2nd_deriv_fd)
xlabel('x axis');
ylabel('y axis');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup 2D cartesian Laplacian with zero boundary condition for sheared grid
fprintf('Setting up Laplacian ...\n\n');

theta = pi/3;
fprintf('Shearing angle: %12.8f\n', theta);

laplacian_2d_sheared = 1/sin(theta)^2 * (   kron(unit_ny, second_derivative_nx) ...
                                          + kron(second_derivative_ny, unit_nx) ...
                             - 2*cos(theta)*kron(first_derivative_ny, first_derivative_nx));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup sheared real-space mesh
fprintf('Setting up real-space mesh ...\n\n');
xx = zeros(nx,1);
yy = zeros(ny,1);

xh = (nx-1)/2;
yh = (ny-1)/2;
for ii=1:nx
  for jj=1:ny
    mesh_sheared_xx(ii,jj) = (ii-1-xh)*dx + cos(theta)*(jj-1-yh)*dy; 
    mesh_sheared_yy(ii,jj) = sin(theta)*(jj-1-yh)*dy; 
  end
end

for ii=1:nx
  for jj=1:ny
    test_function_sheared(ii,jj) = exp(-( mesh_sheared_xx(ii,jj)- mesh_sheared_yy(ii,jj))^2/sigma^2)*exp(- mesh_sheared_xx(ii,jj)^2- mesh_sheared_yy(ii,jj)^2);
    test_function_2nd_deriv_sheared(ii,jj) = 4/sigma^4*exp(-( mesh_sheared_xx(ii,jj)- mesh_sheared_yy(ii,jj))^2/sigma^2)*exp(- mesh_sheared_xx(ii,jj)^2- mesh_sheared_yy(ii,jj)^2) ...
                                              *(sigma^2*(-1 + 2*( mesh_sheared_xx(ii,jj) -  mesh_sheared_yy(ii,jj))^2) + 2*( mesh_sheared_xx(ii,jj) -  mesh_sheared_yy(ii,jj))^2 + sigma^4*(-1 +  mesh_sheared_xx(ii,jj)^2 +  mesh_sheared_yy(ii,jj)^2));
  end
end

mesh(mesh_sheared_xx',mesh_sheared_yy', test_function')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% numerical second derivative of test function with sheared laplacian
test_function_2nd_deriv_sheared_fd = reshape(laplacian_2d_sheared*reshape(test_function_sheared,nx*ny,1),nx,ny);
fprintf('L2 norm of difference between exact and numerical derivative: %12.10e\n', norm(test_function_2nd_deriv_sheared - test_function_2nd_deriv_sheared_fd));
mesh(mesh_sheared_xx,mesh_sheared_yy, test_function_2nd_deriv_sheared - test_function_2nd_deriv_sheared_fd)
view(2)
%contour(mesh_sheared_xx,mesh_sheared_yy, test_function_2nd_deriv_sheared - test_function_2nd_deriv_sheared_fd)
%grid on
%daspect([1 1 1])
