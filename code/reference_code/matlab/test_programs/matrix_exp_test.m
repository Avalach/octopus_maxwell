clear all

N = 1000;

matrix_1 = zeros(N,N);

for i=1:N
  matrix_1(1,i) = i;
  matrix_1(i,1) = i;
end

matrix_2 = zeros(N,N);

for i=1:N
  for j=1:N
    matrix_2(i,j) = i;
  end
end

sparse_matrix_1 = sparse(matrix_1);

tic 
exp_matrix_1 = expm(matrix_1);
toc

tic 
exp_matrix_1 = expm(sparse_matrix_1);
toc

tic 
exp_matrix_2 = expm(matrix_2);
toc

