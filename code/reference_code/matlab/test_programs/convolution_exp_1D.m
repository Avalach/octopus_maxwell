clear;
more off;

Nx   =  4;
order=  8;
xmin = -2;
xmax =  2;
dx   =  (xmax-xmin)/double(Nx);

dt = 0.001

length = xmax-xmin;

xpoints = (xmin+dx/2):dx:(xmax-dx/2);

for nn=1:order
  for kk=0:nn
   binomial(nn,kk+1) = nchoosek(nn,kk);
  end
end

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
for ii=1:Nx
  g1(ii,1)       = exp(-(xpoints(ii))^2);
  f1(ii,1)       = exp(-2*(xpoints(ii))^2);
  f1g1_0(ii,1)     = f1(ii,1);
  f1g1_1(ii,1)     = dt   * (- 4 * exp(-3*(xpoints(ii))^2) * xpoints(ii)) ;
  f1g1_2(ii,1)     = dt^2 * ( 4 * exp(-4*(xpoints(ii))^2) * (-1 + 6 * (xpoints(ii))^2)) ;
  f1g1_3(ii,1)     = dt^3 * ( -16 * exp(-5*(xpoints(ii))^2) * xpoints(ii) * (-5 + 12 * (xpoints(ii))^2)) ;
  f1g1_4(ii,1)     = dt^4 * ( 16 * exp(-6*(xpoints(ii))^2) * (5 - 86*(xpoints(ii))^2 + 120 * (xpoints(ii))^4));
  f1g1_s(ii,1)     = f1g1_0(ii,1) + dt * f1g1_1(ii,1) + 1/2*dt^2*f1g1_2(ii,1) + 1/6*dt^3*f1g1_3(ii,1) + 1/24*dt^4*f1g1_4(ii,1);
end

for jj=0:(Nx/2)
  p(jj+1,1) = 2*pi/(double(length)) * double(jj);
end
for jj=(Nx/2+1):(Nx-1)
  p(jj+1,1) = 2*pi/(double(length)) * double(jj-Nx);
end
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
conv_array = zeros(Nx,Nx);
for qq=1:Nx
  for kk=1:Nx
    for mm=1:Nx
      conv_array(qq,kk) = conv_array(qq,kk) +1/sqrt(Nx) * g1(mm,1)*exp(-i*2*pi*(mm-1)*((qq-1)-(kk-1))/Nx);
    end
  end
end

idx = 0;
for qq=1:Nx
  for kk=1:Nx
    conv_array_p(qq,kk) = conv_array(qq,kk) * i * p(kk,1);
    idx = idx+1;
    test_array_1(qq,kk) = idx;
  end
end

test_array_1 = test_array_1^2;

conv_array_p_0 = zeros(Nx,Nx);
for qq=1:Nx
  conv_array_p_0(qq,qq) = sqrt(Nx);
end
conv_array_p_1 = conv_array_p  *dt;
conv_array_p_2 = conv_array_p_1*dt*conv_array_p /sqrt(Nx);
conv_array_p_3 = conv_array_p_2*dt*conv_array_p /sqrt(Nx);
conv_array_p_4 = conv_array_p_3*dt*conv_array_p /sqrt(Nx);

for qq=1:Nx
  for kk=1:Nx
    conv_array_p_x(qq,kk) = conv_array_p_0(qq,kk) + dt*conv_array_p_1(qq,kk)   + 1/2*dt^2*conv_array_p_2(qq,kk) + 1/6*dt^3*conv_array_p_3(qq,kk) + 1/24*dt^4*conv_array_p_4(qq,kk) ...
                                                  + dt^5*conv_array_p_5(qq,kk) + dt^6*conv_array_p_6            + dt^7*conv_array_p_7            + dt^8*conv_array_p_8;
  end
end

conv_array_p;


for qq=1:Nx
  
end


corr_array = zeros(Nx,Nx);
for qq=1:Nx
  corr_array(qq,qq) = 0;
  tmp_array_1 = zeros(Nx,Nx);
  idx = 0;
  for kk=1:Nx
    for kkp=1:Nx
      idx = idx+1;
      tmp_array(kk,kkp)    = conv_array_p(qq,kkp);
      test_array_2(kk,kkp) = idx;
    end
  end
  exp_tmp_array = sqrt(Nx) * expm(dt*1/sqrt(Nx)*tmp_array);
  corr_value = exp_tmp_array(1,1);
  test_array_2 = test_array_2 * test_array_2;
  for kk=1:Nx
    tmp_array_2(qq,kk) = test_array_2(kk,kk);
    if (qq == kk)
      corr_array(qq,kk) = 0;
    else
      corr_array(qq,kk) = corr_value;
    end
    exp_conv_array_p_2(qq,kk) = exp_tmp_array(kk,kk);
  end    
end

conv_array_p_exp_2 = exp_conv_array_p_2 - corr_array;

%test_array_1
%test_array_2
%tmp_array_2


%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
FT_f1 = 1/sqrt(Nx) * fftn(f1);
FT_g1 = 1/sqrt(Nx) * fftn(g1);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
FT_f1g1_x = zeros(Nx,1);
for qq=1:Nx
  for kk=1:Nx
    FT_f1g1_x(qq,1) = FT_f1g1_x(qq,1) + conv_array_p_x(qq,kk) * FT_f1(kk,1);
  end
end
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
conv_array_p_exp = sqrt(Nx) * expm(dt*1/sqrt(Nx)*conv_array_p);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


conv_array_p_exp  ;
conv_array_p_exp_2;


for qq=1:Nx
  FT_f1g1_1(qq,1) = 0;
  for kk=1:Nx
    FT_f1g1_1(qq,1) = FT_f1g1_1(qq,1) + conv_array_p_1(qq,kk) * FT_f1(kk,1);
  end
end

for qq=1:Nx
  FT_f1g1_2(qq,1) = 0;
  for kk=1:Nx
    FT_f1g1_2(qq,1) = FT_f1g1_2(qq,1) + conv_array_p_2(qq,kk) * FT_f1(kk,1);
  end
end

for qq=1:Nx
  FT_f1g1_3(qq,1) = 0;
  for kk=1:Nx
    FT_f1g1_3(qq,1) = FT_f1g1_3(qq,1) + conv_array_p_3(qq,kk) * FT_f1(kk,1);
  end
end

for qq=1:Nx
  FT_f1g1_4(qq,1) = 0;
  for kk=1:Nx
    FT_f1g1_4(qq,1) = FT_f1g1_4(qq,1) + conv_array_p_4(qq,kk) * FT_f1(kk,1);
  end
end

for ii=1:Nx
    FT_f1g1_s(ii,1) = sqrt(Nx) * FT_f1(ii,1) + FT_f1g1_1(ii,1) + 1/2 * FT_f1g1_2(ii,1) + 1/6 * FT_f1g1_3(ii,1) + 1/24 * FT_f1g1_4(ii,1);
end

for qq=1:Nx
  FT_f1g1_exp(qq,1) = 0;    
  for kk=1:Nx
    FT_f1g1_exp(qq,1) = FT_f1g1_exp(qq,1) + conv_array_p_exp_2(qq,kk) * FT_f1(kk,1);
  end
end

conv_f1g1_1 = ifftn(FT_f1g1_1);
conv_f1g1_2 = ifftn(FT_f1g1_2);
conv_f1g1_3 = ifftn(FT_f1g1_3);
conv_f1g1_4 = ifftn(FT_f1g1_4);
conv_f1g1_s = ifftn(FT_f1g1_s);
%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
conv_f1g1_x = ifftn(FT_f1g1_x);
conv_f1g1_exp = ifftn(FT_f1g1_exp);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

figure(1);
plot(xpoints,f1g1_0,'b')
hold on;
plot(xpoints,f1g1_0,'r')
hold off;

%figure(2);
%plot(xpoints,f1g1_1,'b')
%hold on;
%plot(xpoints,conv_f1g1_1,'r')
%hold off;

%figure(3);
%plot(xpoints,f1g1_2,'b')
%hold on;
%plot(xpoints,conv_f1g1_2,'r')
%hold off;

%figure(4);
%plot(xpoints,f1g1_3,'b')
%hold on;
%plot(xpoints,conv_f1g1_3,'r')
%hold off;

%figure(5);
%plot(xpoints,f1g1_4,'b')
%hold on;
%plot(xpoints,conv_f1g1_4,'r')
%hold off;

%figure(6);
%plot(xpoints,f1g1_s,'b')
%hold on;
%plot(xpoints,conv_f1g1_s,'r')
%hold off;

%figure(7);
%plot(xpoints,f1g1_s,'b')
%hold on;
%plot(xpoints,conv_f1g1_x,'r')
%hold off;

figure(8);
plot(xpoints,f1g1_s,'b')
hold on;
plot(xpoints,conv_f1g1_exp,'r')
hold off;




