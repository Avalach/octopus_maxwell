clear;
more off;

Nx   =  60;
xmin = -2;
xmax =  2;
dx   =  (xmax-xmin)/double(Nx);

length = xmax-xmin;

xpoints = (xmin+dx/2):dx:(xmax-dx/2);

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
for ii=1:Nx
  g1(ii,1)       = exp(-(xpoints(ii))^2);
  f1(ii,1)       = exp(-2*(xpoints(ii))^2);
  f1g1_0(ii,1)     = f1(ii,1);
  f1g1_1(ii,1)     = - 4 * exp(-3*(xpoints(ii))^2) * xpoints(ii);
  f1g1_2(ii,1)     =   4 * exp(-4*(xpoints(ii))^2) * (-1 + 6 * (xpoints(ii))^2);
  f1g1_3(ii,1)     = -16 * exp(-5*(xpoints(ii))^2) * xpoints(ii) * (-5 + 12 * (xpoints(ii))^2);
  f1g1_4(ii,1)     =  16 * exp(-6*(xpoints(ii))^2) * (5 - 86*(xpoints(ii))^2 + 120 * (xpoints(ii))^4);
  f1g1_s(ii,1)     = f1g1_0(ii,1) + f1g1_1(ii,1) + 1/2*f1g1_2(ii,1) + 1/6*f1g1_3(ii,1) + 1/24*f1g1_4(ii,1);
end

for jj=0:(Nx/2)
  p(jj+1,1) = 2*pi/(double(length)) * double(jj);
end
for jj=(Nx/2+1):(Nx-1)
  p(jj+1,1) = 2*pi/(double(length)) * double(jj-Nx);
end
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
conv_arry = zeros(Nx,Nx);
for qq=1:Nx
  for kk=1:Nx
    for mm=1:Nx
      conv_arry(qq,kk) = conv_arry(qq,kk) +1/sqrt(Nx) * g1(mm,1)*exp(-i*2*pi*(mm-1)*((qq-1)-(kk-1))/Nx);
    end
  end
end
for qq=1:Nx
  for kk=1:Nx
    conv_arry_p(qq,kk) = conv_arry(qq,kk) * i * p(kk,1);
  end
end

conv_arry_p_0 = zeros(Nx,Nx);
for qq=1:Nx
  conv_arry_p_0(qq,qq) = sqrt(Nx);
end
conv_arry_p_1 = conv_arry_p;
conv_arry_p_2 = conv_arry_p_1*conv_arry_p /sqrt(Nx);
conv_arry_p_3 = conv_arry_p_2*conv_arry_p /sqrt(Nx);
conv_arry_p_4 = conv_arry_p_3*conv_arry_p /sqrt(Nx);

conv_arry_p_x = expm(conv_arry_p);

for qq=1:Nx
  for kk=1:Nx
    conv_arry_p_s(qq,kk) = conv_arry_p_0(qq,kk) + conv_arry_p_1(qq,kk) + 1/2*conv_arry_p_2(qq,kk) + 1/6*conv_arry_p_3(qq,kk) + 1/24*conv_arry_p_4(qq,kk);
  end
end

%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
FT_f1 = 1/sqrt(Nx) * fftn(f1);
FT_g1 = 1/sqrt(Nx) * fftn(g1);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
FT_f1g1_s = zeros(Nx,1);
for qq=1:Nx
  for kk=1:Nx
    FT_f1g1_s(qq,1) = FT_f1g1_s(qq,1) + conv_arry_p_s(qq,kk) * FT_f1(kk,1);
  end
end
FT_f1g1_x = zeros(Nx,1);
for qq=1:Nx
  for kk=1:Nx
    FT_f1g1_x(qq,1) = FT_f1g1_x(qq,1) + conv_arry_p_x(qq,kk) * FT_f1(kk,1);
  end
end

%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
conv_f1g1_s = ifftn(FT_f1g1_s);
conv_f1g1_x = ifftn(FT_f1g1_x);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


figure(7);
plot(xpoints,f1g1_s,'b')
hold on;
plot(xpoints,conv_f1g1_s,'r')
hold off;

figure(8);
plot(xpoints,f1g1_s,'b')
hold on;
plot(xpoints,conv_f1g1_x,'r')
hold off;




