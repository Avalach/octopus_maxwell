% Copyright (C) 2013 Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
more off;

fprintf('\n');
fprintf('Starting program ...\n\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
order = 2;
boxlength_ma     = 50;
boxlength_em     = 50;
borderlength_em  = 50;
signallength     = 30;
signal_pos       = 0;
dx_em        = 0.2;
dx_ma        = 0.1;
timesteps    = 1000;
const_em     = 1.0;
const_ma     = 1.0;
alpha = 7.2973525698*10^(-3);
h=1;
m=1;
sigma_ma      = 1/2;
sigma_em      = 3;
k_em          = 1/2;
finite_difference_order = 8;
calc_ma       = 0; % 1 = calculation of matter on, 0 = calculation of matter off
calc_em       = 1; % 1 = calculation of EM field on, 0 = calculation of EM field off
em_wave_shape = 2; % 1 = sinus shape, 2 = gauss shape, 3 = sinus and gauss envelope
helicity      = 1; % 1 = positive, -1 = negative helicity
boundary_em   = 0; % 1 = PEC, 2 = PMC, 3 = PML
prop_mode_em  = 2; % 0 = update equation, 1 = crank nicholson, 2 = fourth order taylor of exp


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initial values of v, h, epsilon, and mu in the vacuum and inital values of v, h, epsion, and mu in the boundary region

% vacuumvalues
v_0           = 1/alpha;
epsilon_0     = 1/(4*pi);
mu_0          = 1/(v_0^2*epsilon_0);
h_0           = sqrt(mu_0/epsilon_0);

% boundary values
epsilon_b     = 4.0*epsilon_0;
mu_b          = mu_0;
v_b           = 1/sqrt(epsilon_b*mu_b);
h_b           = sqrt(mu_b/epsilon_b);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% system parameters calculated while program is running

border_em = int64(borderlength_em/dx_em)
nx_em     = int64(boxlength_em/dx_em)
nx_em_b   = nx_em+2*border_em

nx_ma     = int64(boxlength_ma/dx_ma);

%dt = dx_em/c_0;
dt = 0.001;
Sc = v_0*dt/dx_em


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup of figures

set(0,'Units','pixels') 
scnsize = get(0,'ScreenSize');
scnsize(3) = scnsize(3)*0.96;
fig1=figure(1);
set(gcf,'Visible', 'off'); 
clf
fig2=figure(2);
set(gcf,'Visible', 'off'); 
clf
fig3=figure(3);
set(gcf,'Visible', 'off'); 
clf
fig4=figure(4);
set(gcf,'Visible', 'off'); 
clf
fig5=figure(5);
set(gcf,'Visible', 'off'); 
clf
fig6=figure(6);
set(gcf,'Visible', 'off');
clf
fig7=figure(7);
set(gcf,'Visible', 'on');
fig8=figure(8);
set(gcf,'Visible', 'on');

position = get(fig1,'Position');
outerpos = get(fig1,'OuterPosition');
border = outerpos-position;
posshift = 50;

pos1 = [0,...
        scnsize(4)*(1/4),...
        scnsize(3)*(1/3)-border(4)/6,...
        scnsize(4)/2];
pos2 = [scnsize(3)*(1/3)+posshift,...
        scnsize(4)/2-5*border(3),...
        pos1(3),...
        pos1(4)-border(4)/4];
pos3 = [pos2(1),...
        5*border(3),...
        pos1(3),...
        pos2(4)];
pos4 = [scnsize(3)*(2/3)+posshift,...
        pos2(2),...
        pos1(3),...
        pos2(4)];
pos5 = [pos4(1),...
        pos3(2),...
        pos1(3),...
        pos2(4)];
set(fig1,'OuterPosition',pos1); 
set(fig2,'OuterPosition',pos2);
set(fig3,'OuterPosition',pos3);
set(fig4,'OuterPosition',pos4);
set(fig5,'OuterPosition',pos5);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finite difference operator

switch finite_difference_order
  case 2
    % 2nd order finite difference
    for jj=1:nx_em_b
      if (jj < nx_em_b),
        first_deriv_fd_b(jj,jj+1) = 1/(dx_em)*(  1/2);
      end
      if (jj > 1),
        first_deriv_fd_b(jj,jj-1) = 1/(dx_em)*( -1/2);
      end
    end
  case 4
    % 4th order finite difference
    for jj=1:nx_em_b
      if (jj < nx_em_b),
        first_deriv_fd_b(jj,jj+1) = 1/(dx_em)*(  2.0/3.0);
      end
      if (jj > 1),
        first_deriv_fd_b(jj,jj-1) = 1/(dx_em)*( -2.0/3.0);
      end
      if (jj < nx_b - 1),
        first_deriv_fd_b(jj,jj+2) = 1/(dx_em)*( -1.0/12.0);
      end
      if (jj > 2),
        first_deriv_fd_b(jj,jj-2) = 1/(dx_em)*(  1.0/12.0);
      end
    end
  case 6
    % 6th order finite difference
    for jj=1:nx_em_b
      if (jj < nx_em_b),
        first_deriv_fd_b(jj,jj+1) = 1/(dx_em)*(  3.0/4.0);
      end
      if (jj > 1),
        first_deriv_fd_b(jj,jj-1) = 1/(dx_em)*( -3.0/4.0);
      end
      if (jj < nx_em_b - 1),
        first_deriv_fd_b(jj,jj+2) = 1/(dx_em)*( -3.0/20.0);
      end
      if (jj > 2),
        first_deriv_fd_b(jj,jj-2) = 1/(dx_em)*(  3.0/20.0);
      end
      if (jj < nx_em_b - 2),
        first_deriv_fd_b(jj,jj+3) = 1/(dx_em)*(  1.0/60.0);
      end
      if (jj > 3),
        first_deriv_fd_b(jj,jj-3) = 1/(dx_em)*( -1.0/60.0);
      end
    end
  case 8
    % 8th order finite difference
    for jj=1:nx_em_b
      if (jj < nx_em_b),
        first_deriv_fd_b(jj,jj+1) = 1/(dx_em)*( 4.0/5.0);
      end
      if (jj > 1),
        first_deriv_fd_b(jj,jj-1) = 1/(dx_em)*( -4.0/5.0);
      end
      if (jj < nx_em_b - 1),
        first_deriv_fd_b(jj,jj+2) = 1/(dx_em)*( -1.0/5.0);
      end
      if (jj > 2),
        first_deriv_fd_b(jj,jj-2) = 1/(dx_em)*(  1.0/5.0);
      end
      if (jj < nx_em_b - 2),
        first_deriv_fd_b(jj,jj+3) = 1/(dx_em)*(  4.0/105.0);
      end
      if (jj > 3),
        first_deriv_fd_b(jj,jj-3) = 1/(dx_em)*( -4.0/105.0);
      end
      if (jj < nx_em_b - 3),
        first_deriv_fd_b(jj,jj+4) = 1/(dx_em)*( -1.0/280.0);
      end
      if (jj > 4),
        first_deriv_fd_b(jj,jj-4) = 1/(dx_em)*(  1.0/280.0);
      end
    end
  otherwise
    fprintf('Error: unknown finite-difference order');
    exit;
end


% finite difference operator for matter Hamiltonian <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< TODO higher order
for jj=1:nx_ma
  if (jj < nx_ma),
    second_deriv_fd(jj,jj+1) = 1/dx_ma;
  end
  second_deriv_fd(jj,jj) = -2/dx_ma;
  if (jj > 1),
    second_deriv_fd(jj,jj-1) = 1/(dx_ma);
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup real-space mesh
fprintf('Setting up real-space mesh ...\n\n');
xx_em   = zeros(nx_em,1);
xx_ma   = zeros(nx_ma,1);
xx_em_b = zeros(nx_em_b,1);

xh = (double(nx_em)-1)/2;
for jj=1:nx_em
  xx_em(jj) = (double(jj)-1-xh)*dx_em;
end

xhb= (double(nx_em)+2*double(border_em)-1)/2;
for jj=1:(nx_em+2*border_em)
  xx_em_b(jj) = (double(jj)-1-xhb)*dx_em;
end

yh = (double(nx_ma)-1)/2;
for jj=1:nx_ma
  xx_ma(jj) = (double(jj)-1-yh)*dx_ma;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup mu, epsilon, v, and h arrays on the mesh
for jj=1:border_em;
  mu_array_b(                     jj,1) = mu_b;
  mu_array_b(     border_em+nx_em+jj,1) = mu_b; 
  epsilon_array_b(                jj,1) = epsilon_b;
  epsilon_array_b(border_em+nx_em+jj,1) = epsilon_b; 
  v_array_b(                      jj,1) = v_b;
  v_array_b(      border_em+nx_em+jj,1) = v_b; 
  h_array_b(                      jj,1) = h_b;
  h_array_b(      border_em+nx_em+jj,1) = h_b;
end

for jj=1:nx_em
  mu_array_b(     border_em+jj,1) = mu_0;
  epsilon_array_b(border_em+jj,1) = epsilon_0;
  v_array_b(      border_em+jj,1) = v_0;
  h_array_b(      border_em+jj,1) = h_0;
end

for ii=1:nx_em_b
  v_over_h_b(ii,1) = v_array_b(ii,1) / h_array_b(ii,1);
end

%divergence of v and h
div_v_b = first_deriv_fd_b*v_array_b;
div_h_b = first_deriv_fd_b*h_array_b;


fft_v_array_b = fft(v_array_b);

figure(7);
plot(xx_em_b,v_array_b)

figure(8);
plot(xx_em_b,fft_v_array_b)

%if (medium_smooth == 1),
%  div_v_b(1)                 = ( 1/2) * v_array_b(2)                   / dx_em;
%  div_v_b(nx_em+2*border_em) = (-1/2) * v_array_b(nx_em+2*border_em-1) / dx_em;
%  div_h_b(1)                 = ( 1/2) * h_array_b(2)                   / dx_em;
%  div_h_b(nx_em+2*border_em) = (-1/2) * h_array_b(nx_em+2*border_em-1) / dx_em;
%  for ii=2:(nx_em_b-1)
%    div_v_b(ii) = (-1/2 * v_array_b(ii-1) + 1/2 * v_array_b(ii+1)) / dx_em;
%    div_h_b(ii) = (-1/2 * h_array_b(ii-1) + 1/2 * h_array_b(ii+1)) / dx_em;
%  end
%elseif (medium_smooth == 0),
%  for jj=1:nx_em_b
%    div_v_b(jj) = 0;
%    div_h_b(jj) = 0;
%  end
%  small = 0.001;
%  div_v_b(border_em        ) = (v_0-v_b)/(pi*small);
%  div_v_b(border_em+nx_em+1) = (v_b-v_0)/(pi*small);
%  div_h_b(border_em        ) = (h_0-h_b)/(pi*small);
%  div_h_b(border_em+nx_em+1) = (h_b-h_0)/(pi*small);
%end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% finite difference operator matrises

for ii=1:nx_em_b
  Hm_first_deriv_fd_b(ii,:) = v_array_b(ii,1) * first_deriv_fd_b(ii,:);
end

%Hm_first_deriv_fd_b

%for ii=1:nx_em_b
%  for jj=1:nx_em_b
%    Hm_first_deriv_fd_b(ii,jj) = v_array_b(ii,1) * first_deriv_fd_b(jj,ii);
%  end
%end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matrix hamiltonian

MM_Ez = zeros(8,8);
MM_By = zeros(8,8);

MM_Ez(1,3) = -i * h;
MM_By(2,4) = -i * h;
MM_By(3,1) = -i * h;
MM_Ez(4,2) = -i * h;

MM_Ez(5,7) = -i * h;
MM_By(6,8) = -i * h;
MM_By(7,5) = -i * h;
MM_Ez(8,6) = -i * h;

VV(2,1) = -i * 1/2 * h;
VV(1,2) = -i * 1/2 * h;
VV(3,4) = -i * 1/2 * h;
VV(4,3) = -i * 1/2 * h;

VV(6,5) = -i * 1/2 * h;
VV(5,6) = -i * 1/2 * h;
VV(8,7) = -i * 1/2 * h;
VV(7,8) = -i * 1/2 * h;

HH(7,1) = -i * 1/2 * h;
HH(8,2) =  i * 1/2 * h;
HH(5,3) =  i * 1/2 * h;
HH(6,4) = -i * 1/2 * h;

HH(3,5) = -i * 1/2 * h;
HH(4,6) =  i * 1/2 * h;
HH(1,7) =  i * 1/2 * h;
HH(2,8) = -i * 1/2 * h;

for ii=1:nx_em_b
  VV_m_b(ii,ii) = div_v_b(ii);
  HH_m_b(ii,ii) = v_over_h_b(ii) * div_h_b(ii);
end
 
MM_Ez_kron = kron(MM_Ez,Hm_first_deriv_fd_b);
MM_By_kron = kron(MM_By,Hm_first_deriv_fd_b);
 
VV_kron = kron(VV,VV_m_b);
HH_kron = kron(HH,HH_m_b);

%div_v_b
%div_h_b
%v_over_h_b
%VV_m_b
%HH_m_b

Hm_em_b = MM_Ez_kron + MM_By_kron + VV_kron + HH_kron;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% eigenmodes and eigenvectors of Hm_em_b


%[eig_Phi_xx_em_b,eig_val] = eigs(Hm_em_b,5,'lm');
%[eig_Phi_xx_em_b,eig_val] = eig(Hm_em_b);

%eig_number = nx_em_b
%for ii=1:eig_number
%  eig_mode_array_b(ii,1) = eig_val(ii,ii);
%end


%for ii=1:eig_number
%  for jj=1:nx_em_b
%    eig_Fx_plus_b(jj,ii)     = -1/2*eig_Phi_xx_em_b(jj,ii)             + 1/2*eig_Phi_xx_em_b(3*nx_em_b+jj,ii);
%    eig_Fy_plus_b(jj,ii)     = -i/2*eig_Phi_xx_em_b(jj,ii)             - i/2*eig_Phi_xx_em_b(3*nx_em_b+jj,ii);
%    eig_Fz_plus_b(jj,ii)     =  1/2*eig_Phi_xx_em_b(nx_em_b+jj,ii)     + 1/2*eig_Phi_xx_em_b(2*nx_em_b+jj,ii);
%  end
%  for jj=1:nx_em_b
%    eig_Ex_b(jj,ii) = real(sqrt(2/epsilon_array_b(jj))*eig_Fx_plus_b(jj,ii));
%    eig_Ey_b(jj,ii) = real(sqrt(2/epsilon_array_b(jj))*eig_Fy_plus_b(jj,ii));
%    eig_Ez_b(jj,ii) = real(sqrt(2/epsilon_array_b(jj))*eig_Fz_plus_b(jj,ii));
%    eig_Bx_b(jj,ii) = imag(2*mu_array_b(jj)*eig_Fx_plus_b(jj,ii));
%    eig_By_b(jj,ii) = imag(2*mu_array_b(jj)*eig_Fy_plus_b(jj,ii));
%    eig_Bz_b(jj,ii) = imag(2*mu_array_b(jj)*eig_Fz_plus_b(jj,ii));
%  end
%end


%fig7=figure(7);
%plot(xx_em_b,eig_Ez_b(:,2));
%fig8=figure(8);
%plot(xx_em_b,eig_By_b(:,1));
%fig9=figure(9);
%plot(xx_em_b,eig_Ex_b(:,1));

%eig_mode_array_b


% finite difference operator for EM hamiltonian inside the box
%for jj=1:(nx_em)
%  if (jj < nx_em),
%    first_deriv_fd_b(jj+border_em,jj+border_em+1) =   c_0 * 1/(2*dx_em);
%  end
%  if (jj > 1),
%    first_deriv_fd_b(jj+border_em,jj+border_em-1) = - c_0 * 1/(2*dx_em);
%  end
%end
%first_deriv_fd_mirror = first_deriv_fd_b;

% finite difference operator for EM Hamiltonian outside the box in border region
%if(border_em > 0),
%  first_deriv_fd_b(border_em        ,border_em      +1) =  c_0 * 1/(2*dx_em);
%  first_deriv_fd_b(border_em+nx_em  ,border_em+nx_em+1) =  c_0 * 1/(2*dx_em);
%  first_deriv_fd_b(border_em      +1,border_em        ) = -c_b * 1/(2*dx_em);
%  first_deriv_fd_b(border_em+nx_em+1,border_em+nx_em  ) = -c_b * 1/(2*dx_em);
%end
%for jj=1:(border_em)
%  if (jj < border_em),
%    first_deriv_fd_b(jj,jj+1) =   c_b * 1/(2*dx_em);
%  end
%  if (jj+border_em+nx_em < 2*border_em+nx_em)
%    first_deriv_fd_b(jj+border_em+nx_em,jj+border_em+nx_em+1) =   c_b * 1/(2*dx_em);
%  end
%  if (jj > 1),
%    first_deriv_fd_b(jj,jj-1) = - c_b * 1/(2*dx_em);
%  end
%  if (jj+border_em+nx_em > border_em+nx_em+1)
%    first_deriv_fd_b(jj+border_em+nx_em,jj+border_em+nx_em-1) = - c_b * 1/(2*dx_em);
%  end
%end

%for jj=1:border_em
%  first_deriv_fd_mirror(                jj,                jj) = 0;
%  first_deriv_fd_mirror(border_em+nx_em+jj,border_em+nx_em+jj) = 0;
%end
%first_deriv_fd_mirror(border_em      +1,border_em)         = - c_b * 1/(2*dx_em);
%first_deriv_fd_mirror(border_em+nx_em  ,border_em+nx_em+1) =   c_b * 1/(2*dx_em);

%if (boundary_em == 0)
%  E_first_deriv_fd_b = H_first_deriv_fd_b;
%  B_first_deriv_fd_b = H_first_deriv_fd_b;
%elseif (boundary_em == 1)
%  E_first_deriv_fd_b = H_first_deriv_fd_b;
%  B_first_deriv_fd_b = H_first_deriv_fd_mirror;
%elseif (boundary_em == 2)
%  E_first_deriv_fd_b = H_first_deriv_fd_mirror;
%  B_first_deriv_fd_b = H_first_deriv_fd_b;
%end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Schroedinger Hamiltonian

for ii=1:nx_ma
  Phi_xx_ma(ii,1) = 2 *(pi)^(-1/4)/(sqrt(sigma_ma)) * exp(-(xx_ma(ii)^2)/(4*sigma_ma^2));
end
Hm_ma    = -h^2/(2*m)*second_deriv_fd;
unitm_ma = sparse(eye(nx_ma));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial EM-field

fprintf('Setting up electromagnetic field ...\n\n');

% start index of the initial signal
for ii=1:nx_em
  if xx_em(ii) > (signal_pos - double(signallength)/2),
    sig_idx_1 = ii;
    break;
  end
end
% end indexo of the initial signal
for ii=1:nx_em
  if xx_em(ii) > (signal_pos + double(signallength)/2),
    sig_idx_2 = ii-1;
    break;
  end
end

% initialization of the electromagnetic field
Ey = zeros(nx_em,1);
Ez = zeros(nx_em,1);
By = zeros(nx_em,1);
Bz = zeros(nx_em,1);
for ii=sig_idx_1:sig_idx_2
  if (em_wave_shape == 1)
    sinus_wave = sin(k_em*(xx_em(ii)-signal_pos));
    Ey(ii,1) = 0;
    Ez(ii,1) =          sinus_wave;
    By(ii,1) = -1/v_0 * sinus_wave;
    Bz(ii,1) = 0;
  end
  if (em_wave_shape == 2) 
    gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_em)) * exp(-((xx_em(ii)-signal_pos)^2)/(4*sigma_em^2));
    Ey(ii,1) = 0; 
    Ez(ii,1) =          gauss_dist;
    By(ii,1) = -1/v_0 * gauss_dist;
    Bz(ii,1) = 0;
  end
  if (em_wave_shape == 3)
    sinus_wave = sin(k_em*xx_em(ii));
    gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_em)) * exp(-((xx_em(ii)-signal_pos)^2)/(4*sigma_em^2));
    Ey(ii,1) = 0;
    Ez(ii,1) =          sinus_wave * gauss_dist;  
    By(ii,1) = -1/v_0 * sinus_wave * gauss_dist;
    Bz(ii,1) = 0;
  end
end

for ii=1:nx_em
  Ey_b(ii+border_em,1) = Ey(ii,1);
  Ez_b(ii+border_em,1) = Ez(ii,1);
  By_b(ii+border_em,1) = By(ii,1);
  Bz_b(ii+border_em,1) = Bz(ii,1);
end

for ii=1:border_em
  Ey_b(                ii) = 0;
  Ey_b(border_em+nx_em+ii) = 0;
  Ez_b(                ii) = 0;
  Ez_b(border_em+nx_em+ii) = 0;
  By_b(                ii) = 0;
  By_b(border_em+nx_em+ii) = 0;
  Bz_b(                ii) = 0;
  Bz_b(border_em+nx_em+ii) = 0;
end

% Riemann-Silberstein vector    
for ii=1:nx_em_b
  Fy_b_plus( ii,1) = sqrt(epsilon_array_b(ii)/2) * Ey_b(ii) + i / sqrt(2*mu_array_b(ii)) * By_b(ii);
  Fy_b_minus(ii,1) = sqrt(epsilon_array_b(ii)/2) * Ey_b(ii) - i / sqrt(2*mu_array_b(ii)) * By_b(ii);
  Fz_b_plus( ii,1) = sqrt(epsilon_array_b(ii)/2) * Ez_b(ii) + i / sqrt(2*mu_array_b(ii)) * Bz_b(ii);
  Fz_b_minus(ii,1) = sqrt(epsilon_array_b(ii)/2) * Ez_b(ii) - i / sqrt(2*mu_array_b(ii)) * Bz_b(ii);
end

% initialization of the spinor vector
for ii=1:nx_em_b
  % plus Spinor
  Phi_xx_em_b(          ii,1) =   i * Fy_b_plus(ii,1); 
  Phi_xx_em_b(  nx_em_b+ii,1) =       Fz_b_plus(ii,1); 
  Phi_xx_em_b(2*nx_em_b+ii,1) =       Fz_b_plus(ii,1); 
  Phi_xx_em_b(3*nx_em_b+ii,1) =   i * Fy_b_plus(ii,1);
  % minus Spinor
  Phi_xx_em_b(4*nx_em_b+ii,1) =  -i * Fy_b_minus(ii,1);
  Phi_xx_em_b(5*nx_em_b+ii,1) =       Fz_b_minus(ii,1);
  Phi_xx_em_b(6*nx_em_b+ii,1) =       Fz_b_minus(ii,1);
  Phi_xx_em_b(7*nx_em_b+ii,1) =  -i * Fy_b_minus(ii,1);
end



unitm_em_b = sparse(eye(8*nx_em_b));


figure(1);
set(gcf,'Visible', 'off'); 
plot(xx_ma,abs(Phi_xx_ma));
filename = './figures/wavefunction/wave_packet_t=0.png';
print(fig1,'-dpng',filename);
figure(2);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Ez_b);
filename = './figures/E_field_z_b/E_field_z_b_t=0.png';
print(fig2,'-dpng',filename);
figure(3);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Bz_b);
filename = './figures/B_field_z_b/B_field_z_b_t=0.png';
print(fig3,'-dpng',filename);
figure(4);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Ey_b);
time_str = int2str(0);
filename = './figures/E_field_y_b/E_field_y_b_t=0.png';
print(fig4,'-dpng',filename);
figure(5);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,By_b);
filename = './figures/B_field_y_b/B_field_y_b_t=0.png';
print(fig5,'-dpng',filename);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Crank Nicolson for Schroedinger and maxwell equation 

fprintf('Schroedinger and electromagnetic field propagation ...\n\n');

for ii=1:timesteps

  if (mod(ii,10) == 0)
    steps_string = int2str(ii);
    combinedStr = strcat('Timesteps done:    ',steps_string,' ...\n\n');
    fprintf(combinedStr)
  end
 
  if (calc_ma == 1)

    % Schroedinger propagation
    Phi_xx_ma_old = Phi_xx_ma;
    b         = (unitm_ma - i * Hm_ma*dt) * Phi_xx_ma;
    Phi_xx_ma = (unitm_ma + i * Hm_ma*dt) \ b;

    time_str = int2str(ii);
    filename = './figures/wavefunction/wave_packet_t='
    combinedStr = strcat(filename,time_str,'.png');
    print(fig1,'-dpng',combinedStr);
  
  end

  % Maxwell propagation
  if (calc_em == 1)

    % Maxwell propagation
    Phi_xx_em_b_old = Phi_xx_em_b;
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update equation 
    if (prop_mode_em == 0) 
      for jj=2:(nx_em_b-1)
        Phi_xx_em_b(          jj,1) = Phi_xx_em_b(          jj,1) - dt*c/dx_em * (Phi_xx_em_b(2*nx_em_b+jj+1,1) - Phi_xx_em_b(2*nx_em_b+jj-1,1));
        Phi_xx_em_b(  nx_em_b+jj,1) = Phi_xx_em_b(  nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(3*nx_em_b+jj+1,1) - Phi_xx_em_b(3*nx_em_b+jj-1,1));
        Phi_xx_em_b(2*nx_em_b+jj,1) = Phi_xx_em_b(2*nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(          jj+1,1) - Phi_xx_em_b(          jj-1,1));
        Phi_xx_em_b(3*nx_em_b+jj,1) = Phi_xx_em_b(3*nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(  nx_em_b+jj+1,1) - Phi_xx_em_b(  nx_em_b+jj-1,1));
      end
    end
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % crank nicolson  
    if (prop_mode_em == 1)

      for jj=1:nx_em_b
        Fx_b(jj)     = -1/2*Phi_xx_em_b(jj)             + 1/2*Phi_xx_em_b(3*nx_em_b+jj);
        Fy_b(jj)     = -i/2*Phi_xx_em_b(jj)             - i/2*Phi_xx_em_b(3*nx_em_b+jj);
        Fz_b(jj)     =  1/2*Phi_xx_em_b(nx_em_b+jj)     + 1/2*Phi_xx_em_b(2*nx_em_b+jj);
      end

      b           = (unitm_em_b - i*(Hm_em_b)*dt) * Phi_xx_em_b;
      Phi_xx_em_b = (unitm_em_b + i*(Hm_em_b)*dt) \ b;

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % fourth order taylor expansion
    if(prop_mode_em == 2)

      taylor_order = 4;
      zfact = 1;
      h_exp_psi = Phi_xx_em_b;
      % Taylor series for matrix exponential acting on a vector
      for jj = 1:taylor_order
        zfact = zfact*(-sqrt(-1)*dt)/jj;
        hzpsi = Hm_em_b*Phi_xx_em_b; % Hm_em_E*Phi_E + Hm_em_B*Phi_B;
        h_exp_psi = h_exp_psi + zfact*hzpsi;
        if(jj ~= taylor_order) 
          zpsi = hzpsi; 
        end
      end
      Phi_xx_em_b = h_exp_psi; 

      for jj=1:nx_em_b
        Phi_E_b(          jj,1) =  i/2 * imag(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_E_b(  nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_E_b(2*nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_E_b(3*nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_B_b(          jj,1) =  1/2 * real(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_B_b(  nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_B_b(2*nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));       
        Phi_B_b(3*nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
      end

    end

    for jj=1:nx_em_b
      Fx_plus_b(jj)     = -1/2*Phi_xx_em_b(jj)             + 1/2*Phi_xx_em_b(3*nx_em_b+jj);
      Fy_plus_b(jj)     = -i/2*Phi_xx_em_b(jj)             - i/2*Phi_xx_em_b(3*nx_em_b+jj);
      Fz_plus_b(jj)     =  1/2*Phi_xx_em_b(nx_em_b+jj)     + 1/2*Phi_xx_em_b(2*nx_em_b+jj);
    end

    for jj=1:nx_em
      Fx_plus(jj) = Fx_plus_b(jj+border_em);
      Fy_plus(jj) = Fy_plus_b(jj+border_em);
      Fz_plus(jj) = Fz_plus_b(jj+border_em);
    end

    for jj=1:nx_em_b
      Ex_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fx_plus_b(jj));
      Ey_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fy_plus_b(jj));
      Ez_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fz_plus_b(jj));
      Bx_b(jj) = imag(2*mu_array_b(jj)*Fx_plus_b(jj));
      By_b(jj) = imag(2*mu_array_b(jj)*Fy_plus_b(jj));
      Bz_b(jj) = imag(2*mu_array_b(jj)*Fz_plus_b(jj));
    end
   
    for jj=1:nx_em
      Ex(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fx_plus_b(jj+border_em));
      Ey(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fy_plus_b(jj+border_em));
      Ez(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fz_plus_b(jj+border_em));
      Bx(jj) = imag(2*mu_array_b(jj+border_em)*Fx_plus_b(jj+border_em));
      By(jj) = imag(2*mu_array_b(jj+border_em)*Fy_plus_b(jj+border_em));
      Bz(jj) = imag(2*mu_array_b(jj+border_em)*Fz_plus_b(jj+border_em));
    end

    time_array(ii)     = double(ii)*dt;

    energy_b           = dot(Fx_plus_b,Fx_plus_b)+dot(Fy_plus_b,Fy_plus_b)+dot(Fz_plus_b,Fz_plus_b) * dx_em; 
    energy_b_array(ii) = energy_b;


    figure(6);
    set(gcf,'Visible','off');
    plot(time_array,energy_b_array);
    filename = './figures/energy.png';
    print(fig6,'-dpng',filename); 
    

    figure(2);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Ez);
    time_str = int2str(ii);
    filename = './figures/E_field_z/E_field_z_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig2,'-dpng',combinedStr);

    figure(3);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Bz);
    time_str = int2str(ii);
    filename = './figures/B_field_z/B_field_z_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig3,'-dpng',combinedStr);

    figure(4);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Ey);
    time_str = int2str(ii);
    filename = './figures/E_field_y/field_y_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig4,'-dpng',combinedStr);

    figure(5);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,By);
    time_str = int2str(ii);
    filename = './figures/B_field_y/B_field_y_t=';
    combinedStr =  strcat(filename,time_str,'.png');
    print(fig5,'-dpng',combinedStr);

    
    figure(2);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Ez_b);
    time_str = int2str(ii);
    filename = './figures/E_field_z_b/E_field_z_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig2,'-dpng',combinedStr);

    figure(3);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Bz_b);
    time_str = int2str(ii);
    filename = './figures/B_field_z_b/B_field_z_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig3,'-dpng',combinedStr);

    figure(4);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Ey_b);
    time_str = int2str(ii);
    filename = './figures/E_field_y_b/E_field_y_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig4,'-dpng',combinedStr);

    figure(5);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,By_b);
    time_str = int2str(ii);
    filename = './figures/B_field_y_b/B_field_y_b_t=';
    combinedStr =  strcat(filename,time_str,'.png');
    print(fig5,'-dpng',combinedStr);
  
  end

end





