clear;
more off;

Nx   =  20;
Ny   =  20;
xmin = -2;
xmax =  2;
ymin = -2;
ymax =  2;
dx   =  (xmax-xmin)/double(Nx);
dy   =  (ymax-ymin)/double(Ny);

xlength = xmax-xmin;
ylength = ymax-ymin;


xpoints = (xmin+dx/2):dx:(xmax-dx/2);
ypoints = (ymin+dy/2):dy:(ymax-dy/2);

dt=0.01;

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
for ii=1:Nx
  for jj=1:Ny
    g1(ii,jj)       = exp(-(xpoints(ii))^2 -4*(ypoints(jj))^2);
    f1(ii,jj)       = exp(-2*(xpoints(ii))^2 -6*(ypoints(jj))^2);
    f1g1_0(ii,jj)     = f1(ii,jj);
    f1g1_1(ii,jj)     = - 4 * exp(-3*(xpoints(ii))^2 -10*(ypoints(jj))^2) * (xpoints(ii)+3*ypoints(jj)) ;
    f1g1_2(ii,jj)     =   8 * exp(-4*(xpoints(ii))^2 -14*(ypoints(jj))^2) * ( -2+(xpoints(ii)+3*ypoints(jj))*(3*xpoints(ii)+10*ypoints(jj)) ) ;
    f1g1_3(ii,jj)     = - 8 * exp(-5*(xpoints(ii))^2 -18*(ypoints(jj))^2) ...
                            * (24*(xpoints(ii))^3+236*(xpoints(ii))^2*ypoints(jj)+15*ypoints(jj)*(-9+56*(ypoints(jj))^2) + xpoints(ii)*(-41+772*(ypoints(jj))^2)) ;
    f1g1_4(ii,jj)     =  16 * exp(-6*(xpoints(ii))^2 -22*(ypoints(jj))^2) ...
                            * (88+120*(xpoints(ii))^4 + 1612*(xpoints(ii))^3*ypoints(jj)-4076*(ypoints(jj))^2+15120*(ypoints(jj))^4+3*xpoints(ii)*ypoints(jj)  ...
                            *(-807+6032*(ypoints(jj))^2)+(xpoints(ii))^2*(-359+8108*(ypoints(jj))^2) ) ;
    f1g1_s(ii,jj)     = f1g1_0(ii,jj) + f1g1_1(ii,jj)*dt + 1/2*f1g1_2(ii,jj)*dt^2 + 1/6*f1g1_3(ii,jj)*dt^3 + 1/24*f1g1_4(ii,jj)*dt^4 ;
  end
end

for ii=0:(Nx/2)
  px(ii+1,1) = 2*pi/(double(xlength)) * double(ii);
end
for ii=(Nx/2+1):(Nx-1)
  px(ii+1,1) = 2*pi/(double(xlength)) * double(ii-Nx);
end

for jj=0:(Ny/2)
  py(jj+1,1) = 2*pi/(double(ylength)) * double(jj);
end
for jj=(Ny/2+1):(Ny-1)
  py(jj+1,1) = 2*pi/(double(ylength)) * double(jj-Ny);
end

%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

for ii=1:Nx
  for jj=1:Ny
    conv_arry{ii}{jj} = zeros(Nx,Ny);
    for ii_kk=1:Nx
      for jj_kk=1:Ny
        for ii_mm=1:Nx
          for jj_mm=1:Ny
            conv_arry{ii}{jj}(ii_kk,jj_kk) = conv_arry{ii}{jj}(ii_kk,jj_kk) + 1/sqrt(Nx) * 1/sqrt(Ny) * g1(ii_mm,jj_mm)      ...
                                                                            * exp(-i*2*pi*(ii_mm-1)*((ii-1)-(ii_kk-1))/Nx)   ...
                                                                            * exp(-i*2*pi*(jj_mm-1)*((jj-1)-(jj_kk-1))/Ny)   ;
          end
        end
      end
    end
  end
end

idx1=0;
for ii=1:Nx
  for jj=1:Ny
    idx2 = 0;
    idx1 = idx1+1;
    map_ii(idx1) = ii;
    map_jj(idx1) = jj;
    for ii_kk=1:Nx
      for jj_kk=1:Ny
        idx2 = idx2+1;
        conv_matrix(idx1,idx2) = conv_arry{ii}{jj}(ii_kk,jj_kk) * i * (px(ii_kk,1) + py(jj_kk,1));
      end
    end
  end
end

conv_matrix_exp = sqrt(Nx)*sqrt(Ny) * expm(dt*1/sqrt(Nx)*1/sqrt(Ny)*conv_matrix);

for idx1=1:Nx*Ny
  for idx2=1:Nx*Ny
    conv_exp{map_ii(idx1)}{map_jj(idx1)}(map_ii(idx2),map_jj(idx2)) = conv_matrix_exp(idx1,idx2);
  end
end

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
FT_f1 = 1/sqrt(Nx) * 1/sqrt(Ny) * fftn(f1);
FT_g1 = 1/sqrt(Nx) * 1/sqrt(Ny) * fftn(g1);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


for ii=1:Nx
  for jj=1:Ny
    FT_f1g1_x(ii,jj) = 0;
    for ii_kk=1:Nx
      for jj_kk=1:Ny
        FT_f1g1_x(ii,jj) = FT_f1g1_x(ii,jj) + conv_exp{ii}{jj}(ii_kk,jj_kk) * FT_f1(ii_kk,jj_kk);
      end
    end
  end
end


%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
%FT_f1g1_x = zeros(Nx,1);
%for qq=1:Nx
%  for kk=1:Nx
%    FT_f1g1_x(qq,1) = FT_f1g1_x(qq,1) + conv_arry_p_x(qq,kk) * FT_f1(kk,1);
%  end
%end
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
conv_f1g1_x = ifftn(FT_f1g1_x);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

%figure(1);
%mesh(xpoints,ypoints,f1g1_1)

%figure(2);
%mesh(xpoints,ypoints,real(conv_f1g1_1))

%figure(3);
%mesh(xpoints,ypoints,f1g1_1-real(conv_f1g1_1))

%figure(4);
%mesh(xpoints,ypoints,f1g1_2)

%figure(5);
%mesh(xpoints,ypoints,real(conv_f1g1_2))

%figure(6);
%mesh(xpoints,ypoints,f1g1_2-real(conv_f1g1_2))

%figure(7);
%mesh(xpoints,ypoints,f1g1_3)

%figure(8);
%mesh(xpoints,ypoints,real(conv_f1g1_3))

%figure(9);
%mesh(xpoints,ypoints,f1g1_3-real(conv_f1g1_3))

%figure(10);
%mesh(xpoints,ypoints,f1g1_4)

%figure(11);
%mesh(xpoints,ypoints,real(conv_f1g1_4))

%figure(12);
%mesh(xpoints,ypoints,f1g1_4-real(conv_f1g1_4))

figure(13);
mesh(xpoints,ypoints,f1g1_s)

figure(14);
mesh(xpoints,ypoints,real(conv_f1g1_x))

figure(15);
mesh(xpoints,ypoints,f1g1_s-real(conv_f1g1_x))




