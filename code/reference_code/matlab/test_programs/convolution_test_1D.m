clear;
more off;

Nx   =  8;
xmin = -2;
xmax =  2;
dx   =  (xmax-xmin)/double(Nx);
a    =  1;
b    =  2;


xpoints = (xmin+dx/2):dx:(xmax-dx/2);

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
for ii=1:Nx
  f1(ii,1)   = exp(-(a*xpoints(ii))^2);
  g1(ii,1)   = xpoints(ii)^4; % exp(-(b*xpoints(ii))^2);
  f1g1(ii,1) = exp(-(a*xpoints(ii))^2) * xpoints(ii)^4 % exp(-(b*xpoints(ii))^2);
end
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

% delta test
for ii=1:Nx
  for jj=1:Nx
    delta(ii,jj) = 0;
    for kk=1:Nx
      delta(ii,jj) = delta(ii,jj) + 1/Nx * exp(i*2*pi*((ii-1)-(jj-1))*kk/Nx);
    end
  end
end


% fourier transform 
for ii=1:Nx 
  FT_f1(ii,1) = 0;
  for jj=1:Nx
    FT_f1(ii,1) = FT_f1(ii,1) + 1/sqrt(Nx) * f1(jj) * exp(-i*2*pi*(ii-1)*(jj-1)/Nx);
  end
end

for ii=1:Nx 
  FT_g1(ii,1) = 0;
  for jj=1:Nx
    FT_g1(ii,1) = FT_g1(ii,1) + 1/sqrt(Nx) * g1(jj) * exp(-i*2*pi*(ii-1)*(jj-1)/Nx);
  end
end

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
ref_FT_f1 = 1/sqrt(Nx) * fftn(f1);
ref_FT_g1 = 1/sqrt(Nx) * fftn(g1);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

% fourier transform reference
for ii=1:Nx
  FT_f1_g1(ii,1) = 0;
  for jj=1:Nx
    FT_f1_g1(ii,1) = FT_f1_g1(ii,1) + f1(jj,1) * g1(jj,1) * exp(-i*2*pi*(ii-1)*(jj-1)/Nx);
  end
end

% fourier transform reference equivalence 1
for ii=1:Nx
  FT_f1_g1_1(ii,1) = 0;
  for jj=1:Nx
    for kk=1:Nx
      FT_f1_g1_1(ii,1) = FT_f1_g1_1(ii,1) + f1(jj,1) * g1(kk,1) * exp(-i*2*pi*(ii-1)*(kk-1)/Nx) * delta(jj,kk) ;
    end
  end
end

% fourier transform reference equivalence 2
for qq=1:Nx
  FT_f1_g1_2(qq,1) = 0;
  for kk=1:Nx
    for nn=1:Nx
      for mm=1:Nx
        FT_f1_g1_2(qq,1) = FT_f1_g1_2(qq,1)                                                 ...
                       + 1/sqrt(Nx) * f1(nn,1)*exp(-i*2*pi*(kk-1)*(nn-1)/Nx)                ...
                       * 1/sqrt(Nx) * g1(mm,1)*exp(-i*2*pi*(mm-1)*((qq-1)-(kk-1))/Nx)       ;
      end
    end
  end
end

% fourier transform reference equivalence 3
for qq=1:Nx
  FT_f1_g1_3(qq,1) = 0;
  for kk=1:Nx
    for mm=1:Nx
      FT_f1_g1_3(qq,1) = FT_f1_g1_3(qq,1) + FT_f1(kk,1) * 1/sqrt(Nx) * g1(mm,1)*exp(-i*2*pi*(mm-1)*((qq-1)-(kk-1))/Nx);
    end
  end
end


%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
% fourier transform reference equivalence 4
for qq=1:Nx
  conv_array{qq} = zeros(Nx,1);
  for kk=1:Nx
    for mm=1:Nx
      conv_array{qq}(kk,1) = conv_array{qq}(kk,1) +1/sqrt(Nx) * g1(mm,1)*exp(-i*2*pi*(mm-1)*((qq-1)-(kk-1))/Nx);
    end
  end
end
for qq=1:Nx
  FT_f1_g1_4(qq,1) = 0;
  for kk=1:Nx
    FT_f1_g1_4(qq,1) = FT_f1_g1_4(qq,1) + conv_array{qq}(kk,1) * FT_f1(kk,1);
  end
end
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

% fourier transform reference equivalence 5
  conv_arry = zeros(Nx,Nx);
for qq=1:Nx
  for kk=1:Nx
    for mm=1:Nx
      conv_arry(qq,kk) = conv_arry(qq,kk) +1/sqrt(Nx) * g1(mm,1)*exp(-i*2*pi*(mm-1)*((qq-1)-(kk-1))/Nx);
    end
  end
end
for qq=1:Nx
  FT_f1_g1_5(qq,1) = 0;
  for kk=1:Nx
    FT_f1_g1_5(qq,1) = FT_f1_g1_5(qq,1) + conv_arry(qq,kk) * FT_f1(kk,1);
  end
end



FT_f1;

ref_FT_f1;

FT_g1;

ref_FT_g1;


FT_f1_g1;

FT_f1_g1_1

FT_f1_g1_2;

FT_f1_g1_3;

FT_f1_g1_4

FT_f1_g1_5


f1g1;

%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
ref_f1_g1 = ifftn(FT_f1_g1_5);
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

figure(1);
plot(xpoints,f1g1)

figure(2);
plot(xpoints,ref_f1_g1)









