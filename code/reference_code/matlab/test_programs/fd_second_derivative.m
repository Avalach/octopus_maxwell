% Copyright (C) 2013 Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function  [second_derivative, second_derivative_periodic] = fd_second_derivative(nx, dx, finite_difference_order) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% periodic boundary condition
switch finite_difference_order
  case 2
    % 2nd order finite difference
    for jj=1:nx
      second_derivative_periodic(jj,jj) = 1/(dx^2)*(-2);
      second_derivative_periodic(jj,mod(jj,nx)+1) = 1/(dx^2)*( 1);
      second_derivative_periodic(mod(jj,nx)+1,jj) = 1/(dx^2)*( 1);
    end
  case 4
    % 4th order finite difference
    for jj=1:nx
      second_derivative_periodic(jj,jj) = 1/(dx^2)*(-5.0/2.0);
      second_derivative_periodic(jj,mod(jj,nx)+1) = 1/(dx^2)*( 4.0/3.0);
      second_derivative_periodic(mod(jj,nx)+1,jj) = 1/(dx^2)*( 4.0/3.0);
      second_derivative_periodic(jj,mod(jj+1,nx)+1) = 1/(dx^2)*( -1.0/12.0);
      second_derivative_periodic(mod(jj+1,nx)+1,jj) = 1/(dx^2)*( -1.0/12.0);
    end
  case 6
    % 6th order finite difference
    for jj=1:nx
      second_derivative_periodic(jj,jj) = 1/(dx^2)*( -49.0/18.0);
      second_derivative_periodic(jj,mod(jj,nx)+1) = 1/(dx^2)*( 3.0/2.0);
      second_derivative_periodic(mod(jj,nx)+1,jj) = 1/(dx^2)*( 3.0/2.0);
      second_derivative_periodic(jj,mod(jj+1,nx)+1) = 1/(dx^2)*( -3.0/20.0);
      second_derivative_periodic(mod(jj+1,nx)+1,jj) = 1/(dx^2)*( -3.0/20.0);
      second_derivative_periodic(jj,mod(jj+2,nx)+1) = 1/(dx^2)*( 1.0/90.0);
      second_derivative_periodic(mod(jj+2,nx)+1,jj) = 1/(dx^2)*( 1.0/90.0);
    end
  case 8
    % 8th order finite difference
    for jj=1:nx
      second_derivative_periodic(jj,jj) = 1/(dx^2)*( -205.0/72.0);
      second_derivative_periodic(jj,mod(jj,nx)+1) = 1/(dx^2)*( 8.0/5.0);
      second_derivative_periodic(mod(jj,nx)+1,jj) = 1/(dx^2)*( 8.0/5.0);
      second_derivative_periodic(jj,mod(jj+1,nx)+1) = 1/(dx^2)*( -1.0/5.0);
      second_derivative_periodic(mod(jj+1,nx)+1,jj) = 1/(dx^2)*( -1.0/5.0);
      second_derivative_periodic(jj,mod(jj+2,nx)+1) = 1/(dx^2)*( 8.0/315.0);
      second_derivative_periodic(mod(jj+2,nx)+1,jj) = 1/(dx^2)*( 8.0/315.0);
      second_derivative_periodic(jj,mod(jj+3,nx)+1) = 1/(dx^2)*( -1.0/560.0);
      second_derivative_periodic(mod(jj+3,nx)+1,jj) = 1/(dx^2)*( -1.0/560.0);
    end
  otherwise
    fprintf('Error: unknown finite-difference order');
    exit;
end
second_derivative_periodic = sparse(second_derivative_periodic);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% zero boundary condition
switch finite_difference_order
  case 2
    % 2nd order finite difference
    for jj=1:nx
      second_derivative(jj,jj) = 1/(dx^2)*(-2);
      if (jj < nx),
        second_derivative(jj,jj+1) = 1/(dx^2)*( 1);
      end
      if (jj > 1),
        second_derivative(jj,jj-1) = 1/(dx^2)*( 1);
      end
    end
  case 4
    % 4th order finite difference
    for jj=1:nx
      second_derivative(jj,jj) = 1/(dx^2)*( -5.0/2.0);
      if (jj < nx),
        second_derivative(jj,jj+1) = 1/(dx^2)*( 4.0/3.0);
      end
      if (jj > 1),
        second_derivative(jj,jj-1) = 1/(dx^2)*( 4.0/3.0);
      end
      if (jj < nx - 1),
        second_derivative(jj,jj+2) = 1/(dx^2)*( -1.0/12.0);
      end
      if (jj > 2),
        second_derivative(jj,jj-2) = 1/(dx^2)*( -1.0/12.0);
      end
    end
  case 6
    % 6th order finite difference
    for jj=1:nx
      second_derivative(jj,jj) = 1/(dx^2)*( -49.0/18.0);
      if (jj < nx),
        second_derivative(jj,jj+1) = 1/(dx^2)*( 3.0/2.0);
      end
      if (jj > 1),
        second_derivative(jj,jj-1) = 1/(dx^2)*( 3.0/2.0);
      end
      if (jj < nx - 1),
        second_derivative(jj,jj+2) = 1/(dx^2)*( -3.0/20.0);
      end
      if (jj > 2),
        second_derivative(jj,jj-2) = 1/(dx^2)*( -3.0/20.0);
      end
      if (jj < nx - 2),
        second_derivative(jj,jj+3) = 1/(dx^2)*( 1.0/90.0);
      end
      if (jj > 3),
        second_derivative(jj,jj-3) = 1/(dx^2)*( 1.0/90.0);
      end
    end
  case 8
    % 8th order finite difference
    for jj=1:nx
      second_derivative(jj,jj) = 1/(dx^2)*( -205.0/72.0);
      if (jj < nx),
        second_derivative(jj,jj+1) = 1/(dx^2)*( 8.0/5.0);
      end
      if (jj > 1),
        second_derivative(jj,jj-1) = 1/(dx^2)*( 8.0/5.0);
      end
      if (jj < nx - 1),
        second_derivative(jj,jj+2) = 1/(dx^2)*( -1.0/5.0);
      end
      if (jj > 2),
        second_derivative(jj,jj-2) = 1/(dx^2)*( -1.0/5.0);
      end
      if (jj < nx - 2),
        second_derivative(jj,jj+3) = 1/(dx^2)*( 8.0/315.0);
      end
      if (jj > 3),
        second_derivative(jj,jj-3) = 1/(dx^2)*( 8.0/315.0);
      end
      if (jj < nx - 3),
        second_derivative(jj,jj+4) = 1/(dx^2)*( -1.0/560.0);
      end
      if (jj > 4),
        second_derivative(jj,jj-4) = 1/(dx^2)*( -1.0/560.0);
      end
    end
  otherwise
    fprintf('Error: unknown finite-difference order');
    exit;
end
second_derivative = sparse(second_derivative);
