% Copyright (C) 2013 Rene Jestaedt, Heiko Appel
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2, or (at your option)
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
% 02111-1307, USA.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
more off;

fprintf('\n');
fprintf('Starting program ...\n\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters of the real space grid mesh
boxlength_ma_x     = 50;
boxlength_ma_y     = 1;
boxlength_ma_z     = 1;
boxlength_em_x     = 100;
boxlenght_em_y     = 1;
boxlength_em_z     = 1;
n_em_x             = 256;
n_em_y             = 256;
n_em_z             = 256;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameter for the time evolution calculation
timesteps          = 300;
dt                 = 0.1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% physical parameters

alpha = 7.2973525698*10^(-3);
h=1;
c_0           = 1/alpha;
epsilon_0     = 1/(4*pi);
mu_0          = 1/(c_0^2*epsilon_0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters for the initial EM signal

signallength_em_x  = 30;
signallength_em_y  = 30;
signallegnth_em_z  = 30;
sigma_ma           = 1/2;
sigma_em           = 3;
k_em               = 1/2;

%calc_ma       = 0; % 1 = calculation of matter on, 0 = calculation of matter off
%calc_em       = 1; % 1 = calculation of EM field on, 0 = calculation of EM field off
%em_wave_shape = 2; % 1 = sinus shape, 2 = gauss shape, 3 = sinus and gauss envelope
%boundary_em   = 0; % 1 = PEC, 2 = PMC, 3 = PML
%prop_mode_em  = 3; % 0 = update equation, 1 = crank nicholson, 2 = fourth order taylor of exp, 3 = fast Fourier transform


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% system calculated parameters

border_em = int64(borderlength_em/dx_em)
nx_em     = int64(boxlength_em/dx_em)
nx_em_b   = nx_em+2*border_em
nx_ma     = int64(boxlength_ma/dx_ma);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup of figures

set(0,'Units','pixels') 
scnsize = get(0,'ScreenSize');
scnsize(3) = scnsize(3)*0.96;
fig1=figure(1);
set(gcf,'Visible', 'off'); 
clf
fig2=figure(2);
set(gcf,'Visible', 'off'); 
clf
fig3=figure(3);
set(gcf,'Visible', 'off'); 
clf
fig4=figure(4);
set(gcf,'Visible', 'off'); 
clf
fig5=figure(5);
set(gcf,'Visible', 'off'); 
clf
fig6=figure(6);
set(gcf,'Visible', 'off');
clf
position = get(fig1,'Position');
outerpos = get(fig1,'OuterPosition');
border = outerpos-position;
posshift = 50;

pos1 = [0,...
        scnsize(4)*(1/4),...
        scnsize(3)*(1/3)-border(4)/6,...
        scnsize(4)/2];
pos2 = [scnsize(3)*(1/3)+posshift,...
        scnsize(4)/2-5*border(3),...
        pos1(3),...
        pos1(4)-border(4)/4];
pos3 = [pos2(1),...
        5*border(3),...
        pos1(3),...
        pos2(4)];
pos4 = [scnsize(3)*(2/3)+posshift,...
        pos2(2),...
        pos1(3),...
        pos2(4)];
pos5 = [pos4(1),...
        pos3(2),...
        pos1(3),...
        pos2(4)];
set(fig1,'OuterPosition',pos1); 
set(fig2,'OuterPosition',pos2);
set(fig3,'OuterPosition',pos3);
set(fig4,'OuterPosition',pos4);
set(fig5,'OuterPosition',pos5);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% setup real-space mesh

fprintf('Setting up real-space mesh ...\n\n');
xx_em   = zeros(nx_em,1);
xx_ma   = zeros(nx_ma,1);
xx_em_b = zeros(nx_em_b,1);

xh = (double(nx_em)-1)/2;
for jj=1:nx_em
  xx_em(jj) = (double(jj)-1-xh)*dx_em;
end

xhb= (double(nx_em)+2*double(border_em)-1)/2;
for jj=1:(nx_em+2*border_em)
  xx_em_b(jj) = (double(jj)-1-xhb)*dx_em;
end

yh = (double(nx_ma)-1)/2;
for jj=1:nx_ma
  xx_ma(jj) = (double(jj)-1-yh)*dx_ma;
end

% setup mu and epsilon arrays on the mesh
for jj=1:border_em;
  mu_array_b(                     jj) = mu_b;
  mu_array_b(     border_em+nx_em+jj) = mu_b; 
  epsilon_array_b(                jj) = epsilon_b;
  epsilon_array_b(border_em+nx_em+jj) = epsilon_b; 
  c_array_b(                      jj) = c_b;
  c_array_b(      border_em+nx_em+jj) = c_b; 
end

for jj=1:nx_em
  mu_array_b(     border_em+jj) = mu_0;
  epsilon_array_b(border_em+jj) = epsilon_0;
end

figure(6);
set(gcf,'Visible','off');
plot(xx_em_b,epsilon_array_b);
filename = './figures/epsilon.png';
print(fig6,'-dpng',filename); 

figure(6);
set(gcf,'Visible','off');
plot(xx_em_b,mu_array_b);
filename = './figures/mu.png';
print(fig6,'-dpng',filename); 


% finite difference operator for EM hamiltonian inside the box
for jj=1:(nx_em)
  if (jj < nx_em),
    first_deriv_fd_b(jj+border_em,jj+border_em+1) =   c_0 * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_b(jj+border_em,jj+border_em-1) = - c_0 * 1/(2*dx_em);
  end
end
first_deriv_fd_mirror = first_deriv_fd_b;

% finite difference operator for EM Hamiltonian outside the box in border region
if(border_em > 0),
  first_deriv_fd_b(border_em        ,border_em      +1) =  c_0 * 1/(2*dx_em);
  first_deriv_fd_b(border_em+nx_em  ,border_em+nx_em+1) =  c_0 * 1/(2*dx_em);
  first_deriv_fd_b(border_em      +1,border_em        ) = -c_b * 1/(2*dx_em);
  first_deriv_fd_b(border_em+nx_em+1,border_em+nx_em  ) = -c_b * 1/(2*dx_em);
end
for jj=1:(border_em)
  if (jj < border_em),
    first_deriv_fd_b(jj,jj+1) =   c_b * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em < 2*border_em+nx_em)
    first_deriv_fd_b(jj+border_em+nx_em,jj+border_em+nx_em+1) =   c_b * 1/(2*dx_em);
  end
  if (jj > 1),
    first_deriv_fd_b(jj,jj-1) = - c_b * 1/(2*dx_em);
  end
  if (jj+border_em+nx_em > border_em+nx_em+1)
    first_deriv_fd_b(jj+border_em+nx_em,jj+border_em+nx_em-1) = - c_b * 1/(2*dx_em);
  end
end

for jj=1:border_em
  first_deriv_fd_mirror(                jj,                jj) = 0;
  first_deriv_fd_mirror(border_em+nx_em+jj,border_em+nx_em+jj) = 0;
end
first_deriv_fd_mirror(border_em      +1,border_em)         = - c_b * 1/(2*dx_em);
first_deriv_fd_mirror(border_em+nx_em  ,border_em+nx_em+1) =   c_b * 1/(2*dx_em);

if (boundary_em == 0)
  E_first_deriv_fd_b = first_deriv_fd_b;
  B_first_deriv_fd_b = first_deriv_fd_b;
elseif (boundary_em == 1)
  E_first_deriv_fd_b = first_deriv_fd_b;
  B_first_deriv_fd_b = first_deriv_fd_mirror;
elseif (boundary_em == 2)
  E_first_deriv_fd_b = first_deriv_fd_mirror;
  B_first_deriv_fd_b = first_deriv_fd_b;
end

%first_deriv_fd_b

% definition of v and h
v_0 = c_0;
v_b = c_b;
h_0 = sqrt(mu_0/epsilon_0);
h_b = sqrt(mu_b/epsilon_b);

for ii=1:border_em
 v_array_b(ii                ) = v_b;
 v_array_b(ii+nx_em+border_em) = v_b;
 h_array_b(ii                ) = h_b;
 h_array_b(ii+nx_em+border_em) = h_b;
end

for ii=1:nx_em
 v_array_b(ii+border_em) = v_0;
 h_array_b(ii+border_em) = h_0;
end
for ii=1:nx_em_b
  v_over_h_b(ii) = v_array_b(ii) / h_array_b(ii);
end

%divergence of v and h
if (medium_smooth == 1),
  div_v_b(1)                 = ( 1/2) * v_array_b(2)                   / dx_em;
  div_v_b(nx_em+2*border_em) = (-1/2) * v_array_b(nx_em+2*border_em-1) / dx_em;
  div_h_b(1)                 = ( 1/2) * h_array_b(2)                   / dx_em;
  div_h_b(nx_em+2*border_em) = (-1/2) * h_array_b(nx_em+2*border_em-1) / dx_em;
  for ii=2:(nx_em_b-1)
    div_v_b(ii) = (-1/2 * v_array_b(ii-1) + 1/2 * v_array_b(ii+1)) / dx_em;
    div_h_b(ii) = (-1/2 * h_array_b(ii-1) + 1/2 * h_array_b(ii+1)) / dx_em;
  end
elseif (medium_smooth == 0),
  for jj=1:nx_em_b
    div_v_b(jj) = 0;
    div_h_b(jj) = 0;
  end
  small = 1.0;
  div_v_b(border_em        ) = (v_0-v_b)/(pi*small);
  div_v_b(border_em+nx_em+1) = (v_b-v_0)/(pi*small);
  div_h_b(border_em        ) = (h_0-h_b)/(pi*small);
  div_h_b(border_em+nx_em+1) = (h_b-h_0)/(pi*small);
end

% finite difference operator for matter Hamiltonian
for jj=1:nx_ma
  if (jj < nx_ma),
    second_deriv_fd(jj,jj+1) = 1/dx_ma;
  end
  second_deriv_fd(jj,jj) = -2/dx_ma;
  if (jj > 1),
    second_deriv_fd(jj,jj-1) = 1/(dx_ma);
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% lookup table for convolution of v and p for the Hamiltonian part A


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Schroedinger Hamiltonian

for ii=1:nx_ma
  Phi_xx_ma(ii,1) = 2 *(pi)^(-1/4)/(sqrt(sigma_ma)) * exp(-(xx_ma(ii)^2)/(4*sigma_ma^2));
end
Hm_ma    = -h^2/(2*m)*second_deriv_fd;
unitm_ma = sparse(eye(nx_ma));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Maxwell equations in Hamiltonian formalism

fprintf('Setting up electromagnetic field ...\n\n');

% start index of the initial signal
for ii=1:nx_em
  if xx_em(ii) > (signal_pos - double(signallength)/2),
    sig_idx_1 = ii;
    break;
  end
end
% end indexo of the initial signal
for ii=1:nx_em
  if xx_em(ii) > (signal_pos + double(signallength)/2),
    sig_idx_2 = ii-1;
    break;
  end
end

% initialization of the electromagnetic field
Ey = zeros(nx_em,1);
Ez = zeros(nx_em,1);
By = zeros(nx_em,1);
Bz = zeros(nx_em,1);
for ii=sig_idx_1:sig_idx_2
  if (em_wave_shape == 1)
    sinus_wave = sin(k_em*(xx_em(ii)-signal_pos));
    Ey(ii,1) = 0;
    Ez(ii,1) =          sinus_wave;
    By(ii,1) = -1/c_0 * sinus_wave;
    Bz(ii,1) = 0;
  end
  if (em_wave_shape == 2) 
    gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_em)) * exp(-((xx_em(ii)-signal_pos)^2)/(4*sigma_em^2));
    Ey(ii,1) = 0; 
    Ez(ii,1) =          gauss_dist;
    By(ii,1) = -1/c_0 * gauss_dist;
    Bz(ii,1) = 0;
  end
  if (em_wave_shape == 3)
    sinus_wave = sin(k_em*xx_em(ii));
    gauss_dist = 2 *(pi)^(-1/4)/(sqrt(sigma_em)) * exp(-((xx_em(ii)-signal_pos)^2)/(4*sigma_em^2));
    Ey(ii,1) = 0;
    Ez(ii,1) =          sinus_wave * gauss_dist;  
    By(ii,1) = -1/c_0 * sinus_wave * gauss_dist;
    Bz(ii,1) = 0;
  end
end

for ii=1:nx_em
  Ey_b(ii+border_em,1) = Ey(ii,1);
  Ez_b(ii+border_em,1) = Ez(ii,1);
  By_b(ii+border_em,1) = By(ii,1);
  Bz_b(ii+border_em,1) = Bz(ii,1);
end

for ii=1:border_em
  Ey_b(                ii) = 0;
  Ey_b(border_em+nx_em+ii) = 0;
  Ez_b(                ii) = 0;
  Ez_b(border_em+nx_em+ii) = 0;
  By_b(                ii) = 0;
  By_b(border_em+nx_em+ii) = 0;
  Bz_b(                ii) = 0;
  Bz_b(border_em+nx_em+ii) = 0;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Riemann-Silberstein vector   
 
for ii=1:nx_em_b
  Fy_b_plus( ii,1) = sqrt(epsilon_array_b(ii)/2) * Ey_b(ii) + i / sqrt(2*mu_array_b(ii)) * By_b(ii);
  Fy_b_minus(ii,1) = sqrt(epsilon_array_b(ii)/2) * Ey_b(ii) - i / sqrt(2*mu_array_b(ii)) * By_b(ii);
  Fz_b_plus( ii,1) = sqrt(epsilon_array_b(ii)/2) * Ez_b(ii) + i / sqrt(2*mu_array_b(ii)) * Bz_b(ii);
  Fz_b_minus(ii,1) = sqrt(epsilon_array_b(ii)/2) * Ez_b(ii) - i / sqrt(2*mu_array_b(ii)) * Bz_b(ii);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% initialization of the spinor vector

for ii=1:nx_em_b
  % plus Spinor
  Phi_xx_em_b(          ii,1) =   i * Fy_b_plus(ii,1); 
  Phi_xx_em_b(  nx_em_b+ii,1) =       Fz_b_plus(ii,1); 
  Phi_xx_em_b(2*nx_em_b+ii,1) =       Fz_b_plus(ii,1); 
  Phi_xx_em_b(3*nx_em_b+ii,1) =   i * Fy_b_plus(ii,1);
  % minus Spinor
  Phi_xx_em_b(4*nx_em_b+ii,1) =  -i * Fy_b_minus(ii,1);
  Phi_xx_em_b(5*nx_em_b+ii,1) =       Fz_b_minus(ii,1);
  Phi_xx_em_b(6*nx_em_b+ii,1) =       Fz_b_minus(ii,1);
  Phi_xx_em_b(7*nx_em_b+ii,1) =  -i * Fy_b_minus(ii,1);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matrix hamiltonian

MM_Ez = zeros(8,8);
MM_By = zeros(8,8);

MM_Ez(1,3) = -i * h;
MM_By(2,4) = -i * h;
MM_By(3,1) = -i * h;
MM_Ez(4,2) = -i * h;

MM_Ez(5,7) = -i * h;
MM_By(6,8) = -i * h;
MM_By(7,5) = -i * h;
MM_Ez(8,6) = -i * h;

VV(2,1) = -i * 1/2 * h;
VV(1,2) = -i * 1/2 * h;
VV(3,4) = -i * 1/2 * h;
VV(4,3) = -i * 1/2 * h;

VV(6,5) = -i * 1/2 * h;
VV(5,6) = -i * 1/2 * h;
VV(8,7) = -i * 1/2 * h;
VV(7,8) = -i * 1/2 * h;

HH(7,1) = -i * 1/2 * h;
HH(8,2) =  i * 1/2 * h;
HH(5,3) =  i * 1/2 * h;
HH(6,4) = -i * 1/2 * h;

HH(3,5) = -i * 1/2 * h;
HH(4,6) =  i * 1/2 * h;
HH(1,7) =  i * 1/2 * h;
HH(2,8) = -i * 1/2 * h;

for ii=1:nx_em_b
  VV_m_b(ii,ii) = div_v_b(ii);
  HH_m_b(ii,ii) = v_over_h_b(ii) * div_h_b(ii);
end
 
MM_Ez_kron = kron(MM_Ez,E_first_deriv_fd_b);
MM_By_kron = kron(MM_By,B_first_deriv_fd_b);

VV_kron = kron(VV,VV_m_b);
HH_kron = kron(HH,HH_m_b);

Hm_em_b = MM_Ez_kron + MM_By_kron + VV_kron + HH_kron;

unitm_em_b = sparse(eye(8*nx_em_b));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% exponential matrix for FFT method

if (prop_mode_em == 3)

  H_em_A       = zeros(6,6);
  H_em_B1      = zeros(6,6);
  H_em_B2      = zeros(6,6);
  H_em_com_A_B = zeros(6,6);

  H_em_A(1,3) = -i * h;
  H_em_A(2,4) = -i * h;
  H_em_A(3,1) = -i * h;
  H_em_A(4,2) = -i * h;
  H_em_A(5,7) = -i * h;
  H_em_A(6,8) = -i * h;
  H_em_A(7,5) = -i * h;
  H_em_A(8,6) = -i * h;


  for jj=0:(nx_em_b/2)
    px_em_b(jj+1) = 2*pi/(boxlength_em+borderlength_em) * double(jj);
  end
  for jj=(nx_em_b/2+1):(nx_em_b-1)
    px_em_b(jj+1) = 2*pi/(boxlength_em+borderlength_em) * double(jj-nx_em_b);
  end

  FFT_v_array_b   = fftn(v_array_b);
  FFT_h_array_b   = fftn(h_array_b);

  for jj=1:nx_em_b
    FFT_p_b(jj)  = i/h * px_em_b(jj);
    FFT_pv_b(jj) = i/h * px_em_b(jj) * FFT_v_array_b(jj);
    FFT_ph_b(jj) = i/h * px_em_b(jj) * FFT_h_array_b(jj);      
  end

  div_v_b = ifftn(FFT_pv_b);
  div_h_b = ifftn(FFT_ph_b);

  for jj=1:nx_em_b
    v_over_h_div_h_b(jj) = v_over_h_b(jj)*div_h_b(jj);
  end

  FFT_div_v_b          = fftn(div_v_b);
  FFT_v_over_h_div_h_b = fftn(v_over_h_div_h_b);

  for jj=1:nx_em_b
    FFT_p_matrix_b(jj,jj)                = FFT_p_b(jj);
    FFT_div_v_matrix_b(jj,jj)            = FFT_div_v_b(jj);
    FFT_v_over_h_div_h_matrix_b(jj,jj)   = FFT_v_over_h_div_h_b(jj);
  end

  MM_FFT_Ez_kron = kron(MM_Ez,FFT_p_matrix_b);
  MM_FFT_By_kron = kron(MM_By,FFT_p_matrix_b);
  VV_FFT_kron    = kron(VV,FFT_div_v_matrix_b);
  HH_FFT_kron    = kron(HH,FFT_v_over_h_div_h_matrix_b);

  Hm_FFT_em_b = MM_FFT_Ez_kron + MM_FFT_By_kron; % + HH_FFT_kron;% + VV_FFT_kron; % + HH_FFT_kron;  
%  Hm_FFT_expm_b = expm( (-i/h) * dt * Hm_FFT_em_b );

  fprintf('Exponential of the Hamiltonian matrix calculated ...\n\n');


  for ii=1:nx_em_b
    f(ii,1) = exp(-(xx_em_b(ii)/30)^2);
  %  f(ii) = cos( (2*pi)/150 * xx_em_b(ii) );
  %  g(ii) = sin( (2*pi)/(150/5) * xx_em_b(ii) );
    g(ii,1) = 1/(1+xx_em_b(ii)^2);
  end
  w = conv(f,g,'same');
 
  for ii=1:nx_em_b
    v(ii,1) = 0;
    for jj=1:ii
      v(ii,1) = v(ii,1) + f(jj,1) * g(ii+1-jj,1);
    end
  end
  for ii=(nx_em_b+1):(2*nx_em_b-1)
    v(ii,1) = 0;
    for jj=(ii+1-nx_em_b):(nx_em_b)
      v(ii,1) = v(ii,1) + f(jj,1) * g(ii+1-jj,1);
    end
  end
%  v
  for ii=1:nx_em_b
    u(ii,1) = v(ii+nx_em_b/2,1);
  end

  zeros(2*nx_em_b-1,nx_em_b);
  for ii=1:nx_em_b
    for jj=1:nx_em_b
      convmatrix(jj-1+ii,ii) = g(jj,1);
    end 
  end
  vv = convmatrix*f;
  for ii=1:nx_em_b
    uu(ii,1) = vv(ii+nx_em_b/2,1);
  end

  for ii=1:nx_em_b
    convmatrixshort(ii,:) = convmatrix(ii+nx_em_b/2,:);
  end
  uuu = convmatrixshort*f;

  length(f)
  length(g)
  length(w)
  length(convmatrix)


  figure(8);
  set(gcf,'Visible','on');
  plot(xx_em_b,f,'b',xx_em_b,g,'r',xx_em_b,w,'g',xx_em_b,uu,'y');
  d=w-uuu;
  plot(xx_em_b,d,'b');
%  plot(xx_em_b,u,'y');

%  plot(xx_em_b,f);
%  hold on;
%  plot(xx_em_b,g);
%  hold on;
%  plot(xx_em_b,w);
%  hold off;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% exponential matrix alternative calculation for FFT method

if (prop_mode_em == 4)

  for ii=1:nx_em_b
    Phi_xx_alt_em_b{ii}(1) =  i * Fy_b_plus(ii,1);
    Phi_xx_alt_em_b{ii}(2) =      Fz_b_plus(ii,1);
    Phi_xx_alt_em_b{ii}(3) =      Fz_b_plus(ii,1);
    Phi_xx_alt_em_b{ii}(4) =  i * Fy_b_plus(ii,1);
    Phi_xx_alt_em_b{ii}(5) = -i * Fy_b_minus(ii,1);
    Phi_xx_alt_em_b{ii}(6) =      Fz_b_minus(ii,1);
    Phi_xx_alt_em_b{ii}(7) =      Fz_b_minus(ii,1);
    Phi_xx_alt_em_b{ii}(8) = -i * Fy_b_minus(ii,1);
  end

  for jj=0:(nx_em_b/2)
    px_em_b(jj+1) = 2*pi/(boxlength_em+borderlength_em) * double(jj);
  end
  for jj=(nx_em_b/2+1):(nx_em_b-1)
    px_em_b(jj+1) = 2*pi/(boxlength_em+borderlength_em) * double(jj-nx_em_b);
  end

  FFT_v_array_b   = fftn(v_array_b);
  FFT_h_array_b   = fftn(h_array_b);

  for jj=1:nx_em_b
    FFT_p_b(jj)  = i/h * px_em_b(jj);
    FFT_pv_b(jj) = i/h * px_em_b(jj) * FFT_v_array_b(jj);
    FFT_ph_b(jj) = i/h * px_em_b(jj) * FFT_h_array_b(jj);      
  end

  div_v_b = ifftn(FFT_pv_b);
  div_h_b = ifftn(FFT_ph_b);

  for jj=1:nx_em_b
    v_over_h_div_h_b(jj) = v_over_h_b(jj)*div_h_b(jj);
  end

  FFT_div_v_b          = fftn(div_v_b);
  FFT_v_over_h_div_h_b = fftn(v_over_h_div_h_b);

  for ii=1:nx_em_b

    HH_em_alt_b{ii} = zeros(8,8);

    HH_FFT_em_alt_b{ii}(1,3) = -i * h * FFT_p_b(ii);
    HH_FFT_em_alt_b{ii}(2,4) = -i * h * FFT_p_b(ii);
    HH_FFT_em_alt_b{ii}(3,1) = -i * h * FFT_p_b(ii);
    HH_FFT_em_alt_b{ii}(4,2) = -i * h * FFT_p_b(ii);

    HH_FFT_em_alt_b{ii}(5,7) = -i * h * FFT_p_b(ii);
    HH_FFT_em_alt_b{ii}(6,8) = -i * h * FFT_p_b(ii);
    HH_FFT_em_alt_b{ii}(7,5) = -i * h * FFT_p_b(ii);
    HH_FFT_em_alt_b{ii}(8,6) = -i * h * FFT_p_b(ii);

    HH_FFT_em_alt_b{ii}(2,1) = -i * 1/2 * h * FFT_div_v_b(ii);
    HH_FFT_em_alt_b{ii}(1,2) = -i * 1/2 * h * FFT_div_v_b(ii);
    HH_FFT_em_alt_b{ii}(3,4) = -i * 1/2 * h * FFT_div_v_b(ii);
    HH_FFT_em_alt_b{ii}(4,3) = -i * 1/2 * h * FFT_div_v_b(ii);

    HH_FFT_em_alt_b{ii}(6,5) = -i * 1/2 * h * FFT_div_v_b(ii);
    HH_FFT_em_alt_b{ii}(5,6) = -i * 1/2 * h * FFT_div_v_b(ii);
    HH_FFT_em_alt_b{ii}(8,7) = -i * 1/2 * h * FFT_div_v_b(ii);
    HH_FFT_em_alt_b{ii}(7,8) = -i * 1/2 * h * FFT_div_v_b(ii);

    HH_FFT_em_alt_b{ii}(7,1) = -i * 1/2 * h * FFT_v_over_h_div_h_b(ii);
    HH_FFT_em_alt_b{ii}(8,2) =  i * 1/2 * h * FFT_v_over_h_div_h_b(ii);
    HH_FFT_em_alt_b{ii}(5,3) =  i * 1/2 * h * FFT_v_over_h_div_h_b(ii);
    HH_FFT_em_alt_b{ii}(6,4) = -i * 1/2 * h * FFT_v_over_h_div_h_b(ii);

    HH_FFT_em_alt_b{ii}(3,5) = -i * 1/2 * h * FFT_v_over_h_div_h_b(ii);
    HH_FFT_em_alt_b{ii}(4,6) =  i * 1/2 * h * FFT_v_over_h_div_h_b(ii);
    HH_FFT_em_alt_b{ii}(1,7) =  i * 1/2 * h * FFT_v_over_h_div_h_b(ii);
    HH_FFT_em_alt_b{ii}(2,8) = -i * 1/2 * h * FFT_v_over_h_div_h_b(ii);

    Hm_FFT_expm_alt_b{ii} = expm( (-i/h) * dt * HH_FFT_em_alt_b{ii} );

  end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure(1);
set(gcf,'Visible', 'off'); 
plot(xx_ma,abs(Phi_xx_ma));
filename = './figures/wavefunction/wave_packet_t=0.png';
print(fig1,'-dpng',filename);
figure(2);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Ez_b);
filename = './figures/E_field_z_b/E_field_z_b_t=0.png';
print(fig2,'-dpng',filename);
figure(3);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Bz_b);
filename = './figures/B_field_z_b/B_field_z_b_t=0.png';
print(fig3,'-dpng',filename);
figure(4);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,Ey_b);
time_str = int2str(0);
filename = './figures/E_field_y_b/E_field_y_b_t=0.png';
print(fig4,'-dpng',filename);
figure(5);
set(gcf,'Visible', 'off'); 
plot(xx_em_b,By_b);
filename = './figures/B_field_y_b/B_field_y_b_t=0.png';
print(fig5,'-dpng',filename);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1D Crank Nicolson for Schroedinger and maxwell equation 

fprintf('Schroedinger and electromagnetic field propagation ...\n\n');

for ii=1:timesteps

  if (mod(ii,10) == 0)
    steps_string = int2str(ii);
    combinedStr = strcat('Timesteps done:    ',steps_string,' ...\n\n');
    fprintf(combinedStr)
  end
 
  if (calc_ma == 1)

    % Schroedinger propagation
    Phi_xx_ma_old = Phi_xx_ma;
    b         = (unitm_ma - i * Hm_ma*dt) * Phi_xx_ma;
    Phi_xx_ma = (unitm_ma + i * Hm_ma*dt) \ b;

    time_str = int2str(ii);
    filename = './figures/wavefunction/wave_packet_t='
    combinedStr = strcat(filename,time_str,'.png');
    print(fig1,'-dpng',combinedStr);
  
  end

  % Maxwell propagation
  if (calc_em == 1)

    % Maxwell propagation
    Phi_xx_em_b_old = Phi_xx_em_b;
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update equation 
    if (prop_mode_em == 0) 
      for jj=2:(nx_em_b-1)
        Phi_xx_em_b(          jj,1) = Phi_xx_em_b(          jj,1) - dt*c/dx_em * (Phi_xx_em_b(2*nx_em_b+jj+1,1) - Phi_xx_em_b(2*nx_em_b+jj-1,1));
        Phi_xx_em_b(  nx_em_b+jj,1) = Phi_xx_em_b(  nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(3*nx_em_b+jj+1,1) - Phi_xx_em_b(3*nx_em_b+jj-1,1));
        Phi_xx_em_b(2*nx_em_b+jj,1) = Phi_xx_em_b(2*nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(          jj+1,1) - Phi_xx_em_b(          jj-1,1));
        Phi_xx_em_b(3*nx_em_b+jj,1) = Phi_xx_em_b(3*nx_em_b+jj,1) - dt*c/dx_em * (Phi_xx_em_b(  nx_em_b+jj+1,1) - Phi_xx_em_b(  nx_em_b+jj-1,1));
      end
    end
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % crank nicolson  
    if (prop_mode_em == 1)

      for jj=1:nx_em_b
        Fx_b(jj)     = -1/2*Phi_xx_em_b(jj)             + 1/2*Phi_xx_em_b(3*nx_em_b+jj);
        Fy_b(jj)     = -i/2*Phi_xx_em_b(jj)             - i/2*Phi_xx_em_b(3*nx_em_b+jj);
        Fz_b(jj)     =  1/2*Phi_xx_em_b(nx_em_b+jj)     + 1/2*Phi_xx_em_b(2*nx_em_b+jj);
      end

      b           = (unitm_em_b - i*(Hm_em_b)*dt) * Phi_xx_em_b;
      Phi_xx_em_b = (unitm_em_b + i*(Hm_em_b)*dt) \ b;

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % fourth order taylor expansion
    if(prop_mode_em == 2)

      taylor_order = 4;
      zfact = 1;
      h_exp_psi = Phi_xx_em_b;
      % Taylor series for matrix exponential acting on a vector
      for jj = 1:taylor_order
        zfact = zfact*(-sqrt(-1)*dt)/jj;
        hzpsi = Hm_em_b*Phi_xx_em_b; % Hm_em_E*Phi_E + Hm_em_B*Phi_B;
        h_exp_psi = h_exp_psi + zfact*hzpsi;
        if(jj ~= taylor_order) 
          zpsi = hzpsi; 
        end
      end
      Phi_xx_em_b = h_exp_psi; 

      for jj=1:nx_em_b
        Phi_E_b(          jj,1) =  i/2 * imag(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_E_b(  nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_E_b(2*nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_E_b(3*nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_B_b(          jj,1) =  1/2 * real(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
        Phi_B_b(  nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));
        Phi_B_b(2*nx_em_b+jj,1) =  i/2 * imag(Phi_xx_em_b(nx_em_b+jj,1) + Phi_xx_em_b(2*nx_em_b+jj,1));       
        Phi_B_b(3*nx_em_b+jj,1) =  1/2 * real(Phi_xx_em_b(        jj,1) + Phi_xx_em_b(3*nx_em_b+jj,1));
      end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Fast Fourier transform propagation
    if (prop_mode_em == 3)

      FFT_Phi_xx_em_b = fftn(Phi_xx_em_b);
      FFT_Phi_xx_em_b = Hm_FFT_expm_b*FFT_Phi_xx_em_b;
      Phi_xx_em_b     = ifftn(FFT_Phi_xx_em_b);
    
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Fast Fourier transform propagation alternative calculation
    if (prop_mode_em == 4)

      for jj=1:nx_em_b
        FFT_Phi_xx_alt_em_b{ii} 
      end    
 
    end
    

    for jj=1:nx_em_b
      Fx_plus_b(jj)     = -1/2*Phi_xx_em_b(jj)             + 1/2*Phi_xx_em_b(3*nx_em_b+jj);
      Fy_plus_b(jj)     = -i/2*Phi_xx_em_b(jj)             - i/2*Phi_xx_em_b(3*nx_em_b+jj);
      Fz_plus_b(jj)     =  1/2*Phi_xx_em_b(nx_em_b+jj)     + 1/2*Phi_xx_em_b(2*nx_em_b+jj);
    end

    for jj=1:nx_em
      Fx_plus(jj) = Fx_plus_b(jj+border_em);
      Fy_plus(jj) = Fy_plus_b(jj+border_em);
      Fz_plus(jj) = Fz_plus_b(jj+border_em);
    end

    for jj=1:nx_em_b
      Ex_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fx_plus_b(jj));
      Ey_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fy_plus_b(jj));
      Ez_b(jj) = real(sqrt(2/epsilon_array_b(jj))*Fz_plus_b(jj));
      Bx_b(jj) = imag(2*mu_array_b(jj)*Fx_plus_b(jj));
      By_b(jj) = imag(2*mu_array_b(jj)*Fy_plus_b(jj));
      Bz_b(jj) = imag(2*mu_array_b(jj)*Fz_plus_b(jj));
    end
   
    for jj=1:nx_em
      Ex(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fx_plus_b(jj+border_em));
      Ey(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fy_plus_b(jj+border_em));
      Ez(jj) = real(sqrt(2/epsilon_array_b(jj+border_em))*Fz_plus_b(jj+border_em));
      Bx(jj) = imag(2*mu_array_b(jj+border_em)*Fx_plus_b(jj+border_em));
      By(jj) = imag(2*mu_array_b(jj+border_em)*Fy_plus_b(jj+border_em));
      Bz(jj) = imag(2*mu_array_b(jj+border_em)*Fz_plus_b(jj+border_em));
    end

    time_array(ii)     = double(ii)*dt;

    energy_b           = dot(Fx_plus_b,Fx_plus_b)+dot(Fy_plus_b,Fy_plus_b)+dot(Fz_plus_b,Fz_plus_b) * dx_em; 
    energy_b_array(ii) = energy_b;


    figure(6);
    set(gcf,'Visible','off');
    plot(time_array,energy_b_array);
    filename = './figures/energy.png';
    print(fig6,'-dpng',filename); 
    

    figure(2);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Ez);
    time_str = int2str(ii);
    filename = './figures/E_field_z/E_field_z_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig2,'-dpng',combinedStr);

    figure(3);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Bz);
    time_str = int2str(ii);
    filename = './figures/B_field_z/B_field_z_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig3,'-dpng',combinedStr);

    figure(4);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,Ey);
    time_str = int2str(ii);
    filename = './figures/E_field_y/field_y_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig4,'-dpng',combinedStr);

    figure(5);
    set(gcf,'Visible', 'off'); 
    plot(xx_em,By);
    time_str = int2str(ii);
    filename = './figures/B_field_y/B_field_y_t=';
    combinedStr =  strcat(filename,time_str,'.png');
    print(fig5,'-dpng',combinedStr);

    
    figure(2);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Ez_b);
    time_str = int2str(ii);
    filename = './figures/E_field_z_b/E_field_z_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig2,'-dpng',combinedStr);

    figure(3);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Bz_b);
    time_str = int2str(ii);
    filename = './figures/B_field_z_b/B_field_z_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig3,'-dpng',combinedStr);

    figure(4);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,Ey_b);
    time_str = int2str(ii);
    filename = './figures/E_field_y_b/E_field_y_b_t=';
    combinedStr = strcat(filename,time_str,'.png');
    print(fig4,'-dpng',combinedStr);

    figure(5);
    set(gcf,'Visible', 'off'); 
    plot(xx_em_b,By_b);
    time_str = int2str(ii);
    filename = './figures/B_field_y_b/B_field_y_b_t=';
    combinedStr =  strcat(filename,time_str,'.png');
    print(fig5,'-dpng',combinedStr);
  
  end

end





