clear;
more off;

Nx   =  8;
Ny   =  8;
xmin = -2;
xmax =  2;
ymin = -2;
ymax =  2;
dx   =  (xmax-xmin)/double(Nx);
dy   =  (ymax-ymin)/double(Ny);
a_x  =  1;
b_x  =  2;
a_y  =  3;
b_y  =  4;


xpoints = (xmin+dx/2):dx:(xmax-dx/2);
ypoints = (ymin+dy/2):dy:(ymax-dy/2);

for ii=1:Nx
  for jj=1:Ny
    f1(ii,jj)   = exp(-(a_x*xpoints(ii))^2) * exp(-(a_y*ypoints(jj))^2);
    g1(ii,jj)   = exp(-(b_x*xpoints(ii))^2) * exp(-(b_y*ypoints(jj))^2);
    f1g1(ii,jj) = exp(-(a_x*xpoints(ii))^2) * exp(-(b_x*xpoints(ii))^2) * exp(-(a_y*ypoints(jj))^2) * exp(-(b_y*ypoints(jj))^2) ;
  end
end

% delta test
for ii=1:Nx
  for jj=1:Nx
    delta(ii,jj) = 0;
    for kk=1:Nx
      delta(ii,jj) = delta(ii,jj) + 1/Nx * exp(i*2*pi*((ii-1)-(jj-1))*kk/Nx);
    end
  end
end


% fourier transform 
for ii=1:Nx 
  for jj=1:Ny
    FT_f1(ii,jj) = 0;
    for iik=1:Nx
      for jjk=1:Ny
        FT_f1(ii,jj) = FT_f1(ii,jj) + 1/sqrt(Nx) * 1/sqrt(Ny) * f1(iik,jjk) * exp(-i*2*pi*(ii-1)*(iik-1)/Nx) * exp(-i*2*pi*(jj-1)*(jjk-1)/Ny);
      end
    end
  end
end

FT_f1 = 1/sqrt(Nx) * 1/sqrt(Ny) * fftn(f1);

for ii=1:Nx 
  for jj=1:Ny
    FT_g1(ii,jj) = 0;
    for iik=1:Nx
      for jjk=1:Ny
        FT_g1(ii,jj) = FT_g1(ii,jj) + 1/sqrt(Nx) * 1/sqrt(Ny) * g1(iik,jjk) * exp(-i*2*pi*(ii-1)*(iik-1)/Nx) * exp(-i*2*pi*(jj-1)*(jjk-1)/Ny);
      end
    end
  end
end

FT_g1 = 1/sqrt(Nx) * 1/sqrt(Ny) * fftn(g1)

% fourier transform reference
for ii=1:Nx
  for jj=1:Nx
    FT_f1_g1(ii,jj) = 0;
    for iik=1:Nx
      for jjk=1:Ny
        FT_f1_g1(ii,jj) = FT_f1_g1(ii,jj) + f1(iik,jjk) * g1(iik,jjk) * exp(-i*2*pi*(ii-1)*(iik-1)/Nx) * exp(-i*2*pi*(jj-1)*(jjk-1)/Nx);
      end
    end
  end
end

% fourier transform reference equivalence 1
for ii=1:Nx
  for jj=1:Ny
    FT_f1_g1_1(ii,jj) = 0;
    for iik=1:Nx
      for jjk=1:Ny
        for iikk=1:Nx
          for jjkk=1:Ny
            FT_f1_g1_1(ii,jj) = FT_f1_g1_1(ii,jj) + f1(iik,jjk) * g1(iikk,jjkk)                             ...
                              * exp(-i*2*pi*(ii-1)*(iikk-1)/Nx) * delta(iik,iikk)                           ...
                              * exp(-i*2*pi*(jj-1)*(jjkk-1)/Ny) * delta(jjk,jjkk);
          end
        end
      end
    end
  end
end

% fourier transform reference equivalence 2
for ii=1:Nx
  for jj=1:Ny
    FT_f1_g1_2(ii,jj) = 0;

    for iikk=1:Nx
      for jjkk=1:Ny

        for iinn=1:Nx
          for jjnn=1:Ny

            for iimm=1:Nx
              for jjmm=1:Ny

                FT_f1_g1_2(ii,jj) = FT_f1_g1_2(ii,jj)                                       ...
                                  + 1/sqrt(Nx) * 1/sqrt(Ny) * f1(iinn,jjnn)                 ...
                                  * exp(-i*2*pi*(iikk-1)*(iinn-1)/Nx)                       ...
                                  * exp(-i*2*pi*(jjkk-1)*(jjnn-1)/Ny)                       ...
                                  * 1/sqrt(Nx) * 1/sqrt(Ny) * g1(iimm,jjmm)                 ...
                                  * exp(-i*2*pi*(iimm-1)*((ii-1)-(iikk-1))/Nx)              ...
                                  * exp(-i*2*pi*(jjmm-1)*((jj-1)-(jjkk-1))/Ny)              ;
              end
            end
          end
        end
      end
    end
  end
end

% fourier transform reference equivalence 3
for ii=1:Nx
  for jj=1:Ny
    FT_f1_g1_3(ii,jj) = 0;

    for iikk=1:Nx
      for jjkk=1:Ny

        for iimm=1:Nx
          for jjmm=1:Ny
         
            FT_f1_g1_3(ii,jj) = FT_f1_g1_3(ii,jj) + FT_f1(iikk,jjkk)                            ...
                              * 1/sqrt(Nx) * 1/sqrt(Ny) * g1(iimm,jjmm)                         ...
                              * exp(-i*2*pi*(iimm-1)*((ii-1)-(iikk-1))/Nx)                      ...
                              * exp(-i*2*pi*(jjmm-1)*((jj-1)-(jjkk-1))/Ny)                      ;
          end
        end
      end
    end
  end
end

% fourier transform reference equivalence 4
for ii=1:Nx
  for jj=1:Ny
    conv_array{ii}{jj} = zeros(Nx,Ny);
    for iikk=1:Nx
      for jjkk=1:Ny
        for iimm=1:Nx
          for jjmm=1:Ny
            conv_array{ii}{jj}(iikk,jjkk) = conv_array{ii}{jj}(iikk,jjkk)                      ...
                                          + 1/sqrt(Nx) * 1/sqrt(Ny)                            ...
                                          * g1(iimm,jjmm)                                      ...
                                          * exp(-i*2*pi*(iimm-1)*((ii-1)-(iikk-1))/Nx)         ...
                                          * exp(-i*2*pi*(jjmm-1)*((jj-1)-(jjkk-1))/Ny);
          end
        end
      end
    end
  end
end
for ii=1:Nx
  for jj=1:Ny
    FT_f1_g1_4(ii,jj) = 0;
    for iikk=1:Nx
      for jjkk=1:Ny
        FT_f1_g1_4(ii,jj) = FT_f1_g1_4(ii,jj) + conv_array{ii}{jj}(iikk,jjkk) * FT_f1(iikk,jjkk);
      end
    end
  end
end


FT_f1_g1

FT_f1_g1_1;

FT_f1_g1_2;

FT_f1_g1_3;

FT_f1_g1_4


ref_f1_g1 = ifftn(FT_f1_g1_4);


f1g1

ref_f1_g1

figure(1);
mesh(xpoints,ypoints,f1g1)

figure(2);
mesh(xpoints,ypoints,real(ref_f1_g1))









