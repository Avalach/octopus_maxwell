--- src/td/propagator.F90
+++ src/td/propagator.F90
@@ -502,222 +502,252 @@
     call profiling_in(prof, "TD_PROPAGATOR")
     PUSH_SUB(propagator_dt)
 
-    cmplxscl = hm%cmplxscl%space
-
-    if(gauge_field_is_applied(hm%ep%gfield)) then
-      ASSERT(present(gauge_force))
-    end if
+    if (hm%maxwellcheck .eqv. .true.) then
 
-    self_consistent = .false.
-    if(hm%theory_level /= INDEPENDENT_PARTICLES .and. tr%method /= PROP_CAETRS) then
       if(time <= tr%scf_propagation_steps*abs(dt) + M_EPSILON) then
         self_consistent = .true.
         SAFE_ALLOCATE(zpsi1(1:gr%mesh%np, 1:st%d%dim, st%st_start:st%st_end, st%d%kpt%start:st%d%kpt%end))
-
         do ik = st%d%kpt%start, st%d%kpt%end
           do ist = st%st_start, st%st_end
             call states_get_state(st, gr%mesh, ist, ik, zpsi1(:, :, ist, ik))
           end do
         end do
-
       end if
-    end if
 
-    SAFE_ALLOCATE(vaux(1:gr%mesh%np, 1:st%d%nspin))
-    vaux(1:gr%mesh%np, 1:st%d%nspin) = hm%vhxc(1:gr%mesh%np, 1:st%d%nspin)
-    if (cmplxscl) then
-      SAFE_ALLOCATE(Imvaux(1:gr%mesh%np, 1:st%d%nspin))
-      Imvaux(1:gr%mesh%np, 1:st%d%nspin) = hm%Imvhxc(1:gr%mesh%np, 1:st%d%nspin)
-    end if 
-
-    if(.not. propagator_requires_vks(tr)) then
-      SAFE_ALLOCATE(vold(1:gr%mesh%np, 1:st%d%nspin))
-      call lalg_copy(gr%mesh%np, st%d%nspin, tr%v_old(:, :, 1), vold)
-      if(cmplxscl) then
-        SAFE_ALLOCATE(Imvold(1:gr%mesh%np, 1:st%d%nspin))
-        call lalg_copy(gr%mesh%np, st%d%nspin, tr%Imv_old(:, :, 1), Imvold)
-      end if
+      select case(tr%method)
+      case(PROP_ETRS);                     call td_etrs()
+      case(PROP_AETRS, PROP_CAETRS);       call td_aetrs()
+      case(PROP_EXPONENTIAL_MIDPOINT);     call exponential_midpoint()
+      case(PROP_CRANK_NICOLSON);           call td_crank_nicolson(.false.)
+      case(PROP_CRANK_NICOLSON_SPARSKIT);  call td_crank_nicolson(.true.)
+      case(PROP_MAGNUS);                   call td_magnus()
+      case(PROP_CRANK_NICOLSON_SRC_MEM);   call td_crank_nicolson_src_mem()
+      case(PROP_QOCT_TDDFT_PROPAGATOR)
+        call td_qoct_tddft_propagator(hm, gr, st, tr, time, dt)
+      end select
+
+      hm%current_time = time
+
     else
-      call lalg_copy(gr%mesh%np, st%d%nspin, tr%v_old(:, :, 2), tr%v_old(:, :, 3))
-      call lalg_copy(gr%mesh%np, st%d%nspin, tr%v_old(:, :, 1), tr%v_old(:, :, 2))
-      call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc(:, :),     tr%v_old(:, :, 1))
-      call interpolate( (/time - dt, time - M_TWO*dt, time - M_THREE*dt/), tr%v_old(:, :, 1:3), time, tr%v_old(:, :, 0))
-      if(cmplxscl) then
-        call lalg_copy(gr%mesh%np, st%d%nspin, tr%Imv_old(:, :, 2), tr%Imv_old(:, :, 3))
-        call lalg_copy(gr%mesh%np, st%d%nspin, tr%Imv_old(:, :, 1), tr%Imv_old(:, :, 2))
-        call lalg_copy(gr%mesh%np, st%d%nspin, hm%Imvhxc(:, :),     tr%Imv_old(:, :, 1))
-        call interpolate( (/time - dt, time - M_TWO*dt, time - M_THREE*dt/), tr%Imv_old(:, :, 1:3), time, tr%Imv_old(:, :, 0))
+
+      cmplxscl = hm%cmplxscl%space
+
+      if(gauge_field_is_applied(hm%ep%gfield)) then
+        ASSERT(present(gauge_force))
       end if
-    end if
 
-    select case(tr%method)
-    case(PROP_ETRS);                     call td_etrs()
-    case(PROP_AETRS, PROP_CAETRS);       call td_aetrs()
-    case(PROP_EXPONENTIAL_MIDPOINT);     call exponential_midpoint()
-    case(PROP_CRANK_NICOLSON);           call td_crank_nicolson(.false.)
-    case(PROP_CRANK_NICOLSON_SPARSKIT);  call td_crank_nicolson(.true.)
-    case(PROP_MAGNUS);                   call td_magnus()
-    case(PROP_CRANK_NICOLSON_SRC_MEM);   call td_crank_nicolson_src_mem()
-    case(PROP_QOCT_TDDFT_PROPAGATOR)
-      call td_qoct_tddft_propagator(hm, gr, st, tr, time, dt)
-    end select
+      self_consistent = .false.
+      if(hm%theory_level /= INDEPENDENT_PARTICLES .and. tr%method /= PROP_CAETRS) then
+        if(time <= tr%scf_propagation_steps*abs(dt) + M_EPSILON) then
+          self_consistent = .true.
+          SAFE_ALLOCATE(zpsi1(1:gr%mesh%np, 1:st%d%dim, st%st_start:st%st_end, st%d%kpt%start:st%d%kpt%end))
+
+          do ik = st%d%kpt%start, st%d%kpt%end
+            do ist = st%st_start, st%st_end
+              call states_get_state(st, gr%mesh, ist, ik, zpsi1(:, :, ist, ik))
+            end do
+          end do
 
-    if(present(scsteps)) scsteps = 1
+        end if
+      end if
 
-    if(self_consistent) then
+      SAFE_ALLOCATE(vaux(1:gr%mesh%np, 1:st%d%nspin))
+      vaux(1:gr%mesh%np, 1:st%d%nspin) = hm%vhxc(1:gr%mesh%np, 1:st%d%nspin)
+      if (cmplxscl) then
+        SAFE_ALLOCATE(Imvaux(1:gr%mesh%np, 1:st%d%nspin))
+        Imvaux(1:gr%mesh%np, 1:st%d%nspin) = hm%Imvhxc(1:gr%mesh%np, 1:st%d%nspin)
+      end if 
 
-      ! First, compare the new potential to the extrapolated one.
-      if(.not. cmplxscl) then
-        call density_calc(st, gr, st%rho)
+      if(.not. propagator_requires_vks(tr)) then
+        SAFE_ALLOCATE(vold(1:gr%mesh%np, 1:st%d%nspin))
+        call lalg_copy(gr%mesh%np, st%d%nspin, tr%v_old(:, :, 1), vold)
+        if(cmplxscl) then
+          SAFE_ALLOCATE(Imvold(1:gr%mesh%np, 1:st%d%nspin))
+          call lalg_copy(gr%mesh%np, st%d%nspin, tr%Imv_old(:, :, 1), Imvold)
+        end if
       else
-        call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
+        call lalg_copy(gr%mesh%np, st%d%nspin, tr%v_old(:, :, 2), tr%v_old(:, :, 3))
+        call lalg_copy(gr%mesh%np, st%d%nspin, tr%v_old(:, :, 1), tr%v_old(:, :, 2))
+        call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc(:, :),     tr%v_old(:, :, 1))
+        call interpolate( (/time - dt, time - M_TWO*dt, time - M_THREE*dt/), tr%v_old(:, :, 1:3), time, tr%v_old(:, :, 0))
+        if(cmplxscl) then
+          call lalg_copy(gr%mesh%np, st%d%nspin, tr%Imv_old(:, :, 2), tr%Imv_old(:, :, 3))
+          call lalg_copy(gr%mesh%np, st%d%nspin, tr%Imv_old(:, :, 1), tr%Imv_old(:, :, 2))
+          call lalg_copy(gr%mesh%np, st%d%nspin, hm%Imvhxc(:, :),     tr%Imv_old(:, :, 1))
+          call interpolate( (/time - dt, time - M_TWO*dt, time - M_THREE*dt/), tr%Imv_old(:, :, 1:3), time, tr%Imv_old(:, :, 0))
+        end if
       end if
-      call v_ks_calc(ks, hm, st, geo, time = time - dt)
-      SAFE_ALLOCATE(dtmp(1:gr%mesh%np))
-      d_max = M_ZERO
-      do is = 1, st%d%nspin
-        dtmp(1:gr%mesh%np) = hm%vhxc(1:gr%mesh%np, is) - tr%v_old(1:gr%mesh%np, is, 0)
-        d = dmf_nrm2(gr%mesh, dtmp)
-        if(d > d_max) d_max = d
-      end do
-      SAFE_DEALLOCATE_A(dtmp)
 
-      if(d_max > scf_threshold) then
+      select case(tr%method)
+      case(PROP_ETRS);                     call td_etrs()                          
+      case(PROP_AETRS, PROP_CAETRS);       call td_aetrs()
+      case(PROP_EXPONENTIAL_MIDPOINT);     call exponential_midpoint()
+      case(PROP_CRANK_NICOLSON);           call td_crank_nicolson(.false.)
+      case(PROP_CRANK_NICOLSON_SPARSKIT);  call td_crank_nicolson(.true.)
+      case(PROP_MAGNUS);                   call td_magnus()
+      case(PROP_CRANK_NICOLSON_SRC_MEM);   call td_crank_nicolson_src_mem()
+      case(PROP_QOCT_TDDFT_PROPAGATOR)
+        call td_qoct_tddft_propagator(hm, gr, st, tr, time, dt)
+      end select
 
-        ! We do a maximum of 10 iterations. If it still not converged, probably the propagation
-        ! will not be good anyways.
-        do iter = 1, 10
-          if(present(scsteps)) INCR(scsteps, 1)
+      if(present(scsteps)) scsteps = 1
 
-          do ik = st%d%kpt%start, st%d%kpt%end
-            do ist = st%st_start, st%st_end
-              call states_set_state(st, gr%mesh, ist, ik, zpsi1(:, :, ist, ik))
+      if(self_consistent) then
+
+        ! First, compare the new potential to the extrapolated one.
+        if(.not. cmplxscl) then
+          call density_calc(st, gr, st%rho)
+        else
+          call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
+        end if
+        call v_ks_calc(ks, hm, st, geo, time = time - dt)
+        SAFE_ALLOCATE(dtmp(1:gr%mesh%np))
+        d_max = M_ZERO
+        do is = 1, st%d%nspin
+          dtmp(1:gr%mesh%np) = hm%vhxc(1:gr%mesh%np, is) - tr%v_old(1:gr%mesh%np, is, 0)
+          d = dmf_nrm2(gr%mesh, dtmp)
+          if(d > d_max) d_max = d
+        end do
+        SAFE_DEALLOCATE_A(dtmp)
+
+        if(d_max > scf_threshold) then
+
+          ! We do a maximum of 10 iterations. If it still not converged, probably the propagation
+          ! will not be good anyways.
+          do iter = 1, 10
+            if(present(scsteps)) INCR(scsteps, 1)
+  
+            do ik = st%d%kpt%start, st%d%kpt%end
+              do ist = st%st_start, st%st_end
+                call states_set_state(st, gr%mesh, ist, ik, zpsi1(:, :, ist, ik))
+              end do
             end do
-          end do
-          
-          tr%v_old(:, :, 0) = hm%vhxc(:, :)
-          vaux(:, :) = hm%vhxc(:, :)
-          if(cmplxscl) then
+
             tr%v_old(:, :, 0) = hm%vhxc(:, :)
             vaux(:, :) = hm%vhxc(:, :)
-          end if
-          select case(tr%method)
-          case(PROP_ETRS);                     call td_etrs()
-          case(PROP_AETRS, PROP_CAETRS);       call td_aetrs()
-          case(PROP_EXPONENTIAL_MIDPOINT);     call exponential_midpoint()
-          case(PROP_CRANK_NICOLSON);           call td_crank_nicolson(.false.)
-          case(PROP_CRANK_NICOLSON_SPARSKIT);  call td_crank_nicolson(.true.)
-          case(PROP_MAGNUS);                   call td_magnus()
-          case(PROP_CRANK_NICOLSON_SRC_MEM);   call td_crank_nicolson_src_mem()
-          case(PROP_QOCT_TDDFT_PROPAGATOR)
-            call td_qoct_tddft_propagator(hm, gr, st, tr, time, dt)
-          end select
+            if(cmplxscl) then
+              tr%v_old(:, :, 0) = hm%vhxc(:, :)
+              vaux(:, :) = hm%vhxc(:, :)
+            end if
+            select case(tr%method)
+            case(PROP_ETRS);                     call td_etrs()
+            case(PROP_AETRS, PROP_CAETRS);       call td_aetrs()
+            case(PROP_EXPONENTIAL_MIDPOINT);     call exponential_midpoint()
+            case(PROP_CRANK_NICOLSON);           call td_crank_nicolson(.false.)
+            case(PROP_CRANK_NICOLSON_SPARSKIT);  call td_crank_nicolson(.true.)
+            case(PROP_MAGNUS);                   call td_magnus()
+            case(PROP_CRANK_NICOLSON_SRC_MEM);   call td_crank_nicolson_src_mem()
+            case(PROP_QOCT_TDDFT_PROPAGATOR)
+              call td_qoct_tddft_propagator(hm, gr, st, tr, time, dt)
+            end select
 
-          if(.not. cmplxscl) then
-            call density_calc(st, gr, st%rho)
-          else
-            call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
-          end if
-          call v_ks_calc(ks, hm, st, geo, time = time - dt)
-          SAFE_ALLOCATE(dtmp(1:gr%mesh%np))
-          d_max = M_ZERO
-          do is = 1, st%d%nspin
-            dtmp(1:gr%mesh%np) = hm%vhxc(1:gr%mesh%np, is) - vaux(1:gr%mesh%np, is)
-            d = dmf_nrm2(gr%mesh, dtmp)
-            if(d > d_max) d_max = d
+            if(.not. cmplxscl) then
+              call density_calc(st, gr, st%rho)
+            else
+              call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
+            end if
+            call v_ks_calc(ks, hm, st, geo, time = time - dt)
+            SAFE_ALLOCATE(dtmp(1:gr%mesh%np))
+            d_max = M_ZERO
+            do is = 1, st%d%nspin
+              dtmp(1:gr%mesh%np) = hm%vhxc(1:gr%mesh%np, is) - vaux(1:gr%mesh%np, is)
+              d = dmf_nrm2(gr%mesh, dtmp)
+              if(d > d_max) d_max = d
+            end do
+            SAFE_DEALLOCATE_A(dtmp)
+  
+            if(d_max < scf_threshold) exit
           end do
-          SAFE_DEALLOCATE_A(dtmp)
 
-          if(d_max < scf_threshold) exit
-        end do
-        
+        end if
+
+        SAFE_DEALLOCATE_A(zpsi1)
       end if
 
-      SAFE_DEALLOCATE_A(zpsi1)
-    end if
-    
-    SAFE_DEALLOCATE_A(vold)
-    SAFE_DEALLOCATE_A(vaux)
-    
-    SAFE_DEALLOCATE_A(Imvold)
-    SAFE_DEALLOCATE_A(Imvaux)
+      SAFE_DEALLOCATE_A(vold)
+      SAFE_DEALLOCATE_A(vaux)
 
-    ! update density
-    if(.not. propagator_dens_is_propagated(tr)) then 
-      if(.not. cmplxscl) then
-        call density_calc(st, gr, st%rho)
-      else
-        call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
-      end if  
-    end if
+      SAFE_DEALLOCATE_A(Imvold)
+      SAFE_DEALLOCATE_A(Imvaux)
 
-    generate = .false.
+      ! update density
+      if(.not. propagator_dens_is_propagated(tr)) then 
+        if(.not. cmplxscl) then
+          call density_calc(st, gr, st%rho)
+        else
+          call density_calc(st, gr, st%zrho%Re, st%zrho%Im)
+        end if  
+      end if
+
+      generate = .false.
 
-    if(ion_dynamics_ions_move(ions)) then
-      if(.not. propagator_ions_are_propagated(tr)) then
-        call ion_dynamics_propagate(ions, gr%sb, geo, abs(nt*dt), dt)
-        generate = .true.
+      if(ion_dynamics_ions_move(ions)) then
+        if(.not. propagator_ions_are_propagated(tr)) then
+          call ion_dynamics_propagate(ions, gr%sb, geo, abs(nt*dt), dt)
+          generate = .true.
+        end if
       end if
-    end if
 
-    if(gauge_field_is_applied(hm%ep%gfield) .and. .not. propagator_ions_are_propagated(tr)) then
-      call gauge_field_propagate(hm%ep%gfield, gauge_force, dt)
-    end if
+      if(gauge_field_is_applied(hm%ep%gfield) .and. .not. propagator_ions_are_propagated(tr)) then
+        call gauge_field_propagate(hm%ep%gfield, gauge_force, dt)
+      end if
 
-    if(generate .or. geometry_species_time_dependent(geo)) then
-      call hamiltonian_epot_generate(hm, gr, geo, st, time = abs(nt*dt))
-    end if
+      if(generate .or. geometry_species_time_dependent(geo)) then
+        call hamiltonian_epot_generate(hm, gr, geo, st, time = abs(nt*dt))
+      end if
 
-    update_energy_ = optional_default(update_energy, .false.)
+      update_energy_ = optional_default(update_energy, .false.)
 
-    if(update_energy_ .or. propagator_requires_vks(tr)) then
+      if(update_energy_ .or. propagator_requires_vks(tr)) then
 
-      ! save the vhxc potential for later
-      if(.not. propagator_requires_vks(tr)) then
-        SAFE_ALLOCATE(vold(1:gr%mesh%np, 1:st%d%nspin))
-        do ispin = 1, st%d%nspin
-          call lalg_copy(gr%mesh%np, hm%vhxc(:, ispin), vold(:, ispin))
-        end do
-        if(cmplxscl) then
-          SAFE_ALLOCATE(Imvold(1:gr%mesh%np, 1:st%d%nspin))
+        ! save the vhxc potential for later
+        if(.not. propagator_requires_vks(tr)) then
+          SAFE_ALLOCATE(vold(1:gr%mesh%np, 1:st%d%nspin))
           do ispin = 1, st%d%nspin
-            call lalg_copy(gr%mesh%np, hm%Imvhxc(:, ispin), Imvold(:, ispin))
+            call lalg_copy(gr%mesh%np, hm%vhxc(:, ispin), vold(:, ispin))
           end do
+          if(cmplxscl) then
+            SAFE_ALLOCATE(Imvold(1:gr%mesh%np, 1:st%d%nspin))
+            do ispin = 1, st%d%nspin
+              call lalg_copy(gr%mesh%np, hm%Imvhxc(:, ispin), Imvold(:, ispin))
+            end do
+          end if
         end if
-      end if
 
-      ! update Hamiltonian and eigenvalues (fermi is *not* called)
-      call v_ks_calc(ks, hm, st, geo, calc_eigenval = update_energy_, time = abs(nt*dt), calc_energy = update_energy_)
+        ! update Hamiltonian and eigenvalues (fermi is *not* called)
+        call v_ks_calc(ks, hm, st, geo, calc_eigenval = update_energy_, time = abs(nt*dt), calc_energy = update_energy_)
 
-      ! Get the energies.
-      if(update_energy_) call energy_calc_total(hm, gr, st, iunit = -1)
+        ! Get the energies.
+        if(update_energy_) call energy_calc_total(hm, gr, st, iunit = -1)               
 
-      ! restore the vhxc
-      if(.not. propagator_requires_vks(tr)) then
-        do ispin = 1, st%d%nspin
-          call lalg_copy(gr%mesh%np, vold(:, ispin), hm%vhxc(:, ispin))
-        end do
-        SAFE_DEALLOCATE_A(vold)
-        if(cmplxscl) then
+        ! restore the vhxc
+        if(.not. propagator_requires_vks(tr)) then
           do ispin = 1, st%d%nspin
-            call lalg_copy(gr%mesh%np, Imvold(:, ispin), hm%Imvhxc(:, ispin))
+            call lalg_copy(gr%mesh%np, vold(:, ispin), hm%vhxc(:, ispin))
           end do
-          SAFE_DEALLOCATE_A(Imvold)
+          SAFE_DEALLOCATE_A(vold)
+          if(cmplxscl) then
+            do ispin = 1, st%d%nspin
+              call lalg_copy(gr%mesh%np, Imvold(:, ispin), hm%Imvhxc(:, ispin))
+            end do
+            SAFE_DEALLOCATE_A(Imvold)
+          end if
         end if
       end if
-    end if
 
-    ! Recalculate forces, update velocities...
-    if(ion_dynamics_ions_move(ions)) then
-      call forces_calculate(gr, geo, hm%ep, st, abs(nt*dt), dt)
-      call ion_dynamics_propagate_vel(ions, geo, atoms_moved = generate)
-      if(generate) call hamiltonian_epot_generate(hm, gr, geo, st, time = abs(nt*dt))
-      geo%kinetic_energy = ion_dynamics_kinetic_energy(geo)
-    end if
+      ! Recalculate forces, update velocities...
+      if(ion_dynamics_ions_move(ions)) then
+        call forces_calculate(gr, geo, hm%ep, st, abs(nt*dt), dt)
+        call ion_dynamics_propagate_vel(ions, geo, atoms_moved = generate)
+        if(generate) call hamiltonian_epot_generate(hm, gr, geo, st, time = abs(nt*dt))
+        geo%kinetic_energy = ion_dynamics_kinetic_energy(geo)
+      end if
+
+      if(gauge_field_is_applied(hm%ep%gfield)) then
+        call gauge_field_get_force(gr, geo, hm%ep%proj, hm%phase, st, gauge_force)
+        call gauge_field_propagate_vel(hm%ep%gfield, gauge_force, dt)
+      end if
 
-    if(gauge_field_is_applied(hm%ep%gfield)) then
-      call gauge_field_get_force(gr, geo, hm%ep%proj, hm%phase, st, gauge_force)
-      call gauge_field_propagate_vel(hm%ep%gfield, gauge_force, dt)
     end if
 
     POP_SUB(propagator_dt)
@@ -736,76 +766,97 @@
 
       PUSH_SUB(propagator_dt.td_etrs)
 
-      if(hm%theory_level /= INDEPENDENT_PARTICLES) then
+      if(hm%maxwellcheck .eqv. .true.) then
 
-        SAFE_ALLOCATE(vhxc_t1(1:gr%mesh%np, 1:st%d%nspin))
-        SAFE_ALLOCATE(vhxc_t2(1:gr%mesh%np, 1:st%d%nspin))
-        call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t1)
+          do ik = st%d%kpt%start, st%d%kpt%end
+            do ib = st%group%block_start, st%group%block_end
 
-        call density_calc_init(dens_calc, st, gr, st%rho)
+              !save the state
+              call batch_copy(st%group%psib(ib, ik), zpsib_save, reference = .false.)
+              if(batch_is_packed(st%group%psib(ib, ik))) call batch_pack(zpsib_save, copy = .false.)
+              call batch_copy_data(gr%der%mesh%np, st%group%psib(ib, ik), zpsib_save)
 
-        do ik = st%d%kpt%start, st%d%kpt%end
-          do ib = st%group%block_start, st%group%block_end
+              !propagate the state dt with H(time - dt)
+              call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, dt/mu, time - dt)
 
-            !save the state
-            call batch_copy(st%group%psib(ib, ik), zpsib_save, reference = .false.)
-            if(batch_is_packed(st%group%psib(ib, ik))) call batch_pack(zpsib_save, copy = .false.)
-            call batch_copy_data(gr%der%mesh%np, st%group%psib(ib, ik), zpsib_save)
+              call batch_end(zpsib_save)
 
-            !propagate the state dt with H(time - dt)
-            call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, dt/mu, time - dt)
-            call density_calc_accumulate(dens_calc, ik, st%group%psib(ib, ik))
+            end do
+          end do
 
-            !restore the saved state
-            call batch_copy_data(gr%der%mesh%np, zpsib_save, st%group%psib(ib, ik))
+      else
 
-            call batch_end(zpsib_save)
+        if(hm%theory_level /= INDEPENDENT_PARTICLES) then
 
-          end do
-        end do
+          SAFE_ALLOCATE(vhxc_t1(1:gr%mesh%np, 1:st%d%nspin))
+          SAFE_ALLOCATE(vhxc_t2(1:gr%mesh%np, 1:st%d%nspin))
+          call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t1)
 
-        call density_calc_end(dens_calc)
+          call density_calc_init(dens_calc, st, gr, st%rho)
 
-        call v_ks_calc(ks, hm, st, geo)
+          do ik = st%d%kpt%start, st%d%kpt%end
+            do ib = st%group%block_start, st%group%block_end
 
-        call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t2)
-        call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t1, hm%vhxc)
-        call hamiltonian_update(hm, gr%mesh, time = time - dt)
-      end if
+              !save the state
+              call batch_copy(st%group%psib(ib, ik), zpsib_save, reference = .false.)
+              if(batch_is_packed(st%group%psib(ib, ik))) call batch_pack(zpsib_save, copy = .false.)
+              call batch_copy_data(gr%der%mesh%np, st%group%psib(ib, ik), zpsib_save)
 
-      ! propagate dt/2 with H(time - dt)
-      do ik = st%d%kpt%start, st%d%kpt%end
-        do ib = st%group%block_start, st%group%block_end
-          call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, dt/(mu*M_TWO), time - dt)
+              !propagate the state dt with H(time - dt)
+              call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, dt/mu, time - dt)
+              call density_calc_accumulate(dens_calc, ik, st%group%psib(ib, ik))
+
+              !restore the saved state
+              call batch_copy_data(gr%der%mesh%np, zpsib_save, st%group%psib(ib, ik))
+
+              call batch_end(zpsib_save)
+
+            end do
+          end do
+
+          call density_calc_end(dens_calc)
+  
+          call v_ks_calc(ks, hm, st, geo)
+
+          call lalg_copy(gr%mesh%np, st%d%nspin, hm%vhxc, vhxc_t2)
+          call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t1, hm%vhxc)
+          call hamiltonian_update(hm, gr%mesh, time = time - dt)
+        end if
+
+        ! propagate dt/2 with H(time - dt)
+        do ik = st%d%kpt%start, st%d%kpt%end
+          do ib = st%group%block_start, st%group%block_end
+            call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, dt/(mu*M_TWO), time - dt)
+          end do
         end do
-      end do
 
-      ! propagate dt/2 with H(t)
+        ! propagate dt/2 with H(t)
 
-      ! first move the ions to time t
-      if(ion_dynamics_ions_move(ions)) then
-        call ion_dynamics_propagate(ions, gr%sb, geo, time, dt)
-        call hamiltonian_epot_generate(hm, gr, geo, st, time = time)
-      end if
+        ! first move the ions to time t
+        if(ion_dynamics_ions_move(ions)) then
+          call ion_dynamics_propagate(ions, gr%sb, geo, time, dt)
+          call hamiltonian_epot_generate(hm, gr, geo, st, time = time)
+        end if
 
-      if(gauge_field_is_applied(hm%ep%gfield)) then
-        call gauge_field_propagate(hm%ep%gfield, gauge_force, dt)
-      end if
+        if(gauge_field_is_applied(hm%ep%gfield)) then
+          call gauge_field_propagate(hm%ep%gfield, gauge_force, dt)
+        end if
 
-      if(hm%theory_level /= INDEPENDENT_PARTICLES) then
-        call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t2, hm%vhxc)
-      end if
-      call hamiltonian_update(hm, gr%mesh, time = time)
+        if(hm%theory_level /= INDEPENDENT_PARTICLES) then
+          call lalg_copy(gr%mesh%np, st%d%nspin, vhxc_t2, hm%vhxc)
+        end if
+        call hamiltonian_update(hm, gr%mesh, time = time)
 
-      do ik = st%d%kpt%start, st%d%kpt%end
-        do ib = st%group%block_start, st%group%block_end
-          call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, dt/(M_TWO*mu), time)
+        do ik = st%d%kpt%start, st%d%kpt%end
+          do ib = st%group%block_start, st%group%block_end
+            call exponential_apply_batch(tr%te, gr%der, hm, st%group%psib(ib, ik), ik, dt/(M_TWO*mu), time)
+          end do
         end do
-      end do
 
-      if(hm%theory_level /= INDEPENDENT_PARTICLES) then
-        SAFE_DEALLOCATE_A(vhxc_t1)
-        SAFE_DEALLOCATE_A(vhxc_t2)
+        if(hm%theory_level /= INDEPENDENT_PARTICLES) then
+          SAFE_DEALLOCATE_A(vhxc_t1)
+          SAFE_DEALLOCATE_A(vhxc_t2)
+        end if
       end if
 
       POP_SUB(propagator_dt.td_etrs)
