DebugLevel = 1

ProfilingMode = yes

CalculationMode = gs

Dimensions        = 3

BoxShape = PARALLELEPIPED
Lsize    = 20.0
Spacing  = 0.5

XCFunctional = lda_x + lda_c_gl

ConvRelDens = 8e-6

EigenSolverTolerance = 1e-5

ParallelizationStrategy = par_domains

%Species
'jellium'     | species_jellium       | jellium_radius | 8.0 | valence | 8
%

%Coordinates
'jellium' | 0 | 0 | 0
%



