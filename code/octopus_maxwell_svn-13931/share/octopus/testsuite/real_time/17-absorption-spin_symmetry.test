# -*- coding: utf-8 mode: shell-script -*-
# $Id: 17-absorption-spin_symmetry.test 13244 2015-02-28 23:19:28Z dstrubbe $

Test       : Absorption spectrum with spin-symmetry
Program    : octopus
TestGroups : short-run, real_time
Enabled    : Yes

# ground state
Processors : 1
Input      : 17-absorption-spin_symmetry.01-gs.inp
match ; Initial energy ; GREPFIELD(static/info, 'Total       =', 3) ; -11.35988933

Processors : 4
Input      : 17-absorption-spin_symmetry.02-td.inp
match ; Energy [step   1] ; LINEFIELD(td.general/energy, -101, 3) ; -1.135991911595e+01
match ; Energy [step  25] ; LINEFIELD(td.general/energy, -76, 3) ; -1.135839524059e+01
match ; Energy [step  50] ; LINEFIELD(td.general/energy, -51, 3) ; -1.135839521143e+01
match ; Energy [step  75] ; LINEFIELD(td.general/energy, -26, 3) ; -1.135839517954e+01
match ; Energy [step 100] ; LINEFIELD(td.general/energy, -1, 3) ; -1.135839514958e+01

Util       : oct-propagation_spectrum
Input      : 17-absorption-spin_symmetry.03-spectrum.inp
match ; Electronic sum rule ; GREPFIELD(cross_section_vector.1, 'Electronic sum rule', 6) ; 1.976021
match ; Polarizability sum rule ; GREPFIELD(cross_section_vector.1, 'Polarizability (sum rule)', 6) ; 16.425072

Precision : 1e-7

match ; Energy      1 ; LINEFIELD(cross_section_tensor,  -91, 1) ; 1.0
match ; Sigma       1 ; LINEFIELD(cross_section_tensor,  -91, 2) ; 0.44522602E-01
match ; Anisotropy  1 ; LINEFIELD(cross_section_tensor,  -91, 3) ; 0.44824329E-01

match ; Energy      2 ; LINEFIELD(cross_section_tensor,  -81, 1) ; 2.0
match ; Sigma       2 ; LINEFIELD(cross_section_tensor,  -81, 2) ; 0.16273168E+00
match ; Anisotropy  2 ; LINEFIELD(cross_section_tensor,  -81, 3) ; 0.16352124E+00

match ; Energy      3 ; LINEFIELD(cross_section_tensor,  -71, 1) ; 3.0
match ; Sigma       3 ; LINEFIELD(cross_section_tensor,  -71, 2) ; 0.31416863E+00
match ; Anisotropy  3 ; LINEFIELD(cross_section_tensor,  -71, 3) ; 0.31462846E+00

match ; Energy      4 ; LINEFIELD(cross_section_tensor,  -61, 1) ; 4.0
match ; Sigma       4 ; LINEFIELD(cross_section_tensor,  -61, 2) ; 0.44791736E+00
match ; Anisotropy  4 ; LINEFIELD(cross_section_tensor,  -61, 3) ; 0.44625763E+00

match ; Energy      5 ; LINEFIELD(cross_section_tensor,  -51, 1) ; 5.0
match ; Sigma       5 ; LINEFIELD(cross_section_tensor,  -51, 2) ; 0.52056209E+00
match ; Anisotropy  5 ; LINEFIELD(cross_section_tensor,  -51, 3) ; 0.51472024E+00

match ; Energy      6 ; LINEFIELD(cross_section_tensor,  -41, 1) ; 6.0
match ; Sigma       6 ; LINEFIELD(cross_section_tensor,  -41, 2) ; 0.51061328E+00
match ; Anisotropy  6 ; LINEFIELD(cross_section_tensor,  -41, 3) ; 0.49932259E+00

match ; Energy      7 ; LINEFIELD(cross_section_tensor,  -31, 1) ; 7.0
match ; Sigma       7 ; LINEFIELD(cross_section_tensor,  -31, 2) ; 0.42419807E+00
match ; Anisotropy  7 ; LINEFIELD(cross_section_tensor,  -31, 3) ; 0.40792868E+00

match ; Energy      8 ; LINEFIELD(cross_section_tensor,  -21, 1) ; 8.0
match ; Sigma       8 ; LINEFIELD(cross_section_tensor,  -21, 2) ; 0.29046706E+00
match ; Anisotropy  8 ; LINEFIELD(cross_section_tensor,  -21, 3) ; 0.27183323E+00

match ; Energy      9 ; LINEFIELD(cross_section_tensor,  -11, 1) ; 9.0
match ; Sigma       9 ; LINEFIELD(cross_section_tensor,  -11, 2) ; 0.14924976E+00
match ; Anisotropy  9 ; LINEFIELD(cross_section_tensor,  -11, 3) ; 0.13309769E+00

match ; Energy     10 ; LINEFIELD(cross_section_tensor,   -1, 1) ; 10.0
match ; Sigma      10 ; LINEFIELD(cross_section_tensor,   -1, 2) ; 0.36293156E-01
match ; Anisotropy 10 ; LINEFIELD(cross_section_tensor,   -1, 3) ; 0.36178268E-01
